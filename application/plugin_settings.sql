prompt --application/plugin_settings
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(32484096640919901)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_DISPLAY_SELECTOR'
,p_attribute_01=>'N'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(32513655213921548)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IR'
,p_attribute_01=>'LEGACY'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(32529064198921552)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_COLOR_PICKER'
,p_attribute_01=>'FULL'
,p_attribute_02=>'POPUP'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(35395854626727756)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_YES_NO'
,p_attribute_01=>'Y'
,p_attribute_03=>'N'
,p_attribute_05=>'SELECT_LIST'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(122951785222055753)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'PLUGIN_BE.CTB.SELECT2'
,p_attribute_01=>'No encontrado'
,p_attribute_02=>'Texto muy corto'
,p_attribute_03=>'Seleccion muy grande'
,p_attribute_04=>'Buscando...'
,p_attribute_06=>'Leyendo mas resultados'
,p_attribute_07=>'SELECT2'
,p_attribute_08=>'Error de lectura'
,p_attribute_09=>'Texto muy largo'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(234321060025823947)
,p_plugin_type=>'WEB SOURCE TYPE'
,p_plugin=>'NATIVE_ADFBC'
,p_version_scn=>6188208287347
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(554821477822891159)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_DATE_PICKER_APEX'
,p_attribute_01=>'MONTH-PICKER:YEAR-PICKER:TODAY-BUTTON'
,p_attribute_02=>'VISIBLE'
,p_attribute_03=>'15'
,p_attribute_04=>'FOCUS'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(554821628317891159)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_GEOCODED_ADDRESS'
,p_attribute_01=>'RELAX_HOUSE_NUMBER'
,p_attribute_02=>'N'
,p_attribute_03=>'POPUP:ITEM'
,p_attribute_04=>'default'
,p_attribute_06=>'LIST'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(554821753892891159)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_SINGLE_CHECKBOX'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(554821866374891160)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_MAP_REGION'
,p_attribute_01=>'Y'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(554822060215891160)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_STAR_RATING'
,p_attribute_01=>'fa-star'
,p_attribute_04=>'#VALUE#'
,p_version_scn=>6188208287363
);
wwv_flow_imp_shared.create_plugin_setting(
 p_id=>wwv_flow_imp.id(2282252064784740430)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'PLUGIN_COM_SKILLBUILDERS_SUPER_LOV'
,p_attribute_01=>'LIKE_CASE'
,p_attribute_03=>'DEFAULT'
,p_attribute_04=>'clock'
,p_attribute_06=>'0'
,p_attribute_07=>'Y'
,p_attribute_08=>'Datos no encontrados'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
