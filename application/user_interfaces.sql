prompt --application/user_interfaces
begin
--   Manifest
--     USER INTERFACES: 211
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_user_interface(
 p_id=>wwv_flow_imp.id(211)
,p_theme_id=>104
,p_home_url=>'f?p=201:1:&SESSION.:::1:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,F_VERSION:f?p=201,&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,'
||'&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,,,,,,N,&F_VERSION.'
,p_login_url=>'f?p=155:101:&APP_SESSION.::&DEBUG.:::'
,p_theme_style_by_user_pref=>false
,p_global_page_id=>0
,p_nav_list_template_options=>'#DEFAULT#'
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css?rel=stylesheet ',
'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css?integrity=sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=?crossorigin=anonymous ',
'#WORKSPACE_FILES#estilos.css'))
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js ',
'#WORKSPACE_FILES#recursos.js ',
'#WORKSPACE_FILES#validaciones.js'))
,p_include_legacy_javascript=>'PRE18:18'
,p_include_jquery_migrate=>true
,p_nav_bar_type=>'NAVBAR'
,p_nav_bar_template_options=>'#DEFAULT#'
);
wwv_flow_imp.component_end;
end;
/
