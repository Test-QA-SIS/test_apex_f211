prompt --application/shared_components/user_interface/templates/button/btn_mx_recalcular
begin
--   Manifest
--     BUTTON TEMPLATE: BTN_MX_RECALCULAR
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_button_templates(
 p_id=>wwv_flow_imp.id(270536254089046676)
,p_template_name=>'Btn_Mx_Recalcular'
,p_internal_name=>'BTN_MX_RECALCULAR'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'<table class="t10ButtonAlternative3" cellspacing="0" cellpadding="0" border="0"  summary="">',
'<tr>',
'<td class="t10L"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/theme_101/btnizq.gif" alt="" /></a></td>',
'<td class="t10L"background="#IMAGE_PREFIX#themes/theme_101/btnbg.gif"><a href="#LINK#"><img height="25" src="#IMAGE_PREFIX#themes/theme_101/recalcular.gif" alt="" /></a></td>',
'<td class="t10L"background="#IMAGE_PREFIX#themes/theme_101/btnbg.gif"><a href="#LINK#">#LABEL#</a></td>',
'<td class="t10R"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/theme_101/btnder.gif" alt="" /></a></td>',
'</tr>',
'</table>'))
,p_translate_this_template=>'N'
,p_theme_class_id=>6
,p_theme_id=>104
);
wwv_flow_imp.component_end;
end;
/
