prompt --application/shared_components/user_interface/templates/button/button_alternative_1
begin
--   Manifest
--     BUTTON TEMPLATE: BUTTON,_ALTERNATIVE_1
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_button_templates(
 p_id=>wwv_flow_imp.id(270536673013046677)
,p_template_name=>'Button, Alternative 1'
,p_internal_name=>'BUTTON,_ALTERNATIVE_1'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t10ButtonAlternative1" cellspacing="0" cellpadding="0" border="0"  summary="">',
'<tr>',
'<td class="t10L"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/theme_101/button_alt1_left.gif" alt="" /></a></td>',
'<td class="t10C"><a href="#LINK#">#LABEL#</a></td>',
'<td class="t10R"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/theme_101/button_alt1_right.gif" alt="" /></a></td>',
'</tr>',
'</table>'))
,p_translate_this_template=>'N'
,p_theme_class_id=>4
,p_theme_id=>104
);
wwv_flow_imp.component_end;
end;
/
