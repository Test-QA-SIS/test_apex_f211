prompt --application/shared_components/user_interface/templates/page/login
begin
--   Manifest
--     TEMPLATE: LOGIN
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_template(
 p_id=>wwv_flow_imp.id(270517656404046664)
,p_theme_id=>104
,p_name=>'Login'
,p_internal_name=>'LOGIN'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE.">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_101/theme_3_1.css" type="text/css" />',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#',
''))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<br>',
'<br>',
'<br>',
'<table width="200" border="0" align="center" cellpadding="0" cellspacing="0" >',
'  <tr>',
'    <td width="22"><img src="../../i/aplimar/izsup.gif" width="22" height="25" /></td>',
'    <td width="159" background="../../i/aplimar/sup.gif"></td>',
'    <td width="19"><img src="../../i/aplimar/dersup.gif" width="19" height="25" /></td>',
'  </tr>',
'  <tr>',
'    <td background="../../i/aplimar/izq.gif"></td>',
'    <td><table align="center" valign="bottom" bgcolor="#ffffff" height="300"  border="0" cellpadding="10" cellspacing="10" summary=""  >',
'      <tr>',
'        <td align="center"><img src="../../i/aplimar/acj.jpg" width="300"  align="middle" /></td>',
'      </tr>',
'      <tr>',
'        <td>#BODY#</td>',
'      </tr>',
'    </table>',
'<br>',
'</td>',
'    <td background="../../i/aplimar/der.gif"></td>',
'  </tr>',
'  <tr>',
'    <td><img src="../../i/aplimar/infizq.gif" width="22" height="17" /></td>',
'    <td background="../../i/aplimar/inf.gif"></td>',
'    <td><img src="../../i/aplimar/infder.gif" width="19" height="17" /></td>',
'  </tr>',
'  <tr>',
'    <td></td>',
'    <td></td>',
'    <td></td>',
'  </tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE##DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>',
''))
,p_success_message=>'<div class="t10success" id="MESSAGE"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''MESSAGE'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</div>'
,p_notification_message=>'<div class="t10notification" id="MESSAGE"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''MESSAGE'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</div>'
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>'
,p_region_table_cattributes=>'width="100%"'
,p_theme_class_id=>6
,p_grid_type=>'TABLE'
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554846340741895120)
,p_page_template_id=>wwv_flow_imp.id(270517656404046664)
,p_name=>'Page Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>true
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp.component_end;
end;
/
