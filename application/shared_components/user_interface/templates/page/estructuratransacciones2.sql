prompt --application/shared_components/user_interface/templates/page/estructuratransacciones2
begin
--   Manifest
--     TEMPLATE: ESTRUCTURATRANSACCIONES2
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_template(
 p_id=>wwv_flow_imp.id(70829267270734112)
,p_theme_id=>104
,p_name=>'EstructuraTransacciones2'
,p_internal_name=>'ESTRUCTURATRANSACCIONES2'
,p_is_popup=>false
,p_javascript_code_onload=>wwv_flow_string.join(wwv_flow_t_varchar2(
'$("body").on("dialogcreate", ".ui-dialog--apex", function(e) {        ',
'   //var posX = $(this).position().left + 100 + $(this).outerWidth();',
'  // var posY = $(this).position().top + $(this).outerHeight();',
'   var rect = this.getBoundingClientRect();',
'   console.log(rect.top, rect.right, rect.bottom, rect.left);',
'   var posY = rect.top  + 200;',
'   var posX = rect.left + 350;',
'   //alert("XX="+posX+" YY="+posY);',
'    $(this).children(".ui-dialog-content")',
'        .dialog("option", "hide", {effect: "slideUp", duration: 1000})',
'        .dialog("option", "show", {effect: "slideDown", duration: 1000})',
'        .dialog(''option'', ''position'', [posX, posY]);',
'});'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE.">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_101/theme_3_1.css" type="text/css" />',
' #APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'',
'',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#',
''))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table  border="0" cellpadding="0" cellspacing="0" summary="" width="100%">',
'<tr>  ',
'  <td colspan="3" align="center" valign="top" class="t10Logo color_emp_&F_EMP_ID."> <img align="center" src="#WORKSPACE_FILES#emp_logo/emp_&F_EMP_ID..png" /> </td>   ',
'  </tr>',
'<tr bgcolor="#eaeae6">',
'<td valign="top" class="t10Logo">#LOGO##REGION_POSITION_06#</td>',
'<td align="center" valign="top"> </td>',
'<td valign="top" class="t10NavBar">&APP_USER.#NAVIGATION_BAR##REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" width="100%">',
'<tr>',
'<td colspan="4" background="#IMAGE_PREFIX#themes/theme_101/tab_top_back_off.gif"><table border="0" cellpadding="0" cellspacing="0" summary=""><tr><td><img src="#IMAGE_PREFIX#themes/theme_101/tab_right.gif" alt="" /></td>#TAB_CELLS#<td width="100%"><d'
||'iv class="t10Customize">#CUSTOMIZE#</div></td></tr></table></td>',
'</tr>',
'<tr>',
'<td width="1%" valign="top" style="background-color:#eaeae6;"><img src="#IMAGE_PREFIX#themes/theme_101/1px_trans.gif" width="1" height="1" alt="" /></td>',
'<td  colspan="3" class="t10BreadCrumbRegion"><img src="#IMAGE_PREFIX#themes/theme_101/1px_trans.gif" width="1" height="1" alt="" /></td>',
'</tr>',
'<tr>',
'  <td valign="top" style="background-color:#eaeae6;"><img src="#IMAGE_PREFIX#themes/theme_101/1px_trans.gif" width="1" height="1" alt="" /></td>',
'    <td colspan="3" background="#IMAGE_PREFIX#themes/theme_101/breadcrumb_bottom.gif"><img src="#IMAGE_PREFIX#themes/theme_101/breadcrumb_bottom.gif" alt="" /></td>',
'  </tr>',
'  <tr>',
'  <td height="98" colspan="2" valign="top" class="t10SidebarLayout" style="padding-left:1px;padding-right:1px;background-color:#eaeae6;"><span class="t10SidebarLayout" style="padding-left:1px;padding-right:1px;background-color:#eaeae6;">#REGION_POSIT'
||'ION_02#</span> </td>',
'  <td width="98%" align="left" valign="top" class="t10ContentBody">',
'	<table width="100%" border="0" align="left">',
'        <tr>',
'          <td><span class="t10messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</span></td>',
'        </tr>',
'    ',
'	    <tr>',
'	      <td align="left" valign="top">',
'          <table  border="0" align="left" cellpadding="0" cellspacing="0" >',
'            <tr>',
'              <td width="2"   align="right" ><img name="fon_com" src="#WORKSPACE_IMAGES#fon_com.jpg" width="1" height="1" border="0" id="fon_com" alt="" /></td>',
'              <td colspan="2" bgcolor="#CCCCCC" class="EstArriba2"> </td>',
'              <td  align="left"  ><img name="der1" src="#WORKSPACE_IMAGES#der1.jpg" width="12" height="1" border="0" id="der1" alt="" /></td>',
'            </tr>',
'            <tr>',
'              <td bgcolor="#CCCCCC" class="EstIzq2"></td>',
'              <td colspan="2" align="left" valign="top"><table border="0" cellpadding="10" cellspacing="10" bordercolor="#EAEAE8" bgcolor="#EAEAE8">',
'                <tr>',
'                  <td><table align="left" cellpadding="0" cellspacing="0" bgcolor="#EAEAE8">',
'                    <tr>',
'                      <td colspan="3" align="left" valign="top"  >#BODY# </td>',
'                    </tr>',
'                    <tr>',
'                      <td colspan="3" align="left" valign="top">#REGION_POSITION_07#</td>',
'                    </tr>',
'                    <tr>',
'                      <td align="left" valign="top">#REGION_POSITION_03#</td>',
'                      <td>&nbsp;</td>',
'                      <td align="right" valign="top">#REGION_POSITION_04#</td>',
'                    </tr>',
'                    <tr>',
'                      <td colspan="3" align="left" valign="top">#REGION_POSITION_05#</td>',
'                    </tr>',
'                  </table></td>',
'                </tr>',
'              </table></td>',
'              <td valign="top" background="#IMAGE_PREFIX#themes/theme_101/derfon.jpg" ><img name="der2" src="#WORKSPACE_IMAGES#der2.jpg" width="12" height="21" border="0" id="der2" alt="" /></td>',
'            </tr>',
'            ',
'          ',
'            <tr>',
'              <td align="right"><img name="izq_inf1" src="#WORKSPACE_IMAGES#izq_inf1.jpg" width="1" height="11" border="0" id="izq_inf1" alt="" /></td>',
'              <td class="EstAbajo2"><img name="izq_inf2" src="#WORKSPACE_IMAGES#izq_inf2.jpg" width="8" height="11" border="0" id="izq_inf2" alt="" /></td>',
'              <td   background="#IMAGE_PREFIX#themes/theme_101/inf_fon.jpg" class="EstAbajo2"><img name="inf_fon" src="#WORKSPACE_IMAGES#inf_fon.jpg" width="1" height="11" border="0" id="inf_fon" alt="" /></td>',
'              <td  width="12" align="left"><img name="der_inf" src="#WORKSPACE_IMAGES#der_inf.jpg" width="12" height="11" border="0" id="der_inf" alt="" /></td>',
'            </tr>',
'          </table></td>',
'        </tr>',
'    </table>    </td>',
'  </tr>',
'</table>',
''))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#REGION_POSITION_05#',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>'<div class="t10success" id="MESSAGE"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''MESSAGE'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</div>'
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_101/arrows.gif" alt="" /></td>',
'<td><span class="t10standardtabcurrent">#TAB_LABEL#</span></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_101/tab_right.gif" alt="" />#TAB_INLINE_EDIT#</td>'))
,p_non_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><a href="#TAB_LINK#" class="t10standardtab">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_101/tab_right.gif" alt="" />#TAB_INLINE_EDIT#</td>',
''))
,p_notification_message=>'<div class="t10notification" id="MESSAGE"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''MESSAGE'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</div>'
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'&nbsp;|&nbsp;<a href="#LINK#">#TEXT#</a>'
,p_region_table_cattributes=>'width="100%" cellpadding="0" cellspacing="0" summary=""'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_theme_class_id=>16
,p_grid_type=>'TABLE'
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554845531787895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>true
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554845627203895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 2'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554845698267895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 3'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554845801967895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 4'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554845946364895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 5'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554846043488895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 6'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554846114650895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 7'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_page_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554846154839895120)
,p_page_template_id=>wwv_flow_imp.id(70829267270734112)
,p_name=>'Page Position 8'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp.component_end;
end;
/
