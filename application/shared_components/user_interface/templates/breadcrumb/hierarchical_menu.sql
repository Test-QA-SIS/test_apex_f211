prompt --application/shared_components/user_interface/templates/breadcrumb/hierarchical_menu
begin
--   Manifest
--     MENU TEMPLATE: HIERARCHICAL_MENU
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_menu_template(
 p_id=>wwv_flow_imp.id(270537576417046677)
,p_name=>'Hierarchical Menu'
,p_internal_name=>'HIERARCHICAL_MENU'
,p_before_first=>'<ul class="t10HierarchicalMenu">'
,p_current_page_option=>'<li class="t10current"><span>#NAME#</span></li>'
,p_non_current_page_option=>'<li><a href="#LINK#">#NAME#</a></li>'
,p_after_last=>'</ul>'
,p_max_levels=>11
,p_start_with_node=>'CHILD_MENU'
,p_theme_id=>104
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
wwv_flow_imp.component_end;
end;
/
