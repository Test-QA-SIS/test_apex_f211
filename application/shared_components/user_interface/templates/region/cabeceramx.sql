prompt --application/shared_components/user_interface/templates/region/cabeceramx
begin
--   Manifest
--     REGION TEMPLATE: CABECERAMX
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_plug_template(
 p_id=>wwv_flow_imp.id(270521883268046667)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table  align="center" class="t10ReportsRegion" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td height="9" colspan="3"   > </td>',
'</tr>',
'<tr>',
'<td width="5"  ></td>',
'<td   align="right">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td>',
'<td width="3"  ></td>',
'</tr>',
'<tr>',
'  <td colspan="3"  class="t10RegionLeft"><table width="100%" border="0" cellpadding="0"  cellspacing="0">',
'      <tr>',
'        <td width="2%" align="left"><img src="#IMAGE_PREFIX#themes/theme_101/titulo_izq.gif" alt="" width="15" height="40"/></td>',
'        <td width="81%"  background="#IMAGE_PREFIX#themes/theme_101/titulo_medio.gif">#TITLE# </td>',
'        <td width="17%"  background="#IMAGE_PREFIX#themes/theme_101/titulo_medio.gif" align="right"><img src="#IMAGE_PREFIX#themes/theme_101/titulo_der.gif" alt="" width="15" height="40"/></td>',
'      </tr>',
'    </table></td>',
'  </tr>',
'<tr>',
'<td class="t10RegionLeft"></td>',
'<td class="t10RegionBody" bgcolor="#EAEAE8">#BODY#</td>',
'<td class="t10RegionLeft"></td>',
'</tr>',
'<tr>',
'<td></td>',
'<td class="t10RegionFooter">&nbsp;</td>',
'<td></td>',
'</tr>',
'</table>'))
,p_page_plug_template_name=>'CabeceraMX'
,p_internal_name=>'CABECERAMX'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>104
,p_theme_class_id=>7
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554857640570895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>true
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554857683558895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554857828519895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Edit'
,p_placeholder=>'EDIT'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554857924551895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Expand'
,p_placeholder=>'EXPAND'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858016983895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Create'
,p_placeholder=>'CREATE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858114169895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Create2'
,p_placeholder=>'CREATE2'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858161658895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Close'
,p_placeholder=>'CLOSE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858296779895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Help'
,p_placeholder=>'HELP'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858361767895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Delete'
,p_placeholder=>'DELETE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858455819895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Copy'
,p_placeholder=>'COPY'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858634922895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Next'
,p_placeholder=>'NEXT'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858701719895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Previous'
,p_placeholder=>'PREVIOUS'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554858831323895123)
,p_plug_template_id=>wwv_flow_imp.id(270521883268046667)
,p_name=>'Change'
,p_placeholder=>'CHANGE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp.component_end;
end;
/
