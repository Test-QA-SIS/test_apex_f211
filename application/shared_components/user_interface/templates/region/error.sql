prompt --application/shared_components/user_interface/templates/region/error
begin
--   Manifest
--     REGION TEMPLATE: ERROR
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_plug_template(
 p_id=>wwv_flow_imp.id(270522778424046667)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#eaeae6"  id="#REGION_STATIC_ID#" summary="" #REGION_ATTRIBUTES#>',
'<tr>',
'<td> </td>',
'<td  ><table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td> </td>',
'<td bgcolor="#eaeae6" ></td>',
'<td> </td>',
'</tr>',
'</table></td>',
'<td> </td>',
'</tr>',
'<tr>',
'<td  > </td>',
'<td  align="right" class="t10RegionFooter"><font color="#9B0000">#CLOSE#   #PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</font></td>',
'<td  > </td>',
'</tr>',
'<tr>',
'<td  > </td>',
'<td class="t10RegionBody "><font color="#9B0000"><img src="#IMAGE_PREFIX#themes/theme_101/error.gif" alt="" width="34" height="36"/><br />',
'  #BODY#</font></td>',
'<td  > </td>',
'</tr>',
'<tr>',
'<td> </td>',
'<td class="t10RegionFooter">&nbsp;</td>',
'<td></td>',
'</tr>',
'</table>'))
,p_page_plug_template_name=>'ERROR'
,p_internal_name=>'ERROR'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>104
,p_theme_class_id=>8
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554861539561895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>true
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554861600961895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554861667905895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Edit'
,p_placeholder=>'EDIT'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554861768375895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Expand'
,p_placeholder=>'EXPAND'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554861938243895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Create'
,p_placeholder=>'CREATE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862022933895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Create2'
,p_placeholder=>'CREATE2'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862145944895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Close'
,p_placeholder=>'CLOSE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862192570895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Help'
,p_placeholder=>'HELP'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862295707895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Delete'
,p_placeholder=>'DELETE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862449929895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Copy'
,p_placeholder=>'COPY'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862469350895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Next'
,p_placeholder=>'NEXT'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862560720895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Previous'
,p_placeholder=>'PREVIOUS'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554862653729895124)
,p_plug_template_id=>wwv_flow_imp.id(270522778424046667)
,p_name=>'Change'
,p_placeholder=>'CHANGE'
,p_has_grid_support=>false
,p_has_region_support=>false
,p_has_item_support=>false
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp.component_end;
end;
/
