prompt --application/shared_components/user_interface/templates/region/bracketed_region
begin
--   Manifest
--     REGION TEMPLATE: BRACKETED_REGION
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_plug_template(
 p_id=>wwv_flow_imp.id(270520665472046666)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table   border="0">',
'  <tr>',
'    <td  height="9" colspan="3" background="#IMAGE_PREFIX#themes/theme_101/estruc_linea.gif"><img src="#IMAGE_PREFIX#themes/theme_101/estruc_linea.gif" alt="" width="1" height="9"/></td>',
'  </tr>',
'  <tr>',
'    <td colspan="3"><span class="t10RegionBody">#TITLE#</span></td>',
'  </tr>',
'  <tr>',
'    <td  height="9" colspan="3" background="#IMAGE_PREFIX#themes/theme_101/estruc_linea.gif"><img src="#IMAGE_PREFIX#themes/theme_101/estruc_linea.gif" alt="" width="1" height="9"/></td>',
'  </tr>',
'  <tr>',
'    <td class="t10RegionLeft"> </td>',
'    <td  bgcolor="#EAEAE8"><span class="t10RegionBody">#BODY#</span></td>',
'    <td  class="t10RegionLeft"> </td>',
'  </tr>',
'  <tr>',
'    <td class="t10RegionLeft"> </td>',
'    <td class="t10RegionLeft"> </td>',
'    <td class="t10RegionLeft"></td>',
'  </tr>',
'</table>'))
,p_page_plug_template_name=>'Bracketed Region'
,p_internal_name=>'BRACKETED_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>104
,p_theme_class_id=>18
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554854628236895122)
,p_plug_template_id=>wwv_flow_imp.id(270520665472046666)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>true
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554854672133895122)
,p_plug_template_id=>wwv_flow_imp.id(270520665472046666)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp.component_end;
end;
/
