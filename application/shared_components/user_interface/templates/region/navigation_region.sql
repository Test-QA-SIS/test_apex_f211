prompt --application/shared_components/user_interface/templates/region/navigation_region
begin
--   Manifest
--     REGION TEMPLATE: NAVIGATION_REGION
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_plug_template(
 p_id=>wwv_flow_imp.id(270523665656046668)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t10NavigationRegionAlternative1" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# border="0" cellpadding="0" cellspacing="0" summary="">',
'        <tbody><tr>',
'          <td><img src="#IMAGE_PREFIX#themes/theme_101/sb1.gif" height="28" width="11" alt=""></td>',
'          <td background="#IMAGE_PREFIX#themes/theme_101/sb2.gif" nowrap="nowrap"><strong class="tabletophead">#TITLE#</strong></td>',
'          <td><img src="#IMAGE_PREFIX#themes/theme_101/sb3.gif" height="28" width="12" alt=""></td>',
'        </tr>',
'        <tr>',
'          <td background="#IMAGE_PREFIX#themes/theme_101/sb4.gif"><br /></td>',
'          <td bgcolor="#f8f8f8" width="200"><table border="0" cellpadding="0" cellspacing="0" width="100%" summary="">',
'              <tbody><tr>',
'                <td class="helptext" width="100">#BODY#</td>',
'              </tr>',
'</tbody></table></td>',
'<td background="#IMAGE_PREFIX#themes/theme_101/sb5.gif"><br /></td>',
'</tr>',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_101/sb6.gif" height="11" width="11" alt=""></td>',
'<td background="#IMAGE_PREFIX#themes/theme_101/sb7.gif"><img src="#IMAGE_PREFIX#themes/theme_101/sb7.gif" height="11" width="7" alt=""></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_101/sb8.gif" height="11" width="12" alt=""></td>',
'</tr>',
'</tbody></table>'))
,p_page_plug_template_name=>'Navigation Region'
,p_internal_name=>'NAVIGATION_REGION'
,p_theme_id=>104
,p_theme_class_id=>5
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554865379804895125)
,p_plug_template_id=>wwv_flow_imp.id(270523665656046668)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>true
,p_has_button_support=>true
,p_glv_new_row=>true
);
wwv_flow_imp_shared.create_plug_tmpl_display_point(
 p_id=>wwv_flow_imp.id(554865535941895125)
,p_plug_template_id=>wwv_flow_imp.id(270523665656046668)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_has_region_support=>true
,p_has_item_support=>false
,p_has_button_support=>false
,p_glv_new_row=>true
);
wwv_flow_imp.component_end;
end;
/
