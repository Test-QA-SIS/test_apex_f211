prompt --application/shared_components/user_interface/templates/label/required_label_with_help
begin
--   Manifest
--     LABEL TEMPLATE: REQUIRED_LABEL_WITH_HELP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_field_template(
 p_id=>wwv_flow_imp.id(270535158703046676)
,p_template_name=>'Required Label with Help'
,p_internal_name=>'REQUIRED_LABEL_WITH_HELP'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#"><img src="#IMAGE_PREFIX#themes/theme_101/required.png" alt="" style="margin-right:5px;" /><a class="t5RequiredLabelwithHelp" href="javascript:popupFieldHelp(''#CURRENT_ITEM_ID#'',''&SESSION.'')" tabindex="'
||'999">'
,p_template_body2=>'</a></label>'
,p_on_error_before_label=>'<div class="t10InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>104
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
wwv_flow_imp.component_end;
end;
/
