prompt --application/shared_components/user_interface/templates/list/horizontal_images_with_label_list
begin
--   Manifest
--     REGION TEMPLATE: HORIZONTAL_IMAGES_WITH_LABEL_LIST
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270530855788046673)
,p_list_template_current=>'<td class="t10current"><img src="#IMAGE_PREFIX##IMAGE#" border="0" #IMAGE_ATTR#/><br />#TEXT#</td>'
,p_list_template_noncurrent=>'<td><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" border="0" #IMAGE_ATTR#/></a><br /><a href="#LINK#">#TEXT#</a></td>'
,p_list_template_name=>'Horizontal Images with Label List'
,p_internal_name=>'HORIZONTAL_IMAGES_WITH_LABEL_LIST'
,p_theme_id=>104
,p_theme_class_id=>4
,p_list_template_before_rows=>'<table class="t10HorizontalImageswithLabelList"><tr>'
,p_list_template_after_rows=>'</tr></table>'
);
wwv_flow_imp.component_end;
end;
/
