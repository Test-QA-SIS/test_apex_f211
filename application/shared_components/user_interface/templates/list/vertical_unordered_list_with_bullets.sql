prompt --application/shared_components/user_interface/templates/list/vertical_unordered_list_with_bullets
begin
--   Manifest
--     REGION TEMPLATE: VERTICAL_UNORDERED_LIST_WITH_BULLETS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270534163638046675)
,p_list_template_current=>'<li class="t10current">#TEXT#</li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered List with Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LIST_WITH_BULLETS'
,p_theme_id=>104
,p_theme_class_id=>1
,p_list_template_before_rows=>'<ul class="t10VerticalUnorderedListwithBullets">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_imp.component_end;
end;
/
