prompt --application/shared_components/user_interface/templates/list/horizontal_links_list
begin
--   Manifest
--     REGION TEMPLATE: HORIZONTAL_LINKS_LIST
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270531167154046673)
,p_list_template_current=>'<span class="t10current">#TEXT#</span>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Horizontal Links List'
,p_internal_name=>'HORIZONTAL_LINKS_LIST'
,p_theme_id=>104
,p_theme_class_id=>3
,p_list_template_before_rows=>'<div class="t10HorizontalLinksList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_imp.component_end;
end;
/
