prompt --application/shared_components/user_interface/templates/list/vertical_sidebar_list
begin
--   Manifest
--     REGION TEMPLATE: VERTICAL_SIDEBAR_LIST
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270533580760046674)
,p_list_template_current=>'<tr><td class="t10current">#TEXT#</td></tr>'
,p_list_template_noncurrent=>'<tr><td><a href="#LINK#" class="t10nav">#TEXT#</a></td></tr>'
,p_list_template_name=>'Vertical Sidebar List'
,p_internal_name=>'VERTICAL_SIDEBAR_LIST'
,p_theme_id=>104
,p_theme_class_id=>19
,p_list_template_before_rows=>'<table border="0" cellpadding="0" cellspacing="0" summary="" class="t10VerticalSidebarList">'
,p_list_template_after_rows=>'</table>'
);
wwv_flow_imp.component_end;
end;
/
