prompt --application/shared_components/user_interface/templates/list/wizard_progress_list
begin
--   Manifest
--     REGION TEMPLATE: WIZARD_PROGRESS_LIST
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270534473454046675)
,p_list_template_current=>'<tr><td><div class="t10current">#TEXT#</div><img src="#IMAGE_PREFIX#themes/theme_4/arrow_down.gif" width="7" height="6" alt="Down" /></td></tr>'
,p_list_template_noncurrent=>'<tr><td><div>#TEXT#</div><img src="#IMAGE_PREFIX#themes/theme_4/arrow_down.gif" width="7" height="6" alt="Down" /></td></tr>'
,p_list_template_name=>'Wizard Progress List'
,p_internal_name=>'WIZARD_PROGRESS_LIST'
,p_theme_id=>104
,p_theme_class_id=>17
,p_list_template_before_rows=>'<table border="0" cellpadding="0" cellspacing="0" summary="" class="t10WizardProgressList">'
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr><td>&DONE.</td></tr>',
'</table>'))
);
wwv_flow_imp.component_end;
end;
/
