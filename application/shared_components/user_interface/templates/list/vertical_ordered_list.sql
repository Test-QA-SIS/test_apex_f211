prompt --application/shared_components/user_interface/templates/list/vertical_ordered_list
begin
--   Manifest
--     REGION TEMPLATE: VERTICAL_ORDERED_LIST
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270533283149046674)
,p_list_template_current=>'<li class="t10current">#TEXT#</li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Ordered List'
,p_internal_name=>'VERTICAL_ORDERED_LIST'
,p_theme_id=>104
,p_theme_class_id=>2
,p_list_template_before_rows=>'<ol class="t10VerticalOrderedList">'
,p_list_template_after_rows=>'</ol>'
);
wwv_flow_imp.component_end;
end;
/
