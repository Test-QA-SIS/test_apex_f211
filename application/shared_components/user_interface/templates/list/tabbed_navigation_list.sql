prompt --application/shared_components/user_interface/templates/list/tabbed_navigation_list
begin
--   Manifest
--     REGION TEMPLATE: TABBED_NAVIGATION_LIST
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_template(
 p_id=>wwv_flow_imp.id(270532076567046674)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li><a href="#TAB_LINK#" class="t10current">#TEXT#</a></li>',
''))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li><a href="#LINK#">#TEXT#</a></li>',
''))
,p_list_template_name=>'Tabbed Navigation List'
,p_internal_name=>'TABBED_NAVIGATION_LIST'
,p_theme_id=>104
,p_theme_class_id=>7
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t10TabbedNavigationList">',
''))
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_imp.component_end;
end;
/
