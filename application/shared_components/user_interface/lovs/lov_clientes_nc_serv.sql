prompt --application/shared_components/user_interface/lovs/lov_clientes_nc_serv
begin
--   Manifest
--     LOV_CLIENTES_NC_SERV
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28076058854450035349)
,p_lov_name=>'LOV_CLIENTES_NC_SERV'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.per_nro_identificacion || '' - '' ||',
'       nvl(b.per_razon_social,',
'           b.per_primer_apellido || '' '' || b.per_segundo_apellido || '' '' ||',
'           b.per_primer_nombre || '' '' || b.per_segundo_nombre) Cliente,',
'       a.cli_id Cli_id',
'  FROM asdm_clientes a, asdm_personas b',
' WHERE b.per_id = a.per_id',
'   AND b.emp_id = a.emp_id',
'   and a.emp_id = :f_emp_id',
'',
'/*DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_CLIENTES'');',
' lv_lov := lv_lov ||'' WHERE emp_id = ''|| :F_EMP_ID;',
' RETURN(lv_lov);',
'END;*/'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
