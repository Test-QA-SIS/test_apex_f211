prompt --application/shared_components/user_interface/lovs/lov_tipos_descuentos
begin
--   Manifest
--     LOV_TIPOS_DESCUENTOS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(66500744850089048925)
,p_lov_name=>'LOV_TIPOS_DESCUENTOS'
,p_lov_query=>'.'||wwv_flow_imp.id(66500744850089048925)||'.'
,p_location=>'STATIC'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(66500745180947048931)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Valor'
,p_lov_return_value=>'0'
);
wwv_flow_imp.component_end;
end;
/
