prompt --application/shared_components/user_interface/lovs/lov_entidades_destino_forma_pago
begin
--   Manifest
--     LOV_ENTIDADES_DESTINO_FORMA_PAGO
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(46916961875124374)
,p_lov_name=>'LOV_ENTIDADES_DESTINO_FORMA_PAGO'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'lv_lov := pq_ven_listas_caja.fn_lov_entidad_destin_tfp_pag',
'(',
':F_EMP_ID,',
':P30_TFP_ID,',
':P30_ERROR);',
'return (lv_lov);',
'END;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
