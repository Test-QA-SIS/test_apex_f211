prompt --application/shared_components/user_interface/lovs/lov_mnc_id_111
begin
--   Manifest
--     LOV_MNC_ID_111
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(136988695079284547855)
,p_lov_name=>'LOV_MNC_ID_111'
,p_lov_query=>'SELECT  MO.MNC_DESCRIPCION, MO.MNC_ID FROM ASDM_MOTIVOS_NC MO, VEN_PARAMETRIZACION_COMP PC WHERE MO.MNC_ID=PC.MNC_ID AND PC.PCO_ID = (SELECT PCO_ID FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P111_CIN_ID)'
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
