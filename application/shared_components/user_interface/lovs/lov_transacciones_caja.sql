prompt --application/shared_components/user_interface/lovs/lov_transacciones_caja
begin
--   Manifest
--     LOV_TRANSACCIONES_CAJA
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(79140678921428593)
,p_lov_name=>'LOV_TRANSACCIONES_CAJA'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.ttr_descripcion d, b.ttr_id r',
'FROM asdm_tipos_transaccion_empresa a, asdm_tipos_transacciones b',
'WHERE a.ttr_id = b.ttr_id',
'AND a.tgr_id = pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja_otros'')',
'and a.emp_id = :f_emp_id',
'and a.tte_estado_registro=0',
'and b.ttr_estado_registro=0',
'union',
'SELECT b.ttr_descripcion d, b.ttr_id r',
'FROM asdm_tipos_transaccion_empresa a, asdm_tipos_transacciones b',
'WHERE a.ttr_id = b.ttr_id',
'and a.emp_id = :f_emp_id',
'AND a.tgr_id = pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja_matriz'')',
'AND :f_uge_id = :P56_MATRIZ',
'and a.tte_estado_registro=0',
'and b.ttr_estado_registro=0',
'ORDER BY R'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
