prompt --application/shared_components/user_interface/lovs/lov_cartera_cliente
begin
--   Manifest
--     LOV_CARTERA_CLIENTE
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(38261179794538051)
,p_lov_name=>'LOV_CARTERA_CLIENTE'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT vcar.cxc_id||''Saldo: ''|| vcar.saldo_cxc d,',
'vcar.cxc_id r',
'  FROM v_car_cartera_clientes vcar',
' WHERE vcar.cli_id = :p49_cli_id',
'   AND vcar.saldo_cxc > 0',
'   AND vcar.saldo_cuota > 0   ',
' GROUP BY vcar.cxc_id, vcar.saldo_cxc'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
