prompt --application/shared_components/user_interface/lovs/lov_agentes
begin
--   Manifest
--     LOV_AGENTES
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(255034991638006743)
,p_lov_name=>'LOV_AGENTES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select per.per_primer_apellido||'' ''||per.per_segundo_apellido||'' ''|| per.per_primer_nombre||'' ''||per.per_segundo_nombre d, age.age_id r',
'from   asdm_personas per, asdm_agentes age',
'where age.per_id= per.per_id',
'order by 1'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
