prompt --application/shared_components/user_interface/lovs/lov_plan_tc_oventa_rap
begin
--   Manifest
--     LOV_PLAN_TC_OVENTA_RAP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584030680723948453)
,p_lov_name=>'LOV_PLAN_TC_OVENTA_RAP'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'lv_sentencia varchar2(3000);',
'BEGIN',
'lv_sentencia:=pq_ven_listas.fn_lov_tarjeta_credito_plan(:F_EMP_ID,nvl(:P141_TIPO,0));',
'return(lv_sentencia);',
'END;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
