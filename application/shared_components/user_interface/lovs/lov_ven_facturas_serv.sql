prompt --application/shared_components/user_interface/lovs/lov_ven_facturas_serv
begin
--   Manifest
--     LOV_VEN_FACTURAS_SERV
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28076059067593044380)
,p_lov_name=>'LOV_VEN_FACTURAS_SERV'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select com.com_tipo || ''/'' || com.com_id || ''- folio: '' || com.uge_num_sri || ''-'' ||',
'       com.pue_num_sri || ''-'' || com.com_numero || '' - Fecha: '' ||',
'       com.com_fecha || '' - Com_id: '' || com.com_id d, com.com_id r',
'  from ven_comprobantes com, ven_comprobantes_det det',
' where det.emp_id =:f_emp_id',
'   and com.com_id = det.com_id',
'   and com.com_tipo = ''F''',
'   and com.cli_id = :P120_CLI_ID',
'   and exists',
' (SELECT t.ite_sku_id',
'          FROM inv_categorias c, inv_items_categorias t',
'         WHERE c.emp_id =:f_emp_id',
'           and c.cat_id = t.cat_id',
'           and t.ite_sku_id = det.ite_sku_id',
'           and t.ica_estado_registro = 0',
'           AND c.cat_estado_registro = 0 ',
'        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'         START WITH c.cat_id in',
'                    (pq_constantes.fn_retorna_constante(c.emp_id,',
'                                                        ''cn_cat_id_gex_induccion''),',
'                     pq_constantes.fn_retorna_constante(c.emp_id,',
'                                                        ''cn_cat_id_svs'')',
'                     )',
' )',
'',
'',
'and 0 =( select count(p.com_id_ref)',
'                        from ven_comprobantes p',
'                       where p.com_id_ref = com.com_id)',
''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
