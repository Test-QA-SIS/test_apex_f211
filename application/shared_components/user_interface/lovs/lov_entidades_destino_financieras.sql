prompt --application/shared_components/user_interface/lovs/lov_entidades_destino_financieras
begin
--   Manifest
--     LOV_ENTIDADES_DESTINO_FINANCIERAS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(98288478622409000)
,p_lov_name=>'LOV_ENTIDADES_DESTINO_FINANCIERAS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ede.ede_descripcion,',
'       ede.ede_id',
'FROM   asdm_entidades_destinos ede',
'WHERE  ede.ede_tipo = ''EFI''',
'and ede.emp_id = :f_emp_id',
'and ede.ede_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'union',
'select ''-- Seleccione Entidad --'' d, null r from dual',
'order by 1 '))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
