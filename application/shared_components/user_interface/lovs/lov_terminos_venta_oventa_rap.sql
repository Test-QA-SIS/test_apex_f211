prompt --application/shared_components/user_interface/lovs/lov_terminos_venta_oventa_rap
begin
--   Manifest
--     LOV_TERMINOS_VENTA_OVENTA_RAP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584039510467976371)
,p_lov_name=>'LOV_TERMINOS_VENTA_OVENTA_RAP'
,p_lov_query=>'SELECT L.TVE_DESCRIPCION D, L.TVE_ID R FROM VEN_TERMINOS_VENTA L WHERE L.TVE_ID IN (:F_TVE_CONTADO,:F_TVE_TC,2) and L.EMP_ID = :F_EMP_ID AND L.TVE_ESTADO_REGISTRO = 0'
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
