prompt --application/shared_components/user_interface/lovs/lov_estado_oventa_rap
begin
--   Manifest
--     LOV_ESTADO_OVENTA_RAP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584013416624919193)
,p_lov_name=>'LOV_ESTADO_OVENTA_RAP'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov :=  ''select ''''Sin estado'''', -1 from dual'';',
'  IF :P141_ORD_TIPO IS NOT NULL THEN',
'  lv_lov := pq_ven_listas.fn_lov_estados_calidad(:f_emp_id,:P141_ORD_TIPO,:f_seg_id,:P0_ERROR); ',
'  END IF;',
'  RETURN(lv_lov);',
'END;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
