prompt --application/shared_components/user_interface/lovs/lov_ppr_politicas_gener
begin
--   Manifest
--     LOV_PPR_POLITICAS_GENER
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(20098978156471246718)
,p_lov_name=>'LOV_PPR_POLITICAS_GENER'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(1000);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_PPR_POLTICAS_GENERALES'');',
'return (lv_lov);',
'END;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
