prompt --application/shared_components/user_interface/lovs/lov_tipo_direccion
begin
--   Manifest
--     LOV_TIPO_DIRECCION
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(269686189248084722)
,p_lov_name=>'LOV_TIPO_DIRECCION'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
unistr('lv_lov:=''select ''''--Seleccione Tipo Direcci\00BF\00BFn--'''' d, null r from dual'),
'union  '';',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_DIRECCION'');',
'return (lv_lov);',
'END;',
''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
