prompt --application/shared_components/user_interface/lovs/lov_clientes_calif_aa_a_b
begin
--   Manifest
--     LOV_CLIENTES_CALIF_AA_A_B
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(137111410829001922721)
,p_lov_name=>'LOV_CLIENTES_CALIF_AA_A_B'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (PE.PER_RAZON_SOCIAL || PE.PER_PRIMER_NOMBRE || '' '' ||',
'          PE.PER_SEGUNDO_NOMBRE || '' '' || PE.PER_PRIMER_APELLIDO || '' '' ||',
'          PE.PER_SEGUNDO_APELLIDO) || '' - Cod: '' || CL.CLI_ID || '' - Id: '' ||',
'          PE.PER_NRO_IDENTIFICACION NOMBRE,',
'          PE.PER_NRO_IDENTIFICACION IDENTIFICACION',
'     FROM ASDM_CLIENTES CL, ASDM_PERSONAS PE, CAR_CALIF_INI_CLIENTES C',
'    WHERE CL.PER_ID = PE.PER_ID',
'      AND CL.EMP_ID = 1',
'      AND CL.CLI_ID=C.CLI_ID',
'      AND C.CALIFICACION IN (''AA'',''A'',''B'')',
'      AND CL.CLI_ESTADO_REGISTRO = 0',
'      AND PE.PER_ESTADO_REGISTRO = 0'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
