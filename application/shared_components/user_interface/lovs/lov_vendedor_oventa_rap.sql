prompt --application/shared_components/user_interface/lovs/lov_vendedor_oventa_rap
begin
--   Manifest
--     LOV_VENDEDOR_OVENTA_RAP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584041482344991410)
,p_lov_name=>'LOV_VENDEDOR_OVENTA_RAP'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT g.age_id || ''- '' || u.nombre d, g.age_id r',
'  FROM asdm_agentes          g,',
'       kseg_usuarios         u,',
'       asdm_tipos_agentes    tag,',
'       asdm_agentes_tagentes a',
' WHERE g.usu_id = u.usuario_id',
'   AND tag.tag_id = a.tag_id -- H1 mzeas 14/02/2019',
'   AND tag.tag_vende = ''S''',
'	 AND a.ata_comisiona =''S''',
'   AND a.uge_id =  :f_uge_id',
'   AND g.age_id = a.age_id	',
'   AND a.ata_estado_registro = 0',
'   AND tag.tag_estado_registro = 0',
'   AND trunc(SYSDATE) BETWEEN nvl(a.ata_fecha_ini, trunc(SYSDATE)) AND',
'       nvl(a.ata_fecha_fin, trunc(SYSDATE))',
'   AND g.age_estado_registro = 0'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
