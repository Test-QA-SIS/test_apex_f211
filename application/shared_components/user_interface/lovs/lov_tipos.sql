prompt --application/shared_components/user_interface/lovs/lov_tipos
begin
--   Manifest
--     LOV_TIPOS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(76673757103664354)
,p_lov_name=>'LOV_TIPOS'
,p_lov_query=>'.'||wwv_flow_imp.id(76673757103664354)||'.'
,p_location=>'STATIC'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(76674061236664396)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Factura'
,p_lov_return_value=>'F'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(76674270049664518)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>unistr('Nota de cr\00E9dito')
,p_lov_return_value=>'NC'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(76674476181664518)
,p_lov_disp_sequence=>3
,p_lov_disp_value=>'Nota de debito'
,p_lov_return_value=>'ND'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(1555772682798223693)
,p_lov_disp_sequence=>4
,p_lov_disp_value=>'Nota de Debito Gasto Cobranza'
,p_lov_return_value=>'NDG'
);
wwv_flow_imp.component_end;
end;
/
