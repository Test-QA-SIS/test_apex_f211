prompt --application/shared_components/user_interface/lovs/lov_asdm_documentos_5
begin
--   Manifest
--     LOV_ASDM_DOCUMENTOS_5
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(15461580578649570902)
,p_lov_name=>'LOV_ASDM_DOCUMENTOS_5'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   lv_lov varchar2(500);',
'BEGIN',
'   lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_ASDM_DOCUMENTOS'');',
'   lv_lov := lv_lov ||'' WHERE ttr_id = ''||NVL(NVL(:P30_TTR_ID_PADRE,:p0_ttr_id),''ttr_id'');',
'   lv_lov := lv_lov ||'' AND uge_id = ''||NVL(:F_UGE_ID,''UGE_ID'');',
'   lv_lov := lv_lov ||'' AND SEGMENTO = ''||NVL(:F_SEG_ID,''SEGMENTO'');',
'--   lv_lov := lv_lov ||'' AND (TIPO_ORDEN  is not null''||'' AND ''||NVL(:P30_MOTOS_YN,CHR(39)||''N''||CHR(39))||'' = ''||CHR(39)||''S''||CHR(39)||'')'';',
'--   lv_lov := lv_lov ||'' AND (TIPO_ORDEN  = ''||CHR(39)||''VENMO''||CHR(39)||'' AND ''||NVL(:P30_MOTOS_YN,CHR(39)||''N''||CHR(39))||'' = ''||CHR(39)||''S''||CHR(39)||'')'';',
'   lv_lov := lv_lov ||'' AND Documento NOT IN (SELECT    doc_id_padre',
'                                              FROM      asdm_relaciones_documentos)'';',
'',
'   pruebas_iv(444888,lv_lov);',
'   RETURN (lv_lov);',
'END;',
''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
