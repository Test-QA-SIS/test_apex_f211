prompt --application/shared_components/user_interface/lovs/lov_meses_gracia
begin
--   Manifest
--     LOV_MESES_GRACIA
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(136988675925682524624)
,p_lov_name=>'LOV_MESES_GRACIA'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) ||'' MESES'' D, TO_NUMBER(PEN_VALOR) R',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 0 AND',
'       (SELECT PMG.PMG_MESES_GRACIA D',
'          FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_PARAMETROS_MESES_GRACIA PMG',
'         WHERE CXC.CXC_ID = :P102_CXC_ID',
'           AND CXC.UGE_ID = PMG.UGE_ID',
'           AND PMG.PMG_ESTADO_REGISTRO=0)',
' ORDER BY 1'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
