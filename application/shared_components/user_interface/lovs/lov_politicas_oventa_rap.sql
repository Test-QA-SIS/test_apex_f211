prompt --application/shared_components/user_interface/lovs/lov_politicas_oventa_rap
begin
--   Manifest
--     LOV_POLITICAS_OVENTA_RAP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584034854629957969)
,p_lov_name=>'LOV_POLITICAS_OVENTA_RAP'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.pol_id || '' - '' || a.pol_descripcion_larga D, a.pol_id R',
'  FROM ppr_politicas                a,',
'       ppr_politicas_terminos_venta b,',
'       ppr_politicas_agencia        c',
' WHERE b.pol_id = a.pol_id',
'   AND c.pol_id = a.pol_id',
'   AND a.emp_id = :f_emp_id',
'   AND a.pol_estado_registro = ''0''',
'   AND b.ptv_estado_registro = ''0''',
'   AND a.tse_id = :f_seg_id',
'   AND b.tve_id = nvl(:P141_TVE_ID,0)',
'   AND c.age_id = :f_age_id_agencia',
'   AND a.pol_fecha_desde <= trunc(SYSDATE)',
'   AND a.pol_fecha_hasta >= trunc(SYSDATE)',
'   AND c.pag_fecha_desde <= trunc(SYSDATE)',
'   AND c.pag_fecha_hasta >= trunc(SYSDATE)',
'   AND NOT EXISTS',
' (SELECT NULL FROM ppr_politicas_clientes d WHERE d.pol_id = a.pol_id)',
'UNION ALL',
'SELECT a.pol_id || '' - '' || a.pol_descripcion_larga D, a.pol_id R',
'  FROM ppr_politicas                a,',
'       ppr_politicas_terminos_venta b,',
'       ppr_politicas_clientes       d',
' WHERE b.pol_id = a.pol_id',
'   AND d.pol_id = a.pol_id',
'   AND d.cli_id = NVL(:P141_CLI_ID,0)',
'   AND a.emp_id = :f_emp_id',
'   AND a.pol_estado_registro = :F_ESTADO_REG_ACTIVO',
'   AND d.pcl_estado_registro = :F_ESTADO_REG_ACTIVO',
'   AND d.emp_id = :f_emp_id',
'   AND a.tse_id = :f_seg_id',
'   AND b.tve_id = nvl(:P141_TVE_ID,0)',
'   AND a.pol_fecha_desde <= trunc(SYSDATE)',
'   AND a.pol_fecha_hasta >= trunc(SYSDATE)',
'   AND d.pcl_fecha_desde <= trunc(SYSDATE)',
'   AND d.pcl_fecha_hasta >= trunc(SYSDATE)',
' ORDER BY 2'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
