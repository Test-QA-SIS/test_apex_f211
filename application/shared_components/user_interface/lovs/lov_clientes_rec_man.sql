prompt --application/shared_components/user_interface/lovs/lov_clientes_rec_man
begin
--   Manifest
--     LOV_CLIENTES_REC_MAN
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(155453072353429183)
,p_lov_name=>'LOV_CLIENTES_REC_MAN'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.nombre_completo || '' Cod'' || i.cli_id || '' Ced_Ruc'' ||',
'       c.per_nro_identificacion d,',
'       i.cli_id r',
'  from inv_comprobantes_inventario  i,',
'       asdm_transacciones           t,',
'       v_ven_comprobantes           c,',
'       asdm_p.asdm_homologacion_com h',
' where i.trx_id = t.trx_id',
'   and t.ttr_id =',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_retiro_def'')',
'   and i.com_id = c.com_id',
'   and i.emp_id = c.emp_id',
'   and i.emp_id = t.emp_id',
'   AND c.com_id = h.com_id(+)',
'   and i.emp_id = :f_emp_id',
'   AND i.com_id NOT IN (select com_id_ref',
'                          from ven_comprobantes   nc,',
'                               asdm_transacciones tr',
'                         WHERE nc.trx_id = tr.trx_id',
'                           AND tr.ttr_id =',
'                               pq_constantes.fn_retorna_constante(NULL,',
'                                                                  ''cn_ttr_id_nota_credito_retiro'')',
'                           and nc.emp_id = nc.emp_id',
'                           AND nc.cin_id = i.cin_id)',
'',
' ORDER BY c.com_fecha DESC',
''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
