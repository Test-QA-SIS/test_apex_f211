prompt --application/shared_components/user_interface/lovs/lov_promos_combos
begin
--   Manifest
--     LOV_PROMOS_COMBOS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584037135570962276)
,p_lov_name=>'LOV_PROMOS_COMBOS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct l.rit_opcion || '' - '' || l.rit_observacion D,',
'                l.rit_opcion R',
'  from inv_relaciones_item l',
' where l.ite_sku_padre =:p141_ite_sku_id',
' and l.emp_id = :F_EMP_ID',
' and l.rit_estado_registro = :F_ESTADO_REG_ACTIVO',
'',
'and pq_ven_promos_combos.fn_retorna_combo_politica(pn_pol_id => :P141_POL_ID ,pn_tre_id =>l.tre_id, pn_ite_sku_padre => l.ite_sku_padre , pv_rit_opcion => l.rit_opcion) > 0',
' and l.ite_sku_padre in (select distinct r.ite_sku_padre',
'                       from inv_relaciones_item r',
'                      where r.ite_sku_hijo = :p141_ite_sku_id',
'                        and r.tre_id = pq_constantes.fn_retorna_constante(:F_emp_id,''cn_tre_id_promocion''))',
'                        and l.tse_id = :F_SEG_ID ',
' order by rit_opcion'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
