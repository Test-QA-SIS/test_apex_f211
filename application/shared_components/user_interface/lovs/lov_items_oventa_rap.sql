prompt --application/shared_components/user_interface/lovs/lov_items_oventa_rap
begin
--   Manifest
--     LOV_ITEMS_OVENTA_RAP
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(28584024893724931865)
,p_lov_name=>'LOV_ITEMS_OVENTA_RAP'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'lv_lov varchar2(32000);',
'',
'begin',
'',
'    pq_ven_listas2.pr_lov_items_ordenes  (pn_emp_id => :F_EMP_ID,',
'                                          pn_bpr_id => :P141_BODEGA,',
'                                          pn_cei_id => :P141_ESTADO,',
'                                          pn_age_id_agencia => :F_AGE_ID_AGENCIA,',
'                                          pv_ord_tipo => :P141_ORD_TIPO,',
'                                          pn_cli_id => :P141_CLI_ID,',
'                                          pn_age_id_agente => :P141_AGE_ID_AGENTE,',
'                                          pn_lcm_id => null,',
'                                          pn_pol_id => :P141_POL_ID,',
'                                          pn_tse_id => :f_seg_id,',
'                                        --  pv_tipo_venta => :P20_ORD_TIPO,',
'                                          pv_lov => lv_lov,',
'                                          PV_SESION => :APP_SESSION,  -- PAOBERNAL 31/OCTUBRE/2018',
'                                          pn_usu_id => :F_USER_ID,    -- PAOBERNAL 31/OCTUBRE/2018                                    ',
'                                          pv_error => :p0_error);',
'                                          ',
'  return (lv_lov);',
'  ',
'end;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
