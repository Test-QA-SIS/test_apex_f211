prompt --application/shared_components/user_interface/lovs/lov_motivos_nc_servicio
begin
--   Manifest
--     LOV_MOTIVOS_NC_SERVICIO
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(2140136365962435429)
,p_lov_name=>'LOV_MOTIVOS_NC_SERVICIO'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nc.mnc_id||''-''||nc.mnc_impresion d, nc.mnc_id r',
'    FROM asdm_motivos_nc nc,asdm_e.ven_motivos_roles mc',
'   WHERE nc.mnc_tipo =  ''NCSER''',
'     AND nc.emp_id = :f_emp_id',
'     AND nc.mnc_estado_registro = 0',
'     AND nc.mnc_id = mc.mnc_id',
'     AND mc.rol_id = :P0_ROL'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
