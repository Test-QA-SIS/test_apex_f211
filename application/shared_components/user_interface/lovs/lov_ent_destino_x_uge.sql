prompt --application/shared_components/user_interface/lovs/lov_ent_destino_x_uge
begin
--   Manifest
--     LOV_ENT_DESTINO_X_UGE
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(50873577150558055)
,p_lov_name=>'LOV_ENT_DESTINO_X_UGE'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
