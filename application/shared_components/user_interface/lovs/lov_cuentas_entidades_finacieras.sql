prompt --application/shared_components/user_interface/lovs/lov_cuentas_entidades_finacieras
begin
--   Manifest
--     LOV_CUENTAS_ENTIDADES_FINACIERAS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(98288769604409003)
,p_lov_name=>'LOV_CUENTAS_ENTIDADES_FINACIERAS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT cba.cba_nro_cta_corriente d, cba.cba_id r',
'        FROM   asdm_cuentas_bancarias cba',
'        where cba.ede_id = :P6_EDE_ID',
'        ',
''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
