prompt --application/shared_components/user_interface/lovs/lov_terminos_ven_fac
begin
--   Manifest
--     LOV_TERMINOS_VEN_FAC
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(7788006151971853220)
,p_lov_name=>'LOV_TERMINOS_VEN_FAC'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select TVE_DESCRIPCION d, TVE_ID  r from ASDM_E.VEN_TERMINOS_VENTA l',
'where l.tve_estado_registro = 0',
'and l.emp_id = :F_EMP_ID'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
