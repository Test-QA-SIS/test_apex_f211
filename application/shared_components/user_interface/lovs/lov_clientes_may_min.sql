prompt --application/shared_components/user_interface/lovs/lov_clientes_may_min
begin
--   Manifest
--     LOV_CLIENTES_MAY_MIN
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(62247665464963259)
,p_lov_name=>'LOV_CLIENTES_MAY_MIN'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'return pq_asdm_clientes.fn_lov_clientes(:f_emp_id,:F244_SEGMENTO);',
'END;',
''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
