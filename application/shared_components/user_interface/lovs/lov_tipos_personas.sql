prompt --application/shared_components/user_interface/lovs/lov_tipos_personas
begin
--   Manifest
--     LOV_TIPOS_PERSONAS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(30383992406245595975)
,p_lov_name=>'LOV_TIPOS_PERSONAS'
,p_lov_query=>'.'||wwv_flow_imp.id(30383992406245595975)||'.'
,p_location=>'STATIC'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(30383993196040595983)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Natural'
,p_lov_return_value=>'N'
);
wwv_flow_imp_shared.create_static_lov_data(
 p_id=>wwv_flow_imp.id(30383993597930595984)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Juridica'
,p_lov_return_value=>'J'
);
wwv_flow_imp.component_end;
end;
/
