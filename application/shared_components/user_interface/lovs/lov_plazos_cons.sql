prompt --application/shared_components/user_interface/lovs/lov_plazos_cons
begin
--   Manifest
--     LOV_PLAZOS_CONS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(248020274975492783)
,p_lov_name=>'LOV_PLAZOS_CONS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_retorna_plazos(:F_EMP_ID,:P16_POL_ID,:P16_TVE_ID, :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
