prompt --application/shared_components/user_interface/lovs/lov_opciones
begin
--   Manifest
--     LOV_OPCIONES
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(122915976756055696)
,p_lov_name=>'LOV_OPCIONES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.descripcion, a.opcion_id',
'  FROM kseg_e.kseg_opciones a, kseg_e.kseg_opciones_rol b, kdda_e.kdda_objetos c',
' WHERE a.opcion_id = b.opcion_id',
'   AND b.rol_id = :p0_rol   ',
'   AND a.estado = 0',
'   AND b.estado = 0',
'   AND a.objeto_id = c.objeto_id',
'   AND c.tipo_objeto_id = 4',
'   AND c.prefijo IS NOT NULL'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
