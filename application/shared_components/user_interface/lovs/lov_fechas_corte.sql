prompt --application/shared_components/user_interface/lovs/lov_fechas_corte
begin
--   Manifest
--     LOV_FECHAS_CORTE
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(63370060430350200)
,p_lov_name=>'LOV_FECHAS_CORTE'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c001 DESCRIPCION,',
'       a.c002 VALOR',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_FECHAS_CORTE'''))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
