prompt --application/shared_components/user_interface/lovs/lv_ven_comprobantes_f_nd_ret
begin
--   Manifest
--     LV_VEN_COMPROBANTES_F_ND_RET
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(112744980021007574)
,p_lov_name=>'LV_VEN_COMPROBANTES_F_ND_RET'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    lv_lov := pq_asdm_listas.fn_lov_facturas_retencion(:f_emp_id,:P64_CLI_ID);',
'',
'',
'/*kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_COMPROBANTES_F_ND_SIN_RET'');',
'lv_lov := lv_lov || ''AND vco.cli_id = :P64_CLI_ID ORDER BY vco.com_id'';*/',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
