prompt --application/shared_components/user_interface/lovs/lov_car_ugestion_todas
begin
--   Manifest
--     LOV_CAR_UGESTION_TODAS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list_of_values(
 p_id=>wwv_flow_imp.id(20098926263156243762)
,p_lov_name=>'LOV_CAR_UGESTION_TODAS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT u.uge_nombre d, u.uge_id v',
'FROM ASDM_UNIDADES_GESTION U ',
'WHERE U.EMP_ID = :f_emp_id',
'AND U.UGE_ESTADO_REGISTRO = PQ_CONSTANTES.fn_retorna_constante(0, ''cv_estado_reg_activo'')'))
,p_source_type=>'LEGACY_SQL'
,p_location=>'LOCAL'
);
wwv_flow_imp.component_end;
end;
/
