prompt --application/shared_components/logic/build_options
begin
--   Manifest
--     BUILD OPTIONS: 211
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
null;
wwv_flow_imp.component_end;
end;
/
