prompt --application/shared_components/logic/application_items/fsp_after_login_url
begin
--   Manifest
--     APPLICATION ITEM: FSP_AFTER_LOGIN_URL
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_item(
 p_id=>wwv_flow_imp.id(332301754211686787)
,p_name=>'FSP_AFTER_LOGIN_URL'
,p_scope=>'GLOBAL'
,p_protection_level=>'N'
,p_escape_on_http_output=>'N'
,p_version_scn=>6188282789413
);
wwv_flow_imp.component_end;
end;
/
