prompt --application/shared_components/logic/application_items/f_tve_tc
begin
--   Manifest
--     APPLICATION ITEM: F_TVE_TC
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_item(
 p_id=>wwv_flow_imp.id(28583966252488862836)
,p_name=>'F_TVE_TC'
,p_scope=>'GLOBAL'
,p_protection_level=>'S'
,p_escape_on_http_output=>'N'
,p_version_scn=>6188444582600
);
wwv_flow_imp.component_end;
end;
/
