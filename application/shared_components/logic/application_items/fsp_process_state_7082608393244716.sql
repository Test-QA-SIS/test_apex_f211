prompt --application/shared_components/logic/application_items/fsp_process_state_7082608393244716
begin
--   Manifest
--     APPLICATION ITEM: FSP_PROCESS_STATE_7082608393244716
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_item(
 p_id=>wwv_flow_imp.id(74043082140355206)
,p_name=>'FSP_PROCESS_STATE_7082608393244716'
,p_scope=>'GLOBAL'
,p_protection_level=>'N'
,p_escape_on_http_output=>'N'
,p_version_scn=>6188282839023
);
wwv_flow_imp.component_end;
end;
/
