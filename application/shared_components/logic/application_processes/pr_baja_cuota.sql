prompt --application/shared_components/logic/application_processes/pr_baja_cuota
begin
--   Manifest
--     APPLICATION PROCESS: PR_BAJA_CUOTA
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(59138638739718119957)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BAJA_CUOTA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'v_error varchar2(2500);',
'begin',
'',
'pq_car_pago_cuota.pr_baja_cuotas(pn_emp_id => :f_emp_id,',
'                                  pn_div_id => :P1,',
'                                  pn_usu_id => :f_user_id,',
'                                  pv_error  => v_error);             ',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_security_scheme=>'MUST_NOT_BE_PUBLIC_USER'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
