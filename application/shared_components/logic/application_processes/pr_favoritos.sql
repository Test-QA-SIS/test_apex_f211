prompt --application/shared_components/logic/application_processes/pr_favoritos
begin
--   Manifest
--     APPLICATION PROCESS: PR_FAVORITOS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(123195534968125993)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_FAVORITOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'kseg_p.pq_kseg_seguridad.pr_ins_del_favoritos(pn_usu_id => :f_user_id,',
'                                              pn_emp_id => :f_emp_id);'))
,p_process_clob_language=>'PLSQL'
,p_security_scheme=>'MUST_NOT_BE_PUBLIC_USER'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
