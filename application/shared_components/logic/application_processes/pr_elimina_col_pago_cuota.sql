prompt --application/shared_components/logic/application_processes/pr_elimina_col_pago_cuota
begin
--   Manifest
--     APPLICATION PROCESS: PR_ELIMINA_COL_PAGO_CUOTA
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(101260882030060355)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINA_COL_PAGO_CUOTA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'pq_ven_pagos_cuota.pr_elimina_colec_pago_cuota(pn_seq_id => :P5,',
'                                               pn_cli_id => :P4,',
'                                               pn_div_nro_vencimiento => :P3,',
'                                               pn_emp_id  => :P1,',
'                                               pn_cxc_id  => :P2,',
'                                               pn_div_id  => :P6,',
'                                               pn_tfp_id  => :P8,',
'                                               pv_error   => :P9);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
