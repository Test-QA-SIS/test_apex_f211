prompt --application/shared_components/logic/application_processes/pr_controla_caja
begin
--   Manifest
--     APPLICATION PROCESS: pr_controla_caja
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(96353982402139879)
,p_process_sequence=>5
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_controla_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_ven_movimientos_caja.pr_controla_caja(:F_USER_ID,',
'                                      :f_uge_id, ',
'                                      :f_emp_id,',
'                                      :p0_pue_id,',
'                                      :F_PCA_ID,',
'                                      :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error en parametros de caja'
,p_process_when=>'59,60,3,40,42,19,51,41,53,52,12,31,93,96,100,112,999,119,123,127,122,126,137,149,141,145'
,p_process_when_type=>'CURRENT_PAGE_NOT_IN_CONDITION'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
