prompt --application/shared_components/logic/application_processes/pr_kill_process_exit
begin
--   Manifest
--     APPLICATION PROCESS: PR_KILL_PROCESS_EXIT
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(6647965677875476172)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_KILL_PROCESS_EXIT'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'   :P0_ERROR:=asdm_p.pq_kseg_seguridad.fn_kill_session_cancel_event;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error en procesamiento'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
