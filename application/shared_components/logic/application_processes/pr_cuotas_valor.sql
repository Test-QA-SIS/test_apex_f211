prompt --application/shared_components/logic/application_processes/pr_cuotas_valor
begin
--   Manifest
--     APPLICATION PROCESS: PR_CUOTAS_VALOR
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(99123877486658950)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CUOTAS_VALOR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'pq_ven_pagos_cuota.pr_carga_colec_div_pagar(pn_seq_id   => :P3,',
'                                            pv_error    => :P6,',
'                                            pn_emp_id   => :P1,',
'                                            pn_ttr_id_origen => 115,--pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_pago_cuota''),',
'                                            pn_tse_id   => :P4,',
'                                            pn_tfp_id => :P2,',
'                                            pn_total_max => :P5); -- yguaman 2012/01/05. Agregado para usarse en reverso de pago de cuota',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
