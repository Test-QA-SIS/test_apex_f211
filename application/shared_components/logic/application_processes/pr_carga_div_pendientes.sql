prompt --application/shared_components/logic/application_processes/pr_carga_div_pendientes
begin
--   Manifest
--     APPLICATION PROCESS: PR_CARGA_DIV_PENDIENTES
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(103905974729406242)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_DIV_PENDIENTES'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(',
'pn_cli_id  => :P1,',
'pn_emp_id  => :P2,',
'pn_seg_id => :P3,',
'pd_fecha_desde  => :P4,',
'pd_fecha_hasta =>:P5,                                   ',
'pv_error  => :P6);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
