prompt --application/shared_components/logic/application_processes/pr_actualiza_cuota
begin
--   Manifest
--     APPLICATION PROCESS: PR_ACTUALIZA_CUOTA
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(100654582697649594)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACTUALIZA_CUOTA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota.pr_actualiza_colec_div_pagar(',
' pn_emp_id => :P1,',
' pn_div_id => :P2,  ',
' pn_total  => :P3,',
' pn_im_cond => :P4,',
' pn_gc_cond => :P5,',
' pn_tse_id => :P7,',
' pn_act_int_cond => :P6, --- Yguaman 2011/11/08 17:28pm ',
' pn_tfp_id => :P8,  -- Yguaman 2011/12/05',
' pn_total_max => :P9, -- Yguaman 2012/01/05',
' pv_error  => :P10);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
