prompt --application/shared_components/logic/application_processes/pr_carga_constantes
begin
--   Manifest
--     APPLICATION PROCESS: pr_carga_constantes
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(24408920664031336521)
,p_process_sequence=>1
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_constantes'
,p_process_sql_clob=>':cn_tfp_id_orden_pago := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_orden_pago'');'
,p_process_clob_language=>'PLSQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
