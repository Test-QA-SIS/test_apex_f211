prompt --application/shared_components/logic/application_processes/pr_delete_item
begin
--   Manifest
--     APPLICATION PROCESS: PR_DELETE_ITEM
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(120917534377565909980)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_DELETE_ITEM'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_ven_punto_venta.pr_ven_delete_item;',
'  pq_ven_punto_venta.pr_ven_recalcula_detalle(pn_emp_id  => :f_emp_id,',
'                                               pn_tve_id  => :p200_tve_id,',
'                                               pn_pol_id  => :p200_pol_id,',
'                                               pn_bpr_id  => :p200_bpr_id);'))
,p_process_clob_language=>'PLSQL'
,p_security_scheme=>'MUST_NOT_BE_PUBLIC_USER'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
