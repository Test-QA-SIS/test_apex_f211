prompt --application/shared_components/logic/application_processes/p_tree_menu
begin
--   Manifest
--     APPLICATION PROCESS: P_TREE_MENU
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(278535531242772703)
,p_process_sequence=>1
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'P_TREE_MENU'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--pq_kseg_seguridad.pr_coll_tree_menu(:P0_ROL);',
':P0_TITULO := pq_kseg_seguridad.fn_titulo_opcion(:F_OPCION_ID);'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>' '
,p_process_when=>':P0_ROL is not null and :APP_PAGE_ID not in (2,101)'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'SQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
