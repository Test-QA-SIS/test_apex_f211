prompt --application/shared_components/logic/application_processes/pr_act_dividendos
begin
--   Manifest
--     APPLICATION PROCESS: PR_ACT_DIVIDENDOS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(84558576048345212)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACT_DIVIDENDOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  asdm_p.pq_car_cartera_interfaz.pr_act_coll_dividendos;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'  raise_application_error(-20000, ''error'');',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
