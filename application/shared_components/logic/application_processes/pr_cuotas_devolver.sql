prompt --application/shared_components/logic/application_processes/pr_cuotas_devolver
begin
--   Manifest
--     APPLICATION PROCESS: PR_CUOTAS_DEVOLVER
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(128743777622733842)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CUOTAS_DEVOLVER'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'pq_ven_pagos_cuota.pr_carga_colec_div_devolver(pn_seq_id => :P3,',
'                                               pn_emp_id => :P1,',
'                                               pn_uge_id       => :f_uge_id,',
'                                               pn_usu_id       => :f_user_id,',
'                                               pn_uge_id_gasto => :f_uge_id_gasto,',
'                                               pv_error  => :P10);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'ERROR'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
