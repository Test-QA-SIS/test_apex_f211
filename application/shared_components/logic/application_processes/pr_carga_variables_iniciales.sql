prompt --application/shared_components/logic/application_processes/pr_carga_variables_iniciales
begin
--   Manifest
--     APPLICATION PROCESS: PR_CARGA_VARIABLES_INICIALES
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_flow_process(
 p_id=>wwv_flow_imp.id(246980081264953501)
,p_process_sequence=>3
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_VARIABLES_INICIALES'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':F_SEG_ID := pq_ven_listas.fn_retorna_segento_age(:F_UGE_ID);',
':F_AGE_ID_AGENCIA := pq_ven_listas.fn_retorna_agencia(:F_UGE_ID);',
':F_UUG_ID:= pq_car_credito_interfaz.fn_usu_ugestion_conectado',
'                              (pn_usu_id => :f_user_id,',
'                               pn_uge_id => :F_UGE_ID,',
'                               pn_emp_id=>:f_emp_id);',
'',
':f_estado_reg_activo := 0;',
'',
':F_ECH_ID_APLICADO :=  pq_constantes.fn_retorna_constante(NULL, ''cn_ech_id_aplicado'');',
'',
'',
'-- DLOPEZ 27 SEP 2017 cargo variables iniciales',
':F_TVE_CONTADO   := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'');',
':F_TVE_TC   := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''); ',
':F_TSE_ID_MINOREO:=2;',
':F_TSE_ID_MAYOREO:=1;',
':F_TTR_ORDEN_VENTA := 112;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'No se puede encontrar el segmento mayoreo o minoreo de este usuario'
,p_process_when=>'51,41,93,52,31,93,96,100,999,123,127,149'
,p_process_when_type=>'CURRENT_PAGE_NOT_IN_CONDITION'
,p_version_scn=>6188208287363
);
wwv_flow_imp.component_end;
end;
/
