prompt --application/shared_components/security/authentications/application_express
begin
--   Manifest
--     AUTHENTICATION: Application Express
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_authentication(
 p_id=>wwv_flow_imp.id(332299454656686330)
,p_name=>'Application Express'
,p_scheme_type=>'NATIVE_APEX_ACCOUNTS'
,p_invalid_session_type=>'URL'
,p_invalid_session_url=>'f?p=155:101:&SESSION.'
,p_logout_url=>'f?p=155:101'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Use internal Application Express account credentials and login page in this application.'
);
wwv_flow_imp.component_end;
end;
/
