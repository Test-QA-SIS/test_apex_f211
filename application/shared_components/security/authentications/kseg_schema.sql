prompt --application/shared_components/security/authentications/kseg_schema
begin
--   Manifest
--     AUTHENTICATION: KSEG_SCHEMA
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_authentication(
 p_id=>wwv_flow_imp.id(332302061052707631)
,p_name=>'KSEG_SCHEMA'
,p_scheme_type=>'NATIVE_CUSTOM'
,p_attribute_03=>'PQ_KSEG_SEGURIDAD.fn_valida_usuario'
,p_attribute_05=>'N'
,p_invalid_session_type=>'URL'
,p_invalid_session_url=>'f?p=155:101:&SESSION.'
,p_logout_url=>'f?p=155:101'
,p_cookie_name=>'KSEG_USER'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Autenticacion de Kernel de Seguridad'
);
wwv_flow_imp.component_end;
end;
/
