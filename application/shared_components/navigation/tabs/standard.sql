prompt --application/shared_components/navigation/tabs/standard
begin
--   Manifest
--     TABS: 211
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_tab(
 p_id=>wwv_flow_imp.id(106884776491795815)
,p_tab_set=>'Menu'
,p_tab_sequence=>10
,p_tab_name=>'T_PAGOCUOTA'
,p_tab_text=>'Pago Cuota'
,p_tab_step=>6
);
wwv_flow_imp_shared.create_tab(
 p_id=>wwv_flow_imp.id(106885958960800275)
,p_tab_set=>'Menu'
,p_tab_sequence=>20
,p_tab_name=>'T_CONSULTAFACTURAS'
,p_tab_text=>'Consulta Facturas'
,p_tab_step=>65
,p_tab_plsql_condition=>':P6_CLI_ID is not null'
,p_display_condition_type=>'EXPRESSION'
,p_tab_disp_cond_text=>'PLSQL'
);
wwv_flow_imp_shared.create_tab(
 p_id=>wwv_flow_imp.id(119282171012835410)
,p_tab_set=>'T_PAGODECUOTARETENCION'
,p_tab_sequence=>10
,p_tab_name=>'T_PAGODECUOTARETENCION'
,p_tab_text=>'Pago de Cuota Retencion'
,p_tab_step=>64
);
wwv_flow_imp_shared.create_tab(
 p_id=>wwv_flow_imp.id(119288679324837795)
,p_tab_set=>'T_PAGODECUOTARETENCION'
,p_tab_sequence=>20
,p_tab_name=>'T_RETENCIONESAPLICADAS'
,p_tab_text=>'Retenciones Aplicadas'
,p_tab_step=>68
);
wwv_flow_imp_shared.create_tab(
 p_id=>wwv_flow_imp.id(126802351667157097)
,p_tab_set=>'T_REVERSODEPAGODECUOTA'
,p_tab_sequence=>1
,p_tab_name=>'T_REVERSODEPAGODECUOTA'
,p_tab_text=>'Reverso de Pago de Cuota'
,p_tab_step=>23
);
wwv_flow_imp_shared.create_tab(
 p_id=>wwv_flow_imp.id(126800956385148909)
,p_tab_set=>'T_REVERSODEPAGODECUOTA'
,p_tab_sequence=>10
,p_tab_name=>'T_TRASPASODEPAGODECUOTA'
,p_tab_text=>'Traspaso de Pago de Cuota'
,p_tab_step=>79
,p_tab_plsql_condition=>':P23_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:F_EMP_ID,                                 ''cn_tfp_id_traspaso_pago_cuota'') and :P23_IDENTIFICACION is not null and :P23_CLI_ID is not null and :P23_MCA_ID_PAGO_CUOTA is not null and :P23_ERROR i'
||'s null'
,p_display_condition_type=>'EXPRESSION'
,p_tab_disp_cond_text=>'PLSQL'
);
wwv_flow_imp.component_end;
end;
/
