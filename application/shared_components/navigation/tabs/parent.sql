prompt --application/shared_components/navigation/tabs/parent
begin
--   Manifest
--     TOP LEVEL TABS: 211
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_toplevel_tab(
 p_id=>wwv_flow_imp.id(119281956814831328)
,p_tab_set=>'aaa'
,p_tab_sequence=>10
,p_tab_name=>'T_PAGODECUOTARETENCION'
,p_tab_text=>'Pago de Cuota Retencion'
,p_tab_target=>'f?p=&APP_ID.:64:&SESSION.::&DEBUG.:::'
,p_current_on_tabset=>'T_PAGODECUOTARETENCION'
);
wwv_flow_imp_shared.create_toplevel_tab(
 p_id=>wwv_flow_imp.id(126800279456146172)
,p_tab_set=>'aaa'
,p_tab_sequence=>10
,p_tab_name=>'T_REVERSODEPAGODECUOTA'
,p_tab_text=>'Reverso de Pago de Cuota'
,p_tab_target=>'f?p=&APP_ID.:23:&SESSION.::&DEBUG.:::'
,p_current_on_tabset=>'T_REVERSODEPAGODECUOTA'
);
wwv_flow_imp_shared.create_toplevel_tab(
 p_id=>wwv_flow_imp.id(305395255248274683)
,p_tab_set=>'aaa'
,p_tab_sequence=>10
,p_tab_name=>'Pago Cuota'
,p_tab_text=>'Pago Cuota'
,p_tab_target=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:::'
,p_current_on_tabset=>'Menu'
);
wwv_flow_imp.component_end;
end;
/
