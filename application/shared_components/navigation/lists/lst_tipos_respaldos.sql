prompt --application/shared_components/navigation/lists/lst_tipos_respaldos
begin
--   Manifest
--     LIST: LST_TIPOS_RESPALDOS
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_list(
 p_id=>wwv_flow_imp.id(62204954516818262)
,p_name=>'LST_TIPOS_RESPALDOS'
,p_list_status=>'PUBLIC'
);
wwv_flow_imp_shared.create_list_item(
 p_id=>wwv_flow_imp.id(62205266441818273)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'CHEQUES'
,p_list_item_link_target=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.::::'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_imp_shared.create_list_item(
 p_id=>wwv_flow_imp.id(62205572809818281)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'GARANTIAS REALES'
,p_list_item_link_target=>'f?p=&APP_ID.:7:&SESSION.::&DEBUG.::P7_CLI_ID:&F244_CLI_ID.:'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_imp_shared.create_list_item(
 p_id=>wwv_flow_imp.id(62205863053818281)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'OTROS RESPALDOS'
,p_list_item_link_target=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.::::'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_imp.component_end;
end;
/
