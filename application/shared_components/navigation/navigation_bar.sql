prompt --application/shared_components/navigation/navigation_bar
begin
--   Manifest
--     ICON BAR ITEMS: 211
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_shared.create_icon_bar_item(
 p_id=>wwv_flow_imp.id(278546009135870401)
,p_icon_sequence=>10
,p_icon_subtext=>'Login'
,p_icon_target=>'f?p=155:101:&APP_SESSION.::&DEBUG.:::'
,p_nav_entry_is_feedback_yn=>'N'
,p_icon_bar_disp_cond=>'2'
,p_icon_bar_disp_cond_type=>'CURRENT_PAGE_EQUALS_CONDITION'
,p_begins_on_new_line=>'NO'
,p_cell_colspan=>1
);
wwv_flow_imp_shared.create_icon_bar_item(
 p_id=>wwv_flow_imp.id(279857916945912119)
,p_icon_sequence=>10
,p_icon_subtext=>'AP:&APP_ID.-PG:&APP_PAGE_ID.'
,p_icon_target=>'f?p=&APP_ID.:0:&SESSION.::&DEBUG.::::'
,p_nav_entry_is_feedback_yn=>'N'
,p_begins_on_new_line=>'NO'
,p_cell_colspan=>1
);
wwv_flow_imp_shared.create_icon_bar_item(
 p_id=>wwv_flow_imp.id(278499633834718733)
,p_icon_sequence=>20
,p_icon_subtext=>'&F_EMP_ID.-&F_EMPRESA.'
,p_icon_target=>'f?p=155:102:&APP_SESSION.::::F_UGE_ID,F_EMP_ID:0,0'
,p_nav_entry_is_feedback_yn=>'N'
,p_begins_on_new_line=>'NO'
,p_cell_colspan=>1
);
wwv_flow_imp_shared.create_icon_bar_item(
 p_id=>wwv_flow_imp.id(278500438226828650)
,p_icon_sequence=>30
,p_icon_subtext=>'&F_UGESTION.'
,p_icon_target=>'f?p=155:102:&APP_SESSION.::::F_UGE_ID:0'
,p_nav_entry_is_feedback_yn=>'N'
,p_begins_on_new_line=>'NO'
,p_cell_colspan=>1
);
wwv_flow_imp_shared.create_icon_bar_item(
 p_id=>wwv_flow_imp.id(435371783098738045)
,p_icon_sequence=>40
,p_icon_subtext=>'VER: &F_VERSION.'
,p_icon_target=>'javascript:false;'
,p_nav_entry_is_feedback_yn=>'N'
,p_icon_bar_disp_cond=>'NV(''APP_ID'')<>201 OR NV(''APP_PAGE_ID'')<>2'
,p_icon_bar_disp_cond_type=>'EXPRESSION'
,p_icon_bar_flow_cond_instr=>'PLSQL'
,p_begins_on_new_line=>'NO'
,p_cell_colspan=>1
);
wwv_flow_imp_shared.create_icon_bar_item(
 p_id=>wwv_flow_imp.id(332299780735686331)
,p_icon_sequence=>200
,p_icon_subtext=>'Salir'
,p_icon_target=>'f?p=155:101:&SESSION.::NO:::'
,p_icon_height=>32
,p_icon_width=>32
,p_icon_height2=>24
,p_icon_width2=>24
,p_nav_entry_is_feedback_yn=>'N'
,p_begins_on_new_line=>'NO'
,p_cell_colspan=>1
);
wwv_flow_imp.component_end;
end;
/
