prompt --application/pages/page_00160
begin
--   Manifest
--     PAGE: 00160
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>160
,p_name=>'LISTADO DE MOVIMIENTOS PAGADOS CON TARJETA'
,p_step_title=>'LISTADO DE MOVIMIENTOS PAGADOS CON TARJETA'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133255702966604465)
,p_plug_name=>unistr('Par\00E1metros de busqueda')
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(144134749303066999)
,p_plug_name=>'LISTADO DE MOVIMIENTOS PAGADOS CON TARJETA'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.mca_id,',
'       a.mca_fecha,',
'       (SELECT pue.pue_id || '' - '' || pue.pue_nombre',
'          FROM asdm_puntos_emision pue',
'         WHERE pue.pue_id = d.pue_id) punto_emision,',
'       (SELECT uge.uge_id || '' - '' || uge.uge_nombre',
'          FROM asdm_unidades_gestion uge',
'         WHERE uge.uge_id = a.uge_id) unidad_gestion,',
'       (SELECT ttr.ttr_id || '' - '' || ttr.ttr_descripcion',
'          FROM asdm_tipos_transacciones ttr',
'         WHERE ttr.ttr_id = a.ttr_id) tipo_transaccion,',
'      ''Cli_id: ''|| a.cli_id||''/ ID: ''||',
'       (SELECT  pe.per_nro_identificacion || '' - '' ||',
'               nvl(pe.per_razon_social,',
'                   pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'                   pe.per_primer_apellido || '' '' || pe.per_segundo_apellido)',
'          FROM asdm_clientes cl, asdm_personas pe',
'         WHERE cl.per_id = pe.per_id',
'           AND cl.cli_id = a.cli_id) cliente,',
'       ',
'       a.mca_observacion observacion,',
'       decode(a.mca_estado_mc, ''R'', ''REVERSADO'', ''ACEPTADO'') estado_del_movimiento,',
'       a.mca_total total_movimiento,',
'       b.mcd_valor_movimiento valor_pagado_con_tarjeta,',
'       --  b.mcd_id,',
'       (SELECT sys_connect_by_path(ede.ede_descripcion, ''->'')',
'          FROM asdm_entidades_destinos ede',
'         WHERE LEVEL = 5',
'         START WITH ede.ede_id = b.ede_id',
'        CONNECT BY PRIOR ede.ede_id_padre = ede.ede_id) entidad_financiera,',
'       --b.ede_id,',
'       b.mcd_nro_aut_ref autorizacion_registrada,',
'       b.mcd_nro_lote nro_lote_registrado,',
'       b.mcd_nro_comprobante nro_comprobante_registrado,',
'       b.mcd_nro_tarjeta nro_tarjeta_registrado,',
'       b.mcd_nro_recap nro_recap_registrado,',
'       b.mcd_voucher nro_voucher_registrado,',
'       b.mcd_fecha_deposito fecha_deposito_recap,',
'       decode(b.mcd_dep_transitorio, ''S'', ''SI'', ''NO'') esta_hecho_el_transitorio,',
'       b.mcd_titular_tarjeta titular_tarjeta,',
'       c.rpc_id,',
'       c.rpc_fecha fecha_hora_solicitud_pago,',
'       c.rpc_estado,',
'       (SELECT c.rpa_id || '' - '' || r.rpa_nombre',
'          FROM car_redes_pago r',
'         WHERE r.rpa_id = c.rpa_id) red,',
'       c.rpa_valor_pag valor_solicitado,',
'       --e.rpr_id,',
'       --e.rpc_id,',
'       e.rpr_fecha_respuesta fecha_hora_pago_procesado,',
'       fn_limpia_cadena(e.rpr_num_tarj_trun) tarjeta_procesada,',
'       fn_limpia_cadena(e.rpr_fecha_vencim) fecha_vencimiento,',
'       (SELECT e.rpr_cod_ident_red || '' - '' || tra.rpt_nombre',
'          FROM car_redes_pago_tra tra',
'         WHERE tra.rpt1_id = 2',
'           AND tra.rpt_codigo = e.rpr_cod_ident_red) codigo_ident_red,',
'       fn_limpia_cadena(e.rpr_respuesta) respuesta,',
'       e.rpr_secuen_trans,',
'       e.rpr_num_lote,',
'       e.rpr_hora_trans,',
'       e.rpr_fecha_trans,',
'       e.rpr_num_autorizacion,',
'       e.rpr_terminal_id,',
'       e.rpr_merchand_id,',
'       CASE',
'         WHEN fn_limpia_cadena(e.rpr_int_financiamiento) IS NOT NULL THEN',
'          to_number(substr2(e.rpr_int_financiamiento,',
'                            0,',
'                            length(e.rpr_int_financiamiento) - 2) || ''.'' ||',
'                    substr2(e.rpr_int_financiamiento,',
'                            length(e.rpr_int_financiamiento) - 1,',
'                            length(e.rpr_int_financiamiento)))',
'         ELSE',
'          NULL',
'       END int_financiamiento,',
'       e.rpr_nom_gru_tarj,',
'       (SELECT e.rpr_cod_ident_red || '' - '' || tra.rpt_nombre',
'          FROM car_redes_pago_tra tra',
'         WHERE tra.rpt1_id = 4',
'           AND tra.rpt_codigo = e.rpr_mod_lect) rpr_mod_lect,',
'       e.rpr_nom_tarj_hab,',
'       e.rpr_iden_emv,',
'       ''Imprimir'' imprimir,',
'       e.rpr_id_req,       ',
'       CASE',
'         WHEN e.rpr_estado_resp = ''ANULADO'' THEN',
'          3',
'         WHEN f.rpr_diferido = 0 THEN',
'          0',
'         WHEN f.rpr_diferido > 0 AND e.rpr_int_financiamiento IS NOT NULL THEN',
'          1',
'         WHEN f.rpr_diferido > 0 AND e.rpr_int_financiamiento IS NULL THEN',
'          2',
'       END tipo',
'       ',
'  FROM asdm_movimientos_caja         a,',
'       asdm_movimientos_caja_detalle b,',
'       car_redes_pago_cab            c,',
'       ven_periodos_caja             d,',
'       car_redes_pago_resp           e,',
'       car_redes_pago_req            f',
' WHERE a.mca_id = b.mca_id',
'   AND a.mca_fecha >= :p160_fecha_inicial AND a.mca_fecha <= :p160_fecha_final',
'   AND a.mca_id = c.mca_id',
'   AND a.emp_id= :F_EMP_ID',
'   AND a.pca_id = d.pca_id',
'   AND c.rpc_id = e.rpc_id',
'   AND e.mcd_id = b.mcd_id',
'   AND e.rpr_id_req = f.rpr_id',
'   --AND c.uge_id=nvl(:P160_UGE_MATRIZ,:f_uge_id) -- 20231218 AMF',
'   AND (c.uge_id=:f_uge_id or :f_uge_id = :P160_UGE_MATRIZ)',
'   AND b.tfp_id =',
'       pq_constantes.fn_retorna_constante(b.emp_id,',
'                                          ''cn_tfp_id_tarjeta_credito'')',
' ORDER BY 1 DESC;',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(144134835355066999)
,p_name=>'LISTADO DE MOVIMIENTOS PAGADOS CON TARJETA'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'JALVAREZ'
,p_internal_uid=>111881684085302073
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144135228481067023)
,p_db_column_name=>'MCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Mca Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144135574018067028)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fecha del movimiento'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144135959297067029)
,p_db_column_name=>'PUNTO_EMISION'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>unistr('Punto Emisi\00F3n')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144136424076067030)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>unistr('Unidad Gesti\00F3n')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144136763637067030)
,p_db_column_name=>'TIPO_TRANSACCION'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>unistr('Tipo Transacci\00F3n')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144137614450067032)
,p_db_column_name=>'CLIENTE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Cliente'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144138007044067033)
,p_db_column_name=>'OBSERVACION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>unistr('Observaci\00F3n')
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144138357865067033)
,p_db_column_name=>'ESTADO_DEL_MOVIMIENTO'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Estado Del Movimiento'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144138752398067033)
,p_db_column_name=>'TOTAL_MOVIMIENTO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Total Movimiento'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144139188131067033)
,p_db_column_name=>'VALOR_PAGADO_CON_TARJETA'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Valor Pagado Con Tarjeta'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144139598975067034)
,p_db_column_name=>'ENTIDAD_FINANCIERA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Entidad Financiera'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144140029689067034)
,p_db_column_name=>'AUTORIZACION_REGISTRADA'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Autorizacion Registrada'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144140372805067034)
,p_db_column_name=>'NRO_LOTE_REGISTRADO'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Nro Lote Registrado'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144140799756067035)
,p_db_column_name=>'NRO_COMPROBANTE_REGISTRADO'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Nro Comprobante Registrado'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144141174840067035)
,p_db_column_name=>'NRO_TARJETA_REGISTRADO'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Nro Tarjeta Registrado'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144141563294067040)
,p_db_column_name=>'NRO_RECAP_REGISTRADO'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Nro Recap Registrado'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144142003083067040)
,p_db_column_name=>'NRO_VOUCHER_REGISTRADO'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Nro Voucher Registrado'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144142359262067041)
,p_db_column_name=>'FECHA_DEPOSITO_RECAP'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Fecha Deposito Recap'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144142793426067042)
,p_db_column_name=>'ESTA_HECHO_EL_TRANSITORIO'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Esta Hecho El Transitorio'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144143219153067042)
,p_db_column_name=>'TITULAR_TARJETA'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Titular Tarjeta'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144143559340067042)
,p_db_column_name=>'FECHA_HORA_SOLICITUD_PAGO'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Fecha Hora Solicitud Pago'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144144000697067043)
,p_db_column_name=>'RPC_ESTADO'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Rpc Estado'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144144387590067043)
,p_db_column_name=>'RED'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Red'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144144787610067044)
,p_db_column_name=>'VALOR_SOLICITADO'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Valor Solicitado'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144145155051067044)
,p_db_column_name=>'FECHA_HORA_PAGO_PROCESADO'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Fecha Hora Pago Procesado'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144145626508067044)
,p_db_column_name=>'TARJETA_PROCESADA'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Tarjeta Procesada'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144145971936067045)
,p_db_column_name=>'FECHA_VENCIMIENTO'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Fecha Vencimiento'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144146377766067045)
,p_db_column_name=>'CODIGO_IDENT_RED'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Codigo Ident Red'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144146801115067045)
,p_db_column_name=>'RESPUESTA'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Respuesta'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144147155697067046)
,p_db_column_name=>'RPR_SECUEN_TRANS'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Rpr Secuen Trans'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144147570215067046)
,p_db_column_name=>'RPR_NUM_LOTE'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Rpr Num Lote'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144147988284067046)
,p_db_column_name=>'RPR_HORA_TRANS'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Rpr Hora Trans'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144148355649067047)
,p_db_column_name=>'RPR_FECHA_TRANS'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Rpr Fecha Trans'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144148759227067047)
,p_db_column_name=>'RPR_NUM_AUTORIZACION'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'Rpr Num Autorizacion'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144149241250067047)
,p_db_column_name=>'RPR_TERMINAL_ID'
,p_display_order=>36
,p_column_identifier=>'AJ'
,p_column_label=>'Rpr Terminal Id'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144149557184067047)
,p_db_column_name=>'RPR_MERCHAND_ID'
,p_display_order=>37
,p_column_identifier=>'AK'
,p_column_label=>'Rpr Merchand Id'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144149980296067048)
,p_db_column_name=>'INT_FINANCIAMIENTO'
,p_display_order=>38
,p_column_identifier=>'AL'
,p_column_label=>'Int Financiamiento'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144150444853067048)
,p_db_column_name=>'RPR_NOM_GRU_TARJ'
,p_display_order=>39
,p_column_identifier=>'AM'
,p_column_label=>'Rpr Nom Gru Tarj'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144150769475067048)
,p_db_column_name=>'RPR_MOD_LECT'
,p_display_order=>40
,p_column_identifier=>'AN'
,p_column_label=>'Rpr Mod Lect'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144151183161067049)
,p_db_column_name=>'RPR_NOM_TARJ_HAB'
,p_display_order=>41
,p_column_identifier=>'AO'
,p_column_label=>'Rpr Nom Tarj Hab'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144151597780067049)
,p_db_column_name=>'RPR_IDEN_EMV'
,p_display_order=>42
,p_column_identifier=>'AP'
,p_column_label=>'Rpr Iden Emv'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(133256126301604469)
,p_db_column_name=>'IMPRIMIR'
,p_display_order=>52
,p_column_identifier=>'AQ'
,p_column_label=>'Imprimir'
,p_column_link=>'f?p=&APP_ID.:160:&SESSION.:IMPRIMIR:&DEBUG.:RP:P160_RPC_ID,P160_TIPO:#RPC_ID#,#TIPO#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(40552369608725472)
,p_db_column_name=>'RPC_ID'
,p_display_order=>62
,p_column_identifier=>'AR'
,p_column_label=>'Rpc id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(40552630590725474)
,p_db_column_name=>'RPR_ID_REQ'
,p_display_order=>72
,p_column_identifier=>'AS'
,p_column_label=>'Rpr id req'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(40552731984725475)
,p_db_column_name=>'TIPO'
,p_display_order=>82
,p_column_identifier=>'AT'
,p_column_label=>'Tipo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(144152423103091755)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1118993'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'IMPRIMIR:MCA_ID:CLIENTE:TIPO_TRANSACCION:UNIDAD_GESTION:PUNTO_EMISION:MCA_FECHA:OBSERVACION:ESTADO_DEL_MOVIMIENTO:TOTAL_MOVIMIENTO:VALOR_PAGADO_CON_TARJETA:ENTIDAD_FINANCIERA:AUTORIZACION_REGISTRADA:NRO_LOTE_REGISTRADO:NRO_COMPROBANTE_REGISTRADO:NRO_'
||'TARJETA_REGISTRADO:NRO_RECAP_REGISTRADO:NRO_VOUCHER_REGISTRADO:FECHA_DEPOSITO_RECAP:ESTA_HECHO_EL_TRANSITORIO:TITULAR_TARJETA:'
,p_break_on=>'UNIDAD_GESTION:PUNTO_EMISION:0:0:0:0'
,p_break_enabled_on=>'UNIDAD_GESTION:PUNTO_EMISION:0:0:0:0'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(144285380475526327)
,p_plug_name=>'Pagos de Tarjeta Procesados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.rpc_id,',
'       (SELECT ''Cli_id: ''||a.cli_id||'' ID: ''|| pe.per_nro_identificacion || '' - '' ||',
'               nvl(pe.per_razon_social,',
'                   pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'                   pe.per_primer_apellido || '' '' || pe.per_segundo_apellido)',
'          FROM asdm_clientes cl, asdm_personas pe',
'         WHERE cl.per_id = pe.per_id',
'           AND cl.cli_id = a.cli_id) cliente,',
'       (SELECT uge.uge_id || '' - '' || uge.uge_nombre',
'          FROM asdm_unidades_gestion uge',
'         WHERE uge.uge_id = a.uge_id) unidad_gestion,',
'       (SELECT pue.pue_id || '' - '' || pue.pue_nombre',
'          FROM asdm_puntos_emision pue',
'         WHERE pue.pue_id = a.pue_id) punto_emision,',
'       a.rpc_fecha,',
'       a.rpc_estado,',
'       a.mca_id comprobante_caja,',
'       a.rpa_valor_pag,',
'       CASE',
'         WHEN c.rpr_estado_resp = ''ANULADO'' THEN',
'          ''ANULADO''',
'         WHEN b.rpr_diferido = 0 THEN',
'          ''CORRIENTE''',
'         WHEN b.rpr_diferido > 0 AND fn_limpia_cadena(c.rpr_int_financiamiento) IS NOT NULL THEN',
'          ''DIFERIDO CON INTERES''',
'         WHEN b.rpr_diferido > 0 AND fn_limpia_cadena(c.rpr_int_financiamiento) IS NULL THEN',
'          ''DIFERIDO SIN INTERES''',
'       END tipo_pago,',
'       CASE',
'         WHEN c.rpr_estado_resp = ''ANULADO'' THEN',
'          3',
'         WHEN b.rpr_diferido = 0 THEN',
'          0',
'         WHEN b.rpr_diferido > 0 AND fn_limpia_cadena(c.rpr_int_financiamiento) IS NOT NULL THEN',
'          1',
'         WHEN b.rpr_diferido > 0 AND fn_limpia_cadena(c.rpr_int_financiamiento) IS NULL THEN',
'          2',
'       END tipo,',
'       b.rpr_id                 rpr_id_req,',
'       b.rpr_fecha_consulta,',
'       b.rpr_tipo_requerimiento,',
'       b.rpr_tipo_transaccion,',
'       b.rpr_iden_red_adq,',
'       b.rpr_cod_diferido,',
'       b.rpr_diferido,',
'       b.rpr_meses_gracia,',
'       pq_car_redes_pago.fn_conv_tex_a_datos_num(pv_num => b.rpr_total_trans) total,',
'       pq_car_redes_pago.fn_conv_tex_a_datos_num(pv_num => b.rpr_sub_con_iva) base_consumo_tarifa12,',
'       pq_car_redes_pago.fn_conv_tex_a_datos_num(pv_num => b.rpr_sub_sin_iva) base_consumo_tarifa0,',
'       pq_car_redes_pago.fn_conv_tex_a_datos_num(pv_num => b.rpr_iva) iva,',
'       b.rpr_ser_trans,',
'       b.rpr_propina,',
'       b.rpr_monto_gasolina,',
'       b.rpr_secuencial_trans,',
'       b.rpr_hora_trans       rpr_hora_trans_req,',
'       b.rpr_fecha_trans      rpr_fecha_trans_req,',
'       b.rpr_num_autorizacion rpr_num_autorizacion_req,',
'       b.rpr_mid,',
'       b.rpr_tid,',
'       b.rpr_cid,',
'       b.rpr_factura,',
'       b.rpr_fecha_anul,',
'       case when b.rpr_hora_anul is not null then',
'       substr(b.rpr_hora_anul,1,2)||'':''||substr(b.rpr_hora_anul,3,2)||'':''||substr(b.rpr_hora_anul,4,2) ',
'       else',
'       null ',
'       end hora_anulacion,',
'       b.rpr_estado_pago,',
'       b.rpr_secuencia_trans_ref,',
'       c.rpr_id                  rpr_id_res,',
'       c.rpr_fecha_respuesta,',
'       c.rpr_tipo_respuesta,',
'       c.rpr_codigo_respuesta,',
'       c.rpr_red_adq_cor,',
'       c.rpr_red_adq_dif,',
'       c.rpr_num_tarj_trun,',
'       c.rpr_fecha_vencim,',
'       c.rpr_num_tarj_encrip,',
'       c.rpr_men_respuesta,',
'       c.rpr_bin_tarjeta,',
'       c.rpr_filler2,',
'       c.rpr_cod_ident_red,',
'       c.rpr_resp_autorizador,',
'       c.rpr_respuesta,',
'       c.rpr_secuen_trans,',
'       c.rpr_num_lote,',
'       c.rpr_hora_trans          rpr_hora_trans_res,',
'       c.rpr_fecha_trans,',
'       c.rpr_num_autorizacion,',
'       c.rpr_terminal_id,',
'       c.rpr_merchand_id,',
'              CASE',
'         WHEN fn_limpia_cadena(c.rpr_int_financiamiento) IS NOT NULL THEN',
'          to_number(substr2(c.rpr_int_financiamiento,',
'                            0,',
'                            length(c.rpr_int_financiamiento) - 2) || ''.'' ||',
'                    substr2(c.rpr_int_financiamiento,',
'                            length(c.rpr_int_financiamiento) - 1,',
'                            length(c.rpr_int_financiamiento)))',
'         ELSE',
'          NULL',
'       END rpr_int_financiamiento,',
'       c.rpr_men_impresion,',
'       c.rpr_cod_banco_adq,',
'       c.rpr_cod_ban_emi,',
'       c.rpr_nom_gru_tarj,',
'       c.rpr_mod_lect,',
'       c.rpr_nom_tarj_hab,',
'       c.rpr_monto_gasol,',
'       c.rpr_iden_emv,',
'       c.rpr_aid_emv,',
'       c.rpr_tipo_cripto_emv,',
'       c.rpr_verif_pin,',
'       c.rpr_arqc,',
'       c.rpr_tvr,',
'       c.rpr_tsi,',
'       c.ede_id,',
'       c.mcd_id,',
'       c.rpr_estado_resp,',
'       c.rpr_secuen_siguiente,',
'       ''Imprimir'' imprimir',
'  FROM car_redes_pago_cab a, car_redes_pago_req b, car_redes_pago_resp c',
' WHERE a.rpc_id = b.rpc_id',
'   AND a.rpc_id = c.rpc_id',
'   AND b.rpr_tipo_requerimiento = ''PP''',
'   AND c.rpr_tipo_respuesta = ''PP''',
'   AND b.rpr_id = c.rpr_id_req',
'   AND a.emp_id = :f_emp_id',
'   AND a.rpc_fecha >= :p160_fecha_inicial AND a.rpc_fecha <= :p160_fecha_final',
'   and a.uge_id = :f_uge_id',
'   AND a.rpc_estado != ''ERROR''',
'   AND c.rpr_estado_resp != ''ERROR''',
'   --   AND a.uge_id=nvl(:P160_UGE_MATRIZ,:f_uge_id) -- AMF 20231218 ',
'   AND (a.uge_id=:f_uge_id or :f_uge_id = :P160_UGE_MATRIZ)  ',
' ORDER BY a.rpc_id;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(144285482485526328)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'JALVAREZ'
,p_internal_uid=>112032331215761402
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144285592622526329)
,p_db_column_name=>'RPC_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Rpc id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144285729010526330)
,p_db_column_name=>'CLIENTE'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cliente'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144285755354526331)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Unidad gestion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144285901175526332)
,p_db_column_name=>'PUNTO_EMISION'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Punto emision'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286008125526333)
,p_db_column_name=>'RPC_FECHA'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Fecha pago'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286304625526336)
,p_db_column_name=>'RPC_ESTADO'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Estado del pago'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286637592526339)
,p_db_column_name=>'RPA_VALOR_PAG'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Valor Pagado'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286714701526340)
,p_db_column_name=>'TIPO'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'Tipo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286761639526341)
,p_db_column_name=>'RPR_ID_REQ'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'Rpr id req'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286919623526342)
,p_db_column_name=>'RPR_FECHA_CONSULTA'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'Rpr fecha consulta'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144286962233526343)
,p_db_column_name=>'RPR_TIPO_REQUERIMIENTO'
,p_display_order=>150
,p_column_identifier=>'O'
,p_column_label=>'Rpr tipo requerimiento'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144287062597526344)
,p_db_column_name=>'RPR_TIPO_TRANSACCION'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>'Rpr tipo transaccion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144287174987526345)
,p_db_column_name=>'RPR_IDEN_RED_ADQ'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'Rpr iden red adq'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144287270627526346)
,p_db_column_name=>'RPR_COD_DIFERIDO'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'Rpr cod diferido'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144287450899526347)
,p_db_column_name=>'RPR_DIFERIDO'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'Diferido'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144287463532526348)
,p_db_column_name=>'RPR_MESES_GRACIA'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'Meses gracia'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288018381526353)
,p_db_column_name=>'RPR_SER_TRANS'
,p_display_order=>250
,p_column_identifier=>'Y'
,p_column_label=>'Rpr ser trans'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288124390526354)
,p_db_column_name=>'RPR_PROPINA'
,p_display_order=>260
,p_column_identifier=>'Z'
,p_column_label=>'Rpr propina'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288202231526355)
,p_db_column_name=>'RPR_MONTO_GASOLINA'
,p_display_order=>270
,p_column_identifier=>'AA'
,p_column_label=>'Rpr monto gasolina'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288290955526356)
,p_db_column_name=>'RPR_SECUENCIAL_TRANS'
,p_display_order=>280
,p_column_identifier=>'AB'
,p_column_label=>'Rpr secuencial trans'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288374805526357)
,p_db_column_name=>'RPR_HORA_TRANS_REQ'
,p_display_order=>290
,p_column_identifier=>'AC'
,p_column_label=>'Rpr hora trans req'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288498991526358)
,p_db_column_name=>'RPR_FECHA_TRANS_REQ'
,p_display_order=>300
,p_column_identifier=>'AD'
,p_column_label=>'Rpr fecha trans req'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288645067526359)
,p_db_column_name=>'RPR_NUM_AUTORIZACION_REQ'
,p_display_order=>310
,p_column_identifier=>'AE'
,p_column_label=>'Rpr num autorizacion req'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288690770526360)
,p_db_column_name=>'RPR_MID'
,p_display_order=>320
,p_column_identifier=>'AF'
,p_column_label=>'MID'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288811956526361)
,p_db_column_name=>'RPR_TID'
,p_display_order=>330
,p_column_identifier=>'AG'
,p_column_label=>'TID'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288861433526362)
,p_db_column_name=>'RPR_CID'
,p_display_order=>340
,p_column_identifier=>'AH'
,p_column_label=>'CID'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144288996702526363)
,p_db_column_name=>'RPR_FACTURA'
,p_display_order=>350
,p_column_identifier=>'AI'
,p_column_label=>'Rpr factura'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289148474526364)
,p_db_column_name=>'RPR_FECHA_ANUL'
,p_display_order=>360
,p_column_identifier=>'AJ'
,p_column_label=>unistr('Fecha anulaci\00F3n')
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289251526526366)
,p_db_column_name=>'RPR_ESTADO_PAGO'
,p_display_order=>380
,p_column_identifier=>'AL'
,p_column_label=>'Rpr estado pago'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289420198526367)
,p_db_column_name=>'RPR_SECUENCIA_TRANS_REF'
,p_display_order=>390
,p_column_identifier=>'AM'
,p_column_label=>'Rpr secuencia trans ref'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289501951526368)
,p_db_column_name=>'RPR_ID_RES'
,p_display_order=>400
,p_column_identifier=>'AN'
,p_column_label=>'Rpr id res'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289555876526369)
,p_db_column_name=>'RPR_FECHA_RESPUESTA'
,p_display_order=>410
,p_column_identifier=>'AO'
,p_column_label=>'Rpr fecha respuesta'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289728333526370)
,p_db_column_name=>'RPR_TIPO_RESPUESTA'
,p_display_order=>420
,p_column_identifier=>'AP'
,p_column_label=>'Rpr tipo respuesta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289842747526371)
,p_db_column_name=>'RPR_CODIGO_RESPUESTA'
,p_display_order=>430
,p_column_identifier=>'AQ'
,p_column_label=>'Rpr codigo respuesta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289869415526372)
,p_db_column_name=>'RPR_RED_ADQ_COR'
,p_display_order=>440
,p_column_identifier=>'AR'
,p_column_label=>'Rpr red adq cor'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144289996376526373)
,p_db_column_name=>'RPR_RED_ADQ_DIF'
,p_display_order=>450
,p_column_identifier=>'AS'
,p_column_label=>'Rpr red adq dif'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290124077526374)
,p_db_column_name=>'RPR_NUM_TARJ_TRUN'
,p_display_order=>460
,p_column_identifier=>'AT'
,p_column_label=>unistr('N\00FAmero Tarjeta')
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290200517526375)
,p_db_column_name=>'RPR_FECHA_VENCIM'
,p_display_order=>470
,p_column_identifier=>'AU'
,p_column_label=>'Fecha Vencimiento'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290325675526376)
,p_db_column_name=>'RPR_NUM_TARJ_ENCRIP'
,p_display_order=>480
,p_column_identifier=>'AV'
,p_column_label=>'Rpr num tarj encrip'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290443434554727)
,p_db_column_name=>'RPR_MEN_RESPUESTA'
,p_display_order=>490
,p_column_identifier=>'AW'
,p_column_label=>'Rpr men respuesta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290492503554728)
,p_db_column_name=>'RPR_BIN_TARJETA'
,p_display_order=>500
,p_column_identifier=>'AX'
,p_column_label=>'Rpr bin tarjeta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290568040554729)
,p_db_column_name=>'RPR_FILLER2'
,p_display_order=>510
,p_column_identifier=>'AY'
,p_column_label=>'Rpr filler2'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290662550554730)
,p_db_column_name=>'RPR_COD_IDENT_RED'
,p_display_order=>520
,p_column_identifier=>'AZ'
,p_column_label=>'Rpr cod ident red'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290823420554731)
,p_db_column_name=>'RPR_RESP_AUTORIZADOR'
,p_display_order=>530
,p_column_identifier=>'BA'
,p_column_label=>'Rpr resp autorizador'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144290870537554732)
,p_db_column_name=>'RPR_RESPUESTA'
,p_display_order=>540
,p_column_identifier=>'BB'
,p_column_label=>'Rpr respuesta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291033227554733)
,p_db_column_name=>'RPR_SECUEN_TRANS'
,p_display_order=>550
,p_column_identifier=>'BC'
,p_column_label=>'Rpr secuen trans'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291127867554734)
,p_db_column_name=>'RPR_NUM_LOTE'
,p_display_order=>560
,p_column_identifier=>'BD'
,p_column_label=>'Lote'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291219422554735)
,p_db_column_name=>'RPR_HORA_TRANS_RES'
,p_display_order=>570
,p_column_identifier=>'BE'
,p_column_label=>'Rpr hora trans res'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291264690554736)
,p_db_column_name=>'RPR_FECHA_TRANS'
,p_display_order=>580
,p_column_identifier=>'BF'
,p_column_label=>'Rpr fecha trans'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291390577554737)
,p_db_column_name=>'RPR_NUM_AUTORIZACION'
,p_display_order=>590
,p_column_identifier=>'BG'
,p_column_label=>unistr('Num Autorizaci\00F3n')
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291517721554738)
,p_db_column_name=>'RPR_TERMINAL_ID'
,p_display_order=>600
,p_column_identifier=>'BH'
,p_column_label=>'Rpr terminal id'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291626439554739)
,p_db_column_name=>'RPR_MERCHAND_ID'
,p_display_order=>610
,p_column_identifier=>'BI'
,p_column_label=>'Rpr merchand id'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291845762554741)
,p_db_column_name=>'RPR_MEN_IMPRESION'
,p_display_order=>630
,p_column_identifier=>'BK'
,p_column_label=>'Rpr men impresion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144291904953554742)
,p_db_column_name=>'RPR_COD_BANCO_ADQ'
,p_display_order=>640
,p_column_identifier=>'BL'
,p_column_label=>'Rpr cod banco adq'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292047340554743)
,p_db_column_name=>'RPR_COD_BAN_EMI'
,p_display_order=>650
,p_column_identifier=>'BM'
,p_column_label=>'Rpr cod ban emi'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292064881554744)
,p_db_column_name=>'RPR_NOM_GRU_TARJ'
,p_display_order=>660
,p_column_identifier=>'BN'
,p_column_label=>'Emisor'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292248320554745)
,p_db_column_name=>'RPR_MOD_LECT'
,p_display_order=>670
,p_column_identifier=>'BO'
,p_column_label=>'Rpr mod lect'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292323735554746)
,p_db_column_name=>'RPR_NOM_TARJ_HAB'
,p_display_order=>680
,p_column_identifier=>'BP'
,p_column_label=>'Titular'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292374951554747)
,p_db_column_name=>'RPR_MONTO_GASOL'
,p_display_order=>690
,p_column_identifier=>'BQ'
,p_column_label=>'Rpr monto gasol'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292525202554748)
,p_db_column_name=>'RPR_IDEN_EMV'
,p_display_order=>700
,p_column_identifier=>'BR'
,p_column_label=>'Rpr iden emv'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292579700554749)
,p_db_column_name=>'RPR_AID_EMV'
,p_display_order=>710
,p_column_identifier=>'BS'
,p_column_label=>'Rpr aid emv'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292750944554750)
,p_db_column_name=>'RPR_TIPO_CRIPTO_EMV'
,p_display_order=>720
,p_column_identifier=>'BT'
,p_column_label=>'Rpr tipo cripto emv'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292756307554751)
,p_db_column_name=>'RPR_VERIF_PIN'
,p_display_order=>730
,p_column_identifier=>'BU'
,p_column_label=>'Rpr verif pin'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292909988554752)
,p_db_column_name=>'RPR_ARQC'
,p_display_order=>740
,p_column_identifier=>'BV'
,p_column_label=>'Rpr arqc'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144292952574554753)
,p_db_column_name=>'RPR_TVR'
,p_display_order=>750
,p_column_identifier=>'BW'
,p_column_label=>'Rpr tvr'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144293076052554754)
,p_db_column_name=>'RPR_TSI'
,p_display_order=>760
,p_column_identifier=>'BX'
,p_column_label=>'Rpr tsi'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144293207152554755)
,p_db_column_name=>'EDE_ID'
,p_display_order=>770
,p_column_identifier=>'BY'
,p_column_label=>'Ede id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144293343175554756)
,p_db_column_name=>'MCD_ID'
,p_display_order=>780
,p_column_identifier=>'BZ'
,p_column_label=>'Mcd id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144293390273554757)
,p_db_column_name=>'RPR_ESTADO_RESP'
,p_display_order=>790
,p_column_identifier=>'CA'
,p_column_label=>'Rpr estado resp'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144293483395554758)
,p_db_column_name=>'RPR_SECUEN_SIGUIENTE'
,p_display_order=>800
,p_column_identifier=>'CB'
,p_column_label=>'Rpr secuen siguiente'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(133256709273604475)
,p_db_column_name=>'COMPROBANTE_CAJA'
,p_display_order=>810
,p_column_identifier=>'CC'
,p_column_label=>'Comprobante caja'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(133256763456604476)
,p_db_column_name=>'TIPO_PAGO'
,p_display_order=>820
,p_column_identifier=>'CD'
,p_column_label=>'Tipo pago'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(146210590725150735)
,p_db_column_name=>'RPR_INT_FINANCIAMIENTO'
,p_display_order=>870
,p_column_identifier=>'CI'
,p_column_label=>'Interes financiamiento'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(146210687892150736)
,p_db_column_name=>'HORA_ANULACION'
,p_display_order=>880
,p_column_identifier=>'CJ'
,p_column_label=>unistr('Hora anulaci\00F3n')
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(146210849528150737)
,p_db_column_name=>'IMPRIMIR'
,p_display_order=>890
,p_column_identifier=>'CK'
,p_column_label=>'Imprimir'
,p_column_link=>'f?p=&APP_ID.:160:&SESSION.:IMPRIMIR:&DEBUG.:RP:P160_RPC_ID,P160_TIPO:#RPC_ID#,#TIPO#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'#IMPRIMIR#'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144293857800554762)
,p_db_column_name=>'TOTAL'
,p_display_order=>900
,p_column_identifier=>'CL'
,p_column_label=>'Total'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144294048604554763)
,p_db_column_name=>'BASE_CONSUMO_TARIFA12'
,p_display_order=>910
,p_column_identifier=>'CM'
,p_column_label=>'Base consumo tarifa12'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144294100122554764)
,p_db_column_name=>'BASE_CONSUMO_TARIFA0'
,p_display_order=>920
,p_column_identifier=>'CN'
,p_column_label=>'Base consumo tarifa0'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144294162263554765)
,p_db_column_name=>'IVA'
,p_display_order=>930
,p_column_identifier=>'CO'
,p_column_label=>'Iva'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(144320034390556826)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1120669'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'IMPRIMIR:CLIENTE:UNIDAD_GESTION:PUNTO_EMISION:RPC_FECHA:RPR_FECHA_ANUL:HORA_ANULACION:RPC_ESTADO:TIPO_PAGO:RPR_DIFERIDO:RPR_MESES_GRACIA:RPR_INT_FINANCIAMIENTO:RPR_NUM_TARJ_TRUN:RPR_FECHA_VENCIM:RPR_NUM_LOTE:RPR_NUM_AUTORIZACION:RPR_NOM_GRU_TARJ:RPR_'
||'NOM_TARJ_HAB:COMPROBANTE_CAJA:RPR_MID:RPR_TID:RPR_CID:TOTAL:BASE_CONSUMO_TARIFA12:BASE_CONSUMO_TARIFA0:IVA'
,p_break_on=>'UNIDAD_GESTION:PUNTO_EMISION:RPC_ESTADO:0:0:0'
,p_break_enabled_on=>'UNIDAD_GESTION:PUNTO_EMISION:0:0:0'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(146241353139531798)
,p_report_id=>wwv_flow_imp.id(144320034390556826)
,p_name=>'Anulados'
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'RPC_ESTADO'
,p_operator=>'='
,p_expr=>'ANULADO'
,p_condition_sql=>' (case when ("RPC_ESTADO" = #APXWS_EXPR#) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# = ''ANULADO''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#FF7755'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(146241678918531798)
,p_report_id=>wwv_flow_imp.id(144320034390556826)
,p_name=>'Procesados'
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'RPC_ESTADO'
,p_operator=>'in'
,p_expr=>'PROCESADO,FINALIZADO'
,p_condition_sql=>' (case when ("RPC_ESTADO" in (#APXWS_EXPR_VAL1#, #APXWS_EXPR_VAL2#)) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME# ''PROCESADO, FINALIZADO''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#99CCFF'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133256035712604468)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(133255702966604465)
,p_button_name=>'BTN_BUSCAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Buscar'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40552329076725471)
,p_name=>'P160_RPC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(144134749303066999)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40552760108725476)
,p_name=>'P160_TIPO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(144134749303066999)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133255762690604466)
,p_name=>'P160_FECHA_INICIAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133255702966604465)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha inicial'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133255886772604467)
,p_name=>'P160_FECHA_FINAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133255702966604465)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha final'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(412028716134703266)
,p_name=>'P160_UGE_MATRIZ'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(144285380475526327)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(40552461610725473)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  pq_car_redes_pago.pr_imprimir_voucher(pn_rpc_id => :p160_rpc_id,',
'                                        pn_emp_id => :F_emp_id,',
'                                        pv_tipo => :p160_tipo,',
'                                        pv_error => :p0_error);',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'IMPRIMIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>8299310340960547
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(412028847585703267)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_matriz'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'If :f_uge_id = 274 then',
'    :p160_uge_matriz:=:f_uge_id;',
'end if;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>379775696315938341
);
wwv_flow_imp.component_end;
end;
/
