prompt --application/pages/page_00136
begin
--   Manifest
--     PAGE: 00136
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>136
,p_name=>'0'
,p_step_title=>'0'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112141210'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(161490417613284814)
,p_plug_name=>'titulo'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_06'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(178863845798294128)
,p_name=>'<B><font color="RED">PRODUCCION</font></B>-- Roles Asignados'
,p_region_name=>'rol_menu'
,p_template=>wwv_flow_imp.id(270526680051046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_02'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT a.descripcion, a.rol_id, 5000 opcion_id',
'  FROM kseg_e.kseg_roles a, kseg_e.kseg_roles_usuario b, kseg_e.kseg_usuarios c',
' WHERE a.rol_id = b.rol_id',
'   AND c.usuario_id = :F_USER_ID',
'   AND b.usuario_id = c.usuario_id',
'   AND a.emp_id = :F_EMP_ID',
'   AND a.estado = 0}'';'))
,p_display_when_condition=>':F_TOKEN is not null and nvl(:F_POPUP,''N'') = ''N'''
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>3
,p_query_num_rows=>500
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122899535590055677)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=201:1:&SESSION.:ROL:&DEBUG.::P0_ROL,P0_ROL_DESC,P0_TREE_ROOT:#COL02#,#COL01#,#COL03#'
,p_column_linktext=>'#COL01#'
,p_column_link_attr=>'style="color: #0b5581; font-family: Arial, Helvetica, Geneva, sans-serif"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122899873926055678)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122900264922055678)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(178864951470305151)
,p_plug_name=>'Opciones Rol &P0_ROL_DESC. Obsoleto'
,p_region_name=>'opcion_rol'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526680051046670)
,p_plug_display_sequence=>100
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_02'
,p_plug_item_display_point=>'BELOW'
,p_plug_source=>'90662248649290769'
,p_plug_source_type=>'NATIVE_TREE'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>wwv_flow_imp.id(114641119659034707)
,p_plug_comment=>':P0_ROL is not null and nvl(:F_POPUP,''N'') = ''N'''
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(309539467167281352)
,p_name=>'Mis Favoritos'
,p_region_name=>'reg_id_favoritos'
,p_template=>wwv_flow_imp.id(270526680051046670)
,p_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'REGION_POSITION_02'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT f.obj_opcion,',
'       ''f?p='' || f.obj_app || '':'' || f.obj_pag || '':&SESSION.:ROL:NO:'' ||',
'       f.obj_pag ||',
'       '':F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,'' ||',
'       ''P136_ROL,P136_ROL_DESC,P136_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,F_VERSION:'' ||',
'       ''f?p=201,&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P136_ROL.,'' ||',
'       ''&P136_ROL_DESC.,&P136_TREE_ROOT.,'' || f.opc_id || '','' || f.opc_request ||',
'       '',N,&F_VERSION.'' link,',
'       f.opc_id,',
'       ''<img src="#IMAGE_PREFIX#m_del.png" class="lock_ui_row" style="cursor:pointer" onclick="fn_ins_del_fav('' ||',
'       TRIM(f.opc_id) || '',9)">'' "Elimina"',
'  FROM kseg_e.kseg_usuarios_favoritos f',
' WHERE usu_id = :f_user_id',
'   AND rol_id = :p136_rol',
'   AND emp_id = :f_emp_id'))
,p_display_when_condition=>':P136_ROL is not null and nvl(:F_POPUP,''N'') = ''N'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(309539467167281352)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122893924429055666)
,p_query_column_id=>1
,p_column_alias=>'OBJ_OPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Listado opciones favoritas'
,p_use_as_row_header=>'N'
,p_column_link=>'#LINK#'
,p_column_linktext=>'#OBJ_OPCION#'
,p_column_link_attr=>'nowrap="nowrap"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122894279206055669)
,p_query_column_id=>2
,p_column_alias=>'LINK'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122894688303055669)
,p_query_column_id=>3
,p_column_alias=>'OPC_ID'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122895068502055669)
,p_query_column_id=>4
,p_column_alias=>'Elimina'
,p_column_display_sequence=>1
,p_column_heading=>'Elimina'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(309540389059287231)
,p_name=>'Opci&oacute;n'
,p_region_name=>'reg_id_opcion'
,p_template=>wwv_flow_imp.id(270524583473046668)
,p_display_sequence=>80
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'REGION_POSITION_02'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT a.opcion_id       ,              ',
'               a.descripcion,   ',
'               c.nombre_corto,   ',
'                ''<img src="#IMAGE_PREFIX#m_sel.png" class="lock_ui_row" style="cursor:pointer" onclick="fn_ins_del_fav(''||a.opcion_id|| '',0)">'' "Agregar",',
'                CASE',
'         WHEN c.tipo_objeto_id = 4 AND c.prefijo IS NOT NULL THEN',
'          ''f?p='' || c.nombre_corto || '':'' || c.prefijo ||',
'          '':&SESSION.:ROL:NO:'' || c.prefijo ||',
'          '':F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,'' ||',
'          ''P136_ROL,P136_ROL_DESC,P136_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,F_VERSION:'' ||',
'          ''f?p=201,&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P136_ROL.,'' ||',
'          ''&P136_ROL_DESC.,&P136_TREE_ROOT.,'' || a.opcion_id || '','' || a.prefijo ||',
'          '',N,&F_VERSION.''',
'         ELSE',
'          NULL',
'       END AS link',
'        ',
'          FROM kseg_e.kseg_opciones a, kseg_e.kseg_opciones_rol b, kdda_e.kdda_objetos c',
'         WHERE a.opcion_id = b.opcion_id',
'           AND b.rol_id = :p136_rol',
'           AND a.estado = 0',
'           AND b.estado = 0',
'           AND a.opcion_id = NVL(:p136_opcion,0)',
'           AND a.objeto_id = c.objeto_id'))
,p_display_when_condition=>':P136_ROL is not null and nvl(:F_POPUP,''N'') = ''N'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(309540389059287231)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122895774311055671)
,p_query_column_id=>1
,p_column_alias=>'OPCION_ID'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122896577934055672)
,p_query_column_id=>2
,p_column_alias=>'DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Nombre de la opci&oacute;n buscada'
,p_use_as_row_header=>'N'
,p_column_link=>'#LINK#'
,p_column_linktext=>'#DESCRIPCION#'
,p_column_link_attr=>'nowrap="nowrap"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122896199205055672)
,p_query_column_id=>3
,p_column_alias=>'NOMBRE_CORTO'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122897038982055672)
,p_query_column_id=>4
,p_column_alias=>'Agregar'
,p_column_display_sequence=>1
,p_column_heading=>'Agregar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(122897392278055673)
,p_query_column_id=>5
,p_column_alias=>'LINK'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(341110327138688939)
,p_plug_name=>'Opciones Rol &P0_ROL_DESC.'
,p_region_name=>'opcion_rol'
,p_region_template_options=>'#DEFAULT#'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_imp.id(270526680051046670)
,p_plug_display_sequence=>90
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_02'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN connect_by_isleaf = 1 THEN',
'          0',
'         WHEN LEVEL = 1 THEN',
'          1',
'         ELSE',
'          -1',
'       END AS status,',
'       LEVEL,',
'       z.name AS title,',
'       CASE',
'         WHEN z.tipo_objeto_id = 4 AND z.prefijo1 IS NOT NULL THEN',
'          ''#IMAGE_PREFIX#m_sel.png''',
'         ELSE',
'          ''#IMAGE_PREFIX#m_men.png''',
'       END icon,',
'       id AS VALUE,',
'       z.nombre_corto AS tooltip,',
'       CASE',
'         WHEN z.tipo_objeto_id = 4 AND z.prefijo1 IS NOT NULL THEN',
'          ''f?p='' || z.nombre_corto || '':'' || z.prefijo1 ||',
'          '':&SESSION.:ROL:NO:'' || z.prefijo1 ||',
'          '':F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,'' ||',
'          ''P136_ROL,P136_ROL_DESC,P136_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,F_VERSION:'' ||',
'          ''f?p=201,&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P136_ROL.,'' ||',
'          ''&P136_ROL_DESC.,&P136_TREE_ROOT.,'' || z.id || '','' || z.prefijo ||',
'          '',N,&F_VERSION.''',
'         ELSE',
'          NULL',
'       END AS link',
'  FROM (SELECT a.opcion_id       id,',
'               a.opcion_padre_id opcion_padre_id,',
'               a.descripcion     NAME,',
'               a.opcion_id,',
'               a.prefijo,',
'               c.nombre_corto,',
'               c.prefijo         prefijo1,',
'               c.tipo_objeto_id,',
'               a.orden',
'          FROM kseg_e.kseg_opciones a, kseg_e.kseg_opciones_rol b, kdda_e.kdda_objetos c',
'         WHERE a.opcion_id = b.opcion_id',
'           AND b.rol_id = :p136_rol',
'           AND a.estado = 0',
'           AND b.estado = 0',
'           AND a.objeto_id = c.objeto_id) z',
' START WITH z.opcion_padre_id = 0',
'CONNECT BY PRIOR z.opcion_id = z.opcion_padre_id',
' ORDER SIBLINGS BY z.orden'))
,p_lazy_loading=>false
,p_plug_source_type=>'NATIVE_JSTREE'
,p_plug_column_width=>'style=".tree li a, .tree li span {white-space: normal;}"'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P136_ROL is not null and nvl(:F_POPUP,''N'') = ''N'''
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'apple'
,p_attribute_02=>'S'
,p_attribute_03=>'P136_TREE_ROOT'
,p_attribute_04=>'N'
,p_attribute_06=>'tree1'
,p_attribute_07=>'JSTREE'
,p_attribute_10=>'"3"'
,p_attribute_11=>'"2"'
,p_attribute_12=>'"4"'
,p_attribute_15=>'"1"'
,p_attribute_20=>'"5"'
,p_attribute_22=>'"6"'
,p_attribute_23=>'LEVEL'
,p_attribute_24=>'"7"'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(355590018415140342)
,p_plug_name=>'Error'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'AFTER_HEADER'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(3543431827401157900)
,p_plug_name=>'Menu'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>2
,p_plug_display_point=>'REGION_POSITION_02'
,p_plug_item_display_point=>'BELOW'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span id="btn_menu_o" style="background:url(''/i/RightRow.png'') no-repeat left scroll; position:absolute; cursor:pointer; height:62px; vertical-align:top; width:28px; display:none"></span>',
'',
'<span id="btn_menu_v" style="background:url(''/i/LeftRow.png'') no-repeat left scroll; position:absolute; cursor:pointer; height:62px; vertical-align:top; width:28px; display:none"></span>'))
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(9904048706465515044)
,p_plug_name=>'Banner'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>9
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_02'
,p_plug_item_display_point=>'BELOW'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'   htp.p(''<img style="cursor:pointer" src=''''../../i/aplimar/Banner/''||kseg_p.pq_kseg_seguridad.fn_banner(:P136_ROL,:F_EMP_ID,:P136_ERROR)||'''''' width=''''100%'''' height=''''220'''' onclick=''''javascript:fn_unlock(html_PopUp("''||kseg_p.pq_kseg_seguridad.fn_ban'
||'ner_url(:P136_ROL,:F_EMP_ID,:P136_ERROR)||''"));''''/>'');',
'end;'))
,p_plug_source_type=>'NATIVE_PLSQL'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'FUNCTION_BODY'
,p_plug_display_when_condition=>'return kseg_p.pq_kseg_seguridad.fn_rol_banner(:P136_ROL,:F_EMP_ID,:P136_ERROR);'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122898142567055674)
,p_name=>'P136_TREE_ROOT'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(341110327138688939)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122898797798055676)
,p_name=>'P136_TITULO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(161490417613284814)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'HTML_UNSAFE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122900694285055679)
,p_name=>'P136_OPCION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(178863845798294128)
,p_prompt=>'Buscar Opci&oacute;n'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_OPCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.descripcion, a.opcion_id',
'  FROM kseg_e.kseg_opciones a, kseg_e.kseg_opciones_rol b, kdda_e.kdda_objetos c',
' WHERE a.opcion_id = b.opcion_id',
'   AND b.rol_id = :p0_rol   ',
'   AND a.estado = 0',
'   AND b.estado = 0',
'   AND a.objeto_id = c.objeto_id',
'   AND c.tipo_objeto_id = 4',
'   AND c.prefijo IS NOT NULL'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P136_ROL is not null and nvl(:F_POPUP,''N'') = ''N'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'POPUP'
,p_attribute_08=>'CIC'
,p_attribute_10=>'240'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122901072208055679)
,p_name=>'P136_ROL'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(178863845798294128)
,p_prompt=>'Rol'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_new_grid=>true
,p_grid_column=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122901524175055680)
,p_name=>'P136_ROL_DESC'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(178863845798294128)
,p_prompt=>'Rol Desc'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122901859884055680)
,p_name=>'P136_BD'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(178863845798294128)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select emp_bd from asdm_empresas',
'where emp_id = :F_EMP_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122902936008055681)
,p_name=>'P136_ERROR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(355590018415140342)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122903896388055683)
,p_name=>'P136_TREE_ROOT_1'
,p_item_sequence=>30
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122904346602055684)
,p_name=>'P136_TITULO_1'
,p_item_sequence=>40
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'HTML_UNSAFE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122904689231055687)
,p_name=>'P136_OPCION_1'
,p_item_sequence=>10
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Buscar Opci&oacute;n'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_OPCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.descripcion, a.opcion_id',
'  FROM kseg_e.kseg_opciones a, kseg_e.kseg_opciones_rol b, kdda_e.kdda_objetos c',
' WHERE a.opcion_id = b.opcion_id',
'   AND b.rol_id = :p0_rol   ',
'   AND a.estado = 0',
'   AND b.estado = 0',
'   AND a.objeto_id = c.objeto_id',
'   AND c.tipo_objeto_id = 4',
'   AND c.prefijo IS NOT NULL'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P136_ROL is not null and nvl(:F_POPUP,''N'') = ''N'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'POPUP'
,p_attribute_08=>'CIC'
,p_attribute_10=>'240'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122905066706055688)
,p_name=>'P136_ROL_1'
,p_item_sequence=>15
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Rol'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_new_grid=>true
,p_grid_column=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122905524855055688)
,p_name=>'P136_ROL_DESC_1'
,p_item_sequence=>20
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Rol Desc'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122905913025055689)
,p_name=>'P136_BD_1'
,p_item_sequence=>60
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select emp_bd from asdm_empresas',
'where emp_id = :F_EMP_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122906290849055689)
,p_name=>'P136_ERROR_1'
,p_item_sequence=>50
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Error'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122910424186055692)
,p_name=>'pr_start_load_page'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexbeforepagesubmit'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122910852475055693)
,p_event_id=>wwv_flow_imp.id(122910424186055692)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'block_ui();'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122913979950055695)
,p_name=>'pr_load_page'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122915004640055695)
,p_event_id=>wwv_flow_imp.id(122913979950055695)
,p_event_result=>'TRUE'
,p_action_sequence=>5
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CLEAR'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P136_OPCION'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122914458479055695)
,p_event_id=>wwv_flow_imp.id(122913979950055695)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'unblock_ui();'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122906677946055690)
,p_name=>'Expand'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122907180585055690)
,p_event_id=>wwv_flow_imp.id(122906677946055690)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_TREE_EXPAND'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(341110327138688939)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122911267113055693)
,p_name=>'pr_menu_click'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'a.menu_link'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122911803351055693)
,p_event_id=>wwv_flow_imp.id(122911267113055693)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'block_ui();'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122912218338055693)
,p_name=>'pr_btn_link'
,p_event_sequence=>30
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'a.btn_link'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122912679862055694)
,p_event_id=>wwv_flow_imp.id(122912218338055693)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'block_ui();'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122913077024055694)
,p_name=>'da_reset_collections'
,p_event_sequence=>30
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_display_when_cond=>'ROL'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122913639719055694)
,p_event_id=>wwv_flow_imp.id(122913077024055694)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'APEX_COLLECTION.DELETE_ALL_COLLECTIONS_SESSION;'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122907638903055690)
,p_name=>'da_show_hide_report_opcion'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P136_OPCION'
,p_condition_element=>'P136_OPCION'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122908143233055691)
,p_event_id=>wwv_flow_imp.id(122907638903055690)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(309540389059287231)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122908615277055691)
,p_event_id=>wwv_flow_imp.id(122907638903055690)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(309540389059287231)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(122909021860055691)
,p_name=>'AD_REFRESCA'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P136_OPCION'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122910001927055692)
,p_event_id=>wwv_flow_imp.id(122909021860055691)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P136_OPCION, P136_ROL'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(122909514562055692)
,p_event_id=>wwv_flow_imp.id(122909021860055691)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(309540389059287231)
);
wwv_flow_imp.component_end;
end;
/
