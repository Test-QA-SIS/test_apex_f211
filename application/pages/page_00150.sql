prompt --application/pages/page_00150
begin
--   Manifest
--     PAGE: 00150
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>150
,p_tab_set=>'Menu'
,p_name=>'FR_DOCUMENTOS_RELACIONADOS'
,p_step_title=>'FR_DOCUMENTOS_RELACIONADOS'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(41102259738489807)
,p_plug_name=>'Documentos Relacionados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    a.*, ',
'          b.ttr_descripcion transaccion_padre,',
'          c.ttr_descripcion transaccion_hijo,',
'          d.uge_num_sri||''-''||d.pue_id||''-''||LPAD(d.com_numero,7,''0'') factura_padre,',
'          e.uge_num_sri||''-''||e.pue_id||''-''||LPAD(e.com_numero,7,''0'') factura_hijo,',
'          h.per_razon_social||'' ''||h.per_primer_nombre||'' ''||h.per_primer_apellido cliente_padre,',
'          i.per_razon_social||'' ''||i.per_primer_nombre||'' ''||i.per_primer_apellido cliente_hijo,',
'          j.uge_nombre ugestion_padre,',
'          k.uge_nombre ugestion_hijo,',
'          TRUNC(d.com_fecha) fecha_doc_padre,',
'          TRUNC(e.com_fecha) fecha_doc_hijo,',
'          d.com_sec_id com_id_padre,',
'          e.com_sec_id com_id_hijo,',
'          d.com_total_factura total_fac_padre,',
'          e.com_total_factura total_fac_hijo',
'',
'FROM      asdm_relaciones_documentos a,',
'          asdm_tipos_transacciones b,',
'          asdm_tipos_transacciones c,',
'          ven_comprobantes d,',
'          ven_comprobantes e,',
'          asdm_clientes f,',
'          asdm_clientes g,',
'          asdm_personas h,',
'          asdm_personas i,',
'          asdm_unidades_gestion j,',
'          asdm_unidades_gestion k',
'',
'WHERE     b.ttr_id = a.ttr_id_padre',
'AND       c.ttr_id = a.ttr_id_hijo',
'AND       d.com_id = a.doc_id_padre',
'AND       e.com_id = a.doc_id_hijo',
'AND       f.cli_id = d.cli_id',
'AND       g.cli_id = e.cli_id',
'AND       h.per_id = f.per_id',
'AND       i.per_id = g.per_id',
'AND       j.uge_id = d.uge_id',
'AND       k.uge_id = e.uge_id'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(41102355153489807)
,p_name=>'Report 1'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'IVEGA'
,p_internal_uid=>8849203883724881
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41102582508489836)
,p_db_column_name=>'RDO_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Rdo Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'RDO_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41102667452489845)
,p_db_column_name=>'EMP_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41102757295489846)
,p_db_column_name=>'TTR_ID_PADRE'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Ttr Id Padre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41102867518489847)
,p_db_column_name=>'DOC_ID_PADRE'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Doc Id Padre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DOC_ID_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41102976744489847)
,p_db_column_name=>'TTR_ID_HIJO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Ttr Id Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103081015489847)
,p_db_column_name=>'DOC_ID_HIJO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Doc Id Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DOC_ID_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103156581489847)
,p_db_column_name=>'RDO_FECHA_RELACION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'RDO_FECHA_RELACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103282975489848)
,p_db_column_name=>'RDO_OBSERVACIONES'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RDO_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103358135489848)
,p_db_column_name=>'RDO_ESTADO_REGISTRO'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RDO_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103467429489848)
,p_db_column_name=>'TRANSACCION_PADRE'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Transaccion Padre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TRANSACCION_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103581008489848)
,p_db_column_name=>'TRANSACCION_HIJO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Transaccion Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TRANSACCION_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103660224489849)
,p_db_column_name=>'FACTURA_PADRE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Factura Padre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103768953489849)
,p_db_column_name=>'FACTURA_HIJO'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Factura Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103879988489849)
,p_db_column_name=>'CLIENTE_PADRE'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Cliente Padre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41103982867489849)
,p_db_column_name=>'CLIENTE_HIJO'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Cliente Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104080947489850)
,p_db_column_name=>'UGESTION_PADRE'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Agencia Padre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGESTION_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104172828489850)
,p_db_column_name=>'UGESTION_HIJO'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Agencia Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGESTION_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104269586489850)
,p_db_column_name=>'FECHA_DOC_PADRE'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Fecha Padre'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_DOC_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104357236489850)
,p_db_column_name=>'FECHA_DOC_HIJO'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Fecha Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_DOC_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104452696489850)
,p_db_column_name=>'COM_ID_PADRE'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Com Id Padre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104560214489850)
,p_db_column_name=>'COM_ID_HIJO'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Com Id Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104653459489851)
,p_db_column_name=>'TOTAL_FAC_PADRE'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Total Fac.Padre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TOTAL_FAC_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41104766586489851)
,p_db_column_name=>'TOTAL_FAC_HIJO'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Total Fac.Hijo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TOTAL_FAC_HIJO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(41104951505490051)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'88519'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'RDO_FECHA_RELACION:UGESTION_PADRE:FECHA_DOC_PADRE:FACTURA_PADRE:CLIENTE_PADRE:TOTAL_FAC_PADRE:UGESTION_HIJO:FECHA_DOC_HIJO:FACTURA_HIJO:CLIENTE_HIJO:TOTAL_FAC_HIJO:RDO_ESTADO_REGISTRO:'
);
wwv_flow_imp.component_end;
end;
/
