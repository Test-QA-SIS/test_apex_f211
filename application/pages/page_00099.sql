prompt --application/pages/page_00099
begin
--   Manifest
--     PAGE: 00099
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>99
,p_name=>'REIMPRESION NC PRONTO PAGO'
,p_alias=>'REIMPREISION_NC'
,p_step_title=>'REIMPRESION NC PRONTO PAGO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(1795729479780969650)
,p_plug_name=>'Impresion Notas de Credito'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(1837634568096541636)
,p_name=>'Nota de Credito'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT VCO.COM_ID,',
'       VCO.UGE_NUM_SRI || '' - '' || VCO.PUE_NUM_SRI || '' - '' ||',
'       VCO.COM_NUMERO COM_NUMERO,',
'       VCO.CLI_ID,',
'       (SELECT CLI.nombre_completo',
'          FROM V_ASDM_DATOS_CLIENTES CLI',
'         WHERE CLI.cli_id = VCO.CLI_ID',
'           AND CLI.emp_id = VCO.EMP_ID) CLIENTE,',
'       VCO.COM_FECHA,',
'       VCO.COM_TIPO,',
'       VCO.COM_OBSERVACION,',
'       TRX.UGE_ID,',
'       (SELECT VVC.VCO_VALOR_VARIABLE',
'          FROM VEN_VAR_COMPROBANTES VVC',
'         WHERE VVC.COM_ID = VCO.COM_ID',
'           AND VVC.EMP_ID = VCO.EMP_ID',
'           AND VVC.VAR_ID = 3',
'           AND VVC.VCO_ESTADO_REGISTRO = VCO.COM_ESTADO_REGISTRO) VALOR,',
'case when to_char(vco.COM_FECHA,''DD-MM-YYYY'') = to_char(SYSDATE,''DD-MM-YYYY'') then',
'''IMPRIMIR'' ',
'else',
' null',
'--''IMPRIMIR'' ',
'end IMPRIMIR',
'  FROM VEN_COMPROBANTES VCO, ASDM_TRANSACCIONES TRX',
' WHERE TRX.TRX_ID = VCO.TRX_ID',
'   AND TRX.EMP_ID = VCO.EMP_ID',
'   AND VCO.COM_TIPO = ''NC''',
'   AND TRX.TTR_ID = :p99_ttr_id',
'   AND TRX.UGE_ID = :F_UGE_ID',
'  AND VCO.EMP_ID = :F_EMP_ID',
'   AND VCO.COM_ESTADO_REGISTRO = 0',
'  AND VCO.UGE_NUM_SRI = :P99_UGE_NUM_SRI',
'AND VCO.PUE_NUM_SRI = :P99_PUE_NUM_SRI',
'AND VCO.COM_NUMERO = :P99_REIMPRIMIR'))
,p_display_when_condition=>'CARGA_NC'
,p_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838246675116600357)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838246873083600367)
,p_query_column_id=>2
,p_column_alias=>'COM_NUMERO'
,p_column_display_sequence=>3
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838645965728645004)
,p_query_column_id=>3
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838646075203645068)
,p_query_column_id=>4
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>6
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838246976396600367)
,p_query_column_id=>5
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>4
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838247068277600367)
,p_query_column_id=>6
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>7
,p_column_heading=>'Com Tipo'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838247162770600367)
,p_query_column_id=>7
,p_column_alias=>'COM_OBSERVACION'
,p_column_display_sequence=>8
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1837634868820541702)
,p_query_column_id=>8
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838247270054600367)
,p_query_column_id=>9
,p_column_alias=>'VALOR'
,p_column_display_sequence=>9
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1838687679234648909)
,p_query_column_id=>10
,p_column_alias=>'IMPRIMIR'
,p_column_display_sequence=>10
,p_column_heading=>'IMPRIMIR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:99:&SESSION.:REIMPRIME:&DEBUG.::P99_NOTAS_CREDITO:#COM_ID#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(1838408572954618715)
,p_branch_action=>'f?p=&APP_ID.:99:&SESSION.:CARGA_NC:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 02-OCT-2012 12:19 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795729757708969669)
,p_name=>'P99_TTR_DESCRIPCION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'ttr_descripcion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795730060342969676)
,p_name=>'P99_PERIODO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'periodo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795730373937969677)
,p_name=>'P99_DATAFOLIO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'datafolio'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795730553216969678)
,p_name=>'P99_NUEVA_SECUENCIA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'nueva_secuencia'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795730776390969680)
,p_name=>'P99_PUE_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'pue_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795730983152969680)
,p_name=>'P99_TTR_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'ttr_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1795731172951969680)
,p_name=>'P99_NOTAS_CREDITO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'NOTAS DE CREDITO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1803347552290002612)
,p_name=>'P99_PUE_NUM_SRI'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'-'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1803347758378002620)
,p_name=>'P99_UGE_NUM_SRI'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'DIGITE LA NUEVA SECUENCIA A IMPRIMIR'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1803912780650058121)
,p_name=>'P99_SECUENCIA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'-'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>17
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit("VALIDA")'''
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1803912969759058124)
,p_name=>'P99_REIMPRIMIR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(1795729479780969650)
,p_prompt=>'DIGITE LA SECUENCIA QUE DESEA REIMPRIMIR'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit("CARGA_NC")'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P99_SECUENCIA = :P99_NUEVA_SECUENCIA'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(1807150252720333876)
,p_validation_name=>'P99_SECUENCIA'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :P99_SECUENCIA = :P99_NUEVA_SECUENCIA then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'error'
,p_validation_condition=>'VALIDA'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(1803912780650058121)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44291157130441385)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REIMPRESION_NC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'begin',
'',
'ln_tgr_id := pq_car_credito.fn_obtiene_tipo_grupo_trx(pn_emp_id => :f_emp_id,',
'                                                        pn_ttr_id => :p99_ttr_id,',
'                                                        pv_error  => :p0_error);',
'pq_car_provisiones.pr_reimprimir_nc(pn_emp_id          => :f_emp_id,',
'                 pn_com_id          => :p99_notas_credito,',
'                 pn_com_numero      => :p99_secuencia,',
'                 pn_com_numero_ant  => :p99_reimprimir,',
'                 pn_uge_id          => :f_uge_Id,',
'                 pn_uge_id_gasto    => :f_uge_id_gasto,',
'                 pn_usu_id          => :f_user_id,',
'                 pn_pue_id          => :p99_pue_id,',
'                 pv_pue_num_sri     => :p99_pue_num_sri,',
'                 pv_uge_num_est_sri => :p99_uge_num_sri,',
'                 pv_com_tipo        => ''NC'',',
'                 pn_tgr_id          => ln_tgr_id,',
'                 pn_ttr_id          => :p99_ttr_id,',
'                 pn_tnp_id          => 1,',
'                 pv_error           => :p0_error);',
'',
':p99_secuencia := null;',
':p99_reimprimir := null;',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'REIMPRIME'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>12038005860676459
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1796318656934038817)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_folio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'begin',
'',
'ln_tgr_id := pq_car_credito.fn_obtiene_tipo_grupo_trx(pn_emp_id => :f_emp_id,',
'                                                        pn_ttr_id => :p99_ttr_id,',
'                                                        pv_error  => :p0_error);',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'      ln_tgr_id,',
'      :P99_TTR_DESCRIPCION, ',
'      :P99_PERIODO,',
'      :P99_DATAFOLIO,',
'      :P99_NUEVA_SECUENCIA,',
'      :P99_PUE_NUM_SRI  ,',
'      :P99_UGE_NUM_SRI ,',
'      :P99_PUE_ID,',
'      :p99_ttr_id,',
'      :p0_nro_folio,',
'      :P0_ERROR);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>1764065505664273891
);
wwv_flow_imp.component_end;
end;
/
