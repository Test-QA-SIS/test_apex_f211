prompt --application/pages/page_00166
begin
--   Manifest
--     PAGE: 00166
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>166
,p_name=>'Consultar_Json'
,p_alias=>'CONSULTAR-JSON'
,p_page_mode=>'MODAL'
,p_step_title=>'Consultar_Json'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if (!library)',
'   var library = {};',
'',
'library.json = {',
'   replacer: function(match, pIndent, pKey, pVal, pEnd) {',
'      var key = ''<span class=json-key>'';',
'      var val = ''<span class=json-value>'';',
'      var str = ''<span class=json-string>'';',
'      var r = pIndent || '''';',
'      if (pKey)',
'         r = r + key + pKey.replace(/[": ]/g, '''') + ''</span>: '';',
'      if (pVal)',
'         r = r + (pVal[0] == ''"'' ? str : val) + pVal + ''</span>'';',
'      return r + (pEnd || '''');',
'      },',
'   prettyPrint: function(obj) {',
'      var jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;',
'      return JSON.stringify(obj, null, 3)',
'         .replace(/&/g, ''&amp;'').replace(/\\"/g, ''&quot;'')',
'         .replace(/</g, ''&lt;'').replace(/>/g, ''&gt;'')',
'         .replace(jsonLine, library.json.replacer);',
'      }',
'   };',
'',
'',
'',
'try{',
'    var jsonPrev = JSON.parse($v("P166_JSON"));',
'    var dataJsonD = jsonPrev;',
'    $(''#dataJson'').html(library.json.prettyPrint(dataJsonD));',
'} catch(error) {',
'    var jsonPrev = JSON.stringify($v("P166_JSON"));',
'    var dataJsonD = jsonPrev;',
'    $(''#dataJson'').html(library.json.prettyPrint(dataJsonD));',
'}',
'',
''))
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_page_component_map=>'17'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20231213170017'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(683285357883789466)
,p_plug_name=>'Json'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(683285503963789467)
,p_plug_name=>'Data'
,p_parent_plug_id=>wwv_flow_imp.id(683285357883789466)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_source=>'<pre><code id=dataJson></code></pre>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(683285602063789468)
,p_name=>'P166_JSON'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(683285503963789467)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(683285784296789470)
,p_name=>'P166_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(683285503963789467)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
