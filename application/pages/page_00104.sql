prompt --application/pages/page_00104
begin
--   Manifest
--     PAGE: 00104
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>104
,p_name=>'Reverso Refinanciamiento'
,p_step_title=>'Reverso Refinanciamiento'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(48788363404580957)
,p_plug_name=>'Datos Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(48802569824696337)
,p_name=>unistr('Cr\00E9ditos Refinanciados')
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT cxc.cxc_id, cxc.cxc_saldo, cxc.com_id',
'    FROM car_cuentas_por_cobrar cxc',
'   WHERE cxc.cli_id = :p104_cli_id',
'     AND cxc.cxc_estado_registro =0',
'     AND cxc.emp_id = :f_emp_id',
'     AND cxc.cxc_saldo > 0',
'     AND cxc.cxc_estado =''GEN''',
'   ',
'     AND cxc.cxc_id_refin IS NOT NULL',
'order by 1 desc',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT cxc.cxc_id',
'    FROM car_cuentas_por_cobrar cxc',
'   WHERE cxc.cli_id = :p104_cli_id',
'     AND cxc.cxc_estado_registro =0',
'     AND cxc.emp_id = :f_emp_id',
'     AND cxc.cxc_saldo > 0',
'     AND cxc.cxc_estado =''GEN''   ',
'     AND cxc.cxc_id_refin IS NOT NULL'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48803168008714730)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:104:&SESSION.:cargar:&DEBUG.::P104_CXC_ID:#CXC_ID#'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48803254551714732)
,p_query_column_id=>2
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48803365426714732)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Comprobante Original'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(48808973988773276)
,p_plug_name=>'Alertas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>2
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(241392058668229162)
,p_plug_name=>'Comprobantes Generados'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>35
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P104_CXC_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(289809182326496930)
,p_name=>unistr('Nota de D\00E9bito')
,p_parent_plug_id=>wwv_flow_imp.id(241392058668229162)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''AND'''', this.value ,''''P104_UGE_ID'''',''''P104_USER_ID'''',''''P104_EMP_ID'''',''''P104_CLI_ID'''',''''R257556031056732004'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis ',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'')'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'')'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289809460156497030)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289809566661497038)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289809663600497038)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289809761090497038)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289809854333497038)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289809980580497038)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INTMORA'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810083253497038)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>7
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P104_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810182243497038)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>8
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P104_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810260852497038)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>9
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P104_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(289810363064500811)
,p_name=>unistr('Nota de Cr\00E9dito')
,p_parent_plug_id=>wwv_flow_imp.id(241392058668229162)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''ANC'''', this.value ,''''P104_UGE_ID'''',''''P104_USER_ID'''',''''P104_EMP_ID'''',''''P104_CLI_ID'''',''''R257557211794735885'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810667593500812)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810752743500812)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810883185500813)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROBANTE'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289810976514500813)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289811077056500813)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289811170023500813)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INTERES'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289811274498500813)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>7
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P104_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289811371461500813)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>8
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P104_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289811480860500813)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>9
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P104_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(241392174878229163)
,p_plug_name=>unistr('Detalle de Cr\00E9ditos refinanciados')
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P104_CXC_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(48810361006807342)
,p_name=>'Credito a Reversar'
,p_parent_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_id,',
'       (SELECT ede.ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cx.ede_id) tipo_cartera,',
'       cx.cxc_saldo,',
'       cx.com_id,',
'       cx.cxc_fecha_adicion',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cxc_id = :p104_cxc_id'))
,p_display_when_condition=>'P104_CXC_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48817253940070220)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>unistr('Cr\00E9dito')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48820761821091343)
,p_query_column_id=>2
,p_column_alias=>'TIPO_CARTERA'
,p_column_display_sequence=>5
,p_column_heading=>'Tipo Cartera'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48817475900070222)
,p_query_column_id=>3
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48817570862070222)
,p_query_column_id=>4
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Comprobante'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(48817675149070222)
,p_query_column_id=>5
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>4
,p_column_heading=>unistr('Fecha Adici\00F3n')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(241391513898229156)
,p_name=>unistr('Cr\00E9dito seguro refinanciado')
,p_parent_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cse.cxc_id,',
'       (SELECT ede.ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cse.ede_id) tipo_cartera,',
'       cse.cxc_saldo,',
'       cse.cxc_fecha_adicion',
'  FROM car_refinanciamiento_seguro rse,',
'       car_cuentas_por_cobrar      cxc,',
'       car_cuentas_por_cobrar      cse',
' WHERE rse.cxc_id_principal = cxc.cxc_id_refin',
'   AND cxc.cxc_id = :p104_cxc_id',
'   AND rse.cxc_id_credito_nuevo = cse.cxc_id'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cse.cxc_id',
'  FROM car_refinanciamiento_seguro rse,',
'       car_cuentas_por_cobrar      cxc,',
'       car_cuentas_por_cobrar      cse',
' WHERE rse.cxc_id_principal = cxc.cxc_id_refin',
'   AND cxc.cxc_id = :p104_cxc_id',
'   AND rse.cxc_id_credito_nuevo = cse.cxc_id'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241391569277229157)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241391664220229158)
,p_query_column_id=>2
,p_column_alias=>'TIPO_CARTERA'
,p_column_display_sequence=>2
,p_column_heading=>'Tipo cartera'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241391778529229159)
,p_query_column_id=>3
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>3
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241391912058229160)
,p_query_column_id=>4
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>4
,p_column_heading=>unistr('Fecha adici\00F3n')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(48821178098096095)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(48810361006807342)
,p_button_name=>'REVERSAR'
,p_button_static_id=>'REVERSAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Reversar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:104:&SESSION.:REVERSAR:&DEBUG.:::'
,p_button_condition_type=>'NEVER'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(372155952114071354)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(48810361006807342)
,p_button_name=>'PR_REVERSO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535660381046676)
,p_button_image_alt=>'REVERSO'
,p_button_position=>'BOTTOM'
,p_button_condition=>':P104_FECHA_CREDITO=TRUNC(SYSDATE)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(372156957605074301)
,p_branch_name=>'REVERSO'
,p_branch_action=>'f?p=&APP_ID.:104:&SESSION.:REVERSO:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(372155952114071354)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48788972893580996)
,p_name=>'P104_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48789151800580998)
,p_name=>'P104_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48789362963580999)
,p_name=>'P104_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_prompt=>'Telofono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48789573299580999)
,p_name=>'P104_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48789768882581000)
,p_name=>'P104_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48789954779581001)
,p_name=>'P104_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P104_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P104_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48790171937581001)
,p_name=>'P104_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48790379610581002)
,p_name=>'P104_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48790578463581002)
,p_name=>'P104_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48809572388782219)
,p_name=>'P104_ALERTA_1'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(48808973988773276)
,p_item_default=>unistr('Solo se puede REVERSAR refinanciamientos en el mismo d\00EDa que se realizan')
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:40;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48810952826814460)
,p_name=>'P104_CXC_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48983152551870128)
,p_name=>'P104_X'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_use_cache_before_default=>'NO'
,p_item_default=>':F_USER_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(209802356017338809)
,p_name=>'P104_VALIDA_PUNTO'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289607959955059518)
,p_name=>'P104_COM_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Com Id'
,p_source=>'SELECT cxc.com_id from car_cuentas_por_cobrar cxc WHERE cxc.cxc_id=:p104_cxc_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289608656887121671)
,p_name=>'P104_VALOR_NC'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Nc'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C.COM_TOTAL_FACTURA FROM ven_comprobantes c WHERE c.com_id_ref=:P104_COM_ID',
'AND C.COM_TIPO = ''NC''',
'       AND C.TTR_ID=PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_ttr_id_nota_credito_int_pc'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289608975852128150)
,p_name=>'P104_VALOR_ND'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Nd'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C.COM_TOTAL_FACTURA FROM ven_comprobantes c WHERE c.com_id_ref=:P104_COM_ID AND c.com_tipo=''ND''',
'AND C.TTR_ID=PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0, ''cn_ttr_id_nota_debito_refin'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289827560312864260)
,p_name=>'P104_UGE_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289835081434869672)
,p_name=>'P104_USER_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_source=>'F_USER_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289835675611872395)
,p_name=>'P104_EMP_ID'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(48788363404580957)
,p_source=>'F_EMP_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(325510778080346561)
,p_name=>'P104_CXC_ID_REF'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cxc Id Ref'
,p_source=>'SELECT R.CXC_ID_REFIN FROM CAR_CUENTAS_POR_COBRAR R WHERE R.CXC_ID=:P104_CXC_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(338081757688543726)
,p_name=>'P104_PUE_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(338113981469520281)
,p_name=>'P104_PUE_NUM_SRI'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(338114271118525079)
,p_name=>'P104_UGE_NUM_EST_SRI'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(347489174501973913)
,p_name=>'P104_FECHA_CREDITO'
,p_item_sequence=>115
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Fecha Cr\00E9dito')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_fecha_adicion',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cxc_id = :p104_cxc_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(347493855894058529)
,p_name=>'P104_ALERTA'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(241392174878229163)
,p_item_default=>'''SOLO SE PUEDE REALIZAR EL REVERSO EL MISMO DIA DEL REFINANCIAMIENTO'''
,p_prompt=>'Alerta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>4000
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P104_FECHA_CREDITO!=TRUNC(SYSDATE)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(48792258864626984)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_cliente_existe      VARCHAR2(100);',
'  ln_dir_id              asdm_direcciones.dir_id%TYPE;',
'  lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'  ln_cli_id_referido     asdm_clientes.cli_id%TYPE;',
'BEGIN',
'  pq_ven_listas2.pr_datos_clientes(:f_uge_id,',
'                                  :f_emp_id,',
'                                  :p104_identificacion,',
'                                  :p104_nombre,',
'                                  :p104_cli_id,',
'                                  :p104_correo,',
'                                  :p104_direccion,',
'                                  :p104_tipo_direccion,',
'                                  :p104_telefono,',
'                                  :p104_tipo_telefono,',
'                                  lv_cliente_existe,',
'                                  ln_dir_id,',
'                                  ln_cli_id_referido,',
'                                  lv_nombre_cli_referido,',
'                                  :p104_tipo_identificacion,',
'                                  :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>16539107594862058
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(48951561902522777)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reversa'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'',
'',
'  pq_car_anulacion_refinan.pr_reverso_refinanciamiento(pn_cxc_id => :P104_CXC_ID,',
'                                                          pn_emp_id => :f_emp_id,',
'                                                          pn_uge_id => :f_uge_id,',
'                                                          pn_usu_id => :F_USER_ID,',
'                                                          pn_uge_id_gasto => :F_UGE_ID_GASTO,',
'                                                          pn_cli_id => :P104_CLI_ID,',
'                                                          pn_tse_id => :f_seg_id,',
'                                                          PN_AGE_ID_AGENCIA  =>  :f_age_id,',
'                                                          PV_PUE_NUM_SRI     => :P0_PUE_NUM_SRI,',
'                                                          PV_UGE_NUM_EST_SRI =>  :P0_UGE_NUM_EST_SRI,',
'                                                          PN_PUE_ID         => :p104_pue_id,',
'                                                          pv_error => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(48821178098096095)
,p_process_when_type=>'NEVER'
,p_internal_uid=>16698410632757851
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(289607563722029846)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_car_anulacion_refinan.pr_cargar_datos(pn_user_id => :f_user_id,',
'                                           pn_uge_id => :f_uge_id,',
'                                           pn_emp_id => :f_emp_id,',
'                                           pn_cxc_id => :p104_cxc_id,',
'                                           pn_cli_id => :p104_cli_id,',
'                                           pn_pue_id => :p104_pue_id,',
'                                           pv_error => :p0_error);',
'',
'',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri into',
':P104_PUE_NUM_SRI,:P104_UGE_NUM_EST_SRI',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = :p104_pue_id;',
'',
':P104_VALIDA_PUNTO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id =>:p104_pue_id);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cargar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>257354412452264920
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(338096065448743408)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REVERSAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  -- Call the procedure',
'  IF :p104_fecha_credito = trunc(SYSDATE) THEN',
'  ',
'    pq_car_anulacion_refinan.pr_reverso_refinanciamiento(pn_cxc_id          => :p104_cxc_id,',
'                                                         pn_emp_id          => :f_emp_id,',
'                                                         pn_uge_id          => :f_uge_id,',
'                                                         pn_usu_id          => :f_user_id,',
'                                                         pn_uge_id_gasto    => :f_uge_id_gasto,',
'                                                         pn_cli_id          => :p104_cli_id,',
'                                                         pn_tse_id          => :f_seg_id,',
'                                                         pn_age_id_agencia  => :f_age_id_agencia,',
'                                                         pv_pue_num_sri     => :p104_pue_num_sri,',
'                                                         pv_uge_num_est_sri => :p104_uge_num_est_sri,',
'                                                         pn_pue_id          => :p104_pue_id,',
'                                                         pv_error           => :p0_error);',
'  ',
'    IF apex_collection.collection_exists(p_collection_name => ''COL_ND_REFINANCIAMIENTO'') THEN',
'    ',
'      apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                ''cv_col_nd_refinanciamiento''));',
'    ',
'    END IF;',
'',
'    IF apex_collection.collection_exists(p_collection_name => ''COL_NC_REFINANCIAMIENTO'') THEN',
'    apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                              ''cv_col_nc_refinanciamiento''));',
'    END IF;',
'    :p104_cxc_id        := NULL;',
'    :p104_com_id        := NULL;',
'    :p104_valor_nc      := NULL;',
'    :p104_valor_nd      := NULL;',
'    :p104_cxc_id_ref    := NULL;',
'    :p104_fecha_credito := NULL;',
'  ',
'  ELSE',
'    :p0_error := ''SOLO SE PUEDE REALIZAR EL REVERSO EL MISMO DIA QUE SE HACE EL REFINANCIAMIENTO'';',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'REVERSO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>305842914178978482
);
wwv_flow_imp.component_end;
end;
/
