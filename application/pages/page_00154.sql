prompt --application/pages/page_00154
begin
--   Manifest
--     PAGE: 00154
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>154
,p_name=>'Cuotas gratis matriz'
,p_step_title=>'Cuotas gratis matriz'
,p_autocomplete_on_off=>'OFF'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript1.1" type="text/javascript">',
'',
'function carga_cuotas(div_id)',
'{    ',
'    var ajaxResult= new htmldb_Get(null,$x(''pFlowId'').value,''APPLICATION_PROCESS=PR_CARGA_CUOTA'',0);',
'        ajaxResult.add(''P1'',div_id);',
'    ',
'    var v_result_xml = ajaxResult.get(); ',
'    var error_plsql = v_result_xml.indexOf(''Error'');   ',
'',
'',
'    if (error_plsql > 0 ) {',
'        alert(v_result_xml);',
'    }',
'    ',
'    //jQuery(''#R38454175733736091514_ir'').data(''apex-interactiveReport'').refresh ();    ',
'    //apex.jQuery(''#R38419303825032878029'').trigger(''apexrefresh'');',
'    //apex.jQuery(''#R44785275878003620109'').trigger(''apexrefresh'');   ',
'    $(''#lis_creditos'').trigger(''apexrefresh'');',
'    ',
'    //apex.jQuery(''#''+id_grilla2).trigger(''apexrefresh'');',
'} ',
'    ',
'function baja_cuotas(div_id)',
'{    ',
'    var ajaxResult= new htmldb_Get(null,$x(''pFlowId'').value,''APPLICATION_PROCESS=PR_BAJA_CUOTA'',0);',
'        ajaxResult.add(''P1'',div_id);',
'    ',
'    var v_result_xml = ajaxResult.get(); ',
'    var error_plsql = v_result_xml.indexOf(''Error'');   ',
'',
'',
'    if (error_plsql > 0 ) {',
'        alert(v_result_xml);',
'    }',
'    ',
'    //jQuery(''#R38454175733736091514_ir'').data(''apex-interactiveReport'').refresh ();    ',
'    //apex.jQuery(''#R38419303825032878029'').trigger(''apexrefresh'');',
'    //apex.jQuery(''#R44785275878003620109'').trigger(''apexrefresh'');   ',
'    //apex.jQuery(''#''+id_grilla2).trigger(''apexrefresh'');',
'    $(''#lis_creditos'').trigger(''apexrefresh'');',
'}',
'    ',
'</script>',
'',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(117974448096686313439)
,p_name=>'ORDENES CUOTA COMODIN'
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.occ_id,',
'       p.per_tipo_identificacion,',
'       p.per_nro_identificacion,',
'       trim(p.per_primer_apellido||'' ''||p.per_segundo_apellido||'' ''||p.per_primer_nombre||'' ''||p.per_segundo_nombre||'' ''||p.per_razon_social) nombres,',
'       a.cxc_id,',
'       a.div_nro_vencimiento,',
'       kseg_p.pq_kseg_devuelve_datos.fn_nombre_usuario(a.occ_usu_id_genera) usuario_contac,',
'       a.occ_fecha_genera fecha',
'  from car_orden_cuota_comodin a, asdm_clientes b, asdm_personas p',
'  where p.per_id = b.per_id',
'  and b.cli_id = a.cli_id',
'  AND a.uge_id = :F_UGE_ID',
'  and occ_estado is null',
'  and com_id_nc is null'))
,p_display_when_condition=>'F_UGE_ID'
,p_display_when_cond2=>'1601'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
,p_comment=>'1601 -- uge del contac center'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878544885589373148)
,p_query_column_id=>1
,p_column_alias=>'OCC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Occ id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878545300190373148)
,p_query_column_id=>2
,p_column_alias=>'PER_TIPO_IDENTIFICACION'
,p_column_display_sequence=>2
,p_column_heading=>'Per tipo identificacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878545686534373149)
,p_query_column_id=>3
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>3
,p_column_heading=>'Per nro identificacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878546083362373149)
,p_query_column_id=>4
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>4
,p_column_heading=>'Nombres'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878546508457373149)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878546949589373150)
,p_query_column_id=>6
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Div nro vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878547263896373150)
,p_query_column_id=>7
,p_column_alias=>'USUARIO_CONTAC'
,p_column_display_sequence=>7
,p_column_heading=>'Usuario contac'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878547703927373150)
,p_query_column_id=>8
,p_column_alias=>'FECHA'
,p_column_display_sequence=>8
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133208772918465571336)
,p_plug_name=>'New'
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P154_CLI_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(156297788445236250932)
,p_name=>unistr('Cr\00E9ditos del cliente')
,p_region_name=>'lis_creditos'
,p_parent_plug_id=>wwv_flow_imp.id(133208772918465571336)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT p.com_id,',
'       p.cxc_id,',
'       p.div_id,',
'       p.div_nro_vencimiento,',
'       p.div_fecha_vencimiento,',
'       p.div_valor_cuota,',
'       p.div_saldo_cuota,',
'       p.div_valor_pago,',
'       p.int_mora,',
'       p.div_mora_pagada,',
'       p.gto_cob,',
'       p.div_gasto_pagado,',
'       p.respaldo_cheques,',
'       p.ede_id,',
'       p.div_descuento_x_rol,',
'       p.uge_id,',
'       p.cli_id,',
'       apex_item.checkbox(10,',
'                          p.div_id,',
'                          p_attributes => ''onClick="baja_cuotas(this.value)"'') eliminar,',
'       (select d.ede_descripcion',
'       from asdm_entidades_destinos d',
'       where d.ede_id = p.ede_id) cartera,',
'       case when p.div_fecha_vencimiento < trunc(sysdate) then',
'        ''red''--''#FF7755''',
'       end color,',
'       ''Aplicar cuota gratis'' cuota_gratis',
'       ',
'   FROM car_cuotas_pagar p,',
'       (select p.cxc_id, min(p.div_nro_vencimiento) nro_vencimiento',
'          FROM car_cuotas_pagar p',
'         WHERE p.cli_id = :p154_cli_Id',
'           and p.usu_id = :f_user_id',
'         group by p.cxc_id) a',
' WHERE p.cli_id = :p154_cli_Id',
'   and p.usu_id = :f_user_id',
'   and p.cxc_id = a.cxc_id',
'   and p.div_nro_vencimiento = a.nro_vencimiento ',
' order by p.cxc_id, p.div_nro_vencimiento;'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878485547850372977)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878485919730372982)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:143:&SESSION.::&DEBUG.:RP:P143_CXC_ID:#CXC_ID#'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878486270031372982)
,p_query_column_id=>3
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878486674247372983)
,p_query_column_id=>4
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_NRO_VENCIMIENTO#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878487113705372983)
,p_query_column_id=>5
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'Fecha Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_FECHA_VENCIMIENTO#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878487492693372984)
,p_query_column_id=>6
,p_column_alias=>'DIV_VALOR_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_VALOR_CUOTA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878487889744372984)
,p_query_column_id=>7
,p_column_alias=>'DIV_SALDO_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_SALDO_CUOTA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878488348842372984)
,p_query_column_id=>8
,p_column_alias=>'DIV_VALOR_PAGO'
,p_column_display_sequence=>8
,p_column_heading=>'Cuota a Pagar'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:14;">#DIV_VALOR_PAGO#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878488710673372985)
,p_query_column_id=>9
,p_column_alias=>'INT_MORA'
,p_column_display_sequence=>9
,p_column_heading=>'Int Mora'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#INT_MORA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878489124585372985)
,p_query_column_id=>10
,p_column_alias=>'DIV_MORA_PAGADA'
,p_column_display_sequence=>10
,p_column_heading=>'Mora a Pagar'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:14;">#DIV_MORA_PAGADA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878489491996372985)
,p_query_column_id=>11
,p_column_alias=>'GTO_COB'
,p_column_display_sequence=>11
,p_column_heading=>'Gto Cobranza'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#GTO_COB#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878489933687372986)
,p_query_column_id=>12
,p_column_alias=>'DIV_GASTO_PAGADO'
,p_column_display_sequence=>12
,p_column_heading=>'Gto a Pagar'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span style="font-weight:bold;font-size:14;">#DIV_GASTO_PAGADO#</span>',
'',
''))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878490286965372986)
,p_query_column_id=>13
,p_column_alias=>'RESPALDO_CHEQUES'
,p_column_display_sequence=>13
,p_column_heading=>'Respaldo Cheques'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878490725909372986)
,p_query_column_id=>14
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878491134185372987)
,p_query_column_id=>15
,p_column_alias=>'DIV_DESCUENTO_X_ROL'
,p_column_display_sequence=>15
,p_column_heading=>'Descuento Rol'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878491452653372987)
,p_query_column_id=>16
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878491881001372988)
,p_query_column_id=>17
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878492314695372989)
,p_query_column_id=>18
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878492713491372989)
,p_query_column_id=>19
,p_column_alias=>'CARTERA'
,p_column_display_sequence=>19
,p_column_heading=>'Cartera'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878493127158372989)
,p_query_column_id=>20
,p_column_alias=>'COLOR'
,p_column_display_sequence=>20
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117875235356856527545)
,p_query_column_id=>21
,p_column_alias=>'CUOTA_GRATIS'
,p_column_display_sequence=>21
,p_column_heading=>'Cuota gratis'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:154:&SESSION.:CUOTA_GRATIS:&DEBUG.:RP:P154_CXC_ID,P154_DIV_ID:#CXC_ID#,#DIV_ID#'
,p_column_linktext=>'#CUOTA_GRATIS#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156338404310636839407)
,p_plug_name=>'TOTALES A PAGAR'
,p_parent_plug_id=>wwv_flow_imp.id(133208772918465571336)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133208772975182571337)
,p_plug_name=>'DATOS DEL CLIENTE'
,p_region_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="font-size:14;font-family:Tahoma"'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_2'
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from asdm_unidades_gestion u where u.uge_matriz=''S'' and u.emp_id=:F_EMP_ID and u.tug_id=1',
'and u.uge_id=:f_uge_id;'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(152345141711785660225)
,p_plug_name=>'Datos del Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(133208772975182571337)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156371845807971846539)
,p_plug_name=>'INFORMATIVO'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525167944046669)
,p_plug_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_2'
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P154_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(162923315980428868715)
,p_name=>'Creditos Negociados'
,p_template=>wwv_flow_imp.id(270525167944046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_2'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (CASE',
'         WHEN COUNT(*) > 0 and MAX(ed.ede_id) !=809 THEN',
'          ''El cliente tiene el credito: '' || cx.cxc_id || '' Negociado con '' ||',
'          ed.ede_descripcion ||',
'          '', los cobros en caja se podran realizar hasta '' ||',
'          (ne.neg_fecha_recepcion +',
'          pq_car_negociaciones.fn_ret_valor_parametro_ede(pn_ede_id => ne.ede_id,',
'                                                           pn_par_id => pq_constantes.fn_retorna_constante(0,',
'                                                                                                           ''cn_par_id_num_dias_cobro_neg_mor'')))',
'       ',
'          when COUNT(*) > 0 and MAX(ed.ede_id) =809 then ',
'          ''El cliente tiene el credito: '' || cx.cxc_id || '' Negociado con '' ||',
'          ed.ede_descripcion ||',
unistr('          '', no se puede realizar ning\00FAn tipo de cobro '' '),
'       ',
'       END) ALERTA',
'  FROM car_cuentas_por_cobrar  cx,',
'       CAR_CXC_NEGOCIACIONES   CN,',
'       CAR_NEGOCIACIONES       NE,',
'       asdm_entidades_destinos ed',
' WHERE EXISTS',
'       (SELECT ''x''',
'          FROM asdm_E.Car_Tipo_Negociacion_Entidad e',
'         WHERE ctn_id =',
'               pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'           AND e.cte_estado_registro = 0',
'           AND e.ede_id = cx.ede_id)',
'   AND cx.cxc_saldo > 0',
'   AND cx.cxc_estado = ''GEN''',
'   AND CX.CXC_ESTADO_REGISTRO = 0',
'   AND cne_estado_cxc = ''NEGOCIADO''',
'   AND CLI_ID = :P154_CLI_ID',
'   AND CX.CXC_ID = CN.CNE_CXC_ID',
'   AND CN.NEG_ID = NE.NEG_ID',
'   AND cx.ede_id = ed.ede_id',
' GROUP BY cx.cxc_id, ne.neg_fecha_recepcion, ed.ede_descripcion, ne.ede_id'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P154_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_row_count_max=>500
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117878544168040373145)
,p_query_column_id=>1
,p_column_alias=>'ALERTA'
,p_column_display_sequence=>1
,p_column_heading=>'ALERTA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<font color=RED>#ALERTA#</font>'
,p_heading_alignment=>'LEFT'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117878515477901373099)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_button_name=>'LIMPIAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'LIMPIAR TODO'
,p_button_alignment=>'LEFT-CENTER'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117875235585433527547)
,p_name=>'P154_CXC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(156338404310636839407)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117875235670567527548)
,p_name=>'P154_DIV_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(156338404310636839407)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878516726182373101)
,p_name=>'P154_NRO_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_prompt=>'Nro identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.per_nro_identificacion|| '' - '' ||TRIM(b.per_primer_apellido || '' '' || b.per_segundo_apellido || '' '' ||',
'            b.per_primer_nombre || '' '' || b.per_segundo_nombre || '' '' ||',
'            b.per_razon_social) "Nombre Cliente",',
'       b.per_nro_identificacion identificacion',
'  FROM asdm_clientes a, asdm_personas b',
' WHERE b.per_id = a.per_id',
'   AND b.per_estado_registro = 0',
'   AND a.cli_estado_registro = 0',
'   AND a.emp_id = :F_EMP_ID',
'   and b.emp_id = :F_EMP_ID'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878517105935373102)
,p_name=>'P154_NOMBRE_CLIENTE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_tag_attributes=>'readonly'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878517542259373102)
,p_name=>'P154_CLI_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_prompt=>'Cli id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_tag_attributes=>'readonly'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878517915764373102)
,p_name=>'P154_NOMBRE_INSTITUCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_prompt=>'Nombre institucion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'readonly'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878518269852373103)
,p_name=>'P154_PER_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878518731706373103)
,p_name=>'P154_INS_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878519533401373104)
,p_name=>'P154_COBRADOR'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_prompt=>'Cobrador'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878520304378373104)
,p_name=>'P154_MENSAJE_ANTICIPO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_use_cache_before_default=>'NO'
,p_source=>'APLICAR EL PAGO CON ANTICIPO DE CLIENTES'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'NVL(:P154_TOTAL_ANTICIPOS,0) > 0  and :P154_CLI_ID is not null and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878520678653373105)
,p_name=>'P154_SALDO_TOTAL'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878521089860373105)
,p_name=>'P154_MORA_TOTAL'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878521494168373105)
,p_name=>'P154_GCOB_TOTAL'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878521923464373106)
,p_name=>'P154_ALERTA_SEGURO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) =',
'              extract(MONTH FROM SYSDATE) THEN',
'          ''CLIENTE CON SEGURO POR RENOVAR''',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) <',
'              extract(MONTH FROM SYSDATE) AND a.dse_saldo_cuota > 0 THEN',
'          ''CLENTE CON SEGURO CADUCADO PENDIENTE DE PAGO''',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) <',
'              extract(MONTH FROM SYSDATE) AND a.dse_saldo_cuota = 0 THEN',
'          ''CLENTE CON SEGURO CADUCADO''',
'       END texto',
'  FROM car_dividendos_seguros a, car_cuentas_seguros b',
' WHERE b.cse_id = a.cse_id',
'   AND b.cse_plazo = a.dse_nro_vencimiento',
'   AND a.dse_fecha_vencimiento <= LAST_DAY(SYSDATE)',
'   AND b.ese_id = ''ENV''',
'   and b.cli_id = :p154_cli_Id',
'   and rownum = 1'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:16;color:GREEN;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878522283225373106)
,p_name=>'P154_ACUERDO_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(152345141711785660225)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MAX(c.acp_id) ',
'FROM car_acuerdos_pago_cab c ',
'WHERE c.cli_id = :P154_CLI_ID',
'AND   c.acp_estado_registro = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':P154_CLI_ID IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117878534356399373126)
,p_name=>'P154_CONTACTOS_NEG'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(156371845807971846539)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:15;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117878557621173373207)
,p_name=>'ad_nro_identificacion'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P154_NRO_IDENTIFICACION'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(288746637321824631)
,p_event_id=>wwv_flow_imp.id(117878557621173373207)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.cli_id,',
'       a.per_id',
'INTO :P154_CLI_ID,',
'     :P154_PER_ID',
'  FROM asdm_clientes a, asdm_personas b',
' WHERE b.per_id = a.per_id',
'   AND b.per_estado_registro = 0',
'   AND a.cli_estado_registro = 0',
'   AND a.emp_id = :F_EMP_ID',
'   and b.emp_id = :F_EMP_ID',
'   and b.per_nro_identificacion = :P154_NRO_IDENTIFICACION;'))
,p_attribute_02=>'P154_NRO_IDENTIFICACION'
,p_attribute_03=>'P154_CLI_ID,P154_PER_ID'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878558126839373210)
,p_event_id=>wwv_flow_imp.id(117878557621173373207)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'DATOS_CLIENTE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117878558523051373214)
,p_name=>'ad_actualiza'
,p_event_sequence=>30
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.lv_total'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878558971552373215)
,p_event_id=>wwv_flow_imp.id(117878558523051373214)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'ACTUALIZA'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117878559369338373215)
,p_name=>'ad_limpia'
,p_event_sequence=>40
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(117878515477901373099)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878559865782373216)
,p_event_id=>wwv_flow_imp.id(117878559369338373215)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota_dml.pr_del_cuotas_pagar(pn_cli_id => :p154_cli_id,',
'                    pn_usu_id => :f_user_id,',
'                    pv_error  => :p0_error);',
''))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878560405437373217)
,p_event_id=>wwv_flow_imp.id(117878559369338373215)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'LIMPIAR'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117878561669480373218)
,p_name=>'ad_total'
,p_event_sequence=>60
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.lv_total_general'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878562238976373218)
,p_event_id=>wwv_flow_imp.id(117878561669480373218)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'ACTUALIZA_TOTAL'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117878562615094373218)
,p_name=>'ad_cxc_sel'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P154_CXC_SEL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878563091465373219)
,p_event_id=>wwv_flow_imp.id(117878562615094373218)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'pq_car_pago_cuota.pr_cxc_seleccionados(:p154_cxc_sel)'
,p_attribute_02=>'P154_CXC_SEL'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117878563514993373221)
,p_name=>'ad_mora_cond'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P154_INT_MORA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117878564025831373222)
,p_event_id=>wwv_flow_imp.id(117878563514993373221)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P154_MORA_COND'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round((SUM(int_mora) - (SUM(int_mora) * (:p154_int_mora / 100))), 2) mora',
'  FROM car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p154_cli_id',
''))
,p_attribute_07=>'P154_INT_MORA'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878552793219373197)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_vencidos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_contac_center.pr_impresion_cartera(pn_emp_id => :f_emp_id,',
'                                          pn_cli_id => :p154_cli_id,',
'                                          pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'IMP_VENCIDOS'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117846299641949608271
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878556779664373205)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_control'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'if apex_collection.collection_exists(''CO_MOV_CAJA'') then',
'apex_collection.delete_collection(''CO_MOV_CAJA'');',
'end if;',
'delete from asdm_e.CAR_DIV_CONTROL WHERE USU_ID = :f_user_id;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>117846303628394608279
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878553632322373200)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_emision_seguro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  pr_url(--pn_app_origen => null,',
'         pn_app_destino => 242,',
'         --pn_pag_origen => null,',
'         pn_pag_destino => 54,',
'         --pv_sesion => :SESSION,',
'         pv_request => ''CLIORDEN'',',
'         pv_parametros => ''P54_CLI_ID,P54_CLASIFICACION,P54_ORIGEN'',',
'         pv_valores => :P154_CLI_ID||'',''||:P154_CSE_ID||'',''||''P'');',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'IR_SEGURO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117846300481052608274
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878557230156373205)
,p_process_sequence=>50
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_sol_cupo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_car_imprimir.pr_imprimir_doc_pago_cuota (pn_cxc_id => :p154_cxc_id_ref,',
'                                            pn_cli_id => :p154_cli_id,',
'                                            pn_emp_id => :f_emp_id,',
'                                            PV_ERROR => :p0_error);',
'',
'end;  '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'solicitud_cupo'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117846304078886608279
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117875235495033527546)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_aplica_cuota_gratis_matriz'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_age_id_agente number := pq_car_credito_interfaz.fn_retorna_agente_conectado(:f_user_id,',
'                                                                                 :f_emp_id);',
'',
'begin',
'  if ln_age_id_agente is not null then',
'    pq_car_promociones.pr_aplica_cuota_gratis_matriz(pn_emp_id         => :f_emp_id,',
'                                                     pn_uge_id         => :f_uge_id,',
'                                                     pn_usu_id         => :f_user_id,',
'                                                     pn_cxc_id         => :p154_cxc_id,',
'                                                     pn_div_id         => :p154_div_id,',
'                                                     pn_age_id_agente  => ln_age_id_agente,',
'                                                     pn_age_id_agencia => :f_age_id_agencia,',
'                                                     pn_seg_id         => :f_seg_id,',
'                                                     pv_error          => :p0_error);',
'  ',
'  ',
'  :P154_NRO_IDENTIFICACION:=null;',
'  :P154_NOMBRE_CLIENTE:=null;',
'  :P154_CLI_ID:=null;',
'  :P154_NOMBRE_INSTITUCION:=null;',
'  :P154_COBRADOR:=null;',
'  else',
'    :p0_error := ''No esta registrado como agente agente'';',
'    raise_application_error(-20000,''No esta registrado como agente agente'');',
'  end if;',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CUOTA_GRATIS'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117842982343763762620
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878553158699373200)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota.pr_carga_info_cliente(pn_emp_id      => :F_EMP_ID,',
'                                        pn_cli_id      => :P154_CLI_ID,',
'                                        pn_uge_id      => :F_UGE_ID,',
'                                        pn_tse_id      => :F_SEG_ID,',
'                                        pn_user_id     => :f_user_id,',
'                                        pn_ins_id      => :P154_INS_ID,',
'                                        pv_nombre_inst => :P154_NOMBRE_INSTITUCION,',
'                                        pv_cobrador    => :P154_COBRADOR,    ',
'                                        pn_cxc_id_ref => :P154_CXC_ID_REF,',
'                                        pv_error       => :P0_ERROR);',
'',
'',
':P154_CXC_SEL :=NULL;',
'',
'IF apex_collection.collection_exists(''COL_CXC_SELECCIONADOS'') then',
'apex_collection.delete_collection(''COL_CXC_SELECCIONADOS'');',
'end if;',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'DATOS_CLIENTE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117846300007429608274
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878555650549373202)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_act_datos_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota.pr_act_pagos(pn_cli_id => :p154_cli_Id,',
'                               pn_usu_id => :f_user_id,',
'                               pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ACTUALIZA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117846302499279608276
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878554379226373201)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'pr_limpia'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117878515477901373099)
,p_internal_uid=>117846301227956608275
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878554786084373202)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pr'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota_dml.pr_del_cuotas_pagar(pn_cli_id => :p154_cli_id,',
'                    pn_usu_id => :f_user_id,',
'                    pv_error  => :p0_error);',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117878515477901373099)
,p_process_when_type=>'NEVER'
,p_internal_uid=>117846301634814608276
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117878556007094373203)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_alertas_neg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_ede_id_ars NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                             ''cn_ede_id_ars'');',
'',
'  ln_ede_id_recsa NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                               ''cn_ede_id_recsa'');',
'',
'  ln_ede_id_masoluc2 NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                  ''cn_ede_id_masoluc2'');',
'',
'  ln_ede_id_projuridico NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_ede_id_projuridico'');',
'',
'  ln_num_ars         NUMBER;',
'  ln_num_recsa       NUMBER;',
'  ln_num_masoluc2    NUMBER;',
'  ln_num_projuridico NUMBER;',
'  ln_num_finsolred   NUMBER;',
'',
'  ln_ede_id_finsolred number := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                          ''cn_ede_id_finsolred'');',
'',
'BEGIN',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_finsolred',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_finsolred',
'     AND cx.cli_id = :P154_CLI_ID;',
'',
'  IF ln_num_finsolred > 0 THEN',
'  ',
'    :P154_CONTACTOS_NEG := ''Oficina 072833000    EXT 101 Ma Alejandra Perez '' ||',
'                         chr(13) || ''',
'                          Direccion:           Padre Aguirre 9 66 y Gran Colombia  '' ||',
'                         chr(13) || ''',
'                          Francisco Andrade    0998971593 '' ||',
'                         chr(13) || ''',
'                          Priscila Flores      0999062357'' ||',
'                         chr(13);',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_masoluc2',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_masoluc2',
'     AND cx.cli_id = :P154_CLI_ID;',
'',
'  IF ln_num_masoluc2 > 0 THEN',
'  ',
'    :P154_CONTACTOS_NEG := ''En los creditos negociados con RECUMPS se aceptara como abono a la cuenta cualquier valor que el cliente entregue,'' ||',
'                         chr(13) || ''',
'                        sin considerar si este se aplica a la cuota o como interes de mora.'';',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_ars',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_ars',
'     AND cx.cli_id = :P154_CLI_ID;',
'',
'  -- raise_application_Error(-20000, ''aquiiii'' || ln_num_ars);',
'',
'  IF ln_num_ars > 0 THEN',
'  ',
unistr('    :P154_CONTACTOS_NEG := ''Cr\00E9dito  vendido a ARS del Ecuador, por favor contactar al  personal de servicio al cliente en las  oficinas de Quito (Av. Amazonas 2248 y Ram\00EDrez D\00E1valos) '' ||'),
'                         chr(13) || ''',
unistr('                        y Guayaquil (Urdesa Central, Calle diagonal No.417C y V\00EDctor Emilio Estrada Of. 204). Tel\00E9fonos: 02 3815160, 1800 \2013 CARTERA. Contacto: Patricia Rodr\00EDguez'';'),
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_recsa',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_recsa',
'     AND cx.cli_id = :P154_CLI_ID;',
'',
'  IF ln_num_recsa > 0 AND ln_num_ars > 0 THEN',
'  ',
unistr('    :P154_CONTACTOS_NEG := ''Cr\00E9dito  vendido a ARS del Ecuador, por favor contactar al  personal de servicio al cliente en las  oficinas de Quito (Av. Amazonas 2248 y Ram\00EDrez D\00E1valos) '' ||'),
'                         chr(13) || ''',
unistr('                        y Guayaquil (Urdesa Central, Calle diagonal No.417C y V\00EDctor Emilio Estrada Of. 204). Tel\00E9fonos: 02 3815160, 1800 \2013 CARTERA. Contacto: Patricia Rodr\00EDguez'';'),
'  ',
'    :P154_CONTACTOS_NEG := :P154_CONTACTOS_NEG || chr(13) || chr(13) ||',
'                        ',
unistr('                         ''Cr\00E9dito vendido a COBRANZAS DEL ECUADOR (RECSA), por favor contactarse con el personal de servicio al cliente'' ||'),
'                         chr(13) || chr(13) ||',
'                         ''QUITO Katy Egas kegas@recsa.com.ec (02) 2999-800 / 2983-900  EXT. 9822'' ||',
'                         chr(13) ||',
'                         ''CUENCA  Thelmo Guzman tguzman@recsa.com.ec (07)2820858  '' ||',
'                         chr(13) ||',
unistr('                         ''AMBATO  Martha Proa\00F1o mproano@recsa.com.ec (03) 2822-883 / 2822-366  EXT. 6081 '' ||'),
'                         chr(13) ||',
'                         ''GUAYAQUIL Michel Onofre donofre@recsa.com.ec (04) 6011-380 / 6011-350  7004'' ||',
'                         chr(13) ||',
'                         ''MANTA Yandry Arteaga  yarteaga@recsa.com.ec (05) 2629-844 EXT. 6161'' ||',
'                         chr(13) ||',
'                         ''SANTO DOMINGO Susana Borja  sborja@recsa.com.ec (02) 2767-942 / 2767-943  EXT. 6111'';',
'  ',
'  ELSIF ln_num_recsa > 0 THEN',
'  ',
unistr('    :P154_CONTACTOS_NEG := ''Cr\00E9dito vendido a COBRANZAS DEL ECUADOR (RECSA), por favor contactarse con el personal de servicio al cliente'' ||'),
'                         chr(13) || chr(13) ||',
'                         ''QUITO Katy Egas kegas@recsa.com.ec (02) 2999-800 / 2983-900  EXT. 9822'' ||',
'                         chr(13) ||',
'                         ''CUENCA  Thelmo Guzman tguzman@recsa.com.ec (07)2820858  '' ||',
'                         chr(13) ||',
unistr('                         ''AMBATO  Martha Proa\00F1o mproano@recsa.com.ec (03) 2822-883 / 2822-366  EXT. 6081 '' ||'),
'                         chr(13) ||',
'                         ''GUAYAQUIL Michel Onofre donofre@recsa.com.ec (04) 6011-380 / 6011-350  7004'' ||',
'                         chr(13) ||',
'                         ''MANTA Yandry Arteaga  yarteaga@recsa.com.ec (05) 2629-844 EXT. 6161'' ||',
'                         chr(13) ||',
'                         ''SANTO DOMINGO Susana Borja  sborja@recsa.com.ec (02) 2767-942 / 2767-943  EXT. 6111'';',
'  ',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_projuridico',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_projuridico',
'     AND cx.cli_id = :P154_CLI_ID;',
'',
'  IF ln_num_projuridico > 0 THEN',
'  ',
unistr('    :P154_CONTACTOS_NEG := ''Cr\00E9dito  vendido a Projuridico, por favor contactar al  personal de servicio al cliente en las  oficinas de Guayaquil (Lorenzo de Garaicoa Nro. 732 y Victor Manuel Rendon, Ed. Plaza Centenario) '' ||'),
'                         chr(13) || ''',
unistr('                        Piso 3, Oficina 32.  Tel\00E9fonos: 04 2598300, Ext. 6000 Contacto: Jessica Granizo'';'),
'  ',
'  END IF;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P154_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_process_when_type=>'EXISTS'
,p_internal_uid=>117846302855824608277
);
wwv_flow_imp.component_end;
end;
/
