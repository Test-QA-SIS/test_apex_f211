prompt --application/pages/page_00130
begin
--   Manifest
--     PAGE: 00130
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>130
,p_name=>'Reporte Depositos Tarjetas'
,p_step_title=>'Reporte Depositos Tarjetas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(36903758134691077)
,p_plug_name=>'Depositos Tarjetas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(36922453183809984)
,p_plug_name=>'Depositos Reales'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (SELECT u.uge_id',
'          FROM asdm_unidades_gestion u',
'         WHERE u.uge_id = caj.uge_id) uge_id,',
'       ',
'       (SELECT u.uge_nombre',
'          FROM asdm_unidades_gestion u',
'         WHERE u.uge_id = caj.uge_id) unidad_gestion,',
'       cde.mca_id,',
'       caj.mca_fecha fecha,',
'       ',
'       pq_car_tarjetas_credito.fn_retorna_ede_des_padre_tar(pn_ede_id    => cde.ede_id,',
'                                                            pn_parametro => 1) banco,',
'       ',
'       pq_car_tarjetas_credito.fn_retorna_ede_des_padre_tar(pn_ede_id    => cde.ede_id,',
'                                                            pn_parametro => 2) tarjeta,',
'       ',
'       ede.ede_descripcion,',
'       cde.ede_id,',
'       cde.mcd_valor_movimiento,',
'       cde.mcd_nro_tarjeta,',
'       cde.mcd_voucher,',
'       cde.mcd_nro_recap,',
'       cde.mcd_titular_tarjeta,',
'       cde.mcd_id_ref',
'  FROM (SELECT dre.dre_fecha, dre.dre_valor, dde.mca_id, dde.mcd_id',
'          FROM ven_depositos_recap dre, ven_depositos_recap_det dde',
'         WHERE dre.dre_id = dde.dre_id',
'           AND dre.dre_estado_registro = 0',
'              --               pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'           AND dre.dre_estado_registro = dde.drd_estado_registro',
'           AND dre.emp_id = :f_emp_id) a,',
'       asdm_movimientos_caja_detalle cde,',
'       asdm_entidades_destinos ede,',
'       asdm_movimientos_caja caj',
' WHERE a.mcd_id = cde.mcd_id',
'   AND ede.ede_id = cde.ede_id',
'   AND cde.mca_id = caj.mca_id',
'      --   AND caj.uge_id = :p130_uge_id',
'   AND trunc(caj.mca_fecha) BETWEEN :p130_fecha_inicio AND :p130_fecha_fin',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(36922570022809984)
,p_name=>'Depositos Reales'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_owner=>'JORDONEZ'
,p_internal_uid=>4669418753045058
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36923752694821939)
,p_db_column_name=>'MCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Mca Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36923860287821939)
,p_db_column_name=>'BANCO'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Banco'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'BANCO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36923983443821939)
,p_db_column_name=>'TARJETA'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Tarjeta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924053337821939)
,p_db_column_name=>'EDE_DESCRIPCION'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Ede Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924152639821939)
,p_db_column_name=>'EDE_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924275517821939)
,p_db_column_name=>'MCD_VALOR_MOVIMIENTO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Mcd Valor Movimiento'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_VALOR_MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924378903821940)
,p_db_column_name=>'MCD_NRO_TARJETA'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Mcd Nro Tarjeta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_NRO_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924462817821940)
,p_db_column_name=>'MCD_VOUCHER'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Mcd Voucher'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_VOUCHER'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924583376821940)
,p_db_column_name=>'MCD_NRO_RECAP'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Mcd Nro Recap'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_NRO_RECAP'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924673956821940)
,p_db_column_name=>'MCD_TITULAR_TARJETA'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Mcd Titular Tarjeta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_TITULAR_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36924759658821940)
,p_db_column_name=>'MCD_ID_REF'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Mcd Id Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_ID_REF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36935352058925759)
,p_db_column_name=>'FECHA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37071983346268742)
,p_db_column_name=>'UGE_ID'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37072052428268835)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(36922952967810266)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'46699'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'UGE_ID:UNIDAD_GESTION:MCA_ID:FECHA:BANCO:TARJETA:EDE_DESCRIPCION:EDE_ID:MCD_VALOR_MOVIMIENTO:MCD_NRO_TARJETA:MCD_VOUCHER:MCD_NRO_RECAP:MCD_TITULAR_TARJETA:MCD_ID_REF'
,p_sort_column_1=>'FECHA'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37063583983164924)
,p_name=>'Depositos Transitorios'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge_id,',
'       unidad_gestion,',
'       mca_id,',
'       trx_id,',
'       cli_id,',
'       mca_fecha,',
'       mca_observacion,',
'       mca_id_referencia,',
'       mca_total,',
'       mcd_id,',
'       ede_id,',
'       mcd_id_ref,',
'       tfp_id,',
'       banco,',
'       tarjeta,',
'       a.ede_id_tarjeta,',
'       ede_descripcion,',
'       mcd_valor_movimiento,',
'       mcd_nro_aut_ref,',
'       mcd_nro_lote,',
'       mcd_nro_retencion,',
'       mcd_nro_comprobante,',
'       mcd_nro_tarjeta,',
'       mcd_nro_recap,',
'       mcd_voucher,',
'       mcd_fecha_deposito,',
'       mcd_dep_transitorio,',
'       mcd_titular_tarjeta,',
'       t.ede_id_cuenta',
'  FROM (SELECT (SELECT u.uge_id',
'                  FROM asdm_unidades_gestion u',
'                 WHERE u.uge_id = c.uge_id) uge_id,',
'               ',
'               (SELECT u.uge_nombre',
'                  FROM asdm_unidades_gestion u',
'                 WHERE u.uge_id = c.uge_id) unidad_gestion,',
'               ',
'               c.mca_id,',
'               c.trx_id,',
'               c.cli_id,',
'               c.mca_fecha,',
'               c.mca_observacion,',
'               c.mca_total,',
'               c.mca_id_referencia,',
'               d.mcd_id,',
'               mcd.ede_id, --cambio AC d.ede_id',
'               (SELECT DISTINCT ede_descripcion',
'                  FROM asdm_entidades_destinos',
'                 WHERE ede_tipo = ''EFI'' /*cv_tede_entidad_financiera*/',
'                   AND emp_id = c.emp_id',
'                   AND rownum = 1',
'                 START WITH ede_id = mcd.ede_id',
'                CONNECT BY PRIOR ede_id_padre = ede_id) banco,',
'               (SELECT DISTINCT ede_descripcion',
'                  FROM asdm_entidades_destinos',
'                 WHERE ede_tipo = ''TJ'' /*cv_tede_tarjeta_credito*/',
'                   AND emp_id = c.emp_id',
'                 START WITH ede_id = mcd.ede_id',
'                CONNECT BY PRIOR ede_id_padre = ede_id) tarjeta,',
'               (SELECT DISTINCT ede_id',
'                  FROM asdm_entidades_destinos',
'                 WHERE ede_tipo = ''TJ'' /*cv_tede_tarjeta_credito*/',
'                   AND emp_id = c.emp_id',
'                 START WITH ede_id = mcd.ede_id',
'                CONNECT BY PRIOR ede_id_padre = ede_id) ede_id_tarjeta,',
'               d.mcd_id_ref,',
'               e.ede_descripcion,',
'               d.tfp_id,',
'               d.mcd_valor_movimiento,',
'               d.mcd_nro_aut_ref,',
'               d.mcd_nro_lote,',
'               d.mcd_nro_retencion,',
'               d.mcd_nro_comprobante,',
'               d.mcd_nro_tarjeta,',
'               d.mcd_nro_recap,',
'               d.mcd_voucher,',
'               d.mcd_fecha_deposito,',
'               d.mcd_dep_transitorio,',
'               d.mcd_titular_tarjeta',
'          FROM asdm_movimientos_caja         c,',
'               asdm_movimientos_caja_detalle d,',
'               asdm_movimientos_caja_detalle mcd,',
'               asdm_entidades_destinos       e,',
'               ven_periodos_caja             pca,',
'               asdm_transacciones            trx',
'         WHERE pca.pca_id = c.pca_id',
'           AND c.trx_id = trx.trx_id',
'           AND trx.ttr_id IN (9, 23)',
'           AND mcd.ede_id = e.ede_id(+)',
'           AND c.mca_id = d.mca_id',
'           AND d.tfp_id =   pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito'')',
'           AND d.mcd_id_ref = mcd.mcd_id(+)',
'              -- and (d.mcd_nro_recap is null or d.mcd_voucher is null)',
'           AND pq_ven_depositos_reales.fn_verifica_dep_recap(d.mcd_id) = 0',
'           AND d.mcd_estado_registro = 0 /*cv_estado_reg_activo*/',
'           AND c.mca_estado_registro = 0 /*cv_estado_reg_activo*/',
'              --  AND c.uge_id = :p130_uge_id',
'           AND trunc(c.mca_fecha) BETWEEN :p130_fecha_inicio AND',
'               :p130_fecha_fin) a,',
'       asdm_deposito_tarjetas t',
' WHERE a.ede_id_tarjeta = t.ede_id_tarjeta(+)',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Descargar'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_exp_filename=>'Depositos_Tramsitorios'
,p_plug_query_exp_separator=>';'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37078067940302236)
,p_query_column_id=>1
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37078178702302244)
,p_query_column_id=>2
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>2
,p_column_heading=>'Unidad Gestion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37063879891164975)
,p_query_column_id=>3
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>3
,p_column_heading=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37063960974165048)
,p_query_column_id=>4
,p_column_alias=>'TRX_ID'
,p_column_display_sequence=>4
,p_column_heading=>'TRX_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064061497165048)
,p_query_column_id=>5
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064154328165048)
,p_query_column_id=>6
,p_column_alias=>'MCA_FECHA'
,p_column_display_sequence=>6
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064255503165048)
,p_query_column_id=>7
,p_column_alias=>'MCA_OBSERVACION'
,p_column_display_sequence=>7
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064380000165048)
,p_query_column_id=>8
,p_column_alias=>'MCA_ID_REFERENCIA'
,p_column_display_sequence=>8
,p_column_heading=>'REFERENCIA'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064465726165048)
,p_query_column_id=>9
,p_column_alias=>'MCA_TOTAL'
,p_column_display_sequence=>9
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064558747165048)
,p_query_column_id=>10
,p_column_alias=>'MCD_ID'
,p_column_display_sequence=>10
,p_column_heading=>'MCD_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064676712165048)
,p_query_column_id=>11
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>11
,p_column_heading=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064759983165048)
,p_query_column_id=>12
,p_column_alias=>'MCD_ID_REF'
,p_column_display_sequence=>12
,p_column_heading=>'MCD_ID_REF'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064865919165048)
,p_query_column_id=>13
,p_column_alias=>'TFP_ID'
,p_column_display_sequence=>13
,p_column_heading=>'TFP_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37064983469165048)
,p_query_column_id=>14
,p_column_alias=>'BANCO'
,p_column_display_sequence=>14
,p_column_heading=>'BANCO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065060115165048)
,p_query_column_id=>15
,p_column_alias=>'TARJETA'
,p_column_display_sequence=>15
,p_column_heading=>'TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065171457165048)
,p_query_column_id=>16
,p_column_alias=>'EDE_ID_TARJETA'
,p_column_display_sequence=>16
,p_column_heading=>'EDE_ID_TARJETA'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065283852165048)
,p_query_column_id=>17
,p_column_alias=>'EDE_DESCRIPCION'
,p_column_display_sequence=>17
,p_column_heading=>'EDE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065372851165048)
,p_query_column_id=>18
,p_column_alias=>'MCD_VALOR_MOVIMIENTO'
,p_column_display_sequence=>18
,p_column_heading=>'VALOR_MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065475588165048)
,p_query_column_id=>19
,p_column_alias=>'MCD_NRO_AUT_REF'
,p_column_display_sequence=>19
,p_column_heading=>'MCD_NRO_AUT_REF'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065561741165048)
,p_query_column_id=>20
,p_column_alias=>'MCD_NRO_LOTE'
,p_column_display_sequence=>20
,p_column_heading=>'MCD_NRO_LOTE'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065672072165048)
,p_query_column_id=>21
,p_column_alias=>'MCD_NRO_RETENCION'
,p_column_display_sequence=>21
,p_column_heading=>'MCD_NRO_RETENCION'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065761613165048)
,p_query_column_id=>22
,p_column_alias=>'MCD_NRO_COMPROBANTE'
,p_column_display_sequence=>22
,p_column_heading=>'MCD_NRO_COMPROBANTE'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065857765165048)
,p_query_column_id=>23
,p_column_alias=>'MCD_NRO_TARJETA'
,p_column_display_sequence=>23
,p_column_heading=>'MCD_NRO_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37065981280165048)
,p_query_column_id=>24
,p_column_alias=>'MCD_NRO_RECAP'
,p_column_display_sequence=>24
,p_column_heading=>'MCD_NRO_RECAP'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37066074951165048)
,p_query_column_id=>25
,p_column_alias=>'MCD_VOUCHER'
,p_column_display_sequence=>25
,p_column_heading=>'MCD_VOUCHER'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37066161891165049)
,p_query_column_id=>26
,p_column_alias=>'MCD_FECHA_DEPOSITO'
,p_column_display_sequence=>26
,p_column_heading=>'MCD_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37066255960165076)
,p_query_column_id=>27
,p_column_alias=>'MCD_DEP_TRANSITORIO'
,p_column_display_sequence=>27
,p_column_heading=>'MCD_DEP_TRANSITORIO'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37066379986165076)
,p_query_column_id=>28
,p_column_alias=>'MCD_TITULAR_TARJETA'
,p_column_display_sequence=>28
,p_column_heading=>'MCD_TITULAR_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37066477733165076)
,p_query_column_id=>29
,p_column_alias=>'EDE_ID_CUENTA'
,p_column_display_sequence=>29
,p_column_heading=>'EDE_ID_CUENTA'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(36921958666804745)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(36903758134691077)
,p_button_name=>'CARGAR'
,p_button_static_id=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Cargar'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36906078004706245)
,p_name=>'P130_UGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(36903758134691077)
,p_prompt=>'Unidad de Gestion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36906280431706317)
,p_name=>'P130_FECHA_INICIO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(36903758134691077)
,p_prompt=>'Fecha Inicio'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36906477971706348)
,p_name=>'P130_FECHA_FIN'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(36903758134691077)
,p_prompt=>'Fecha Fin'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
