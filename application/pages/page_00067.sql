prompt --application/pages/page_00067
begin
--   Manifest
--     PAGE: 00067
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>67
,p_name=>'Report 1'
,p_step_title=>'Report 1'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(100272976012215047)
,p_plug_name=>'Anulacion Efectivizacion Cheques en &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cre.cre_id,',
'       rca.rca_id,',
'       cre.cre_titular_cuenta titular,',
'       cre.cli_id             cli_id,       ',
'       cli.nombre_completo    cliente,',
'       ede.ede_descripcion    entidad,',
'       cre.cre_nro_cuenta     nro_cuenta,',
'       cre.cre_nro_cheque     nro_cheque,',
'       cre.cre_nro_pin        nro_pin,',
'       cre.cre_valor          valor,',
'       cre.cre_fecha_deposito fecha_deposito,',
'       cre.cre_saldo_cheque   saldo_cheque',
'  FROM asdm_cheques_recibidos   cre,',
'       v_asdm_clientes_personas cli,',
'       asdm_entidades_destinos  ede,',
'       car_respaldos_cartera    rca,',
'       asdm_transacciones       trx',
' WHERE cre.cre_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'   AND cre.ech_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ech_id_aplicado'')',
'   AND cre.cre_tipo =',
'       pq_constantes.fn_retorna_constante(0, ''cv_tch_posfechado'')',
'   AND cre.emp_id = :f_emp_id',
'   AND cre.cli_id = cli.cli_id',
'   AND cre.ede_id = ede.ede_id',
'   AND trunc(trx.trx_fecha_ejecucion) = trunc(SYSDATE)',
'   AND rca.rca_id = cre.rca_id',
'   and trx.trx_id = cre.trx_id',
'   and cli.emp_id = :f_emp_id',
'   and rca.emp_id = :f_emp_id',
'   and ede.emp_id = :f_emp_id',
'   AND rca.uge_id = :f_uge_id',
' ORDER BY cre.cre_id desc;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_plug_display_when_condition=>'efectivizar'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(100273156026215074)
,p_name=>'Report 1'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link=>'f?p=&APP_ID.:67:&SESSION.:DETALLE:&DEBUG.::P67_CRE_ID,P67_RCA_ID,P67_CLIENTE,P67_ENTIDAD,P67_NRO_CUENTA,P67_NRO_CHEQUE,P67_NRO_PIN,P67_VALOR,P67_CLI_ID,P67_SALDO_CHEQUE:#CRE_ID#,#RCA_ID#,#CLIENTE#,#ENTIDAD#,#NRO_CUENTA#,#NRO_CHEQUE#,#NRO_PIN#,#VALOR#'
||',#CLI_ID#,#SALDO_CHEQUE#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'PLOPEZ'
,p_internal_uid=>68020004756450148
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100273454380215088)
,p_db_column_name=>'ENTIDAD'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ENTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100273566584215088)
,p_db_column_name=>'NRO_CUENTA'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Nro Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100273653348215089)
,p_db_column_name=>'NRO_CHEQUE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Nro Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100354667046698166)
,p_db_column_name=>'FECHA_DEPOSITO'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Fecha Deposito'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100354770446698167)
,p_db_column_name=>'VALOR'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100354871552698167)
,p_db_column_name=>'CRE_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Cre Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100355076123698167)
,p_db_column_name=>'RCA_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Rca Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'RCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100371882542612566)
,p_db_column_name=>'TITULAR'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Titular'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TITULAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100371960697612566)
,p_db_column_name=>'CLI_ID'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100372067407612566)
,p_db_column_name=>'CLIENTE'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100372173990612567)
,p_db_column_name=>'NRO_PIN'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Nro Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(100372275901612567)
,p_db_column_name=>'SALDO_CHEQUE'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Saldo Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SALDO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(100274967893215092)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'680219'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:CLIENTE:TITULAR:ENTIDAD:NRO_CUENTA:NRO_CHEQUE:NRO_PIN:FECHA_DEPOSITO:VALOR:SALDO_CHEQUE'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(100372967351636630)
,p_name=>'Detalle del Cheque:  Cliente => &P67_CLIENTE. ;   Entidad => &P67_ENTIDAD. ;   Cheque  => &P67_NRO_CHEQUE. ; PIN  => &P67_NRO_PIN. ;    Valor => &P67_VALOR.'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ede.ede_descripcion       entidad,',
'       cre.cre_nro_cuenta        nro_cuenta,',
'       cre.cre_nro_cheque        nro_cheque,',
'       cre.cre_fecha_deposito    fecha_deposito,',
'       cxc.cxc_id                cuenta_x_cobrar,',
'       com.com_id                factura,',
'       div.div_nro_vencimiento   cuota,',
'       div.div_fecha_vencimiento fecha_vencimiento,',
'       rdi.rdi_valor_respaldado            valor,',
'       cre.cre_saldo_cheque      saldo_cheque',
'  FROM asdm_cheques_recibidos   cre,',
'       asdm_entidades_destinos  ede,',
'       car_respaldos_cartera    rca,',
'       car_respaldos_dividendos rdi,',
'       car_dividendos           div,',
'       car_cuentas_por_cobrar   cxc,',
'       ven_comprobantes         com',
' WHERE cre.cre_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'   AND cre.ech_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ech_id_aplicado'')',
'   AND cre.cre_tipo =',
'       pq_constantes.fn_retorna_constante(0, ''cv_tch_posfechado'')',
'  AND cre.emp_id = :f_emp_id',
'   AND cre.ede_id = ede.ede_id',
'   AND rca.rca_id = cre.rca_id',
'   AND rdi.rca_id = rca.rca_id',
'   AND div.div_id = rdi.div_id',
'   AND cxc.cxc_id = div.cxc_id',
'   AND cxc.cli_id = cre.cli_id',
'   AND com.ord_id(+) = cxc.ord_id',
'   AND com.cli_id(+) = cxc.cli_id',
'   and cxc.emp_id = :f_emp_id',
'   and rca.emp_id = :f_emp_id',
'   and com.emp_id = :f_emp_id',
'   AND rca.uge_id = :f_uge_id',
'   and cre.cre_id = :p67_cre_id',
'   and rca.rca_id = :p67_rca_id',
'ORDER BY CUOTA',
'',
''))
,p_display_when_condition=>'DETALLE'
,p_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395158103766418)
,p_query_column_id=>1
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>1
,p_column_heading=>'Entidad'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395256691766424)
,p_query_column_id=>2
,p_column_alias=>'NRO_CUENTA'
,p_column_display_sequence=>2
,p_column_heading=>'Nro Cuenta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395378675766424)
,p_query_column_id=>3
,p_column_alias=>'NRO_CHEQUE'
,p_column_display_sequence=>3
,p_column_heading=>'Nro Cheque'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395464818766424)
,p_query_column_id=>4
,p_column_alias=>'FECHA_DEPOSITO'
,p_column_display_sequence=>4
,p_column_heading=>'Fecha Deposito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100427264130853246)
,p_query_column_id=>5
,p_column_alias=>'CUENTA_X_COBRAR'
,p_column_display_sequence=>5
,p_column_heading=>'Cuenta X Cobrar'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395573181766424)
,p_query_column_id=>6
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>6
,p_column_heading=>'Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395677571766424)
,p_query_column_id=>7
,p_column_alias=>'CUOTA'
,p_column_display_sequence=>8
,p_column_heading=>'Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395769920766424)
,p_query_column_id=>8
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>7
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(100395883108766424)
,p_query_column_id=>9
,p_column_alias=>'VALOR'
,p_column_display_sequence=>9
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128665178259279872)
,p_query_column_id=>10
,p_column_alias=>'SALDO_CHEQUE'
,p_column_display_sequence=>10
,p_column_heading=>'Saldo Cheque'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(100446375305913272)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_button_name=>'ANULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'ANULAR'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(128552882358826983)
,p_branch_action=>'f?p=&APP_ID.:67:&SESSION.:ANULAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(100446375305913272)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 09-JAN-2012 15:17 by ACALLE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(100440466728883228)
,p_branch_action=>'f?p=&FLOW_ID.:67:&SESSION.'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>99
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100396960658776607)
,p_name=>'P67_CRE_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100397271740779751)
,p_name=>'P67_RCA_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Rca Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100398956418794314)
,p_name=>'P67_CLIENTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100399162057794315)
,p_name=>'P67_ENTIDAD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Entidad'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100399358923794315)
,p_name=>'P67_NRO_CUENTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Nro Cuenta'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100399557530794315)
,p_name=>'P67_NRO_CHEQUE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Nro Cheque'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100399771921794315)
,p_name=>'P67_NRO_PIN'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Nro Pin'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(100455373752950607)
,p_name=>'P67_VALOR'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'Valor Cheque'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(127269173435585285)
,p_name=>'P67_CLI_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'cli_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(128412566178478875)
,p_name=>'P67_OBSERVACIONES'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'OBSERVACION'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>200
,p_cHeight=>3
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(128668855318292226)
,p_name=>'P67_SALDO_CHEQUE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(100372967351636630)
,p_prompt=>'saldo_cheque'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(128549878071816283)
,p_validation_name=>'P67_OBSERVACIONES'
,p_validation_sequence=>10
,p_validation=>'P67_OBSERVACIONES'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Observacion'
,p_always_execute=>'Y'
,p_associated_item=>wwv_flow_imp.id(128412566178478875)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(100298755162215784)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'  :p0_ttr_descripcion := NULL;',
'  :p0_datfolio        := NULL;',
'  :p0_fol_sec_actual  := NULL;',
'  :p0_ttr_id          := pq_constantes.fn_retorna_constante(NULL,',
'                                                            ''cn_ttr_id_pago_cuota'');',
'  :p0_periodo         := NULL;',
'  ln_tgr_id           := pq_constantes.fn_retorna_constante(0,',
'                                                            ''cn_tgr_id_mov_caja'');',
'  pq_ven_movimientos_caja.pr_datos_folio_caja_usu(pn_emp_id          => :f_emp_id,',
'                                                  pn_pca_id          => :f_pca_id,',
'                                                  pn_tgr_id          => ln_tgr_id,',
'                                                  pv_ttr_descripcion => :p0_ttr_descripcion,',
'                                                  pv_periodo         => :p0_periodo,',
'                                                  pv_datfolio        => :p0_datfolio,',
'                                                  pn_fol_sec_actual  => :p0_fol_sec_actual,',
'                                                  pn_pue_num_sri     => :p0_pue_num_sri,',
'                                                  pn_uge_num_est_sri => :p0_pue_num_sri,',
'                                                  pn_pue_id          => :p0_pue_id,',
'                                                  pn_ttr_id          => :p0_ttr_id,',
'                                                  pn_nro_folio       => :p0_nro_folio,',
'                                                  pv_error           => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>68045603892450858
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(127795564989740505)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_anula_efectivizacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_movimientos_caja.pr_anula_efectivizacion(pn_emp_id                => :f_emp_id,',
'                        pn_uge_id                => :f_uge_id,',
'                        pn_uge_id_gasto          => :f_uge_id_gasto,',
'                        pn_usu_id                => :f_user_id,',
'                        pn_pca_id                => :F_PCA_ID,',
'                        pn_cli_id                => :p67_cli_id,',
'                        pn_mca_total             => :p67_valor - NVL(:p67_saldo_cheque,0),',
'                        pn_rca_id                => :p67_rca_id,',
'                        pn_cre_id                => :p67_cre_id,',
'                        pv_observacion           => :p67_observaciones,',
'                        pv_error                 => :p0_error);',
'if :p0_error is null then',
'pq_car_cartera.pr_imp_chq_efectivizados(pn_emp_id    => :f_emp_id,',
'                                     pn_pue_id    => :p0_pue_id,',
'                                     pn_user_id   => :f_user_id,',
'                                     pn_uge_id   => :f_uge_id,',
'                                     pv_error     => :p0_error);',
'end if;',
'IF apex_collection.collection_exists(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                   ''cv_coleccion_mov_caja'')) THEN',
'        apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                  ''cv_coleccion_mov_caja''));',
'      END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_PAGOS_REVERSO'') THEN',
'        apex_collection.delete_collection(p_collection_name => ''COLL_PAGOS_REVERSO'');',
'      END IF;',
'',
':p67_observaciones:= null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ANULAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>95542413719975579
);
wwv_flow_imp.component_end;
end;
/
