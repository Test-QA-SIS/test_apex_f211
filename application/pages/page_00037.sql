prompt --application/pages/page_00037
begin
--   Manifest
--     PAGE: 00037
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>37
,p_name=>'VEN_DEPOSITOS_REALES'
,p_step_title=>'VEN_DEPOSITOS_REALES'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'ENARANJO'
,p_last_upd_yyyymmddhh24miss=>'20240122120052'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(51888879244574743)
,p_plug_name=>'VEN_DEPOSITOS_REALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.dre_id,',
'       a.emp_id,',
'       a.EDE_id,',
'       a.trx_id,',
'     --  a.mca_id,',
'       a.dre_fecha,',
'       a.dre_numero_papeleta,',
'       a.dre_valor,',
'       a.dre_razon_descuadre,',
'       a.dre_valor_descuadre,',
'       a.dre_estado_registro,',
'       w.ent,',
'       w.cta,',
'       decode (a.ede_id, null,''Tarjetas'', ''Efectivo/Cheques'') Deposito_Real',
'FROM   ven_depositos_reales a, asdm_transacciones b,',
'       (SELECT z.ucb_id,',
'               c.ede_id          cta_id,',
'               c.ede_descripcion cta,',
'               d.ede_id          ent_id,',
'               d.ede_descripcion ent',
'        FROM   asdm_ugestion_ctabancos z,',
'               asdm_entidades_destinos c,',
'               asdm_entidades_destinos d',
'        WHERE Z.ede_id = c.ede_id',
'               AND c.ede_id_padre = d.ede_id',
'               AND z.uge_id = :f_uge_id) w',
'WHERE  a.EDE_id = w.CTA_id(+)',
'and b.trx_id = a.trx_id',
'and b.emp_id = a.emp_id',
'and b.uge_id = :f_uge_id',
'and TRUNC(a.dre_fecha) = TO_DATE(:p37_fecha,''DD-MM-YYYY'')',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(51889064909574748)
,p_name=>'VEN_DEPOSITOS_REALES'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'PLOPEZ'
,p_internal_uid=>19635913639809822
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51889165504574760)
,p_db_column_name=>'DRE_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Dre Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'DRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51889252187574763)
,p_db_column_name=>'EMP_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51889484024574763)
,p_db_column_name=>'TRX_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Trx Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51889653467574764)
,p_db_column_name=>'DRE_FECHA'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_static_id=>'DRE_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51889760108574764)
,p_db_column_name=>'DRE_NUMERO_PAPELETA'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Numero Papeleta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'DRE_NUMERO_PAPELETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51889882432574764)
,p_db_column_name=>'DRE_VALOR'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'DRE_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51890068992574765)
,p_db_column_name=>'DRE_RAZON_DESCUADRE'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Razon Descuadre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'DRE_RAZON_DESCUADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51890166632574765)
,p_db_column_name=>'DRE_VALOR_DESCUADRE'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Valor Descuadre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'DRE_VALOR_DESCUADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51890276644574765)
,p_db_column_name=>'DRE_ESTADO_REGISTRO'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>' Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'DRE_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(52404580399838434)
,p_db_column_name=>'ENT'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ENT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(52404662406838435)
,p_db_column_name=>'CTA'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(53644363495873428)
,p_db_column_name=>'EDE_ID'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(133835865299627474)
,p_db_column_name=>'DEPOSITO_REAL'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Deposito Real'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DEPOSITO_REAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(51890863191575975)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292625533919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'DRE_ID:DRE_FECHA:ENT:CTA:DRE_NUMERO_PAPELETA:DRE_VALOR:DRE_VALOR_DESCUADRE:DRE_RAZON_DESCUADRE:DRE_ESTADO_REGISTRO:DEPOSITO_REAL'
,p_sort_column_1=>'DRE_ID'
,p_sort_direction_1=>'DESC'
,p_break_on=>'DEPOSITO_REAL:0:0:0:0:0'
,p_break_enabled_on=>'DEPOSITO_REAL:0:0:0:0:0'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51890470926574765)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(51888879244574743)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Deposito Efectivo/Cheques'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:85:&SESSION.::&DEBUG.:85::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133833671832600993)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(51888879244574743)
,p_button_name=>'DEPOSITO_TARJETAS'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Deposito Tarjetas'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:66:&SESSION.:recap:&DEBUG.:66::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45825751338215069)
,p_name=>'P37_FECHA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(51888879244574743)
,p_prompt=>'Fecha'
,p_source=>'sysdate'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit()";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'DIGITE LA FECHA QUE QUIERE CONSULTAR LOS MOVIMIENTOS QUE SE REALIZARON'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(132695753430179859)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'END IF;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>100442602160414933
);
wwv_flow_imp.component_end;
end;
/
