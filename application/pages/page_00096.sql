prompt --application/pages/page_00096
begin
--   Manifest
--     PAGE: 00096
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>96
,p_name=>'Migracion Facturas'
,p_step_title=>'Migracion Facturas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'16'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(881794167440160948)
,p_plug_name=>unistr('MIGRACI\00D3N')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523966992046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(1678135356853026056)
,p_plug_name=>'MODIFICACION PARA EL SEGURO DE MOTOS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523966992046668)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(882799157930271704)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(881794167440160948)
,p_button_name=>'EJECUTAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Ejecutar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1678242961486036903)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(1678135356853026056)
,p_button_name=>'EJECUTAR1'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Ejecutar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(2329862879866232823)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(1678135356853026056)
,p_button_name=>'UPDATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Update'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(882291468919218139)
,p_name=>'P96_COM_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(881794167440160948)
,p_prompt=>'NUMERO FACTURA DEL S.I.C.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1678182072437030557)
,p_name=>'P96_COM_ID_1'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(1678135356853026056)
,p_prompt=>'NUMERO FACTURA DEL S.I.C.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(882199964286207279)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_migracion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'LN_SUCURSAL NUMBER;',
'LN_FACTURA NUMBER;',
'begin',
'',
'DELETE FROM ven_var_comprobantes_detman a',
'WHERE a.com_id=:P96_COM_ID;',
'',
'DELETE FROM ven_var_comprobantes_man b',
'WHERE b.com_id=:P96_COM_ID;',
'',
'DELETE FROM ven_comprobantes_det_rel b',
'WHERE cde_id IN (SELECT a.cde_id FROM ven_comprobantes_det a WHERE a.com_id=:P96_COM_ID);',
'',
'',
'',
'DELETE FROM ven_var_comprobantes WHERE com_id=:P96_COM_ID;',
'',
'',
'',
'DELETE FROM VEN_VAR_COMPROBANTES_DET B',
'WHERE B.CDE_ID IN (SELECT a.cde_id FROM ven_comprobantes_det a WHERE a.com_id=:P96_COM_ID);',
'',
'DELETE FROM ven_comprobantes_det WHERE com_id=:P96_COM_ID;',
'',
'DELETE FROM asdm_homo_det_com WHERE com_id=:P96_COM_ID;',
'',
'',
'',
'',
'UPDATE asdm_homologacion_com A',
'SET A.ESTADO=''''',
'WHERE A.COM_ID=:P96_COM_ID;',
'',
'SELECT A.CODIGO_SUCURSAL,A.SECUENCIA_FACTURA ',
'INTO LN_SUCURSAL,LN_FACTURA',
'FROM asdm_p.asdm_homologacion_com A',
'WHERE A.COM_ID=:P96_COM_ID;',
'',
'',
' pq_ven_migracion.pr_actualiza_comprobante(:F_EMP_ID,LN_SUCURSAL,LN_FACTURA,:P0_ERROR);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'ERROR'
,p_process_when_button_id=>wwv_flow_imp.id(882799157930271704)
,p_process_success_message=>'CORRECTO'
,p_internal_uid=>849946813016442353
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1678434270059058277)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cambio_seguro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_ite_sku_id inv_items.ite_sku_id%type;',
'ln_cde_id ven_comprobantes_det.cde_id%type;',
'begin',
'SELECT a.ite_sku_id',
' INTO ln_ite_sku_id',
'  FROM ven_comprobantes_det a,',
'       inv_items_categorias b',
' WHERE a.ite_sku_id = b.ite_sku_id',
'   AND b.cat_id =',
'       pq_constantes.fn_retorna_constante(a.emp_id,',
'                                          ''cn_cat_id_stock'')',
'   AND a.com_id =:P96_COM_ID_1 ;',
'',
'SELECT a.cde_id',
' INTO ln_cde_id',
'  FROM ven_comprobantes_det a,',
'       inv_items_categorias b',
' WHERE a.ite_sku_id = b.ite_sku_id',
'   AND b.cat_id =',
'       pq_constantes.fn_retorna_constante(a.emp_id,',
'                                          ''cn_cat_id_gar_moto'')',
'   AND a.com_id =:P96_COM_ID_1;',
'',
'UPDATE ven_comprobantes_det_rel a ',
'SET a.ite_sku_id=ln_ite_sku_id',
'WHERE a.cde_id=ln_cde_id;',
'',
'commit; ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(1678242961486036903)
,p_process_success_message=>'PROCESO CORRECTO'
,p_internal_uid=>1646181118789293351
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(2329893257141235681)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_update'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'UPDATE ven_ordenes',
'SET ord_tipo=''VENMO''',
'WHERE ord_id=52019;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(2329862879866232823)
,p_process_when_type=>'NEVER'
,p_internal_uid=>2297640105871470755
);
wwv_flow_imp.component_end;
end;
/
