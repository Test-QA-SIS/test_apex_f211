prompt --application/pages/page_00141
begin
--   Manifest
--     PAGE: 00141
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>141
,p_name=>'OrdenVentaRapida'
,p_step_title=>'OrdenVentaRapida'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'',
'',
'function getKeyCode(e)',
'{',
'e= (window.event)? event : e;',
'intKey = (e.keyCode)? e.keyCode: e.charCode;',
'return intKey;',
'}      ',
'',
'function fnEnter(){',
'var event = jQuery.Event(''keypress'');',
'event.which = 13; ',
'event.keyCode = 13; //keycode to trigger this for simulating enter',
'jQuery(document.getElementById(''P141_PER_NRO_IDENTIFICACION'')).trigger(event)',
'}',
'',
'function fnEnter1(obj){',
'var event = jQuery.Event(''keypress'');',
'event.which = 13; ',
'event.keyCode = 13; //keycode to trigger this for simulating enter',
'obj.trigger(event)',
'}',
'',
'</script>',
'',
'',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240124100513'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(41053405341056935)
,p_plug_name=>'Opcion Excluida'
,p_plug_display_sequence=>1
,p_plug_display_point=>'BODY_2'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7658810192888156611)
,p_name=>'....'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>500
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'SELECT SYSDATE FROM DUAL;'
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>50
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634454282631016576)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>2
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634454676154016576)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>1
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634455119933016576)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634455524887016577)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634455942072016577)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634456316800016577)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634456664845016578)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634457090457016578)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634457491341016578)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634457940658016579)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634458311311016579)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634458726564016579)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634459114551016580)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634459507133016582)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634459931395016582)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634460337611016582)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634460680775016583)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634461101725016583)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634461459419016583)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634461947389016583)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634462286445016584)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634462712139016584)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634463092018016584)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634463471290016585)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634463858985016585)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634464266502016586)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634464737189016586)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634465109126016587)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634465543317016587)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634465876854016587)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634466273952016588)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634466655066016588)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634467130095016589)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634467536545016589)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634467945484016589)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634468298778016590)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634468716047016590)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634469089289016591)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634469453636016591)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634469921413016591)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634470340221016591)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634470729204016592)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634471087848016593)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634471480935016593)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634471944245016593)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634472312423016594)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634472707378016594)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634473102205016594)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634473469764016595)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634473944793016595)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7674044696806190360)
,p_name=>'FECHAS CORTE CARTERA'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>130
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(c012) valor ',
'    ',
'      FROM apex_collections',
'     WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'       AND c007 =',
'           pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                              ''cn_var_id_valor_financiar'')',
'      /* AND (c015 IN',
'           (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                ''cn_tre_id_combo'')) OR',
'           c015 IS NULL OR c015 <> 999)*/;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(7674044696806190360)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634442898469016541)
,p_query_column_id=>1
,p_column_alias=>'VALOR'
,p_column_display_sequence=>1
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7703014825103929588)
,p_plug_name=>'ADICIONALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>65
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P141_NUMERO_DETALLES > 0'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(12684618420628222412)
,p_name=>'cupo'
,p_parent_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_sequence=>570
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apex_item.text(p_idx        => 1,',
'                      p_value      => (pq_car_manejo_cupos.fn_retorna_cupo_utilizado(pn_cli_id => cu.cli_id,',
'                                                     pn_tse_id => :F_SEG_ID)+NVL(:P141_CUOTA_CRE,0)),',
'                      p_size => 6,',
'                      p_attributes => ''readonly style="color:black;font-size:20;text-align:center"'') cupo_utilizado,',
'       apex_item.text(p_idx        => 2,',
'                      p_value      => (cu.cup_cupo_disponible-NVL(:P141_CUOTA_CRE,0)),',
'                      p_size => 6,',
'                      p_attributes => ''readonly style="background-color:#5FA827;color:#FFF6CF;font-size:40;text-align:center"'') cupo_disponible',
'   FROM car_cupos_clientes cu',
' WHERE cu.cli_id = :P141_CLI_ID --604587',
'   AND cu.cup_tipo_cupo = ''M''',
'   AND cu.cup_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')'))
,p_display_when_condition=>':P141_CLI_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634453250700016574)
,p_query_column_id=>1
,p_column_alias=>'CUPO_UTILIZADO'
,p_column_display_sequence=>1
,p_column_heading=>'CUPO_UTILIZADO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634453633781016575)
,p_query_column_id=>2
,p_column_alias=>'CUPO_DISPONIBLE'
,p_column_display_sequence=>2
,p_column_heading=>'CUPO_DISPONIBLE'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7703014995810929589)
,p_plug_name=>'<SPAN STYLE="font-size: 16pt">Orden de Venta</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_2'
,p_plug_item_display_point=>'BELOW'
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'P141_ESTADO_INACTIVO'
,p_plug_display_when_cond2=>'0'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7650067924233056990)
,p_plug_name=>'OtrosDatosCabecera'
,p_parent_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7710804106088414935)
,p_plug_name=>'GARANTE'
,p_parent_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>630
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P141_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(8125930513066142533)
,p_plug_name=>'DATOS_VEN'
,p_parent_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>620
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(8125930095751137551)
,p_plug_name=>'DATOS_POL'
,p_parent_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>50
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P141_DIRECCION IS NOT NULL AND :P141_TELEFONO IS NOT NULL'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(8125930303716139906)
,p_plug_name=>'DATOS_TAR'
,p_parent_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38244419044147254311)
,p_plug_name=>'Datos del Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(120774607976337703928)
,p_plug_name=>'VTEX'
,p_parent_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_plug_display_sequence=>70
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7703015196854929590)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>90
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_CUPO_CORRECTO IS NULL and',
':P141_NUMERO_DETALLES > 0'))
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7703015601876929590)
,p_name=>'CO_VARIABLES_ENTRADA'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BEFORE_FOOTER'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C001,',
'C002,',
'C003,',
'C004,',
'C005,',
'C006,',
'C007,',
'C008,',
'C009,',
'C010',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_ENTRADA''',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'DescargarExcel'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_exp_filename=>'VariablesOrdenVentaEntrada'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634499904483016636)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634500323243016636)
,p_query_column_id=>2
,p_column_alias=>'C002'
,p_column_display_sequence=>2
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634500687105016637)
,p_query_column_id=>3
,p_column_alias=>'C003'
,p_column_display_sequence=>3
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634501082090016637)
,p_query_column_id=>4
,p_column_alias=>'C004'
,p_column_display_sequence=>4
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634501521358016638)
,p_query_column_id=>5
,p_column_alias=>'C005'
,p_column_display_sequence=>5
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634501854050016639)
,p_query_column_id=>6
,p_column_alias=>'C006'
,p_column_display_sequence=>6
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634502337687016639)
,p_query_column_id=>7
,p_column_alias=>'C007'
,p_column_display_sequence=>7
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634502710781016639)
,p_query_column_id=>8
,p_column_alias=>'C008'
,p_column_display_sequence=>8
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634503145613016640)
,p_query_column_id=>9
,p_column_alias=>'C009'
,p_column_display_sequence=>9
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634503466408016640)
,p_query_column_id=>10
,p_column_alias=>'C010'
,p_column_display_sequence=>10
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7703021206872929594)
,p_name=>'TOTALES'
,p_display_sequence=>80
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c007, to_number(c008) ord, c009 descr, round(SUM(c012), 4) valor',
'  FROM apex_collections, ppr_variables_ttransaccion t',
' WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'   AND t.ttr_id = 112',
'   AND t.vtt_func_graba_cab = ''SUM''',
'   AND t.var_id = c007',
'      /*  AND c001 IN (SELECT DISTINCT c001',
'                     FROM apex_collections',
'                    WHERE collection_name = ''CO_VARIABLES_ENTRADA''',
'                      AND c001 != ''0'')',
'         --   AND (c015 != 4 OR c015 IS NULL)',
'      AND (c015 IN (4) OR c015 IS NULL OR c015 <> 999)*/',
'   AND c004 = ''S''',
' GROUP BY c007, to_number(c008), c009',
'UNION',
'SELECT DISTINCT c007, to_number(c008) ord, c009 descr, round(c012, 2) valor',
'  FROM apex_collections, ppr_variables_ttransaccion t',
' WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'   AND t.ttr_id = 112',
'   AND t.vtt_func_graba_cab = ''DIS''',
'   AND t.var_id = c007',
'      /* AND c001 IN (SELECT DISTINCT c001',
'                     FROM apex_collections',
'                    WHERE collection_name = ''CO_VARIABLES_ENTRADA''',
'                      AND c001 != ''0'')',
'      AND (c015 != 4 OR c015 IS NULL)*/',
'   AND c004 = ''S''',
''))
,p_display_when_condition=>'P141_ESTADO_INACTIVO'
,p_display_when_cond2=>'0'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div style="width:210px">',
''))
,p_footer=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_headings_type=>'NO_HEADINGS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
,p_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C007, to_number(c008) ord,',
'       c009 descr,',
'       round(sum(c012),',
'             4) valor',
'  FROM apex_collections, ppr_variables_ttransaccion t',
' WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'AND t.ttr_id = 112',
'  and t.VTT_FUNC_GRABA_CAB =''SUM''',
'AND T.VAR_ID = C007',
' ',
' ',
'   AND c001 in (select distinct c001',
'                  from apex_collections',
'                 where collection_name = ''CO_VARIABLES_ENTRADA''',
'                   and c001 != ''0'')',
'--   AND (c015 != 4 OR c015 IS NULL)',
'   AND (c015 in(4) OR c015 IS NULL OR c015 <> 999)',
'   AND c004 = ''S''',
' GROUP BY C007,to_number(c008),',
'          c009',
'          ',
'          ',
'          ',
'UNION',
'',
'SELECT     DISTINCT C007  ',
',  to_number(c008) ord,',
'       c009 descr,',
'       round(c012,',
'             2) valor',
'  FROM apex_collections, ppr_variables_ttransaccion t',
' WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'AND t.ttr_id = 112',
'  and t.VTT_FUNC_GRABA_CAB =''DIS''',
'AND T.VAR_ID = C007',
' ',
' ',
'   AND c001 in (select distinct c001',
'                  from apex_collections',
'                 where collection_name = ''CO_VARIABLES_ENTRADA''',
'                   and c001 != ''0'')',
'   AND (c015 != 4 OR c015 IS NULL)',
'   AND c004 = ''S'''))
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(7703021206872929594)
,p_plug_column_width=>' valign="top"'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634504182482016643)
,p_query_column_id=>1
,p_column_alias=>'C007'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634504579106016644)
,p_query_column_id=>2
,p_column_alias=>'ORD'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634505047631016645)
,p_query_column_id=>3
,p_column_alias=>'DESCR'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634505437633016645)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7703027593691929597)
,p_name=>'Detalle Orden'
,p_display_sequence=>110
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_detalle(:f_emp_id,:P0_ERROR,:P141_ORD_TIPO);'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NULL',
'  FROM apex_collections col',
' WHERE col.collection_name = ''CO_DETALLE'''))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_plug_query_max_columns=>100
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>50
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(7703027593691929597)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634515959489016672)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634516365003016673)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:141:&SESSION.:EDITAR:&DEBUG.::P141_SEQ_ID,P141_AGE_ID_AGENTE,P141_ITE_SKU_ID,P141_DESCRIPCION_LARGA,P141_ESTADO,P141_ESTADO_DES,P141_BODEGA,P141_CANTIDAD,P141_LCM_ID,P141_ITE_SEC_ID,P141_ITE_SKU_MOD,P141_ACCION,P141_DESCUENTO_AUTO,P141_I'
||'TE_SKU_ID_REL:#COL04#,#COL14#,#COL08#,#COL10#,#COL11#,#COL12#,#COL06#,#COL18#,#COL17#,#COL09#,#COL08#,E,N,#COL16#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634516778339016674)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:141:&SESSION.:EDITAR_GEX:&DEBUG.::P141_SEQ_ID,P141_AGE_ID_AGENTE,P141_GEX,P141_ESTADO,P141_ESTADO_DES,P141_BODEGA,P141_TIPO_ITEM,P141_ITE_SKU_ID,P141_ACCION,P141_DESCUENTO_AUTO,P141_RECALCULA,P141_CANTIDAD_GEX,P141_ITE_SKU_ID_REL:#COL04#'
||',#COL14#,#COL08#,#COL11#,#COL12#,#COL06#,#COL15#,#COL16#,E,N,,,#COL16#'
,p_column_linktext=>'#COL03#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634517162421016674)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634517552595016675)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634517984528016675)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634518442083016676)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634518798986016676)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634519157262016677)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634519635357016677)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634519951406016678)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634520396274016678)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634520774072016678)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634521168340016679)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634521595375016679)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634522024193016680)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'CENTER'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634522414021016680)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634522796176016681)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634523230997016681)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634523627926016681)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634523953370016682)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634524439334016682)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634524803479016683)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634525155520016683)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634525588574016684)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634525980222016684)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634526359772016684)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634526803362016685)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634527223478016685)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634527613212016685)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634527971599016686)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634528439707016686)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634528814023016687)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634529238338016687)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634529556850016688)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634529957091016688)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634530368859016688)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634530848597016688)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634531247417016689)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634531647451016689)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634531998191016689)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634532433796016690)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634532850434016690)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634533247562016690)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634533555060016690)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634534010079016690)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634534415332016691)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634534803155016691)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634548336140016714)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634535242745016692)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634535648986016692)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634535986804016692)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634536375954016693)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634536811921016693)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634537238372016694)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634537583363016694)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634538005614016695)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634538435457016696)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634538834593016697)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634539195200016697)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634539627747016698)
,p_query_column_id=>61
,p_column_alias=>'COL61'
,p_column_display_sequence=>61
,p_column_heading=>'Col61'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634539985162016698)
,p_query_column_id=>62
,p_column_alias=>'COL62'
,p_column_display_sequence=>62
,p_column_heading=>'Col62'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634540368622016698)
,p_query_column_id=>63
,p_column_alias=>'COL63'
,p_column_display_sequence=>63
,p_column_heading=>'Col63'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634540771792016699)
,p_query_column_id=>64
,p_column_alias=>'COL64'
,p_column_display_sequence=>64
,p_column_heading=>'Col64'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634541171656016699)
,p_query_column_id=>65
,p_column_alias=>'COL65'
,p_column_display_sequence=>65
,p_column_heading=>'Col65'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634541614469016700)
,p_query_column_id=>66
,p_column_alias=>'COL66'
,p_column_display_sequence=>66
,p_column_heading=>'Col66'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634542048507016700)
,p_query_column_id=>67
,p_column_alias=>'COL67'
,p_column_display_sequence=>67
,p_column_heading=>'Col67'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634542441004016701)
,p_query_column_id=>68
,p_column_alias=>'COL68'
,p_column_display_sequence=>68
,p_column_heading=>'Col68'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634542837621016701)
,p_query_column_id=>69
,p_column_alias=>'COL69'
,p_column_display_sequence=>69
,p_column_heading=>'Col69'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634543174790016701)
,p_query_column_id=>70
,p_column_alias=>'COL70'
,p_column_display_sequence=>70
,p_column_heading=>'Col70'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634543604443016702)
,p_query_column_id=>71
,p_column_alias=>'COL71'
,p_column_display_sequence=>71
,p_column_heading=>'Col71'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634544043831016702)
,p_query_column_id=>72
,p_column_alias=>'COL72'
,p_column_display_sequence=>72
,p_column_heading=>'Col72'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634544435714016702)
,p_query_column_id=>73
,p_column_alias=>'COL73'
,p_column_display_sequence=>73
,p_column_heading=>'Col73'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634544792567016704)
,p_query_column_id=>74
,p_column_alias=>'COL74'
,p_column_display_sequence=>74
,p_column_heading=>'Col74'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634545249006016711)
,p_query_column_id=>75
,p_column_alias=>'COL75'
,p_column_display_sequence=>75
,p_column_heading=>'Col75'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634545646595016711)
,p_query_column_id=>76
,p_column_alias=>'COL76'
,p_column_display_sequence=>76
,p_column_heading=>'Col76'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634545964270016712)
,p_query_column_id=>77
,p_column_alias=>'COL77'
,p_column_display_sequence=>77
,p_column_heading=>'Col77'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634546388281016712)
,p_query_column_id=>78
,p_column_alias=>'COL78'
,p_column_display_sequence=>78
,p_column_heading=>'Col78'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634546841457016712)
,p_query_column_id=>79
,p_column_alias=>'COL79'
,p_column_display_sequence=>79
,p_column_heading=>'Col79'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634547160807016713)
,p_query_column_id=>80
,p_column_alias=>'COL80'
,p_column_display_sequence=>80
,p_column_heading=>'Col80'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634547598581016713)
,p_query_column_id=>81
,p_column_alias=>'COL81'
,p_column_display_sequence=>81
,p_column_heading=>'Col81'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634547988555016713)
,p_query_column_id=>82
,p_column_alias=>'COL82'
,p_column_display_sequence=>82
,p_column_heading=>'Col82'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634548702752016714)
,p_query_column_id=>83
,p_column_alias=>'COL83'
,p_column_display_sequence=>83
,p_column_heading=>'Col83'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634549094954016714)
,p_query_column_id=>84
,p_column_alias=>'COL84'
,p_column_display_sequence=>84
,p_column_heading=>'Col84'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634549491953016715)
,p_query_column_id=>85
,p_column_alias=>'COL85'
,p_column_display_sequence=>85
,p_column_heading=>'Col85'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634549890272016715)
,p_query_column_id=>86
,p_column_alias=>'COL86'
,p_column_display_sequence=>86
,p_column_heading=>'Col86'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634550326265016715)
,p_query_column_id=>87
,p_column_alias=>'COL87'
,p_column_display_sequence=>87
,p_column_heading=>'Col87'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634550705284016716)
,p_query_column_id=>88
,p_column_alias=>'COL88'
,p_column_display_sequence=>88
,p_column_heading=>'Col88'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634511244890016666)
,p_query_column_id=>89
,p_column_alias=>'COL89'
,p_column_display_sequence=>89
,p_column_heading=>'Col89'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634511580649016667)
,p_query_column_id=>90
,p_column_alias=>'COL90'
,p_column_display_sequence=>90
,p_column_heading=>'Col90'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634512031431016667)
,p_query_column_id=>91
,p_column_alias=>'COL91'
,p_column_display_sequence=>91
,p_column_heading=>'Col91'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634512387636016668)
,p_query_column_id=>92
,p_column_alias=>'COL92'
,p_column_display_sequence=>92
,p_column_heading=>'Col92'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634512764769016668)
,p_query_column_id=>93
,p_column_alias=>'COL93'
,p_column_display_sequence=>93
,p_column_heading=>'Col93'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634513236329016668)
,p_query_column_id=>94
,p_column_alias=>'COL94'
,p_column_display_sequence=>94
,p_column_heading=>'Col94'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634513640494016668)
,p_query_column_id=>95
,p_column_alias=>'COL95'
,p_column_display_sequence=>95
,p_column_heading=>'Col95'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634513965831016669)
,p_query_column_id=>96
,p_column_alias=>'COL96'
,p_column_display_sequence=>96
,p_column_heading=>'Col96'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634514401597016670)
,p_query_column_id=>97
,p_column_alias=>'COL97'
,p_column_display_sequence=>97
,p_column_heading=>'Col97'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634514841398016671)
,p_query_column_id=>98
,p_column_alias=>'COL98'
,p_column_display_sequence=>98
,p_column_heading=>'Col98'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634515195305016672)
,p_query_column_id=>99
,p_column_alias=>'COL99'
,p_column_display_sequence=>99
,p_column_heading=>'Col99'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634515618305016672)
,p_query_column_id=>100
,p_column_alias=>'COL100'
,p_column_display_sequence=>100
,p_column_heading=>'Col100'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7703030818370929598)
,p_plug_name=>'CO_VARIABLES_DETALLE'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>90
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BEFORE_FOOTER'
,p_plug_item_display_point=>'BELOW'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id, C001,',
'C002,',
'C003,',
'C004,',
'C005,',
'C006,',
'C007,',
'C008,',
'C009,',
'C010,',
'C011,',
'C012,',
'C013,',
'C014,',
'C015,',
'C016,',
'C017,',
'C018,',
'C019,',
'C020,',
'C021,',
'C022',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_DETALLE'''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_column_width=>'valign=top'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(17830412030470829039)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'ETENESACA'
,p_internal_uid=>17798158879201064113
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634578093583016759)
,p_db_column_name=>'C001'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'C001'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634578511701016759)
,p_db_column_name=>'C002'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'C002'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634578896323016759)
,p_db_column_name=>'C003'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'C003'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634579307301016760)
,p_db_column_name=>'C004'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'C004'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634579654308016760)
,p_db_column_name=>'C005'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'C005'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634580064692016761)
,p_db_column_name=>'C006'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'C006'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634580545290016761)
,p_db_column_name=>'C007'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'C007'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634580928110016761)
,p_db_column_name=>'C008'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'C008'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634581278511016762)
,p_db_column_name=>'C009'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'C009'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634581691732016762)
,p_db_column_name=>'C010'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'C010'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634582065470016762)
,p_db_column_name=>'C011'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'C011'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634582506083016763)
,p_db_column_name=>'C012'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'C012'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634582895436016763)
,p_db_column_name=>'C013'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'C013'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634583350795016763)
,p_db_column_name=>'C014'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'C014'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634583653634016764)
,p_db_column_name=>'C015'
,p_display_order=>150
,p_column_identifier=>'O'
,p_column_label=>'C015'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634584107149016764)
,p_db_column_name=>'C016'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>'C016'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634584550440016764)
,p_db_column_name=>'C017'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'C017'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634584867799016765)
,p_db_column_name=>'C018'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'C018'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634585265905016765)
,p_db_column_name=>'C019'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'C019'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634585669823016766)
,p_db_column_name=>'C020'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'C020'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634586065550016767)
,p_db_column_name=>'C021'
,p_display_order=>210
,p_column_identifier=>'U'
,p_column_label=>'C021'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634586485848016767)
,p_db_column_name=>'C022'
,p_display_order=>220
,p_column_identifier=>'V'
,p_column_label=>'C022'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7634577709068016753)
,p_db_column_name=>'SEQ_ID'
,p_display_order=>230
,p_column_identifier=>'W'
,p_column_label=>'Seq id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(14952092353746136636)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'76023337'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'C001:C002:C003:C004:C005:C006:C007:C008:C009:C010:C011:C012:C013:C014:C015:C016:C017:C018:C019:C020:C021:C022:SEQ_ID'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7703262016208987144)
,p_plug_name=>'Tipo_Despacho'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>110
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_ord_ID is null ',
'and (select count(*) ',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_DETALLE'') > 0'))
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7708031822256169243)
,p_plug_name=>'Cambio de GEX'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P141_GEX'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7710809722685859632)
,p_plug_name=>'INGRESA DETALLE'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'P141_ESTADO_INACTIVO'
,p_plug_display_when_cond2=>'0'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'sql expression',
'select count(1)',
'       from apex_collections',
'     where collection_name =''COLL_RELACIONES_ITEMS'') = 0;'))
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7703015409250929590)
,p_plug_name=>'INSERTA ARTICULOS'
,p_parent_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7915554602236646310)
,p_plug_name=>'OpcionesPromos'
,p_parent_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(Select count(*)',
'  from inv_items',
' where ite_sku_id = :P141_ITE_SKU_ID',
'   and ite_sku_id in',
'       (select j.ite_sku_hijo',
'          from inv_relaciones_item j',
'         where j.tre_id =',
'               pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tre_id_promocion'')',
'        and j.rit_tipo_item_combo = ''P''',
'',
')) > 0',
'and  :P141_POL_ID is not null'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(23016185918896612843)
,p_name=>'Promociones/Combos'
,p_parent_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>530
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_promos_combos.fn_promociones_detalle(pn_emp_id => :f_emp_id,',
'                                                      pn_ite_sku_principal =>  :p141_ite_sku_id,',
'                                                      pv_ord_tipo => :P141_ORD_TIPO,',
'                                                      pn_age_id => :F_AGE_ID_AGENCIA,',
'                                                      pn_bpr_id => :P141_BODEGA,',
'                                                      pn_lcm_id=> :P141_LCM_ID,',
'                                                      pn_tse_id => :F_SEG_ID, ',
'                                                      pn_pol_id => :P141_POL_ID,',
'                                                      pn_cei_id => :P141_ESTADO,',
'                                                      PN_RIT_OPCION => :P141_RIT_OPCION,',
'                                                      pv_error => :p0_error',
');',
'',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(Select count(*)',
'  from inv_items',
' where ite_sku_id = :P141_ITE_SKU_ID',
'   and ite_sku_id in',
'       (select j.ite_sku_hijo',
'          from inv_relaciones_item j',
'         where j.tre_id =',
'               pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tre_id_promocion'')',
'        and j.rit_tipo_item_combo = ''P''',
'        AND (j.tse_id = :F_SEG_ID OR j.tse_id IS NULL)',
'',
'and  :P141_POL_ID is not null',
'AND :P141_ITE_SKU_ID_REL IS NULL',
')) > 0'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS_INITCAP'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634611525629016817)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634611918551016818)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634612292264016818)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634612693850016819)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634613072604016819)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634613481905016819)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634613891352016819)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634614313270016820)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634614652559016820)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634615141687016820)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634615460908016820)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634615905137016821)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634616325683016821)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634616735141016822)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634617053119016822)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634617504142016823)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634617903639016823)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634618270390016824)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634618664246016824)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634619090063016825)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634619536397016825)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634619903604016825)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634620280817016826)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634620655614016826)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634621095877016827)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634621523821016827)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634621944049016827)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634622349309016828)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634622678922016828)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634623086402016828)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634623499025016828)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634623923675016830)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634624269361016830)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634624695401016831)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634625146984016831)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634625491507016831)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634625934793016832)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634626262339016832)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634626741926016834)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634627097091016834)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634627455642016835)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634627901961016835)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634628252089016836)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634628723138016836)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634629118628016837)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634629497092016837)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634629920557016837)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634630325446016838)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634630658082016838)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634631090187016839)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634631513281016839)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634631866329016840)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634632322589016840)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634632713605016841)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634633117960016841)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634633550271016842)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634633914213016843)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634634331005016843)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634634702026016843)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634635141329016844)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7873397720499400449)
,p_name=>'CO_DETALLE'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_point=>'BEFORE_FOOTER'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001,',
'c002,',
'c003,',
'c004,',
'c005,',
'c006,',
'c007,',
'c008,',
'c009,',
'c010,',
'c011,',
'c012,',
'c013,',
'c014,',
'c015,',
'c016,',
'c017,',
'c018,',
'c019,',
'c020,',
'c021,',
'c022,',
'c023,',
'c024,',
'c025,',
'c026,',
'c027,',
'c028,',
'c029,',
'c030,',
'c031,',
'c032,',
'c033,',
'c034,',
'c035,',
'c040,',
'c041,',
'c042',
'',
'',
'from apex_collections',
'where collection_name = ''CO_DETALLE'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634636188496016846)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634636592996016847)
,p_query_column_id=>2
,p_column_alias=>'C002'
,p_column_display_sequence=>2
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634637020281016847)
,p_query_column_id=>3
,p_column_alias=>'C003'
,p_column_display_sequence=>3
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634637446030016848)
,p_query_column_id=>4
,p_column_alias=>'C004'
,p_column_display_sequence=>4
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634637760757016849)
,p_query_column_id=>5
,p_column_alias=>'C005'
,p_column_display_sequence=>5
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634638172732016849)
,p_query_column_id=>6
,p_column_alias=>'C006'
,p_column_display_sequence=>6
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634638597320016850)
,p_query_column_id=>7
,p_column_alias=>'C007'
,p_column_display_sequence=>7
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634639021156016850)
,p_query_column_id=>8
,p_column_alias=>'C008'
,p_column_display_sequence=>8
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634639395836016851)
,p_query_column_id=>9
,p_column_alias=>'C009'
,p_column_display_sequence=>9
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634639797109016851)
,p_query_column_id=>10
,p_column_alias=>'C010'
,p_column_display_sequence=>10
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634640236899016852)
,p_query_column_id=>11
,p_column_alias=>'C011'
,p_column_display_sequence=>11
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634640612559016852)
,p_query_column_id=>12
,p_column_alias=>'C012'
,p_column_display_sequence=>12
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634640973846016852)
,p_query_column_id=>13
,p_column_alias=>'C013'
,p_column_display_sequence=>13
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634641402157016853)
,p_query_column_id=>14
,p_column_alias=>'C014'
,p_column_display_sequence=>14
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634641843852016853)
,p_query_column_id=>15
,p_column_alias=>'C015'
,p_column_display_sequence=>15
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634642196538016853)
,p_query_column_id=>16
,p_column_alias=>'C016'
,p_column_display_sequence=>16
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634642583296016854)
,p_query_column_id=>17
,p_column_alias=>'C017'
,p_column_display_sequence=>17
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634642997187016854)
,p_query_column_id=>18
,p_column_alias=>'C018'
,p_column_display_sequence=>18
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634643448806016854)
,p_query_column_id=>19
,p_column_alias=>'C019'
,p_column_display_sequence=>19
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634643809193016854)
,p_query_column_id=>20
,p_column_alias=>'C020'
,p_column_display_sequence=>20
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634644191557016855)
,p_query_column_id=>21
,p_column_alias=>'C021'
,p_column_display_sequence=>21
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634644596298016855)
,p_query_column_id=>22
,p_column_alias=>'C022'
,p_column_display_sequence=>22
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634645040157016856)
,p_query_column_id=>23
,p_column_alias=>'C023'
,p_column_display_sequence=>23
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634645422900016856)
,p_query_column_id=>24
,p_column_alias=>'C024'
,p_column_display_sequence=>24
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634645768113016856)
,p_query_column_id=>25
,p_column_alias=>'C025'
,p_column_display_sequence=>25
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634646220694016857)
,p_query_column_id=>26
,p_column_alias=>'C026'
,p_column_display_sequence=>26
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634646591764016857)
,p_query_column_id=>27
,p_column_alias=>'C027'
,p_column_display_sequence=>27
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634646970826016857)
,p_query_column_id=>28
,p_column_alias=>'C028'
,p_column_display_sequence=>28
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634647363081016857)
,p_query_column_id=>29
,p_column_alias=>'C029'
,p_column_display_sequence=>29
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634647782784016858)
,p_query_column_id=>30
,p_column_alias=>'C030'
,p_column_display_sequence=>30
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634648244145016858)
,p_query_column_id=>31
,p_column_alias=>'C031'
,p_column_display_sequence=>31
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634648560015016858)
,p_query_column_id=>32
,p_column_alias=>'C032'
,p_column_display_sequence=>32
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634648962829016859)
,p_query_column_id=>33
,p_column_alias=>'C033'
,p_column_display_sequence=>33
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634649451053016859)
,p_query_column_id=>34
,p_column_alias=>'C034'
,p_column_display_sequence=>34
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634649791232016859)
,p_query_column_id=>35
,p_column_alias=>'C035'
,p_column_display_sequence=>35
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634650205855016860)
,p_query_column_id=>36
,p_column_alias=>'C040'
,p_column_display_sequence=>36
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634650625946016860)
,p_query_column_id=>37
,p_column_alias=>'C041'
,p_column_display_sequence=>37
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634651036534016861)
,p_query_column_id=>38
,p_column_alias=>'C042'
,p_column_display_sequence=>38
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(14801663498300443583)
,p_name=>'col_fechas_corte'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>580
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select c001, c002, c003, c004, c005 from apex_collections where collection_name = ''COLL_FECHAS_CORTE'''
,p_display_when_condition=>':f_user_id = 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634663030909016874)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634663356103016875)
,p_query_column_id=>2
,p_column_alias=>'C002'
,p_column_display_sequence=>2
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634663837382016875)
,p_query_column_id=>3
,p_column_alias=>'C003'
,p_column_display_sequence=>3
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634664222943016876)
,p_query_column_id=>4
,p_column_alias=>'C004'
,p_column_display_sequence=>4
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7634664592322016878)
,p_query_column_id=>5
,p_column_alias=>'C005'
,p_column_display_sequence=>5
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(58289727698850693774)
,p_name=>'CO_COMBOSEGURODESCTO'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_point=>'BEFORE_FOOTER'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select C001, C002, C003, C004, C005, C006, C007, C008, C009',
'  from apex_collections',
' where collection_name = ''CO_COMBOSEGURODESCTO'';'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58289727784307693775)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58289727882939693776)
,p_query_column_id=>2
,p_column_alias=>'C002'
,p_column_display_sequence=>2
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297086645724269727)
,p_query_column_id=>3
,p_column_alias=>'C003'
,p_column_display_sequence=>3
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297086677027269728)
,p_query_column_id=>4
,p_column_alias=>'C004'
,p_column_display_sequence=>4
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297086802147269729)
,p_query_column_id=>5
,p_column_alias=>'C005'
,p_column_display_sequence=>5
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297086899533269730)
,p_query_column_id=>6
,p_column_alias=>'C006'
,p_column_display_sequence=>6
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297087034369269731)
,p_query_column_id=>7
,p_column_alias=>'C007'
,p_column_display_sequence=>7
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297087121204269732)
,p_query_column_id=>8
,p_column_alias=>'C008'
,p_column_display_sequence=>8
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58297087212512269733)
,p_query_column_id=>9
,p_column_alias=>'C009'
,p_column_display_sequence=>9
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634594860640016791)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_button_name=>'carga_detalle'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CargaET'
,p_button_position=>'BELOW_BOX'
,p_button_execute_validations=>'N'
,p_button_condition=>':F_USER_ID = 3686'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634443582755016545)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_button_name=>'P141_RECALCULAR'
,p_button_static_id=>'P20_RECALCULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536254089046676)
,p_button_image_alt=>'Recalcular'
,p_button_alignment=>'LEFT'
,p_button_condition=>':p141_ord_id is null or :P141_ORD_MODIFICA is not null'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_request_source=>'recalcular'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634551104803016716)
,p_button_sequence=>430
,p_button_plug_id=>wwv_flow_imp.id(7703027593691929597)
,p_button_name=>'P141_ELIMINAR_LINEA'
,p_button_static_id=>'P141_ELIMINAR_LINEA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>unistr('Eliminar Art\00EDculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select count(1)',
'       from apex_collections',
'     where collection_name =''CO_DETALLE'''))
,p_button_condition_type=>'EXISTS'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634914849817890563)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_button_name=>'editar_cliente'
,p_button_static_id=>'editar_cliente'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Editar cliente'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:145:&SESSION.:editar_cliente:&DEBUG.:RP:P145_PER_NRO_IDENTIFICACION,P145_PER_TIPO_IDENTIFICACION,P145_TDI_ID,P145_TTE_ID,P145_PER_ID,F_POPUP,P145_TEL_ID:&P141_PER_NRO_IDENTIFICACION.,&P141_PER_TIPO_IDENTIFICACION.,&P141_TDI_ID.,&P141_TTE_ID.,&P141_PER_ID.,S,&P141_TEL_ID.'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634915211564890564)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_button_name=>'No_POPUP'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'No popup'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:141:&SESSION.::&DEBUG.:RP:F_POPUP:N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634914364020890562)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_button_name=>'nuevo_cliente'
,p_button_static_id=>'nuevo_cliente'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Nuevo cliente'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:145:&SESSION.:nuevo_cliente:&DEBUG.:RP:P145_PER_TIPO_IDENTIFICACION,P145_PER_NRO_IDENTIFICACION,P145_TDI_ID,P145_TTE_ID,F_POPUP,P145_TEL_ID,P145_PAG:&P141_PER_TIPO_IDENTIFICACION.,&P141_PER_NRO_IDENTIFICACION.,&P141_TDI_ID.,&P141_TTE_ID.,S,&P141_TEL_ID.,'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(130256032261784693232)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_button_name=>'p141_upd_direccion'
,p_button_static_id=>'editar_direccion'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Editar direccion'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=140:3:&SESSION.:editar_direccion:&DEBUG.:RP,3:P3_EMP_ID,P3_CLI_ID,P3_DIR_ID,P3_TDI_ID,F_POPUP,P3_EDITAR_MAPA:&F_EMP_ID.,&P141_CLI_ID.,&P141_DIR_ID.,&P141_TDI_ID.,S,&P141_EDITAR_MAPA.'
,p_button_condition=>'P141_PER_NOMBRE'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634506164043016652)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_button_name=>'GRABAR_CON_ENTREGA_DOMICILIO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar e Ir a Facturaci&oacuten >>>'
,p_button_position=>'BOTTOM'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :p141_ord_tipo !=',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cv_ord_tipo_consignacion'')',
'   ',
'   and (:p141_tve_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tve_id_contado'') or',
'       :p141_tve_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tve_id_tarjeta_credito'') or',
'       :p141_tve_id =2  ) AND :P141_ENTRADA_CRE IS NOT NULL'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634498839091016633)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_imp.id(7703015196854929590)
,p_button_name=>'grabar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535660381046676)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(80146220052770509328)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_button_name=>'BT_BACK'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< --- Regresar Cliente 360'
,p_button_position=>'HELP'
,p_button_condition=>'P141_REQUEST'
,p_button_condition2=>'cliente_360'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_button_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'P141_REQUEST',
'cliente_360'))
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7634474626267016598)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_button_name=>'LIMPIAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo'
,p_button_position=>'HELP'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7634705653257016953)
,p_branch_name=>'Go To Page 201'
,p_branch_action=>'f?p=147:201:&SESSION.::&DEBUG.:201:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,P201_PER_NRO_IDENTIFICACION,P201_APP_REGRESA,P201_PAG_REGRESA:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_PARAMETRO.,S,&P141_IDENTIFICACION.,&APP_ID.,&APP_PAGE_ID.'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_branch_condition=>'P141_CLIENTE_EXISTE'
,p_branch_condition_text=>'N'
,p_branch_comment=>'Created 07-ABR-2010 17:22 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7634706099330016953)
,p_branch_name=>'Go To Page 30 Con Pago'
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.:factura:&DEBUG.:141,30:P30_CLI_ID,P30_ORD_ID,P30_FACTURAR,P30_TVE_ID,P30_TFP_ID,P30_BANCOS,P30_TARJETA,P30_TIPO,P30_PLAN,P30_ENTRADA,P30_COMISION,P30_ORD_TIPO,P30_POLITICA_VENTA,P30_CLI_NOMBRE,P30_CLI_IDENTIFICACION:&P141_CLI_ID.,&P141_ORD_ID.,S,&P141_TVE_ID.,&P141_FORMA_PAGO.,&P141_BANCOS.,&P141_TARJETA.,&P141_TIPO.,&P141_PLAN_TARJETA.,&P141_ENTRADA.,,&P141_ORD_TIPO.,&P141_POL_ID.,&P141_PER_PRIMER_NOMBRE.,&P141_PER_NRO_IDENTIFICACION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(7634506164043016652)
,p_branch_sequence=>10
,p_branch_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_branch_condition=>'P141_REALIZAR_PAGO'
,p_branch_condition_text=>'S'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(15342033294461429169)
,p_branch_name=>'Go To Page 242 55 Sin Pago'
,p_branch_action=>'f?p=242:55:&SESSION.:ROL:&DEBUG.:141:F_UGE_ID,P55_ORD_ID,P55_FECHA,P55_TIPO,P55_CLI_ID,P55_TVE_ID,P55_POL_ID,P55_DESDE,P55_PLAZO,P55_ORD_SEC_ID,F_USER_ID,F_EMP_ID,F_EMP_FONDO,F_EMPRESA,F_EMP_LOGO,F_LOGO,F_OPCION_ID,F_POPUP,F_SEG_ID,F_TOKEN,F_UGESTION,F_UUG_ID,P0_ROL,P0_ROL_DESC,P0_OPCION,P0_TITULO,P0_BD,F_LOGOUT_URL,F_UGE_ID_GASTO,P0_TREE_ROOT,F_VERSION:&F_UGE_ID.,&P141_ORD_ID.,SYSDATE,&P141_ORD_TIPO.,&P141_CLI_ID.,&P141_TVE_ID.,&P141_POL_ID.,I,&P141_PLAZO_FACTURA.,&P141_ORD_SEC_ID.,&F_USER_ID.,&F_EMP_ID.,&F_EMP_FONDO.,&F_EMPRESA.,&F_EMP_LOGO.,&F_LOGO.,&F_OPCION_ID.,N,&F_SEG_ID.,&F_TOKEN.,&F_UGESTION.,&F_UUG_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_OPCION.,&P0_TITULO.,&P0_BD.,&F_LOGOUT_URL.,&F_UGE_ID_GASTO.,&P0_TREE_ROOT.,&F_VERSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(7634506164043016652)
,p_branch_sequence=>20
,p_branch_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_branch_condition=>'P141_REALIZAR_PAGO'
,p_branch_condition_text=>'N'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7634704899029016953)
,p_branch_name=>'Go To Page recalcular'
,p_branch_action=>'f?p=&APP_ID.:141:&SESSION.:recalcular:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>30
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'recalcular'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 07-FEB-2012 11:08 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7634705251310016953)
,p_branch_name=>'Go To Page 141'
,p_branch_action=>'f?p=&APP_ID.:141:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>40
,p_branch_comment=>'Created 19-MAR-2010 15:12 by MURGILES'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41053480464056936)
,p_name=>'P141_ESTADO_INACTIVO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(41053405341056935)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41053561015056937)
,p_name=>'P141_MENSAJE_OPCION_EXCLUIDA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(41053405341056935)
,p_source=>unistr('Estimado usuario, por favor utilizar la aplicaci\00F3n del cotizador movil.')
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;font-weight: bold;color:RED;font-family:Arial"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P141_ESTADO_INACTIVO'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(187977744187354769)
,p_name=>'P141_PINPAD'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(8125930303716139906)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634148641332203434)
,p_name=>'P141_ENTREGA_DOMICILIO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_item_default=>'N'
,p_prompt=>'Entrega domicilio'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_SI_NO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(300);',
'BEGIN',
'lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_SI_NO'');',
'return (lv_lov);',
'END;'))
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634149005159203438)
,p_name=>'P141_DIR_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634149110895203439)
,p_name=>'P141_MONTO_PAGAR'
,p_item_sequence=>1204
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Monto Pagar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round(SUM(to_number(a.c005)),2)',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_CRONOGRAMA'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634149181907203440)
,p_name=>'P141_FORMA_PAGO'
,p_item_sequence=>43
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_source=>'select decode(:p141_tve_id,pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''),pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito''),null) tfp from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634149696621203445)
,p_name=>'P141_COM_NUM'
,p_item_sequence=>1331
,p_item_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634149828860203446)
,p_name=>'P141_PUE_ELECTRONICO'
,p_item_sequence=>1351
,p_item_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634444003625016548)
,p_name=>'P141_DESCTO_PUNTOS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>'F_EMP_ID'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634445217597016558)
,p_name=>'P141_ENTRADA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_prompt=>'Entrada'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634446545296016565)
,p_name=>'P141_SALDO_MAX_APLICAR'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Saldo M\00E1ximo a Aplicar')
,p_source=>'pq_asdm_promociones.fn_obtener_saldo_max_aplica_od(:f_emp_id,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),:P141_cli_id,:P141_ord_id, :P0_error)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634446874735016565)
,p_name=>'P141_DESCTO_FACTURA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634447327694016565)
,p_name=>'P141_TOTAL_FACTURA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634447695732016566)
,p_name=>'P141_TOTAL_FACTURA_ING'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634448071574016566)
,p_name=>'P141_CONT'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634448494494016566)
,p_name=>'P141_POCE_NACIONAL'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634448907599016566)
,p_name=>'P141_ERROR'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="border-style:none;font-size:14px;color:RED;text-align:left;"'
,p_colspan=>20
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_CUOTA_CRE>0 and :request= ''recalcular''  and (select count(*) from CAR_CUENTAS_POR_COBRAR where cli_id= :P141_CLI_ID AND CXC_SALDO>0 AND CXC_ESTADO= ''GEN'') = 0 '
,p_display_when2=>'SQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634449350046016567)
,p_name=>'P141_MARGEN_FACTURA'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''MG'' || to_char(to_number(round(',
'(',
'(SELECT SUM(C012)',
'                                                    FROM apex_collections a',
'                                                   WHERE a.collection_name =',
'                                                         ''CO_VARIABLES_DETALLE''',
'                                                     AND a.c007 =     pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_var_id_mg_tot_comp'')',
'                                                     AND nvl(a.c015, 0) NOT IN',
'                                                         (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                             ''cn_tre_id_gex''),',
'                                                          999,',
'                                                          pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                             ''cn_tre_id_gar_moto''))) ',
'                                                ',
'',
'                                           ) /',
'                                                (SELECT SUM(C012)',
'                                                   FROM apex_collections a',
'                                                  WHERE a.collection_name =',
'                                                        ''CO_VARIABLES_DETALLE''',
'                                                    AND a.c007 =',
'                                                        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                           ''cn_var_id_venta_neta'')',
'                                                    AND nvl(a.c015, 0) NOT IN',
'                                                        (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                            ''cn_tre_id_gex''),',
'                                                         999,',
'                                                         pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                            ''cn_tre_id_gar_moto''))),',
'                                                4) * 100),',
'                                ''999990.99'')),',
'               '' '',',
'               '''')',
'  from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:28;color:BLUE;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''MG'' || to_char(to_number(round(((SELECT SUM(C012)',
'                         FROM apex_collections a',
'                        WHERE a.collection_name = ''CO_VARIABLES_DETALLE''',
'                          AND a.c007 =',
'                              pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                 ''cn_var_id_venta_neta'')',
'                          AND nvl(a.c015, 0) NOT IN',
'                              (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                  ''cn_tre_id_gex''),',
'                               999,',
'                               pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                  ''cn_tre_id_gar_moto''))) -',
'                     ',
'                     (SELECT SUM(C012)',
'                         FROM apex_collections a',
'                        WHERE a.collection_name = ''CO_VARIABLES_DETALLE''',
'                          AND a.c007 =',
'                              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                 ''cn_var_id_costo_total'')',
'                          AND nvl(a.c015, 0) NOT IN',
'                              (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                  ''cn_tre_id_gex''),',
'                               999,',
'                               pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                  ''cn_tre_id_gar_moto'')))) /',
'                     (SELECT SUM(C012)',
'                        FROM apex_collections a',
'                       WHERE a.collection_name = ''CO_VARIABLES_DETALLE''',
'                         AND a.c007 =',
'                             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                ''cn_var_id_venta_neta'')',
'                         AND nvl(a.c015, 0) NOT IN',
'                             (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                 ''cn_tre_id_gex''),',
'                              999,',
'                              pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                 ''cn_tre_id_gar_moto''))),',
'                     4) * 100),''999990.99'')),'' '','''')',
'  from dual'))
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634449734167016569)
,p_name=>'P141_VALOR_MARGEN_FACTURA'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''C''||to_char(to_number(round(SUM(C012),2)),''999990.99'')),'' '','''') from apex_collections',
'         where collection_name = ''CO_VARIABLES_DETALLE''',
'           and C007 =     pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_var_id_mg_tot_comp'')',
'           AND nvl(c015, 0) NOT IN(',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gex''),',
'             999,',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gar_moto''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:28;color:BLUE;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''C''||to_char(to_number(round(SUM(C012),2)),''999990.99'')),'' '','''') from apex_collections',
'         where collection_name = ''CO_VARIABLES_DETALLE''',
'           and C007 = 27',
'           and c015 <> 999'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634450092813016569)
,p_name=>'P141_ENTRADA_OBLIGATORIA'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
' LN_VALOR NUMBER;',
'BEGIN',
'',
'SELECT COUNT(I.CLI_ID) INTO LN_VALOR FROM ASDM_CLIENTES_INSTITUCIONES I WHERE I.CLI_ID = :P141_CLI_ID AND I.CIN_ESTADO_REGISTRO = ''0'';',
'',
'IF LN_VALOR = 0 THEN',
'   RETURN nvl(pq_car_analisis_cliente.fn_def_obligatorio(pn_cli_id => :P141_CLI_ID,',
'                                                        pn_cpd_id => 1,',
'                                                        pn_emp_id => :f_emp_id,',
'                                                        pn_pol_id => :p141_pol_id),0);',
'ELSE',
'  RETURN 0;',
'END IF;',
'',
'END;'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634450494409016570)
,p_name=>'P141_UTILIDAD_BRUTA'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''C''||to_char(to_number(round(SUM(C012),2)),''999990.99'')),'' '','''') from apex_collections',
'         where collection_name = ''CO_VARIABLES_DETALLE''',
'           and C007 = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_var_id_tot_comp'')',
'           AND nvl(c015, 0) NOT IN(',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gex''),',
'             999,',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gar_moto''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:28;color:BLUE;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634450876475016570)
,p_name=>'P141_COMISION_FINAL'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''V''||to_char(to_number(round((SUM(C012)*0.035),2)),''999990.99'')),'' '','''') from apex_collections',
'         where collection_name = ''CO_VARIABLES_DETALLE''',
'           and C007 = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_var_id_tot_comp'')',
'           AND nvl(c015, 0) NOT IN(',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gex''),',
'             999,',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gar_moto''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:28;color:BLUE;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634451309429016570)
,p_name=>'P141_UTILIDAD_BRUTA_GEX'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''GC''||to_char(to_number(round(SUM(C012),2)),''999990.99'')),'' '','''') from apex_collections',
'         where collection_name = ''CO_VARIABLES_DETALLE''',
'           and C007 = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_var_id_tot_comp'')',
'           AND nvl(c015, 0) IN(',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gex''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:28;color:BLUE;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634451680484016571)
,p_name=>'P141_COMISION_FINAL_GEX'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''GV''||to_char(to_number(round((SUM(C012)*0.065),2)),''999990.99'')),'' '','''') from apex_collections',
'         where collection_name = ''CO_VARIABLES_DETALLE''',
'           and C007 = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_var_id_tot_comp'')',
'           AND nvl(c015, 0) IN(',
'    pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gex''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:28;color:BLUE;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634452144462016571)
,p_name=>'P141_RECALCULA'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_tag_attributes=>'readonly style="color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634452534640016571)
,p_name=>'P141_ENTRADA_MINIMA'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(7703014825103929588)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':P141_TVE_ID != pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634475366692016599)
,p_name=>'P141_ORD_FECHA'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha:'
,p_source=>'ORD_FECHA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634475786983016600)
,p_name=>'P141_TIPO_IDENTIFICADOR'
,p_item_sequence=>33
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TID_DESCRIPCION',
'  FROM INV_TIPOS_IDENTIFICADOR',
' WHERE TID_ID =  PQ_CONSTANTES.fn_retorna_constante(:f_emp_id, ''cn_tid_id_identificador'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634476243493016600)
,p_name=>'P141_TVE_ID_ORIGINAL'
,p_item_sequence=>53
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>unistr('Utilizado para mantener el termino de venta que vino como par\00BF\00BFmetro y que permita cambiar a contado, tarj credito y credito propio el t\00BF\00BFrmino de venta')
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634476564298016600)
,p_name=>'P141_AGE_ID_AGENCIA'
,p_item_sequence=>63
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_use_cache_before_default=>'NO'
,p_source=>'F_AGE_ID_AGENCIA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634477009556016600)
,p_name=>'P141_ORD_ID'
,p_item_sequence=>1030
,p_item_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_prompt=>'ord_id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px; color: blue;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P141_ORD_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634477430401016601)
,p_name=>'P141_ORD_SEC_ID'
,p_item_sequence=>1031
,p_item_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Orden'
,p_source=>'select ord_sec_id from ven_ordenes where ord_id = :p141_ord_id and emp_id = :f_emp_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18px; color: blue;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P141_ORD_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634477803696016601)
,p_name=>'P141_TSE_ID'
,p_item_sequence=>73
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_use_cache_before_default=>'NO'
,p_source=>'F_SEG_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634479005446016602)
,p_name=>'P141_CLIENTE_EXISTE'
,p_item_sequence=>93
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634479375451016602)
,p_name=>'P141_EOR_OBSERVACIONES'
,p_item_sequence=>1101
,p_item_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_prompt=>'Observaciones para Despacho'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>50
,p_cMaxlength=>200
,p_cHeight=>5
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634480205137016603)
,p_name=>'P141_COND_CAB'
,p_item_sequence=>113
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_use_cache_before_default=>'NO'
,p_source=>':P141_POL_ID||''-''||:P141_TVE_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634480562926016603)
,p_name=>'P141_TRX_ID'
,p_item_sequence=>123
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634480980128016603)
,p_name=>'P141_PLAZO_ANT'
,p_item_sequence=>133
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_use_cache_before_default=>'NO'
,p_source=>':P141_PLAZO_FACTURA'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634481834722016604)
,p_name=>'P141_TRX_ID_ESTADO'
,p_item_sequence=>153
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634482204938016605)
,p_name=>'P141_TTR_ID'
,p_item_sequence=>163
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_source=>'112'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634482603243016605)
,p_name=>'P141_ORD_ID_VAR'
,p_item_sequence=>173
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634482997554016605)
,p_name=>'P141_BPR_ID'
,p_item_sequence=>183
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634483359978016605)
,p_name=>'P141_CLI_ID_REFERIDO'
,p_item_sequence=>193
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634483807943016606)
,p_name=>'P141_NOMBRE_CLI_REFERIDO'
,p_item_sequence=>203
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634484194693016606)
,p_name=>'P141_TITULO'
,p_item_sequence=>213
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634484607062016606)
,p_name=>'P141_COM_ID'
,p_item_sequence=>223
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634484974227016607)
,p_name=>'P141_ORD_MODIFICA'
,p_item_sequence=>233
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634485392373016607)
,p_name=>'P141_PER_TIPO_IDENTI'
,p_item_sequence=>243
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634485833024016607)
,p_name=>'P141_CADENA'
,p_item_sequence=>253
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.seq_id||'':''||a.c003||''::''||a.c005||'':''||a.C031 CADENA FROM apex_collections a WHERE a.collection_name=''CO_DETALLE''',
'AND a.c003=:P141_ITE_SKU_ID ',
'and (a.c006 is null or a.c008 = ''P'')',
'and (a.c031 = :P141_BODEGA or a.c032 = :p141_lcm_id)'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634486159235016607)
,p_name=>'P141_ACCION'
,p_item_sequence=>263
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634486563053016608)
,p_name=>'P141_TIPO_VENTA'
,p_item_sequence=>273
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_use_cache_before_default=>'NO'
,p_source=>'P15_TIPO_VENTA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634486973495016608)
,p_name=>'P141_APROBACION_COMERCIAL'
,p_item_sequence=>283
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634487368310016608)
,p_name=>'P141_VISUALIZA_VARIABLES'
,p_item_sequence=>293
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    upv_visualiza_yn',
'FROM      ASDM_USUARIOS_PERMISO_VISTA',
'WHERE     usuario_id = :F_USER_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634488231737016609)
,p_name=>'P141_PERMITE_ENTRADA'
,p_item_sequence=>313
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634488634264016609)
,p_name=>'P141_DESCUENTO_AUTO'
,p_item_sequence=>323
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634490379717016613)
,p_name=>'P141_TIPO_IDENTIFICACION'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(7710804106088414935)
,p_prompt=>unistr('Tipo Identificaci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634490739663016616)
,p_name=>'P141_GARANTE'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(7710804106088414935)
,p_prompt=>'Garante'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_tag_attributes=>'onchange="doSubmit(''garante'')"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'CENTER-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'ENTERABLE'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634491121850016616)
,p_name=>'P141_NOMBRE_GARANTE'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(7710804106088414935)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634491456227016617)
,p_name=>'P141_CLI_ID_GARANTE'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(7710804106088414935)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634492244691016618)
,p_name=>'P141_TVE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_item_default=>'1'
,p_prompt=>'Forma Venta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TERMINOS_VENTA_OVENTA_RAP'
,p_lov=>'SELECT L.TVE_DESCRIPCION D, L.TVE_ID R FROM VEN_TERMINOS_VENTA L WHERE L.TVE_ID IN (:F_TVE_CONTADO,:F_TVE_TC,2) and L.EMP_ID = :F_EMP_ID AND L.TVE_ESTADO_REGISTRO = 0'
,p_cHeight=>1
,p_tag_attributes2=>'onchange="doSubmit(''LIMPIA_VENTA'')"'
,p_new_grid=>true
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Terminos de venta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634493143827016620)
,p_name=>'P141_POL_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_prompt=>unistr('Pol\00EDtica:')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_POLITICAS_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.pol_id || '' - '' || a.pol_descripcion_larga D, a.pol_id R',
'  FROM ppr_politicas                a,',
'       ppr_politicas_terminos_venta b,',
'       ppr_politicas_agencia        c',
' WHERE b.pol_id = a.pol_id',
'   AND c.pol_id = a.pol_id',
'   AND a.emp_id = :f_emp_id',
'   AND a.pol_estado_registro = ''0''',
'   AND b.ptv_estado_registro = ''0''',
'   AND a.tse_id = :f_seg_id',
'   AND b.tve_id = nvl(:P141_TVE_ID,0)',
'   AND c.age_id = :f_age_id_agencia',
'   AND a.pol_fecha_desde <= trunc(SYSDATE)',
'   AND a.pol_fecha_hasta >= trunc(SYSDATE)',
'   AND c.pag_fecha_desde <= trunc(SYSDATE)',
'   AND c.pag_fecha_hasta >= trunc(SYSDATE)',
'   AND NOT EXISTS',
' (SELECT NULL FROM ppr_politicas_clientes d WHERE d.pol_id = a.pol_id)',
'UNION ALL',
'SELECT a.pol_id || '' - '' || a.pol_descripcion_larga D, a.pol_id R',
'  FROM ppr_politicas                a,',
'       ppr_politicas_terminos_venta b,',
'       ppr_politicas_clientes       d',
' WHERE b.pol_id = a.pol_id',
'   AND d.pol_id = a.pol_id',
'   AND d.cli_id = NVL(:P141_CLI_ID,0)',
'   AND a.emp_id = :f_emp_id',
'   AND a.pol_estado_registro = :F_ESTADO_REG_ACTIVO',
'   AND d.pcl_estado_registro = :F_ESTADO_REG_ACTIVO',
'   AND d.emp_id = :f_emp_id',
'   AND a.tse_id = :f_seg_id',
'   AND b.tve_id = nvl(:P141_TVE_ID,0)',
'   AND a.pol_fecha_desde <= trunc(SYSDATE)',
'   AND a.pol_fecha_hasta >= trunc(SYSDATE)',
'   AND d.pcl_fecha_desde <= trunc(SYSDATE)',
'   AND d.pcl_fecha_hasta >= trunc(SYSDATE)',
' ORDER BY 2'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_TVE_ID'
,p_ajax_items_to_submit=>'P141_TVE_ID'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P141_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634493532845016621)
,p_name=>'P141_PLAZO_FACTURA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_prompt=>'Plazo:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_PLAZOS_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_retorna_plazos(:F_EMP_ID,:P141_POL_ID,:P141_TVE_ID,:P0_ERROR); ',
'  RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P141_POL_ID'
,p_ajax_items_to_submit=>'P141_POL_ID,P141_TVE_ID'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_colspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634493893484016621)
,p_name=>'P141_FECHA_CORTE_CRE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_prompt=>'Fecha 1er Venc.:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_FECHAS_CORTE2'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c001 DESCRIPCION,',
'       a.c002 VALOR',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_FECHAS_CORTE'''))
,p_lov_cascade_parent_items=>'P141_PLAZO_FACTURA'
,p_ajax_items_to_submit=>'P141_PLAZO_FACTURA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>':P141_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634494305042016622)
,p_name=>'P141_DIAS_GRACIA_PRIMER_VENC'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_prompt=>'D/Gracia primer Venc:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'CENTER-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634495381955016625)
,p_name=>'P141_BANCOS'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(8125930303716139906)
,p_prompt=>'Bancos'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_BANCOS_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_bancos(:f_emp_id,:P141_POL_ID, :P141_TVE_ID);',
' ',
'   RETURN(lv_lov);',
'',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_POL_ID'
,p_ajax_items_to_submit=>'P141_POL_ID'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'')) and :P141_PINPAD = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634495842836016625)
,p_name=>'P141_TARJETA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(8125930303716139906)
,p_prompt=>'Tarjeta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TARJETAS_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'lv_sentencia varchar2(3000);',
'BEGIN',
'lv_sentencia:=pq_ven_listas.fn_lov_tarjeta_credito(:F_EMP_ID,nvl(:P141_BANCOS,0));',
'return(lv_sentencia);',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_BANCOS'
,p_ajax_items_to_submit=>'P141_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'')) and :P141_PINPAD = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634496167957016626)
,p_name=>'P141_TIPO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(8125930303716139906)
,p_prompt=>'Tipo'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_TAR_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'lv_sentencia varchar2(3000);',
'BEGIN',
'lv_sentencia:=pq_ven_listas.fn_lov_tarjeta_credito_tipo(:F_EMP_ID,nvl(:P141_TARJETA,0));',
'return(lv_sentencia);',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_TARJETA'
,p_ajax_items_to_submit=>'P141_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'')) and :P141_PINPAD = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634496608386016626)
,p_name=>'P141_PLAN_TARJETA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(8125930303716139906)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                                  ''cn_ede_plan_tarjeta'')'))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Plan Tarjeta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_PLAN_TC_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'lv_sentencia varchar2(3000);',
'BEGIN',
'lv_sentencia:=pq_ven_listas.fn_lov_tarjeta_credito_plan(:F_EMP_ID,nvl(:P141_TIPO,0));',
'return(lv_sentencia);',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_TIPO'
,p_ajax_items_to_submit=>'P141_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>':P141_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'')) and :P141_PINPAD = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634497288311016630)
,p_name=>'P141_AGE_ID_AGENTE_CAB'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(8125930513066142533)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'return pq_ven_listas.fn_retorna_vended_cab_ord_def(:f_emp_id,:p141_cli_id,:f_seg_id,:f_user_id, :F_UGE_ID, :P0_ERROR); -- PAOBERNAL 18/OCT/2018',
''))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Vendedor'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_VENDEDOR_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT g.age_id || ''- '' || u.nombre d, g.age_id r',
'  FROM asdm_agentes          g,',
'       kseg_usuarios         u,',
'       asdm_tipos_agentes    tag,',
'       asdm_agentes_tagentes a',
' WHERE g.usu_id = u.usuario_id',
'   AND tag.tag_id = a.tag_id -- H1 mzeas 14/02/2019',
'   AND tag.tag_vende = ''S''',
'	 AND a.ata_comisiona =''S''',
'   AND a.uge_id =  :f_uge_id',
'   AND g.age_id = a.age_id	',
'   AND a.ata_estado_registro = 0',
'   AND tag.tag_estado_registro = 0',
'   AND trunc(SYSDATE) BETWEEN nvl(a.ata_fecha_ini, trunc(SYSDATE)) AND',
'       nvl(a.ata_fecha_fin, trunc(SYSDATE))',
'   AND g.age_estado_registro = 0'))
,p_lov_display_null=>'YES'
,p_lov_translated=>'Y'
,p_lov_null_text=>'--Seleccione --'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634497709453016632)
,p_name=>'P141_COBRADOR'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(8125930513066142533)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634498086866016632)
,p_name=>'P141_TIPO_VENTA_ORDEN'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(8125930513066142533)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634499207619016634)
,p_name=>'P141_CUPO_CORRECTO'
,p_item_sequence=>975
,p_item_plug_id=>wwv_flow_imp.id(7703015196854929590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634506556510016653)
,p_name=>'P141_ENTRADA_CRE'
,p_item_sequence=>702
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_prompt=>'Entrada Cre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634506973771016654)
,p_name=>'P141_VALOR_FINANCIAR_CRE'
,p_item_sequence=>712
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_prompt=>'Valor Financiar Cre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634507355079016654)
,p_name=>'P141_TASA_CRE'
,p_item_sequence=>722
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_prompt=>'Tasa Cre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634507755805016655)
,p_name=>'P141_CUOTA_CRE'
,p_item_sequence=>732
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_item_default=>'0'
,p_prompt=>'Cuota Cre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634508172898016655)
,p_name=>'P141_PLAZO_CRE'
,p_item_sequence=>742
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634508631525016656)
,p_name=>'P141_DESCUENTO_ROL'
,p_item_sequence=>822
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634509016846016658)
,p_name=>'P141_INSTITUCION'
,p_item_sequence=>1084
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_prompt=>unistr('<SPAN STYLE="color:RED"> Instituci\00F3n: </span>')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P141_DESCUENTO_ROL = pq_constantes.fn_retorna_constante(null,''cv_si'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634509400194016658)
,p_name=>'P141_ERROR_DESCUENTO_ROL'
,p_item_sequence=>1114
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_prompt=>'<SPAN STYLE="color:RED">Bloqueado Descuento Rol:</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>2
,p_rowspan=>2
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_ERROR_DESCUENTO_ROL IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634509835429016658)
,p_name=>'P141_TOTAL_CREDITO'
,p_item_sequence=>1194
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634551456917016717)
,p_name=>'P141_NUMERO_DETALLES'
,p_item_sequence=>1027
,p_item_plug_id=>wwv_flow_imp.id(7703027593691929597)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select count(1)',
'       from apex_collections',
'     where collection_name = ''CO_DETALLE'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634587892953016782)
,p_name=>'P141_GEX'
,p_item_sequence=>1144
,p_item_plug_id=>wwv_flow_imp.id(7708031822256169243)
,p_prompt=>'Gex'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_GEX'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_retorna_gex_item(pn_emp_id     => :f_emp_id,',
'                                                  pn_ite_sku_id => nvl(:P141_ITE_SKU_ID,0),',
'                                                  pn_cei_id     => nvl(:P141_ESTADO,0),',
'                                                  pv_error      => :p0_error);',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634588299391016782)
,p_name=>'P141_CANTIDAD_GEX'
,p_item_sequence=>1154
,p_item_plug_id=>wwv_flow_imp.id(7708031822256169243)
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>4000
,p_tag_attributes=>'onchange="doSubmit(''carga_detalle'')"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P141_GEX'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634588711670016782)
,p_name=>'P141_TIPO_ITEM'
,p_item_sequence=>1164
,p_item_plug_id=>wwv_flow_imp.id(7708031822256169243)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634589355508016783)
,p_name=>'P141_SEQ_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634589798871016783)
,p_name=>'P141_VIENE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_item_default=>':f_emp_id||'' iteSKU ''||:p141_ite_sku_id||'' ieb ''||:p141_ieb_id||'' estado ''||:p141_estado||'' polit''||:p141_pol_id||'' canti''|| :p141_cantidad||'' descto ''||:p141_tipo_descto||'' desctLINEA ''||:p141_descto_linea||'' desctPUNTOS ''||:p141_descto_puntos||'' de'
||'sctFACTURA ''||:p141_descto_factura||''TVE ''||:p141_tve_id||'' FACTURA''||:p141_plan_tarjeta||'' entrada ''||:p141_entrada||'' plazoFACT ''||:p141_plazo_factura||'' agente ''||:p141_age_id_agente||'' agencia''||:p141_age_id_agencia||'' plazoFACT ''||:p141_plazo_fa'
||'ctura||'' poceNACIONAL''||:p141_poce_nacional||'' entraMINIma ''||:p141_entrada_minima||'' bodeg ''||:p141_bodega||'' lcm''||:p141_lcm_id||'' client ''||:p141_cli_id||''com_id ''|| :p141_com_id||'' tipo_nc ''||:p141_tipo_nc'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634590225356016784)
,p_name=>'P141_ITE_SKU_MOD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634590606853016785)
,p_name=>'P141_IID_VALOR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634591032974016785)
,p_name=>'P141_ITE_PERMITE_BACK_ORDERS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634591392826016787)
,p_name=>'P141_LABEL_TIPO_DESCUEN'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_ven_listas.fn_retorna_label_valor_desc(:P141_TIPO_DESCTO);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634591817813016788)
,p_name=>'P141_ESTADO_DES'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634592151939016788)
,p_name=>'P141_CAT_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_ven_listas.fn_retorna_cat_id_tipo_stock(:P141_ITE_SKU_ID,:F_EMP_ID);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634592595953016788)
,p_name=>'P141_LCM_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_prompt=>'Local Cliente'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_LOCALESCLIENTESMAYOREO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_locales_cliente(nvl(:P16_CLI_ID,0), :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(null,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_POL_ID IS NOT NULL',
'and :P141_PLAZO_FACTURA is not null',
'and :f_seg_id = pq_constantes.fn_retorna_constante(null,''cn_tse_id_mayoreo'')'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634593023836016789)
,p_name=>'P141_ITE_SKU_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'ALWAYS'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634593383761016789)
,p_name=>'P141_AGE_ID_AGENTE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_use_cache_before_default=>'NO'
,p_source=>'P141_AGE_ID_AGENTE_CAB'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634593789450016789)
,p_name=>'P141_TLD_ID'
,p_item_sequence=>1518
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634595264158016791)
,p_name=>'P141_IEB_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634596130491016792)
,p_name=>'P141_BODEGA'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_item_default=>'137'
,p_prompt=>'Bodega Despacho'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_BODEGAS_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov VARCHAR2(8000);',
'BEGIN',
'',
'',
'    lv_lov := pq_ven_listas.fn_lov_bodegas(:F_AGE_ID_AGENCIA,',
'                                           :f_emp_id,',
'                                           :F_SEG_ID,',
'                                           nvl(:p20_lcm_id,0), ',
'                                           :P0_ERROR);',
'',
'',
'    RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_ORD_TIPO NOT IN (pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig''),pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig_moto''))',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_POL_ID IS NOT NULL',
'AND ',
':P141_ORD_TIPO NOT IN (pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig''),pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig_moto''))',
'and :P141_PLAZO_FACTURA is not null'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634596551041016792)
,p_name=>'P141_ESTADO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_item_default=>'20'
,p_prompt=>'Estado'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTADO_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov :=  ''select ''''Sin estado'''', -1 from dual'';',
'  IF :P141_ORD_TIPO IS NOT NULL THEN',
'  lv_lov := pq_ven_listas.fn_lov_estados_calidad(:f_emp_id,:P141_ORD_TIPO,:f_seg_id,:P0_ERROR); ',
'  END IF;',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return  pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_cei_id_ventas'') ;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634596886445016792)
,p_name=>'P141_ITE_SEC_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_prompt=>'Item'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ITEMS_OVENTA_RAP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'lv_lov varchar2(32000);',
'',
'begin',
'',
'    pq_ven_listas2.pr_lov_items_ordenes  (pn_emp_id => :F_EMP_ID,',
'                                          pn_bpr_id => :P141_BODEGA,',
'                                          pn_cei_id => :P141_ESTADO,',
'                                          pn_age_id_agencia => :F_AGE_ID_AGENCIA,',
'                                          pv_ord_tipo => :P141_ORD_TIPO,',
'                                          pn_cli_id => :P141_CLI_ID,',
'                                          pn_age_id_agente => :P141_AGE_ID_AGENTE,',
'                                          pn_lcm_id => null,',
'                                          pn_pol_id => :P141_POL_ID,',
'                                          pn_tse_id => :f_seg_id,',
'                                        --  pv_tipo_venta => :P20_ORD_TIPO,',
'                                          pv_lov => lv_lov,',
'                                          PV_SESION => :APP_SESSION,  -- PAOBERNAL 31/OCTUBRE/2018',
'                                          pn_usu_id => :F_USER_ID,    -- PAOBERNAL 31/OCTUBRE/2018                                    ',
'                                          pv_error => :p0_error);',
'                                          ',
'  return (lv_lov);',
'  ',
'end;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_BODEGA,P141_ESTADO,P141_ORD_TIPO'
,p_ajax_items_to_submit=>'P141_BODEGA,P141_ESTADO,P141_ORD_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>5
,p_tag_attributes=>'onChange=''fnEnter1(this)'''
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634598166127016794)
,p_name=>'P141_STOCK'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_use_cache_before_default=>'NO'
,p_prompt=>'<SPAN STYLE="font-size: 10pt;;color:Darkslategray">Stock</span>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return ',
'pq_ven_listas.fn_retorna_stock(:P141_ITE_SKU_ID,:P141_ESTADO,:P141_BODEGA,:P141_LCM_ID,:F_EMP_ID,:P141_ORD_TIPO,:f_seg_id);'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>3
,p_cMaxlength=>2000
,p_tag_attributes=>'style="font-size:16;color:GREEN;font-family:Arial Narrow" readonly'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634598620562016795)
,p_name=>'P141_DESCRIPCION_LARGA'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_prompt=>unistr('<SPAN STYLE="font-size: 10pt;;color:Darkslategray">Descripci\00F3n Item</span>')
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>40
,p_cMaxlength=>2000
,p_cHeight=>2
,p_tag_attributes=>'style="font-size:16;color:BLUE;font-family:Arial Narrow" readonly'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P141_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634598992391016795)
,p_name=>'P141_TIPO_DESCTO'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_item_default=>'1'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DESCR, VAL',
'  FROM (SELECT ''%(Valor entero ejem 3%)'' DESCR, ''1'' VAL, ''1'' ORD',
'          FROM DUAL',
'        UNION',
'        SELECT ''Valor'' DESCR, ''0'' VAL, ''2'' ORD',
'          FROM DUAL',
'        UNION',
'        SELECT ''%Margen(Valor entero ejem 25%)'' DESCR, ''2'' VAL, ''3'' ORD',
'          FROM DUAL',
'        UNION',
'        SELECT ''Precio'' DESCR, ''3'' VAL, ''4'' ORD',
'          FROM DUAL',
'         WHERE :F_SEG_ID = ASDM_P.pq_constantes.fn_retorna_constante(0,''cn_tse_id_minoreo''))',
' ORDER BY ORD ASC'))
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P141_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>':p141_accion <> ''E'' OR :P141_DESCUENTO_AUTO=''S'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634599359459016795)
,p_name=>'P141_PLAZO_LISTA'
,p_item_sequence=>194
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_prompt=>'Plazo<br>Lista'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'onchange="doSubmit()"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(null,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634600217445016796)
,p_name=>'P141_DESCTO_LINEA'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_item_default=>'0'
,p_prompt=>'<SPAN STYLE="font-size: 10pt;;color:Darkslategray">&P141_LABEL_TIPO_DESCUEN.</span>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_TVE_ID IS NOT NULL AND NVL(:P141_ITEM_COMBO_YN,''N'') <> ''S'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when=>':p141_accion <> ''E'' OR :P141_DESCUENTO_AUTO=''S'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634601811615016797)
,p_name=>'P141_CANTIDAD'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_prompt=>'<SPAN STYLE="font-size: 10pt;;color:Darkslategray">Cantidad</span>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c012 FROM apex_collections a WHERE a.collection_name=''CO_DETALLE''',
'AND a.c003=:P141_ITE_SKU_ID and a.c031=:P141_BODEGA and a.c006 is null'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>2000
,p_tag_attributes=>'onkeypress="javascript:if (getKeyCode(event)==13) doSubmit(''carga_detalle'')"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*P141_ITE_SKU_ID*/',
':P141_ITE_SKU_ID IS NOT NULL AND ',
'(',
'(NVL(:P141_STOCK,0) > 0 AND :P141_TSE_ID = pq_constantes.fn_retorna_constante(null,''cn_tse_id_minoreo'')) OR',
'(:P141_TSE_ID = pq_constantes.fn_retorna_constante(null,''cn_tse_id_mayoreo'') OR',
':P141_CAT_ID_ITEM IS NOT NULL)',
'',
')'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when=>':P141_ORD_ID IS NOT NULL AND :P141_ORD_MODIFICA IS NULL '
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634602153650016797)
,p_name=>'P141_PROMOCION_SN'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>'select * from inv_items_categorias where cat_id in ( pq_constantes.fn_retorna_constante(:f_emp_id,''cn_cat_id_promocional'') ,( pq_constantes.fn_retorna_constante(:f_emp_id,''cn_cat_id_mixto''))) and ite_sku_id=:P141_ITE_SKU_ID'
,p_display_when_type=>'EXISTS'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634603033217016798)
,p_name=>'P141_FACTOR_MARGEN'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_ppr_polit_precios.fn_get_factor(pq_constantes.fn_retorna_constante(0,''cn_fac_id_margen''),:P141_POL_ID,:P141_ITE_SKU_ID||''::''||:P141_ESTADO||'':''||:P141_BODEGA)*100;'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>'P141_POL_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634603364060016798)
,p_name=>'P141_ITEM_COMBO_YN'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634603760090016798)
,p_name=>'P141_CODIGO_ITEM_COMBO'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634604193044016798)
,p_name=>'P141_NUMERO_LINEAS'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    pq_ven_ordenes.fn_numero_lineas_factura(:F_EMP_ID)',
'FROM      DUAL;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634604637335016799)
,p_name=>'P141_CAT_ID_ITEM'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(7703015409250929590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634606390743016808)
,p_name=>'P141_RIT_OPCION'
,p_item_sequence=>1488
,p_item_plug_id=>wwv_flow_imp.id(7915554602236646310)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
' RETURN ASDM_P.PQ_VEN_PROMOS_COMBOS.fn_P20_RIT_OPCION_DEFAULT(pn_emp_id            => :F_emp_id,',
'                                                              pn_ite_sku_principal => :p141_ite_sku_id,',
'                                                              pn_tse_id            => :F_SEG_ID,',
'                                                              pn_pol_id            => :P141_POL_ID); -- PAOBERNAL 24/OCT/2018'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Opcion Promo Combo'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_PROMOS_COMBOS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct l.rit_opcion || '' - '' || l.rit_observacion D,',
'                l.rit_opcion R',
'  from inv_relaciones_item l',
' where l.ite_sku_padre =:p141_ite_sku_id',
' and l.emp_id = :F_EMP_ID',
' and l.rit_estado_registro = :F_ESTADO_REG_ACTIVO',
'',
'and pq_ven_promos_combos.fn_retorna_combo_politica(pn_pol_id => :P141_POL_ID ,pn_tre_id =>l.tre_id, pn_ite_sku_padre => l.ite_sku_padre , pv_rit_opcion => l.rit_opcion) > 0',
' and l.ite_sku_padre in (select distinct r.ite_sku_padre',
'                       from inv_relaciones_item r',
'                      where r.ite_sku_hijo = :p141_ite_sku_id',
'                        and r.tre_id = pq_constantes.fn_retorna_constante(:F_emp_id,''cn_tre_id_promocion''))',
'                        and l.tse_id = :F_SEG_ID ',
' order by rit_opcion'))
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>' nvl(:P141_DESCUENTO_AUTO , ''N'') = ''N'' AND :P141_ITE_SKU_ID_REL IS NULL'
,p_display_when2=>'SQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634635499436016844)
,p_name=>'P141_PROMOS_COMBOS'
,p_item_sequence=>1478
,p_item_plug_id=>wwv_flow_imp.id(23016185918896612843)
,p_prompt=>'Sin Combos'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC2: ;N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634651676700016863)
,p_name=>'P141_ORD_TIPO'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_prompt=>'Ord Tipo'
,p_source=>'TRIM(:F_PARAMETRO)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634655692210016866)
,p_name=>'P141_DIA_CORTE'
,p_item_sequence=>1091
,p_item_plug_id=>wwv_flow_imp.id(7650067924233056990)
,p_source=>'P15_DIA_CORTE'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634915610686890570)
,p_name=>'P141_CLI_ID'
,p_item_sequence=>23
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_prompt=>'Cli_id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634916008378890570)
,p_name=>'P141_PER_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634916361671890573)
,p_name=>'P141_NRO_IDE_AUX'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634916845221890573)
,p_name=>'P141_PER_TIPO_IDENTIFICACION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_item_default=>'CED'
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634917245099890574)
,p_name=>'P141_PER_NRO_IDENTIFICACION'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_prompt=>'Nro Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(decode(a.per_primer_nombre,',
'                  NULL,',
'                  NULL,',
'                  rtrim(a.per_primer_nombre)) ||',
'           decode(a.per_segundo_nombre,',
'                  NULL,',
'                  NULL,',
'                  '' '' || rtrim(a.per_segundo_nombre)) ||',
'           decode(a.per_primer_apellido,',
'                  NULL,',
'                  NULL,',
'                  '' '' || rtrim(a.per_primer_apellido)) ||',
'           decode(a.per_segundo_apellido,',
'                  NULL,',
'                  NULL,',
'                  '' '' || rtrim(a.per_segundo_apellido) || '' ->'' || a.per_id),',
'           a.per_razon_social) || '' - '' || a.per_nro_identificacion d,',
'       a.per_nro_identificacion r',
'  FROM asdm_personas a',
' WHERE a.per_tipo_identificacion = :p141_per_tipo_identificacion',
'   AND a.emp_id = :f_emp_id'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P141_PER_TIPO_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>50
,p_tag_attributes=>'onChange=''fnEnter()'''
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634917574343890575)
,p_name=>'P141_PER_CORREO_ELECTRONICO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_prompt=>'Correo Electronico'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634918002306890575)
,p_name=>'P141_PER_PRIMER_NOMBRE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634918402406890576)
,p_name=>'P141_PER_SEGUNDO_NOMBRE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634918756828890576)
,p_name=>'P141_PER_PRIMER_APELLIDO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634919151978890576)
,p_name=>'P141_PER_SEGUNDO_APELLIDO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634919592559890576)
,p_name=>'P141_PER_NOMBRE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634920004415890577)
,p_name=>'P141_TDI_ID'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634920369179890577)
,p_name=>'P141_DIRECCION'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634920784707890577)
,p_name=>'P141_TTE_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634921233345890578)
,p_name=>'P141_TELEFONO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7634921610001890578)
,p_name=>'P141_URL'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15320759734207669039)
,p_name=>'P141_TEO_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15342032075223429157)
,p_name=>'P141_REALIZAR_PAGO'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(7703021206872929594)
,p_item_default=>'N'
,p_prompt=>'Realiza Pago'
,p_source=>'N'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTATICO_SI_NO'
,p_lov=>'.'||wwv_flow_imp.id(28584042131898005164)||'.'
,p_cHeight=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38545306028388500035)
,p_name=>'P141_ITE_SKU_ID_REL'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(7710809722685859632)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63172609747844562141)
,p_name=>'P141_TEL_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(80146220026847509327)
,p_name=>'P141_REQUEST'
,p_item_sequence=>333
,p_item_plug_id=>wwv_flow_imp.id(7703014995810929589)
,p_item_default=>'V(''REQUEST'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(120774608086979703929)
,p_name=>'P141_ORDEN_EXTERNA_REF_VTEX'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(120774607976337703928)
,p_prompt=>'Orden externa ref vtex'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(121053647432738353452)
,p_name=>'P141_ORIGEN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(120774607976337703928)
,p_prompt=>'Origen:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>unistr('STATIC:Vtex;VTEX,Archivo Plano;ARCH,Cotizado M\00F3vil;COTM,Rappid;RAPP')
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(130257710656144800829)
,p_name=>'P141_EDITAR_MAPA'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(38244419044147254311)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153809356167938533869)
,p_name=>'P141_FORMA_CREDITO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pq_car_ecredit_biometria.fn_aplica_ecredit(pn_cli_id   => :p141_cli_id,',
'                                        pn_nro_ide  => :p141_per_nro_identificacion,',
'                                        pn_age_id   => :f_age_id_agencia,',
'                                        pn_tve_id   => :p141_tve_id,',
'                                        pn_emp_id   => :f_emp_id,',
'                                        pd_f_minima => trunc(SYSDATE),',
'                                        pn_cxc_id   => NULL,',
'                                        pn_ord_id   => NULL) valor',
'  FROM dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(206176473317293967436)
,p_name=>'P141_TIPO_PLAN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(8125930095751137551)
,p_item_default=>'COR'
,p_prompt=>'Tipo plan Tarjeta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:DIFERIDO;DIF,CORRIENTE;COR'
,p_cHeight=>1
,p_tag_attributes2=>'onchange="doSubmit(''LIMPIA_VENTA'')"'
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P141_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(7634666153721016897)
,p_computation_sequence=>20
,p_computation_item=>'P0_DESPLIEGA_AUT'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    if :P141_ENTRADA is null then',
'        return ''N'';',
'    end if;',
'    if :P141_ENTRADA_MINIMA > nvl(:P141_ENTRADA,0) then',
'        return ''S'';',
'    end if;',
'end;'))
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(7634666551565016897)
,p_computation_sequence=>30
,p_computation_item=>'P0_TRX_AUT'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_computation=>'86'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634669800592016905)
,p_validation_name=>'VALIDA ENTRADA'
,p_validation_sequence=>20
,p_validation=>'nvl(:P141_ENTRADA,0) > to_number(nvl(:P141_ENTRADA_MINIMA,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Entrada no puede ser menor a la minima, Solicite Autorizacion'
,p_validation_condition=>':P141_TVE_ID = 2'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(100860158454678114)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634670225119016906)
,p_validation_name=>'Cantidad'
,p_validation_sequence=>50
,p_validation=>':p141_cantidad > 0 and :p141_cantidad is not null'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese una cantidad correcta'
,p_validation_condition=>':request=''carga_detalle'' and :p141_tipo_venta=''Ven'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634601811615016797)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634672987173016907)
,p_validation_name=>'Cantidad stock'
,p_validation_sequence=>60
,p_validation=>'to_number(:p141_cantidad) <= to_number(:P141_STOCK);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'La cantidad debe ser menor al Stock'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:p141_cat_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_cat_id_stock'') ',
'and :P141_TSE_ID = pq_constantes.fn_retorna_constante(null,''cn_tse_id_minoreo''))',
'or :F_PARAMETRO = ''FCO''',
'and :request = ''carga_detalle'''))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634601811615016797)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634667383613016903)
,p_validation_name=>'P141_AGE_ID_AGENTE_CAB'
,p_validation_sequence=>80
,p_validation=>'P141_AGE_ID_AGENTE_CAB'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Error!! el cliente no tiene vendedor asignado'
,p_validation_condition=>'carga_detalle'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(7634497288311016630)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634667835317016904)
,p_validation_name=>'stock_0'
,p_validation_sequence=>100
,p_validation=>'to_number(:P141_STOCK) > 0;'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'No pondemos hacer ordenes si el stock es 0!'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*(:p141_cat_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_cat_id_stock'') ',
'and :P141_TSE_ID = pq_constantes.fn_retorna_constante(null,''cn_tse_id_minoreo''))',
'or :F_PARAMETRO = ''FCO''',
'and :request = ''carga_detalle''*/',
'',
'(:p141_cat_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_cat_id_stock'') ',
'and :P141_TSE_ID = pq_constantes.fn_retorna_constante(null,''cn_tse_id_minoreo'')',
'AND NVL(:P141_CANTIDAD,0) > 0)',
'or :F_PARAMETRO = ''FCO''',
'and :request = ''carga_detalle'' ',
'AND NVL(:P141_CANTIDAD,0) > 0'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634601811615016797)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634668212059016904)
,p_validation_name=>'IID_VALOR_REPETIDO'
,p_validation_sequence=>110
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT count(i.ite_sku_id)',
'          FROM inv_items_identificador i, inv_items it',
'         WHERE i.iid_valor = :P141_IID_VALOR',
'',
'           AND i.ite_sku_id = it.ite_sku_id',
'           AND i.tid_id =',
'               pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                  ''cn_tid_id_identificador'')',
'   and it.ite_estado_registro =0',
'           and i.iid_estado_registro =0  ',
')<= 1'))
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Error.... numero de identificadores repetidos'
,p_validation_condition=>'busca_barras'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(7634590606853016785)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634668618491016905)
,p_validation_name=>'VALIDA ENTRADA_GRABAR'
,p_validation_sequence=>120
,p_validation=>'nvl(:P141_ENTRADA,0) > to_number(nvl(:P141_ENTRADA_MINIMA,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Entrada no puede ser menor a la minima, Solicite Autorizacion'
,p_validation_condition=>':P141_TVE_ID = 2'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(100853451507678101)
,p_associated_item=>wwv_flow_imp.id(7634445217597016558)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634669001036016905)
,p_validation_name=>'VALIDA ENTRADA NULA'
,p_validation_sequence=>130
,p_validation=>'P141_ENTRADA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'La entrada no puede ser nula'
,p_validation_condition=>':request=''Recalcular'' and :request=''CRONOGRAMA_PAGOS'' AND :P141_ORD_TIPO = pq_constantes.fn_retorna_constante(NULL,''cv_ord_tipo_orden_venta'')'
,p_validation_condition_type=>'NEVER'
,p_associated_item=>wwv_flow_imp.id(7634445217597016558)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634671000322016906)
,p_validation_name=>'P141_BODEGA'
,p_validation_sequence=>160
,p_validation=>'P141_BODEGA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione una Bodega de despacho'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''carga_detalle'' and',
':P141_ORD_TIPO != ''FCO'' and :P141_ORD_TIPO != ''FCOMO'''))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634596130491016792)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634666973970016898)
,p_validation_name=>'P141_DESCTO_LINEA'
,p_validation_sequence=>170
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :P141_DESCTO_LINEA > :P141_FACTOR_MARGEN THEN',
'RETURN  ''EL % DE MARGEN INGRESADO ES MAYOR AL DE LA POLITICA'';',
'',
'ELSE',
'',
'RETURN  NULL;',
'',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'EL % DE MARGEN INGRESADO ES MAYOR AL DE LA POLITICA'
,p_validation_condition=>'P141_TIPO_DESCTO'
,p_validation_condition2=>'2'
,p_validation_condition_type=>'NEVER'
,p_associated_item=>wwv_flow_imp.id(7634600217445016796)
,p_error_display_location=>'INLINE_WITH_FIELD'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634671763110016907)
,p_validation_name=>'P141_PLAZO_FACTURA'
,p_validation_sequence=>180
,p_validation=>'P141_PLAZO_FACTURA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione el plazo'
,p_when_button_pressed=>wwv_flow_imp.id(100853451507678101)
,p_associated_item=>wwv_flow_imp.id(7634493532845016621)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634672180698016907)
,p_validation_name=>'Cantidad Detalles'
,p_validation_sequence=>190
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*to_number(:P141_NUMERO_DETALLES) <= 10*/',
'to_number(:P141_NUMERO_DETALLES) <= 15;--TO_NUMBER(:P141_NUMERO_LINEAS);'))
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('No puede ingresar m\00E1s articulos en una sola orden')
,p_validation_condition=>':request = ''carga_detalle'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634601811615016797)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634672576532016907)
,p_validation_name=>'P141_POL_ID'
,p_validation_sequence=>200
,p_validation=>'P141_POL_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Seleccione la pol\00EDtica')
,p_validation_condition=>'CRONOGRAMA_PAGOS,GRABAR_CON_ENTREGA_DOMICILIO,recalcular'
,p_validation_condition_type=>'REQUEST_IN_CONDITION'
,p_associated_item=>wwv_flow_imp.id(7634493143827016620)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634673824977016908)
,p_validation_name=>'P141_CANTIDAD_GEX'
,p_validation_sequence=>210
,p_validation=>':P141_CANTIDAD_GEX > 0 AND :P141_CANTIDAD_GEX <= :P141_CANTIDAD'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'No puede ser 0 y tampoco mayor a la cantidad del item principal.'
,p_validation_condition=>'P141_GEX'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_associated_item=>wwv_flow_imp.id(7634588299391016782)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634674560631016908)
,p_validation_name=>'P141_CANTIDAD'
,p_validation_sequence=>220
,p_validation=>'P141_CANTIDAD'
,p_validation2=>'^[0-9]+$'
,p_validation_type=>'REGULAR_EXPRESSION'
,p_error_message=>'El item solo permite valores Enteros'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT nvl(unm_numero_decimales ,0)',
'FROM inv_items a, inv_unidades_medida b',
'WHERE b.unm_id = a.unm_id_principal',
'AND a.ite_sec_id = :P141_ITE_SEC_ID',
'AND a.emp_id = :f_emp_id)=0;'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634601811615016797)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7634673403311016907)
,p_validation_name=>'P141_PLAN_TARJETA'
,p_validation_sequence=>230
,p_validation=>'P141_PLAN_TARJETA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe ingresar el Plan de la Tarjeta'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:request in (''busca_item'',''carga_detalle'') and :P141_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito'') and :P141_PINPAD = 0) or',
'(:request in (''busca_item'',''carga_detalle'') and :P141_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'') and :P141_POL_INDUCCION > 0)'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(7634496608386016626)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634693541470016939)
,p_name=>'AC_CAMBIA_BODEGA_PROMOS_COMBOS'
,p_event_sequence=>1
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.bodegac11'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634694015059016940)
,p_event_id=>wwv_flow_imp.id(7634693541470016939)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(23016185918896612843)
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.event.trigger("#Promociones","apexrefresh");',
''))
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634688388187016930)
,p_name=>'PR_TIPO_DESC'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.tipo_desc'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634688941687016932)
,p_event_id=>wwv_flow_imp.id(7634688388187016930)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var v_result;',
'var lv_value;',
'var ln_tipo_desc;',
'var ln_valor_desc;',
'',
'$(document).ready(function(){',
'    $(''.tipo_desc'').each( function() {      ',
'          lv_value     = this.value;',
'          if (this.checked == true){',
'',
'          var ajaxResult = new htmldb_Get(null,$x(''pFlowId'').value,''APPLICATION_PROCESS=PR_TIPO_DESC'',0);',
'                                                               ajaxResult.add(''F_TIPO_DESC'',lv_value);',
'                                                               v_result = ajaxResult.get();',
'',
'          }',
'     });',
' ',
'ln_tipo_desc=v_result.substr(0,v_result.indexOf("%")); ',
'ln_valor_desc=v_result.substr(v_result.indexOf("%")+1); ',
'',
'',
'     $(''.tipo_desc_item'').each( function() {',
'         var ln_v_result=ln_tipo_desc*1;',
'         var ln_tipo_des=this.value*1;         ',
'         if (ln_v_result==ln_tipo_des){',
'           this.checked=true;',
'         }',
'         ',
'     });',
'',
'if($v(''P141_TSE_ID'')==''22'')',
'{ ',
'  $s(''P141_DESCTO_LINEA'',ln_valor_desc*1);',
'};',
'',
'});',
''))
,p_da_action_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var v_result;',
'var lv_value;',
'var ln_tipo_desc;',
'var ln_valor_desc;',
'',
'$(document).ready(function(){',
'    $(''.tipo_desc'').each( function() {      ',
'          lv_value     = this.value;',
'          if (this.checked == true){',
'',
'          var ajaxResult = new htmldb_Get(null,$x(''pFlowId'').value,''APPLICATION_PROCESS=PR_TIPO_DESC'',0);',
'                                                               ajaxResult.add(''F_TIPO_DESC'',lv_value);',
'                                                               v_result = ajaxResult.get();',
'',
'          }',
'     });',
' ',
'ln_tipo_desc=v_result.substr(0,v_result.indexOf("%")); ',
'ln_valor_desc=v_result.substr(v_result.indexOf("%")+1); ',
'',
'',
'     $(''.tipo_desc_item'').each( function() {',
'         var ln_v_result=ln_tipo_desc*1;',
'         var ln_tipo_des=this.value*1;         ',
'         if (ln_v_result==ln_tipo_des){',
'           this.checked=true;',
'         }',
'         ',
'     });',
'',
'if($v(''P20_TSE_ID'')==''22'')',
'{ ',
'  $s(''P20_DESCTO_LINEA'',ln_valor_desc*1);',
'};',
'',
'});',
''))
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634689368147016933)
,p_event_id=>wwv_flow_imp.id(7634688388187016930)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P141_TIPO_DESCTO'
,p_attribute_05=>'PLSQL'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634689798303016934)
,p_name=>'PR_TIPO_DESC_MAY'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.tipo_descmay'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'focusout'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634690267118016934)
,p_event_id=>wwv_flow_imp.id(7634689798303016934)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var v_result;',
'var lv_value;',
'',
'$(document).ready(function(){',
'    $(''.tipo_descmay'').each( function() {      ',
'           if (this.value!=''''){',
'lv_value=this.id;',
'',
'}',
'',
'});',
'',
'     $(''.tipo_desc_item'').each( function() {',
'         var ln_v_result=lv_value*1;',
'         var ln_tipo_des=this.value*1;',
'         if (ln_v_result==ln_tipo_des){',
'           this.checked=true;',
'         }',
'         ',
'     });',
'',
'});'))
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634690799160016934)
,p_event_id=>wwv_flow_imp.id(7634689798303016934)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P141_TIPO_DESCTO'
,p_attribute_05=>'PLSQL'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634691201764016935)
,p_name=>'da_cambia_plan_tarjeta'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_PLAN_TARJETA'
,p_condition_element=>'P141_PLAN_TARJETA'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634691738601016935)
,p_event_id=>wwv_flow_imp.id(7634691201764016935)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634692118364016935)
,p_name=>'show_cli'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_DATOS_CLIENTE'
,p_condition_element=>'P141_DATOS_CLIENTE'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634694390755016940)
,p_name=>'AD_PLAZOS'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_POL_ID'
,p_condition_element=>'P141_TVE_ID'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'2'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'EXPRESSION'
,p_display_when_cond=>':p141_tve_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'')'
,p_display_when_cond2=>'PLSQL'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634694853067016941)
,p_event_id=>wwv_flow_imp.id(7634694390755016940)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_credito.pr_obtener_fechas_primer_pago(pn_cli_id                => :p141_cli_id,',
'                                               pn_emp_id                => :f_emp_id,',
'                                               pn_tse_id                => :f_seg_id,',
'                                               pd_fecha                 => sysdate,--:p141_ord_fecha,',
'                                               pn_pol_id                => :p141_pol_id,',
'                                               pn_dias_gracia_1_venc    => :p141_dias_gracia_primer_venc,',
'                                               pn_mes_gracia_entre_venc => :p141_meses_gracia_entre_venc,',
'                                               pn_plazo_venta           => :p141_plazo_factura,',
'                                               pv_error                 => :p0_error);'))
,p_attribute_02=>'P141_POL_ID'
,p_attribute_03=>'P141_FECHA_CORTE_CRE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634695439966016941)
,p_event_id=>wwv_flow_imp.id(7634694390755016940)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(23016185918896612843)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634695838766016941)
,p_name=>'ad_bod_estado'
,p_event_sequence=>55
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_BODEGA,P141_ESTADO,P141_PLAZO_FACTURA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_display_when_cond=>'P141_ITE_sKU_iD'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634696306497016942)
,p_event_id=>wwv_flow_imp.id(7634695838766016941)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_ITE_SKU_ID,P141_DESCRIPCION_LARGA,P141_CAT_ID,P141_STOCK,,P141_DESCUENTO_AUTO,P141_ITE_SEC_ID'
,p_attribute_01=>'PLSQL_EXPRESSION'
,p_attribute_04=>'NULL'
,p_attribute_07=>'P141_BODEGA,P141_ESTADO,P141_ITE_SKU_ID'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634696660594016942)
,p_name=>'ad_items'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_ITE_SEC_ID'
,p_condition_element=>'P141_ITE_SEC_ID'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634697233918016943)
,p_event_id=>wwv_flow_imp.id(7634696660594016942)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_item'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634697637093016943)
,p_name=>'ad_limpia'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_TVE_ID,P141_POL_ID'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'EXISTS'
,p_display_when_cond=>'select * From apex_collections where collection_name = ''CO_VARIABLES_DETALLE'''
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634698116997016943)
,p_event_id=>wwv_flow_imp.id(7634697637093016943)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'LIMPIA_VENTA'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634698532520016943)
,p_name=>'ad_estado'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_ESTADO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634698966617016943)
,p_event_id=>wwv_flow_imp.id(7634698532520016943)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(23016185918896612843)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634699362519016944)
,p_name=>'AD_RIT_OPCION'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_RIT_OPCION'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634699894981016944)
,p_event_id=>wwv_flow_imp.id(7634699362519016944)
,p_event_result=>'TRUE'
,p_action_sequence=>5
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P141_RIT_OPCION'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634700364286016944)
,p_event_id=>wwv_flow_imp.id(7634699362519016944)
,p_event_result=>'TRUE'
,p_action_sequence=>15
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(23016185918896612843)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634700834462016944)
,p_name=>'ad_plazo_recalcula'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_PLAZO_FACTURA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634701325550016945)
,p_event_id=>wwv_flow_imp.id(7634700834462016944)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_RECALCULA'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'RECALCULAR'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634701699744016945)
,p_name=>'REC_SIGUIENTE'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_RECALCULA'
,p_condition_element=>'P141_RECALCULA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'RECALCULAR'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634702247792016947)
,p_event_id=>wwv_flow_imp.id(7634701699744016945)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'BUTTON'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634702673383016948)
,p_event_id=>wwv_flow_imp.id(7634701699744016945)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'JQUERY_SELECTOR'
,p_affected_elements=>'.cronograma'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634703157449016948)
,p_event_id=>wwv_flow_imp.id(7634701699744016945)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_ven_promos_combos.pr_act_desc_lista_cambia_plazo(pn_emp_id => :f_emp_id,',
'                                                      pn_pol_id => :P141_POL_ID,',
'                                                      pn_tse_id => :F_SEG_ID,',
'                                                      pn_plazo => :P141_PLAZO_FACTURA,',
'                                                      pn_age_id_agencias => :P141_AGE_ID_AGENCIA,',
'                                                      pv_error => :p0_error);',
''))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634921963570892771)
,p_name=>'da_hideShowButtons_new_edit_client'
,p_event_sequence=>120
,p_condition_element=>'P141_PER_ID'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634922913815892778)
,p_event_id=>wwv_flow_imp.id(7634921963570892771)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(7634914364020890562)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634922434703892776)
,p_event_id=>wwv_flow_imp.id(7634921963570892771)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(7634914849817890563)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634923366208892789)
,p_event_id=>wwv_flow_imp.id(7634921963570892771)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(7634914849817890563)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634923890781892790)
,p_event_id=>wwv_flow_imp.id(7634921963570892771)
,p_event_result=>'FALSE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(7634914364020890562)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634924362140892790)
,p_event_id=>wwv_flow_imp.id(7634921963570892771)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'F_POPUP'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'N'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634387817356566430)
,p_event_id=>wwv_flow_imp.id(7634921963570892771)
,p_event_result=>'FALSE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'F_POPUP'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'N'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634924819252896223)
,p_name=>'da_onClose_modalPage_nuevo'
,p_event_sequence=>130
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#nuevo_cliente'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634925733534896224)
,p_event_id=>wwv_flow_imp.id(7634924819252896223)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP := ''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634926700601896225)
,p_event_id=>wwv_flow_imp.id(7634924819252896223)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P0_ERROR'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P0_ERROR'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634925187319896224)
,p_event_id=>wwv_flow_imp.id(7634924819252896223)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_NRO_IDE_AUX'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P145_PER_NRO_IDENTIFICACION'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344893408646927941)
,p_event_id=>wwv_flow_imp.id(7634924819252896223)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'window.onunload = null;'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634926213558896224)
,p_event_id=>wwv_flow_imp.id(7634924819252896223)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634927064245898647)
,p_name=>'da_onClose_modalPage_editar'
,p_event_sequence=>140
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#editar_cliente'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634928459688898650)
,p_event_id=>wwv_flow_imp.id(7634927064245898647)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP := ''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634928982277898650)
,p_event_id=>wwv_flow_imp.id(7634927064245898647)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P0_ERROR'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P0_ERROR'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634928044100898650)
,p_event_id=>wwv_flow_imp.id(7634927064245898647)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_NRO_IDE_AUX'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P145_PER_NRO_IDENTIFICACION'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344893511647927942)
,p_event_id=>wwv_flow_imp.id(7634927064245898647)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'window.onunload = null;'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634927515170898648)
,p_event_id=>wwv_flow_imp.id(7634927064245898647)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634929432578901435)
,p_name=>'da_onEnter_cedula_carga'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_PER_NRO_IDENTIFICACION'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(this.browserEvent.keyCode == 13 && document.getElementById("P141_PER_NRO_IDENTIFICACION").value != "" )'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634929764551901436)
,p_event_id=>wwv_flow_imp.id(7634929432578901435)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(49714021092664473)
,p_name=>'da_cliente_360_carga'
,p_event_sequence=>160
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'document.getElementById("P141_PER_NRO_IDENTIFICACION").value != "" ;'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_display_when_cond=>'cliente_360'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(49714057449664474)
,p_event_id=>wwv_flow_imp.id(49714021092664473)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340369642362318467)
,p_name=>'da_onEnter_cedula_borra'
,p_event_sequence=>170
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_PER_NRO_IDENTIFICACION'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'this.browserEvent.keyCode == 13 && document.getElementById("P141_PER_NRO_IDENTIFICACION").value == "" && document.getElementById("P141_CLI_ID").value != ""'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340369817814318469)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_NRO_IDE_AUX'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340369972324318471)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_PER_NOMBRE'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340370110456318472)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_DIRECCION'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340370227678318473)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_TELEFONO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340370253438318474)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_PER_ID'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340370408928318475)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_CLI_ID'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340370538666318476)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_PER_CORREO_ELECTRONICO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344460896476855034)
,p_event_id=>wwv_flow_imp.id(15340369642362318467)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_PER_NRO_IDENTIFICACION:= null;',
':P141_NRO_IDE_AUX := NULL;',
':P141_PER_NOMBRE:= null;',
':P141_DIRECCION:= null;',
':P141_TELEFONO:= null;',
':P141_PER_ID := null;',
':P141_CLI_ID := nuLL;',
':P141_PER_CORREO_ELECTRONICO := null;'))
,p_attribute_03=>'P141_PER_NRO_IDENTIFICACION,P141_NRO_IDE_AUX,P141_PER_NOMBRE,P141_DIRECCION,P141_TELEFONO,P141_PER_ID,P141_CLI_ID,P141_PER_CORREO_ELECTRONICO'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634390856302566461)
,p_name=>'da_onEnter_IteSecId'
,p_event_sequence=>180
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_ITE_SEC_ID'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'this.browserEvent.keyCode == 13 && document.getElementById("P141_ITE_SEC_ID").value != ""'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634391042508566462)
,p_event_id=>wwv_flow_imp.id(7634390856302566461)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_item'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(7634387944661566431)
,p_name=>'da_onEscapeKeyPress'
,p_event_sequence=>190
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'document'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'custom'
,p_bind_event_type_custom=>'dialogclose'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(7634388143905566433)
,p_event_id=>wwv_flow_imp.id(7634387944661566431)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP:=''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340368252490318454)
,p_name=>'da_set_cantidad_cursor'
,p_event_sequence=>200
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'EXPRESSION'
,p_display_when_cond=>':P141_ITE_SEC_ID IS NOT NULL AND :P141_ITE_SEC_ID > 0'
,p_display_when_cond2=>'PLSQL'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340368250789318453)
,p_event_id=>wwv_flow_imp.id(15340368252490318454)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'//alert("T");',
'$("#P141_CANTIDAD").focus();',
'document.getElementById("#P141_CANTIDAD").select();'))
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340368858093318460)
,p_name=>'da_borra_cedula_spl'
,p_event_sequence=>210
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#superlov_button_id'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340369020425318461)
,p_event_id=>wwv_flow_imp.id(15340368858093318460)
,p_event_result=>'TRUE'
,p_action_sequence=>120
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_PER_NRO_IDENTIFICACION:= null;',
':P141_NRO_IDE_AUX := NULL;',
':P141_PER_NOMBRE:= null;',
':P141_DIRECCION:= null;',
':P141_TELEFONO:= null;',
':P141_PER_ID := null;',
':P141_CLI_ID := nuLL;',
':P141_PER_CORREO_ELECTRONICO := null;'))
,p_attribute_03=>'P141_PER_NRO_IDENTIFICACION,P141_NRO_IDE_AUX,P141_PER_NOMBRE,P141_DIRECCION,P141_TELEFONO,P141_PER_ID,P141_CLI_ID,P141_PER_CORREO_ELECTRONICO'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344460963285855035)
,p_event_id=>wwv_flow_imp.id(15340368858093318460)
,p_event_result=>'TRUE'
,p_action_sequence=>130
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15342033459569429171)
,p_name=>'ActualizaEntregaDom'
,p_event_sequence=>220
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_ENTREGA_DOMICILIO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15342033607675429172)
,p_event_id=>wwv_flow_imp.id(15342033459569429171)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(7703021206872929594)
,p_attribute_01=>'PLSQL_EXPRESSION'
,p_attribute_04=>'null;'
,p_attribute_07=>'P141_ENTREGA_DOMICILIO'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(50696270645184965730)
,p_name=>'AD_CARGA_FECHAS_CORTE'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P141_PLAZO_FACTURA,P141_POL_ID'
,p_condition_element=>'P141_TVE_ID'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'2'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'EXPRESSION'
,p_display_when_cond=>':p141_tve_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'') AND :P141_PLAZO_FACTURA IS NOT NULL AND :P141_POL_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(50696270748271965731)
,p_event_id=>wwv_flow_imp.id(50696270645184965730)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_credito.pr_obtener_fechas_primer_pago(pn_cli_id                => :p141_cli_id,',
'                                               pn_emp_id                => :f_emp_id,',
'                                               pn_tse_id                => :f_seg_id,',
'                                               pd_fecha                 => sysdate,--:p141_ord_fecha,',
'                                               pn_pol_id                => :p141_pol_id,',
'                                               pn_dias_gracia_1_venc    => :p141_dias_gracia_primer_venc,',
'                                               pn_mes_gracia_entre_venc => :p141_meses_gracia_entre_venc,',
'                                               pn_plazo_venta           => :p141_plazo_factura,',
'                                               pv_error                 => :p0_error);'))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(50696270815509965732)
,p_event_id=>wwv_flow_imp.id(50696270645184965730)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(23016185918896612843)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(50696270926972965733)
,p_event_id=>wwv_flow_imp.id(50696270645184965730)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P141_FECHA_CORTE_CRE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(130257710528198800827)
,p_name=>'da_close_dialog_upd_dir'
,p_event_sequence=>240
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#editar_direccion'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(130257710631353800828)
,p_event_id=>wwv_flow_imp.id(130257710528198800827)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634930557939907612)
,p_process_sequence=>245
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_load_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p141_tdi_id :=  pq_constantes.fn_retorna_constante(0,''cn_tdi_id_despacho'');',
'SELECT COUNT(1)',
'  INTO :p141_pinpad',
'  FROM car_redes_pago_pue p',
' WHERE p.rpp_estado_registro = 0',
'   AND p.uge_id = :f_uge_id;',
'IF :P141_TVE_ID in (pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado'')) and :P141_PINPAD != 0 then',
' :p141_plan_tarjeta:=pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_ede_plan_tarjeta'');',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'P141_TDI_ID'
,p_process_when_type=>'ITEM_IS_NULL'
,p_internal_uid=>7602677406670142686
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634930309973905197)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_get_datos_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  --raise_application_error(-20000,''request:''||:request);',
'  if :p141_per_nro_identificacion IS NULL then',
'    if :P141_NRO_IDE_AUX IS NOT NULL THEN',
'      :p141_per_nro_identificacion := :P141_NRO_IDE_AUX;',
'    END IF;',
'  end if;',
'  IF :p141_per_nro_identificacion IS NOT NULL THEN',
'  ',
'    -- se debe buscar la direccion principal y no llamar 2 veces a la tabla direcciones',
'    BEGIN',
'      SELECT p.per_id,',
'             c.cli_id,',
'             p.per_correo_electronico,',
'             p.per_primer_nombre,',
'             p.per_segundo_nombre,',
'             p.per_primer_apellido,',
'             p.per_segundo_apellido,',
'             p.per_razon_social,',
'             d.dir_descripcion,',
'             d.dir_id',
'        INTO :p141_per_id,',
'             :p141_cli_id,',
'             :p141_per_correo_electronico,',
'             :p141_per_primer_nombre,',
'             :p141_per_segundo_nombre,',
'             :p141_per_primer_apellido,',
'             :p141_per_segundo_apellido,',
'             :p141_per_nombre,',
'             :p141_direccion,',
'             :P141_DIR_ID',
'        FROM asdm_personas p, asdm_clientes c, asdm_direcciones d',
'       WHERE p.per_nro_identificacion = :p141_per_nro_identificacion',
'         and d.per_id(+) = p.per_id',
'         AND d.dir_estado_registro(+) = :f_estado_reg_activo',
'         AND d.tdi_id(+) = :p141_tdi_id',
'         AND p.emp_id = :f_emp_id',
'         AND c.per_id(+) = p.per_id',
'         AND p.per_tipo_identificacion = :p141_per_tipo_identificacion;',
'    ',
'      if :P141_DIR_ID is null then   --TMontalvan 18/11/2020 Validar si el cliente tiene o no direccion para editar el mapa',
'        :P141_EDITAR_MAPA := ''N'';',
'      else',
'        :P141_EDITAR_MAPA := ''S'';',
'      end if;',
'    ',
'      IF :p141_per_primer_apellido IS NOT NULL THEN',
'        :p141_per_nombre := :p141_per_primer_nombre || '' '' ||',
'                            :p141_per_segundo_nombre || '' '' ||',
'                            :p141_per_primer_apellido || '' '' ||',
'                            :p141_per_segundo_apellido;',
'      END IF;',
'    begin',
'          select *',
'            into :p141_tel_id, :p141_telefono, :p141_tte_id',
'            from (select tt.tel_id, tt.tel_numero, tt.tte_id',
'                    from asdm_telefonos tt, asdm_tipos_telefonos tte',
'                   where tt.per_id = :p141_per_id',
'                     and tt.tel_estado_registro = :f_estado_reg_activo',
'                     and tt.emp_id = :f_emp_id',
'                     and tte.tte_estado_registro = :f_estado_reg_activo',
'                     and tte.tte_id = tt.tte_id',
'                   order by 1 desc)',
'           where rownum = 1;',
'        exception',
'          when others then',
'            :p141_tte_id := pq_constantes.fn_retorna_constante(0,',
'                                                               ''cn_tte_id_domicilio'');',
'        end;',
'    EXCEPTION',
'      WHEN no_data_found THEN',
'        IF :p141_per_primer_apellido IS NOT NULL THEN',
'          :p141_per_id                 := NULL;',
'          :p141_cli_id                 := NULL;',
'          :p141_per_correo_electronico := NULL;',
'          :p141_per_primer_nombre      := NULL;',
'          :p141_per_segundo_nombre     := NULL;',
'          :p141_per_primer_apellido    := NULL;',
'          :p141_per_segundo_apellido   := NULL;',
'          :p141_per_nombre             := NULL;',
'          :p141_direccion              := NULL;',
'          :p141_telefono               := NULL;',
'          :p141_per_nombre             := NULL;',
'          :P141_DIR_ID                 := NULL;',
'        END IF;',
'        :p141_tte_id := pq_constantes.fn_retorna_constante(0,',
'                                                               ''cn_tte_id_domicilio'');        ',
'    END;',
'  ELSE',
'    :p141_per_id                 := NULL;',
'    :p141_cli_id                 := NULL;',
'    :p141_per_correo_electronico := NULL;',
'    :p141_per_primer_nombre      := NULL;',
'    :p141_per_segundo_nombre     := NULL;',
'    :p141_per_primer_apellido    := NULL;',
'    :p141_per_segundo_apellido   := NULL;',
'    :p141_per_nombre             := NULL;',
'    :p141_direccion              := NULL;',
'    :p141_telefono               := NULL;',
'    :p141_per_nombre             := NULL;',
'    :P141_DIR_ID                 := NULL;',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''busca_cliente'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602677158704140271
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634680837156016918)
,p_process_sequence=>2
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_items'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'      Modificacion: FPALOMEQUE',
'      Fecha:        19/07/2021',
'      Descripcion:  Se modifica proceso para que lea el nuevo proceso para obtener descuentos',
'*/',
'DECLARE',
'',
'  ln_nro_combo NUMBER(10);',
'  ln_tse_id    NUMBER(01);',
'  ln_lde_id ppr_lista_descuentos.lde_id%TYPE; --FPALOMEQUE 19/07/2021',
'  ln_pet_id   number;',
'  lv_forma_tc varchar2(20);',
'  ',
'BEGIN',
'',
'',
'  IF :p141_pol_id IS NULL THEN',
'    :p141_ite_sku_id := NULL;',
'    raise_application_error(-20000,',
unistr('                            '' ***** Seleccione la pol\00EDtica ****** '');'),
'  ',
'  END IF;',
'',
'  pq_ven_ordenes.pr_valida_ite_politica(pn_emp_id => :f_emp_id,',
'                                        pn_pol_id => :p141_pol_id,',
'                                        pn_ttr_id => :p141_ttr_id,',
'                                        pv_error  => :p0_error);',
'  ',
'  --IF :p141_ite_sku_id IS NULL THEN',
'    pq_ven_listas2.pr_datos_items_ordenes(:f_emp_id,',
'                                          :p141_ite_sec_id,',
'                                          :f_seg_id,',
'                                          :p141_ite_sku_id,',
'                                          :p141_pol_id,',
'                                          :p141_bodega,',
'                                          :p141_estado,',
'                                          :p141_descripcion_larga,',
'                                          :p141_estado_des,',
'                                          :p141_stock,',
'                                          :p141_cat_id,',
'                                          :p141_ord_tipo,',
'                                          :p141_ite_permite_back_orders,',
'                                          :p141_lcm_id,',
'                                          :p141_cli_id,',
'                                          :p141_age_id_agente,',
'                                          :p0_error);',
'                                                                                  ',
'  --END IF;                                        ',
'',
'  /*',
'  pq_ven_promos_combos.pr_lista_descuento_valor(pn_ite_id             => :p141_ite_sku_id,',
'                                                pn_cei_id             => :p141_estado,',
'                                                pn_pol_id             => :p141_pol_id,',
'                                                pn_tse_id             => :f_seg_id,',
'                                                pn_emp_id             => :f_emp_id,',
'                                                pn_lpp_plazo          => :P141_PLAZO_FACTURA,',
'                                                pn_age_id_agencia => :F_AGE_ID_AGENCIA, -- DLOPEZ PAOBERNAL 27/OCTUBRE/2018  LISTA DESCUENTOS ',
'                                                pn_lde_valor          => :P141_DESCTO_LINEA,',
'                                                pn_lde_tipo_descuento => :P141_TIPO_DESCTO,',
'                                                pn_tld_id             => :P141_TLD_ID,',
'                                                pn_lda_id => :P20_LDA_ID, -- DLOPEZ PAOBERNAL 27/OCTUBRE/2018  LISTA DESCUENTOS',
'                                                pn_descuento_auto     => :P141_DESCUENTO_AUTO,',
'                                                pv_error              => :p0_error);',
'  */',
'  ',
'  --FPALOMEQUE 19/07/2021 Nuevo proceso',
'    /*pq_ven_promos_combos.pr_obtener_descuento(  pn_ite_sku_id         => :p141_ite_sku_id,',
'                                                pn_cei_id             => :p141_estado,',
'                                                pn_pol_id             => :p141_pol_id,',
'                                                pn_tse_id             => :f_seg_id,',
'                                                pn_emp_id             => :f_emp_id,',
'                                                pn_lpp_plazo          => :P141_PLAZO_FACTURA,',
'                                                pn_age_id_agencia     => :F_AGE_ID_AGENCIA, ',
'                                                pn_lde_valor          => :P141_DESCTO_LINEA,',
'                                                pn_lde_tipo_descuento => :P141_TIPO_DESCTO,',
'                                                pn_tld_id             => :P141_TLD_ID,',
'                                                pn_lde_id             => ln_lde_id, ',
'                                                pn_descuento_auto     => :P141_DESCUENTO_AUTO,',
'                                                pv_error              => :p0_error,',
'                                                pd_fecha              => NULL);   */',
'  --FPALOMEQUE 19/07/2021 Nuevo proceso        ',
'  ',
'  --Enaranjo 03/10/2023 nuevas estructuras',
'  lv_forma_tc := pq_constantes.fn_retorna_constante(pn_emp_id    => 0,pv_constante => ''cn_valor_tarjeta'');',
'  ',
'  pq_ven_pro_descuentos.pr_obtener_descuento_item(pn_ite_sku_id           => :p141_ite_sku_id,',
'                                                  pn_tve_id             => :P141_TVE_ID,',
'                                                  pn_cei_id             => :p141_estado,',
'                                                  pn_pol_id             => :p141_pol_id,',
'                                                  pn_tse_id             => :f_seg_id,',
'                                                  pn_emp_id             => :f_emp_id,',
'                                                  pn_ldd_plazo          => :P141_PLAZO_FACTURA,',
'                                                  pn_age_id_agencia     => :F_AGE_ID_AGENCIA, ',
'                                                  pn_ldd_valor          => :P141_DESCTO_LINEA,',
'                                                  pn_ldd_tipo_descuento => :P141_TIPO_DESCTO,',
'                                                  pn_tpr_id             => :P141_TLD_ID,',
'                                                  pn_ldc_id             => ln_lde_id, ',
'                                                  pn_descuento_auto     => :P141_DESCUENTO_AUTO,',
'                                                  pv_error              => :p0_error,',
'                                                  pd_fecha              => NULL,',
'                                                  pv_ldd_forma_tc       => lv_forma_tc,',
'                                                  pn_pet_id             => ln_pet_id);',
'                 ',
'  IF :P141_DESCUENTO_AUTO = ''N'' THEN',
'    :P141_DESCTO_LINEA := 0;',
'    :P141_TIPO_DESCTO  := 1;',
'  ',
'  END IF;',
'',
'  BEGIN',
'    SELECT tse_id',
'      INTO ln_tse_id',
'      FROM asdm_agencias',
'     WHERE age_id = :f_age_id_agencia;',
'  END;',
'',
'  -- BUSCAR CATEGORIA DEL ITEM.',
'  BEGIN',
'    SELECT b.cat_id',
'      INTO :p141_cat_id_item',
'      FROM inv_items a, inv_items_categorias b',
'     WHERE a.ite_sec_id = :p141_ite_sec_id',
'       AND a.emp_id = :f_emp_id',
'       AND b.ite_sku_id = a.ite_sku_id',
'       AND b.cat_id IN',
'           (SELECT c.cat_id',
'              FROM inv_categorias c',
'             WHERE c.emp_id = :f_emp_id',
'               AND c.cat_estado_registro = 0',
'            CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'             START WITH c.cat_id =',
'                        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                           ''cn_cat_id_servicios_padre''));',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      :p141_cat_id_item := NULL;',
'  END;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':REQUEST =  ''busca_item'' and :P0_ERROR is null AND :P141_ITE_SEC_ID IS NOT NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602427685886251992
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634682366135016920)
,p_process_sequence=>4
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_cambios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comunes.pr_elimina_coleccion(:P0_ERROR);',
':P141_PER_ID := null;',
':P141_PER_CORREO_ELECTRONICO := null;',
':P141_PER_PRIMER_NOMBRE := null;',
':P141_PER_SEGUNDO_NOMBRE := null;',
':P141_PER_PRIMER_APELLIDO := null;',
':P141_PER_SEGUNDO_APELLIDO := null;',
':P141_PER_NOMBRE := null;',
':P141_TDI_ID := null;',
':P141_TTE_ID := null;',
':P141_DIRECCION := null;',
':P141_TELEFONO := null;',
':P141_DIR_ID := null;',
':P141_EOR_OBSERVACIONES := null;',
':P141_CAT_ID := null;',
':P141_STOCK := null;',
':P141_POL_ID := null;',
':P141_NUMERO_DETALLES := null;',
':P141_PER_NRO_IDENTIFICACION := null;',
':P141_ENTRADA_CRE := 0;',
':P141_VALOR_FINANCIAR_CRE := 0;',
':P141_TASA_CRE := 0;',
':P141_CUOTA_CRE := 0;',
':P141_PLAZO_CRE := 0;',
':P141_DESCUENTO_ROL := NULL;',
':P141_INSTITUCION := NULL;',
':P141_ERROR_DESCUENTO_ROL := NULL;',
':P141_TOTAL_CREDITO := 0;',
':P141_DESCUENTO_AUTO:= NULL;',
'---------------------',
':p141_tld_id := NULL;',
':p141_tipo_descto := NULL;',
':p141_descto_linea := NULL;',
':P141_SEQ_ID:= NULL;',
':P141_ITE_SKU_ID:= NULL;',
':P141_DESCRIPCION_LARGA:= NULL;',
':P141_ESTADO:= NULL;',
':P141_ESTADO_DES:= NULL;',
':P141_BODEGA:= NULL;',
':P141_CANTIDAD:= NULL;',
':P141_LCM_ID:= NULL;',
':P141_ITE_SEC_ID:= NULL;',
':P141_ITE_SKU_MOD:= NULL;',
':P141_ACCION:= NULL;',
':P141_RIT_OPCION := NULL;',
':P141_CLI_ID := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(7634474626267016598)
,p_internal_uid=>7602429214865251994
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634678478238016913)
,p_process_sequence=>24
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ING_CABECERA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  Lv_Error varchar2(2000);',
'BEGIN',
'  ',
'',
' pq_ven_comunes.pr_ing_fac_cab(',
'                                 :f_emp_id,',
'                                 :p141_pol_id,',
'                                 ''0'', --llave',
'                                 pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_orden_venta'') ,',
'                                 :p141_tve_id,',
'                                 :p141_plazo_factura,',
'                                 :p141_plan_tarjeta,',
'                                 Lv_Error',
');',
'',
'',
':P0_ERROR := lv_error;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_POL_ID is not null and :P141_TVE_ID is not null and (',
':P141_COND_CAB != :P141_POL_ID||''-''||:P141_TVE_ID or :P141_PLAZO_ANT != :P141_PLAZO_FACTURA) and :P0_ERROR is null',
''))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602425326968251987
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634678793074016914)
,p_process_sequence=>34
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_O_ACTUALIZAR_DET'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_total_factura NUMBER(15, 2);',
'  ln_gex           inv_items.ite_sku_id%TYPE := NULL;',
'  ln_cantidad_gex  NUMBER;',
'  lv_error         CLOB;',
'  ln_contador      NUMBER;',
'  pn_number        NUMBER;',
'  ln_cont          NUMBER;',
'  pc_text          VARCHAR2(8000);',
'',
'  ln_rit_id              inv_relaciones_item.rit_id%TYPE; -- dlopez / pao bernal 17 jul 2018',
'  ln_rit_porcentaje      inv_relaciones_item.rit_porcentaje%TYPE;',
'  lv_rit_tipo_item_combo inv_relaciones_item.rit_tipo_item_combo%TYPE;',
'  ln_rit_valor_descuento inv_relaciones_item.rit_valor_descuento%TYPE;',
'',
'  ln_rit_tipo_descuento inv_relaciones_item.rit_tipo_descuento%TYPE;',
'  ln_ite_sku_ingresado  inv_relaciones_item.ite_sku_padre%TYPE;',
'  ln_ite_sku_padre      inv_relaciones_item.ite_sku_padre%TYPE;',
'',
'  lv_bpr_permite_back_orders inv_bodegas_propias.bpr_permite_back_orders%TYPE;',
'  cn_ttr_id_orden_venta      asdm_tipos_transacciones.ttr_id%TYPE := 112;',
'  ln_bag_id                  asdm_bodegas_agencias.bag_id%TYPE;',
'  lv_bag_tipo_asignacion     asdm_bodegas_agencias.bag_tipo_asignacion%TYPE;',
'  ln_contador_item           NUMBER;',
'',
'  --INICIO PAOBERNAL 11/JUN/2018',
'  ln_cn_descto_linea          asdm_e.asdm_constantes_empresa.cem_valor%TYPE := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                                                  ''cn_descto_linea_precio'');',
'  ln_cn_tve_id_credito_propio asdm_e.asdm_constantes_empresa.cem_valor%TYPE := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                                                  ''cn_tve_id_credito_propio'');',
'  ln_cn_tve_id_regalos        asdm_e.asdm_constantes_empresa.cem_valor%TYPE := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                                                  ''cn_tve_id_regalos'');',
'  --FIN    PAOBERNAL 11/JUN/2018   ',
'',
'BEGIN',
'',
'  --INICIO PAOBERNAL 11/JUN/2018  ',
'  IF :p141_tipo_descto = ln_cn_descto_linea THEN',
'    IF :p141_tve_id IN (ln_cn_tve_id_credito_propio, ln_cn_tve_id_regalos) THEN',
'      raise_application_error(-20001,',
'                              ''ERROR -- EL TIPO DE DESCUENTO PRECIO ES VALIDO SOLO PARA VENTAS AL CONTADO'');',
'    END IF;',
'  END IF;',
'  --FIN    PAOBERNAL 11/JUN/2018',
'',
'  --Validaciones datos ingresados ---------------------------------------',
'  IF :p141_pol_id IS NULL THEN',
unistr('    raise_application_error(-20001, ''Seleccione una pol\00EDtica'');'),
'  END IF;',
'  IF :p141_ite_sku_id IS NULL THEN',
'    raise_application_error(-20001, ''Seleccione el item'');',
'  END IF;',
'  IF :p141_ite_sec_id IS NULL THEN',
'    raise_application_error(-20001, ''Seleccione un item'');',
'  END IF;',
'',
'  IF :p141_tve_id IS NULL THEN',
'    raise_application_error(-20001,',
'                            ''Seleccione la forma de Venta'' || :p141_tve_id);',
'  END IF;',
'',
'  IF :p141_tve_id = 0 THEN',
'    raise_application_error(-20001,',
'                            ''Error la forma de venta no puede ser 0 '' ||',
'                            :p141_tve_id);',
'  END IF;',
'',
'  -----------------------------------------------------------------------',
'',
'  pq_ven_ordenes_contado.pr_valida_item_serie_obliga(pn_ite_sku_id => :p141_ite_sku_id,',
'                                                     pn_emp_id     => :f_emp_id,',
'                                                     pv_error      => :p0_error);',
'',
'',
'  ----sept 22 2017 LCALLE',
'  IF :p141_ord_tipo NOT IN',
'     (pq_constantes.fn_retorna_constante(NULL,',
'                                         ''cv_ord_tipo_orden_factura_consig''),',
'      pq_constantes.fn_retorna_constante(NULL,',
'                                         ''cv_ord_tipo_orden_factura_consig_moto'')) THEN',
'    --R2577-03 mfidrovo 10/10/2019 inicio',
'    pq_ven_validaciones.pr_valida_bodega_agencia(pn_emp_id     => :f_emp_id,',
'                                                 pn_bpr_id     => :p141_bodega,',
'                                                 pn_age_id     => :f_age_id_agencia,',
'                                                 pn_ite_sku_id => :p141_ite_sku_id);',
'    --R2577-03 mfidrovo 10/10/2019 fin    ',
'  END IF;',
'  ----- LCalle',
'',
'  /*',
'  IF :P141_POL_ID in(444,445) AND :APP_USER NOT IN',
'         (''GIZAMBRANO'', ''MPACHECO'',''EVERA'') THEN',
'   raise_application_error(-20001,''La politica no esta activa''||:P141_POL_ID);',
'  END IF;',
'  */',
'',
'  --Andres Calle',
'  --09/09/2014',
'  /*if :P141_PRECIO_SRV is not null then',
'    ',
'      pq_ppr_polit_precios.pr_ingreso_precio_st(pn_emp_id     => :f_emp_id,',
'                                                pn_ite_sku_id => :p141_ite_sku_id,',
'                                                pn_valor      => :P141_PRECIO_SRV,',
'                                                pv_error      => :p0_error);',
'    ',
'    end if;',
'  ',
'    IF :p141_ord_tipo = ''VENMO'' THEN',
'    ',
'      SELECT COUNT(seq_id)',
'        INTO ln_contador',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''CO_DETALLE'';',
'    ',
'    END IF;',
'  */',
'  IF ln_contador = 1 AND',
'     :p141_tve_id IN',
'     (pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_contado''),',
'      pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                         ''cn_tve_id_tarjeta_credito'')) AND',
'     :p141_ord_tipo = ''VENMO'' AND :p141_accion != ''E'' THEN',
'  ',
'    NULL;',
'  ',
'  ELSIF ln_contador = 2 AND',
'        :p141_tve_id =',
'        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tve_id_credito_propio'') AND',
'        :p141_ord_tipo = ''VENMO'' AND :p141_accion != ''E'' THEN',
'  ',
'    NULL;',
'  ',
'  ELSE',
'  ',
'    IF :p141_accion = ''E'' AND :p141_cadena IS NOT NULL THEN',
'      --:p141_cadena is not null etenesaca 18/01/2016  ',
'      pq_ven_comunes.pr_respalda_coleccion_detalle;',
'      pq_ven_comunes.pr_elimina_coleccion_detalle(:f_emp_id, -- PAOBERNAL 09/NOVIEMBRE/2018',
'                                                  :p141_cadena,',
'                                                  :p0_error);',
'      ln_gex          := :p141_gex;',
'      ln_cantidad_gex := :p141_cantidad_gex;',
'    ELSE',
'      ln_gex          := NULL;',
'      ln_cantidad_gex := NULL;',
'    END IF;',
'  ',
'    -- dlopez 15 jun 2016 Si seleccionaron promo Mar0 carga tipo desc margen y des 0',
'    IF nvl(:p141_promo_mg0, ''N'') = ''S'' THEN',
'      :p141_tipo_descto  := 2;',
'      :p141_descto_linea := 0;',
'    END IF;',
'  ',
'    -- dlopez 20 oct 2016 para cargar tipo , % distribucion del item promo combo, y tipo relacion',
'    ln_ite_sku_ingresado := :p141_ite_sku_id;',
'    IF :p20_ite_sku_id_rel IS NULL THEN',
'      --Si se edita se omite la carga de los combos',
'      pq_ven_promos_combos.pr_datos_principal_pantalla(pn_ite_sku_id          => :p141_ite_sku_id,',
'                                                       pn_emp_id              => :f_emp_id,',
'                                                       pn_rit_opcion          => :p141_rit_opcion,',
'                                                       pn_cantidad            => :p141_cantidad,',
'                                                       pv_rit_tipo_item_combo => lv_rit_tipo_item_combo,',
'                                                       pn_rit_porcentaje      => ln_rit_porcentaje,',
'                                                       pn_rit_valor_descuento => ln_rit_valor_descuento,',
'                                                       pn_rit_tipo_descuento  => ln_rit_tipo_descuento,',
'                                                       pn_ite_sku_padre       => ln_ite_sku_padre,',
'                                                       pn_rit_id              => ln_rit_id, -- dlopez / pao bernal 17 jul 2018',
'                                                       pn_tse_id              => :f_seg_id,',
'                                                       pv_carga_promos_combos => :p141_promos_combos);',
'    END IF;',
'  ',
'    --pruebas_dl(null,''ln_rit_tipo_descuento''||ln_rit_tipo_descuento||''lv_rit_tipo_item_combo''||lv_rit_tipo_item_combo||''p141_ite_sku_id''||:p141_ite_sku_id||''P141_RIT_OPCION''||:P141_RIT_OPCION,''combo'');',
'  ',
'    -- Dlopez para promos / combos',
'    IF lv_rit_tipo_item_combo = ''P'' THEN',
'      :p141_tipo_descto  := nvl(ln_rit_tipo_descuento, 0);',
'      :p141_descto_linea := nvl(ln_rit_valor_descuento, 0);',
'      :p141_ite_sku_id   := ln_ite_sku_padre;',
'    END IF;',
'  ',
'    -- Dlopez para descuentos automaticos desde lista de descuentos',
'    -- Si el tipo de descuetno es 0(valor) debe multiplicar por la cantidad',
'    IF :p141_descuento_auto = ''S'' AND :p141_tipo_descto = 0 THEN',
'      :p141_descto_linea := :p141_descto_linea * :p141_cantidad;',
'    END IF;',
'    ---------------------------------------------------------------------------',
'    IF (ln_gex IS NULL) AND (:p20_ite_sku_id_rel IS NULL) THEN',
'    ',
'      -- INICIO -- PAOBERNAL 26/JULIO/2018 ',
'      asdm_p.pq_ven_comunes.pr_carga_coleccion_detalle(pn_emp_id                   => :f_emp_id,',
'                                                       pn_ttr_id                   => cn_ttr_id_orden_venta,',
'                                                       pn_ite_sku_id               => :p141_ite_sku_id,',
'                                                       pn_sku_rel_id               => NULL, --ITE_SKU_REL',
'                                                       pn_tip_rel_id               => NULL, --TIPO_REL_ID',
'                                                       pv_tip_ite_combo            => lv_rit_tipo_item_combo, --TIPO_ITE_COMBO',
'                                                       pn_rit_porcentaje           => ln_rit_porcentaje, --RIT_PORCENTAJE',
'                                                       pn_ieb_id                   => :p141_ieb_id,',
'                                                       pn_cei                      => :p141_estado,',
'                                                       pn_pol_id                   => :p141_pol_id,',
'                                                       pd_cot_fecha                => SYSDATE,',
'                                                       pn_det_cantidad             => :p141_cantidad,',
'                                                       pn_det_tipo_descto          => :p141_tipo_descto,',
'                                                       pn_det_descto               => :p141_descto_linea,',
'                                                       pn_cab_descto_ptos          => :p141_descto_puntos,',
'                                                       pn_cab_descto_fact          => :p141_descto_factura,',
'                                                       pn_cab_termino_vta          => :p141_tve_id,',
'                                                       pn_cab_plan_tarjcr          => :p141_plan_tarjeta,',
'                                                       pn_cab_entrada_ing          => :p141_entrada,',
'                                                       pn_cab_plazo_fact           => :p141_plazo_factura,',
'                                                       pn_age_id_agente            => :p141_age_id_agente_cab, --:p141_age_id_agente',
'                                                       pn_age_id_agencia           => :p141_age_id_agencia,',
'                                                       pn_total_factura            => ln_total_factura,',
'                                                       pn_poce_nacional            => :p141_poce_nacional,',
'                                                       pn_entrada_minima           => :p141_entrada_minima,',
'                                                       pn_bpr_id                   => :p141_bodega,',
'                                                       pn_lcm_id                   => :p141_lcm_id,',
'                                                       pn_gex                      => ln_gex,',
'                                                       pn_gex_cantidad             => to_number(ln_cantidad_gex),',
'                                                       pn_cli_id                   => :p141_cli_id,',
'                                                       pn_com_id                   => :p141_com_id, --com_id',
'                                                       pn_tipo_nc                  => :p141_tipo_nc,',
'                                                       pn_valor_nc                 => NULL, --valor nc',
'                                                       pn_servicio_manejo_producto => nvl(:p141_servicio_manejo_producto,',
'                                                                                          0),',
'                                                       pv_error                    => lv_error,',
'                                                       pn_tld_id                   => :p141_tld_id,',
'                                                       pn_com_id_origen            => NULL, -- PAOBERNAL 26/JULIO/2018',
'                                                       pn_cde_id_origen            => NULL, -- PAOBERNAL 26/JULIO/2018',
'                                                       pn_rit_id                   => ln_rit_id,',
'                                                       pv_editado                  => :p20_accion); -- dlopez / pao bernal 17 jul 2018    ',
'      -- FIN -- PAOBERNAL 26/JULIO/2018',
'    ',
'    ELSE',
'    ',
'      -- INICIO -- PAOBERNAL 26/JULIO/2018',
'      pq_ven_comunes.pr_carga_ncoleccion_detalle(pn_emp_id                   => :f_emp_id,',
'                                                 pn_ttr_id                   => cn_ttr_id_orden_venta,',
'                                                 pn_ite_sku_id               => :p141_ite_sku_id,',
'                                                 pn_sku_rel_id               => NULL, --ITE_SKU_REL',
'                                                 pn_tip_rel_id               => NULL, --TIPO_REL_ID',
'                                                 pv_tip_ite_combo            => lv_rit_tipo_item_combo, --TIPO_ITE_COMBO',
'                                                 pn_rit_porcentaje           => ln_rit_porcentaje, --RIT_PORCENTAJE',
'                                                 pn_ieb_id                   => :p141_ieb_id,',
'                                                 pn_cei                      => :p141_estado,',
'                                                 pn_pol_id                   => :p141_pol_id,',
'                                                 pd_cot_fecha                => SYSDATE,',
'                                                 pn_det_cantidad             => :p141_cantidad,',
'                                                 pn_det_tipo_descto          => :p141_tipo_descto,',
'                                                 pn_det_descto               => :p141_descto_linea,',
'                                                 pn_cab_descto_ptos          => :p141_descto_puntos,',
'                                                 pn_cab_descto_fact          => :p141_descto_factura,',
'                                                 pn_cab_termino_vta          => :p141_tve_id,',
'                                                 pn_cab_plan_tarjcr          => :p141_plan_tarjeta,',
'                                                 pn_cab_entrada_ing          => :p141_entrada,',
'                                                 pn_cab_plazo_fact           => :p141_plazo_factura,',
'                                                 pn_age_id_agente            => :p141_age_id_agente_cab, --:p141_age_id_agente',
'                                                 pn_age_id_agencia           => :p141_age_id_agencia,',
'                                                 pn_total_factura            => ln_total_factura,',
'                                                 pn_poce_nacional            => :p141_poce_nacional,',
'                                                 pn_entrada_minima           => :p141_entrada_minima,',
'                                                 pn_bpr_id                   => :p141_bodega,',
'                                                 pn_lcm_id                   => :p141_lcm_id,',
'                                                 pn_gex                      => ln_gex,',
'                                                 pn_gex_cantidad             => to_number(ln_cantidad_gex),',
'                                                 pn_cli_id                   => :p141_cli_id,',
'                                                 pn_com_id                   => :p141_com_id, --com_id',
'                                                 pn_tipo_nc                  => :p141_tipo_nc,',
'                                                 pn_valor_nc                 => NULL, --valor nc',
'                                                 pn_servicio_manejo_producto => nvl(:p141_servicio_manejo_producto,',
'                                                                                    0),',
'                                                 pv_error                    => lv_error,',
'                                                 pn_tld_id                   => :p141_tld_id,',
'                                                 pn_com_id_origen            => NULL, -- PAOBERNAL 26/JULIO/2018',
'                                                 pn_cde_id_origen            => NULL, -- PAOBERNAL 26/JULIO/2018',
'                                                 pn_rit_id                   => ln_rit_id, -- dlopez / pao bernal 17 jul 2018',
'                                                 pv_editado                  => :p20_accion -- H1-R2711 ETENESACA  ',
'                                                 );',
'      -- FIN -- PAOBERNAL 26/JULIO/2018                                                    ',
'    ',
'    END IF;',
'    -- Carga items promocioens cargar los promocionales',
'    IF nvl(:p141_promos_combos, ''S'') = ''S'' THEN',
'      asdm_p.pruebas_paob(pn_numero  => 33333333333,',
'                          pc_texto1  => NULL,',
'                          pv_texto2  => :f_emp_id || '':'' ||',
'                                        cn_ttr_id_orden_venta || '':'' ||',
'                                        :p141_pol_id || '':'' || :p141_tve_id || '':'' ||',
'                                        :p141_plan_tarjeta || '':'' ||',
'                                        :p141_entrada || '':'' ||',
'                                        :p141_plazo_factura || '':'' ||',
'                                        :p141_age_id_agente_cab || '':'' ||',
'                                        :p141_age_id_agencia || '':'' ||',
'                                        ln_total_factura || '':'' ||',
'                                        :p141_poce_nacional || '':'' ||',
'                                        :p141_entrada_minima || '':'' ||',
'                                        :p141_bodega || '':'' || :p141_lcm_id || '':'' ||',
'                                        :p141_cli_id || '':'' || 0 || '':'' ||',
'                                        :f_seg_id || '':'' || :p141_cantidad || '':'' ||',
'                                        :p141_ite_sku_id || '':'' ||',
'                                        :p141_rit_opcion || '':'' ||',
'                                        ln_ite_sku_ingresado || '':'' || ''S'' || '':'' ||',
'                                        :p20_ord_tipo,',
'                          pn_numero1 => NULL);',
'      pq_ven_promos_combos.pr_carga_promociones(pn_emp_id                   => :f_emp_id,',
'                                                pn_ttr_id                   => cn_ttr_id_orden_venta,',
'                                                pn_pol_id                   => :p141_pol_id,',
'                                                pn_tve_id                   => :p141_tve_id,',
'                                                pn_cab_plan_tarjcr          => :p141_plan_tarjeta,',
'                                                pn_cab_entrada_ing          => :p141_entrada,',
'                                                pn_cab_plazo_fact           => :p141_plazo_factura,',
'                                                pn_age_id_agente            => :p141_age_id_agente_cab,',
'                                                pn_age_id_agencia           => :p141_age_id_agencia,',
'                                                pn_total_factura            => ln_total_factura,',
'                                                pn_poce_nacional            => :p141_poce_nacional,',
'                                                pn_entrada_minima           => :p141_entrada_minima,',
'                                                pn_bpr_id                   => :p141_bodega,',
'                                                pn_lcm_id                   => :p141_lcm_id,',
'                                                pn_cli_id                   => :p141_cli_id,',
'                                                pn_servicio_manejo_producto => 0, --nvl(:P141_SERVICIO_MANEJO_PRODUCTO,0),',
'                                                pn_tse_id                   => :f_seg_id,',
'                                                pn_cant_principal           => :p141_cantidad,',
'                                                pn_ite_sku_id               => :p141_ite_sku_id,',
'                                                pn_rit_opcion               => :p141_rit_opcion,',
'                                                pn_ite_sku_ingresado        => ln_ite_sku_ingresado,',
'                                                pv_valida_stock             => ''S'',',
'                                                pv_ord_tipo                 => :p20_ord_tipo, -- PAOBERNAL 09/NOVIEMBRE/2018',
'                                                pv_error                    => lv_error);',
'    ',
'      asdm_p.pruebas_paob(pn_numero  => 33333333333,',
'                          pc_texto1  => NULL,',
'                          pv_texto2  => lv_error,',
'                          pn_numero1 => NULL);',
'    ',
'    END IF;',
'    -- dlopez feb 2016 carga ser log',
'    pq_ven_ordenes.pr_carga_items_automaticos(pn_emp_id          => :f_emp_id,',
'                                              pn_ttr_id          => cn_ttr_id_orden_venta,',
'                                              pn_pol_id          => :p141_pol_id,',
'                                              pn_tve_id          => :p141_tve_id,',
'                                              pn_cab_plan_tarjcr => :p141_plan_tarjeta,',
'                                              pn_cab_entrada_ing => :p141_entrada,',
'                                              pn_cab_plazo_fact  => :p141_plazo_factura,',
'                                              pn_age_id_agente   => :p141_age_id_agente_cab,',
'                                              pn_age_id_agencia  => :p141_age_id_agencia,',
'                                              pn_total_factura   => ln_total_factura,',
'                                              pn_poce_nacional   => :p141_poce_nacional,',
'                                              pn_entrada_minima  => :p141_entrada_minima,',
'                                              pn_bpr_id          => :p141_bodega,',
'                                              pn_lcm_id          => :p141_lcm_id,',
'                                              pn_cli_id          => :p141_cli_id,',
'                                              pn_serv_log        => :p141_servicio_manejo_producto,',
'                                              pv_error           => :p0_error);',
'    :p141_promo_mg0    := NULL;',
'    :p141_tipo_descto  := 1;',
'    :p141_descto_linea := NULL;',
'  ',
'    -- PAOBERNAL 09/NOVIEMBRE/2018 COLOCAR LOS PARAMETROS EN ESTE PROCEDIMIENTO ',
'    pq_ven_comunes.pr_recalcula_coleccion_detalle(pn_emp_id                  => :f_emp_id,',
'                                                  pn_pol_id                  => :p141_pol_id,',
'                                                  pn_ttr_id                  => cn_ttr_id_orden_venta,',
'                                                  pn_cab_descto_ptos         => :p141_descto_puntos,',
'                                                  pn_cab_descto_fact         => :p141_descto_factura,',
'                                                  pn_cab_termino_vta         => :p141_tve_id,',
'                                                  pn_cab_plan_tarjcr         => :p141_plan_tarjeta,',
'                                                  pn_cab_entrada_ing         => :p141_entrada,',
'                                                  pn_cab_plazo_fact          => :p141_plazo_factura,',
'                                                  pn_total_factura           => :p141_total_factura,',
'                                                  pn_poce_nacional           => :p141_poce_nacional,',
'                                                  pn_entrada_minima          => :p141_entrada_minima,',
'                                                  pn_plazo_cre               => :p141_plazo_cre,',
'                                                  pn_entrada_cre             => :p141_entrada_cre,',
'                                                  pn_valor_financiar_cre     => :p141_valor_financiar_cre,',
'                                                  pn_tasa_cre                => :p141_tasa_cre,',
'                                                  pn_cuota_cre               => :p141_cuota_cre,',
'                                                  pn_com_id                  => NULL, ---com_id',
'                                                  pn_val_castigo_nc          => NULL, --valor nc',
'                                                  pn_fabrica_credito         => NULL, --fc',
'                                                  pn_nuevo_valor_fc          => NULL, --nuevo fc',
'                                                  pn_nuevo_interes_fc        => NULL, --NUEVO INT FC',
'                                                  pn_total_pagar_fc_generado => NULL,',
'                                                  pv_error                   => lv_error,',
'                                                  pn_rol_id                  => :p0_rol,',
'                                                  pn_age_id_agencia          => :f_age_id_agencia,',
'                                                  pv_clave_autoriza          => :p141_clave_descuento,',
'                                                  pn_usu_id                  => :p141_usu_autoriza_descuento,',
'                                                  pn_cli_id                  => :p141_cli_id,',
'                                                  pn_rol_id_autoriza         => :p141_rol_aut,',
'                                                  pn_es_preciador            => NULL);',
'  ',
'    SELECT nvl(SUM(nvl(c012, 0)), 0)',
'      INTO :p141_total_credito',
'      FROM apex_collections',
'     WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'       AND c007 =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_var_id_total_credito'');',
'  ',
'    --pr_car_log_pruebas_ac(66,66,66,66,:p141_total_credito||'' ** ''||:p141_plazo_factura||'' ** ''||:p141_tasa_cre||'' ** ''||:p141_cuota_cre||'' ** ''||:p141_fecha_corte_cre||'' tve_id''||:p141_tve_id);',
'    IF :p141_tve_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tve_id_credito_propio'') THEN',
'      /*',
'      Andres Calle',
'      14/11/2017',
'      */',
'    ',
'      pq_car_credito.pr_genera_cronograma(pn_emp_id              => :f_emp_id,',
'                                          pn_total_cre           => :p141_total_credito,',
'                                          pn_plazo               => :p141_plazo_factura,',
'                                          pn_tasa                => :p141_tasa_cre / 12,',
'                                          pn_cuota               => :p141_cuota_cre,',
'                                          pn_monto_financiar     => :p141_valor_financiar_cre,',
'                                          pd_primer_vencimiento  => to_date(:p141_fecha_corte_cre,',
'                                                                            ''dd/mm/yyyy''),',
'                                          pv_descuento_rol       => ''N'', --nvl(:p141_descuento_rol,''N''),',
'                                          pv_nom_coll_dividendos => ''COLL_CRONOGRAMA'',',
'                                          pn_age_id_agencia      => :f_age_id_agencia,',
'                                          pn_cli_id              => :p141_cli_id,',
'                                          pv_cupo_correcto       => :p141_cupo_correcto,',
'                                          pv_error               => :p0_error);',
'    END IF;',
'  ',
'    :p141_ieb_id                   := NULL;',
'    :p141_ite_sku_id               := NULL;',
'    :p141_age_id_agente            := NULL;',
'    :p141_cantidad                 := NULL;',
'    :p141_stock                    := NULL;',
'    :p141_estado                   := NULL;',
'    :p141_seq_id                   := NULL;',
'    :p141_cadena                   := NULL;',
'    :p141_ite_sku_id_rel           := NULL;',
'    :p141_tipo_item                := NULL;',
'    :p141_gex                      := NULL;',
'    :p141_cantidad_gex             := NULL;',
'    :p141_descto_linea             := NULL;',
'    :p141_descripcion_larga        := NULL;',
'    :p141_ite_sec_id               := NULL;',
'    :p141_accion                   := NULL;',
'    :p141_total_factura            := ln_total_factura;',
'    :p141_servicio_manejo_producto := NULL;',
'    :p141_tipo_descto              := 1; --POR DEFECTO %',
'    :p141_promo_mg0                := NULL;',
'    :p141_tld_id                   := NULL;',
'  ',
'  END IF;',
'',
'  :p0_error       := :p0_error || lv_error;',
'  :p141_recalcula := NULL;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    :p141_ite_sec_id := NULL;',
'    :p141_ite_sku_id := NULL;',
'    :p0_error        := lv_error || SQLERRM;',
'  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request =''carga_detalle'' and :P0_ERROR is null',
''))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602425641804251988
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'para pruebas y poder grabar en la tabla vm',
'',
'/*',
'  ',
'  if (:F_USER_ID= 3267) THEN',
'  ',
'  ',
'  ',
'  pc_text:= pq_ven_listas.fn_lov_items_ordenes(:f_emp_id,',
'                                               :P20_BODEGA, ',
'                                               :P20_ESTADO,',
'                                               :F_AGE_ID_AGENCIA,',
'                                               :P20_ORD_TIPO,',
'                                               :P20_CLI_ID,',
'                                               :P20_AGE_ID_AGENTE_CAB,',
'                                               :P20_LCM_ID,',
'                                               :P20_POL_ID, ',
'                                               :f_seg_id,',
'                                               :P20_ORD_TIPO,',
'                                               :P0_ERROR); ',
'   SELECT nvl(MAX(sec), 0) + 1',
'  INTO   ln_cont',
'  FROM   vm;',
'  INSERT INTO vm',
'  VALUES',
'    (1990,',
'     ',
'     ln_cont,',
'     pc_text);',
'  COMMIT;',
'  ',
'  END IF;*/'))
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634677659591016913)
,p_process_sequence=>44
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINARA_LINEA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'    FOR i IN 1 .. apex_application.g_f50.COUNT LOOP',
'         pq_ven_comunes.pr_elimina_coleccion_detalle(:f_emp_id,apex_application.g_f50(i),:p0_error);',
'     END LOOP;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(7634551104803016716)
,p_process_when=>':P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602424508321251987
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634676540136016911)
,p_process_sequence=>54
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_RECALCULAR_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    ln_desc_factura_ant NUMBER(11, 4) := :p141_descto_factura;',
'    lv_autorizacion     VARCHAR2(100);',
'    lb_resultado        BOOLEAN := TRUE;',
' LN_CONT_TRE_ID      NUMBER;',
'',
'BEGIN',
'--etenesaca 21/12/2017 Genera un inconveniente',
'IF apex_collection.collection_exists(''COLL_CRONOGRAMA'') THEN',
'  apex_collection.delete_collection(''COLL_CRONOGRAMA'');',
'END IF;  ',
'',
'    ---:p0_error := NULL;',
'/*if :P141_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'') then',
'  pq_car_credito.pr_obtener_fechas_primer_pago(pn_cli_id                => :p141_cli_id,',
'                                               pn_emp_id                => :f_emp_id,',
'                                               pn_tse_id                => :f_seg_id,',
'                                               pd_fecha                 => :p141_ord_fecha,',
'                                               pn_pol_id                => :p141_pol_id,',
'                                               pn_dias_gracia_1_venc    => :p141_dias_gracia_primer_venc,',
'                                               pn_mes_gracia_entre_venc => :p141_meses_gracia_entre_venc,',
'                                               pn_plazo_venta           => :p141_plazo_factura,',
'                                               pv_error                 => :p0_error);',
'end if;',
'*/',
'    IF :p0_error IS NOT NULL',
'       OR :p141_total_factura_ing IS NULL THEN',
'',
' ---MCAGUANA 15/08/2013 --- EN LOS PROMOCIONALES DEBO VOLVER A CALCULAR LAS VARIABLES DE ENTRADA ',
'   /* SELECT COUNT(*)',
'      INTO LN_CONT_TRE_ID',
'      FROM apex_collections b',
'     WHERE b.collection_name = ''CO_DETALLE''',
'       and b.c007 =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_tre_id_promocion'');',
'  ',
'    IF (:P141_APROBACION_COMERCIAL IS NOT NULL)  AND',
'       (LN_CONT_TRE_ID > 0) THEN',
'      PQ_VEN_COMUNES.pr_cargar_var_ent_promo(:P141_ORD_ID,',
'                                             :f_emp_id,',
'                                             :p0_error);',
'    ',
'    END IF;',
'  */',
'    --FIN MCAGUANA 15/08/2013',
'',
'        :p141_descto_factura := ln_desc_factura_ant;',
'',
'      -- PAOBRNAL 09/NOVIEMBRE/2018 ARREGLAR LOS PARAMETROS DE ESTE PROCEDIMIENTO',
'      pq_ven_comunes.pr_recalcula_coleccion_detalle(pn_emp_id                  =>:f_emp_id,',
'                                                    pn_pol_id                  =>:p141_pol_id,',
'                                                    pn_ttr_id                  =>112,',
'                                                    pn_cab_descto_ptos         =>:p141_descto_puntos,',
'                                                    pn_cab_descto_fact         =>:p141_descto_factura,',
'                                                    pn_cab_termino_vta         =>:p141_tve_id,',
'                                                    pn_cab_plan_tarjcr         => :p141_plan_tarjeta,',
'                                                    pn_cab_entrada_ing         =>:p141_entrada,',
'                                                    pn_cab_plazo_fact          =>:p141_plazo_factura,',
'                                                    pn_total_factura           =>:p141_total_factura,',
'                                                    pn_poce_nacional           =>:p141_poce_nacional,',
'                                                    pn_entrada_minima          =>:p141_entrada_minima,',
'                                                    pn_plazo_cre               =>:P141_PLAZO_CRE,',
'                                                    pn_entrada_cre             =>:P141_ENTRADA_CRE,',
'                                                    pn_valor_financiar_cre     =>:P141_VALOR_FINANCIAR_CRE,',
'                                                    pn_tasa_cre                =>:P141_TASA_CRE,	',
'                                                    pn_cuota_cre               =>:P141_CUOTA_CRE,',
'                                                    pn_com_id                  => NULL,',
'                                                    pn_val_castigo_nc          => NULL,',
'                                                    pn_fabrica_credito         => NULL, ---fc',
'                                                    pn_nuevo_valor_fc          => NULL, ---valor fc',
'                                                    pn_nuevo_interes_fc        => NULL, --NUEVO INT FC',
'                                                    pn_total_pagar_fc_generado => NULL,',
'                                                    pv_error                   =>:p0_error,',
'                                                    pn_rol_id                  =>:P0_ROL,',
'                                                    pn_age_id_agencia          =>:F_AGE_ID_AGENCIA,',
'                                                    pv_clave_autoriza          =>:P141_CLAVE_DESCUENTO, ',
'                                                    pn_usu_id                  =>:P141_USU_AUTORIZA_DESCUENTO,',
'                                                    pn_cli_id                  =>:P141_CLI_ID,',
'                                                    pn_rol_id_autoriza         =>:P141_ROL_AUT,',
'                                                    pn_es_preciador            => null);',
'',
'---modificado porque no esta tomando el valor correcto de la entrada real,  siempre envia un centavo mas',
'--se va a obtener directamente de la variable ',
'--11/septiembre/2012',
'SELECT  sum(c012)',
'INTO :P141_ENTRADA_CRE',
'  FROM apex_collections',
' WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'/*   AND c020 !=',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_cat_id_combo'')',
'*/',
'   /*AND (c015 in(pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tre_id_combo'')) OR c015 IS NULL OR c015 <> 999)--;',
'   */',
'   AND c007 =       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_entrada'');',
'',
':P141_ENTRADA_CRE := round(:P141_ENTRADA_CRE,2);',
'',
'--fin',
'',
'',
'',
'',
'',
'        IF :p0_despliega_aut = ''S'' THEN',
'            :p0_despliega_aut := ''N'';',
'        END IF;',
'    END IF;',
'',
'SELECT  NVL(sum(NVL(c012,0)),0)',
'INTO :P141_TOTAL_CREDITO',
'  FROM apex_collections',
' WHERE collection_name = ''CO_VARIABLES_DETALLE''',
' /*  AND (c015 in(pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tre_id_combo'')) OR c015 IS NULL OR c015 <> 999)*/',
'/*   AND c020 !=',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_cat_id_combo'')*/',
'',
'   AND c007 =       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_total_credito'');',
'',
'',
'if :p141_tve_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'') then',
'/*',
'Andres Calle',
'14/11/2017',
'*/',
'--pr_car_log_pruebas_ac(66,66,66,66,:p141_total_credito||'' ** ''||:p141_plazo_factura||'' ** ''||:p141_tasa_cre||'' ** ''||:p141_cuota_cre||'' ** ''||:p141_fecha_corte_cre);',
' pq_car_credito.pr_genera_cronograma(pn_emp_id              => :f_emp_id,',
'                                      pn_total_cre           => :p141_total_credito,',
'                                      pn_plazo               => :p141_plazo_factura,',
'                                      pn_tasa                => :p141_tasa_cre / 12,',
'                                      pn_cuota               => :p141_cuota_cre,',
'                                      pn_monto_financiar     => :p141_valor_financiar_cre,',
'                                      pd_primer_vencimiento  => to_date(:p141_fecha_corte_cre,',
'                                                                        ''dd/mm/yyyy''),',
'                                      pv_descuento_rol       => ''N'',--nvl(:p141_descuento_rol,''N''),',
'                                      pv_nom_coll_dividendos => ''COLL_CRONOGRAMA'',',
'                                      pn_age_id_agencia      => :f_age_id_agencia,',
'                                      pn_cli_id              => :p141_cli_id,',
'                                      pv_cupo_correcto       => :p141_cupo_correcto,',
'                                      pv_error               => :p0_error);',
'',
'end if;',
'',
'',
':p141_ieb_id            := NULL;',
':p141_ite_sku_id        := NULL;',
':p141_age_id_agente     := NULL;',
':p141_cantidad          := NULL;',
':p141_stock             := NULL;',
':p141_estado            := NULL;',
':p141_seq_id            := NULL;',
':p141_total_factura_ing := NULL;',
':p0_autorizacion       := NULL;',
':P141_TIPO_DESCTO  := 1;--POR DEFECTO %',
'',
' :p141_recalcula := null;',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'      ROLLBACK;',
'      raise_application_error(-20001,',
'                              ''PR_RECALCULAR_DETALLE '' || SQLERRM);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
' ',
' :REQUEST  in (''recalcular'',''CUEN'',''P20_ELIMINAR_LINEA'')',
'and exists (select * ',
' from apex_collections where collection_name = ''CO_DETALLE'') and :P0_ERROR is null'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'SQL'
,p_internal_uid=>7602423388866251985
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST=''recalcular''',
'and exists (select * ',
' from apex_collections where collection_name = ''CO_DETALLE'') and :P0_ERROR is null'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634677321731016913)
,p_process_sequence=>64
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR_COLECCIONES'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comunes.pr_elimina_coleccion(:P0_ERROR',
');'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(7634474626267016598)
,p_process_when=>':P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602424170461251987
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634675743106016910)
,p_process_sequence=>74
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_factores_meses_gracia'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_car_validaciones.pr_valida_meses_gracia(pn_dias_primer_venc => :P141_DIAS_GRACIA_PRIMER_VENC,',
'                       pn_meses_entre_venc => :P141_MESES_GRACIA_ENTRE_VENC,',
'                       pv_error => :P0_ERROR);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>7602422591836251984
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634684037239016925)
,p_process_sequence=>84
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_permite_entrada'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the function',
'  :p141_permite_entrada:= NVL(pq_car_validaciones.fn_valida_pol_term_venta(pn_pol_id => :P141_POL_ID,',
'                                                          pn_emp_id => :f_emp_id,',
'                                                          pn_tve_id => :P141_TVE_ID), ''S'');',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'recalcular'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>7602430885969251999
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58297087329647269734)
,p_process_sequence=>94
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_VALIDA_COMBOSEGURO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  LV_MENSAJE_COMBOSEGURO VARCHAR2(300) := NULL;',
'  LV_ERROR               VARCHAR2(300) := NULL;',
'  ',
'BEGIN',
'   pq_ven_promos_combos.pr_valida_comboseguro(pn_emp_id => :f_emp_id,',
'                                              pn_cli_id => :P141_CLI_ID,',
'                                              pv_error => :p0_error); ',
'    ',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(7634506164043016652)
,p_internal_uid=>58264834178377504808
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634148939041203437)
,p_process_sequence=>104
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABAR_ORDENES'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_cuenta_caja   NUMBER := 0;',
'  lc_error         CLOB;',
'  lv_tipo_orden    VARCHAR2(5);',
'  ld_fecha_sistema DATE;',
'  ln_pca_id        NUMBER;',
'  ln_per_id        NUMBER := 0;    --TMontalvan R2826-03 15/07/2020',
'  ln_veo_id        NUMBER;         --TMontalvan R2826-03 15/07/2020',
'  ln_ord_id        NUMBER;         --TMontalvan R2826-03 15/07/2020',
'  ln_opt_id NUMBER;  --MPO-29 15-JUN-2023 ',
'BEGIN',
'',
'--R2844-03  mfidrovo 27/07/2020 se cambiar  pq_ven_ordenes_contado_dml.pr_ins_ven_ordenes_ecommerce por  pq_ven_ordenes_contado_dml.pr_ins_ven_ordenes_externas',
'  IF :p141_realizar_pago = ''S'' THEN',
'    --INICIO ETENESACA R2698-01 18/02/2020',
'    -- Se realiza join con puntos emision para validar que el punto emision este activo',
'  ',
'     pq_ven_movimientos_caja.pr_consultar_caja(:f_user_id,',
'                                                  :f_uge_id,',
'                                                  :f_emp_id,',
'                                                  :p0_pue_id,',
'                                                  ln_pca_id,',
'                                                  ld_fecha_sistema,',
'                                                  :p0_error);',
'    --FIN ETENESACA R2698-01 18/02/2020',
'    --FIN FIDROVO 21/04/2020',
'  END IF;',
'',
'  :p0_error := NULL;',
'  pq_ven_ordenes_contado.pr_graba_ordenes(pn_tse_id          => :f_seg_id,',
'                                          pn_emp_id          => :f_emp_id,',
'                                          pn_tve_id          => :p141_tve_id,',
'                                          pn_rol_id          => :p0_rol,',
'                                          pn_uge_id          => :f_uge_id,',
'                                          pn_usu_id          => :f_user_id,',
'                                          pn_uge_id_gasto    => :f_uge_id,',
'                                          pn_age_id_agencia  => :f_age_id_agencia,',
'                                          pn_pol_id          => :p141_pol_id,',
'                                          pn_cli_id_destino  => :p141_cli_id,',
'                                          pn_age_id_agente   => :p141_age_id_agente_cab,',
'                                          pv_ord_tipo        => :p141_ord_tipo,',
'                                          pn_ord_plazo       => :p141_plazo_factura,',
'                                          pn_ede_id          => :p141_plan_tarjeta,',
'                                          pn_entrada         => :p141_entrada_cre,',
'                                          pn_monto_pagar     => :p141_monto_pagar,',
'                                          pn_valor_financiar => :p141_valor_financiar_cre,',
'                                          pn_ord_id          => :p141_ord_id,',
'                                          pn_ord_sec_id      => :p141_ord_sec_id,',
'                                          pv_observacion_est => :p141_eor_observaciones,',
'                                          pn_dir_id          => :p141_dir_id,',
'                                          pv_msg_graba_orden => :p0_msg_graba_orden,',
'                                          pv_forma_cre       => nvl(:P141_FORMA_CREDITO, ''M''),',
'                                          pv_error           => :p0_error);',
'  --INICIO ILIMA R2698-02 18/02/2020',
'  BEGIN',
'  ',
'    IF (:p141_ord_id IS NOT NULL) THEN',
'                --INICIO MPO-29 15-JUN-2023 ',
'      IF :p20_tve_id = asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                               ''cn_tve_id_tarjeta_credito'') THEN',
'        pq_ven_ordenes_dml.pr_ins_ven_ordenes_plan_tcred(pn_opt_id              => ln_opt_id,',
'                                                         pn_ord_id              => :p141_ord_id,',
'                                                         pv_opt_plan            => :p141_tipo_plan,',
'                                                         pn_com_id              => NULL,',
'                                                         pv_opt_estado_registro => pq_constantes.fn_retorna_constante(0,',
'                                                                                                                      ''cv_estado_reg_activo''),',
'                                                         pv_error               => :p0_error);',
'      END IF;',
'    --FIN MPO-29 15-JUN-2023 ',
'      lv_tipo_orden := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                          ''cv_ord_tipo_orden_rapida'');',
'    ',
'      pq_ven_ordenes.pr_graba_traza_orden_venta(pn_ord_id   => :p141_ord_id,',
'                                                pv_ord_tipo => lv_tipo_orden,',
'                                                pn_emp_id   => :f_emp_id,',
'                                                pn_usu_id   => :f_user_id,',
'                                                pc_error    => lc_error);',
'    ',
'    END IF;',
'  ',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'  END;',
'',
'  --FIN ILIMA R2698-02 18/02/2020',
'  ',
'  --INICIO TMONTALVAN R2826-03 15/07/2020',
'BEGIN',
' if :P141_ORDEN_EXTERNA_REF_VTEX is not null then',
'  IF (:p141_ord_id IS NOT NULL) THEN',
'    BEGIN',
'     SELECT c.per_id into ln_per_id',
'       FROM asdm_e.asdm_clientes c',
'      WHERE c.cli_id = :p141_cli_id',
'        AND c.emp_id = :f_emp_id',
'        AND c.cli_estado_registro = 0;  ',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        ln_per_id := NULL;',
'    END;',
'',
'    BEGIN',
'      SELECT e.veo_id, e.ord_id',
'        INTO ln_veo_id, ln_ord_id',
'        FROM ven_ordenes_externas e',
'       WHERE e.orderid = :P141_ORDEN_EXTERNA_REF_VTEX;',
'  ',
'      IF ln_ord_id is not null THEN',
'        raise_application_error(-20001,',
'                                ''Orden ORD_ID:'' || ln_ord_id ||',
'                              '' Ya ha sido generada'');',
'      ELSE',
'        UPDATE ven_ordenes_externas ee',
'           SET ee.ord_id    = :p141_ord_id,',
'               ee.veo_error = ''Orden procesada manualmente desde el SIC'',',
'               ee.veo_fecha = SYSDATE',
'         WHERE ee.orderid = :P141_ORDEN_EXTERNA_REF_VTEX',
'           AND ee.veo_id = ln_veo_id;',
'      END IF;',
'    EXCEPTION',
'      WHEN no_data_found THEN',
'      --mfidrovo R2844-03 27/07/2020 inicio',
'       pq_ven_ordenes_contado_dml.pr_ins_ven_ordenes_externas(pn_veo_id       => ln_veo_id,',
'                                                               pd_veo_fecha => SYSDATE,',
'                                                               pn_emp_id    => :f_emp_id,',
'                                                               pn_uge_id    => :f_uge_id,',
'                                                               pn_usu_id    => :f_user_id,',
'                                                               pv_orderid   => :P141_ORDEN_EXTERNA_REF_VTEX,',
'                                                               pn_cli_id    => :p141_cli_id,',
'                                                               pn_com_id    => NULL,',
'                                                               pv_veo_error => ''Orden procesada manualmente desde el SIC'',',
'                                                               pn_per_id    => ln_per_id,',
'                                                               pn_ord_id    => :p141_ord_id,',
'                                                               pv_oex_origen => :P141_ORIGEN, --mfidrovo R2844-03 27/07/2020',
'                                                               pv_error     => :p0_error);  ',
'    --mfidrovo R2844-03 27/07/2020 fin',
'    END;',
'  end if;                                                                  ',
'end if;',
' EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'END;',
'  --FIN TMONTALVAN R2826-03 15/07/2020',
'',
'',
'  -- dlopez 4 dic 2017 se eliminan colecciones de listado items',
'  IF apex_collection.collection_exists(''COL_VALORES'') THEN',
'    apex_collection.delete_collection(''COL_VALORES'');',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_NOMBRES'') THEN',
'    apex_collection.delete_collection(''COL_NOMBRES'');',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_LISTA'') THEN',
'    apex_collection.delete_collection(''COL_LISTA'');',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COLL_ITEMS_ORDEN'') THEN',
'    apex_collection.delete_collection(''COLL_ITEMS_ORDEN'');',
'  END IF;',
'  -------------------------------------------------------    ',
'  IF :f_user_id = 6 THEN',
'    ROLLBACK;',
'    raise_application_error(-20000, :p0_error || '' LLEGA '' || SQLERRM);',
'  END IF;',
'',
'  IF :p0_error IS NOT NULL THEN',
'    ROLLBACK;',
'    raise_application_error(-20000, :p0_error || '' '' || SQLERRM);',
'  END IF;',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    ROLLBACK;',
'    raise_application_error(-20000, :p0_error || '' '' || SQLERRM);',
'  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(7634506164043016652)
,p_internal_uid=>7601895787771438511
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15344893614319927943)
,p_process_sequence=>114
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_datos_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' if :p141_cli_id is not null and (:p141_telefono IS NULL OR :p141_direccion is null) then',
unistr('     :P0_error := ''Datos del cliente incompletos, para continuar complete la informaci\00F3n'';'),
'     raise_application_error(-20000,:P0_error); ',
'  end if;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''busca_cliente'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>15312640463050163017
,p_process_comment=>':request = ''busca_cliente'''
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63172609782436562142)
,p_process_sequence=>124
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_totales'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P141_PLAZO_CRE := NULL;',
':P141_ENTRADA_CRE:= NULL;',
':P141_VALOR_FINANCIAR_CRE:= NULL;',
':P141_TASA_CRE:= NULL;	',
':P141_CUOTA_CRE:= NULL;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'select null from apex_collections where collection_name = ''CO_DETALLE'';'
,p_process_when_type=>'NOT_EXISTS'
,p_internal_uid=>63140356631166797216
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(80146220183004509329)
,p_process_sequence=>134
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_back'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  pr_url(pn_app_destino => :F_APP_ORIGEN,',
'         pn_pag_destino => :F_PAG_ORIGEN,',
'         pv_request => ''redirect'',',
'         pv_parametros => ''P200_PER_TIPO_IDENTIFICACION,P200_PER_NRO_IDENTIFICACION,F_POPUP'',',
'         pv_valores => :P141_PER_TIPO_IDENTIFICACION||'',''||:P141_PER_NRO_IDENTIFICACION||'',N'',',
'         pv_evento => ''SUBMIT'');         ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(80146220052770509328)
,p_internal_uid=>80113967031734744403
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634686793966016929)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_bloqueo_temp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'if :F_AGE_ID_AGENCIA not in (15,21,19) then',
unistr('raise_application_error(-20000,''Estimado Usuario, esta opci\00F3n no est\00E1 activa para su agencia '');'),
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>7602433642696252003
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634678078963016913)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_llama_eliminar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--pq_ven_comunes.pr_elimina_coleccion(:P0_ERROR);',
'pq_ven_comunes.pr_elimina_coleccion_detalle(:p141_seq_id,:p0_error); -- PAOBERNAL 12-NOVIEMBRE-2018',
'',
':P141_EMP_ID := null;',
':P141_ieb_id := null;',
':P141_ite_sku_id := null;',
':P141_ITE_SEC_ID := null;',
':P141_cantidad  := null;',
':P141_stock  := null;',
':P141_costo  := null;',
':P141_precio  := null;',
':P141_iva  := null;',
':P141_valor  := null;',
':P141_SEQ_ID := null;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''Eliminar'' OR :P141_PER_ID IS NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602424927693251987
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634676066629016911)
,p_process_sequence=>80
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'CARGA_FECHAS_CORTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_car_credito.pr_obtener_fechas_primer_pago(pn_cli_id                => :p141_cli_id,',
'                                               pn_emp_id                => :f_emp_id,',
'                                               pn_tse_id                => :f_seg_id,',
'                                               pd_fecha                 => :p141_ord_fecha,',
'                                               pn_pol_id                => :p141_pol_id,',
'                                               pn_dias_gracia_1_venc    => :p141_dias_gracia_primer_venc,',
'                                               pn_mes_gracia_entre_venc => :p141_meses_gracia_entre_venc,',
'                                               pn_plazo_venta           => :p141_plazo_factura,',
'                                               pv_error                 => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>7602422915359251985
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634680447006016916)
,p_process_sequence=>80
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reiniciar_coleccion_cronograma'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'--if APEX_COLLECTION.COLLECTION_EXISTS  (p_collection_name => ''COLL_CRONOGRAMA'' ) then',
'--APEX_COLLECTION.DELETE_COLLECTION (p_collection_name => ''COLL_CRONOGRAMA'' );',
'--end if;',
'if apex_collection.collection_exists(''COL_COBERTURA'') then',
'apex_collection.delete_collection(''COL_COBERTURA'');',
'end if;',
'if apex_collection.collection_exists(''COL_CRONOGRAMA_SEG'') then',
'apex_collection.delete_collection(''COL_CRONOGRAMA_SEG'');',
'end if;',
'if apex_collection.collection_exists(''COL_SEG_BENEFICIARIOS'') then',
'apex_collection.delete_collection(''COL_SEG_BENEFICIARIOS'');',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>7602427295736251990
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634679226054016915)
,p_process_sequence=>105
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cobrador'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_age_id NUMBER;',
'  ln_zon_id NUMBER;',
'  lv_zona   VARCHAR2(100);',
'',
'BEGIN',
':P141_COBRADOR := pq_car_cobranza.fn_retorna_cobrador(:f_emp_id,',
'                                    :p141_cli_Id,',
'                                    :f_uge_Id);',
'if :P141_COBRADOR is null then',
' :P141_cobrador := ''SIN COBRADOR'';',
'end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':f_seg_id = 2'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602426074784251989
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634685626718016927)
,p_process_sequence=>225
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_descuentos_edita'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_llave VARCHAR2(500);',
'',
'BEGIN',
'  -- Busca llave',
'  BEGIN',
'    SELECT a.c003 || ''::'' || a.c005 || '':'' || a.c031, a.c033',
'      INTO lv_llave, :p141_tld_id',
'      FROM apex_collections a',
'     WHERE a.collection_name = ''CO_DETALLE''',
'       AND a.c003 = :p141_ite_sku_id',
'       AND (a.c006 IS NULL OR a.c008 = ''P'')',
'       AND (a.c031 = :p141_bodega OR a.c032 = :p141_lcm_id);',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      NULL;',
'    WHEN OTHERS THEN',
'      raise_application_error(-20000,',
'                              ''Error al busca llave para cargar descuento al editar'' ||',
'                              SQLERRM);',
'  END;',
'  -- Busca descuentos',
'',
'  SELECT c012',
'    INTO :p141_descto_linea',
'    FROM apex_collections',
'   WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'     AND c007 =',
'         pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                            ''cn_var_id_por_dscto'')',
'     AND c001 = lv_llave;',
'  SELECT c012',
'    INTO :p141_tipo_descto',
'    FROM apex_collections',
'   WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'     AND c007 =',
'         pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                            ''cn_var_id_tip_dscto'')',
'     AND c001 = lv_llave;',
'',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'  WHEN OTHERS THEN',
'    ROLLBACK;',
'  ',
'    raise_application_error(-20000,',
'                            ''Error al busca descuento al editar'' || SQLERRM);',
'  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''EDITAR'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7602432475448252001
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(41053676017056938)
,p_process_sequence=>255
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_load_opcion_excluida'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
unistr('R2409-12 FPALOMEQUE 2/7/2019 ''SIC - Parametrizaci\00F3n para inhabilitar opci\00F3n Orde de Venta por asesor y por Agencia'''),
'Cuando este proceso me retorne 1 -> El usuario conectado no debe poder abrir la orden de venta',
'Cuando este proceso me retorne 0 -> El usuario conectado puede abrir la orden de venta',
'*/',
'DECLARE',
'  ln_oex_id NUMBER;',
'BEGIN',
'',
'  SELECT oex.oex_id',
'    INTO ln_oex_id',
'    FROM asdm_e.asdm_opcion_excluida oex',
'   WHERE oex.oex_estado_registro = ''0''',
'     AND oex.emp_id = :f_emp_id',
'     AND (oex.usu_id = :f_user_id OR oex.uge_id = :f_uge_id)',
'     AND rownum = 1;',
'',
'  IF ln_oex_id IS NOT NULL THEN',
'    :p141_estado_inactivo := 1;',
'  END IF;',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    :p141_estado_inactivo := 0;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>8800524747292012
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634149594727203444)
,p_process_sequence=>255
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id      asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'  ln_cuenta_caja NUMBER;',
'  cv_activa varchar2(1):=''A'';',
'  cv_estado_reg_activo varchar2(1):=''0'';',
'BEGIN',
'   --INICIO ETENESACA R2698-01 18/02/2020',
'  -- Se realiza join con puntos emision para validar que el punto emision este activo',
'  -- Se obtiene datos de la caja para poder registrar ordenes',
'  SELECT count(1)  INTO ln_cuenta_caja',
'  FROM asdm_puntos_emision c, ven_periodos_caja d',
' WHERE d.emp_id = c.emp_id',
'   and d.emp_id = :F_EMP_ID',
'   AND d.pue_id = c.pue_id',
'   AND d.pca_estado_registro = cv_estado_reg_activo',
'   AND c.pue_estado_registro = cv_estado_reg_activo',
'  AND c.uge_id = :F_UGE_ID',
'   AND d.pca_estado_pc = cv_activa',
'   AND d.pca_token = cv_activa;',
'',
'',
'  IF ln_cuenta_caja > 0 THEN',
'    null;    ',
'  ELSE',
'    :p0_pue_id := NULL;',
'    raise_application_error(-20001,''Debe tener una Caja ACTIVA'');',
'  END IF;',
'   --FIN ETENESACA R2698-01 18/02/2020',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':p0_pue_id IS NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>7601896443457438518
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38545305815583500033)
,p_process_sequence=>255
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_editar_item'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :request = ''EDITAR_GEX'' THEN',
'    --iTEMS RELACIONADOS',
'    BEGIN',
'      SELECT a.seq_id || '':'' || a.c003 || '':'' || a.c006 || '':'' || a.c005 || '':'' ||',
'             a.c031 cadena',
'        INTO :p141_cadena',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''CO_DETALLE''',
'         AND a.c003 = nvl(:p141_gex, 0)',
'         AND a.c006 = :p141_ite_sku_id',
'         AND a.c007 IS NOT NULL',
'         AND (a.c031 = :p141_bodega OR a.c032 = :p141_lcm_id);',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        :p141_cadena := NULL;',
'    END;',
'  ELSIF :request = ''EDITAR'' THEN',
'   :p141_GEX:=null;',
'    --ITEMS TIPO STOCK',
'    BEGIN',
'      SELECT a.seq_id || '':'' || a.c003 || '':'' || a.c006 || '':'' || a.c005 || '':'' ||',
'             a.c031 cadena',
'        INTO :p141_cadena',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''CO_DETALLE''',
'         AND a.c003 = :p141_ite_sku_id',
'         AND (a.c006 IS NULL OR a.c008 IN (''P'', ''M''))',
'         AND (a.c031 = :p141_bodega OR a.c032 = :p141_lcm_id);',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        :p141_cadena := NULL;',
'    END;',
'  ELSE',
'    :p141_cadena := NULL;',
'  END IF;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'EDITAR, EDITAR_GEX'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>38513052664313735107
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38545305855511500034)
,p_process_sequence=>265
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_campos_edicion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  :p141_CADENA:=null;',
'  :p141_accion:=''N'';',
'  :p141_ITE_SKU_ID_REL := null;',
'  :p141_GEX:= null;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'EDITAR, EDITAR_GEX'
,p_process_when_type=>'REQUEST_NOT_IN_CONDITION'
,p_internal_uid=>38513052704241735108
);
wwv_flow_imp.component_end;
end;
/
