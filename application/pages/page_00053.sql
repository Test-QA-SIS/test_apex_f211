prompt --application/pages/page_00053
begin
--   Manifest
--     PAGE: 00053
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>53
,p_name=>unistr('Reimpresi\00BF\00BFn de Facturas')
,p_step_title=>unistr('Reimpresi\00BF\00BFn de Facturas')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
' ',
'',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(75795863370698546)
,p_plug_name=>unistr('Reimpresi\00BF\00BFn')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(75799658269753860)
,p_name=>'Factura'
,p_parent_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.COM_ID,',
'       a.PUE_NUM_SRI || ''-'' || a.UGE_NUM_SRI || ''-'' || a.COM_NUMERO numero,',
'       a.COM_FECHA,',
'       a.CLI_ID,',
'       a.nombre_completo,',
'       a.age_nombre_comercial',
'  FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO = :P53_COM_NUMERO',
'   AND a.AGE_ID_AGENCIA = :F_AGE_ID_AGENCIA',
'   AND a.COM_TIPO = :P53_COM_TIPO',
'   AND a.pue_num_sri = :P53_PUE_NUM_SRI',
'   AND a.uge_num_sri = :P53_UGE_NUM_EST_SRI',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT  count(*)',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO=:P53_COM_NUMERO',
' AND a.AGE_ID_AGENCIA=:F_AGE_ID_AGENCIA)> 0'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(75799960853753930)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(75800054682753936)
,p_query_column_id=>2
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>3
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(75800177814753937)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>1
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(75800282497753937)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(75800376556753937)
,p_query_column_id=>5
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>5
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(75800453818753937)
,p_query_column_id=>6
,p_column_alias=>'AGE_NOMBRE_COMERCIAL'
,p_column_display_sequence=>6
,p_column_heading=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(75876362105982041)
,p_plug_name=>'NUEVA SECUENCIA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT  count(*)',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO=:P53_COM_NUMERO',
' AND a.AGE_ID_AGENCIA=:F_AGE_ID_AGENCIA',
'AND a.com_tipo=:P53_COM_TIPO)> 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(75898965803125033)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_button_name=>'REIMPRIMIR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Reimprimir'
,p_button_position=>'BOTTOM'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT  count(*)',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO = :P93_COM_NUMERO',
'   AND a.AGE_ID_AGENCIA = :F_AGE_ID_AGENCIA',
'   AND a.COM_TIPO = :P93_COM_TIPO',
'   AND a.pue_num_sri = :P93_PUE_NUM_SRI',
'   AND a.uge_num_sri = :P93_UGE_NUM_EST_SRI) = 1'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(76034956600983269)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(94218666826972388)
,p_branch_action=>'f?p=&APP_ID.:53:&SESSION.:reimprimir:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(75898965803125033)
,p_branch_sequence=>20
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 27-SEP-2011 08:46 by ACALLE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(94217352497958851)
,p_branch_action=>'f?p=&APP_ID.:53:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 27-SEP-2011 08:44 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75797362809707887)
,p_name=>'P53_COM_NUMERO'
,p_is_required=>true
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_prompt=>'Documento Nro'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75877783922988369)
,p_name=>'P53_NUEVA_SECUENCIA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_prompt=>' - '
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75975566046692722)
,p_name=>'P53_DATFOLIO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_prompt=>'DATFOLIO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75975767419692730)
,p_name=>'P53_PUE_NUM_SRI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75975978060692730)
,p_name=>'P53_UGE_NUM_EST_SRI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_prompt=>'NUEVA SECUENCIA A IMPRIMIR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75976158727692731)
,p_name=>'P53_PUE_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_prompt=>'PUE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75976373904692731)
,p_name=>'P53_TTR_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_prompt=>'TTR_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75976553293692731)
,p_name=>'P53_PERIODO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_prompt=>'PERIODO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75976772115692731)
,p_name=>'P53_ERROR'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_prompt=>'ERROR'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75979354187708261)
,p_name=>'P53_TTR_DESCRIPCION'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(75876362105982041)
,p_prompt=>'TTR_DESCRIPCION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76032751967972477)
,p_name=>'P53_COM_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Com Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT   a.COM_ID',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO=:P53_COM_NUMERO',
' AND a.AGE_ID_AGENCIA=:F_AGE_ID_AGENCIA',
'AND a.COM_TIPO = :P53_COM_TIPO',
'   AND a.pue_num_sri = :P53_PUE_NUM_SRI',
'   AND a.uge_num_sri = :P53_UGE_NUM_EST_SRI'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76358653212165007)
,p_name=>'P53_COM_NUMERO_ANT'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_use_cache_before_default=>'NO'
,p_prompt=>'com_numero'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT   a.COM_NUMERO',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO=:P53_COM_NUMERO',
' AND a.AGE_ID_AGENCIA=:F_AGE_ID_AGENCIA;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76358860416165020)
,p_name=>'P53_TRX_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_use_cache_before_default=>'NO'
,p_prompt=>'trx_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76656281803529568)
,p_name=>'P53_COM_TIPO'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(75795863370698546)
,p_prompt=>'Tipo Documento'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS'
,p_lov=>'.'||wwv_flow_imp.id(76673757103664354)||'.'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(94233854941110906)
,p_validation_name=>'P53_COM_NUMERO'
,p_validation_sequence=>10
,p_validation=>'P53_COM_NUMERO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Nro del Documento'
,p_always_execute=>'Y'
,p_validation_condition=>'reimprimir'
,p_associated_item=>wwv_flow_imp.id(75797362809707887)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(76023977669885259)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REIMPRIMIR_FAC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'if :P53_COM_NUMERO_ANT is not null then',
'  pq_ven_comprobantes.pr_reimprimir_mayoreo(pn_emp_id         => :F_EMP_ID,',
'                                            pn_com_id         => :P53_COM_ID,',
'                                            pn_com_numero     => :P53_NUEVA_SECUENCIA,',
'                                            pn_com_numero_ant => :P53_COM_NUMERO_ANT,',
'                                            pn_age_id         => :F_AGE_ID_AGENCIA,',
'                                            pn_tse_id         => :f_seg_id,',
'                                            pn_uge_id         => :f_uge_id,',
'                                            pn_uge_id_gasto   => :f_uge_id_gasto,',
'                                            pn_usu_id         => :f_user_id,',
'                                            pn_pue_id         => :P53_PUE_ID,',
'                                            pn_trx_id         => :P53_TRX_ID,',
'                                            pv_error          => :P0_ERROR);',
'  :P53_COM_NUMERO_ANT := null;',
'  :P53_NUEVA_SECENCIA := null;',
'  :p53_com_numero := null;',
'else :p53_com_numero := null;',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(75898965803125033)
,p_process_when=>'reimprimir'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>43770826400120333
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(75973763013672940)
,p_process_sequence=>100
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'carga_datos_folio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%type;',
'',
'BEGIN',
'',
'  ln_tgr_id  := pq_ven_listas_caja.fn_tipo_grupo_transaccion_seg(:f_emp_id,:f_seg_id);',
'  :P53_TTR_ID := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id); ',
'',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       LN_TGR_ID,',
'      :P53_TTR_DESCRIPCION, ',
'      :P53_PERIODO,',
'      :P53_DATFOLIO,',
'      :P53_NUEVA_SECUENCIA,',
'      :P53_PUE_NUM_SRI  ,',
'      :P53_UGE_NUM_EST_SRI ,',
'      :P53_PUE_ID,',
'      :P53_TTR_ID,',
'      :p0_nro_folio,',
'      :P53_ERROR);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>43720611743908014
);
wwv_flow_imp.component_end;
end;
/
