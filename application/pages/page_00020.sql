prompt --application/pages/page_00020
begin
--   Manifest
--     PAGE: 00020
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>20
,p_name=>'DEFINICION_FACTURA'
,p_step_title=>'DEFINICION_FACTURA'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'/* LISTADO DE CONSTANTES */   ',
'var reg_ident = ''7993885998909229'';',
'',
'    ',
'//ACTUALIZA REGION',
'function fn_refresh_region(reg_id1){',
'    $a_report(reg_id1,''1'',500,''15''); ',
'}',
'    ',
'function fn_upd_check_iden(pn_seq_id,pn_sel,pn_ord_id,pv_identificador){',
'    var ajaxRequest = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=PR_UPD_CHECK_IDEN'',0);',
'    //alert(pn_seq_id);',
'    ajaxRequest.addParam(''x02'',pn_seq_id);',
'    ajaxRequest.addParam(''x03'',pn_sel);',
'    ajaxRequest.addParam(''x04'',pn_ord_id);',
'    ajaxRequest.addParam(''x05'',pv_identificador);',
'    var ajaxResult = ajaxRequest.get();      ',
'    ajaxRequest = null;',
'    ///fn_refresh_region(reg_ident);',
'    //refresh_ir_report();',
'    $(''#factura'').trigger(''apexrefresh'');',
'}',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20240119100834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(39286580153009492)
,p_plug_name=>'FACTURACION <B>&P0_TTR_DESCRIPCION. -    &P0_PERIODO.'
,p_region_name=>'factura'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'se quita el folio ya que los usuarios deben ingresar la factura que les toca',
'&P0_DATFOLIO.'))
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(45811658770270256)
,p_name=>'IDENTIFICADORES'
,p_parent_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apex_item.checkbox(p_idx                      => 15,',
'                          p_value                    => c.c006,',
'                          p_attributes               => ''onChange="fn_upd_check_iden('' ||',
'                                                        c.seq_id ||',
'                                                        '',this.value,'' ||',
'                                                        chr(39) ||',
'                                                        :p20_orden_venta ||',
'                                                        chr(39) || '','' ||',
'                                                        chr(39) || c.c002 ||',
'                                                        chr(39) || '')"'', --R2394-10 mfidrovo',
'                          p_checked_values           => ''1'',',
'                          p_checked_values_delimitor => '':'') seleccione,',
'       c.seq_id,',
'       c.c001 ebs_id,',
'       c.c002 identificador,',
'       c.c003 ite_sec_id,',
'       c.c004 descripcion_larga,',
'       c.c005 ite_sku_id,',
'       c.c007 tid_id,',
'       c.c008 tipo_identificador,',
'       c.c009 bodega',
'  FROM apex_collections c',
' WHERE c.collection_name = ''COL_IDENT_CONSIGNACION'' --''COL_IDENT_CONSIGNACION'''))
,p_display_when_condition=>'(:P20_VALIDA_IDENTIFICADOR >0 and asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,''cn_identificadores_factura'')=''S'') or :P20_ORD_TIPO IN (''FCO'',''FCOMO'')'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>100
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45811812483270257)
,p_query_column_id=>1
,p_column_alias=>'SELECCIONE'
,p_column_display_sequence=>1
,p_column_heading=>'Seleccione'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45811891476270258)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812049797270259)
,p_query_column_id=>3
,p_column_alias=>'EBS_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Ebs id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812128100270260)
,p_query_column_id=>4
,p_column_alias=>'IDENTIFICADOR'
,p_column_display_sequence=>4
,p_column_heading=>'Identificador'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812178131270261)
,p_query_column_id=>5
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Ite sec id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812290777270262)
,p_query_column_id=>6
,p_column_alias=>'DESCRIPCION_LARGA'
,p_column_display_sequence=>6
,p_column_heading=>'Descripcion larga'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812413710270263)
,p_query_column_id=>7
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Ite sku id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812511289270264)
,p_query_column_id=>8
,p_column_alias=>'TID_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Tid id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45812595240270265)
,p_query_column_id=>9
,p_column_alias=>'TIPO_IDENTIFICADOR'
,p_column_display_sequence=>9
,p_column_heading=>'Tipo identificador'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609050644155853)
,p_query_column_id=>10
,p_column_alias=>'BODEGA'
,p_column_display_sequence=>10
,p_column_heading=>'Bodega'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(56609121357155854)
,p_name=>'IDENTIFICADORES CARGADOS'
,p_parent_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'  FROM (SELECT',
'        ',
'         apex_item.checkbox(p_idx                      => 15,',
'                            p_value                    => 1,',
'                            p_attributes               => ''disabled="disabled"'', --R2394-10 mfidrovo',
'                            p_checked_values           => ''1'',',
'                            p_checked_values_delimitor => '':'') seleccione,',
'         ',
'         to_char(se.ebs_id) ebs_id,',
'         se.ebs_valor_identificador identificador,',
'         to_char(i.ite_sec_id) ite_sec_id,',
'         i.ite_descripcion_larga,',
'         to_char(i.ite_sku_id) ite_sku_id,',
'         to_char(se.tid_id) tid_id,',
'         (SELECT ti.tid_nombre',
'            FROM inv_tipos_identificador ti',
'           WHERE ti.tid_id = se.tid_id) tipo_identificador,',
'         0 orden,',
'         (SELECT uge.uge_nombre',
'            FROM inv_bodegas_propias bpr, asdm_unidades_gestion uge',
'           WHERE bpr.uge_id = uge.uge_id',
'             AND bpr_id = vd.bpr_id) bodega',
'        ',
'          FROM inv_items_estado_bodega_stock st,',
'               ven_ordenes_det               vd,',
'               inv_items_estado_bodega_serie se,',
'               inv_items                     i,',
'               inv_comprobantes_inventario   cin,',
'               inv_comprobantes_detalle      cd',
'         WHERE vd.ord_id = cin.ord_id',
'           AND cin.cin_id = cd.cin_id',
'           AND cd.ieb_id = se.ieb_id',
'           AND st.ite_sku_id = vd.ite_sku_id',
'           AND vd.ord_id = :p20_orden_venta',
'           AND i.ite_sku_id = st.ite_sku_id',
'           AND se.ieb_id = st.ieb_id',
'           AND se.ebs_estado_registro = 0',
'           AND vd.bpr_id = cin.bpr_id',
'           AND se.ebs_valor_identificador IN',
'               (SELECT ics.ics_valor_identificador',
'                  FROM mov_inv_despachos ide, mov_inv_comprobantes_serie ics',
'                 WHERE ide.ide_id = ics.ide_id',
'                   AND ide.ide_ord_id = :p20_orden_venta)',
'        UNION',
'        SELECT apex_item.checkbox(p_idx                      => 15,',
'                                  p_value                    => 0,',
'                                  p_attributes               => ''disabled="disabled"'', --R2394-10 mfidrovo',
'                                  p_checked_values           => ''1'',',
'                                  p_checked_values_delimitor => '':'') seleccione,',
'               c.c001 ebs_id,',
'               c.c002,',
'               c.c003,',
'               c.c004,',
'               c.c005,',
'               c.c007,',
'               c.c008,',
'               1,',
'               c.c009',
'          FROM apex_collections c',
'         WHERE c.collection_name = ''COL_IDENT_CONSIGNACION'' --''COL_IDENT_CONSIGNACION''',
'           AND (c.c005 = :p20_filtro_items OR :p20_filtro_items IS NULL)',
'           AND (c.c002 = :p20_filtro_identificadores OR',
'               :p20_filtro_identificadores IS NULL))',
' ORDER BY orden'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    ',
'       se.ebs_valor_identificador identificador',
'  FROM inv_items_estado_bodega_stock st,',
'       ven_ordenes_det               vd,',
'       inv_items_estado_bodega_serie se,',
'       inv_items                     i,',
'       inv_comprobantes_inventario   cin,',
'       inv_comprobantes_detalle      cd',
' WHERE vd.ord_id = cin.ord_id',
'   AND cin.cin_id = cd.cin_id',
'   AND cd.ieb_id = se.ieb_id',
'   AND st.ite_sku_id = vd.ite_sku_id',
'   AND vd.ord_id =:P20_ORDEN_VENTA and i.ite_sku_id = st.ite_sku_id',
'   AND se.ieb_id = st.ieb_id',
'   AND se.ebs_estado_registro = 0 AND vd.bpr_id = cin.bpr_id',
'   and se.ebs_valor_identificador in(',
'        select ics.ics_valor_identificador from   mov_inv_despachos ide,',
'                 mov_inv_comprobantes_serie ics',
'                 where ide.ide_id = ics.ide_id ',
'                 and  ide.ide_ord_id = :P20_ORDEN_VENTA)'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609187903155855)
,p_query_column_id=>1
,p_column_alias=>'SELECCIONE'
,p_column_display_sequence=>1
,p_column_heading=>'Seleccione'
,p_use_as_row_header=>'N'
,p_column_format=>'PCT_GRAPH:::'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609394167155857)
,p_query_column_id=>2
,p_column_alias=>'EBS_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Ebs Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609503470155858)
,p_query_column_id=>3
,p_column_alias=>'IDENTIFICADOR'
,p_column_display_sequence=>3
,p_column_heading=>'Identificador'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609593547155859)
,p_query_column_id=>4
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Ite Sec Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56610166209155865)
,p_query_column_id=>5
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>5
,p_column_heading=>unistr('Descripci\00F3n Larga')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609767377155861)
,p_query_column_id=>6
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Ite Sku Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609886992155862)
,p_query_column_id=>7
,p_column_alias=>'TID_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Tid Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56609951728155863)
,p_query_column_id=>8
,p_column_alias=>'TIPO_IDENTIFICADOR'
,p_column_display_sequence=>8
,p_column_heading=>'Tipo Identificador'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56610272748155866)
,p_query_column_id=>9
,p_column_alias=>'ORDEN'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(56610125526155864)
,p_query_column_id=>10
,p_column_alias=>'BODEGA'
,p_column_display_sequence=>9
,p_column_heading=>'Bodega'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(20132535965435043897)
,p_plug_name=>'Restriccion Politica'
,p_parent_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>50
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P20_POL_RESTRICCION = 1'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(165053578411140814731)
,p_plug_name=>'Pistoleo'
,p_parent_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>60
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT z.ode_id,',
'       z.bpr_id_bodega_despacho,',
'       u.uge_nombre unidad_gestion,',
'       ''Pistolear'' pistoleo,',
'       CASE WHEN LENGTH(items) > 100 THEN ',
'         SUBSTR(ITEMS, 0, 80) || ''........'' ',
'       ELSE',
'         items',
'       END items,',
'       (cantidad) cantidad',
'  FROM (SELECT od.ode_id,',
'               od.bpr_id_bodega_despacho,',
'               listagg(''&#9830; '' || substr(i.ite_descripcion_corta, 0, 35), ''</BR>'') within GROUP(ORDER BY i.ite_sku_id) items,',
'               SUM(dd.odd_cantidad) cantidad',
'          FROM inv_ordenes_despacho   od,',
'               inv_odespachos_detalle dd,',
'               inv_items              i',
'         WHERE od.ode_id = dd.ode_id',
'           AND dd.ite_sku_id = i.ite_sku_id',
'           AND od.emp_id = :f_emp_id',
'           AND od.ord_id = :p20_orden_venta',
'           AND dd.odd_estado NOT IN (''C'', ''E'', ''T'')',
'           AND i.ite_serie_obligatoria = ''S''',
'           AND NOT EXISTS (SELECT cab.scl_ode_id',
'                  FROM slm_cabecera_lista cab',
'                 WHERE cab.scl_estado_lista IN (''B'', ''C'')',
'                   AND cab.scl_ode_id = od.ode_id)',
'         GROUP BY od.ode_sec_id,',
'                  od.ode_id,',
'                  od.ord_id,',
'                  od.bpr_id_bodega_despacho) z,',
'       inv_bodegas_propias b,',
'       asdm_unidades_gestion u',
' WHERE u.uge_id = b.uge_id',
'   AND b.bpr_id = z.bpr_id_bodega_despacho',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(165053578497735814732)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'MFIDROVO'
,p_internal_uid=>165021325346466049806
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053578552116814733)
,p_db_column_name=>'ODE_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Ode id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053578680088814734)
,p_db_column_name=>'BPR_ID_BODEGA_DESPACHO'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Bpr id bodega despacho'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053578847019814735)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Unidad gestion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053578984208814737)
,p_db_column_name=>'ITEMS'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Items'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053578928254814736)
,p_db_column_name=>'PISTOLEO'
,p_display_order=>60
,p_column_identifier=>'D'
,p_column_label=>'Pistoleo'
,p_column_link=>'f?p=196:450:&SESSION.::&DEBUG.:RP,450:F_POPUP,F_EMP_ID,P450_ODE_ID,P450_BPR_ID,P450_NUM_IT:S,&F_EMP_ID.,#ODE_ID#,#BPR_ID_BODEGA_DESPACHO#,#CANTIDAD#'
,p_column_linktext=>'#PISTOLEO#'
,p_column_link_attr=>'style="color: #0269ff;"'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053579065529814738)
,p_db_column_name=>'CANTIDAD'
,p_display_order=>70
,p_column_identifier=>'F'
,p_column_label=>'Cantidad'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(165063825328861751044)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1650315722'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'ODE_ID:BPR_ID_BODEGA_DESPACHO:UNIDAD_GESTION:ITEMS:PISTOLEO:CANTIDAD'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(199707358629682957939)
,p_plug_name=>'Forma de Credito'
,p_parent_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>59
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(39293977190009507)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(39294181793009507)
,p_plug_name=>'INSERTA ARTICULOS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'NEVER'
,p_plug_display_when_condition=>'P20_COM_ID'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<!--',
'',
'function pull_multi_value2(pValue){',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ''APPLICATION_PROCESS=P_BUSCA_ITEM'',0);',
'if(pValue){',
'get.add(''F_ITE_SKU_ID'',pValue)',
'}else{',
'get.add(''F_ITE_SKU_ID'',''null'')',
'}    ',
'',
'    gReturn = get.get(''XML'');',
'',
'    if(gReturn){',
'        var l_Count = gReturn.getElementsByTagName("item").length;',
'        for(var i = 0;i<l_Count;i++){',
'            var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'            var l_ID = l_Opt_Xml.getAttribute(''id'');',
'            var l_El = html_GetElement(l_ID);    ',
'            if(l_Opt_Xml.firstChild){',
'                var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'            }else{',
'                var l_Value = '''';',
'            }',
'',
'            if(l_El){',
'                if(l_El.tagName == ''INPUT''){',
'                    l_El.value = l_Value;',
'                }else if(l_El.tagName == ''SPAN'' && ',
'                l_El.className == ''grabber''){',
'                    l_El.parentNode.innerHTML = l_Value;',
'                    l_El.parentNode.id = l_ID;',
'                }else{',
'                    l_El.innerHTML = l_Value;',
'                }',
'            }',
'        }',
'    }',
'    get = null;',
'',
'}',
'',
'//-->',
'</script>'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(39304551897009524)
,p_name=>'DetalleFactura'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_detalle(:P0_ERROR,:P20_ORD_TIPO);'
,p_display_when_condition=>'P20_POL_ID'
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>50
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(39304551897009524)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305267328009524)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305376693009524)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.::P20_SEQ_ID,P20_AGE_ID_AGENTE,P20_IEB_ID,P20_ITE_SKU_ID,P20_DESCRIPCION_LARGA,P20_ESTADO,P20_ESTADO_DES,P20_CANTIDAD:#COL03#,#COL04#,#COL05#,#COL06#,#COL07#,#COL08#,#COL09#,#COL10#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305473102009525)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305580004009525)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305665917009525)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305778440009525)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305857592009525)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305973287009525)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306080666009525)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306183306009525)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306271585009525)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39304767680009524)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39304879542009524)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39304951650009524)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305079732009524)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39305156242009524)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306369470009525)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306464405009525)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306577577009525)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306681519009525)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306753571009525)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306861085009525)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39306953176009525)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307058752009525)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307156568009525)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307253805009526)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307381666009526)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307477729009526)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307568529009526)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307673254009526)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307767280009526)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307882191009526)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39307952160009526)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308057643009526)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308158615009526)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308263854009526)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308359840009526)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308452930009526)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308581669009526)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308670920009526)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308780390009526)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308880552009526)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39308978925009526)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309069959009527)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309177380009527)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309264885009527)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309364048009527)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309461557009527)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309574623009527)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309665137009527)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309761102009527)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309880050009527)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39309962516009527)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310057013009527)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310168910009527)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310251766009527)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310372390009527)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310465926009528)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310578219009528)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39310659593009528)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(39317764401009580)
,p_name=>'TOTALES'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>110
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT vtt.vtt_etiqueta descripcion,',
'       round(VOR.VOR_valor_variable,2) valor',
'FROM   ven_ordenes          ORD,',
'       Ven_Var_Ordenes             VOR,',
'       ppr_variables_ttransaccion vtt',
'WHERE  ORD.ORD_id = VOR.ORD_id',
'       AND VOR.var_id = vtt.var_id',
'       AND vtt.ttr_id = pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_orden_venta'')',
'       AND vtt.vtt_mostrar_cabecera = pq_constantes.fn_retorna_constante(NULL,''cv_si'')',
'       AND ORD.ORD_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'       AND ORD.emp_id = :F_EMP_ID',
'       AND ORD.ORD_id = :P20_ORDEN_VENTA}'';'))
,p_display_when_condition=>'P20_POL_ID'
,p_display_condition_type=>'NEVER'
,p_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div style="width:210px">',
''))
,p_footer=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>5
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(39317764401009580)
,p_plug_column_width=>' valign="top"'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39317960239009580)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39318057187009580)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39318166848009581)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39318282414009581)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39318375205009581)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(39319478380009582)
,p_name=>'Detalle completo'
,p_display_sequence=>55
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT *',
'FROM apex_collections',
'WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_col_ord_det'')}'';'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270527874080046671)
,p_plug_query_max_columns=>66
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen datos'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320962287009622)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321058909009622)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39319682177009583)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Orden'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39319759608009595)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Detalle'
,p_use_as_row_header=>'N'
,p_column_alignment=>'CENTER'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39319853174009595)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Vendedor'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39319953204009595)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Id Item'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320054044009595)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320152977009595)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'SKU id'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320277749009595)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Descripcion Articulo'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320355592009595)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Material'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320470161009595)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Cantidad'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320570498009595)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Costo'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320655527009595)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Pvp Unitario'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320755471009622)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Pvp Total'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39320865133009622)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321169407009622)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321253865009622)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'IVA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321360736009622)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Total'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321461960009622)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321568122009622)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321665249009623)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321767074009623)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321857542009623)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39321967117009623)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322081017009623)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322181600009623)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322256237009623)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322379883009623)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322464778009623)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322563585009623)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322660247009623)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322755558009623)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322851539009623)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39322981956009623)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323056302009623)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323170351009624)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323255005009624)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323382298009624)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323455225009624)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323575375009624)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323657903009624)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323755588009624)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323858344009624)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39323960037009624)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324071987009624)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324166957009626)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324276700009626)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324364705009626)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324474216009626)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324577930009626)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324683258009626)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324782593009627)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324867914009627)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39324969008009627)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325056817009627)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325171019009627)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325275390009627)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325368373009627)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325463370009627)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325579212009627)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325681475009627)
,p_query_column_id=>61
,p_column_alias=>'COL61'
,p_column_display_sequence=>61
,p_column_heading=>'Col61'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325769291009627)
,p_query_column_id=>62
,p_column_alias=>'COL62'
,p_column_display_sequence=>62
,p_column_heading=>'Col62'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325874863009627)
,p_query_column_id=>63
,p_column_alias=>'COL63'
,p_column_display_sequence=>63
,p_column_heading=>'Col63'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39325957455009627)
,p_query_column_id=>64
,p_column_alias=>'COL64'
,p_column_display_sequence=>64
,p_column_heading=>'Col64'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39326062101009627)
,p_query_column_id=>65
,p_column_alias=>'COL65'
,p_column_display_sequence=>65
,p_column_heading=>'Col65'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39326164568009627)
,p_query_column_id=>66
,p_column_alias=>'COL66'
,p_column_display_sequence=>66
,p_column_heading=>'Col66'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(3952103960960282527)
,p_name=>'comprobantes_relacionados'
,p_parent_plug_id=>wwv_flow_imp.id(39319478380009582)
,p_display_sequence=>160
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select  pq_ven_ordenes_servicios.fn_detalle_aplica_garantia(pn_ord_id => :P20_ORDEN_VENTA) Aplica from dual'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select d.ord_id',
'  from ven_comprobantes_relacionados r, ven_ordenes_det d',
' where d.ode_id = r.ode_id_padre',
'   and d.ord_id = :P20_ORDEN_VENTA'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(3952104266112282544)
,p_query_column_id=>1
,p_column_alias=>'APLICA'
,p_column_display_sequence=>1
,p_column_heading=>'APLICA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(15330780047167638176)
,p_name=>'Identificadores Items'
,p_parent_plug_id=>wwv_flow_imp.id(39319478380009582)
,p_display_sequence=>10
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select i.ite_sec_id, i.ite_descripcion_larga, i.ite_serie_obligatoria serie_obligatoria, d.sdl_nro_serie',
'  from inv_ordenes_despacho   dc,',
'       inv_odespachos_detalle dd,',
'       slm_cabecera_lista     c,',
'       slm_detalle_lista      d,',
'       inv_items              i',
' where ord_id = :P20_ORDEN_VENTA',
'   and i.ite_sku_id = dd.ite_sku_id',
'   and dc.ode_id = dd.ode_id',
'   and c.scl_ode_id = dc.ode_id',
'   and d.ite_sku_id = dd.ite_sku_id',
'   and c.scl_id = d.scl_id;'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(23394676649254341227)
,p_query_column_id=>1
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ite sec id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(23394676741606341228)
,p_query_column_id=>2
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>2
,p_column_heading=>'Ite descripcion larga'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(23394676810497341229)
,p_query_column_id=>3
,p_column_alias=>'SERIE_OBLIGATORIA'
,p_column_display_sequence=>3
,p_column_heading=>'Serie obligatoria'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(23394676903134341230)
,p_query_column_id=>4
,p_column_alias=>'SDL_NRO_SERIE'
,p_column_display_sequence=>4
,p_column_heading=>'Sdl nro serie'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(39327982167009629)
,p_name=>'Reporte Totales'
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_comprobantes.fn_devuelve_totales_factura ',
'                          (:F_EMP_ID,',
'                           :P20_ORDEN_VENTA,',
'                           :P20_ERROR',
'                           );',
'',
'',
''))
,p_display_when_condition=>'P20_POL_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_header=>'<div style="width:210px">'
,p_footer=>'</div'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270527874080046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'NO_HEADINGS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328152880009630)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328262618009630)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328366978009630)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328461617009630)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328554012009630)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328678802009630)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328782775009630)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328852221009630)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39328959921009630)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329080892009630)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329171617009630)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329274837009630)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329366317009630)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329465119009630)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329563776009630)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329660237009630)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329783840009630)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329862047009631)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39329968420009631)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330055402009631)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330163956009631)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330253370009631)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330373142009631)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330461505009633)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330563004009633)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330658499009633)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330764038009633)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330875985009633)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39330982206009633)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331054046009633)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331169397009633)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331275553009633)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331353667009633)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331461053009633)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331582627009633)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331653789009633)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331765534009634)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331863065009634)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39331964091009634)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332079534009634)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332183930009634)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332257140009634)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332374077009634)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332463769009634)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332551273009634)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332664717009634)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332781229009634)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332853147009634)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39332977322009634)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333076772009634)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333155706009634)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333282497009634)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333362491009635)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333455087009635)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333555249009635)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333676813009635)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333775392009635)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333862364009635)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39333961442009635)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39334080198009635)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(111120174679259767)
,p_name=>'pruebasfp'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name=''COL_ORD_DETALLE'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111120474490259822)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111120563372259837)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111120652630259837)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111120752739259837)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111120861270259837)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111120976259259837)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121067171259837)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121151586259837)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121278692259837)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121375710259837)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121451299259837)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121577157259837)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121681624259837)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121777403259837)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121883783259837)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111121964477259837)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122079969259838)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122153676259838)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122256509259838)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122381421259838)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122452603259838)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122560637259838)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122664877259838)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122766607259838)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122874648259838)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111122970948259838)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123075059259838)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123171173259838)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123283567259838)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123364200259838)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123471327259838)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123557218259838)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123669540259838)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123758485259838)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123868011259838)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111123958809259839)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124082841259839)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124178014259839)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124275195259839)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124353255259839)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124457235259839)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124568083259839)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124658074259839)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124753629259839)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124854545259839)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111124979910259839)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125077364259839)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125165168259839)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125277418259839)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125381272259839)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125460879259839)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125556068259839)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125655461259839)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125775945259839)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125881806259840)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111125976822259840)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126069277259840)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126151680259840)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126269480259840)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126356041259840)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126474070259840)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126579287259840)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126657572259840)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126762259259840)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126872090259840)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(111126982570259840)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(30627313283208086121)
,p_plug_name=>'boton_activos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>140
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(30627433376413093594)
,p_name=>'Analisis del Cliente'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>140
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT s.seq_id seq_id,',
'       s.c001   pan_id,',
'       s.c002   parametro_analisis,',
'       s.c003   tipo_parametro,',
'       s.c004   resultado_parametro,',
'       s.c005   valor_parametro,',
'       s.c006   observacion,',
's.c007',
'  FROM apex_collections s',
' WHERE s.collection_name = ''COLL_ANALISIS'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627438683036093996)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439271934094012)
,p_query_column_id=>2
,p_column_alias=>'PAN_ID'
,p_column_display_sequence=>2
,p_column_heading=>'PAN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439357035094012)
,p_query_column_id=>3
,p_column_alias=>'PARAMETRO_ANALISIS'
,p_column_display_sequence=>3
,p_column_heading=>'PARAMETRO_ANALISIS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439471443094012)
,p_query_column_id=>4
,p_column_alias=>'TIPO_PARAMETRO'
,p_column_display_sequence=>4
,p_column_heading=>'TIPO_PARAMETRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439566017094012)
,p_query_column_id=>5
,p_column_alias=>'RESULTADO_PARAMETRO'
,p_column_display_sequence=>5
,p_column_heading=>'RESULTADO_PARAMETRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439671210094012)
,p_query_column_id=>6
,p_column_alias=>'VALOR_PARAMETRO'
,p_column_display_sequence=>6
,p_column_heading=>'VALOR_PARAMETRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439756486094012)
,p_query_column_id=>7
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>7
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30627439864607094012)
,p_query_column_id=>8
,p_column_alias=>'C007'
,p_column_display_sequence=>8
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(30627486855073096954)
,p_plug_name=>'boton_activos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>150
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39326260927009627)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_imp.id(39327982167009629)
,p_button_name=>'Facturar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_image_alt=>'Siguiente >>'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>':p0_TTR_ID= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante'') AND :P20_ERROR_FACTURA IS NULL and  :P20_COM_NUM = :P20_COM_NUMERO AND :P20_POL_RESTRICCION = 0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(73495573642746202)
,p_button_sequence=>100
,p_button_plug_id=>wwv_flow_imp.id(39327982167009629)
,p_button_name=>'facturar_may'
,p_button_static_id=>'facturar_may'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Facturar'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>':p0_TTR_ID= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'') AND :P20_ERROR_FACTURA IS NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39294958485009508)
,p_button_sequence=>13
,p_button_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_button_name=>'P20_AGREGAR_ARTICULOS'
,p_button_static_id=>'P20_AGREGAR_ARTICULOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>unistr('Agregar Art\00C2\00BF\00C2\00BF\00C2\00BF\00C2\00BFculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>'P20_TVE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_request_source=>'pr_cargar_detalle'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39310761484009528)
,p_button_sequence=>430
,p_button_plug_id=>wwv_flow_imp.id(39304551897009524)
,p_button_name=>'P20_ELIMINAR_LINEA'
,p_button_static_id=>'P20_ELIMINAR_LINEA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>unistr('Eliminar_Art\00C2\00BF\00C2\00BF\00C2\00BF\00C2\00BFculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>'P20_COM_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_request_source=>'Eliminar'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39286753077009492)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_button_name=>'REGRESAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_condition=>':P20_ES_CONSIGNACION = ''S'''
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(30626907471562059730)
,p_branch_name=>'br_imprime_lavado'
,p_branch_action=>'f?p=&APP_ID.:20:&SESSION.:imprime_lavado:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>5
,p_branch_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(39336067066009642)
,p_branch_action=>'f?p=&APP_ID.:16:&SESSION.::&DEBUG.:20::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(66915655547407005)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(39336255010009642)
,p_branch_name=>'Go To Page 30'
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.:factura:&DEBUG.:30:P30_CLI_ID,P30_ORD_ID,P30_FACTURAR,P30_TVE_ID,P30_TFP_ID,P30_BANCOS,P30_TARJETA,P30_TIPO,P30_PLAN,P30_ENTRADA,P30_COMISION,P30_CLI_NOMBRE,P30_COT_ID,P30_ORD_TIPO,P30_POLITICA_VENTA,P30_CPR_ID,P30_ORD_ID_REFERIDOR,P30_FORMA_CREDITO,P30_FORMA,P30_RPC_ID:&P20_CLI_ID.,&P20_ORDEN_VENTA.,S,&P20_TVE_ID.,&P20_FORMA_PAGO.,&P20_BANCOS.,&P20_TARJETA.,&P20_TIPO.,&P20_PLAN.,&P20_ENTRADA.,&P20_COMISION.,&P20_NOMBRE.,&P20_COT_ID.,&P20_ORD_TIPO.,&P20_POL_ID.,&P20_CPR_ID.,&P20_ORD_ID_REFERIDOR.,&P20_FORMA_CRE.,&P20_FORMA.,'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(39326260927009627)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(36153275070951386)
,p_branch_action=>'f?p=&APP_ID.:20:&SESSION.:facturar_may:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(73495573642746202)
,p_branch_sequence=>12
,p_branch_comment=>'Created 22-AUG-2012 13:55 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(74006369280407294)
,p_branch_action=>'f?p=&APP_ID.:27:&SESSION.::&DEBUG.:51::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(73495573642746202)
,p_branch_sequence=>15
,p_branch_comment=>'Created 18-JUL-2011 11:55 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(39336462563009643)
,p_branch_action=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>40
,p_branch_condition_type=>'NEVER'
,p_branch_comment=>'Created 28-SEP-2009 09:03 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39286961863009495)
,p_name=>'P20_TIPO_TELEFONO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Telefono'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  tte.tte_descripcion',
'  FROM asdm_telefonos tel,asdm_tipos_telefonos tte, asdm_personas pers ,asdm_clientes cli',
'  WHERE tel.tte_id = tte.tte_id',
'   AND  pers.per_id=tel.per_id ',
'   AND pers.per_id = cli.per_id',
'   AND tel.tel_id =pq_asdm_clientes.fn_tel_principal_mostrar(cli.per_id,:f_seg_id)',
'   AND tel.tel_estado_registro =pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND cli.cli_id=:p20_cli_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39287168705009495)
,p_name=>'P20_TIPO_DIRECCION'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Direccion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tdi.tdi_descripcion',
'   FROM asdm_direcciones dir, asdm_tipos_direccion tdi, asdm_personas pers, asdm_clientes cli',
'  WHERE dir.tdi_id = tdi.tdi_id',
'  AND dir.per_id=pers.per_id',
'  AND pers.per_id=cli.per_id',
'  AND dir.dir_id = pq_asdm_clientes.fn_dir_principal_mostrar(cli.per_id,:f_seg_id,:f_emp_id)',
'  AND cli.cli_id=:p20_cli_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39287370384009496)
,p_name=>'P20_COND_CAB'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cond Cab'
,p_source=>':P20_POL_ID||''-''||:P20_TVE_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39287552407009496)
,p_name=>'P20_PLAZO_ANT'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Plazo Ant'
,p_source=>':P20_PLAZO_FACTURA'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39287762504009496)
,p_name=>'P20_TVE_ID'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Terminos de Venta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_named_lov=>'LOV_TERMINOS_VEN_FAC'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select TVE_DESCRIPCION d, TVE_ID  r from ASDM_E.VEN_TERMINOS_VENTA l',
'where l.tve_estado_registro = 0',
'and l.emp_id = :F_EMP_ID'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P20_POL_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P20_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Terminos de venta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'LOV'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39288267965009500)
,p_name=>'P20_COM_FECHA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha'
,p_source=>'COM_FECHA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>12
,p_cMaxlength=>255
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39288468402009501)
,p_name=>'P20_COM_OBSERVACIONES'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>255
,p_cHeight=>2
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when=>'P20_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39288681694009501)
,p_name=>'P20_DIRECCION'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT dir.dir_descripcion',
'   FROM asdm_direcciones dir, asdm_tipos_direccion tdi, asdm_personas pers, asdm_clientes cli',
'  WHERE dir.tdi_id = tdi.tdi_id',
'  AND dir.per_id=pers.per_id',
'  AND pers.per_id=cli.per_id',
'  AND dir.dir_id = pq_asdm_clientes.fn_dir_principal_mostrar(cli.per_id,:f_seg_id,:f_emp_id)',
'  AND cli.cli_id=:p20_cli_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39288879969009501)
,p_name=>'P20_IDENTIFICACION'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Identificacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="pull_multi_value(this.value)";'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>':'
,p_read_only_when=>'P20_COM_ID'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39289076072009501)
,p_name=>'P20_CORREO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Correo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.per_correo_electronico FROM  v_asdm_clientes_personas a',
'WHERE a.cli_id=:p20_cli_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>57
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39289258699009502)
,p_name=>'P20_NOMBRE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Nombre'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT',
'vcli.nombre_completo',
' FROM v_asdm_clientes_personas vcli ',
' WHERE vcli.cli_id=:P20_CLI_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'disabled = ''disabled'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-BOTTOM'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39289455394009502)
,p_name=>'P20_AGE_ID_AGENTE'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Age_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>5
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'P20_disable_value'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39289672238009502)
,p_name=>'P20_TELEFONO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT   tel.tel_numero',
'  FROM asdm_telefonos tel,asdm_tipos_telefonos tte, asdm_personas pers ,asdm_clientes cli',
'  WHERE tel.tte_id = tte.tte_id',
'   AND  pers.per_id=tel.per_id ',
'   AND pers.per_id = cli.per_id',
'   AND tel.tel_id =pq_asdm_clientes.fn_tel_principal_mostrar(cli.per_id,:f_seg_id)',
'   AND tel.tel_estado_registro =pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND cli.cli_id=:p20_cli_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39289860437009502)
,p_name=>'P20_POL_ID'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Politica'
,p_source=>'pol_id'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT a.pol_id || '' - '' || a.pol_descripcion_larga alias, a.pol_id',
'      FROM ppr_politicas                a,',
'           ppr_politicas_terminos_venta b,',
'           ppr_politicas_agencia        c',
'     WHERE b.pol_id = a.pol_id',
'       AND c.pol_id = a.pol_id',
'       AND a.emp_id = :f_emp_id',
'       AND a.pol_estado_registro = ''0''',
'       AND a.tse_id = :F_SEG_ID',
'       AND b.tve_id = :P20_TVE_ID',
'       AND c.age_id = :F_AGE_ID_AGENCIA',
'       AND a.pol_fecha_desde <= trunc(SYSDATE)',
'       AND a.pol_fecha_hasta >= trunc(SYSDATE)',
'       AND c.pag_fecha_desde <= trunc(SYSDATE)',
'       AND c.pag_fecha_hasta >= trunc(SYSDATE)',
'       AND NOT EXISTS',
'     (SELECT NULL FROM ppr_politicas_clientes d WHERE d.pol_id = a.pol_id) ',
'          UNION ALL',
'          SELECT a.pol_id || '' - '' || a.pol_descripcion_larga, a.pol_id',
'            FROM ppr_politicas                a,',
'                 ppr_politicas_terminos_venta b,',
'                 ppr_politicas_clientes       d',
'           WHERE b.pol_id = a.pol_id',
'             AND d.pol_id = a.pol_id',
'             AND d.cli_id = :P20_CLI_ID',
'             AND a.emp_id = :f_emp_id',
'             AND a.pol_estado_registro = ''0''',
'             AND d.pcl_estado_registro = ''0''',
'             AND d.emp_id = :f_emp_id',
'             AND a.tse_id = :F_SEG_ID',
'             AND b.tve_id = :P20_TVE_ID',
'             AND a.pol_fecha_desde <= trunc(SYSDATE)',
'             AND a.pol_fecha_hasta >= trunc(SYSDATE)',
'             AND d.pcl_fecha_desde <= trunc(SYSDATE)',
'             AND d.pcl_fecha_hasta >= trunc(SYSDATE) ',
'             order by 2',
''))
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P20_cli_id'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Debe ingresar la politica con la cual desea facturar'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39290381766009503)
,p_name=>'P20_COM_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'com_id'
,p_pre_element_text=>'<b><font COLOR="#990000">'
,p_post_element_text=>'</font>'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_display_when=>':p0_TTR_ID!= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39290561373009503)
,p_name=>'P20_AGE_ID_AGENCIA'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Age Id Agencia'
,p_source=>'F_AGE_ID_AGENCIA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39290758610009503)
,p_name=>'P20_CLIENTE_EXISTE'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Cliente Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange=''pull_multi_value(this.value)'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39290980909009504)
,p_name=>'P20_TRX_ID'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39291171077009504)
,p_name=>'P20_PLAZO_FACTURA'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Plazo Factura'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P20_TVE_ID'
,p_read_only_when2=>'2'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39291359360009504)
,p_name=>'P20_PLAN_TARJETA'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Plan Tarjeta'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select (SELECT sys_connect_by_path(ede.ede_descripcion, ''->'') ',
'  FROM asdm_entidades_destinos ede',
'  where ede.ede_id = ord.ede_id',
' START WITH ede.ede_id_padre IS NULL',
'CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre)',
'  from ven_ordenes ord',
' where ord_id = :p20_orden_venta',
'   and emp_id = :f_emp_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P20_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39291555296009504)
,p_name=>'P20_ORDEN_VENTA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Secuencia Orden'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39291778673009505)
,p_name=>'P20_SALDO'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Saldo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39291966378009505)
,p_name=>'P20_COM_NUMERO'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_source=>':P0_FOL_SEC_ACTUAL'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39292158320009505)
,p_name=>'P20_COM_TIPO'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Com Tipo'
,p_source=>'F'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39292361024009505)
,p_name=>'P20_LCM_ID'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'LCM_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39292555714009505)
,p_name=>'P20_ORD_TIPO'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Ord Tipo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p0_TTR_ID= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39292774783009506)
,p_name=>'P20_DIR_ID_ENVIAR_FACTURA'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Direcci\00C2\00BF\00C2\00BF\00C2\00BF\00C2\00BFn Entrega Factura')
,p_source=>'29320'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_DIRECCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov VARCHAR2(8000);',
'BEGIN',
'    lv_lov := pq_ven_listas.fn_lov_direcciones_per(nvl(:P20_CLI_ID,0),nvl(:f_seg_id,0), nvl(null,0), :p0_error);',
'    RETURN(lv_lov);',
'END;',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p0_TTR_ID= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39292960426009506)
,p_name=>'P20_CLI_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Cliente id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>30
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39293161344009506)
,p_name=>'P20_ES_CONSIGNACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Es Consignacion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39293374172009506)
,p_name=>'P20_DIR_ID'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Dir Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39293567769009507)
,p_name=>'P20_CLI_ID_REFERIDO'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'ID Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39293754889009507)
,p_name=>'P20_NOMBRE_CLI_REFERIDO'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Cliente Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39294371650009508)
,p_name=>'P20_IEB_ID'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39294555684009508)
,p_name=>'P20_ITE_SKU_ID'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_prompt=>unistr('Art\00C2\00BF\00C2\00BF\00C2\00BF\00C2\00BFculo')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ITEMS_FAC'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_items_ordenes(',
'                         :f_emp_id,',
'                         :P25_BODEGA,',
'                         :P25_ESTADO, ',
'                         :p0_error); ',
'  RETURN(lv_lov);',
'END;',
'',
''))
,p_cSize=>5
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="pull_multi_value2(this.value)";'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P20_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39294769147009508)
,p_name=>'P20_CANTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>5
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P20_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39295156391009509)
,p_name=>'P20_STOCK'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39295367915009509)
,p_name=>'P20_ESTADO'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>2
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39295557339009509)
,p_name=>'P20_SEQ_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39295756183009509)
,p_name=>'P20_BODEGA'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_source=>'pq_ven_listas.fn_retorna_bodega_xdefecto(:P20_AGE_ID_AGENCIA,:P20_ERROR);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_read_only_when=>'P20_COM_ID'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39295968195009510)
,p_name=>'P20_DESCRIPCION_LARGA'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_prompt=>'Descripcion Larga'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39296163709009510)
,p_name=>'P20_ESTADO_DES'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Estado Art\00C2\00BF\00C2\00BF\00C2\00BF\00C2\00BFculo')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>2
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39296358589009510)
,p_name=>'P20_DESCTO_LINEA'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_item_default=>'0'
,p_prompt=>'Descto Linea'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39296557282009510)
,p_name=>'P20_TIPO_DESCTO'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_item_default=>'0'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC2:%;1,Valor;0'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P20_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39296751883009511)
,p_name=>'P20_BODEGA_NOMBRE'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(39294181793009507)
,p_prompt=>'Bodega'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge.uge_nombre',
'FROM   inv_bodegas_propias   bpr,',
'       asdm_unidades_gestion uge',
'WHERE  bpr.uge_id = uge.uge_id',
'       AND bpr.bpr_id = :P20_BODEGA'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P20_PLAZO_FACTURA'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39318467043009581)
,p_name=>'P20_ENTRADA_CRE'
,p_item_sequence=>692
,p_item_plug_id=>wwv_flow_imp.id(39317764401009580)
,p_prompt=>'Entrada Cre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>19
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39318666659009581)
,p_name=>'P20_VALOR_FINANCIAR_CRE'
,p_item_sequence=>702
,p_item_plug_id=>wwv_flow_imp.id(39317764401009580)
,p_prompt=>'Valor Financiar Cre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>19
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39318873792009581)
,p_name=>'P20_TASA_CRE'
,p_item_sequence=>712
,p_item_plug_id=>wwv_flow_imp.id(39317764401009580)
,p_prompt=>'Tasa Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39319054721009581)
,p_name=>'P20_CUOTA_CRE'
,p_item_sequence=>722
,p_item_plug_id=>wwv_flow_imp.id(39317764401009580)
,p_prompt=>'Cuota Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39319255980009582)
,p_name=>'P20_PLAZO_CRE'
,p_item_sequence=>732
,p_item_plug_id=>wwv_flow_imp.id(39317764401009580)
,p_prompt=>'Plazo Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45812704065270266)
,p_name=>'P20_VALIDA_IDENTIFICADOR'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45812928078270268)
,p_name=>'P20_TEI_ID_SITUACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45812964103270269)
,p_name=>'P20_CEI_ID_CONSIG_EN'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45813137215270270)
,p_name=>'P20_TEI_ID_CALIDAD'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(50991257272337924)
,p_name=>'P20_EBS_ID'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Identificador.'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_IDENTIFICADORES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_identificadores(:P20_ORD_TIPO,:P20_ODE_ID,:P20_ORDEN_VENTA, :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P20_ODE_ID'
,p_ajax_items_to_submit=>'P20_ODE_ID'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    ln_count_iden number := 0;',
'begin',
'    begin',
'        SELECT se.ebs_id',
'          INTO ln_count_iden',
'                  FROM inv_items_estado_bodega_stock st,',
'                       ven_ordenes_det               vd,',
'                       inv_items_estado_bodega_serie se,',
'                       inv_items                     i,',
'                       inv_comprobantes_inventario   cin,',
'                       inv_comprobantes_detalle      cd',
'                 WHERE vd.ord_id = cin.ord_id',
'                   AND cin.cin_id = cd.cin_id',
'                   AND cd.ieb_id = se.ieb_id',
'                   AND st.ite_sku_id = vd.ite_sku_id',
'                   AND vd.ord_id = :p20_orden_venta',
'                   AND i.ite_sku_id = st.ite_sku_id',
'                   AND se.ieb_id = st.ieb_id',
'                   AND se.ebs_estado_registro = 0',
'                   AND vd.bpr_id = cin.bpr_id',
'                   AND exists (SELECT ics.ics_valor_identificador',
'                          FROM mov_inv_despachos ide, mov_inv_comprobantes_serie ics',
'                         WHERE ide.ide_id = ics.ide_id',
'                           AND ide.ide_ord_id = :p20_orden_venta',
'                           AND ics.ics_valor_identificador = se.ebs_valor_identificador);',
'    exception',
'        when others then',
'            ln_count_iden :=0;',
'    end;',
'    ',
'    if :P20_ORD_TIPO IN (''VENMO'') AND :P20_VALIDA_IDENTIFICADOR = 0 and ln_count_iden = 0 then',
'        return true;',
'    else',
'        return false;',
'    end if;',
'end;'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'FUNCTION_BODY'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55041357091765136)
,p_name=>'P20_FORMA'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(57278256496320677)
,p_name=>'P20_COBRADOR'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Cobrador'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67236779288977853)
,p_name=>'P20_MARGEN_FACTURA'
,p_item_sequence=>742
,p_item_plug_id=>wwv_flow_imp.id(39319478380009582)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''MG'' || to_char(to_number(round((',
'(select SUM(v.vod_valor_variable) from ven_ordenes_det d, ven_var_ordenes_det v',
'where d.ord_id = :P20_ORDEN_VENTA',
'and v.ode_id = d.ode_id',
'and v.var_id = 3136',
'and d.ite_sku_id not in',
'(select c.ite_sku_id from v_inv_items_categoria c',
'where c.cat_id in(pq_constantes.fn_retorna_constante(d.emp_id,''cn_cat_id_gex''),pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                             ''cn_tre_id_gar_moto'')))',
'))/',
'(',
'select SUM(v.vod_valor_variable) from ven_ordenes_det d, ven_var_ordenes_det v',
'where d.ord_id = :P20_ORDEN_VENTA',
'and v.ode_id = d.ode_id',
'and v.var_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_var_id_venta_neta'')',
'and d.ite_sku_id not in',
'(select c.ite_sku_id from v_inv_items_categoria c',
'where c.cat_id in(pq_constantes.fn_retorna_constante(d.emp_id,''cn_cat_id_gex''),pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                             ''cn_tre_id_gar_moto'')))',
'),4)*100),''999990.99'')),'' '','''')',
'from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''MG'' || to_char(to_number(round((',
'(select SUM(v.vod_valor_variable) from ven_ordenes_det d, ven_var_ordenes_det v',
'where d.ord_id = :P20_ORDEN_VENTA',
'and v.ode_id = d.ode_id',
'and v.var_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_var_id_venta_neta'')',
'and d.ite_sku_id not in',
'(select c.ite_sku_id from v_inv_items_categoria c',
'where c.cat_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_cat_id_gex''))',
')',
'-',
'(select SUM(v.vod_valor_variable) from ven_ordenes_det d, ven_var_ordenes_det v',
'where d.ord_id = :P20_ORDEN_VENTA',
'and v.ode_id = d.ode_id',
'and v.var_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_var_id_costo_total'')',
'and d.ite_sku_id not in',
'(select c.ite_sku_id from v_inv_items_categoria c',
'where c.cat_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_cat_id_gex''))',
'))/',
'(',
'select SUM(v.vod_valor_variable) from ven_ordenes_det d, ven_var_ordenes_det v',
'where d.ord_id = :P20_ORDEN_VENTA',
'and v.ode_id = d.ode_id',
'and v.var_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_var_id_venta_neta'')',
'and d.ite_sku_id not in',
'(select c.ite_sku_id from v_inv_items_categoria c',
'where c.cat_id = pq_constantes.fn_retorna_constante(d.emp_id,''cn_cat_id_gex''))',
'),4)*100),''999990.99'')),'' '','''')',
'from dual'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(73990157927309333)
,p_name=>'P20_ODE_ID'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.ode_id',
'FROM   inv_ordenes_despacho d, inv_odespachos_detalle odd, inv_items_categorias ic',
'WHERE  d.ord_id = :P20_ORDEN_VENTA',
'and odd.ode_id = d.ode_id',
'and odd.ite_sku_id = ic.ite_sku_id',
'AND ic.cat_id IN',
'               (SELECT c.cat_id',
'                  FROM asdm_e.inv_categorias c',
'                CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'                 START WITH c.cat_id =',
'                            asdm_p.pq_constantes.fn_retorna_constante(d.emp_id,',
'                                                                      ''cn_cat_id_motorizados''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(89170568731610322)
,p_name=>'P20_ESTABLECIMIENTO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Establecimiento'
,p_source=>':P0_UGE_NUM_EST_SRI'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p0_TTR_ID != pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(89170782895610328)
,p_name=>'P20_PUNTO_EMISION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Punto de Emision'
,p_source=>':P0_PUE_NUM_SRI'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p0_TTR_ID!= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(89358265182028383)
,p_name=>'P20_ORD_SEC_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'# Orden'
,p_source=>'select lPAD(ord_sec_id,10,'' '') from ven_ordenes where ord_id = :P20_ORDEN_VENTA'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(89566475392043615)
,p_name=>'P20_TEO_ID'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Teo Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(89571856046066415)
,p_name=>'P20_ERROR_FACTURA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:16px; color: red;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94420869950499329)
,p_name=>'P20_COM_NUM'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Secuencia Factura'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p0_TTR_ID!= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when=>':P20_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98248769379111364)
,p_name=>'P20_FORMA_PAGO'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Forma Pago'
,p_source=>'select decode(:p20_tve_id,pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''),pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito''),null) from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98275482379228626)
,p_name=>'P20_BANCOS'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Bancos'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98275655498230365)
,p_name=>'P20_TARJETA'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Tarjeta'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98275862771232393)
,p_name=>'P20_TIPO'
,p_item_sequence=>440
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Tipo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98276168658234085)
,p_name=>'P20_PLAN'
,p_item_sequence=>460
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98430971549585672)
,p_name=>'P20_ENTRADA'
,p_item_sequence=>470
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98860151900504004)
,p_name=>'P20_COMISION'
,p_item_sequence=>480
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104784375731418317)
,p_name=>'P20_COT_ID'
,p_item_sequence=>490
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(533893252835330250)
,p_name=>'P20_ES_POLITICA_GOBIERNO'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_car_consultas.fn_valida_poltica_gobierno(pn_pol_id => :P20_POL_ID,',
'                                                         pn_emp_id => :f_emp_id);'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6445618570344560322)
,p_name=>'P20_VALOR_MARGEN_FACTURA'
,p_item_sequence=>752
,p_item_plug_id=>wwv_flow_imp.id(39319478380009582)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''C''||to_char(to_number(round(SUM(vod_valor_variable),2)),''999990.99'')),'' '','''') from ven_ordenes_det d, ven_var_ordenes_det v where d.ord_id = :P20_ORDEN_VENTA and v.ode_id = d.ode_id and v.var_id = 3136',
'',
'and d.ite_sku_id not in',
'(select c.ite_sku_id from v_inv_items_categoria c where c.cat_id in  ',
'               (  pq_constantes.fn_retorna_constante(d.emp_id,''cn_cat_id_gex''),',
'                  pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tre_id_gar_moto'')',
'               )',
')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'select replace((''C''||to_char(to_number(round(SUM(vod_valor_variable),2)),''999990.99'')),'' '','''') from ven_ordenes_det d, ven_var_ordenes_det v where d.ord_id = :P20_ORDEN_VENTA and v.ode_id = d.ode_id and v.var_id = 27'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7316852457398997484)
,p_name=>'P20_UTILIDAD_BRUTA'
,p_item_sequence=>872
,p_item_plug_id=>wwv_flow_imp.id(39319478380009582)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''C'' || to_char(to_number(round(((select SUM(v.vod_valor_variable)',
'                                                   from ven_ordenes_det     d,',
'                                                        ven_var_ordenes_det v',
'                                                  where d.ord_id =:P20_ORDEN_VENTA',
'                                                    and v.ode_id = d.ode_id',
'                                                    and v.var_id =',
'                                                        pq_constantes.fn_retorna_constante( :F_EMP_ID,',
'                                                                                           ''cn_var_id_tot_comp'')',
'                                                    and d.ite_sku_id not in',
'                                                        (select c.ite_sku_id',
'                                                           from v_inv_items_categoria c',
'                                                          where c.cat_id in',
'                                                                (pq_constantes.fn_retorna_constante(d.emp_id,',
'                                                                                                    ''cn_cat_id_gex''),',
'                                                                 pq_constantes.fn_retorna_constante( :F_EMP_ID,',
'                                                                                                    ''cn_tre_id_gar_moto''))))),',
'                                               4) ),',
'                               ''999990.99'')),',
'               '' '',',
'               '''')',
'  from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:24;color:BLUE"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7317314873992004925)
,p_name=>'P20_COMISION_FINAL'
,p_item_sequence=>882
,p_item_plug_id=>wwv_flow_imp.id(39319478380009582)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select replace((''V'' || to_char(to_number(round(((select SUM(v.vod_valor_variable)',
'                                                   from ven_ordenes_det     d,',
'                                                        ven_var_ordenes_det v',
'                                                  where d.ord_id =  :P20_ORDEN_VENTA',
'                                                    and v.ode_id = d.ode_id',
'                                                    and v.var_id =',
'                                                        pq_constantes.fn_retorna_constante( :F_EMP_ID,',
'                                                                                           ''cn_var_id_tot_comp'')',
'                                                    and d.ite_sku_id not in',
'                                                        (select c.ite_sku_id',
'                                                           from v_inv_items_categoria c',
'                                                          where c.cat_id in',
'                                                                (pq_constantes.fn_retorna_constante(d.emp_id,',
'                                                                                                    ''cn_cat_id_gex''),',
'                                                                 pq_constantes.fn_retorna_constante( :F_EMP_ID,',
'                                                                                                    ''cn_tre_id_gar_moto'')))) *0.035),',
'                                               4) ),',
'                               ''999990.99'')),',
'               '' '',',
'               '''')',
'  from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:24;color:BLUE"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(17383191851451095807)
,p_name=>'P20_PUE_ELECTRONICO'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(20132620681092049796)
,p_name=>'P20_POL_RESTRICCION'
,p_item_sequence=>772
,p_item_plug_id=>wwv_flow_imp.id(20132535965435043897)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(20132692372465053774)
,p_name=>'P20_ALERTA_RESTRICCION'
,p_item_sequence=>782
,p_item_plug_id=>wwv_flow_imp.id(20132535965435043897)
,p_prompt=>'   '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:24;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(20132809354997061865)
,p_name=>'P20_CLAVE_POL'
,p_item_sequence=>792
,p_item_plug_id=>wwv_flow_imp.id(20132535965435043897)
,p_prompt=>'Clave Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''VALIDA_CLAVE'');" '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(20132921368140071032)
,p_name=>'P20_TOTAL_FACTURA'
,p_item_sequence=>802
,p_item_plug_id=>wwv_flow_imp.id(20132535965435043897)
,p_prompt=>'Total Factura'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round(o.vor_valor_variable, 2)',
'  from asdm_e.ven_var_ordenes o',
' where ord_id = :P20_ORDEN_VENTA',
'   and var_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total_cred_entrada'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(26901172753006298421)
,p_name=>'P20_ACTA_ENTREGA_RECEPCION'
,p_item_sequence=>812
,p_item_plug_id=>wwv_flow_imp.id(39327982167009629)
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC2:TITULAR DEL SUBMINISTRO;S,ARRENDATARIO;N'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_attribute_04=>'VERTICAL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(30627605273681104317)
,p_name=>'P20_RESULTADO_ANALISIS'
,p_item_sequence=>822
,p_item_plug_id=>wwv_flow_imp.id(30627433376413093594)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(30627650867858106970)
,p_name=>'P20_AUX_CLAVE'
,p_item_sequence=>832
,p_item_plug_id=>wwv_flow_imp.id(30627433376413093594)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(30627841476904117960)
,p_name=>'P20_MUESTRA_BOTON'
,p_item_sequence=>842
,p_item_plug_id=>wwv_flow_imp.id(30627433376413093594)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(30627925966552122785)
,p_name=>'P20_VALOR_LAVADO'
,p_item_sequence=>852
,p_item_plug_id=>wwv_flow_imp.id(30627433376413093594)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_car_analisis_cliente.fn_retorna_valor_lavado(pn_emp_id => :F_emp_id,',
'                                                 pn_ord_id => :P20_ORDEN_VENTA,',
'                                                 pn_cli_id => :P20_CLI_ID);'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34612203003935567532)
,p_name=>'P20_ORD_ID_REFERIDOR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34612203058132567533)
,p_name=>'P20_CPR_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'New'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(101128809559738289827)
,p_name=>'P20_ORD_FECHA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142311785839158031050)
,p_name=>'P20_FORMA_CRE'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(165053578015200814727)
,p_name=>'P20_TSE_ID_MINOREO'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(165053578083829814728)
,p_name=>'P20_EMP_ID_MARCIMEX'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(165053578166524814729)
,p_name=>'P20_PAYBAHN_ACTIVO'
,p_item_sequence=>580
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(165053578327802814730)
,p_name=>'P20_EXISTE_CELULARES'
,p_item_sequence=>590
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(171514337981803543657)
,p_name=>'P20_VENTA_PAYJOY'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(39286580153009492)
,p_prompt=>'Venta Payjoy'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_cattributes_element=>'1'
,p_rowspan=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(199707358671977957940)
,p_name=>'P20_SHOW_FC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(199707358629682957939)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'HTML_UNSAFE'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(39334453193009637)
,p_computation_sequence=>10
,p_computation_item=>'P20_PLAZO_FACTURA'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  Ln_valor number(3) := 0;',
'begin',
'  if :P20_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_credito_propio'') then',
'       Ln_Valor := 12;',
'  else',
'      Ln_Valor := 0;',
'  end if;',
'   return  Ln_Valor;',
'end;'))
,p_compute_when=>':P20_TVE_ID is not null and :P20_COND_CAB != :P20_POL_ID||''-''||:P20_TVE_ID'
,p_compute_when_text=>'PLSQL'
,p_compute_when_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(39334270481009635)
,p_computation_sequence=>20
,p_computation_item=>'P0_DESPLIEGA_AUT'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    if :P20_ENTRADA is null then',
'        return ''N'';',
'    end if;',
'    if :P20_ENTRADA_MINIMA > nvl(:P20_ENTRADA,0) then',
'        return ''S'';',
'    end if;',
'end;'))
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(39334660787009637)
,p_computation_sequence=>30
,p_computation_item=>'P0_TRX_AUT'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_computation=>'86'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(39335253954009641)
,p_validation_name=>'VALIDA_PEKAS'
,p_validation_sequence=>10
,p_validation=>'nvl(:P20_DESCTO_PUNTOS,0) <= to_number(nvl(:P20_PEKAS,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'No puede exceder del limite de pekas'
,p_validation_condition=>'P20_DESCTO_PUNTOS'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(39334857120009638)
,p_validation_name=>'VALIDA ENTRADA'
,p_validation_sequence=>20
,p_validation=>'nvl(:P20_ENTRADA,0) > to_number(nvl(:P20_ENTRADA_MINIMA,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Entrada no puede ser menor a la minima, Solicite Autorizacion'
,p_validation_condition=>':P20_ENTRADA is not null and :P0_DESPLIEGA_AUT = ''S'' and :P0_AUTORIZACION is null'
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(35701867381797209)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(39335071178009641)
,p_validation_name=>'VALIDA_AUTORIZACION'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   Lv_Autorizacion  varchar2(100);',
'begin',
'  FOR reg_usu in 1..APEX_APPLICATION.G_F30.COUNT LOOP',
'    return pq_asdm_seguridad.fn_verifica_autorizacion(',
'                          to_number(APEX_APPLICATION.G_F30(reg_usu)),',
'                          :P0_TRX_AUT,',
'                          :P20_IDENTIFICACION,',
'                          :P20_ENTRADA,:P0_AUTORIZACION,',
'                          Lv_Autorizacion,:P20_ERROR);',
'  END LOOP;',
'end;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_BOOLEAN'
,p_error_message=>'Autorizacion no valida, solicitela nuevamente '
,p_validation_condition=>'P0_AUTORIZACION'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(94433472861585358)
,p_validation_name=>'P20_COM_NUM'
,p_validation_sequence=>40
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p20_com_num = :P20_com_numero then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>':p0_TTR_ID!= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(94420869950499329)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51052175288119021)
,p_validation_name=>'P20_EBS_ID'
,p_validation_sequence=>50
,p_validation=>'P20_EBS_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'DEBE SELECCIONAR UN IDENTIFICADOR(FACTURA DE MOTO)'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    ln_count_iden number := 0;',
'begin',
'    begin',
'        SELECT se.ebs_id',
'          INTO ln_count_iden',
'                  FROM inv_items_estado_bodega_stock st,',
'                       ven_ordenes_det               vd,',
'                       inv_items_estado_bodega_serie se,',
'                       inv_items                     i,',
'                       inv_comprobantes_inventario   cin,',
'                       inv_comprobantes_detalle      cd',
'                 WHERE vd.ord_id = cin.ord_id',
'                   AND cin.cin_id = cd.cin_id',
'                   AND cd.ieb_id = se.ieb_id',
'                   AND st.ite_sku_id = vd.ite_sku_id',
'                   AND vd.ord_id = :p20_orden_venta',
'                   AND i.ite_sku_id = st.ite_sku_id',
'                   AND se.ieb_id = st.ieb_id',
'                   AND se.ebs_estado_registro = 0',
'                   AND vd.bpr_id = cin.bpr_id',
'                   AND exists (SELECT ics.ics_valor_identificador',
'                          FROM mov_inv_despachos ide, mov_inv_comprobantes_serie ics',
'                         WHERE ide.ide_id = ics.ide_id',
'                           AND ide.ide_ord_id = :p20_orden_venta',
'                           AND ics.ics_valor_identificador = se.ebs_valor_identificador);',
'    exception',
'        when others then',
'            ln_count_iden :=0;',
'    end;',
'    ',
'    if :P20_ORD_TIPO IN (''VENMO'') AND :P20_VALIDA_IDENTIFICADOR = 0 and ln_count_iden = 0 then',
'        return true;',
'    else',
'        return false;',
'    end if;',
'end;'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'FUNCTION_BODY'
,p_associated_item=>wwv_flow_imp.id(50991257272337924)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(20133202566782091652)
,p_name=>'AD_SUBMIT_RES'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P20_POL_RESTRICCION'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(20133202865043091673)
,p_event_id=>wwv_flow_imp.id(20133202566782091652)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P20_POL_RESTRICCION'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(165053579371120814741)
,p_name=>'da_dialog_closed_pistoleo'
,p_event_sequence=>20
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'window'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460250488656868464)
,p_event_id=>wwv_flow_imp.id(165053579371120814741)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP := ''N'';'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(165053579521594814742)
,p_event_id=>wwv_flow_imp.id(165053579371120814741)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(165053578411140814731)
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(20133020752453078035)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_politica_restriccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'',
'',
'  :P20_POL_RESTRICCION := pq_car_general.fn_valida_pol_restrinccion(pn_pol_id => :P20_POL_ID,',
'                                                                    pn_emp_id => :f_emp_id,',
'                                                                    pn_uge_id => :f_uge_id);',
'',
'',
'  if :P20_POL_RESTRICCION > 0 then',
'  ',
'    :P20_ALERTA_RESTRICCION := ''La Politica de esta Venta es Restringida, por favor comuniquese con el Dep. Cartera '';',
'  ',
'  end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'orden_venta'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>20100767601183313109
,p_process_comment=>'request = orden_venta'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39335362340009641)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_CABECERA_ORD'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'',
'     ',
':f_per_id := :p20_identificacion;',
'   ',
'pq_ven_ordenes.pr_carga_ordenventa_cabecera(:f_emp_id,',
'                                                 :p20_orden_venta,',
'                                                 :p20_identificacion,',
'                                                 :p20_pol_id,',
'                                                 :p20_tve_id,',
'                                                 :p20_plazo_factura,',
'                                                 :p20_total_factura,',
'                                                 :p20_poce_nacional,',
'                                                 :p20_entrada_minima,',
'                                                 :p0_error);',
'  ',
'          pq_ven_listas2.pr_datos_clientes ( :F_UGE_ID,',
'                                            :F_EMP_ID,',
'                                            :P20_identificacion,',
'                                            :P20_nombre,',
'                                            :P20_cli_id,',
'                                            :P20_correo,',
'                                            :P20_direccion,',
'                                            :P20_tipo_direccion,',
'                                            :P20_telefono,',
'                                            :P20_tipo_telefono,',
'                                            :P20_cliente_existe,',
'                                            :P20_DIR_ID,',
'                                            :P20_CLI_ID_REFERIDO,',
'                                            :P20_NOMBRE_CLI_REFERIDO,',
'                                             NULL,  ',
'                                            :p0_error);',
'             ',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>7082211070244715
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(73665267032634213)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_FAC_MAYOREO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_age_id_agente asdm_agentes.age_id%TYPE;',
'  lv_ode_estado    inv_ordenes_despacho.ode_estado%TYPE;',
' ',
'BEGIN',
'--if :P20_ORDEN_VENTA <> 1786608 then',
'-- se inactiva hasta realizar requerimiento exclusivo para minoreo',
'/*',
'',
'pq_ven_validaciones.pr_valida_identif_pistoleo(pn_ord_id => :P20_ORDEN_VENTA,',
'                                                 pn_emp_id => :f_emp_id);',
'                                                 ',
'                                                 ',
'*/                                                 ',
'--end if;',
'',
'   if (:P20_ORD_TIPO != pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig'') and :P20_ORD_TIPO != pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig_moto'')) then',
'     if pq_ven_ordenes.fn_es_orden_venta_servicios(pn_ord_id => :P20_ORDEN_VENTA,',
'                                                  pn_emp_id => :f_emp_id) = ''N'' then',
'',
'      lv_ode_estado := pq_inv_orden_despacho.fn_estado_orden_despacho(:f_emp_id,',
'                                                                      :p20_tve_id,',
'                                                                      :p0_ttr_id,',
'                                                                      :p0_rol,',
'                                                                      :p20_ode_id,',
'                                                                      :f_seg_id);',
'',
'',
'   pq_inv_movimientos_dml.pr_upd_inv_ord_despacho_est(pn_ode_id     => :p20_ode_id,',
'                                                         pv_ode_estado => lv_ode_estado,',
'                                                         pv_error      => :p0_error);',
'  end if;',
'end if;',
'    ',
'      asdm_p.pq_ven_comprobantes.pr_graba_comprobante_orden(pn_emp_id                => :f_emp_id,',
'                                                        pn_uge_id                => :f_uge_id,',
'                                                        pn_uge_id_gasto          => :f_uge_id_gasto,',
'                                                        pn_usu_id                => :f_user_id,',
'                                                        pn_ttr_id                => :p0_ttr_id,',
'                                                        pn_ttr_id_cxc            => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                       ''cn_ttr_id_debito_car_facturacion''),',
'                                                        pn_com_id                => :p20_com_id,',
'                                                        pn_trx_id                => :p20_trx_id,',
'                                                        pn_pue_id                => :p0_pue_id,',
'                                                        pn_dir_id_enviar_factura => NULL,',
'                                                        pd_com_fecha             => SYSDATE,',
'                                                        pn_com_numero            => :p0_fol_sec_actual,',
'                                                        pv_uge_num_sri           => :p0_uge_num_est_sri,',
'                                                        pv_pue_num_sri           => :p0_pue_num_sri,',
'                                                        pn_age_id_agente         => NULL,',
'                                                        pn_age_id_agencia        => :f_age_id_agencia,',
'                                                        pn_ord_id                => :p20_orden_venta,',
'                                                        pv_com_observacion       => :p20_com_observaciones,',
'                                                        pn_com_saldo             => NULL,',
'                                                        pn_com_nro_impresion     => 0,',
'                                                        pn_tse_id                => :f_seg_id,',
'                                                        pv_app_user              => :app_user,',
'                                                        pv_session               => :session,',
'                                                        pv_token                 => :f_token,',
'                                                        pn_user_id               => :f_user_id,',
'                                                        pn_rol                   => :p0_rol,',
'                                                        pv_rol_desc              => :p0_rol_desc,',
'                                                        pn_tree_rot              => :p0_tree_root,',
'                                                        pn_opcion                => :f_opcion_id,',
'                                                        pv_parametro             => :f_parametro,',
'                                                        pv_popup                 => :f_popup,',
'                                                        pv_empresa               => :f_empresa,',
'                                                        pv_emp_logo              => :f_emp_logo,',
'                                                        pv_emp_fondo             => :f_emp_fondo,',
'                                                        pv_ugestion              => :f_ugestion,',
'                                                        pn_mnc_id                => NULL,',
'                                                        pv_ord_tipo              => :p20_ord_tipo,',
'                                                        pn_nro_folio             => :p0_nro_folio,',
'                                                        pv_forma_credito         => :p20_forma_cre,',
'                                                        pv_error                 => :p0_error);',
'    ',
'--hsolano 26/09/2018 se grega proceso para grabar identificadores',
'pq_mov_inv_despachos.pr_graba_iden_ven_comprobantes(pn_com_id => :p20_com_id,',
'                                                      pn_emp_id => :f_emp_id,',
'                                                      pv_error => :p0_error);',
'    ',
'--hsolano 26/09/2018 se comenta proceso ya que es solo en el caso de motos',
'   --***---',
'IF :P20_ORD_TIPO  in (''FCO'',''FCOMO'') THEN',
'pq_ven_comunes.pr_graba_identificadores_comp(pn_ode_id => :p20_ode_id,',
'                                               pn_ebs_id => :p20_ebs_id,',
'                                               pn_age_id => :f_age_id_agencia,',
'                                               pn_emp_id => :f_emp_id,',
'                                               pn_ord_id =>:P20_ORDEN_VENTA,',
'                                               pv_error  => :p0_error);',
' END IF;',
'---***--',
'    ',
'   ',
'  ',
'    ln_age_id_agente := pq_car_credito_interfaz.fn_retorna_agente_conectado(:f_user_id,',
'                                                                            :f_emp_id);',
'    ',
'    ',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(73495573642746202)
,p_process_when=>'facturar_may'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>41412115762869287
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(98309182342642712)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_valor_entrada ven_var_ordenes.vor_valor_variable%TYPE;',
'  lv_error varchar2(1000) := null;',
'  ln_induccion number := pq_car_consultas.fn_valida_poltica_gobierno(pn_pol_id => :P20_POL_ID,',
'                                                         pn_emp_id => :f_emp_id);',
'begin',
'  --obtengo la entidad destino de la orden de venta',
'  lv_Error := ''NO SE ENCUENTRA LA ENTIDAD DE LA ORDEN'' || :p20_orden_venta;',
'  select ord.ede_id',
'    into :P20_PLAN',
'    from ven_ordenes ord',
'   where ord_id = :p20_orden_venta',
'     and emp_id = :f_emp_id;',
'',
'',
'  --obtengo el valor de la entrada ',
'  --utilizo este valor para cargar automaticamente la entrada al momento de intentar facturar',
'  lv_Error := ''NO SE ENCUENTRA EL VALOR DE LA ENTRADA DE LA ORDEN'' || :p20_orden_venta;',
'  SELECT nvl(vor.vor_valor_variable, 0)',
'    into ln_valor_entrada',
'    FROM ppr_variables_ttransaccion vtt,',
'         ven_var_ordenes            vor,',
'         asdm_empresas              emp',
'   WHERE vtt.ttr_id =',
'         pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_orden_venta'')',
'     AND vor.var_id = vtt.var_id',
'     AND emp.emp_id = vtt.emp_id',
'     AND emp.emp_id = vor.emp_id',
'     AND emp.emp_id = :f_emp_id',
'     and vor.var_id =',
'         pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_entrada'')',
'     AND vor.ord_id = :p20_orden_venta;',
'  if ln_valor_entrada > 0 then',
'    :P20_ENTRADA := ln_valor_entrada;',
'  else',
'    :P20_ENTRADA := null;',
'  end if;',
'if nvl(ln_induccion,0) = 0 then',
'  pq_ven_listas_caja.pr_datos_tarjeta_credito(pn_ede_id  => :P20_PLAN,',
'                                              pn_emp_id  => :f_emp_id,',
'                                              pn_banco   => :P20_BANCOS,',
'                                              pn_tarjeta => :P20_TARJETA,',
'                                              pn_tipo    => :P20_TIPO,',
'                                              pn_forma => :p20_forma,',
'                                              pn_comision=> :P20_COMISION,',
'                                              pv_error   => :p0_error);',
'end if;',
'exception',
'  when no_data_found then',
'    :p0_error := LV_ERROR ||'' pr_carga_datos_tarjeta ''||',
'                 SQLERRM;',
'  ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P20_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito'')'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>66056031072877786
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39335567725009642)
,p_process_sequence=>95
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_DETALLE_ORDEN'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'lv_sentencia varchar2(20000);',
'lv_nombre_coleccion VARCHAR(100):= pq_constantes.fn_retorna_constante(null,''cv_col_ord_det'');',
'',
'BEGIN',
' lv_sentencia := pq_ven_ordenes.fn_devuelve_var_orden_det',
'                              (:P20_ORDEN_VENTA,',
'                                :F_EMP_ID);',
'',
'IF apex_collection.collection_exists(p_collection_name => lv_nombre_coleccion) THEN',
'      apex_collection.delete_collection(p_collection_name => lv_nombre_coleccion);',
'    END IF;',
'',
' apex_collection.create_collection_from_query_b',
'            (p_collection_name => lv_nombre_coleccion,',
'             p_query           => lv_sentencia );',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'orden_venta'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>7082416455244716
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15330779912515638175)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Valida_pistoleo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_error VARCHAR2(1000); --R3428-04 mfidrovo',
'begin',
'if :P20_ORDEN_VENTA NOT IN(1908576,1911347,1918659,1919090) then',
' pq_ven_validaciones.pr_valida_identif_pistoleo(pn_ord_id => :P20_ORDEN_VENTA,',
'                                                 pn_emp_id => :f_emp_id,',
'                                                 pn_tse_id => :f_seg_id);',
'end if;                                  ',
'',
'  pq_inv_paybahn.pr_cargar_coleccion_celulares(pn_ord_id => :p20_orden_venta,',
'                                               pn_emp_id => :f_emp_id,',
'                                               pv_error  => lv_error);',
'',
'  --R3428-04 mfidrovo 11/11/2021 inicio',
'  IF :f_seg_id = :p20_tse_id_minoreo AND :f_emp_id = :p20_emp_id_marcimex AND',
'     :p20_paybahn_activo = 0 AND :p20_existe_celulares > 0 THEN',
'  ',
'    pq_inv_paybahn.pr_valida_item_celulares(pn_ord_id => :p20_orden_venta,',
'                                            pn_emp_id => :f_emp_id,',
'                                            pv_error  => lv_error);',
'  ',
'  END IF;',
'  --R3428-04 mfidrovo 11/11/2021 fin',
'',
'',
'end;    '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(39326260927009627)
,p_internal_uid=>15298526761245873249
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(46713668787086164)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_generar_promociones'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'/*',
'  pq_ven_promociones_comunes.pr_genera_promos_ordenes(:p20_orden_venta,',
'                                                      :f_emp_id);*/',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>14460517517321238
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39348182109090646)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_borra_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                         ''cv_coleccion_mov_caja''));',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(39326260927009627)
,p_internal_uid=>7095030839325720
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(20133136981196086263)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Pr_valida_clave_politica'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_clave number;',
'  ln_valor number;',
'',
'begin',
'',
'  pq_car_general.pr_clave_venta_motos(pn_cli_id     => :P20_CLI_ID,',
'                                      pn_per_id     => null,',
'                                      pn_emp_id     => :f_emp_id,',
'                                      pn_valor      => :P20_TOTAL_FACTURA,',
'                                      pn_resultado  => ln_clave,',
'                                      pn_uge_id     => :f_uge_id,',
'                                      pn_usu_id     => :f_user_id,',
'                                      pn_tipo_clave => 9,',
'                                      pn_tipo_uso   => 0,',
'                                      pv_error      => :p0_error);',
'',
'  if ln_clave = :P20_CLAVE_POL then',
'  ',
'    :P20_POL_RESTRICCION    := 0;',
'    :P20_ALERTA_RESTRICCION := ''Clave Correcta'';',
'  ',
'  else',
'  ',
'    :P20_POL_RESTRICCION    := 1;',
'    :P20_ALERTA_RESTRICCION := '' Clave Incorrecta - La Politica de esta Venta es Restringida, por favor comuniquese con el Dep. Cartera '';',
'  ',
'  end if;',
'',
'exception',
'  when others then',
'    raise_application_error(-20000, sqlerrm);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'VALIDA_CLAVE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>20100883829926321337
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(45812818857270267)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_identificadores'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'--R2394-10 mfidrovo 30-09-2019',
'IF :P20_VALIDA_IDENTIFICADOR >0 THEN',
'pq_mov_inv_despachos.pr_valida_iden_consig(pn_ord_id => :P20_ORDEN_VENTA,',
'                                             pv_error => :p0_error);',
'END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(39326260927009627)
,p_process_when=>'asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,''cn_identificadores_factura'')=''S'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'SQL'
,p_internal_uid=>13559667587505341
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(45813159227270271)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_iden_consignacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_mov_inv_despachos.pr_valida_iden_consig(pn_ord_id => :P20_ORDEN_VENTA,',
'                                             pv_error => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(73495573642746202)
,p_internal_uid=>13560007957505345
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P20_ORD_FECHA >= TO_DATE(''05-11-2019'',''DD-MM-YYYY'') and ',
':P20_ORD_FECHA < to_date(''13''-''11-2019 20:27:30'', ''DD-MM-YYYY HH24:MI:SS'')'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(45811614774270255)
,p_process_sequence=>30
,p_process_point=>'BEFORE_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_crea_col_ident_consignacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  :P20_TEI_ID_SITUACION := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_tei_id_situacion'');',
'  :P20_CEI_ID_CONSIG_EN := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_cei_consig_en'');',
'  :P20_TEI_ID_CALIDAD   := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_tei_id_calidad'');',
'  ',
'  --R2394-10 mfidrovo 20/09/2019 inicio                                                   ',
'  pq_mov_inv_despachos.pr_crea_col_ident_orden(pn_ord_id           => :P20_ORDEN_VENTA,',
'                                               pn_tei_id_situacion => :P20_TEI_ID_SITUACION,',
'                                               pn_tei_id_calidad   => :P20_TEI_ID_CALIDAD,',
'                                               pn_tse_id           => :F_SEG_iD,',
'                                               pv_ord_tipo         => :P20_ORD_TIPO,',
'                                               pn_emp_id           => :F_EMP_ID,',
'                                               pv_error            => :p0_error);',
' --R2394-10 mfidrovo 20/09/2019 fin                                                   ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''orden_venta'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>13558463504505329
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1089945346635727633)
,p_process_sequence=>1
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_ecredit_data'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'  Autor:       FPALOMEQUE/RALVARADO',
'  Fecha:       16/03/2023',
'  Descripcion: Retorna 1 si la validacion fue correcta, 0 si no fue satisfactorio',
'*/',
'DECLARE',
'  ln_val_biom NUMBER;',
'  ln_val_sms  NUMBER;',
'  ln_val_fsd  NUMBER;',
'BEGIN',
'',
'  BEGIN',
'    ln_val_biom := pq_car_ecredit_biometria.fn_val_biometrica_fac(pn_ord_id => :p20_orden_venta,',
'                                                              pn_emp_id => :f_emp_id,',
'                                                              pn_age_id => :f_age_id_agencia);',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      ln_val_biom := 0;',
'  END;',
'',
'  IF ln_val_biom = 0 THEN',
'    raise_application_error(-20000,',
'                            ''El cliente tiene pendiente Enrolamiento/Verificacion Biometrica!!: ORD_ID: '' ||',
'                            :p20_orden_venta);',
'  END IF;',
'',
'  -- Validacion para envios codigos verificacion SMS',
'  BEGIN',
'    ln_val_sms := pq_car_ecredit_biometria.fn_val_sms_fac(pn_ord_id => :p20_orden_venta,',
'                                                      pn_emp_id => :f_emp_id,',
'                                                      pn_age_id => :f_age_id_agencia);',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      ln_val_sms := 0;',
'  END;',
'',
'  IF ln_val_sms = 0 THEN',
'    raise_application_error(-20000,',
'                            ''El cliente tiene pendiente la Validacion por SMS: ORD_ID: '' ||',
'                            :p20_orden_venta);',
'  END IF;',
'',
'  -- Validacion de documentos firmados',
'  BEGIN',
'    ln_val_fsd := pq_car_ecredit_biometria.fn_val_archivos_firmados(pn_ord_id => :p20_orden_venta);',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      ln_val_fsd := 0;',
'  END;',
'',
'  IF ln_val_fsd = 0 THEN',
'    raise_application_error(-20000,',
'                            ''No se han encontrado archivos firmados para Security Data, por favor validar, ord_id: '' ||',
'                            :p20_orden_venta);',
'  END IF;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>1057692195365962707
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15357070869975221070)
,p_process_sequence=>2
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_bloque_venta_motos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  DECLARE',
'  ln_cont NUMBER;',
'BEGIN',
'      SELECT COUNT(1)',
'        INTO ln_cont',
'        FROM ven_ordenes o',
'       WHERE o.ord_tipo IN (SELECT tipo FROM tb_temp_tord)',
'         AND o.ord_id = :P20_ORDEN_VENTA',
'         AND o.emp_id = :F_EMP_ID;',
'      IF ln_cont > 0 THEN',
'        raise_application_error(-20000,',
unistr('                                ''Temporalmente no se puede facturar MOTOS, en el transcurso de la ma\00F1ana regularizaremos el sistema'');'),
'      END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>15324817718705456144
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
' DECLARE',
'  lv_tipo_ord VARCHAR2(10) ;',
'  BEGIN',
'  SELECT o.ord_tipo INTO lv_tipo_ord ',
'  FROM ven_ordenes o WHERE o.ord_id = :P20_ORDEN_VENTA;',
'  ',
'  IF lv_tipo_ord = ''VENMO'' THEN',
unistr('    raise_application_error(-20001, ''La ANT ya NO tiene el Servicio de matriculaci\00F3n habilitado, por lo tanto las motos se podr\00E1n facturar a partir de 1 de Enero de 2018'');'),
'  END IF;',
'  END;',
'  '))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(16314961177919209905)
,p_process_sequence=>3
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ValidaFechaOrden'
,p_process_sql_clob=>'  pq_ven_validaciones.pr_valida_fecha_orden(pn_ord_id => :P20_ORDEN_VENTA);'
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>16282708026649444979
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(152177475654234695046)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ValidaSeguroDesgravamen'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare ',
'ln_valida_seguro number;',
'BEGIN',
'    ln_valida_seguro := pq_ven_validaciones.fn_valida_seguro_desgravamen(pn_ord_id => :P20_ORDEN_VENTA);',
'    if(ln_valida_seguro>0)then',
'        raise_application_error(-20000, ''DEBE FACTURAR PRIMERO EL SEGURO DE DESGRAVAMEN''||:P20_ORDEN_VENTA);',
'    END IF;',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>152145222502964930120
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(142311785940814031051)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_forma_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'    Modificacion: FPALOMEQUE',
'    Descripcion:  Carga campo Forma de Credito del ven_ordenes',
'    Fecha:        07/04/2021',
'    Historia:     R3163-09',
'*/',
'BEGIN',
'  SELECT o.forma_credito',
'    INTO :p20_forma_cre',
'    FROM ven_ordenes o',
'   WHERE o.ord_id = :p20_orden_venta',
'     AND rownum = 1;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    :p20_forma_cre := ''M'';',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>142279532789544266125
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(30626235459220022443)
,p_process_sequence=>31
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_analisis_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  ',
'/*',
'raise_application_Error(-20000, ''emp_id: '' || :f_emp_id || '' CLI_ID '' || :P20_CLI_ID ||  ''co: '' || pq_constantes.fn_retorna_constante(0,',
'                                                                                                   ''cn_ttr_id_analisis_cliente'') || ''tve'' || :P20_TVE_ID ||',
'                                ''seg: '' ||  :F_SEG_ID);*/',
'    pq_car_analisis_cliente.pr_analizar_cliente(pn_emp_id    => :f_emp_id,',
'                                                pn_cli_id    => :P20_CLI_ID,',
'                                                pb_resultado => :P20_RESULTADO_ANALISIS,',
'                                                pn_ttr_id    => pq_constantes.fn_retorna_constante(0,',
'                                                                                                   ''cn_ttr_id_analisis_cliente''),',
'                                                pn_tve_id    => :P20_TVE_ID,',
'                                                pn_tse_id    => :F_SEG_ID,',
'                                                pn_ord_id    => :P20_ORDEN_VENTA,',
'                                                pv_error     => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>30593982307950257517
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39335759663009642)
,p_process_sequence=>41
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%type;',
'ln_tipo_fac asdm_tipos_grupo_transaccion.tgr_id%type;',
'BEGIN',
'',
'  ln_tgr_id  := pq_ven_listas_caja.fn_tipo_grupo_transaccion_seg(:f_emp_id,:f_seg_id);',
'  :P0_TTR_ID := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id); ',
'',
'ln_tipo_fac:= pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_comprobante_may'');',
'--if  :P0_TTR_ID <> ln_tipo_fac then',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       LN_TGR_ID,',
'      :P0_TTR_DESCRIPCION, ',
'      :P0_PERIODO,',
'      :P0_DATFOLIO,',
'      :P0_FOL_SEC_ACTUAL,',
'      :P0_PUE_NUM_SRI  ,',
'      :P0_UGE_NUM_EST_SRI ,',
'      :P0_PUE_ID,',
'      :P0_TTR_ID,',
'      :P0_NRO_FOLIO,',
'      :P0_ERROR);',
'--end if;',
'',
':P20_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P0_PUE_ID);',
'/*raise_application_error(-20000,'':P20_PUE_ELECTRONICO: ''||:P20_PUE_ELECTRONICO ||'':P51TTR_ID: ''||:F_PCA_ID);*/',
'IF :P20_PUE_ELECTRONICO= ''E'' THEN ',
':P20_COM_NUM:= :P0_FOL_SEC_ACTUAL;',
'END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>7082608393244716
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(46743765754416501)
,p_process_sequence=>51
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_transaccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    trx_id',
'INTO      :p20_trx_id',
'FROM      ven_ordenes',
'WHERE     ord_id = :p20_orden_venta;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>14490614484651575
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(89544774558958227)
,p_process_sequence=>61
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Estado_Ordenes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare ln_uge_cupo  NUMBER;',
'--h1 hsolano 10/10/2018 proyecto de identificadores',
'ln_teo_id_ident number :=asdm_p.pq_constantes.fn_retorna_constante(null,',
'                                                                     ''cn_teo_id_ident_ingresados'');',
'  ln_count_teo_id_iden number;',
'  ln_iden_factura number;',
'  ln_desp_bod_agen number;',
'  ln_pol_id_marcimex ppr_politicas.pol_id%TYPE := 471; --politca amrcimex para regular faltante de invetario',
'--h1',
'ln_uge_cupo_nombre VARCHAR2(3000);',
'--',
'lv_identificador_factura VARCHAR2(1) := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,''cn_identificadores_factura''); --R2394-08 mfidrovo 20/09/2019',
'',
'begin',
'',
'select teo_id INTO :p20_teo_id  from ven_ordenes where ord_id = :P20_ORDEN_VENTA;',
'',
'if :p20_tve_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_credito_propio'')  then',
'  if :p20_teo_id =  pq_constantes.fn_retorna_constante(NULL, ''cn_teo_id_aprob_financiera'') then',
'    :p20_error_factura := null;',
'  else',
'    :p20_error_factura := ''ERROR FALTA LA APROBACION FINANCIERA'';',
'  end if;',
'else',
'  ',
'if :p20_tve_id != pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_regalos'') then',
'',
'  if :p20_teo_id =  pq_constantes.fn_retorna_constante(NULL, ''cn_teo_id_aprob_comercial'') then',
'    :p20_error_factura := null;',
'  else',
'    :p20_error_factura := ''ERROR FALTA LA APROBACION COMERCIAL'';',
'  end if;',
'end if;',
'end if;',
'IF :F_SEG_ID = 2 and :p20_tve_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_credito_propio'') THEN',
'--:p20_cobrador := pq_car_cobranza.fn_retorna_cobrador(:f_emp_id, :p20_cli_id, :f_uge_id);',
unistr('/*Valida que la unidad de gesti\00F3n donde se aprob\00F3 el \00FAtlimo cupo tenga cobrador*/'),
'--MBARIAS 11/04/2019',
'begin',
'',
'   ',
'   ln_uge_cupo:=pq_car_manejo_cupos.fn_get_uge_aprobacion(:p20_cli_id,:f_emp_id);',
'   if :f_uge_id = 1601 then ',
'   ln_uge_cupo:=:f_uge_id;',
'   end if;',
'   Select uge.uge_nombre ',
'   into ln_uge_cupo_nombre',
'  from asdm_unidades_gestion uge ',
' where uge.uge_id = ln_uge_cupo',
'   and uge.uge_estado_registro = 0;   ',
'      ',
'   ',
'   ',
'  :p20_cobrador := pq_car_cobranza.fn_retorna_cobrador(:f_emp_id, :p20_cli_id, ln_uge_cupo);',
'',
'  ',
'  if :p20_cobrador is null AND :f_uge_id <> 1601  then',
'    :p20_error_factura := ''DEBE ASIGNAR UN COBRADOR EN LA AGENCIA ''||ln_uge_cupo_nombre||'' AL CLIENTE PARA PODER CONTINUAR'';',
'  end if;',
'  ',
'  EXCEPTION',
'   WHEN no_data_found THEN',
'         :p20_error_factura := ''DEBE ASIGNAR UN COBRADOR EN LA AGENCIA ''||ln_uge_cupo_nombre||'' AL CLIENTE PARA PODER CONTINUAR **'';',
'  end;',
'if :p20_cobrador is null  AND :f_uge_id <> 1601  then',
':p20_error_factura := ''DEBE ASIGNAR UN COBRADOR AL CLIENTE PARA PODER CONTINUAR ***'';',
' :p20_error_factura := ''DEBE ASIGNAR UN COBRADOR EN LA AGENCIA ''||ln_uge_cupo_nombre||'' AL CLIENTE PARA PODER CONTINUAR ***/ '';',
'end if;',
'END IF;',
'',
'--h1 hsolano 10/10/2018 proyecto de identificadores',
'/*Se agrega validacion que exista el estado ingresdo identificadores en la orden',
'       este estado se agrega directamente cuando los items de la orden no requieren identificadores',
unistr('       y se agrega al pistolear en el picking cuando los identificadores requeridos est\00E1n completos*/'),
'       ',
'       if :p20_error_factura is null and :F_SEG_ID = 2 and :p20_pol_id <> ln_pol_id_marcimex then',
'         select count(i.ite_sku_id)',
'           into ln_iden_factura',
'           from ven_ordenes_det od, inv_items i',
'          where od.ord_id = :p20_orden_venta',
'            and i.ite_sku_id = od.ite_sku_id',
'            and i.ite_fac_lleva_identificador = ''S'';',
'            ',
'          select count(bo.bag_id)',
'            into ln_desp_bod_agen',
'            from inv_ordenes_despacho od,',
'                 asdm_bodegas_agencias bo',
'           where bo.bpr_id = od.bpr_id_bodega_despacho',
'             and bo.bag_de_agencia = ''S''',
'             and bo.age_id = nvl(:P20_AGE_ID_AGENCIA,(SELECT a.age_id FROM asdm_agencias a WHERE a.uge_id = :f_uge_id))',
'             and od.ord_id = :p20_orden_venta;',
'         ',
'         if ln_iden_factura > 0 or ln_desp_bod_agen > 0 then',
'',
'             begin',
'               select count(eo.teo_id)',
'                 into ln_count_teo_id_iden',
'                 from ven_estados_ordenes eo',
'                where eo.ord_id = :p20_orden_venta',
'                  and eo.teo_id = ln_teo_id_ident;',
'             exception when others then',
'               ln_count_teo_id_iden := 0;',
'             end;',
'',
'             --R2394-10 mfidrovo 20/09/2019 inicio             ',
'             if lv_identificador_factura = ''N'' then',
'                 if ln_count_teo_id_iden = 0 then',
'                   :p20_error_factura := ''ERROR FALTA INGRESAR IDENTIFICADORES'';',
'                 end if;',
'             end if;',
'             --R2394-10 mfidrovo 20/09/2019 fin',
'         end if;',
'       end if;',
'--h1',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>57291623289193301
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(533899663311347476)
,p_process_sequence=>71
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_forma_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'  if :p20_tve_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tve_id_tarjeta_credito'') and',
'     :P20_ES_POLITICA_GOBIERNO = 0 then',
'  ',
'    :P20_FORMA_PAGO := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                          ''cn_tfp_id_tarjeta_credito'');',
'  ',
'  elsif :p20_tve_id =',
'        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tve_id_tarjeta_credito'') and',
'        :P20_ES_POLITICA_GOBIERNO = 1 then',
'  ',
'',
'',
'    :P20_FORMA_PAGO := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                          ''cn_tfp_id_plan_gobierno'');',
'  ',
'  end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>501646512041582550
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(21025123056849581948)
,p_process_sequence=>81
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_bloquea_comprobantes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pr_bloquea_comprobantes(pn_age_id_agencia => :F_AGE_ID_AGENCIA,',
'                          pv_nro_identificacion => :P20_IDENTIFICACION,',
'                          pn_cli_id => null);'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>20992869905579817022
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(30626634569871044423)
,p_process_sequence=>91
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_documentos_lavado'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  pq_car_imprimir.pr_imprimir_lavado_activos(pn_ord_id => :P20_ORDEN_VENTA,',
'                                             pn_emp_id => :f_emp_id,',
'                                             pn_uge_id => :F_uge_id,',
'                                             pn_age_id => :P20_AGE_ID_AGENTE,',
'                                             pv_error  => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprime_lavado'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>30594381418601279497
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(140178452066738418576)
,p_process_sequence=>101
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'      Creacion: 11/11/2021',
'      Autor: mfidrovo',
'      Descripcion:  carga de variables de paybahn  ',
'      Historia: R3428-04',
'*/',
'DECLARE',
'  ln_cat_id_celulares NUMBER;',
'  lv_error            VARCHAR2(1000);',
'  lv_coleccion        VARCHAR2(100) := ''COLL_ITEMS_CELULARES'';',
'',
'BEGIN',
'  :p20_tse_id_minoreo := pq_constantes.fn_retorna_constante(0,',
'                                                            ''cn_tse_id_minoreo'');',
'',
'  :p20_emp_id_marcimex := pq_constantes.fn_retorna_constante(0,',
'                                                             ''cn_emp_id_marcimex'');',
'',
'  :p20_paybahn_activo := pq_constantes.fn_retorna_constante(0,',
'                                                            ''cn_paybahn_activo'');',
'',
'  pq_inv_paybahn.pr_cargar_coleccion_celulares(pn_ord_id => :p20_orden_venta,',
'                                               pn_emp_id => :f_emp_id,',
'                                               pv_error  => lv_error);',
'',
'  BEGIN',
'    ln_cat_id_celulares := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_cat_id_celulares'');',
'  ',
'    SELECT COUNT(*)',
'      INTO :p20_existe_celulares',
'      FROM ven_ordenes          ord,',
'           ven_ordenes_det      ode,',
'           inv_paybahn_items pit          ',
'     WHERE ord.ord_id = ode.ord_id       ',
'        AND ode.ite_sku_id = pit.ite_sku_id',
'       AND ord.ord_id = :p20_orden_venta;',
'  ',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'    ',
'  END;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>140146198915468653650
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(199707358758204957941)
,p_process_sequence=>111
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_load_text_forma_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'  Creacion:    13/07/2023',
'  Autor:       FPALOMEQUE',
'  Descripcion: Carga el valor de forma de credito en region a mostrar',
'  Historia:    CTD-106',
'*/',
'DECLARE',
'  lv_forma_credito VARCHAR2(100);',
'BEGIN',
'  SELECT o.forma_credito',
'    INTO lv_forma_credito',
'    FROM ven_ordenes o',
'   WHERE o.ord_id = :p20_orden_venta;',
'',
'  -- Indica mensaje de forma de credito',
'  SELECT ''<div style="position:relative;float: left;top: 50%;left: 50%;transform: translate(-50%, -20%);"><p><span style="font-size:20;color:black;font-weight:bold">Forma de Credito: '' || CASE',
'           WHEN lv_forma_credito = ''P'' THEN',
'            ''<span style="font-size:20;color:#a99f04;font-weight:bold">"PENDIENTE"</span>''',
'           WHEN lv_forma_credito = ''I'' THEN',
'            ''<span style="font-size:20;color:#c26809;font-weight:bold">"INCONSISTENTE"</span>,''',
'           WHEN lv_forma_credito = ''M'' THEN',
'            ''<span style="font-size:20;color:#0b8e88;font-weight:bold">"MANUAL"</span>''',
'           WHEN lv_forma_credito = ''E'' THEN',
'            ''<span style="font-size:20;color:#0b8034;font-weight:bold">"ELECTRONICA"</span>''',
'           ELSE',
'            ''<span style="font-size:20;color:#9d1010;font-weight:bold">"SIN FORMA DE CREDITO"</span>''',
'         END || ''</span></p></div>''',
'    INTO :p20_show_fc',
'    FROM dual;',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    NULL;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>199675105606935193015
);
wwv_flow_imp.component_end;
end;
/
