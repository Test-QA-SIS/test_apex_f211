prompt --application/pages/page_00024
begin
--   Manifest
--     PAGE: 00024
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>24
,p_name=>'Formas de Pago'
,p_alias=>'FORMAS_DE_PAGO'
,p_step_title=>'Formas de Pago'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(251839055597435350)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839279600435351)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Editar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:24:&SESSION.::&DEBUG.::P24_SEQ_ID,P24_MCD_VALOR_MOVIMIENTO,P24_TFP_ID,P24_EDE_ID,P24_CRE_TITULAR_CUENTA,P24_CRE_NRO_CHEQUE,P24_CRE_NRO_CUENTA,P24_CRE_NRO_PIN:#COL02#,#COL10#,#COL08#,#COL03#,#COL16#,#COL17#,#COL18#,#COL19#'
,p_column_linktext=>'Editar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839379945435351)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839469094435351)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>6
,p_column_heading=>'ede_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839580154435351)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>7
,p_column_heading=>'Entidad Financiera'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839675771435351)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>12
,p_column_heading=>'ptc'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839754098435351)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>13
,p_column_heading=>'ncr_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839876206435351)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>20
,p_column_heading=>'cre_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251839957508435351)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>3
,p_column_heading=>'tfp_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840059989435351)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>4
,p_column_heading=>'Forma Pago'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840160773435351)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>5
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840266154435351)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>14
,p_column_heading=>'aut_ref'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840370544435351)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>15
,p_column_heading=>'recap_lote'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840458232435351)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>16
,p_column_heading=>'valor_documento'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840561551435351)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>17
,p_column_heading=>'nro_ret'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840676759435352)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>18
,p_column_heading=>'nro_comprobante'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840776427435352)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>8
,p_column_heading=>'Titular Cuenta'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840857001435352)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>9
,p_column_heading=>'Nro Cheque'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251840963675435352)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>10
,p_column_heading=>'Nro Cuenta'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841077034435352)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>11
,p_column_heading=>'Nro Pin'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841164614435352)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>19
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:24:&SESSION.:eliminar:&DEBUG.::P24_SEQ_ID,P24_MCD_VALOR_MOVIMIENTO,P24_TFP_ID_ELIMINAR:#COL02#,#COL10#,#COL08#'
,p_column_linktext=>'Eliminar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841273315435352)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841355376435352)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841480350435352)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841568244435352)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841655809435352)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841766445435352)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841879998435352)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251841968838435352)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251842062383435352)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251842174500435352)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(251855767755741635)
,p_plug_name=>'parametros'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(251878469439063741)
,p_name=>'Cheques disponibles'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cre.cre_id, ',
'       cre.emp_id, ',
'       cre.ede_id, ',
'       ede.ede_descripcion,',
'       cre.cre_id_reemplazado, ',
'       cre.rca_id, ',
'       cre.cre_estado_cheque, ',
'       cre.cre_nro_cheque, ',
'       cre.cre_nro_cuenta, ',
'       cre.cre_nro_pin, ',
'       cre.cre_valor, ',
'       cre.cre_titular_cuenta, ',
'       cre.cre_observaciones, ',
'       cre.cre_tipo, ',
'       cre.cre_fecha_deposito, ',
'       cre.cre_estado_registro, ',
'       cre.cli_id, ',
'       cre.cre_beneficiario, ',
'       cre.trx_id,',
'       ''Pagar'' Pago',
'FROM asdm_cheques_recibidos cre,',
'     asdm_entidades_destinos ede',
'WHERE cre.cre_tipo = pq_constantes.fn_cv_tch_posfechado',
'      AND (cre.cre_estado_cheque = pq_constantes.fn_cv_ech_ingresado ',
'           OR cre.cre_estado_cheque = pq_constantes.fn_cv_ech_postergado)',
'      AND cre.cre_fecha_deposito < SYSDATE',
'      AND cre.ede_id = ede.ede_id',
'      AND cre.cli_id = :P24_CLI_ID ',
'      AND cre.emp_id = :F_EMP_ID',
'      AND cre.cre_nro_cheque NOT IN (SELECT c013',
'                                   FROM apex_collections',
'                                  WHERE collection_name  = pq_constantes_caja.fn_cv_coleccion_mov_caja',
'                                        AND c014 = cre.cre_nro_cuenta',
'                                        AND c015 = cre.cre_nro_pin)',
''))
,p_display_when_condition=>'(:P24_TFP_ID = pq_constantes.fn_cn_tfp_id_cheque) AND (TO_NUMBER(:P24_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P24_SUM_PAGOS))'
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen cheques disponibles'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(251878469439063741)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251878767663063746)
,p_query_column_id=>1
,p_column_alias=>'CRE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cre Id'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251878860993063748)
,p_query_column_id=>2
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251878960297063748)
,p_query_column_id=>3
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Ede Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(255251472711615565)
,p_query_column_id=>4
,p_column_alias=>'EDE_DESCRIPCION'
,p_column_display_sequence=>4
,p_column_heading=>'Ede Descripcion'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879065504063748)
,p_query_column_id=>5
,p_column_alias=>'CRE_ID_REEMPLAZADO'
,p_column_display_sequence=>5
,p_column_heading=>'Cre Id Reemplazado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879156013063748)
,p_query_column_id=>6
,p_column_alias=>'RCA_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Rca Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879277023063748)
,p_query_column_id=>7
,p_column_alias=>'CRE_ESTADO_CHEQUE'
,p_column_display_sequence=>7
,p_column_heading=>'Cre Estado Cheque'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879363059063748)
,p_query_column_id=>8
,p_column_alias=>'CRE_NRO_CHEQUE'
,p_column_display_sequence=>8
,p_column_heading=>'Nro. Cheque'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879462714063748)
,p_query_column_id=>9
,p_column_alias=>'CRE_NRO_CUENTA'
,p_column_display_sequence=>9
,p_column_heading=>'Nro. Cuenta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879575778063748)
,p_query_column_id=>10
,p_column_alias=>'CRE_NRO_PIN'
,p_column_display_sequence=>10
,p_column_heading=>'Nro. Pin'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879659939063748)
,p_query_column_id=>11
,p_column_alias=>'CRE_VALOR'
,p_column_display_sequence=>11
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879776832063748)
,p_query_column_id=>12
,p_column_alias=>'CRE_TITULAR_CUENTA'
,p_column_display_sequence=>12
,p_column_heading=>'Cre Titular Cuenta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879856799063748)
,p_query_column_id=>13
,p_column_alias=>'CRE_OBSERVACIONES'
,p_column_display_sequence=>13
,p_column_heading=>'Observaciones'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251879971466063748)
,p_query_column_id=>14
,p_column_alias=>'CRE_TIPO'
,p_column_display_sequence=>14
,p_column_heading=>'Cre Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251880077552063748)
,p_query_column_id=>15
,p_column_alias=>'CRE_FECHA_DEPOSITO'
,p_column_display_sequence=>15
,p_column_heading=>'Fecha Deposito'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251880180506063748)
,p_query_column_id=>16
,p_column_alias=>'CRE_ESTADO_REGISTRO'
,p_column_display_sequence=>16
,p_column_heading=>'Cre Estado Registro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251880280840063748)
,p_query_column_id=>17
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>17
,p_column_heading=>'Cli Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251880358494063748)
,p_query_column_id=>18
,p_column_alias=>'CRE_BENEFICIARIO'
,p_column_display_sequence=>18
,p_column_heading=>'Cre Beneficiario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(251880474370063748)
,p_query_column_id=>19
,p_column_alias=>'TRX_ID'
,p_column_display_sequence=>19
,p_column_heading=>'Trx Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(253804960350725331)
,p_query_column_id=>20
,p_column_alias=>'PAGO'
,p_column_display_sequence=>20
,p_column_heading=>'Pago'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:24:&SESSION.:carga_detalle:&DEBUG.::P24_CRE_ID,P24_EDE_ID,P24_MCD_VALOR_MOVIMIENTO,P24_CRE_TITULAR_CUENTA,P24_CRE_NRO_CHEQUE,P24_CRE_NRO_CUENTA,P24_CRE_NRO_PIN:#CRE_ID#,#EDE_ID#,#CRE_VALOR#,#CRE_TITULAR_CUENTA#,#CRE_NRO_CHEQUE#,#CRE_NRO_'
||'CUENTA#,#CRE_NRO_PIN#'
,p_column_linktext=>'#PAGO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(252404751808978432)
,p_plug_name=>'Agregar Forma Pago'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(252405758041980288)
,p_plug_name=>'Datos Cheque Nuevo'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'(:P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')) AND (TO_NUMBER(:P24_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P24_SUM_PAGOS))'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(256408767533974968)
,p_plug_name=>'Mensaje'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>2
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P24_MENSAJE IS NOT NULL;'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(263026751510317872)
,p_plug_name=>'Datos Guardar Factura desde Orden'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(252405275190978436)
,p_button_sequence=>43
,p_button_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_button_name=>'P24_CARGAR'
,p_button_static_id=>'P24_CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Cargar'
,p_button_alignment=>'LEFT'
,p_button_condition=>'TO_NUMBER(:P24_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P24_SUM_PAGOS)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_request_source=>'carga_detalle'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(253960372825271310)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(251839055597435350)
,p_button_name=>'BT_REALIZAR_PAGO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Pagar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'(:P24_VALOR_TOTAL_PAGOS = :P24_SUM_PAGOS) AND (:P24_MENSAJE IS NULL)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(251853563160683504)
,p_branch_action=>'f?p=&APP_ID.:24:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 01-MAR-2010 11:11 by ONARANJO'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(251850766349647314)
,p_name=>'P24_SEQ_ID'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(251855767755741635)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252405068027978435)
,p_name=>'P24_TFP_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_FORMAS_PAGO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'        lv_lov := pq_ven_listas_caja.fn_lov_formas_pago',
'(',
':F_EMP_ID,',
':P0_TTR_ID,',
':P30_POLITICA_VENTA);',
'',
'return (lv_lov);',
'',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252405483912978436)
,p_name=>'P24_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>42
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'TO_NUMBER(:P24_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P24_SUM_PAGOS)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252406064145980291)
,p_name=>'P24_EDE_ID'
,p_item_sequence=>66
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Entidad Destino'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_FINANCIERAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ede.ede_descripcion,',
'       ede.ede_id',
'FROM   asdm_entidades_destinos ede',
'WHERE  ede.ede_tipo = ''EFI''',
'and ede.emp_id = :f_emp_id',
'and ede.ede_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'union',
'select ''-- Seleccione Entidad --'' d, null r from dual',
'order by 1 '))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252406273916980291)
,p_name=>'P24_CRE_NRO_CHEQUE'
,p_item_sequence=>68
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Cheque'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252406469701980291)
,p_name=>'P24_CRE_NRO_CUENTA'
,p_item_sequence=>69
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Cuenta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252406680578980291)
,p_name=>'P24_CRE_NRO_PIN'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Pin'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252406875351980291)
,p_name=>'P24_CRE_TITULAR_CUENTA'
,p_item_sequence=>67
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Titular Cuenta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252407083834980292)
,p_name=>'P24_CRE_FECHA_DEPOSITO'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_prompt=>unistr('Fecha Dep\00BF\00BFsito')
,p_source=>'sysdate'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(252636261286807394)
,p_name=>'P24_VALOR_ANTICIPO_CLIENTES'
,p_item_sequence=>41
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_pagos_cuota.fn_saldo_anticipo_cliente(:P24_TFP_ID,',
'				                    :P24_CLI_ID,',
'				                    :F_EMP_ID,',
'                                                    :P0_ERROR);'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Total Saldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(253682776572575831)
,p_name=>'P24_CLI_ID'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(251855767755741635)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(253856165770509730)
,p_name=>'P24_VALOR_TOTAL_PAGOS'
,p_item_sequence=>39
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_prompt=>'Valor a Pagar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(253858056812535499)
,p_name=>'P24_CLI_IDENTIFICACION'
,p_item_sequence=>37
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_prompt=>unistr('Identificaci\00BF\00BFn Cliente')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(253858652788543775)
,p_name=>'P24_CLI_NOMBRE'
,p_item_sequence=>38
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(253983060254485257)
,p_name=>'P24_SUM_PAGOS'
,p_item_sequence=>53
,p_item_plug_id=>wwv_flow_imp.id(252404751808978432)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Sum Pagos'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(TO_NUMBER(c006)),0) suma_valor',
'  FROM apex_collections',
'WHERE collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(254054455723956964)
,p_name=>'P24_CRE_ID'
,p_item_sequence=>64
,p_item_plug_id=>wwv_flow_imp.id(252405758041980288)
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(256278770961923125)
,p_name=>'P24_TFP_ID_ELIMINAR'
,p_item_sequence=>82
,p_item_plug_id=>wwv_flow_imp.id(251855767755741635)
,p_prompt=>'Tfp Id Eliminar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(256409051734979916)
,p_name=>'P24_MENSAJE'
,p_item_sequence=>92
,p_item_plug_id=>wwv_flow_imp.id(256408767533974968)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(263027060860320630)
,p_name=>'P24_ORD_ID'
,p_item_sequence=>102
,p_item_plug_id=>wwv_flow_imp.id(263026751510317872)
,p_prompt=>'Ord Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(253576075854598431)
,p_validation_name=>'valida_anticipo_cliente'
,p_validation_sequence=>10
,p_validation=>'TO_NUMBER(:P24_VALOR_ANTICIPO_CLIENTES) >= TO_NUMBER(:P24_MCD_VALOR_MOVIMIENTO);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'El valor debe ser menor o igual al Saldo de Anticipo Cliente'
,p_validation_condition=>':P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'') AND :P24_MCD_VALOR_MOVIMIENTO IS NOT NULL'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(252405483912978436)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(253592764490774879)
,p_validation_name=>'valida_mcd_valor_movimiento'
,p_validation_sequence=>10
,p_validation=>'NVL(:P24_MCD_VALOR_MOVIMIENTO,0) <> 0;'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Especifique un valor'
,p_validation_condition=>'carga_detalle'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(252405483912978436)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(254670063638514352)
,p_validation_name=>'P24_EDE_ID'
,p_validation_sequence=>20
,p_validation=>'P24_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione Una Entidad'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(252406064145980291)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(254678973306640126)
,p_validation_name=>'P24_CRE_TITULAR_CUENTA'
,p_validation_sequence=>30
,p_validation=>'P24_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la cuenta'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(252406875351980291)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(254679783480652582)
,p_validation_name=>'P24_CRE_NRO_CHEQUE'
,p_validation_sequence=>40
,p_validation=>'P24_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cheque'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(252406273916980291)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(254680374607659419)
,p_validation_name=>'P24_CRE_NRO_CUENTA'
,p_validation_sequence=>50
,p_validation=>'P24_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cuenta'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(252406469701980291)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(254680554306663042)
,p_validation_name=>'P24_CRE_NRO_PIN'
,p_validation_sequence=>60
,p_validation=>'P24_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Nro de Pin'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(252406680578980291)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(262897155152890046)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'',
'  :p0_ttr_descripcion := NULL;',
'  :p0_datfolio        := NULL;',
'  :p0_fol_sec_actual  := NULL;',
'  :p0_periodo         := NULL;',
'  :p0_ttr_id          := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id);',
'   ln_tgr_id          := pq_ven_listas_caja.fn_tipo_grupo_transaccion_seg (:f_emp_id, :f_seg_id); ',
'    ',
'  pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                  :f_pca_id,',
'                                                   ln_tgr_id,',
'                                                  :p0_ttr_descripcion,',
'                                                  :p0_periodo,',
'                                                  :p0_datfolio,',
'                                                  :p0_fol_sec_actual,',
'                                                  :p0_pue_num_sri,',
'                                                  :p0_uge_num_est_sri,',
'                                                  :p0_pue_id,',
'                                                  :p0_ttr_id,',
'                                                  :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>230644003883125120
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(252527053165818382)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(:P24_EDE_ID,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    :P24_CRE_ID,',
'                                                    :P24_TFP_ID,',
'                                                    :P24_MCD_VALOR_MOVIMIENTO,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    :P24_CRE_TITULAR_CUENTA,',
'                                                    :P24_CRE_NRO_CHEQUE,',
'                                                    :P24_CRE_NRO_CUENTA,',
'                                                    :P24_CRE_NRO_PIN);',
':P24_SEQ_ID := NULL;',
':P24_MCD_VALOR_MOVIMIENTO := NULL;',
':P24_CRE_ID := NULL;',
':P24_CRE_FECHA_DEPOSITO := SYSDATE;',
':P24_CRE_TITULAR_CUENTA := NULL;',
':P24_EDE_ID := NULL;',
':P24_CRE_NRO_CHEQUE := NULL;',
':P24_CRE_NRO_CUENTA := NULL;',
':P24_CRE_NRO_PIN := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'(:REQUEST = ''carga_detalle'') AND (TO_NUMBER(:P24_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P24_SUM_PAGOS))'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>220273901896053456
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(251867970852941199)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_linea_coleccion_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF :P24_TFP_ID_ELIMINAR = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'') THEN',
'   :P24_VALOR_ANTICIPO_CLIENTES := :P24_VALOR_ANTICIPO_CLIENTES + :P24_MCD_VALOR_MOVIMIENTO;',
'END IF;',
'pq_inv_movimientos.pr_borra_reg_colecciones(:p24_seq_id ,pq_constantes.fn_retorna_constante(:f_emp_id,''coleccion_mov_caja''));',
'',
':P24_SEQ_ID := NULL;',
':P24_MCD_VALOR_MOVIMIENTO := NULL;',
':P24_TFP_ID_ELIMINAR := NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>219614819583176273
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(252825782130409821)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion_boton'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF :P24_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'') THEN',
'   :P24_VALOR_ANTICIPO_CLIENTES := :P24_VALOR_ANTICIPO_CLIENTES - :P24_MCD_VALOR_MOVIMIENTO;',
'END IF;',
'pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(:P24_EDE_ID,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    :P24_CRE_ID,',
'                                                    :P24_TFP_ID ,',
'                                                    :P24_MCD_VALOR_MOVIMIENTO,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    :P24_CRE_TITULAR_CUENTA,',
'                                                    :P24_CRE_NRO_CHEQUE,',
'                                                    :P24_CRE_NRO_CUENTA,',
'                                                    :P24_CRE_NRO_PIN,',
'                                                    :P0_ERROR);',
'',
':P24_SEQ_ID := NULL;',
':P24_MCD_VALOR_MOVIMIENTO := NULL;',
':P24_CRE_ID := NULL;',
':P24_CRE_FECHA_DEPOSITO := SYSDATE;',
':P24_EDE_ID := NULL;',
':P24_CRE_TITULAR_CUENTA := NULL;',
':P24_CRE_NRO_CHEQUE := NULL;',
':P24_CRE_NRO_CUENTA := NULL;',
':P24_CRE_NRO_PIN := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(252405275190978436)
,p_process_when=>'(:REQUEST = ''carga_detalle'') AND (TO_NUMBER(:P24_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P24_SUM_PAGOS))'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>220572630860644895
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(254641674428904973)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'pq_ven_pagos_cuota.pr_graba_pago_cuota(:F_EMP_ID,',
'                                       :F_UGE_ID,',
'                                       :F_USER_ID,',
'                                       :F_PCA_ID,',
'                                       :P24_AGE_ID_GESTIONADO_POR,',
'                                       :P24_CLI_ID,',
'                                       :P24_SUM_PAGOS,',
'                                       :P20_ERROR);',
'',
'IF :P24_PV_ERROR IS NULL THEN',
'   :P24_MENSAJE := ''El Pago de Cuota(s) se ha realizado satisfactoriamente'';',
'END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(253960372825271310)
,p_process_success_message=>'Pago Cuota Grabado'
,p_internal_uid=>222388523159140047
);
wwv_flow_imp.component_end;
end;
/
