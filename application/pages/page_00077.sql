prompt --application/pages/page_00077
begin
--   Manifest
--     PAGE: 00077
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>77
,p_name=>'Provision por Montos'
,p_step_title=>'Provision por Montos'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(122954357905390570)
,p_plug_name=>'Provisiones por Montos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>21
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'c001 TIPO_PROVISION,',
'c002 UNIDAD_GESTION,',
'c003 CLIENTE,',
'c004 COM_ID,',
'c005 CDE_ID,',
'c006 ITE_SKU_ID,',
'(select it.ite_descripcion_larga from v_inv_items it where it.ite_sku_id = to_number(c.c006)) DESCRIPCION,',
'round(to_number(c007),2) VENTA_NETA',
'from apex_collections  c where c.collection_name = ''COL_PROV_MONTOS_DET''',
'and c.c003 = :p82_cli_id',
'and c.c001 = :p82_tpr_id',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(122954569814390570)
,p_name=>'Report 1'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>90701418544625644
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124376965918607511)
,p_db_column_name=>'TIPO_PROVISION'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Tipo Provision'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377065678607511)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377156820607511)
,p_db_column_name=>'CLIENTE'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377268714607512)
,p_db_column_name=>'COM_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377359855607512)
,p_db_column_name=>'CDE_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Cde Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377465433607512)
,p_db_column_name=>'ITE_SKU_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Ite Sku Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ITE_SKU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377581855607512)
,p_db_column_name=>'DESCRIPCION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(124377665622607512)
,p_db_column_name=>'VENTA_NETA'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Venta Neta'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VENTA_NETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(122955864856390572)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'907028'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'UNIDAD_GESTION:TIPO_PROVISION:CLIENTE:COM_ID:CDE_ID:ITE_SKU_ID:DESCRIPCION:VENTA_NETA'
,p_sum_columns_on_break=>'VENTA_NETA'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(122970966621478961)
,p_name=>'Provisiones por Montos'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'c001 TIPO_PROVISION,',
'c002 Unidad_gestion,',
'c003 CLIENTE,',
'c004 NOMBRES,',
'c005 MONTO_INICIAL,',
'c006 MONTO_FINAL,',
'c007 PORCENTAJE,',
'round(to_number(c008),2) TOTAL_VENTA,',
'round(to_number(C009),2) REBAJA',
'from apex_collections where collection_name = ''COL_PROVISIONES_MONTOS''',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124373954490604221)
,p_query_column_id=>1
,p_column_alias=>'TIPO_PROVISION'
,p_column_display_sequence=>1
,p_column_heading=>'Tipo Provision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374072795604224)
,p_query_column_id=>2
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>2
,p_column_heading=>'Unidad Gestion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374159156604224)
,p_query_column_id=>3
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>3
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374267462604225)
,p_query_column_id=>4
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>4
,p_column_heading=>'Nombres'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374371631604225)
,p_query_column_id=>5
,p_column_alias=>'MONTO_INICIAL'
,p_column_display_sequence=>5
,p_column_heading=>'Monto Inicial'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374451381604225)
,p_query_column_id=>6
,p_column_alias=>'MONTO_FINAL'
,p_column_display_sequence=>6
,p_column_heading=>'Monto Final'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374563763604225)
,p_query_column_id=>7
,p_column_alias=>'PORCENTAJE'
,p_column_display_sequence=>7
,p_column_heading=>'Porcentaje'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374674121604225)
,p_query_column_id=>8
,p_column_alias=>'TOTAL_VENTA'
,p_column_display_sequence=>8
,p_column_heading=>'Total Venta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124374751507604225)
,p_query_column_id=>9
,p_column_alias=>'REBAJA'
,p_column_display_sequence=>9
,p_column_heading=>'Rebaja'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(122956062057390572)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(122954357905390570)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:76:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122984756887523416)
,p_name=>'P77_TPR_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(122954357905390570)
,p_prompt=>'Tpr Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122985062774525150)
,p_name=>'P77_CLI_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(122954357905390570)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
