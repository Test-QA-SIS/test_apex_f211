prompt --application/pages/page_00030
begin
--   Manifest
--     PAGE: 00030
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>30
,p_name=>'PagoFactura'
,p_step_title=>'PagoFactura'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>',
'',
'<script type="text/javascript" >',
'',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00C3\00BAmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'',
'function valida_numEnterTab(evt,itemid,ln_valor){',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'  if (evt.keyCode == 13) {',
'     ',
'        $x(itemid).focus();',
'        return false;',
'           ',
'   }else if(evt.keyCode == 8){',
'       return true;',
'   }else if(evt.keyCode == 9){',
'       return true;',
'   }else if(evt.keyCode == null){',
'       return true;  ',
'   }else{',
'       if (!patron_numero.test((ln_valor).value)) {',
unistr('        alert(''Ingrese solo n\00FAmeros'');'),
'        ln_valor.value = '''';',
'        html_GetElement((ln_valor).id).focus();',
'        return true;',
'        } ',
'    }',
' return false;',
'}',
'',
'function valida_numero_dec(ln_valor)',
'{',
'var patron_numero =/^^\d{1,10}(\.\d{1,2})?$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00C3\00BAmeros con dos Decimales'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function sololetras(e) {',
'    key=e.keyCode || e.which;',
'    teclado=String.fromCharCode(key).toLowerCase();',
unistr('    letras="qwertyuiopasdfghjkl\00F1zxcvbnm ";'),
'    especiales="8-37-38-46-164";',
'    teclado_especial=false;',
'    for(var i in especiales){',
'        if(key==especiales[i]){',
'            teclado_especial=true;',
'            break;',
'        }',
'    }',
' ',
'    if(letras.indexOf(teclado)==-1 && !teclado_especial){',
'        return ;        ',
'    }',
'',
'    return 1;',
'}',
'',
'',
'',
'function valida_letras(ln_valor)',
'{',
'let regex = /^[a-zA-Z]+$/; ',
'if (!regex.test((ln_valor).value))',
'{',
'alert(''Debe Ingresar solo Letras'');',
'ln_valor.value = "";',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function convertir_mayu(ln_valor)',
'{',
'var res_valida_letras;',
'ln_valor.value = ln_valor.value.toUpperCase();',
'',
'',
'}',
'',
'</script>',
'',
''))
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38522767998834628)
,p_plug_name=>'Consulta Puntos Acumulados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>32
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>'(:P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'')) OR :P30_POLITICA_VENTA IN(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_pol_id_48''),112)'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(35738481051115160)
,p_name=>'Promociones disponibles'
,p_parent_plug_id=>wwv_flow_imp.id(38522767998834628)
,p_display_sequence=>35
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    a.bip_id,',
'          a.pro_id,',
'          b.pro_descripcion,',
'          a.per_id,',
'          d.ite_sku_id_ganado || e.ite_descripcion_larga premio,--a.bip_valor_saldo,-- * b.pro_factor_conversion bip_valor_saldo,',
'          d.bpd_valor_generado bip_valor_saldo,',
'          ROUND(d.bpd_valor_generado/ d.bpd_valor_generado,2) cantidad_premios,',
'          c.pta_descripcion,',
'          c.pta_id,',
'          d.pdi_id',
'FROM      ven_bitacora_promociones a, ',
'          ven_promociones b, ',
'          ven_promos_tipos_aplica c,',
'          ven_bitacora_promos_det d,',
'          inv_items e',
'WHERE     a.per_id=NVL(:P30_PER_ID_PUNTOS,:P30_per_id)',
'AND       :P30_PER_ID_PUNTOS IS NOT NULL',
'AND       a.pro_id=b.pro_id',
'AND       c.pro_id=a.pro_id',
'AND       c.pta_id=a.pta_id',
'AND       d.bpd_valor_saldo > 0--a.bip_valor_saldo > 0',
'AND       d.pro_id = a.pro_id',
'AND       d.per_id_beneficiario = a.per_id',
'AND       d.pta_id = c.pta_id',
'AND       e.ite_sku_id(+) = d.ite_sku_id_ganado',
'AND       ((a.pta_id = 2 AND :p30_politica_venta =  pq_constantes.fn_retorna_constante(null,''cn_obsequios_puntos'')))',
'',
'UNION',
'',
'SELECT    bip_id, pro_id, pro_descripcion, per_id, premio, ',
'          SUM(bip_valor_saldo) saldo,',
'          SUM(cantidad_premios) cantidad_premios, ',
'          pta_descripcion, pta_id, pdi_id',
'FROM      ',
'',
'(SELECT    a.bip_id,',
'          a.pro_id,',
'          b.pro_descripcion,',
'          a.per_id,',
'          d.ite_sku_id_ganado || e.ite_descripcion_larga premio,--a.bip_valor_saldo,-- * b.pro_factor_conversion bip_valor_saldo,',
'          d.bpd_valor_generado bip_valor_saldo,',
'          ROUND(d.bpd_valor_generado/ d.bpd_valor_generado,2) cantidad_premios,',
'          c.pta_descripcion,',
'          c.pta_id,',
'          d.pdi_id',
'FROM      ven_bitacora_promociones a, ',
'          ven_promociones b, ',
'          ven_promos_tipos_aplica c,',
'          ven_bitacora_promos_det d,',
'          inv_items e',
'WHERE     a.per_id=NVL(:P30_PER_ID_PUNTOS,:P30_per_id)',
'AND       :P30_PER_ID_PUNTOS IS NOT NULL',
'AND       a.pro_id=b.pro_id',
'AND       c.pro_id=a.pro_id',
'AND       c.pta_id=a.pta_id',
'AND       d.bpd_valor_saldo > 0--a.bip_valor_saldo > 0',
'AND       d.pro_id = a.pro_id',
'AND       d.per_id_beneficiario = a.per_id',
'AND       d.pta_id = c.pta_id',
'AND       e.ite_sku_id(+) = d.ite_sku_id_ganado',
'AND       ((a.pta_id = 1 AND :p30_politica_venta <> pq_constantes.fn_retorna_constante(null,''cn_obsequios_puntos'')))',
')',
'GROUP BY bip_id, pro_id, pro_descripcion, per_id, premio, pta_descripcion, pta_id, pdi_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35738766651115162)
,p_query_column_id=>1
,p_column_alias=>'BIP_ID'
,p_column_display_sequence=>7
,p_column_heading=>'BIP_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35738870957115164)
,p_query_column_id=>2
,p_column_alias=>'PRO_ID'
,p_column_display_sequence=>6
,p_column_heading=>'PRO_ID'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35738977900115164)
,p_query_column_id=>3
,p_column_alias=>'PRO_DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Promocion'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_PRO_ID,P30_SALDO_MAX_APL_DEV,P30_SALDO_PROMO_DEV,P30_PTA_ID,P30_PUNTOS_CANCELAR,P30_PDI_ID:#PRO_ID#,#BIP_VALOR_SALDO#,#BIP_VALOR_SALDO#,#PTA_ID#,#BIP_VALOR_SALDO#,#PDI_ID#'
,p_column_linktext=>'#PRO_DESCRIPCION#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35739076547115164)
,p_query_column_id=>4
,p_column_alias=>'PER_ID'
,p_column_display_sequence=>5
,p_column_heading=>'PER_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(11419962553808412586)
,p_query_column_id=>5
,p_column_alias=>'PREMIO'
,p_column_display_sequence=>9
,p_column_heading=>'Premio'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35739183046115164)
,p_query_column_id=>6
,p_column_alias=>'BIP_VALOR_SALDO'
,p_column_display_sequence=>4
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39268181472456134)
,p_query_column_id=>7
,p_column_alias=>'CANTIDAD_PREMIOS'
,p_column_display_sequence=>8
,p_column_heading=>'Cantidad Premios'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37840864011312425)
,p_query_column_id=>8
,p_column_alias=>'PTA_DESCRIPCION'
,p_column_display_sequence=>3
,p_column_heading=>'Tipo Aplicacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37840963742312515)
,p_query_column_id=>9
,p_column_alias=>'PTA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cod.Tipo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(11419962663850412591)
,p_query_column_id=>10
,p_column_alias=>'PDI_ID'
,p_column_display_sequence=>10
,p_column_heading=>'Pdi Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(11419784653677403074)
,p_plug_name=>' PARAMETROS_PROMOCION_PUNTOS'
,p_parent_plug_id=>wwv_flow_imp.id(38522767998834628)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>570
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(41550158277998833)
,p_name=>'prueba mov caja'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'      *',
'FROM   apex_collections',
'WHERE  collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41550466565998866)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41550564767998873)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41550655208998873)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41550774673998873)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41550871341998873)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41550959003998873)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551052276998873)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551157010998873)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551278494998876)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551357906998876)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551455592998876)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551571071998877)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551670485998877)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551780898998877)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551869302998877)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41551981759998877)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552077559998877)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552155782998877)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552280188998877)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552370073998877)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552479661998877)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552561834998881)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552673540998881)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552773341998881)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552868415998881)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41552977838998881)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553059068998882)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553169873998882)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553255151998882)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553381946998882)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553464882998882)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553571550998882)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553656728998882)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553755844998882)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553873939998882)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41553983355998882)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554074503998882)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554151966998882)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554257573998882)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554363994998882)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554465434998884)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554572867998884)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554680513998884)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554770898998885)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554851735998885)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41554952547998885)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555074966998885)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555168397998885)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555273167998885)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555382060998885)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555477339998885)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555573875998885)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555662836998885)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555755099998885)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555866452998885)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41555955158998885)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556071479998885)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556155523998885)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556265736998885)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556357343998887)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556469157998887)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556572701998887)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556674241998887)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556770616998887)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556876995998887)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41556971729998887)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(42924372414299819)
,p_name=>'prueba col baja inv'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>503
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                          ''cv_col_baja_inventario'');',
'',
'',
''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42998667798790489)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42998775073790500)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42998855668790500)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42998967291790500)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999058024790500)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999173518790500)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999259627790500)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999361440790500)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999464089790500)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999556368790500)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999661850790500)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999763015790500)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999866977790500)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42999970975790500)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000081541790500)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000166990790501)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000260885790501)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000378275790501)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000473018790501)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000570826790501)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000655372790501)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000760908790507)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000855002790507)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43000955905790507)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001060180790507)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001164085790507)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001282789790507)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001362983790507)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001472150790507)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001553465790507)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001658187790507)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001757529790508)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001867335790508)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43001980564790508)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002076593790508)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002183640790508)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002283281790508)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002382910790508)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002460928790508)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002574727790508)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002666881790508)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002771589790508)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002869940790508)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43002981798790508)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003064305790508)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003166185790508)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003263332790508)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003358161790508)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003467960790509)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003571004790509)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003652493790509)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003759668790509)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003864209790509)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104672778399569791)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104672866135569795)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104672958390569795)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673074368569795)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673167473569795)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673262770569795)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673370784569796)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673465064569796)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673567502569796)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673657421569796)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673763610569796)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104673866444569796)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43003975163790509)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(43010681402939134)
,p_name=>'prueba johanna precancelacion'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>130
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'SELECT c001 FROM apex_collections  WHERE collection_name = ''COLL_PREC_SELECCIONADOS'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43270358214884230)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(51002972818389730)
,p_name=>'Identificadores'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>560
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_2'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'   select c.c002 identificador',
'   from apex_collections c',
'   where c.collection_name = ''COL_IDENT_CONSIGNACION''',
'   and c.c006 = 1;    '))
,p_display_when_condition=>':P20_ORD_TIPO = ''VENMO'' AND :p20_ebs_id IS NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(45813367009270273)
,p_query_column_id=>1
,p_column_alias=>'IDENTIFICADOR'
,p_column_display_sequence=>1
,p_column_heading=>'Identificador'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(63341972044349916)
,p_name=>'prueba coll pago cuota'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name=''CO_PAGO_CUOTA'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342253081349932)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342375186349937)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342478447349937)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342567352349937)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342668374349937)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342776768349937)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342879418349937)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63342978552349937)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343062650349937)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343172660349937)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343275962349937)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343365819349937)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343566912349937)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343668847349937)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343770888349937)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343860801349938)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63343974048349938)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344057066349938)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344178267349938)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344252733349938)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344366680349938)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344459412349938)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344581037349938)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344662249349938)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344765051349938)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344854990349938)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63344961389349939)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345074217349939)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345175132349939)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345357924349939)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345457830349939)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345574308349939)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345662908349939)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345775373349939)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345855408349939)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63345957215349939)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346064402349939)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346183782349939)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346256214349939)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346354651349939)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346465882349939)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346579690349939)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346671111349939)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346783793349939)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346858721349939)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63346981205349940)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347052129349940)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347164299349940)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347267183349940)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347370907349940)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347463514349940)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347563334349940)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347662102349940)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35223475258794073)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35223565711794080)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35223656020794080)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35223764852794080)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35223866722794080)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35223975004794080)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35224072977794080)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35224159224794080)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35224272702794080)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35224376195794080)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35224459640794080)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(35224575552794080)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63347783971349940)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(104753066924254956)
,p_plug_name=>unistr('Asignaci\00C3\00B3n de Folios para :  ')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>62
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'(:P30_INT_MORA_COB>0 or :P30_GTOS_COB_COB>0 ) and (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and :P30_VALIDA_PUNTO = ''P'') or 	(:P30_ES_PRECANCELACION = ''S'' and :P30_VALIDA_PUNTO = ''P'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(104657655926516000)
,p_name=>unistr('Inter\00C3\00A9s de Mora - Nota de D\00C3\00A9bito ')
,p_parent_plug_id=>wwv_flow_imp.id(104753066924254956)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>500
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value =>NVL(c020,0),',
'                      p_size => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos_gc(''''PR_VALIDA_FOLIOS_GASTOS'''','' || seq_id ||',
'       '',''''IM'''', this.value ,''''P30_F_UGE_ID'''',''''P30_F_USER_ID'''',''''P30_F_EMP_ID'''',''''P30_CLI_ID'''',''''R72404504656751074'''',''''R72503601849514425'''')"'') folio,',
'c011 Establec,',
'c012 PtoEmis/*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =  pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_int_mora'');',
'',
'',
'}'';'))
,p_display_when_condition=>':P30_INT_MORA_COB>0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693251789666181)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693354846666190)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693466604666191)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693574202666191)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693662061666191)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693778040666191)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Int.Mora'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693878971666191)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104693967995666191)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
,p_report_column_width=>3
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694080469666191)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
,p_report_column_width=>3
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694176276666191)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694270171666191)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694379577666191)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694476748666191)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694555667666191)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694676994666191)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694758860666194)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694883926666194)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104694952427666194)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695053293666194)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695155743666194)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695278870666194)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695362234666194)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695468247666194)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695564385666194)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695678180666194)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695778700666194)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695862076666194)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104695970287666194)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696071594666194)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696172009666195)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696267471666195)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696373889666195)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696454020666195)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696582819666195)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696651961666195)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696777364666195)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696873240666195)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104696965854666195)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697060116666195)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697170456666195)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697262898666195)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697351520666195)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697472936666195)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697567580666195)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697665956666196)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697763354666196)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697866725666196)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104697968182666196)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698061149666196)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698164728666196)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698282016666196)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698358134666196)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698463177666196)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698560742666196)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698663954666196)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698755213666196)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698869608666196)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104698971683666196)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104699071118666196)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104699154298666206)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(104756753119279351)
,p_name=>'Gastos Cobranza - Nota Debito'
,p_parent_plug_id=>wwv_flow_imp.id(104753066924254956)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>520
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{ SELECT seq_id,',
'        c001 cli_id,',
'        c003 comprob,',
'        c006 emp_id,',
'        c007 cxc_id,',
'        c010 gtoscob,',
'        /*apex_item.text(30,',
'                       nvl(c020, 0),',
'                       p_size => 8,',
'                       p_attributes => ''onChange="onSubmit(''''PR_VALIDA_FOLIOS_GASTOS'''')"'') folio,*/',
'',
'         -- Activar si se quiere que no haga submit al digitar el valor',
'        apex_item.text(30,',
'                       nvl(c020, 0),',
'                       p_size => 8,',
'                       p_attributes => ''onChange="pr_ejecuta_proc_campos_datos_2(''''PR_VALIDA_FOLIOS_GASTOS'''','' || seq_id ||',
'       '',''''GC'''', this.value ,''''P30_F_UGE_ID'''',''''P30_F_USER_ID'''',''''P30_F_EMP_ID'''',''''P30_CLI_ID'''',''''R72503601849514425'''',''''R72404504656751074'''' )"'') folio,',
'',
'c011 Establec,',
'c012 PtoEmis',
'   FROM apex_collections',
'  WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_gto_cobr'');',
'',
'',
'',
' ',
'',
'}'';'))
,p_display_when_condition=>':P30_GTOS_COB_COB > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104756955212279358)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757066516279367)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757155001279367)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757267304279367)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757363698279367)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757483929279367)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Gtos.Cob.'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757573385279369)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757680677279371)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
,p_report_column_width=>3
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757752812279371)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
,p_report_column_width=>3
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757879422279371)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104757983961279371)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758061136279371)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758153557279372)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758256241279372)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758357400279372)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758460638279372)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758582700279372)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758652516279372)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758768568279372)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758862267279372)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104758967588279372)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759065925279372)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759157916279372)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759274407279372)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759355175279372)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759467260279372)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759577278279372)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759677777279372)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759761196279372)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759877286279373)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104759956512279373)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760070066279373)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760178619279373)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760268305279373)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760377078279373)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760476075279373)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760560061279375)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760658040279375)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760757906279375)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760873186279375)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104760965641279375)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761058943279375)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761169039279375)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761260506279375)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761363244279375)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761453938279375)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761580098279375)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761657681279375)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761764217279375)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761854906279375)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104761964125279376)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762065118279376)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762181458279376)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762254497279376)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762383077279376)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762466507279376)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762568629279376)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762658631279376)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762763095279376)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(104762872625279376)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(104770569835340942)
,p_plug_name=>'Pagar'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>72
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(135230079858577532)
,p_name=>'pruebas jaime andres'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>550
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'from apex_collections',
'where collection_name = ',
' pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_plz_fact'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230373382577833)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230457682577854)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230574094577854)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230678351577854)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230765093577854)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230858163577854)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135230962006577855)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231065776577855)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231165562577855)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231268916577855)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231377477577855)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231462163577855)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231567012577855)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231664346577855)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231778757577855)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231878241577855)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135231959147577855)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232053686577855)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232175707577855)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232276476577855)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232371454577855)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232480334577855)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232555917577856)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232654538577856)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232752491577856)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232868683577856)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135232952435577856)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233065858577863)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233152095577863)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233262181577863)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233373596577871)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233473846577871)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233574688577871)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233669260577871)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233757732577871)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233877026577871)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135233969608577871)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234060889577871)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234164755577871)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234273971577872)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234382020577872)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234457268577872)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234580984577872)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234675931577872)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234780045577872)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234860998577872)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135234975173577872)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235063700577872)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235171493577872)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235269153577872)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235375342577872)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235463859577872)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235558612577872)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235652654577872)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235752381577872)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235877103577872)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135235983780577872)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236069847577873)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236168185577873)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236266545577873)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236378394577873)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236474998577873)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236581047577873)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236666398577873)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236762807577873)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135236880126577873)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(142809657812230213)
,p_plug_name=>'Parametros Identificadores'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>540
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_when_condition=>'TO_NUMBER(:P30_NRO_REG_ITEM_IDEN) > 0'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(142810656888230215)
,p_name=>'sql unpivot coleccion identificadores'
,p_parent_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>130
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'   return ',
'pq_inv_movimientos_iden.fn_sql_col_inv_ids_unpivot(pn_emp_id => :F_EMP_ID, ',
'                                                            pv_error => :P0_ERROR);',
'END;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142810851833230218)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142810960058230220)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811062875230220)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811159226230220)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811264068230220)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811371731230220)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811456680230220)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811583582230220)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811679370230220)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811757900230220)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811875443230220)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142811957221230221)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812065566230221)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812175441230221)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812264232230221)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814079872230221)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812682589230221)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812769273230221)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812880202230221)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812982753230221)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813080126230221)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813155756230221)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813275125230221)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813369573230221)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813452090230221)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813560713230221)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813672815230221)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813768607230221)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813870063230221)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142813981370230221)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815966122230222)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816070792230222)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814176515230221)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814259795230221)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814353563230221)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814453562230222)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814564730230222)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814666532230222)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814779666230222)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814856705230222)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142814972511230222)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815060627230222)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815181356230222)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815275228230222)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815382712230222)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815456556230222)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815572217230222)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815683067230222)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815764278230222)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142815856905230222)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812359767230221)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812452897230221)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142812563673230221)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816157551230222)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816776311230227)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816270135230222)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816362728230222)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816465586230222)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816563802230222)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142816674153230227)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(142816855544230235)
,p_plug_name=>'Identificadores'
,p_parent_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>150
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'TO_NUMBER(:P30_NRO_REG_ITEM_IDEN) > 0'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(142817459435230238)
,p_name=>'COL_COMPROBANTE_INV'
,p_parent_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT * ',
'  FROM apex_collections c ',
' WHERE collection_name = ''COL_INV_IDENTIFICADORES1'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820455241230239)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820576221230239)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820672317230239)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820763972230239)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820871465230239)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820967227230239)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821083608230239)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821179616230239)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821265250230239)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821383875230239)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821472586230239)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821565868230239)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821654270230239)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821764293230239)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821878856230239)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142821957384230240)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822054757230240)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822155514230240)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822267557230241)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822355737230241)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822465517230241)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822564488230241)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822679163230241)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822765795230241)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822864805230241)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142822966585230241)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823076253230241)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823161765230241)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823280478230241)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823364088230241)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823480130230241)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823578485230241)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823682555230241)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823770765230241)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823859690230241)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142823982876230241)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142824053499230242)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142824178842230242)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820358494230239)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142817675315230238)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142817762609230238)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142817875466230238)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142817980907230238)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818071143230238)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818155341230238)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818279186230238)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818352452230238)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818464459230238)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818559284230238)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818676583230238)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818751696230238)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818868246230238)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142818964936230238)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819056056230238)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819177127230238)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819278732230238)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819374707230239)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819468448230239)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819553253230239)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819655369230239)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819767908230239)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819861007230239)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142819968510230239)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820080589230239)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820156686230239)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142820274937230239)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(187975102089354743)
,p_name=>'Detalle Pago Tarjeta'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>42
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'REGION_POSITION_07'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tfp_descripcion,',
'       to_number(c006) valor,',
'       c040 detalle_pago,',
'       co.seq_id,',
'       (SELECT substr(r.rpr_num_tarj_trun, 1, 6)',
'          FROM car_redes_pago_resp r',
'         WHERE r.rpr_id = c030) bin,',
'       (SELECT r.rpr_fecha_respuesta',
'          FROM car_redes_pago_resp r',
'         WHERE r.rpr_id = c030) fecha,',
'       (SELECT r.rpr_secuen_trans',
'          FROM car_redes_pago_resp r',
'         WHERE r.rpr_id = c030) nroregistro,',
'       (SELECT r.rpc_id FROM car_redes_pago_resp r WHERE r.rpr_id = c030) rpc_id,',
'       (select r.rpr_id_req from car_redes_pago_resp r where r.rpr_id=c030) rpr_id,',
'       CASE',
'         WHEN (c030 IS NOT NULL AND',
'              (SELECT trunc(r.rpr_fecha_respuesta)',
'                  FROM car_redes_pago_resp r',
'                 WHERE r.rpr_id = c030) = trunc(SYSDATE)) THEN',
'          ''ANULAR''',
'         ELSE',
'          ''''',
'       END anular',
'  FROM apex_collections co, asdm_tipos_formas_pago p',
' WHERE collection_name = ''CO_MOV_CAJA''',
'   AND p.tfp_id = to_number(co.c005);',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT to_number(c006) valor',
'  FROM apex_collections co',
' WHERE collection_name = ''CO_MOV_CAJA''',
'   AND  to_number(co.c005)=pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'');'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975199145354744)
,p_query_column_id=>1
,p_column_alias=>'TFP_DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Forma de Pago'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:20;">#TFP_DESCRIPCION#</span>'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975285725354745)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>3
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:20;">#VALOR#</span>'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975386559354746)
,p_query_column_id=>3
,p_column_alias=>'DETALLE_PAGO'
,p_column_display_sequence=>4
,p_column_heading=>'Detalle pago'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')',
''))
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975482204354747)
,p_query_column_id=>4
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:ELIMINA_MOVCAJA:&DEBUG.:RP:P30_SEQ_ID_MOVCAJA:#SEQ_ID#'
,p_column_linktext=>'<img src="#WORKSPACE_IMAGES#eliminar.gif" class="elimina">'
,p_column_link_attr=>'class="lock_ui_row" '
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975646097354748)
,p_query_column_id=>5
,p_column_alias=>'BIN'
,p_column_display_sequence=>5
,p_column_heading=>'Bin'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>' :P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975689380354749)
,p_query_column_id=>6
,p_column_alias=>'FECHA'
,p_column_display_sequence=>6
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>' :P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975835642354750)
,p_query_column_id=>7
,p_column_alias=>'NROREGISTRO'
,p_column_display_sequence=>7
,p_column_heading=>'Nroregistro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')',
''))
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187975919259354751)
,p_query_column_id=>8
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')',
''))
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187976050729354752)
,p_query_column_id=>9
,p_column_alias=>'RPR_ID'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')',
''))
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(187976061578354753)
,p_query_column_id=>10
,p_column_alias=>'ANULAR'
,p_column_display_sequence=>10
,p_column_heading=>'Anular'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:reverso_tarjeta:&DEBUG.:RP:P30_RPR_ID_REQ,P30_RPC_ID,P30_TIPO_IMPRESION,P30_SEQ_ID_MOVCAJA:#RPR_ID#,#RPC_ID#,3,#SEQ_ID#'
,p_column_linktext=>'#ANULAR#'
,p_column_link_attr=>'#ANULAR#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(268472356684510817)
,p_plug_name=>'Mensaje'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>2
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_when_condition=>':P30_MENSAJE IS NOT NULL;'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(268472760214510828)
,p_plug_name=>'<SPAN STYLE="font-size: 10pt">PAGO DE CLIENTE - &P30_CLI_ID.  - &P30_CLI_NOMBRE.</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(46858968446350463)
,p_plug_name=>'tfp_Cheques'
,p_parent_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>140
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(46862081868373289)
,p_plug_name=>'IngresoValor'
,p_parent_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>200
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(46883076907513722)
,p_plug_name=>'tfp_deposito_transferencia'
,p_parent_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>160
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(73604162096598946)
,p_plug_name=>'Tarjeta Credito'
,p_parent_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>150
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P30_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(43076265770640152)
,p_plug_name=>'Procesar pago'
,p_parent_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(40551575700725464)
,p_plug_name=>'Datos de la tarjeta'
,p_parent_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(43076423504640153)
,p_name=>'Lectura tarjeta'
,p_parent_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT rpc_id,',
'       rpr_fecha_respuesta,',
'       RPR_TALLA,',
'       RPR_TIPO_RESPUESTA,',
'       RPR_CODIGO_RESPUESTA,',
'       RPR_RED_ADQ_COR,',
'       RPR_RED_ADQ_DIF,',
'       RPR_NUM_TARJ_TRUN,',
'       RPR_FECHA_VENCIM,',
'       RPR_NUM_TARJ_ENCRIP,',
'       RPR_MEN_RESPUESTA,',
'       RPR_FILLER',
'  FROM car_redes_pago_resp',
' WHERE rpc_id = :P30_RPC_ID',
'   AND RPR_TIPO_RESPUESTA = ''LT'';'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43076933439640158)
,p_query_column_id=>1
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Rpc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43076959826640159)
,p_query_column_id=>2
,p_column_alias=>'RPR_FECHA_RESPUESTA'
,p_column_display_sequence=>2
,p_column_heading=>'Rpr fecha respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077145439640160)
,p_query_column_id=>3
,p_column_alias=>'RPR_TALLA'
,p_column_display_sequence=>3
,p_column_heading=>'Rpr talla'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077196388640161)
,p_query_column_id=>4
,p_column_alias=>'RPR_TIPO_RESPUESTA'
,p_column_display_sequence=>4
,p_column_heading=>'Rpr tipo respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077274957640162)
,p_query_column_id=>5
,p_column_alias=>'RPR_CODIGO_RESPUESTA'
,p_column_display_sequence=>5
,p_column_heading=>'Rpr codigo respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077431899640163)
,p_query_column_id=>6
,p_column_alias=>'RPR_RED_ADQ_COR'
,p_column_display_sequence=>6
,p_column_heading=>'Rpr red adq cor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077504367640164)
,p_query_column_id=>7
,p_column_alias=>'RPR_RED_ADQ_DIF'
,p_column_display_sequence=>7
,p_column_heading=>'Rpr red adq dif'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077614559640165)
,p_query_column_id=>8
,p_column_alias=>'RPR_NUM_TARJ_TRUN'
,p_column_display_sequence=>8
,p_column_heading=>'Rpr num tarj trun'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077713086640166)
,p_query_column_id=>9
,p_column_alias=>'RPR_FECHA_VENCIM'
,p_column_display_sequence=>9
,p_column_heading=>'Rpr fecha vencim'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077765249640167)
,p_query_column_id=>10
,p_column_alias=>'RPR_NUM_TARJ_ENCRIP'
,p_column_display_sequence=>10
,p_column_heading=>'Rpr num tarj encrip'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43077945154640168)
,p_query_column_id=>11
,p_column_alias=>'RPR_MEN_RESPUESTA'
,p_column_display_sequence=>11
,p_column_heading=>'Rpr men respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078042330640169)
,p_query_column_id=>12
,p_column_alias=>'RPR_FILLER'
,p_column_display_sequence=>12
,p_column_heading=>'Rpr filler'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(43078138868640170)
,p_name=>'Consulta Tarjeta'
,p_parent_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT res.rpc_id,',
'       res.rpr_fecha_respuesta,',
'       res.rpr_talla,',
'       res.rpr_tipo_respuesta,',
'       (SELECT tra.rpt_nombre',
'          FROM car_redes_pago_tipo_tra ttr, car_redes_pago_tra tra',
'         WHERE ttr.rpa_id = ca.rpa_id',
'           AND ttr.rpt_nombre = ''RESPUESTA''',
'           AND ttr.rpt_id = tra.rpt1_id',
'           AND tra.rpt_codigo = res.rpr_codigo_respuesta) respuesta,',
'       res.rpr_codigo_respuesta,',
'       (SELECT tra.rpt_nombre',
'          FROM car_redes_pago_tipo_tra ttr, car_redes_pago_tra tra',
'         WHERE ttr.rpa_id = ca.rpa_id',
'           AND ttr.rpt_nombre = ''COD RED ADQUIRIENTE''',
'           AND ttr.rpt_id = tra.rpt1_id',
'           AND tra.rpt_codigo = res.rpr_codigo_respuesta) red_adq_corriente,',
'       res.rpr_red_adq_cor,',
'           (SELECT tra.rpt_nombre',
'          FROM car_redes_pago_tipo_tra ttr, car_redes_pago_tra tra',
'         WHERE ttr.rpa_id = ca.rpa_id',
'           AND ttr.rpt_nombre = ''COD DIFERIDO''',
'           AND ttr.rpt_id = tra.rpt1_id',
'           AND tra.rpt_codigo = res.rpr_codigo_respuesta) red_adq_diferido,',
'       res.rpr_red_adq_dif,',
'       res.rpr_num_tarj_trun,',
'       res.rpr_fecha_vencim,',
'       res.rpr_num_tarj_encrip,',
'       res.rpr_men_respuesta',
'  FROM car_redes_pago_resp res, car_redes_pago_cab ca',
' WHERE res.rpc_id = :P30_RPC_ID',
'   AND res.rpc_id = ca.rpc_id',
'   AND res.rpr_tipo_respuesta = ''LT'';',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078190033640171)
,p_query_column_id=>1
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Rpc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078321229640172)
,p_query_column_id=>2
,p_column_alias=>'RPR_FECHA_RESPUESTA'
,p_column_display_sequence=>2
,p_column_heading=>'Rpr fecha respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078433910640173)
,p_query_column_id=>3
,p_column_alias=>'RPR_TALLA'
,p_column_display_sequence=>3
,p_column_heading=>'Rpr talla'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078483285640174)
,p_query_column_id=>4
,p_column_alias=>'RPR_TIPO_RESPUESTA'
,p_column_display_sequence=>4
,p_column_heading=>'Rpr tipo respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211594152150745)
,p_query_column_id=>5
,p_column_alias=>'RESPUESTA'
,p_column_display_sequence=>8
,p_column_heading=>'Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078599751640175)
,p_query_column_id=>6
,p_column_alias=>'RPR_CODIGO_RESPUESTA'
,p_column_display_sequence=>5
,p_column_heading=>'Rpr codigo respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211670025150746)
,p_query_column_id=>7
,p_column_alias=>'RED_ADQ_CORRIENTE'
,p_column_display_sequence=>9
,p_column_heading=>'Red adq corriente'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211776904150747)
,p_query_column_id=>8
,p_column_alias=>'RPR_RED_ADQ_COR'
,p_column_display_sequence=>10
,p_column_heading=>'Rpr red adq cor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211914430150748)
,p_query_column_id=>9
,p_column_alias=>'RED_ADQ_DIFERIDO'
,p_column_display_sequence=>11
,p_column_heading=>'Red adq diferido'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211968779150749)
,p_query_column_id=>10
,p_column_alias=>'RPR_RED_ADQ_DIF'
,p_column_display_sequence=>12
,p_column_heading=>'Rpr red adq dif'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146212071831150750)
,p_query_column_id=>11
,p_column_alias=>'RPR_NUM_TARJ_TRUN'
,p_column_display_sequence=>13
,p_column_heading=>'Rpr num tarj trun'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43173130255268228)
,p_query_column_id=>12
,p_column_alias=>'RPR_FECHA_VENCIM'
,p_column_display_sequence=>7
,p_column_heading=>'Rpr fecha vencim'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43078654001640176)
,p_query_column_id=>13
,p_column_alias=>'RPR_NUM_TARJ_ENCRIP'
,p_column_display_sequence=>6
,p_column_heading=>'Rpr num tarj encrip'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146212161225150751)
,p_query_column_id=>14
,p_column_alias=>'RPR_MEN_RESPUESTA'
,p_column_display_sequence=>14
,p_column_heading=>'Rpr men respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(95497583714324387)
,p_plug_name=>'tfp_retencion'
,p_parent_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(1491558071405022431)
,p_name=>'Retenciones'
,p_parent_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>1210
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'seq_id id,',
'to_number(c001) com_id,',
'c002 fecha_ret,',
'c003 fecha_validez,',
'c004 nro_retencion,',
'c005 nro_autorizacion,',
'c006 nro_establecimiento,',
'c007 nro_pto_emision,',
'c008 porcentaje,',
'to_number(c009) base,',
'to_number(c013) valor_calculado,',
'apex_item.text(p_idx        => 25,',
'                      p_value      => to_number(c010),',
'                      p_attributes => ''unreadonly id="f47_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c013||'',''||seq_id||'',this.value)"'',',
'                      p_size       => ''10'') valor_retencion,',
'''X'' eliminar,',
'c026 tipo',
'',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION''',
'UNION',
'select ',
'NULL id,',
'NULL com_id,',
'NULL fecha_ret,',
'NULL fecha_validez,',
'NULL nro_retencion,',
'NULL nro_autorizacion,',
'NULL nro_establecimiento,',
'NULL nro_pto_emision,',
'NULL porcentaje,',
'SUM(to_number(c009)) base,',
'sum(to_number(c013)) valor_calculado,',
'TO_CHAR(SUM(to_number(c010))) valor_retencion,',
'NULL eliminar,',
'null tipo',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491558679173022452)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>13
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491558878379022452)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ord Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491558973169022452)
,p_query_column_id=>3
,p_column_alias=>'FECHA_RET'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Ret'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559072154022453)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VALIDEZ'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Validez'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559182834022453)
,p_query_column_id=>5
,p_column_alias=>'NRO_RETENCION'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Retencion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559271194022453)
,p_query_column_id=>6
,p_column_alias=>'NRO_AUTORIZACION'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Autorizacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559362962022453)
,p_query_column_id=>7
,p_column_alias=>'NRO_ESTABLECIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Nro Establecimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559458128022453)
,p_query_column_id=>8
,p_column_alias=>'NRO_PTO_EMISION'
,p_column_display_sequence=>7
,p_column_heading=>'Nro Pto Emision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559579091022453)
,p_query_column_id=>9
,p_column_alias=>'PORCENTAJE'
,p_column_display_sequence=>8
,p_column_heading=>'Porcentaje'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559655348022453)
,p_query_column_id=>10
,p_column_alias=>'BASE'
,p_column_display_sequence=>10
,p_column_heading=>'Base'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559771902022453)
,p_query_column_id=>11
,p_column_alias=>'VALOR_CALCULADO'
,p_column_display_sequence=>11
,p_column_heading=>'Valor Calculado'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559852062022453)
,p_query_column_id=>12
,p_column_alias=>'VALOR_RETENCION'
,p_column_display_sequence=>12
,p_column_heading=>'Valor Retencion'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1491559981067022453)
,p_query_column_id=>13
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>14
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:ELIMINA_RET:&DEBUG.:RP:P30_SEQ_ID_RET:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15332691147743409454)
,p_query_column_id=>14
,p_column_alias=>'TIPO'
,p_column_display_sequence=>9
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(55050476500427206031)
,p_name=>'Bono Hogar del Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_sequence=>20
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT col.c001 valor_bono, col.c002 aplicar',
'  FROM apex_collections col',
' WHERE col.collection_name = ''COL_BONOS_APLICAR'''))
,p_display_when_condition=>'SELECT NULL FROM apex_collections col WHERE col.collection_name =''COL_BONOS_APLICAR'''
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(55050476616688206032)
,p_query_column_id=>1
,p_column_alias=>'VALOR_BONO'
,p_column_display_sequence=>1
,p_column_heading=>'Valor bono'
,p_use_as_row_header=>'N'
,p_column_css_style=>'color:BLUE; font-size:24px;font-weight: bold;'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(55050476691792206033)
,p_query_column_id=>2
,p_column_alias=>'APLICAR'
,p_column_display_sequence=>2
,p_column_heading=>'Aplicar'
,p_use_as_row_header=>'N'
,p_column_css_style=>'color:BLUE; font-size:20px;font-weight: bold;'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:limpia:&DEBUG.:RP:P30_GCA_ID,P30_VALOR_BONO:#APLICAR#,#VALOR_BONO#'
,p_column_linktext=>'Aplicar Bono Hogar'
,p_column_link_attr=>'class="lock_ui_row" '
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(268475383156510844)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>52
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT to_number(c006) valor',
'  FROM apex_collections co',
' WHERE collection_name = ''CO_MOV_CAJA''',
'   AND  to_number(co.c005)=pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'');'))
,p_display_condition_type=>'NOT_EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268475579287510846)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_SEQ_ID,P30_TFP_ID,P30_EDE_ID,P30_PLAN,P30_MCD_VALOR_MOVIMIENTO,P30_CRE_TITULAR_CUENTA,P30_CRE_NRO_CUENTA,P30_CRE_NRO_CHEQUE,P30_CRE_NRO_PIN,P30_TJ_NUMERO,P30_MCD_NRO_AUT_REF,P30_TJ_NUMERO_VOUCHER,P30_LOTE,P30_M'
||'CD_NRO_COMPROBANTE,P30_RAP_NRO_RETENCION,P30_SALDO_RETENCION,P30_RAP_COM_ID,P30_RAP_FECHA,P30_RAP_FECHA_VALIDEZ,P30_RAP_NRO_AUTORIZACION,P30_MODIFICACION,P30_RAP_NRO_ESTABL_RET,P30_RAP_NRO_PEMISION_RET,P30_PRE_ID:#COL03#,#COL04#,#COL05#,#COL05#,#COL3'
||'0#,#COL11#,#COL12#,#COL13#,#COL14#,#COL15#,#COL16#,#COL17#,#COL18#,#COL19#,#COL20#,#COL21#,#COL22#,#COL23#,#COL24#,#COL25#,S,#COL26#,#COL27#,#COL28#'
,p_column_linktext=>'Editar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268475659374510846)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:eliminar:&DEBUG.::P30_SEQ_ID:#COL03#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268475754961510846)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268475863215510846)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268475966410510846)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476069292510846)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476159495510846)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476259403510846)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476356427510846)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476480199510846)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476580123510846)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476682513510846)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476777472510846)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476857925510846)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268476972043510846)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477060474510846)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477173525510847)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477283924510847)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477381015510847)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477452822510847)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477575233510847)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_cattributes_element=>'style="color:RED;"'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477655095510847)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477756311510847)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477871513510847)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268477952055510847)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268478058486510847)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268478175642510847)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268478276391510847)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268478375530510847)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268478464070510847)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(268478579909510847)
,p_plug_name=>'Datos Guardar Factura desde Orden'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(272621973545073528)
,p_name=>'Dividendos a pagar'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001   cli_id,',
'       c002   com_id,',
'       c003   com_numero,',
'       c004   nro_ven,',
'       c005   div_id,',
'       c006   emp_id,',
'       c007   cxc_id,',
'       c008   ttr_id,',
'       c009   fecha_ven,',
'       /*       d.div_saldo_interes_mora,',
'       d.div_int_mora_condonado,',
'       d.div_gasto_cobr_generado,',
'       d.div_gasto_cob_condonado,*/',
'       c015 saldo_cuota,',
'       c016 valor_pagar',
'',
'  FROM apex_collections, car_dividendos d',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'   AND c005 = d.div_id',
''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(272621973545073528)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63513562449529674)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629057447106793)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Cli Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629176643106796)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Com Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629265421106796)
,p_query_column_id=>4
,p_column_alias=>'COM_NUMERO'
,p_column_display_sequence=>4
,p_column_heading=>'Com Numero'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629383925106796)
,p_query_column_id=>5
,p_column_alias=>'NRO_VEN'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Ven'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629477801106796)
,p_query_column_id=>6
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629572793106796)
,p_query_column_id=>7
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629683865106796)
,p_query_column_id=>8
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629783048106796)
,p_query_column_id=>9
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>9
,p_column_heading=>'Ttr Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272629867237106796)
,p_query_column_id=>10
,p_column_alias=>'FECHA_VEN'
,p_column_display_sequence=>10
,p_column_heading=>'Fecha Ven'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272630472210106796)
,p_query_column_id=>11
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>11
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(272630563175106796)
,p_query_column_id=>12
,p_column_alias=>'VALOR_PAGAR'
,p_column_display_sequence=>12
,p_column_heading=>'Valor Pagar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(4879834173473981002)
,p_plug_name=>'MENSAJE CUOTAS GRATIS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>590
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'pq_asdm_promociones.fn_promo_cuotas_gratis is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(6241726860950626750)
,p_plug_name=>'PROMOCIONES_APLICABLES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'row returned',
'',
'',
'select ',
'A.POR_ID,',
'A.POR_ID POR_ID_DISPLAY,',
'A.ORD_ID,',
'A.PTA_ID,',
'A.PRO_ID,',
'A.EMP_ID,',
'A.POR_ESTADO_REGISTROS,',
'A.PER_ID,',
'A.POR_NO_ID_BENEF,',
'A.PTA_NOMBRE,',
'A.PTA_SELECCIONADO,',
'B.PRO_DESCRIPCION',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p30_ord_id'))
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(46749483434572957)
,p_name=>'Promociones Aplicables'
,p_parent_plug_id=>wwv_flow_imp.id(6241726860950626750)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="font-size:12;font-family:Arial Narrow"'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_TABFORM'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.POR_ID,',
'A.POR_ID POR_ID_DISPLAY,',
'A.ORD_ID,',
'A.PTA_ID,',
'A.PRO_ID,',
'A.EMP_ID,',
'A.POR_ESTADO_REGISTROS,',
'A.PER_ID,',
'A.POR_NO_ID_BENEF,',
'A.PTA_NOMBRE,',
'A.PTA_SELECCIONADO,',
'B.PRO_DESCRIPCION,',
'A.ITE_SKU_ID,',
'C.ITE_DESCRIPCION_LARGA,',
'A.POR_PUNTOS_ACUMULAR',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B,',
'     ASDM_E.INV_ITEMS C',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p30_ord_id',
'AND c.ite_sku_id(+) = a.ite_sku_id'))
,p_display_when_condition=>'NVL(:P30_TERMINO_VENTA,11) <> 11'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
,p_comment=>'--'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46754058219572993)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46749765247572970)
,p_query_column_id=>2
,p_column_alias=>'POR_ID'
,p_column_display_sequence=>9
,p_column_heading=>'Por Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46749881835572976)
,p_query_column_id=>3
,p_column_alias=>'POR_ID_DISPLAY'
,p_column_display_sequence=>10
,p_column_heading=>'Por Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_ID_DISPLAY'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46749960641572976)
,p_query_column_id=>4
,p_column_alias=>'ORD_ID'
,p_column_display_sequence=>15
,p_column_heading=>'Orden Venta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'ORD_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750054762572976)
,p_query_column_id=>5
,p_column_alias=>'PTA_ID'
,p_column_display_sequence=>11
,p_column_heading=>'Pta Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PTA_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750165268572976)
,p_query_column_id=>6
,p_column_alias=>'PRO_ID'
,p_column_display_sequence=>12
,p_column_heading=>'Pro Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PRO_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750266857572976)
,p_query_column_id=>7
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>13
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'EMP_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750364560572976)
,p_query_column_id=>8
,p_column_alias=>'POR_ESTADO_REGISTROS'
,p_column_display_sequence=>14
,p_column_heading=>'Por Estado Registros'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_ESTADO_REGISTROS'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750461279572976)
,p_query_column_id=>9
,p_column_alias=>'PER_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Referidor'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'POPUPKEY'
,p_named_lov=>wwv_flow_imp.id(6241387265836580872)
,p_lov_show_nulls=>'YES'
,p_column_width=>65
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PER_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750551797572976)
,p_query_column_id=>10
,p_column_alias=>'POR_NO_ID_BENEF'
,p_column_display_sequence=>7
,p_column_heading=>'ID.Beneficiario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_NO_ID_BENEF'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750660326572976)
,p_query_column_id=>11
,p_column_alias=>'PTA_NOMBRE'
,p_column_display_sequence=>3
,p_column_heading=>unistr('Opciones de Aplicaci\00C3\00B3n')
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PTA_NOMBRE'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46750751872572976)
,p_query_column_id=>12
,p_column_alias=>'PTA_SELECCIONADO'
,p_column_display_sequence=>8
,p_column_heading=>'Aplica?'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'RADIOGROUP_FROM_LOV'
,p_named_lov=>wwv_flow_imp.id(50210174501447921)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'NO'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_attribute_01=>'2'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PTA_SELECCIONADO'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46821663311948214)
,p_query_column_id=>13
,p_column_alias=>'PRO_DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Promocion'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(11419128554711354642)
,p_query_column_id=>14
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>16
,p_column_heading=>'Ite Sku Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(11419128869462354650)
,p_query_column_id=>15
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>4
,p_column_heading=>'Ite Descripcion Larga'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(11419128961056354650)
,p_query_column_id=>16
,p_column_alias=>'POR_PUNTOS_ACUMULAR'
,p_column_display_sequence=>5
,p_column_heading=>'Por Puntos Acumular'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7782320775109544065)
,p_plug_name=>'borrar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>1200
,p_plug_display_point=>'REGION_POSITION_05'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(15446032758079729330)
,p_plug_name=>'Documento Relacionado'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270527564230046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>':P30_POLITICA_VENTA IN(48,33) AND :P30_FACTURAR = ''S'' /*pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pol_id_48''),33)*/'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(18131494266459778749)
,p_name=>'PRUEBA_JA'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>580
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM APEX_COLLECTIONS',
'WHERE COLLECTION_NAME = ''CO_MOV_CAJA'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131516679707779766)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131517859892779815)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131517955932779815)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518081634779815)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518168285779815)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518269671779815)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518360175779815)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518468424779815)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518557310779815)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518666996779815)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518759060779818)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518873255779818)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131518980091779818)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519082326779818)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519165576779818)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519257927779818)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519378710779818)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519458080779818)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519552347779818)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519669773779818)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519766301779818)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519854934779818)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131519974134779818)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520060150779818)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520161051779818)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520257038779818)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520371437779818)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520469333779818)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520573502779818)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520679624779818)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131520770951779818)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521280461779835)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521351944779835)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521453706779835)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521573259779835)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521674001779835)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521767482779835)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521860287779835)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131521981389779835)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522076891779835)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522153725779835)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522261074779835)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522352121779835)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522482475779835)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522570835779835)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522658566779835)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522759840779835)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522861378779835)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131522981182779835)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523061913779835)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523161601779835)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523270494779835)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523375279779835)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523470489779835)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523577497779835)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523657655779835)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523759537779835)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523874182779835)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131523976685779835)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524057273779835)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524156374779835)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524252969779835)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524355478779835)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524479620779836)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524554210779836)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(18131524673076779836)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(38124736606148203047)
,p_name=>'Garant&iacute;as -Bono/s'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(GAR) GARANTIA_TOTAL,',
'               SUM(BONO)  bh_total,',
'               (SUM(GAR) + SUM(BONO)) total_certificado, tipo, cantidad',
'          FROM (',
'                SELECT c.c001 ite_sku_principal,',
'       c.c002 tre_id,',
'       c.c003 det_item_adicional,',
'       c.c004 det_item_principal,',
'       c.c005 var_id,',
'       c.c006 cantidad,',
'       c.c007 GAR,',
'       c.c008 BONO,',
'       c.c009 tipo',
'  FROM apex_collections c',
' WHERE c.collection_name = ''COLL_GARHP'')',
'         GROUP BY cantidad, tipo',
''))
,p_display_when_condition=>':P30_BONO_HPROTEGIDO > 0 AND :P30_BONO_HPROTEGIDO IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38124736727588203048)
,p_query_column_id=>1
,p_column_alias=>'GARANTIA_TOTAL'
,p_column_display_sequence=>1
,p_column_heading=>'Garantia total'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38124736779988203049)
,p_query_column_id=>2
,p_column_alias=>'BH_TOTAL'
,p_column_display_sequence=>2
,p_column_heading=>'Bh total'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38124736901200203050)
,p_query_column_id=>3
,p_column_alias=>'TOTAL_CERTIFICADO'
,p_column_display_sequence=>3
,p_column_heading=>'Total certificado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38124736984310203051)
,p_query_column_id=>4
,p_column_alias=>'TIPO'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38124737111125203052)
,p_query_column_id=>5
,p_column_alias=>'CANTIDAD'
,p_column_display_sequence=>5
,p_column_heading=>'Cantidad'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38124737422718203055)
,p_plug_name=>'Garant&iacute;as'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_07'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(101128809688690289828)
,p_name=>'Identificadores'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>570
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_2'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (SELECT ti.tid_descripcion',
'          FROM inv_tipos_identificador ti',
'         WHERE ti.tid_id = a.tid_id) primario,',
'       a.ebs_valor_identificador valor_primario,',
'       c.tid_descripcion secundario,',
'       b.ise_valor_identificador valor_secundario',
'  FROM inv_items_estado_bodega_serie a,',
'       inv_iden_secundarios_ebs      b,',
'       inv_tipos_identificador       c',
' WHERE a.ebs_id = b.ebs_id',
'   AND a.ebs_id = :p20_ebs_id',
'   AND c.tid_id = b.tid_id;  '))
,p_display_when_condition=>':P20_ORD_TIPO = ''VENMO'' AND :p20_ebs_id IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_row_count_max=>500
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(101128809949284289830)
,p_query_column_id=>1
,p_column_alias=>'PRIMARIO'
,p_column_display_sequence=>1
,p_column_heading=>'Primario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(101128809964998289831)
,p_query_column_id=>2
,p_column_alias=>'VALOR_PRIMARIO'
,p_column_display_sequence=>2
,p_column_heading=>'Valor primario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(101128810100466289832)
,p_query_column_id=>3
,p_column_alias=>'SECUNDARIO'
,p_column_display_sequence=>3
,p_column_heading=>'Secundario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(101128810193535289833)
,p_query_column_id=>4
,p_column_alias=>'VALOR_SECUNDARIO'
,p_column_display_sequence=>4
,p_column_heading=>'Valor secundario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(199707358876937957942)
,p_plug_name=>'Forma de Credito'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_07'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(43175688017268254)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(43076423504640153)
,p_button_name=>'BTN_LECTURA_TARJETA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'CONSULTAR'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(43175787965268255)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(43078138868640170)
,p_button_name=>'BTN_CONSULTA_TARJETA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'CONSULTAR'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133254464565604453)
,p_button_sequence=>130
,p_button_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_button_name=>'BTN_VER_SALDO_PTJ'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_image_alt=>'VER'
,p_button_redirect_url=>'f?p=&APP_ID.:159:&SESSION.::&DEBUG.:RP:P159_CLI_ID,P159_UGE_ID:&P30_CLI_ID.,&F_UGE_ID.'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ca.rpa_valor_pag',
'          FROM car_redes_pago_cab  ca,',
'               asdm_clientes       cl,',
'               asdm_personas       pe,',
'               car_redes_pago_req  r,',
'               car_redes_pago_resp rs',
'         WHERE ca.rpc_id = rs.rpc_id',
'           AND ca.cli_id = cl.cli_id',
'           AND cl.per_id = pe.per_id',
'           AND ca.rpc_id = r.rpc_id',
'           AND rs.rpr_id_req = r.rpr_id',
'           AND ca.rpc_estado != ''CERRADO''',
'           AND r.rpr_estado_pago  IN (''PROCESADO'')',
'           AND rs.rpr_estado_resp  IN (''PROCESADO'')',
'           AND ca.cli_id = :p30_cli_id',
'           AND ca.uge_id = :f_uge_id',
'           AND rs.rpr_tipo_respuesta = ''PP''',
'           AND r.rpr_tipo_requerimiento = ''PP'';'))
,p_button_condition_type=>'EXISTS'
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(142817073835230236)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(142816855544230235)
,p_button_name=>'IDENTIFICADORES'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Identificadores'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:92:&SESSION.:identificadores:&DEBUG.::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_EMP_LOGO,F_EMP_FONDO,F_POPUP,F_APP_ID_REGRESA,F_PAG_ID_REGRESA,P92_TTR_ID,P92_LCM_ID,P92_BPR_ID_IDEN:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_EMP_LOGO.,&F_EMP_FONDO.,N,&APP_ID.,&APP_PAGE_ID.,,,&P30_BPR_ID_IDEN.'
,p_button_condition=>'TO_NUMBER(:P30_NRO_REG_ITEM_IDEN) > 0'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(46751174508572982)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Insertar Registro'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(279841965747865685)
,p_button_sequence=>500
,p_button_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_button_name=>'CARGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:30:&SESSION.:cargar_tfp:&DEBUG.:::'
,p_button_condition=>'to_number(:P30_VALOR_TOTAL_PAGOS) > to_number(:P30_SUM_PAGOS)'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(287054276940984727)
,p_button_sequence=>600
,p_button_plug_id=>wwv_flow_imp.id(104770569835340942)
,p_button_name=>'Graba_fac_pag'
,p_button_static_id=>'pagar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'&P30_LABEL_BOTON.'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--(:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
'/*(',
'(:P30_POLITICA_VENTA <> pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') OR :P30_POLITICA_VENTA IS NULL) AND (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
') OR',
'(',
'(:P30_POLITICA_VENTA = pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') AND :P30_FACTURA_YN_R = ''S'') AND (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
')*/',
'',
'(:P30_HAY_PROMOCION = ''S'' AND :P30_FACTURA_YN = ''S'' AND ((',
'(:P30_POLITICA_VENTA <> pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') OR :P30_POLITICA_VENTA IS NULL) AND (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
') OR',
'(',
'(:P30_POLITICA_VENTA = pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') AND :P30_FACTURA_YN_R = ''S'') AND (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
'))) ',
'',
'OR',
'',
'(NVL(:P30_HAY_PROMOCION,''N'') <> ''S'' AND ((',
'(:P30_POLITICA_VENTA <> pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') OR :P30_POLITICA_VENTA IS NULL) AND (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
') OR',
'(',
'(:P30_POLITICA_VENTA = pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') AND :P30_FACTURA_YN_R = ''S'') AND (:P30_VALOR_TOTAL_PAGOS = :P30_SUM_PAGOS and ((:P30_VALOR_TOTAL_PAGOS<>0 and :P30_FACTURAR <> ''S'') or :P30_FACTURAR = ''S''))',
')))'))
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_cattributes=>'style="font-size:16;font-family:Tahoma;color:BLACK;font-weight:bold"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51035678574731997)
,p_button_sequence=>640
,p_button_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_button_name=>'IDEN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Iden'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38534968185948222)
,p_button_sequence=>650
,p_button_plug_id=>wwv_flow_imp.id(38522767998834628)
,p_button_name=>'BT_FILTRAR_PERSONAS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Filtrar Beneficiario'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(46750865960572982)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.:::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(46751068032572982)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(46750954162572982)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_button_name=>'SUBMIT'
,p_button_static_id=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Grabar'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_cattributes=>'style="font-size:16;font-family:Tahoma;color:BLUE;font-weight:bold"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42350273602772160)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Pago de Cuota'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:&P30_PAGINA_REGRESO.:&SESSION.::&DEBUG.:30::'
,p_button_condition=>':P30_FACTURAR = ''N'' and NVL(:P30_ES_PRECANCELACION,''N'') <> ''S'''
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(119045075468441477)
,p_button_sequence=>610
,p_button_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Pago de Cuota con Retencion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:64:&SESSION.::&DEBUG.:30::'
,p_button_condition=>'P30_FACTURAR'
,p_button_condition2=>'R'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(119369860005137776)
,p_button_sequence=>620
,p_button_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_button_name=>'REGRESAR'
,p_button_static_id=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Facturacion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:20:&SESSION.:orden_venta:&DEBUG.:30:P20_ORDEN_VENTA,P20_IDENTIFICACION,P20_POL_ID,P20_TVE_ID,P20_CLI_ID,P20_ORD_TIPO,P20_COT_ID:&P30_ORD_ID.,&P30_CLI_IDENTIFICACION.,&P30_POLITICA_VENTA.,&P30_TVE_ID.,&P30_CLI_ID.,&P30_ORD_TIPO.,&P30_COT_ID.'
,p_button_condition=>'P30_FACTURAR'
,p_button_condition2=>'S'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_button_cattributes=>'class="redirect" style="font-size:16;font-family:Tahoma;color:balck;font-weight:bold"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133125780631411383)
,p_button_sequence=>630
,p_button_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Precancelacion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:49:&SESSION.::&DEBUG.:30::'
,p_button_condition=>'P30_ES_PRECANCELACION'
,p_button_condition2=>'S'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(268486253879510885)
,p_branch_name=>'Go To Page 30'
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.:Graba_fac_pag:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(287054276940984727)
,p_branch_sequence=>10
,p_branch_comment=>'Created 01-MAR-2010 11:11 by ONARANJO'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(287054578108984731)
,p_branch_name=>'Go To Page 30'
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(279841965747865685)
,p_branch_sequence=>30
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(42352264214816754)
,p_branch_name=>'Go To Page 6'
,p_branch_action=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:30::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(42350273602772160)
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 06-FEB-2011 20:05 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(54773373008449545)
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.:RP::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>50
,p_branch_condition_type=>'NEVER'
,p_branch_comment=>'Created 28-SEP-2010 11:35 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(698184628239101866)
,p_branch_name=>'11111111'
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>60
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(35506564380657083)
,p_name=>'P30_VUELTO'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Vuelto/Cambio Efectivo'
,p_pre_element_text=>'<b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" style="font-size:16"'
,p_tag_attributes=>'style="font-size:26"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number;',
' ',
'BEGIN',
'',
'',
' select count(*) into ln_cuenta from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                         ''cv_coleccion_mov_caja'');',
'',
' if ln_cuenta >0  and :P30_VALOR_TOTAL_PAGOS > 0 and :P30_FACTURAR <> ''R'' then',
'     return true;',
' else ',
'     return false;',
' end if;',
'',
'END;'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'FUNCTION_BODY'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(35754679909332463)
,p_name=>'P30_PRO_ID'
,p_item_sequence=>620
,p_item_plug_id=>wwv_flow_imp.id(11419784653677403074)
,p_prompt=>'Pro Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36329476522996873)
,p_name=>'P30_PER_ID'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_promociones_sql.fn_cliente_persona(:p30_cli_id,:f_emp_id, ''P'');',
''))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36513968921913080)
,p_name=>'P30_REQ'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>':request'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37953068521089509)
,p_name=>'P30_PUNTAJE_FIJO'
,p_item_sequence=>650
,p_item_plug_id=>wwv_flow_imp.id(11419784653677403074)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Puntaje Fijo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.pta_id',
'      from ven_bitacora_promociones a, ven_promociones b, ven_promos_tipos_aplica c',
'      where a.per_id=:P30_per_id',
'      and a.pro_id=b.pro_id',
'       and c.pro_id=a.pro_id',
'       and c.pta_id=a.pta_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38449259885604554)
,p_name=>'P30_TERMINO_VENTA'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>':p30_tve_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Termino Venta'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38520969814816231)
,p_name=>'P30_PER_ID_PUNTOS'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(38522767998834628)
,p_prompt=>'Cliente con Puntaje'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_PERSONAS_PROMOS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   lv_lov varchar2(5000);',
'BEGIN',
'   lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_PERSONAS_PROMOS'');',
'   lv_lov := lv_lov || '' AND a.emp_id = '' || :F_EMP_ID;',
'   return (lv_lov);',
'END;',
''))
,p_lov_display_null=>'YES'
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit('''');"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38538579025083831)
,p_name=>'P30_PTA_ID'
,p_item_sequence=>660
,p_item_plug_id=>wwv_flow_imp.id(11419784653677403074)
,p_prompt=>'Pta Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39356151475195362)
,p_name=>'P30_LABEL_BOTON'
,p_item_sequence=>202
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Label Boto'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE WHEN ',
'   :P30_FACTURAR = ''N'' or :P30_FACTURAR = ''R'' or :P30_FACTURAR = ''M'' THEN ''PAGAR''',
'WHEN ',
'   :P30_FACTURAR = ''S'' THEN ''FACTURAR''',
'ELSE ''ERROR'' END FROM DUAL'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39689476338134873)
,p_name=>'P30_TOTAL_FACTURA'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    vor_valor_variable',
'FROM      ven_var_ordenes',
'WHERE     ord_id = :p30_ord_id',
'AND       var_id = 3'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39694881729325597)
,p_name=>'P30_PUNTOS_CANCELAR'
,p_item_sequence=>121
,p_item_plug_id=>wwv_flow_imp.id(11419784653677403074)
,p_prompt=>'Puntos Cancelar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:12;color:blue;font-family:Arial"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P30_POLITICA_VENTA <> 112'
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>unistr('Ingrese la cantidad de puntos que el cliente est\00E1 pagando.')
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39744570318823757)
,p_name=>'P30_POLITICA_VENTA'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    pol_id',
'FROM      ven_ordenes',
'WHERE     ord_id = :p30_ord_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39974177610993974)
,p_name=>'P30_PER_ID_PROMO'
,p_item_sequence=>700
,p_item_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_prompt=>'Per Id Promo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.PER_ID',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p30_ord_id',
'and a.pta_seleccionado=''S'';'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40551469638725463)
,p_name=>'P30_TARJETA_EMI'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Emisor Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')  '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40551730383725465)
,p_name=>'P30_TJ_NUMERO_1'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40551842673725466)
,p_name=>'P30_TJ_NUMERO_VOUCHER_1'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40551938098725467)
,p_name=>'P30_TJ_RECAP_1'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Recap'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40552035414725468)
,p_name=>'P30_MCD_NRO_AUT_REF_1'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Autorizaci&oacute;n:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(40969871821020313)
,p_name=>'P30_ALERTA_INTERES_MORA'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>'''El valor de Interes de Mora que se va a cobrar es: '' || :P30_INT_MORA_COB'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:16px; color: red;"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'to_number(:P30_INT_MORA_COB) > 0 and :F_SEG_ID = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42348970093742835)
,p_name=>'P30_PRECIONEENTER'
,p_item_sequence=>17
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Presione Enter para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42360074801009024)
,p_name=>'P30_SALDO_ANTICIPOS'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Anticipos'
,p_source=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P30_CLI_ID,:f_uge_id))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P30_CLI_ID,:f_uge_id))'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43173545457268232)
,p_name=>'P30_TIPO_PAGO_TJ'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_item_default=>'M'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
unistr('  FROM (SELECT ''Autom\00E1tico'' d, ''A'' r'),
'          FROM dual',
'         WHERE (SELECT COUNT(pue.rpp_id)',
'                  FROM car_redes_pago_pue pue',
'                 WHERE pue.uge_id = :F_uge_id',
'                 AND pue.rpp_estado_registro = 0) > 0',
'        UNION',
'        SELECT ''Manual'' d, ''M'' r',
'          FROM dual)'))
,p_label_alignment=>'RIGHT-TOP'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43173578503268233)
,p_name=>'P30_RPC_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43173705648268234)
,p_name=>'P30_PROCESO_TJ'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_item_default=>'PP'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'  FROM (SELECT ''LECTURA DE TARJETA'' D, ''LT'' R',
'          FROM DUAL',
'        UNION',
'        SELECT ''CONSULTA DE TARJETA'' D, ''CT'' R',
'          FROM DUAL',
'        UNION',
'        SELECT ''PROCESAR EL PAGO'' D, ''PP'' R',
'          FROM DUAL)'))
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43174008592268237)
,p_name=>'P30_TRAMA_RESP'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_prompt=>'Resp'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(46882667557511068)
,p_name=>'P30_MCD_NRO_COMPROBANTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(46883076907513722)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Comprobante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>13
,p_cMaxlength=>12
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51424373230592967)
,p_name=>'P30_PUNTOS_USADOS'
,p_item_sequence=>790
,p_item_plug_id=>wwv_flow_imp.id(11419784653677403074)
,p_prompt=>'Puntos Usados'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    vor_valor_variable',
'FROM      ven_var_ordenes',
'WHERE     ord_id = :p30_ord_id',
'AND       var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total'')',
'AND       :P30_PER_ID_PUNTOS IS NOT NULL',
'AND       :P30_POLITICA_VENTA IN(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_pol_id_48''))',
'AND       :P30_PUNTOS_CANCELAR > 0',
'',
'UNION',
'',
'SELECT    TO_NUMBER(:P30_MCD_VALOR_MOVIMIENTO)',
'FROM      DUAL',
'WHERE     :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'')',
'--AND       :P30_PUNTOS_CANCELAR > 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55041484735765137)
,p_name=>'P30_FORMA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_prompt=>'Forma:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.ede_descripcion, d.ede_id',
'  FROM asdm_entidades_destinos d',
' WHERE d.ede_id_padre = :p30_tipo',
'   AND d.ede_tipo = ''PTJ''',
'   AND d.ede_estado_registro = 0;'))
,p_lov_cascade_parent_items=>'P30_TIPO'
,p_ajax_items_to_submit=>'P30_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p30_tarjeta',
'and c.emp_id = :f_emp_id'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(59060070739672882)
,p_name=>'P30_TVE_ID'
,p_item_sequence=>222
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(59122682550995772)
,p_name=>'P30_ODE_ID'
,p_item_sequence=>232
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_use_cache_before_default=>'NO'
,p_prompt=>'ODE_ID'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ode_id',
'FROM   inv_ordenes_despacho',
'WHERE  ord_id = :P30_ORD_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62538467080870308)
,p_name=>'P30_ES_PRECANCELACION'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(68673079875084647)
,p_name=>'P30_SALDO_PROMO'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Saldo Total Promoci\00C3\00B3n')
,p_source=>'to_number(pq_asdm_promociones.fn_obtener_saldo_actual(:P30_CLI_ID,:F_EMP_ID,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),NULL,:P0_Error))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_help_text=>'Saldo total de promociones'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(68713878611368107)
,p_name=>'P30_SALDO_MAX_APLICAR'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Maximo a Aplicar'
,p_pre_element_text=>'<b>'
,p_source=>'pq_asdm_promociones.fn_obtener_saldo_maximo_aplica(:f_emp_id,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),:P30_cli_id,:P30_ord_id, :P0_error)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') ',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(73716960195986244)
,p_name=>'P30_TJ_NUMERO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(73717168852988784)
,p_name=>'P30_TJ_NUMERO_VOUCHER'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(73717782705992728)
,p_name=>'P30_TJ_TITULAR_Y'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_prompt=>'Titular:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(73718760326995700)
,p_name=>'P30_TJ_ENTIDAD'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_prompt=>'Entidad'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_TARJETA_CREDITO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    ',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_PLAN_TARJETA_CORRIENTE'');',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(74251770974234885)
,p_name=>'P30_TJ_RECAP'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Recap'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(90427277094161177)
,p_name=>'P30_BANCOS'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_prompt=>'Banco:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.ede_descripcion l, a.ede_id r',
'  FROM asdm_entidades_destinos a',
' WHERE a.ede_tipo = ''EFI''',
'   AND a.ede_estado_registro = 0',
'   AND a.ede_es_induccion IS NULL',
'   AND a.emp_id = :F_EMP_ID',
'   AND EXISTS (SELECT NULL',
'          FROM asdm_entidades_destinos ed',
'         WHERE ed.ede_id_padre = a.ede_id',
'           AND ed.ede_tipo = ''TJ''',
'           AND ed.ede_estado_registro = 0',
'           AND ed.emp_id = :F_EMP_ID)'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>'SELECT a.ede_descripcion,a.ede_id FROM asdm_entidades_destinos a WHERE a.ede_tipo=''EFI'' and a.emp_id = :f_emp_id;'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(90427466706161182)
,p_name=>'P30_TARJETA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_prompt=>'Tarjeta:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.ede_descripcion d, d.ede_id r',
'  FROM asdm_entidades_destinos d',
' WHERE d.ede_id_padre = :p30_bancos',
'   AND d.ede_tipo = ''TJ''',
'   AND d.ede_estado_registro = 0',
'   AND d.emp_id = :f_emp_id;'))
,p_lov_cascade_parent_items=>'P30_BANCOS'
,p_ajax_items_to_submit=>'P30_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id FROM asdm_entidades_destinos b WHERE b.ede_tipo=''TJ''',
'AND b.ede_id_padre=:p30_bancos',
'and b.emp_id = :f_emp_id'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(90427652483161183)
,p_name=>'P30_TIPO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_prompt=>'Tipo:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion, c.ede_id',
'  FROM asdm_entidades_destinos c',
' WHERE c.ede_tipo = ''PTJ''',
'   AND c.ede_estado_registro = 0',
'   AND c.ede_id_padre = :p30_tarjeta',
'   AND c.emp_id = :f_emp_id;'))
,p_lov_cascade_parent_items=>'P30_TARJETA'
,p_ajax_items_to_submit=>'P30_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p30_tarjeta',
'and c.emp_id = :f_emp_id'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(90427883002161185)
,p_name=>'P30_PLAN'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_prompt=>'Plan:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT rpad(d.ede_plazo_plan, 2, '' '') || '' MESES, Gracia: '' ||',
unistr('       d.ede_meses_gracia || '' Comisi\00F3n: '' || d.ede_comision ||'),
unistr('       '' Inter\00E9s: '' || d.ede_interes d,'),
'       d.ede_id',
'  FROM asdm_entidades_destinos d',
' WHERE d.ede_id_padre = :P30_FORMA',
'   AND d.ede_tipo = ''DPT''',
'   AND d.ede_estado_registro = 0',
' ORDER BY d.ede_plazo_plan ASC;'))
,p_lov_cascade_parent_items=>'P30_FORMA'
,p_ajax_items_to_submit=>'P30_FORMA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_descripcion, ''->'') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_tede_detalle_plan_tarjeta'')',
' and ede.emp_id = :f_emp_id',
'START WITH ede.ede_id_padre =:P30_TIPO',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre'))
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95499468439367344)
,p_name=>'P30_RAP_NRO_RETENCION'
,p_item_sequence=>262
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_AUTORIZACION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95499652986372261)
,p_name=>'P30_RAP_NRO_AUTORIZACION'
,p_item_sequence=>272
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'Nro Autorizacion:'
,p_post_element_text=>' 10 - 37 - 49 digitos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>49
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_FECHA'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95499863029375241)
,p_name=>'P30_RAP_FECHA'
,p_item_sequence=>282
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Retencion:'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95500283115380967)
,p_name=>'P30_RAP_FECHA_VALIDEZ'
,p_item_sequence=>292
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95500867316385922)
,p_name=>'P30_RAP_COM_ID'
,p_item_sequence=>302
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'Factura:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LV_VEN_COMPROBANTES_F_ND'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    lv_lov := pq_asdm_listas.fn_lov_facturas_pago_cuota(:f_emp_id);',
'',
'/*lv_lov :=kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_COMPROBANTES_F_ND_SIN_RET'');',
'lv_lov := lv_lov || ''AND vco.cli_id = :P64_CLI_ID ORDER BY vco.com_id'';*/',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Facturas Disponibles --'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p30_FACTURAR = ''N'' '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95586356689991123)
,p_name=>'P30_SALDO_RETENCION'
,p_item_sequence=>312
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_post_element_text=>'  para Anticipo de Clientes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96049169470685225)
,p_name=>'P30_LOTE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(73604162096598946)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96050175273705779)
,p_name=>'P30_MCD_NRO_AUT_REF'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Autorizaci&oacute;n:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96093078352104048)
,p_name=>'P30_MODIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Modificacion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96297867118757046)
,p_name=>'P30_RAP_NRO_ESTABL_RET'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'Nro Retencion:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_PEMISION_RET'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96298478893760489)
,p_name=>'P30_RAP_NRO_PEMISION_RET'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_RETENCION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96299157206763669)
,p_name=>'P30_PRE_ID'
,p_item_sequence=>372
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'Pre Id:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96687265571987141)
,p_name=>'P30_MCD_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(46883076907513722)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Deposito:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98456067814840068)
,p_name=>'P30_ENTRADA'
,p_item_sequence=>382
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Entrada'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--Utilizo solo cuando la venta se ha realizado con una tarjeta de credito.',
'--aqui cargo el valor de la entrada que cancelo a parte de la tarjeta de credito'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98863363459516834)
,p_name=>'P30_COMISION'
,p_item_sequence=>392
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Comision'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(98927961108471634)
,p_name=>'P30_SALDO_PAGO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Pago'
,p_pre_element_text=>'<b>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*select nvl(:p30_valor_total_pagos,0) - (to_number(sum(nvl(c006,0))) + TO_NUMBER(sum(nvl(c018,0)))) from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')*/',
'',
'select nvl(:p30_valor_total_pagos,0) - to_number(sum(nvl(c006,0)))  from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(103076953191182917)
,p_name=>'P30_TITULAR_TC'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_prompt=>'Titular Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104790957686460448)
,p_name=>'P30_COT_ID'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104793981712476792)
,p_name=>'P30_INT_MORA_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(104657655926516000)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104794456563479035)
,p_name=>'P30_GTOS_COB_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(104756753119279351)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104980762064278160)
,p_name=>'P30_F_UGE_ID'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Uge Id'
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104982358385286510)
,p_name=>'P30_F_USER_ID'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F User Id'
,p_source=>'F_USER_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104982576047291693)
,p_name=>'P30_F_EMP_ID'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Emp Id'
,p_source=>'F_EMP_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(118861153036240178)
,p_name=>'P30_NUM_FOLIO_NCREDITO'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'No. Folio N/C Inicial'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''validar_nc'' )"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') ',
'or ',
'',
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_promocion_puntaje'') ) and :P30_VALIDA_PUNTO = ''P'''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(118958153699063451)
,p_name=>'P30_SECUENCIA_ACTUAL'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(118993657958197099)
,p_name=>'P30_MENSAJE_SECUENCIA'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>unistr('La secuencia actual de la Nota de Cr\00C3\00A9dito es: &P30_SECUENCIA_ACTUAL.')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') AND :P30_NUM_FOLIO_NCREDITO IS NOT NULL',
'',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(121708554593058627)
,p_name=>'P30_SALDO_PROMO_DEV'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Total Promocion Devuelta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
' ',
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_dev_promo'') or',
' ',
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'') '))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_help_text=>'Saldo Total de Promocion Devuelta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(121723272806328775)
,p_name=>'P30_SALDO_MAX_APL_DEV'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Saldo Maximo a Aplicar'
,p_pre_element_text=>'<b>'
,p_source=>'pq_asdm_promociones.fn_obtener_saldo_max_apl_dev(:f_emp_id,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),:P30_cli_id,:P30_ord_id, :P0_error)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_dev_promo'') ',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122602668298258343)
,p_name=>'P30_MENSAJE_ANTICIPO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED">    APLICAR EL PAGO CON ANTICIPO DE CLIENTES</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'CENTER-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'to_number(:P30_SALDO_ANTICIPOS) > 0 and :P30_FACTURAR = ''N'' AND NVL(:P30_ES_PRECANCELACION,''N'') <> ''S'' and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133254362337604452)
,p_name=>'P30_SALDO_TARJETA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo de pago de Tarjeta'
,p_pre_element_text=>'<b>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_car_redes_pago.fn_retorna_saldo_tarjeta(pn_cli_id => :p30_cli_id,',
'                                                        pn_uge_id => :F_UGE_ID);'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ca.rpa_valor_pag',
'          FROM car_redes_pago_cab  ca,',
'               asdm_clientes       cl,',
'               asdm_personas       pe,',
'               car_redes_pago_req  r,',
'               car_redes_pago_resp rs',
'         WHERE ca.rpc_id = rs.rpc_id',
'           AND ca.cli_id = cl.cli_id',
'           AND cl.per_id = pe.per_id',
'           AND ca.rpc_id = r.rpc_id',
'           AND rs.rpr_id_req = r.rpr_id',
'           AND ca.rpc_estado != ''CERRADO''',
'           AND r.rpr_estado_pago  IN (''PROCESADO'')',
'           AND rs.rpr_estado_resp  IN (''PROCESADO'')',
'           AND ca.cli_id = :p30_cli_id',
'           AND ca.uge_id = :f_uge_id',
'           AND rs.rpr_tipo_respuesta = ''PP''',
'           AND r.rpr_tipo_requerimiento = ''PP'';'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133254651921604455)
,p_name=>'P30_PINPAD'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_use_cache_before_default=>'NO'
,p_prompt=>'PINPAD'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''Pinpad: ''||rpa.rpa_nombre ||''/Comercio: '' || rpp.rpp_cid  d,',
'       rpp.rpp_id r',
'  FROM car_redes_pago_pue rpp, asdm_puntos_emision pue, car_redes_pago rpa',
' WHERE rpp.pue_id = pue.pue_id',
'   AND rpp.rpa_id=rpa.rpa_id',
'   AND rpp.rpp_estado_registro = 0',
'   AND rpp.pue_id = :p0_pue_id;'))
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_NRO_PINPAD >1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133254770971604456)
,p_name=>'P30_RPR_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133254881472604457)
,p_name=>'P30_RED'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_item_default=>'2'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135561655681310659)
,p_name=>'P30_ORD_TIPO'
,p_item_sequence=>600
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_prompt=>'Ord Tipo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142809868474230214)
,p_name=>'P30_NRO_IDEN_NO_INGRESADOS'
,p_item_sequence=>524
,p_item_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Iden No Ingresados'
,p_source=>'pq_inv_movimientos_iden.fn_retorna_num_reg_col_ids(pv_error => :P0_ERROR);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142810064662230214)
,p_name=>'P30_NRO_REG_ITEM_IDEN'
,p_item_sequence=>514
,p_item_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Reg Item Iden'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos_iden.fn_retorna_num_reg_inv_det(pn_emp_id => :F_EMP_ID, ',
'                                                   pv_cadena_inicio_reg_iden => :P30_CADENA_INICIO_REG_IDEN,  ',
'                                                   pv_error  => :P0_ERROR);'))
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142810254272230215)
,p_name=>'P30_BANDERA_IDEN'
,p_item_sequence=>554
,p_item_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_prompt=>'Bandera Iden'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142810478301230215)
,p_name=>'P30_CADENA_INICIO_REG_IDEN'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cadena Inicio Reg Iden'
,p_source=>'CHR(39) || pq_constantes.fn_retorna_constante(NULL,''cv_ite_inicio_reg_nserie_ven'') || CHR(39)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142817276188230237)
,p_name=>'P30_MENSAJE2'
,p_item_sequence=>534
,p_item_plug_id=>wwv_flow_imp.id(142816855544230235)
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:17"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos_iden.fn_valida_mensaje_col_inv_iden(pn_emp_id => :F_EMP_ID, ',
'                                                       pv_cadena_inicio_reg_iden => :P30_CADENA_INICIO_REG_IDEN, ',
'                                                       pv_error => :P0_ERROR);'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(143274561272398345)
,p_name=>'P30_BPR_ID_IDEN'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_imp.id(142809657812230213)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bpr Id Iden'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos_iden.fn_retorna_bpr_id_ven_ord(pn_ord_id => :P30_ORD_ID, ',
'                                                  pv_ord_tipo => :P30_ORD_TIPO, ',
'                                                  pv_error => :P0_ERROR);'))
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(144293770047554761)
,p_name=>'P30_TIPO_IMPRESION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(40551575700725464)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(146211457522150744)
,p_name=>'P30_RPR_ID_REQ'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(146214013791150769)
,p_name=>'P30_NRO_PINPAD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(43076265770640152)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT count(1)',
'  FROM car_redes_pago_pue rpp',
' WHERE rpp.pue_id=:p0_pue_id',
'   AND rpp.rpp_estado_registro = 0 ;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155338173794518115)
,p_name=>'P30_INI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>'0'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'style="border-style:none;background-color:transparent;font-size:15px;text-align:left;color:transparent;" '
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>'style="border-style:none;background-color:transparent;font-size:1px;text-align:left;color:transparent;" '
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155338472925518118)
,p_name=>'P30_INI2'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>'0'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="border-style:none;background-color:transparent;font-size:1px;text-align:left;color:transparent;" '
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'style="border-style:none;background-color:transparent;font-size:15px;text-align:left;color:transparent;" '
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(187976172876354754)
,p_name=>'P30_SEQ_ID_MOVCAJA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(187975102089354743)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268478963526510858)
,p_name=>'P30_CLI_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268479152790510862)
,p_name=>'P30_CLI_NOMBRE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268479367018510863)
,p_name=>'P30_TFP_ID'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF NVL(:P30_SALDO_ANTICIPOS,0) = 0 THEN',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes''); ',
'END IF'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_FORMAS_PAGO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'        lv_lov := pq_ven_listas_caja.fn_lov_formas_pago',
'(',
':F_EMP_ID,',
':P0_TTR_ID,',
':P30_POLITICA_VENTA);',
'',
'return (lv_lov);',
'',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P30_VALOR_TOTAL_PAGOS > 0 and :P30_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268479762923510864)
,p_name=>'P30_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Valor Recibido:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''cargar_tfp'');" '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P30_VALOR_TOTAL_PAGOS > 0 and :P30_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Presione Enter para cargar el valor'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268479975036510864)
,p_name=>'P30_EDE_ID'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>'NULL'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Entidad Destino:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_FORMA_PAGO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'lv_lov := pq_ven_listas_caja.fn_lov_entidad_destin_tfp_pag',
'(',
':F_EMP_ID,',
':P30_TFP_ID,',
':P30_ERROR);',
'return (lv_lov);',
'END;'))
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') or ',
':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268480172007510864)
,p_name=>'P30_CRE_NRO_CHEQUE'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(46858968446350463)
,p_prompt=>'Nro Cheque:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268480377073510865)
,p_name=>'P30_CRE_NRO_CUENTA'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(46858968446350463)
,p_prompt=>'Nro Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268480578476510865)
,p_name=>'P30_CRE_NRO_PIN'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(46858968446350463)
,p_prompt=>'Nro Pin:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268480778783510866)
,p_name=>'P30_FACTURAR'
,p_item_sequence=>112
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_prompt=>'Facturar'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268480963522510866)
,p_name=>'P30_COM_ID'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268481174466510866)
,p_name=>'P30_TRX_ID'
,p_item_sequence=>132
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268481377608510866)
,p_name=>'P30_SUM_PAGOS'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(TO_NUMBER(c006)),0) suma_valor',
'  FROM apex_collections',
'WHERE collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268481766914510867)
,p_name=>'P30_ORD_ID'
,p_item_sequence=>102
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268481977547510867)
,p_name=>'P30_VALOR_TOTAL_PAGOS'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Pagar'
,p_pre_element_text=>'<b>'
,p_format_mask=>'999G999G999G999G990D00'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'return pq_ven_listas_caja.fn_ret_valor_cuota0(:P30_ORD_ID, :p30_facturar,:P30_VALOR_TOTAL_PAGOS);',
'',
'',
'end;'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268482163623510867)
,p_name=>'P30_CRE_TITULAR_CUENTA'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Titular Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>100
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268482383169510868)
,p_name=>'P30_CRE_FECHA_DEPOSITO'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(46858968446350463)
,p_prompt=>'Fecha Cheque:'
,p_source=>'to_char(sysdate, ''DD/MM/YYYY'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268482554192510868)
,p_name=>'P30_CRE_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(46858968446350463)
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268482757648510868)
,p_name=>'P30_SEQ_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Registro a modficar (seq_id) :'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P30_SEQ_ID IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268483182486510868)
,p_name=>'P30_MENSAJE'
,p_item_sequence=>92
,p_item_plug_id=>wwv_flow_imp.id(268472356684510817)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268483374927510869)
,p_name=>'P30_CLI_IDENTIFICACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>unistr('Identificaci\00C3\00B3n Cliente')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283185270188066337)
,p_name=>'P30_ERROR'
,p_item_sequence=>142
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(533825961610143847)
,p_name=>'P30_ES_POLITICA_GOBIERNO'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_consultas.fn_valida_poltica_gobierno(pn_pol_id => :P30_POLITICA_VENTA,',
'                                                         pn_emp_id => :f_emp_id);'))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1516137363024170487)
,p_name=>'P30_RET_IVA'
,p_item_sequence=>255
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'Retiene Iva'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Si;S,No;N'
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1520154758803324323)
,p_name=>'P30_TRE_ID'
,p_item_sequence=>1961
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'%'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  if :P30_RET_IVA = ''S'' then',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion , RPO_id',
'FROM CAR_RET_PORCENTAJES',
'where rpo_emp_id = ''||:f_emp_id;',
'  else',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion, RPO_id',
'FROM CAR_RET_PORCENTAJES',
'WHERE RPO_DESCRIPCION = ''''RET FUENTE''''',
'and rpo_emp_id = ''|| :f_emp_id;',
'  end if;',
'  RETURN(lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1520535263104337561)
,p_name=>'P30_SEQ_ID_RET'
,p_item_sequence=>1971
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1520943664386352183)
,p_name=>'P30_SUBTOTAL_IVA'
,p_item_sequence=>1981
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P30_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_con_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1521202279038360603)
,p_name=>'P30_SUBTOTAL_SIN_IVA'
,p_item_sequence=>1991
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Sin Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P30_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_sin_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1521505183770373507)
,p_name=>'P30_SUBTOTAL'
,p_item_sequence=>1992
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_item_default=>'(nvl(:P30_SUBTOTAL_IVA,0) + nvl(:P30_SUBTOTAL_SIN_IVA,0)) - nvl(:P30_FLETE, 0)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Subtotal'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1521723467165381213)
,p_name=>'P30_FLETE'
,p_item_sequence=>2011
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Flete'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P30_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_servicio_manejo_producto'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1524545077861482615)
,p_name=>'P30_IVA'
,p_item_sequence=>2021
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P30_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_iva_calculo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1525162464478504019)
,p_name=>'P30_IVA_BIENES'
,p_item_sequence=>2031
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P30_IVA - (:P30_FLETE * 0.12)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Bienes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1525491173307515189)
,p_name=>'P30_IVA_FLETE'
,p_item_sequence=>2041
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P30_FLETE * 0.14'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Flete'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1525741154761523783)
,p_name=>'P30_INT_DIFERIDO'
,p_item_sequence=>2051
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Int Diferido'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P30_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_int_diferido'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1526083462081535575)
,p_name=>'P30_BASE'
,p_item_sequence=>2061
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'Base'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGA_RET'');" '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1526281081478541791)
,p_name=>'P30_PRECIONEENTER11'
,p_item_sequence=>2071
,p_item_plug_id=>wwv_flow_imp.id(95497583714324387)
,p_prompt=>'<SPAN STYLE="font-size: 10pt;color:RED;">Presione Enter para cargar el valor</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1531337972272728361)
,p_name=>'P30_OBSERVACIONES_RET'
,p_item_sequence=>2081
,p_item_plug_id=>wwv_flow_imp.id(1491558071405022431)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(2328669575527172233)
,p_name=>'P30_PAGINA_REGRESO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(2407793979585984204)
,p_name=>'P30_JA'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_item_default=>':P30_CLI_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(4879834556396993022)
,p_name=>'P30_MENSAJE_PROMO_CUOTAS'
,p_item_sequence=>941
,p_item_plug_id=>wwv_flow_imp.id(4879834173473981002)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_asdm_promociones.fn_promo_cuotas_gratis;'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:18;color:GREEN"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(5609590582773731988)
,p_name=>'P30_FACTURA_YN_R'
,p_item_sequence=>740
,p_item_plug_id=>wwv_flow_imp.id(35738481051115160)
,p_prompt=>'Factura Yn R'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6281675255392886329)
,p_name=>'P30_AVISO'
,p_item_sequence=>750
,p_item_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_prompt=>'AVISO:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'***ANTES DE GRABAR SELECCIONE UN REFERIDOR*** ',
'***SI EL CLIENTE NO ES REFERIDO DAR CLIC EN GRABAR SIN SELECCIONAR REFERIDOR*** ',
'***RECUERDE QUE ESTA INFORMACION ESTA SIENDO AUDITADA***'))
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'style="font-size:16;color:BLUE;font-family:Arial BLACK"'
,p_tag_attributes=>'style="font-size:12;color:RED;font-family:Arial Black"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6281891676648901874)
,p_name=>'P30_AYUDA'
,p_item_sequence=>760
,p_item_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_prompt=>'Ayuda'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Causas por las que un referidor no ha sido encontrado:',
'',
unistr('1. El referidor no ha sido creado por el vendedor al momento de ingresar la orden de venta. Soluci\00F3n: Pida al vendedor que cree al referidor y vuelva a realizar la b\00FAsqueda.'),
'',
unistr('2. No se est\00E1 realizando bien la b\00FAsqueda del referidor. Soluci\00F3n: Busque al referidor por el n\00FAmero de identificaci\00F3n.'),
'',
unistr('3. El referidor que est\00E1 buscando probablemente est\00E1 en el listado de personas que no pueden ser referidores. Soluci\00F3n: Compruebe que la persona que busca no est\00E9 en el listado de no permitidos. ')))
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6282107978164911768)
,p_name=>'P30_FACTURA_YN'
,p_item_sequence=>770
,p_item_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_prompt=>'Factura Yn'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6282488559988944367)
,p_name=>'P30_HAY_PROMOCION'
,p_item_sequence=>780
,p_item_plug_id=>wwv_flow_imp.id(46749483434572957)
,p_prompt=>'Hay Promocion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7781059675344477658)
,p_name=>'P30_BORRAR_DL'
,p_item_sequence=>1951
,p_item_plug_id=>wwv_flow_imp.id(7782320775109544065)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P30_HAY_PROMOCION ||'' otR ''|| :P30_ORD_ID ||'' TVE ''|| :p30_tve_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'A'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(11421239972564522093)
,p_name=>'P30_PDI_ID'
,p_item_sequence=>800
,p_item_plug_id=>wwv_flow_imp.id(11419784653677403074)
,p_prompt=>'Pdi Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13466671858459442334)
,p_name=>'P30_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13466710771965446243)
,p_name=>'P30_VALOR_REFINANCIAMIENTO'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_prompt=>'Valor Refinanciamiento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:red"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13466735181315448923)
,p_name=>'P30_CXC_ID_REF'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15337642160894025363)
,p_name=>'P30_SALDO_CONDONACION'
,p_item_sequence=>402
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Saldo Condonacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(cc.ccc_saldo)',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p30_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p30_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15337642324649025364)
,p_name=>'P30_TOTAL_PAGO_CONDONACION'
,p_item_sequence=>412
,p_item_plug_id=>wwv_flow_imp.id(46862081868373289)
,p_prompt=>'Total Pago Condonacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c016), 0)',
'',
'  fROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'   AND EXISTS',
' (select CC.CCC_ID',
'          from asdm_e.car_condonacion_cab cc, CAR_CONDONACION_DETALLE DE',
'         where cc.cli_id = :p30_cli_id',
'           and cc.ccc_estado_registro = 0',
'           and cc.ccc_saldo > 0',
'           AND CC.CCC_ID = DE.CCC_ID',
'           AND DE.CXC_ID = to_number(c007));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p30_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15448325452958983290)
,p_name=>'P30_TTR_ID_PADRE'
,p_item_sequence=>801
,p_item_plug_id=>wwv_flow_imp.id(15446032758079729330)
,p_item_default=>':p0_ttr_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>unistr('Tipo Transacci\00F3n')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_TIPOS_TRANSACCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ASDM_TIPOS_TRANSACCIONES_MUESTRA_ID'');',
'return (lv_lov);',
'END;',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit('''')" style="font-size:18;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15461352178040551761)
,p_name=>'P30_DOC_ID_PADRE'
,p_item_sequence=>805
,p_item_plug_id=>wwv_flow_imp.id(15446032758079729330)
,p_prompt=>'Documento'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.DOCUMENTO||''-''||',
'       a.TRANSACCION||''-''||',
'       a.COD_CLIENTE||''-''||',
'       a.IDENTIFICACION||''-''||',
'       a.NOMBRE_CLIENTE,',
'       a.DOCUMENTO',
'from v_asdm_documentos a',
'WHERE ttr_id = NVL(NVL(:P30_TTR_ID_PADRE,:p0_ttr_id),''ttr_id'')',
'AND uge_id = NVL(:F_UGE_ID,''UGE_ID'')',
'AND SEGMENTO = NVL(:F_SEG_ID,''SEGMENTO'')',
'AND Documento NOT IN (SELECT doc_id_padre FROM asdm_relaciones_documentos);'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15461881272808597621)
,p_name=>'P30_MOTOS_YN'
,p_item_sequence=>881
,p_item_plug_id=>wwv_flow_imp.id(15446032758079729330)
,p_prompt=>'Motos Yn'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    DECODE(c.tre_id,1000,''S'',''N'') MOTO',
'FROM      ven_ordenes a,',
'          ven_ordenes_det b,',
'          inv_relaciones_item c,',
'          inv_tipos_relaciones d',
'WHERE     a.ord_id = :P20_ORDEN_VENTA',
'AND       b.ord_id = a.ord_id',
'AND       c.ite_sku_hijo = b.ite_sku_id',
'AND       d.tre_id = c.tre_id',
'AND       d.tre_id = 1000',
'AND       ROWNUM = 1'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15463066282430694962)
,p_name=>'P30_COM_ID_PADRE'
,p_item_sequence=>851
,p_item_plug_id=>wwv_flow_imp.id(15446032758079729330)
,p_prompt=>'Comprobante Padre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15466817852553979743)
,p_name=>'P30_FACTURA_PADRE'
,p_item_sequence=>861
,p_item_plug_id=>wwv_flow_imp.id(15446032758079729330)
,p_prompt=>'Factura Padre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15467260779959016016)
,p_name=>'P30_RELACION_OBLIGATORIA'
,p_item_sequence=>871
,p_item_plug_id=>wwv_flow_imp.id(15446032758079729330)
,p_prompt=>'Relacion Obligatoria'
,p_source=>'pq_ven_comunes.fn_revisa_doc_obligatorio(:P20_ORDEN_VENTA,:F_EMP_ID)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(21229496658694837571)
,p_name=>'P30_VALIDA_PUNTO'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38124737344842203054)
,p_name=>'P30_BONO_HPROTEGIDO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38124737422718203055)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(cp) + SUM(bh) FROM (',
'SELECT  c.c007 cp,',
'       c.c008 bh',
'  FROM apex_collections c',
' WHERE c.collection_name = ''COLL_GARHP''',
' )'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38213631307720505728)
,p_name=>'P30_CPR_ID'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38213631362381505729)
,p_name=>'P30_ORD_ID_REFERIDOR'
,p_item_sequence=>620
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38213631531850505730)
,p_name=>'P30_COM_ID_REFERIDOR'
,p_item_sequence=>630
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38213631580949505731)
,p_name=>'P30_GRABAR_REFERIDOR'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_imp.id(268472760214510828)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55050476761687206034)
,p_name=>'P30_GCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(55050476500427206031)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55050476946111206035)
,p_name=>'P30_VALOR_BONO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(55050476500427206031)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63378252969573067364)
,p_name=>'P30_VALIDA_CLAVE_ANTICIPO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(268475383156510844)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63378253096750067365)
,p_name=>'P30_CLAVE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(104770569835340942)
,p_prompt=>'Clave Anticipos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P30_VALIDA_CLAVE_ANTICIPO = 1 and :f_seg_id = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142311785999181031052)
,p_name=>'P30_FORMA_CREDITO'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_imp.id(268478579909510847)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(199707359023411957943)
,p_name=>'P30_SHOW_FC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(199707358876937957942)
,p_prompt=>'Show fc'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'HTML_UNSAFE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(35892958308651747)
,p_validation_name=>'P30_MCD_VALOR_MOVIMIENTO_num'
,p_validation_sequence=>1
,p_validation=>'P30_MCD_VALOR_MOVIMIENTO'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'Error!!! Ingrese un valor correcto'
,p_always_execute=>'Y'
,p_validation_condition=>':request = ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268479762923510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
,p_validation_comment=>'AND :P30_TFP_ID <> pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(42326054467559447)
,p_validation_name=>'P30_MCD_VALOR_MOVIMIENTO_0'
,p_validation_sequence=>3
,p_validation=>'(:P30_MCD_VALOR_MOVIMIENTO) > 0'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese un valor mayor a 0'
,p_validation_condition=>'cargar_tfp'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(268479762923510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(35890182360627135)
,p_validation_name=>'P30_MCD_VALOR_MOVIMIENTO'
,p_validation_sequence=>5
,p_validation=>'to_number(:P30_MCD_VALOR_MOVIMIENTO) > to_number(:P30_SALDO_PROMO_DEV)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Valor aplicar no puede ser mayor a total de puntaje....'
,p_validation_condition=>'cargar_tfp'
,p_validation_condition_type=>'NEVER'
,p_associated_item=>wwv_flow_imp.id(268479762923510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(268483866730510880)
,p_validation_name=>'P30_EDE_ID'
,p_validation_sequence=>20
,p_validation=>'P30_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione Una Entidad'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:REQUEST = ''cargar_tfp'') ',
'AND ((:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')) or (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'')))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268479975036510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46751976568572986)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'ORD_ID not null'
,p_validation_sequence=>30
,p_validation=>'ORD_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'ORD_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46752178154572989)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'ORD_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'ORD_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'ORD_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(268484852672510881)
,p_validation_name=>'P30_CRE_TITULAR_CUENTA'
,p_validation_sequence=>30
,p_validation=>'P30_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la cuenta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268482163623510867)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46752377603572989)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'PTA_ID must be numeric'
,p_validation_sequence=>40
,p_validation=>'PTA_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'PTA_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(268484680232510880)
,p_validation_name=>'P30_CRE_NRO_CHEQUE'
,p_validation_sequence=>40
,p_validation=>'P30_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cheque'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268480172007510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46752577977572990)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'PRO_ID must be numeric'
,p_validation_sequence=>50
,p_validation=>'PRO_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'PRO_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(268484054286510880)
,p_validation_name=>'P30_CRE_NRO_CUENTA'
,p_validation_sequence=>50
,p_validation=>'P30_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cuenta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268480377073510865)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46752777379572990)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'EMP_ID not null'
,p_validation_sequence=>60
,p_validation=>'EMP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'EMP_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46752969354572990)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'EMP_ID must be numeric'
,p_validation_sequence=>60
,p_validation=>'EMP_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'EMP_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(268484270040510880)
,p_validation_name=>'P30_CRE_NRO_PIN'
,p_validation_sequence=>60
,p_validation=>'P30_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el PIN'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268480578476510865)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46753166428572990)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'POR_ESTADO_REGISTROS not null'
,p_validation_sequence=>70
,p_validation=>'POR_ESTADO_REGISTROS'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'POR_ESTADO_REGISTROS'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46753352620572990)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'POR_ESTADO_REGISTROS must be numeric'
,p_validation_sequence=>70
,p_validation=>'POR_ESTADO_REGISTROS'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'POR_ESTADO_REGISTROS'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(279925083297362646)
,p_validation_name=>'P30_TFP_ID'
,p_validation_sequence=>70
,p_validation=>'P30_TFP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione una forma de pago'
,p_validation_condition=>':P30_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_contado'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268479367018510863)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46753552616572990)
,p_tabular_form_region_id=>wwv_flow_imp.id(46749483434572957)
,p_validation_name=>'PER_ID must be numeric'
,p_validation_sequence=>80
,p_validation=>'PER_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(46750954162572982)
,p_associated_column=>'PER_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(42354653357879880)
,p_validation_name=>'P30_MCD_VALOR_MOVIMIENTO_ANT'
,p_validation_sequence=>90
,p_validation=>'to_number(:P30_MCD_VALOR_MOVIMIENTO) <= to_number(:P30_SALDO_ANTICIPOS);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese un valor menor al disponible en Anticipo de Clientes'
,p_always_execute=>'Y'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes'') ',
'AND NVL(:P30_MCD_VALOR_MOVIMIENTO,0) > 0'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268479762923510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(46952978835420349)
,p_validation_name=>'P30_MCD_NRO_COMPROBANTE'
,p_validation_sequence=>100
,p_validation=>'P30_MCD_NRO_COMPROBANTE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de comprobante de dep\00C3\00B3sito o transferencia')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(46882667557511068)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(68975553329601541)
,p_validation_name=>'P30_MCD_VALOR_MOVIMIENTO_PR'
,p_validation_sequence=>110
,p_validation=>'round(to_number(nvl(:P30_SALDO_MAX_APLICAR,0)),4)>= round(to_number(nvl(:P30_MCD_VALOR_MOVIMIENTO,0)),4);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El valor no debe ser mayor al "Saldo M\00E1ximo a Aplicar....')
,p_validation_condition=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'')  and :P30_MCD_VALOR_MOVIMIENTO > 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(268479762923510864)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(95556261212093644)
,p_validation_name=>'P30_RAP_NRO_AUTORIZACION'
,p_validation_sequence=>130
,p_validation=>'to_number(:P30_RAP_NRO_AUTORIZACION)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de autorizaci\00C3\00B3n de la retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'') and :P30_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95499652986372261)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(95556878527098640)
,p_validation_name=>'P30_RAP_FECHA'
,p_validation_sequence=>140
,p_validation=>':P30_RAP_FECHA IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la fecha de la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95499863029375241)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(95557559265102584)
,p_validation_name=>'P30_RAP_FECHA_VALIDEZ'
,p_validation_sequence=>150
,p_validation=>':P30_RAP_FECHA_VALIDEZ IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la fecha de validez de la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95500283115380967)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(95564371264172230)
,p_validation_name=>'P30_RAP_COM_ID'
,p_validation_sequence=>160
,p_validation=>':P30_RAP_COM_ID IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la factura a la que pertenece la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'') and :P30_FACTURAR = ''N'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95500867316385922)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(95566875336192335)
,p_validation_name=>'P30_RAP_NRO_RETENCION'
,p_validation_sequence=>170
,p_validation=>'to_number(:P30_RAP_NRO_RETENCION)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95500867316385922)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97236862058985247)
,p_validation_name=>'P30_RAP_NRO_ESTABL_RET'
,p_validation_sequence=>180
,p_validation=>'to_number(:P30_RAP_NRO_ESTABL_RET)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de establecimiento de la retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(96297867118757046)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97237661150994442)
,p_validation_name=>'P30_RAP_NRO_PEMISION_RET'
,p_validation_sequence=>190
,p_validation=>'to_number(:P30_RAP_NRO_PEMISION_RET)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero del punto de emisi\00C3\00B3n de la retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(96298478893760489)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(103079554061221090)
,p_validation_name=>'P30_PLAN'
,p_validation_sequence=>200
,p_validation=>'P30_PLAN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe Seleccionar el Plan'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P30_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(90427883002161185)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(103080372762226431)
,p_validation_name=>'P30_TITULAR_TC'
,p_validation_sequence=>210
,p_validation=>'P30_TITULAR_TC'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el titular de la Tarjeta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P30_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(103076953191182917)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109105380573139299)
,p_validation_name=>'P30_RAP_NRO_AUTORIZACION_MAX'
,p_validation_sequence=>250
,p_validation=>'LENGTH(:P30_RAP_NRO_AUTORIZACION) = 10'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El n\00C3\00BAmero de autorizaci\00C3\00B3n de la retenci\00C3\00B3n debe ser de 10 d\00C3\00ADgitos.')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'') and :P30_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95499652986372261)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109124475126194493)
,p_validation_name=>'P30_RAP_FECHA_VALIDEZ_MAYOR_ACTUAL'
,p_validation_sequence=>260
,p_validation=>'trunc(to_date(:P30_RAP_FECHA_VALIDEZ)) >= trunc(SYSDATE)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'La fecha de validez no debe ser menor a la fecha actual'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95500283115380967)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109137765523248555)
,p_validation_name=>'P30_RAP_FECHA_VALIDEZ_MAYOR_FECHA'
,p_validation_sequence=>270
,p_validation=>'trunc(to_date(:P30_RAP_FECHA_VALIDEZ)) >= trunc(to_date(:P30_RAP_FECHA))'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('La fecha de validez no puede ser menor a la fecha de ingreso de la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(95500283115380967)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109159951680320215)
,p_validation_name=>'P30_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>280
,p_validation=>'P30_MCD_FECHA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la fecha de deposito'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(96687265571987141)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109164652981339473)
,p_validation_name=>'P30_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>290
,p_validation=>'trunc(to_date(:P30_MCD_FECHA_DEPOSITO)) <= trunc(sysdate)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Fecha deposito no puede ser mayor a la fecha actual'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(96687265571987141)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(118983867130161915)
,p_validation_name=>'P30_NUM_FOLIO_NCREDITO'
,p_validation_sequence=>300
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Declare ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'   lv_ttr_id varchar2 (30);',
'',
'',
'BEGIN  ',
'  if  :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_puntualito_regalon'') then',
'  ',
'   --lv_ttr_id := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito'');',
'   lv_ttr_id := pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_nc_rebaja_puntualito'');',
'  ',
'  elsif :P30_TFP_ID =',
'        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tfp_id_promocion_puntaje'') then',
'',
'     lv_ttr_id := pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_nc_rebaja_puntaje'');',
'     --lv_ttr_id := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntaje'');',
'',
'  end if;',
'',
'',
'--ln_tgr_id  :=  pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito'');',
' -- :P0_TTR_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito'');',
'',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito''),',
'      :P0_TTR_DESCRIPCION, ',
'      :P0_PERIODO,',
'      :P0_DATFOLIO,',
'      :P30_SECUENCIA_ACTUAL,',
'      :P0_PUE_NUM_SRI  ,',
'      :P0_UGE_NUM_EST_SRI ,',
'      :P0_PUE_ID,',
'      lv_ttr_id ,',
'      --pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito''),',
'      :P0_NRO_FOLIO,',
'      :P0_ERROR);',
'',
'',
'',
'if :P30_NUM_FOLIO_NCREDITO = :P30_SECUENCIA_ACTUAL then',
'return null;',
'else',
'return ''Secuencia Incorrecta'';--||'' ''||:p30_secuencia_actual;',
'end if;',
'END;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>':request= ''validar_nc'' AND (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_promocion_puntaje''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(118861153036240178)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(5145433457763794438)
,p_validation_name=>'P30_MCD_NRO_COMPROBANTE'
,p_validation_sequence=>310
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select mcd.mcd_nro_comprobante',
'                from asdm_movimientos_caja         mca,',
'                     asdm_movimientos_caja_detalle mcd',
'               where mcd.mca_id = mca.mca_id',
'                 and mcd.emp_id = mca.emp_id',
'                 and mcd.tfp_id IN',
'                     (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                         ''cn_tfp_id_deposito''),',
'                      pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                         ''cn_tfp_id_transferencia''))',
'                 and mcd.mcd_nro_comprobante = to_number(:P30_MCD_NRO_COMPROBANTE)',
'                 and mca.mca_estado_mc is null',
'                 and mca.mca_id_referencia is null',
'                 and mca.emp_id = :F_emp_id',
'                 and not exists',
'               (select null',
'                        from asdm_movimientos_caja ca',
'                       where ca.mca_id_referencia = mca.mca_id);'))
,p_validation_type=>'NOT_EXISTS'
,p_error_message=>'YA EXISTE UNA PAPELETA INGRESADA CON ESTE NUMERO'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(46882667557511068)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
,p_validation_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63378253159256067366)
,p_validation_name=>'P30_ANTICIPO'
,p_validation_sequence=>320
,p_validation=>'P30_CLAVE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar la Clave'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(103076953191182917)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(55050477065651206037)
,p_name=>'da_Submit_aplica_bono'
,p_event_sequence=>1
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_display_when_cond=>'limpia'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(55050477183338206038)
,p_event_id=>wwv_flow_imp.id(55050477065651206037)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  LN_TFP_ID_BONO_HOGAR asdm_e.asdm_constantes_empresa.cem_valor%TYPE := ASDM_P.PQ_CONSTANTES.fn_retorna_constante(pn_emp_id => :F_EMP_ID, ',
'                                                                                                                  pv_constante => ''cn_tfp_id_bono_hogar'');',
'                                                                                                                  ',
'BEGIN                                                                                                                   ',
'    IF :P30_TFP_ID = LN_TFP_ID_BONO_HOGAR THEN',
'        --RAISE_APPLICATION_ERROR(-20001,''p30_valor_bono: ''||:p30_valor_bono||'' p30_valor_total_pagos: ''||:p30_valor_total_pagos);',
'        IF (:p30_valor_bono IS NOT NULL) AND (nvl(to_number(:p30_valor_total_pagos),0) >= to_number(:p30_valor_bono)) THEN',
'          --RAISE_APPLICATION_ERROR(-20001, ''QUI ENTRO: ''||:p30_valor_bono);',
'          :p30_mcd_valor_movimiento := to_number(:p30_valor_bono);  ',
'        ELSIF nvl(to_number(:p30_valor_total_pagos),0) < nvl(to_number(:p30_valor_bono),0) THEN',
'          --RAISE_APPLICATION_ERROR(-20001, ''QUI ENTRO else: ''||:p30_valor_bono);',
'          :p30_mcd_valor_movimiento := to_number(:P30_SALDO_PAGO);',
'        END IF;',
'    ELSE',
'      RAISE_APPLICATION_ERROR(-20000,''La Forma de Pago no corresponde a Bono Hogar'');',
'    END IF;',
'END;'))
,p_attribute_02=>'P30_VALOR_BONO,P30_VALOR_TOTAL_PAGOS,P30_MCD_VALOR_MOVIMIENTO'
,p_attribute_03=>'P30_MCD_VALOR_MOVIMIENTO'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
,p_da_action_comment=>'-- ILIMA PAOBERNAL 01/FEBRERO/2019L ESPECIFICAR LOS ITEMS SUBMIT Y RETURN'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(55050477299822206039)
,p_event_id=>wwv_flow_imp.id(55050477065651206037)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'cargar_tfp'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(107224751543863411)
,p_name=>'pr_refrescar'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107225057629863412)
,p_event_id=>wwv_flow_imp.id(107224751543863411)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(104657655926516000)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107249152554986641)
,p_event_id=>wwv_flow_imp.id(107224751543863411)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(104756753119279351)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107631264897223584)
,p_event_id=>wwv_flow_imp.id(107224751543863411)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(268472760214510828)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107889483428033784)
,p_event_id=>wwv_flow_imp.id(107224751543863411)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(95497583714324387)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(155353064611827682)
,p_name=>'AD_GRABACION'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#pagar'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(155353360600827731)
,p_event_id=>wwv_flow_imp.id(155353064611827682)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_INI2'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'INI2'
,p_attribute_09=>'N'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(155353576732831179)
,p_event_id=>wwv_flow_imp.id(155353064611827682)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_INI'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'INI'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(155353772361839347)
,p_name=>'AD_EJECUTA_SUBMIT'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_INI'
,p_condition_element=>'P30_INI'
,p_triggering_condition_type=>'IN_LIST'
,p_triggering_expression=>'INI'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(155354068136839353)
,p_event_id=>wwv_flow_imp.id(155353772361839347)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'doSubmit(''&P30_INI2.'');'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(49725579860637295)
,p_name=>'CIERRA_PROCESO'
,p_event_sequence=>60
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'unload'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(49725872727637298)
,p_event_id=>wwv_flow_imp.id(49725579860637295)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_INI'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'FIN'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(49726255057639605)
,p_event_id=>wwv_flow_imp.id(49725579860637295)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_INI2'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'FIN2'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15466708453807970632)
,p_name=>'AD_VALIDA_COMPROBANTE_PADRE'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_COM_ID_PADRE'
,p_condition_element=>'P30_COM_ID_PADRE'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15466710062483970689)
,p_event_id=>wwv_flow_imp.id(15466708453807970632)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_FACTURA_PADRE'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_comunes.fn_busca_documento_relacionado(:p30_ttr_id_padre,',
'                                                     :p30_com_id_padre,',
'                                                     :P30_CLI_ID);'))
,p_attribute_07=>'P30_COM_ID_PADRE'
,p_attribute_08=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(21230860878076912101)
,p_name=>'AD_REFRESH_VALIDA_PUNTO'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_VALIDA_PUNTO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(21230863556153912174)
,p_event_id=>wwv_flow_imp.id(21230860878076912101)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P30_VALIDA_PUNTO'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(1526690771159555214)
,p_name=>'ad_submit_tiene_iva_ret'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_RET_IVA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1526691075857555218)
,p_event_id=>wwv_flow_imp.id(1526690771159555214)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P30_RET_IVA'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(1526955775100565840)
,p_name=>'ad_muestra_items_iva_s'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_RET_IVA'
,p_condition_element=>'P30_RET_IVA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1526956175907565842)
,p_event_id=>wwv_flow_imp.id(1526955775100565840)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_INT_DIFERIDO,P30_IVA_FLETE,P30_IVA_BIENES,P30_IVA,P30_FLETE,P30_SUBTOTAL,P30_SUBTOTAL_IVA,P30_SUBTOTAL_SIN_IVA'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1526956375483565842)
,p_event_id=>wwv_flow_imp.id(1526955775100565840)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_IVA_FLETE,P30_IVA_BIENES,P30_IVA'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43173834541268235)
,p_name=>'da_procesar_pago'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_PROCESO_TJ'
,p_condition_element=>'P30_PROCESO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'PP'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43173855244268236)
,p_event_id=>wwv_flow_imp.id(43173834541268235)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43078138868640170)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174250023268239)
,p_event_id=>wwv_flow_imp.id(43173834541268235)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43076423504640153)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174098510268238)
,p_event_id=>wwv_flow_imp.id(43173834541268235)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TRAMA_RESP'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44568121261012333)
,p_event_id=>wwv_flow_imp.id(43173834541268235)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43076265770640152)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43174316313268240)
,p_name=>'da_consutla_tarjeta'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_PROCESO_TJ'
,p_condition_element=>'P30_PROCESO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'CT'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174378276268241)
,p_event_id=>wwv_flow_imp.id(43174316313268240)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TRAMA_RESP'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174510518268242)
,p_event_id=>wwv_flow_imp.id(43174316313268240)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43076265770640152)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174581628268243)
,p_event_id=>wwv_flow_imp.id(43174316313268240)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43076423504640153)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174680798268244)
,p_event_id=>wwv_flow_imp.id(43174316313268240)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43078138868640170)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43174790744268245)
,p_name=>'da_lectura_tarjeta'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_PROCESO_TJ'
,p_condition_element=>'P30_PROCESO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'LT'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43174941714268246)
,p_event_id=>wwv_flow_imp.id(43174790744268245)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TRAMA_RESP'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43175003858268247)
,p_event_id=>wwv_flow_imp.id(43174790744268245)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43078138868640170)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43175128984268248)
,p_event_id=>wwv_flow_imp.id(43174790744268245)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43076265770640152)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43175205819268249)
,p_event_id=>wwv_flow_imp.id(43174790744268245)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43076423504640153)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43175265179268250)
,p_name=>'ad_automatico'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_TIPO_PAGO_TJ'
,p_condition_element=>'P30_TIPO_PAGO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'A'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43175353553268251)
,p_event_id=>wwv_flow_imp.id(43175265179268250)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TJ_NUMERO,P30_TJ_NUMERO_VOUCHER,P30_TJ_RECAP,P30_MCD_NRO_AUT_REF'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(146213714418150766)
,p_event_id=>wwv_flow_imp.id(43175265179268250)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TJ_NUMERO_1,P30_TJ_NUMERO_VOUCHER_1,P30_TJ_RECAP_1,P30_MCD_NRO_AUT_REF_1,P30_PROCESO_TJ,P30_PINPAD'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(146213771025150767)
,p_event_id=>wwv_flow_imp.id(43175265179268250)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         VARCHAR2(250);',
'  ln_rpr_id           NUMBER;',
'',
'BEGIN',
'',
'  BEGIN',
'    SELECT rpp.rpp_id',
'      INTO :p30_pinpad',
'      FROM car_redes_pago_pue rpp',
'     WHERE rpp.pue_id = :p0_pue_id',
'       AND rpp.rpp_estado_registro = 0;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      :p30_pinpad := NULL;',
'  END;',
'  IF :p30_pinpad IS NOT NULL THEN',
'    -- Call the procedure                                    ',
'    pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id                 => :p30_pinpad,',
'                                                   pn_cli_id                 => :p30_cli_id,',
'                                                   pn_uge_id                 => :f_uge_id,',
'                                                   pn_emp_id                 => :f_emp_id,',
'                                                   pn_pue_id                 => :p0_pue_id,',
'                                                   pv_rpr_tipo_requerimiento => ''LT'',',
'                                                   pn_rpc_id                 => :p30_rpc_id,',
'                                                   pn_rpr_id_req             => :p30_rpr_id_req,',
'                                                   pv_trama                  => lv_data_set,',
'                                                   pn_ede_id                 => NULL,',
'                                                   pv_error                  => :p0_error);',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'      pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id             => :p30_pinpad,',
'                                              pn_pue_id             => :p0_pue_id,',
'                                              pn_cli_id             => :p30_cli_id,',
'                                              pv_tipo_requerimiento => ''LT'',',
'                                              pv_trama_req          => lv_data_set,',
'                                              pv_trama_res          => :p30_trama_resp,',
'                                              pn_rpc_id             => :p30_rpc_id,',
'                                              pn_rpr_id_req         => :p30_rpr_id_req,',
'                                              pn_rpr_id_res         => :p30_rpr_id,',
'                                              pc_error              => pc_error);',
'                                              ',
'',
'    ',
'      IF pc_error IS NOT NULL THEN',
'        raise_application_error(-20000, pc_error);',
'        :p0_error := pc_error;',
'      END IF;',
'    END IF;',
'    pq_car_redes_pago.pr_retorna_datos_lectura(pn_rpr_id_req => :p30_rpr_id,',
'                                               pn_ede_id     => :p30_bancos);',
'  ',
'  END IF;',
'END;',
''))
,p_attribute_02=>'P30_RED,P30_CLI_ID,F_UGE_ID,F_EMP_ID,P0_PUE_ID,P30_RPC_ID,P30_RPR_ID_REQ,P0_ERROR,P30_RPR_ID,P30_NRO_PINPAD'
,p_attribute_03=>'P30_RPC_ID,P30_RPR_ID_REQ,P0_ERROR,P30_TRAMA_RESP,P30_RPR_ID,P30_BANCOS,P30_PINPAD,P0_ERROR'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43175459573268252)
,p_name=>'ad_manual'
,p_event_sequence=>150
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_TIPO_PAGO_TJ'
,p_condition_element=>'P30_TIPO_PAGO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'M'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43175594964268253)
,p_event_id=>wwv_flow_imp.id(43175459573268252)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TJ_NUMERO,P30_TJ_NUMERO_VOUCHER,P30_TJ_RECAP,P30_MCD_NRO_AUT_REF'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(146213572598150765)
,p_event_id=>wwv_flow_imp.id(43175459573268252)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P30_TJ_NUMERO_1,P30_TJ_NUMERO_VOUCHER_1,P30_TJ_RECAP_1,P30_MCD_NRO_AUT_REF_1,P30_PROCESO_TJ,P30_PINPAD'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(144294303384554766)
,p_name=>'ad_pinpad'
,p_event_sequence=>160
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P30_PINPAD'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(187974609613354738)
,p_event_id=>wwv_flow_imp.id(144294303384554766)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         VARCHAR2(250);',
'  ln_rpr_id           NUMBER;',
'',
'BEGIN',
'',
'',
'  -- Call the procedure                                    ',
'  pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id                 => :p30_pinpad,',
'                                                 pn_cli_id                 => :p30_cli_id,',
'                                                 pn_uge_id                 => :f_uge_id,',
'                                                 pn_emp_id                 => :f_emp_id,',
'                                                 pn_pue_id                 => :p0_pue_id,',
'                                                 pv_rpr_tipo_requerimiento => ''LT'',',
'                                                 pn_rpc_id                 => :p30_rpc_id,',
'                                                 pn_rpr_id_req             => :p30_rpr_id_req,',
'                                                 pv_trama                  => lv_data_set,',
'                                                 pn_ede_id                 => NULL,',
'                                                 pv_error                  => :p0_error);',
'',
'  IF :p0_error IS NULL THEN',
'  ',
'    pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id             => :p30_pinpad,',
'                                            pn_pue_id             => :p0_pue_id,',
'                                            pn_cli_id             => :p30_cli_id,',
'                                            pv_tipo_requerimiento => ''LT'',',
'                                            pv_trama_req          => lv_data_set,',
'                                            pv_trama_res          => :p30_trama_resp,',
'                                            pn_rpc_id             => :p30_rpc_id,',
'                                            pn_rpr_id_req         => :p30_rpr_id_req,',
'                                            pn_rpr_id_res         => :p30_rpr_id,',
'                                            pc_error              => pc_error);',
'  ',
'    IF pc_error IS NOT NULL THEN',
'      raise_application_error(-20000, pc_error);',
'      :p0_error := pc_error;',
'    END IF;',
'  END IF;',
'  pq_car_redes_pago.pr_retorna_datos_lectura(pn_rpr_id_req => :p30_rpr_id,',
'                                             pn_ede_id     => :p30_bancos);',
'',
'END;',
''))
,p_attribute_02=>'P30_RED,P30_CLI_ID,F_UGE_ID,F_EMP_ID,P0_PUE_ID,P30_RPC_ID,P30_RPR_ID_REQ,P0_ERROR,P30_RPR_ID,P30_NRO_PINPAD,P30_PINPAD'
,p_attribute_03=>'P30_RPC_ID,P30_RPR_ID_REQ,P0_ERROR,P30_TRAMA_RESP,P30_RPR_ID,P30_BANCOS,P30_PINPAD'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(104828551349676176)
,p_process_sequence=>60
,p_process_point=>'AFTER_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_total_gastos_para_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_pagos_cuota.pr_total_gastos_para_folios(',
'pn_cli_id => :P30_CLI_ID,',
'pn_emp_id => :F_EMP_ID,',
'pn_seg_id => :F_SEG_ID,',
'pv_facturar => :P30_FACTURAR,',
'pn_int_mora => :P30_INT_MORA_COB,',
'pn_gto_cob  => :P30_GTOS_COB_COB);'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>72575400079911250
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(6282591258387953446)
,p_process_sequence=>160
,p_process_point=>'AFTER_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_VERIFICAR_PROMO_APLICA'
,p_process_sql_clob=>':P30_HAY_PROMOCION := ''S'';'
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.POR_ID,',
'A.POR_ID POR_ID_DISPLAY,',
'A.ORD_ID,',
'A.PTA_ID,',
'A.PRO_ID,',
'A.EMP_ID,',
'A.POR_ESTADO_REGISTROS,',
'A.PER_ID,',
'A.POR_NO_ID_BENEF,',
'A.PTA_NOMBRE,',
'A.PTA_SELECCIONADO,',
'B.PRO_DESCRIPCION',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p30_ord_id'))
,p_process_when_type=>'EXISTS'
,p_internal_uid=>6250338107118188520
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(5609764958924753539)
,p_process_sequence=>150
,p_process_point=>'AFTER_FOOTER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_VALIDAR_POL_REGALOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P30_POLITICA_VENTA = 112 AND :P30_PER_ID_PUNTOS IS NOT NULL AND :P30_PUNTOS_CANCELAR IS NOT NULL THEN',
'   :P30_FACTURA_YN_R := ''S'';',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>5577511807654988613
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(6280043679532760838)
,p_process_sequence=>150
,p_process_point=>'AFTER_FOOTER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_persona_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'BEGIN',
'   SELECT    PER_ID',
'   INTO      :F_PER_ID_P',
'   FROM      ASDM_CLIENTES',
'   WHERE     CLI_ID = :P30_CLI_ID;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>6247790528262995912
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(105050356054692681)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_gastos_para_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
' :P30_INI := NULL;',
' :P30_INI2 := NULL;',
'',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_int_mora''));',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_gto_cobr''));',
'',
'pq_ven_pagos_cuota.pr_carga_gastos_para_folios(pn_cli_id => :P30_CLI_ID,',
'                                       pn_emp_id => :F_EMP_ID,',
'                                       pn_seg_id => :F_SEG_ID,',
'                                       pv_error => :P0_ERROR',
'                                       );',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''formas_pago'' '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>72797204784927755
,p_process_comment=>'Existia un problema cuando se hacia este procedimiento desde esta pantalla, porque al inicio se quedaba el request cargado con ''formas_pago'' y si se hacia un refresh F5 se volvia a cargar el proceso, es por esta razon que se puso en al App:211 pag: 6'
||unistr(' para que se carga cuando hagan clic en pagar all\00C3\00AD.')
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15332691160997409455)
,p_process_sequence=>5
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_borra_coleccion_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if apex_collection.collection_exists(''COLL_VALOR_RETENCION'') then',
'  ',
'apex_collection.delete_collection(''COLL_VALOR_RETENCION'');',
'',
'end if;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'factura'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>15300438009727644529
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(268485359801510883)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_linea_coleccion_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
' lv_nombre_coleccion VARCHAR2(50);',
' ',
' ln_valor_par_anticipo number;',
' ln_valor_anticipo number;',
'BEGIN',
'',
'lv_nombre_coleccion := pq_constantes.fn_retorna_constante(NULL,',
'                                                              ''cv_coleccion_mov_caja'');',
'',
'pq_inv_movimientos.pr_borra_reg_colecciones(:p30_seq_id ,lv_nombre_coleccion);',
'',
'pq_ven_movimientos_caja.pr_vueltos(pn_total_pagar => to_number(:P30_VALOR_TOTAL_PAGOS),',
'                                     pn_vuelto => :p30_vuelto,',
'                                     pn_emp_id => :f_emp_id,',
'                                     pv_error => :p0_error);',
'',
':P30_SEQ_ID := NULL;',
':P30_MCD_VALOR_MOVIMIENTO := NULL;',
':P30_TFP_ID_ELIMINAR := NULL;',
':P30_MODIFICACION := NULL;',
'',
'begin',
'',
'  select nvl(pa.pen_valor, 0)',
'    into ln_valor_par_anticipo',
'    from asdm_e.car_par_entidades pa',
'   where pa.par_id = 85;',
'',
'  select nvl(sum(TO_NUMBER(c006)), 0)',
'    into ln_valor_anticipo',
'    from apex_collections co',
'   where collection_name = ''CO_MOV_CAJA''',
'     and to_number(c005) =',
'         pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                            ''cn_tfp_id_anticipo_clientes'');',
'',
'  if ln_valor_anticipo >= ln_valor_par_anticipo then',
'    :P30_VALIDA_CLAVE_ANTICIPO := 1;',
'  else',
'    :P30_VALIDA_CLAVE_ANTICIPO := 0;',
'  end if;',
'',
'exception',
'  when others then',
'    null;',
'  ',
'end;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>236232208531745957
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(272652962865524613)
,p_process_sequence=>80
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_colec_cuota_cero'
,p_process_sql_clob=>'pq_ven_pagos_cuota.pr_carga_colec_cuota_cero(:P30_CLI_ID,:f_emp_id,:P30_ORD_ID,:p0_error);'
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P30_FACTURAR = ''S'' AND',
':P30_COM_ID IS NULL'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>240399811595759687
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(105593355451616528)
,p_process_sequence=>85
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_existe_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota.pr_valida_existe_folios(pn_emp_id => :F_EMP_ID,',
'                        pn_cli_id => :P30_CLI_ID,',
'                        pn_gasto  => ''IM'',',
'                        pv_error  => :P0_ERROR);',
'',
'pq_ven_pagos_cuota.pr_valida_existe_folios(pn_emp_id => :F_EMP_ID,',
'                        pn_cli_id => :P30_CLI_ID,',
'                        pn_gasto  => ''GC'',',
'                        pv_error  => :P0_ERROR);',
'',
'IF :P0_ERROR is not null then',
'  :p30_ini := NULL;',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P0_ERROR is null and :p30_ini = ''INI'' and :P30_VALIDA_PUNTO = ''P'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>73340204181851602
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(287158660730509797)
,p_process_sequence=>90
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_factura_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_clave                    NUMBER;',
'  ln_valor                    NUMBER;',
'  ln_valor_dev                NUMBER;',
'  ln_diferencia_saldo_tarjeta NUMBER;',
'  lv_error                    VARCHAR2(500);',
'  ln_seq_id                   NUMBER;',
'  lv_observacion              VARCHAR2(1000);',
'  ln_resultado                NUMBER;',
'BEGIN',
'',
'  ----MPO-30  JALVAREZ 25JUN2023',
'  lv_error := pq_car_validaciones.fn_valida_tipo_plan_tarjeta(pn_ord_id => :p30_ord_id,',
'                                                              pn_emp_id => :f_emp_id);',
'  IF lv_error IS NULL THEN',
'    -- Call the procedure',
'  ',
'    /*',
'      Modificacion: FPALOMEQUE',
'      Descripcion:  Nuevo campo Forma de Credito ',
'      Fecha:        31/03/2021',
'      Historia:     R3163-09',
'    */',
'  ',
'    BEGIN',
'      SELECT pq_car_redes_pago.fn_retorna_saldo_tarjeta(pn_cli_id => :p30_cli_id,',
'                                                        pn_uge_id => :f_uge_id) -',
'             to_number(SUM(nvl(c006, 0))) valor',
'        INTO ln_diferencia_saldo_tarjeta',
'        FROM apex_collections',
'       WHERE collection_name =',
'             pq_constantes.fn_retorna_constante(NULL,',
'                                                ''cv_coleccion_mov_caja'')',
'         AND to_number(c005) =',
'             asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                       ''cn_tfp_id_tarjeta_credito'');',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        ln_diferencia_saldo_tarjeta := 0;',
'    END;',
'  ',
'    ----INICIO: R92: VPROANO 9/JULIO/2019 VALIDACION ',
'  ',
'    IF nvl(ln_diferencia_saldo_tarjeta, 0) <= 0 THEN',
'      BEGIN',
'        SELECT nvl(to_number(c006), 0) valor',
'          INTO ln_valor',
'          FROM apex_collections co, asdm_tipos_formas_pago p',
'         WHERE collection_name = ''CO_MOV_CAJA''',
'           AND p.tfp_id = to_number(co.c005)',
'           AND p.tfp_id =',
'               asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                         ''cn_tfp_id_anticipo_clientes''); ----IN (5, 14, 36, 58, 80, 102, 125, 155, 182, 212, 239);',
'        ----JALVAREZ 25ENE2023 SE COMENTA LA PROGRAMACION ANTERIOR PARA USAR UNA CONSTANTE SEGUN LA EMPRESA',
'      EXCEPTION',
'        WHEN no_data_found THEN',
'          ln_valor := 0;',
'      END;',
'    ',
'      --    raise_application_error(-20000,'' valor:::: ''||ln_valor);',
'      IF :p30_valida_clave_anticipo = 1 AND :f_seg_id = 1 THEN',
'        pq_car_manejo_claves.pr_valida_clave_anticipo(pn_emp_id => :f_emp_id,',
'                                                      pn_cli_id => :p30_cli_id,',
'                                                      pn_valor  => ln_valor,',
'                                                      pn_clave  => ln_clave,',
'                                                      pv_error  => :p0_error);',
'        --raise_application_error(-20000,'' valor:::: ''||ln_clave);',
'        IF ln_clave = :p30_clave THEN',
'          NULL;',
'        ELSE',
'          raise_application_error(-20000, ''ERROR EN LA CLAVE '');',
'        END IF;',
'      END IF;',
'    ',
'      /*   if :f_seg_id = 1 then',
'      ',
'        raise_application_error(-20000,'' valida'');',
'      end if;*/',
'    ',
'      ----FIN: R92: VPROANO 9/JULIO/2019 VALIDACION ',
'      SELECT nvl(SUM(to_number(c006)), 0)',
'        INTO ln_valor_dev',
'        FROM apex_collections co',
'       WHERE collection_name = ''CO_MOV_CAJA''',
'         AND to_number(c005) =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_tfp_id_dev_promo'');',
'    ',
'      IF nvl(ln_valor_dev, 0) > 0 THEN',
'      ',
'        NULL;',
'        --raise_application_error(-20000, ''Por el momento NO se puede utilizar la Forma de Pago Promociones Devueltas * '');',
'      ',
'      END IF;',
'    ',
'      pq_ven_comprobantes.pr_graba_factura_pago_cuota(pn_emp_id           => :f_emp_id,',
'                                                      pn_uge_id           => :f_uge_id,',
'                                                      pn_uge_id_gasto     => :f_uge_id_gasto,',
'                                                      pn_pca_id           => :f_pca_id,',
'                                                      pn_ttr_id           => :p0_ttr_id,',
'                                                      pn_pol_id           => :p30_politica_venta,',
'                                                      pn_tve_id_1         => :p30_termino_venta,',
'                                                      pn_tve_id           => :p30_tve_id,',
'                                                      pn_com_id           => :p30_com_id,',
'                                                      pn_com_id_referidor => :p30_com_id_referidor,',
'                                                      pn_cli_id           => :p30_cli_id,',
'                                                      pn_per_id           => :p30_per_id,',
'                                                      pn_per_id_puntos    => :p30_per_id_puntos,',
'                                                      pn_trx_id           => :p30_trx_id,',
'                                                      pn_pue_id           => :p0_pue_id,',
'                                                      pn_sec_actua        => :p30_secuencia_actual,',
'                                                      pn_com_numero       => :p0_fol_sec_actual,',
'                                                      pv_uge_num_sri      => :p0_uge_num_est_sri,',
'                                                      pv_pue_num_sri      => :p0_pue_num_sri,',
'                                                      pn_nro_folio        => :p0_nro_folio,',
'                                                      pn_age_id_agencia   => :f_age_id_agencia,',
'                                                      pn_ord_id           => :p30_ord_id,',
'                                                      pn_ord_id_referidor => :p30_ord_id_referidor,',
'                                                      pv_ord_tipo         => :p30_ord_tipo,',
'                                                      pn_cot_id           => :p30_cot_id,',
'                                                      pv_com_observacion  => :p20_com_observaciones,',
'                                                      pn_cxc_id_refinan   => :p30_cxc_id_ref,',
'                                                      pn_bono_hprotegido  => :p30_bono_hprotegido,',
'                                                      pn_ode_id           => :p30_ode_id,',
'                                                      pn_ebs_id           => :p20_ebs_id,',
'                                                      pn_tse_id           => :f_seg_id,',
'                                                      pv_emp_logo         => :f_emp_logo,',
'                                                      pv_emp_fondo        => :f_emp_fondo,',
'                                                      pv_ugestion         => :f_ugestion,',
'                                                      pn_mnc_id           => :p19_mca_id,',
'                                                      pn_mca_id_con       => :p19_mca_id_con,',
'                                                      pn_cpr_id           => :p30_cpr_id,',
'                                                      pn_pro_id           => :p30_pro_id,',
'                                                      pn_pta_id           => :p30_pta_id,',
'                                                      pn_pdi_id           => :p30_pdi_id,',
'                                                      pv_app_user         => :app_user,',
'                                                      pv_session          => :session,',
'                                                      pv_token            => :f_token,',
'                                                      pn_user_id          => :f_user_id,',
'                                                      pn_rol              => :p0_rol,',
'                                                      pv_rol_desc         => :p0_rol_desc,',
'                                                      pn_tree_rot         => :p0_tree_root,',
'                                                      pn_opcion           => :f_opcion_id,',
'                                                      pv_parametro        => :f_parametro,',
'                                                      pv_popup            => :f_popup,',
'                                                      pv_empresa          => :f_empresa,',
'                                                      pn_es_pol_gobierno  => :p30_es_politica_gobierno,',
'                                                      pv_facturar         => :p30_facturar,',
'                                                      pv_graba_referidor  => :p30_grabar_referidor,',
'                                                      pn_total_factura    => :p30_total_factura,',
'                                                      pn_suma_pagos       => :p30_sum_pagos,',
'                                                      pn_valor_refinan    => :p30_valor_refinanciamiento,',
'                                                      pn_pago_refinan     => :p30_pago_refinanciamiento,',
'                                                      pn_mcd_valor_mov    => :p30_mcd_valor_movimiento,',
'                                                      pn_vuelto           => :p30_vuelto,',
'                                                      pn_puntos_cancelar  => :p30_puntos_cancelar,',
'                                                      pn_puntos_usuados   => :p30_puntos_usados,',
'                                                      pn_puntaje_fijo     => :p30_puntaje_fijo,',
'                                                      pv_ini              => :p30_ini,',
'                                                      pv_ini2             => :p30_ini2,',
'                                                      pv_forma_credito    => :p30_forma_credito, --FPALOMEQUE R3163-09',
'                                                      pv_error            => :p0_error);',
'    ',
'      pq_ven_ordenes_dml.pr_upd_ven_ordenes_plan_tcred(pn_ord_id => :p30_ord_id,',
'                                                       pn_com_id => :p30_com_id,',
'                                                       pv_error  => :p0_error);',
'    ',
unistr('      -- R3555-02 hsolano 11/03/2022 actualizaci\00F3n com_id payjoy'),
'      IF :p20_venta_payjoy IS NOT NULL THEN',
'        pq_ven_otros_dml.pr_upd_car_payjoy_ventas_com(pn_emp_id => :f_emp_id,',
'                                                      pn_ord_id => :p30_ord_id,',
'                                                      pn_com_id => :p30_com_id,',
'                                                      pc_error  => :p0_error);',
'      END IF;',
'      --Grabamos resumen combos enaranjo 23/08/2023',
'      pq_ven_pro_comunes.pr_resumen_promocion(pn_com_id => :p30_com_id,',
'                                          pv_error => :p0_error);',
'    ELSE',
'    ',
'      :p0_error := ''DEBE CONSUMIR TODO EL SALDO DE LOS PAGOS DE TARJETAS'';',
'    END IF;',
'    IF :p30_ord_tipo = ''VENMO'' and :p30_forma_credito = ''E'' THEN',
'        pq_car_ecredit.pr_firma_doc_posfactura(pn_ord_id => :p30_ord_id,',
'                                               pn_cli_id => :p30_cli_id,',
'                                               pn_emp_id => :f_emp_id,',
'                                               pn_resultado => ln_resultado,',
'                                               pv_observacion => lv_observacion,',
'                                               pn_uge_id => :f_uge_id,',
'                                               pv_proveedor => ''FSD'',',
'                                               pv_error => :pv_error);',
'        IF nvl(ln_resultado,0) = 0 then',
'          RAISE_APPLICATION_ERROR (-20000,''ERROR: ''||lv_observacion);',
'        end if;',
'      END IF;',
'  ELSE',
'  ',
'    SELECT MAX(co.seq_id)',
'      INTO ln_seq_id',
'      FROM apex_collections co',
'     WHERE collection_name = ''CO_MOV_CAJA''',
'       AND to_number(c005) =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_tfp_id_tarjeta_credito'');',
'  ',
'    pq_inv_movimientos.pr_borra_reg_colecciones(ln_seq_id, ''CO_MOV_CAJA'');',
'    :p0_error := lv_error;',
'    raise_application_error(-20000, lv_error);',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p30_ini = ''INI'' and :p30_com_id is null '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>'REALIZO'
,p_internal_uid=>254905509460744871
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'and :request = ''Graba_fac_pag''',
'',
'pr_graba_factura_pago_cuota'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(27796198939070220944)
,p_process_sequence=>110
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_cabrcera_pistoleo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
' ',
'  pq_lectores_slm.pr_graba_lista_factura(pn_ord_id => :p30_ord_id,',
'                                         pn_emp_id => :f_emp_id,',
'                                         pn_tse_id => :f_seg_id,',
'                                         pn_user_id => 1,--:p30_f_user_id,',
'                                         pv_error => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':p30_ini = ''INI'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>27763945787800456018
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38124737199212203053)
,p_process_sequence=>120
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_garantias'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_intangibles.pr_crea_col_orden_gar(pn_ord_id => :p30_ord_id,',
'                                          pn_emp_id => :f_emp_id,',
'                                          pc_error => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>38092484047942438127
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(55050476983916206036)
,p_process_sequence=>130
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_datos_bono'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_contador NUMBER;',
'BEGIN',
'',
'',
'  --raise_application_error(-20000,''TESTB'');',
'    SELECT COUNT(s.gsi_valor_bono)',
'      INTO ln_contador',
'      FROM ven_garantias_siniestros s,',
'           ven_garantias_cab        ca,',
'           ven_garantias_det        de',
'     WHERE ca.cli_id = :p30_cli_id',
'       AND ca.gca_id = s.gca_id',
'       AND ca.gca_estado_bono = ''N''',
'       AND s.gsi_estado = ''A''',
'       AND de.gca_id = ca.gca_id',
'       AND de.gde_tipo = ''OREB'' -- ILIMA PAOBERNAL 01/FEBRERO/2019 -- CAMBIAMOS FEB POR OREB',
'       AND de.ord_id = :p30_ord_id;',
'    IF ln_contador > 0 THEN',
'    --if apex_collection.collection_exists(''COL_BONOS_APLICAR'') = FALSE THEN',
'    pq_ven_intangibles.pr_obtener_datos_bono(pn_cli_id => :p30_cli_id,',
'                                               pn_ord_id => :p30_ord_id,',
'                                               pc_error  => :p0_error);',
'   -- END IF;                                           ',
'   --:p30_tfp_id := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_bono_hogar'');',
'      --Etenesaca 05-07-2018 COmpra Protegida - Bono Hogar',
'      ',
'    else',
'      IF apex_collection.collection_exists(''COL_BONOS_APLICAR'') THEN',
'        apex_collection.delete_collection(''COL_BONOS_APLICAR'');',
'      END IF;',
'    END IF;',
'     --   raise_application_error(-20000,''p30_valor_total_pagos:''||:p30_valor_total_pagos||'' p30_valor_bono:''||:p30_valor_bono);',
'   ',
' ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>55018223832646441110
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(18248905567098787557)
,p_process_sequence=>150
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_estado_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_com_id number;',
'  ln_cli_id number;',
'',
'  cv_estado_reg_activo varchar2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                         ''cv_estado_reg_activo'');',
'  lv_error             varchar2(3000);',
'begin',
'',
'  select co.cli_id, co.com_id',
'    into ln_cli_id, ln_com_id',
'    from ven_comprobantes co',
'   where co.ord_id = :p30_ord_id',
'     and co.com_estado_registro = cv_estado_reg_activo;',
'',
'/*  pq_car_cartera.pr_estado_cliente_venta(pn_com_id => ln_com_id,',
'                                         pn_cli_id => ln_cli_id,',
'                                         pn_usu_id => :f_user_id,',
'                                         pn_emp_id => :f_emp_id,',
'                                         pn_uge_id => :f_uge_id,',
'                                         pn_ord_id => :p30_ord_id,',
'                                         pv_error  => :p0_error);  */',
'                                         ',
'                                            ',
'begin',
'  -- Call the procedure',
'  pq_car_credito_dml.pr_upd_asdm_estado_cliente_ven(pn_com_id => ln_com_id,',
'                                                    pn_ord_id => :p30_ord_id,',
'                                                    pv_error => :pv_error);',
'end;',
'                                            ',
'                                            ',
'',
'  :p30_ini  := ''FIN'';',
'  :p30_ini2 := ''FIN2'';',
'',
'exception',
'  when no_data_found then',
'    null;',
'  when others then',
'  ',
'    :p30_ini  := ''FIN'';',
'    :p30_ini2 := ''FIN2'';',
'    lv_error  := sqlerrm;',
'    raise_application_Error(-20000, :p0_error || '' - '' || lv_error);',
'  ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p30_facturar = ''S'' AND :p30_ini = ''INI'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>18216652415829022631
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(268485952680510883)
,p_process_sequence=>160
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'BEGIN',
'',
':P0_TTR_DESCRIPCION := NULL;',
':P0_DATFOLIO := NULL;',
':P0_FOL_SEC_ACTUAL := NULL;',
':P0_PERIODO := null;',
'',
'IF :P30_FACTURAR = ''S'' THEN',
'   :P0_TTR_ID := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id); ',
'   ln_tgr_id  := pq_ven_listas_caja.fn_tipo_grupo_transaccion_seg (:f_emp_id, :f_seg_id); ',
'ELSE',
'   ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja'');',
'   :P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota'');',
'',
'   -- Yguaman 2012/01/23. Para precancelacion',
'   IF NVL(:P30_ES_PRECANCELACION,''N'') = ''S'' THEN',
'       :P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota_precancelacion''); ',
'   END IF; ',
'END IF;',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'                       :F_EMP_ID,',
'                       :F_PCA_ID,',
'                        LN_TGR_ID,',
'                       :P0_TTR_DESCRIPCION, ',
'                       :P0_PERIODO,',
'                       :P0_DATFOLIO,',
'                       :P0_FOL_SEC_ACTUAL,',
'                       :P0_PUE_NUM_SRI,',
'                       :P0_UGE_NUM_EST_SRI,',
'                       :P0_PUE_ID,',
'                       :P0_TTR_ID,',
'                       :P0_NRO_FOLIO,',
'                       :P0_ERROR);',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>236232801410745957
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133254616696604454)
,p_process_sequence=>170
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_retorna_datos_rpagos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  /* Creacion: 20-jul-2022',
'  Autor: John Alvarez',
unistr('  Descripcion: Procedimiento para repurar la informaci\00F3n de un proceso de pago procesado'),
'  Historia: */',
'BEGIN',
'',
'  -- Call the procedure',
'  pq_car_redes_pago.pr_carga_datos_tarjeta(pn_emp_id => :f_emp_id,',
'                                           pn_vueltos => :p30_vueltos,',
'                                           pn_cli_id => :p30_cli_id,',
'                                           pn_uge_id => :f_uge_id,',
'                                           pn_rpr_id => :p30_rpr_id,',
'                                           pn_rpc_id => :p30_rpc_id,',
'                                           pv_error => :p0_error);',
'',
':p30_tfp_id:=0;',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    NULL;',
'END;',
'',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'UTILIZAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101001465426839528
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(40552162718725470)
,p_process_sequence=>180
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Pr_cargar_datos_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_pinpad NUMBER;',
'BEGIN',
'  SELECT COUNT(1)',
'    INTO ln_pinpad',
'    FROM car_redes_pago_pue p',
'   WHERE p.rpp_estado_registro = 0',
'     AND p.uge_id = :f_uge_id;',
'',
'  IF ln_pinpad > 0 THEN',
'    :p30_bancos  := NULL;',
'    :p30_tarjeta := NULL;',
'    :p30_tipo    := NULL;',
'    :p30_forma   := NULL;',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>8299011448960544
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(187975038016354742)
,p_process_sequence=>200
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reveso_pago_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pv_error VARCHAR2(1000);',
'BEGIN',
'',
'pq_car_redes_pago.pr_requerimiento_reverso_pago(pn_pue_id => :p0_pue_id,',
'                                                  pn_cli_id => :p30_cli_id,',
'                                                  pn_rpc_id => :p30_rpc_id,',
'                                                  pn_rpr_id_req => :p30_rpr_id_req,',
'                                                  pv_tipo => ''A'',',
'                                                  pv_error => :p0_error);',
'  IF pv_error IS NULL THEN',
'    apex_collection.delete_member(''CO_MOV_CAJA'', :p30_seq_id_movcaja);',
'  ELSE',
'    :p0_error := pv_error;',
'  END IF;',
'   :p30_rpc_id:=null;                                                 ',
' :p30_rpr_id_req :=null;  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'reverso_tarjeta'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>155721886746589816
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(144293603221554759)
,p_process_sequence=>210
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Pr_imprimir_voucher'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_car_redes_pago.pr_imprimir_voucher(pn_rpc_id => :p30_rpc_id,',
'                                        pn_emp_id => :F_emp_id,',
'                                        pv_tipo => :P30_TIPO_IMPRESION,',
'                                        pv_error => :p0_error);',
'                       ',
' :P30_TIPO_IMPRESION:=null;',
'  :p30_rpc_id:=null;                                                 ',
' :p30_rpr_id_req :=null;  '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>112040451951789833
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(187976307995354755)
,p_process_sequence=>220
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_seq_id number;',
'begin',
'',
'select seq_id',
'into ln_seq_id',
'from apex_collections where collection_name = ''CO_MOV_CAJA''',
'and seq_id = :P30_SEQ_ID_MOVCAJA;',
'if ln_seq_id > 0 then',
'apex_collection.delete_member(''CO_MOV_CAJA'', :P30_SEQ_ID_MOVCAJA);',
'end if;',
':P30_VUELTO :=null;',
':P30_MCD_VALOR_MOVIMIENTO := null;',
'exception when no_data_found then null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ELIMINA_MOVCAJA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>155723156725589829
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109016871158606782)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_anticipo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--- VALIDA QUE EL VALOR A PAGAR CON ANTICIPO NO SOBREPASE EL TOTAL DE ANTICIPOS',
'pq_ven_pagos_cuota.pr_validar_pago_anticipo(pn_emp_id => :F_EMP_ID,',
'                                     pn_cli_id => :P30_CLI_ID, ',
'                                     pn_antic_ing => :P30_MCD_VALOR_MOVIMIENTO,',
'                                     pn_uge_id  => :f_uge_id,--agregado ac',
'                                     pv_error  => :P0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and',
':P30_TFP_ID =pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'') and :P0_ERROR is null'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>76763719888841856
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15337642433733025365)
,p_process_sequence=>2
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_total_condonacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if to_number(:P30_TOTAL_PAGO_CONDONACION) > to_number(:P30_SALDO_CONDONACION)   then',
'',
'raise_application_error(-20000, ''El valor del pago de condonacion '' || to_number(:P30_TOTAL_PAGO_CONDONACION) || '' no debe ser mayor al saldo de Condonacion '' ||',
'                       to_number(:P30_SALDO_CONDONACION));',
'',
'end if;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(cc.ccc_saldo)',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p30_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_process_when_type=>'EXISTS'
,p_internal_uid=>15305389282463260439
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1528886960087627754)
,p_process_sequence=>3
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_subtotal'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_subtotal NUMBER;',
'  ln_flete    NUMBER;',
'  ',
'  cn_var_id_subtotal_con_iva NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                               ''cn_var_id_subtotal_con_iva'');',
'                                               ',
'  cn_var_id_subtotal_sin_iva NUMBER :=    pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                               ''cn_var_id_subtotal_sin_iva'');       ',
'                                               ',
' cn_var_id_ser_manejo_produ  NUMBER :=     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_var_id_servicio_manejo_producto'');                                                                                   ',
'',
'BEGIN',
'',
'  BEGIN',
'  ',
'    SELECT SUM(a.vor_valor_variable)',
'      INTO ln_subtotal',
'      FROM ven_var_ordenes a',
'     WHERE a.ord_id = :P30_ord_id',
'       AND a.emp_id = :f_emp_id',
'       AND a.var_id IN',
'           (cn_var_id_subtotal_con_iva,',
'            cn_var_id_subtotal_sin_iva);',
'  ',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ln_subtotal := 0;',
'  END;',
'',
'  BEGIN',
'  ',
'    SELECT a.vor_valor_variable',
'      INTO ln_flete',
'      FROM ven_var_ordenes a',
'     WHERE a.ord_id = :P30_ord_id',
'       AND a.emp_id = :f_emp_id',
'       AND a.var_id = cn_var_id_ser_manejo_produ;',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ln_flete := 0;',
'  END;',
'',
'  --raise_application_error(-20000, ''ln_subtotal: '' || ln_subtotal || '' ln_flete: '' || ln_flete);',
'',
'  :P30_SUBTOTAL := nvl(ln_subtotal, 0) - nvl(ln_flete, 0);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'limpia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1496633808817862828
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(95673377383716170)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   ln_comprobante ven_comprobantes.com_id%TYPE;',
'',
'begin',
'',
'IF :P30_FACTURAR = ''S'' THEN',
'      ln_comprobante := :P30_ORD_ID;',
'ELSE',
'      ln_comprobante := :P30_RAP_COM_ID;',
'END IF;',
'',
'--pruebas_car_aux(pn_number =>:P30_MCD_VALOR_MOVIMIENTO ,pv_dato => '':P30_MCD_VALOR_MOVIMIENTO: '' || :P30_MCD_VALOR_MOVIMIENTO);',
'',
'pq_ven_pagos_cuota.pr_valida_pago_retencion(pn_nro_retencion        => :P30_RAP_NRO_RETENCION,',
'                         pn_emp_id               => :F_EMP_ID,',
'                         pn_com_id               => ln_comprobante,',
'                         pv_facturar             => :P30_FACTURAR, --- Para ver si viene de la facturacion o del pago de cuota                  ',
'                         pv_RAP_NRO_ESTABL_RET   => :P30_RAP_NRO_ESTABL_RET ,',
'                         pv_RAP_NRO_PEMISION_RET => :P30_RAP_NRO_PEMISION_RET,',
'                         pn_saldo_retencion      => :P30_SALDO_RETENCION,',
'                         pn_mcd_valor_movimiento => :P30_MCD_VALOR_MOVIMIENTO,',
'                         pn_valor_total_pagos    => :P30_VALOR_TOTAL_PAGOS, -- Usado cuando viene de facturacion',
'                         pv_modificacion         => :P30_MODIFICACION,',
'                         pn_pre_id               => :P30_PRE_ID,',
'                         pv_dejar_saldo          => ''S'',',
'                         PV_RAP_NRO_AUTORIZACION => :P30_RAP_NRO_AUTORIZACION,',
'                         pv_error                => :P0_error);',
'',
'/*exception',
'when others then',
'  raise_application_error(-20000,''Error en la validacion'' || :P0_error);*/',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and :p30_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'') and :P0_ERROR is null'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>63420226113951244
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109909377890529641)
,p_process_sequence=>6
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_total_a_pagar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_pagos_cuota.pr_validar_pago(pn_emp_id      => :F_emp_id,',
'                                   pn_seq_id      => :P30_SEQ_ID,',
'                                   pn_tfp_id      => :P30_TFP_ID,',
'                                   pn_valor_ing   => :P30_MCD_VALOR_MOVIMIENTO,',
'                                   pn_total_pagar => :P30_VALOR_TOTAL_PAGOS,',
'                                   pv_error       => :P0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>77656226620764715
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(96067365641882707)
,p_process_sequence=>8
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_Actualiza_Coleccion_Movimiento_Detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF NVL(:P30_ES_PRECANCELACION,''N'') <> ''S'' THEN',
'  --- Valida si tiene anticipo de clientes, a obligarlo a aplicar el pago con anticipo',
'  pq_ven_pagos_cuota.pr_advertencia_anticipos(pn_emp_id    => :f_emp_id,',
'                                              pn_cli_id    => :p30_cli_id,',
'                                              pn_antic_ing => :P30_MCD_VALOR_MOVIMIENTO,',
'                                              pn_tfp_id    => :P30_TFP_ID,',
'                                              pn_seq_id    => :p30_seq_id,',
'                                              pn_facturar  => :p30_facturar,',
'                                              pn_uge_id    => :f_uge_id,',
'                                              pv_error     => :p0_error);',
' END IF;',
'',
'  IF :p0_error IS NOT NULL THEN',
'   -- Porq no salia el error en pantalla',
'   raise_application_error(-20000, :p0_error);',
'  ELSE',
'    /*Agregado por Andres Calle',
'    07/septiembre/2011',
'    Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'    */',
'    IF :p30_tfp_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_tarjeta_credito'') THEN',
'      :p30_ede_id := :p30_plan;',
'    END IF;',
'  ',
'    /*Agregado por Andres Calle',
'    14/Octubre/2011',
'    valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'    */',
'    IF :p30_comision IS NOT NULL and :P30_ES_POLITICA_GOBIERNO = 0 THEN',
'      pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                  pn_ede_id   => :p30_ede_id,',
'                                                  pn_comision => :p30_comision,',
'                                                  pv_error    => :p0_error);',
'    END IF;',
'  ',
unistr('    ---Yguaman 2011/10/05 - 06 Se a\00C3\00B1adieron parametros en pq_ven_movimientos_caja.pr_actualiza_col_mov_caja para pago de cuota con retenci\00C3\00B3n'),
'  ',
'    pq_ven_movimientos_caja.pr_actualiza_col_mov_caja(pn_ede_id               => :p30_ede_id,',
'                                                      pn_cre_id               => :p30_cre_id, --null,',
'                                                      pn_tfp_id               => :p30_tfp_id,',
'                                                      pn_mcd_valor_movimiento => nvl(to_number(:p30_mcd_valor_movimiento),',
'                                                                                     0),',
'                                                      pn_mcd_nro_aut_ref      => :p30_mcd_nro_aut_ref,',
'                                                      pn_mcd_nro_lote         => :p30_lote,',
'                                                      pn_mcd_nro_retencion    => :p30_rap_nro_retencion,',
'                                                      pn_mcd_nro_comprobante  => :p30_mcd_nro_comprobante,',
'                                                      pv_titular_cuenta       => :p30_cre_titular_cuenta, -- :P30_TJ_TITULAR',
'                                                      pv_cre_nro_cheque       => :p30_cre_nro_cheque,',
'                                                      pv_cre_nro_cuenta       => :p30_cre_nro_cuenta,',
'                                                      pv_nro_pin              => :p30_cre_nro_pin,',
'                                                      pv_nro_tarjeta          => :p30_tj_numero,',
'                                                      pv_mcd_voucher          => :p30_tj_numero_voucher,',
'                                                      pn_seq_id               => :p30_seq_id,',
unistr('                                                      -----Para pago de cuota con retenci\00C3\00B3n'),
'                                                      pn_saldo_retencion      => nvl(to_number(:p30_saldo_retencion),',
'                                                                                     0), ---- yguaman 2011/10/05 ',
'                                                      pn_com_id               => :p30_rap_com_id, ---- yguaman 2011/10/05',
'                                                      pd_rap_fecha            => :p30_rap_fecha, --- Yguaman 2011/10/05 ',
'                                                      pd_rap_fecha_validez    => :p30_rap_fecha_validez, --- Yguaman 2011/10/05',
'                                                      pn_rap_nro_autorizacion => :p30_rap_nro_autorizacion, -- Yguaman 2011/10/05',
'                                                      pn_rap_nro_establ_ret   => :p30_rap_nro_establ_ret, -- Yguaman  2011/10/06',
'                                                      pn_rap_nro_pemision_ret => :p30_rap_nro_pemision_ret, -- Yguaman  2011/10/06',
'                                                      pn_pre_id               => :p30_pre_id, -- Yguaman  2011/10/06 ',
'                                                      pd_fecha_deposito       => :p30_mcd_fecha_deposito,',
'                                                      pn_emp_id               => :f_emp_id, --- Yguaman 2011/10/06  ',
'                                                      pv_titular_tarjeta_cre  => :p30_titular_tc, -- Andres Calle',
'                                                      pv_error                => :p0_error);',
'  ',
'    :p30_seq_id               := NULL;',
'    :p30_ede_id               := NULL;',
'    :p30_tfp_id               := NULL;',
'    :p30_mcd_valor_movimiento := NULL;',
'    :p30_tj_titular           := NULL;',
'    :p30_cre_nro_cheque       := NULL;',
'    :p30_cre_nro_cuenta       := NULL;',
'    :p30_cre_nro_pin          := NULL;',
'    :p30_mcd_nro_aut_ref      := NULL;',
'    :p30_tj_numero_voucher    := NULL;',
'    :p30_mcd_nro_comprobante  := NULL;',
'    :p30_tj_numero            := NULL;',
'    :p30_plan                 := NULL;',
'    :p30_mcd_fecha_deposito   := NULL;',
'  ',
'    -- NULL RETENCIONES ---',
'    :p30_rap_nro_retencion    := NULL; -- yguaman 2011/10/05 ',
'    :p30_saldo_retencion      := NULL; -- yguaman 2011/10/05 ',
'    :p30_rap_com_id           := NULL; -- yguaman 2011/10/05',
'    :p30_rap_fecha            := NULL; -- Yguaman 2011/10/05 ',
'    :p30_rap_fecha_validez    := NULL; -- Yguaman 2011/10/05',
'    :p30_rap_nro_autorizacion := NULL; -- yguaman 2011/10/05',
'    :p30_rap_nro_establ_ret   := NULL; --Yguaman  2011/10/06',
'    :p30_rap_nro_pemision_ret := NULL; --Yguaman  2011/10/06',
'    :p30_pre_id               := NULL; -- Yguaman  2011/10/06',
'  ',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P30_SEQ_ID IS NOT NULL and :request = ''cargar_tfp'' and :P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>63814214372117781
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(46753651370572990)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_imp.id(46749483434572957)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_PROMOCIONES_ORDENES'
,p_attribute_03=>'POR_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_imp.id(46750954162572982)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
,p_internal_uid=>14500500100808064
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(268485183515510883)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar     NUMBER;',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
unistr('  ln_valor_retencion  asdm_retenciones_aplicadas.rap_valor_retencion%TYPE; -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n'),
'',
'  lv_col_val_retencion VARCHAR2(50) := ''COLL_VALOR_RETENCION'';',
'',
'  ln_cuenta  NUMBER;',
'  lv_anio    VARCHAR2(4) := ''2011'';',
'  lv_mensaje VARCHAR2(3000) := NULL;',
'  ln_rpr_id  NUMBER;',
'',
'  raise_puntaje EXCEPTION;',
'  raise_pinpad EXCEPTION;',
'  lv_col_det_retencion  VARCHAR2(50) := ''COLL_DET_RETENCION'';',
'  ln_valor_par_anticipo NUMBER;',
'  ln_valor_anticipo     NUMBER;',
'',
'  CURSOR cur_datos_col IS',
'    SELECT to_number(co.c001) com_id,',
'           co.c002 fecha_ret,',
'           co.c003 fecha_validez,',
'           co.c004 nro_ret,',
'           co.c005 nro_autoriza,',
'           co.c006 nro_establecimiento,',
'           co.c007 nro_pemision,',
'           co.c008 porcentaje,',
'           co.c009 base,',
'           co.c010 valor_ret,',
'           to_number(co.c011) uge_id,',
'           to_number(co.c012) tse_id,',
'           blob001 xml',
'      FROM apex_collections co',
'     WHERE co.collection_name = lv_col_val_retencion;',
'',
'  ln_valor_dev NUMBER;',
'',
'BEGIN',
'  ln_valor_retencion  := 0;',
'  ln_com_id_retencion := NULL;',
'',
'  IF :p30_modificacion IS NULL THEN',
'  ',
'    IF :p30_tfp_id = 50 THEN',
'      IF to_number(:p30_mcd_valor_movimiento) >',
'         to_number(:p30_saldo_promo_dev) THEN',
'        :p30_mcd_valor_movimiento := :p30_saldo_promo_dev;',
'      ',
'        RAISE raise_puntaje;',
'      ',
'      END IF;',
'    ',
'    END IF;',
'  ',
'    /*Agregado por Andres Calle',
'    07/septiembre/2011',
'    Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'    */',
'    IF :p30_tfp_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_tarjeta_credito'') THEN',
'    ',
'      :p30_ede_id             := :p30_plan;',
'      :p30_cre_titular_cuenta := :p30_titular_tc; -- Para que se muestre en el mismo titular del cheque pero igual se guarde en el titular de TC',
unistr('      ---JALVAREZ 05SEP2022  Req: MPO-2 Integraci\00F3n Medianet - AJUSTE EN PANTALLA '),
'      IF :p30_tipo_pago_tj = ''M'' THEN',
'        IF :p30_tj_numero IS NULL THEN',
unistr('          lv_mensaje := ''El n\00FAmero de tarjeta esta en blanco'';'),
'        END IF;',
'      ',
'        IF :p30_tj_numero_voucher IS NULL THEN',
'          IF lv_mensaje IS NULL THEN',
unistr('            lv_mensaje := ''El n\00FAmero de voucher esta en blanco'';'),
'          ELSE',
'            lv_mensaje := lv_mensaje ||',
unistr('                          '', el n\00FAmero de voucher esta en blanco'';'),
'          END IF;',
'        END IF;',
'      ',
'        IF :p30_mcd_nro_aut_ref IS NULL THEN',
'          IF lv_mensaje IS NULL THEN',
unistr('            lv_mensaje := ''El n\00FAmero de autorizaci\00F3n esta en blanco'';'),
'          ELSE',
'            lv_mensaje := lv_mensaje ||',
unistr('                          '', el n\00FAmero de autorizaci\00F3n esta en blanco'';'),
'          END IF;',
'        END IF;',
'      END IF;',
'      IF lv_mensaje IS NULL THEN',
'        NULL;',
'        IF :p30_tipo_pago_tj = ''A'' THEN',
'          IF :p30_pinpad IS NULL THEN',
'            BEGIN',
'              SELECT rpp.rpp_id',
'                INTO :p30_pinpad',
'                FROM car_redes_pago_pue rpp',
'               WHERE rpp.pue_id = :p0_pue_id',
'                 AND rpp.rpp_estado_registro = 0;',
'            EXCEPTION',
'              WHEN OTHERS THEN',
'                :p30_pinpad := NULL;',
'            END;',
'          END IF;',
'          IF :p30_pinpad IS NOT NULL THEN',
'            pq_car_redes_pago.pr_procesar_pago_caja(pn_rpp_id_pinpad => :p30_pinpad,',
'                                                    pn_rpa_id_red    => :p30_red,',
'                                                    pn_cli_id        => :p30_cli_id,',
'                                                    pn_uge_id        => :f_uge_id,',
'                                                    pn_emp_id        => :f_emp_id,',
'                                                    pn_ede_id        => :p30_ede_id,',
'                                                    pn_usu_id        => :f_user_id,',
'                                                    pn_total         => nvl(to_number(:p30_mcd_valor_movimiento),',
'                                                                            0),',
'                                                    pn_rpr_id        => ln_rpr_id,',
'                                                    pv_sujeto_a_iva  => ''S'',',
'                                                    pn_ord_id        => :P30_ORD_ID,',
'                                                    pv_tipo          => :p30_tipo_impresion,',
'                                                    pn_rpc_id        => :p30_rpc_id,',
'                                                    pv_error         => lv_mensaje);',
'                                                    ',
'',
'            IF lv_mensaje IS NULL THEN',
'              pq_car_redes_pago.pr_recuperar_datos_tj(pn_rpr_id           => ln_rpr_id,',
'                                                      pv_titular          => :p30_titular_tc,',
'                                                      pv_tarjeta          => :p30_tarjeta_emi,',
'                                                      pv_num_tarjeta      => :p30_tj_numero,',
'                                                      pv_num_voucher      => :p30_tj_numero_voucher,',
'                                                      pv_tj_recap         => :p30_tj_recap,',
'                                                      pv_num_autorizacion => :p30_mcd_nro_aut_ref,',
'                                                      pv_error            => lv_mensaje);',
'            ',
'              :p30_tj_numero_1         := :p30_tj_numero;',
'              :p30_tj_numero_voucher_1 := :p30_tj_numero_voucher;',
'              :p30_tj_recap_1          := :p30_tj_recap;',
'              :p30_mcd_nro_aut_ref_1   := :p30_mcd_nro_aut_ref;',
'              :p30_lote                := :p30_tj_recap;',
'              :p30_mcd_nro_comprobante := :p30_tj_numero_voucher;',
'            ',
'            END IF;',
'          ELSE',
'            RAISE raise_pinpad;',
'            :p0_error := ''No tiene asignado un pinpad'';',
'          END IF;',
'        END IF;',
'      END IF;',
'    END IF;',
'  ',
'    /*Agregado por Andres Calle',
'    14/Octubre/2011',
'    valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'    */',
'    IF :p30_tfp_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_tarjeta_credito'') AND',
'       :p30_comision > 0 THEN',
'      pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                  pn_ede_id   => :p30_ede_id,',
'                                                  pn_comision => :p30_comision,',
'                                                  pv_error    => :p0_error);',
'    ',
'    END IF;',
'  ',
'    IF nvl(:p30_es_precancelacion, ''N'') <> ''S''',
'      --- Yguaman 2012/03/28',
'       AND :f_seg_id =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_minoreo'') THEN',
'      --- Valida si tiene anticipo de clientes, a obligarlo a aplicar el pago con anticipo',
'      pq_ven_pagos_cuota.pr_advertencia_anticipos(pn_emp_id    => :f_emp_id,',
'                                                  pn_cli_id    => :p30_cli_id,',
'                                                  pn_antic_ing => 0,',
'                                                  pn_tfp_id    => NULL,',
'                                                  pn_seq_id    => :p30_seq_id,',
'                                                  pn_facturar  => :p30_facturar,',
'                                                  pn_uge_id    => :f_uge_id,',
'                                                  pv_error     => :p0_error);',
'    ',
'    END IF;',
'    IF :p0_error IS NULL THEN',
'    ',
'      pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                      pn_tfp_id => :p30_tfp_id,',
'                                                      pv_error  => :p0_error);',
'    ',
'      IF :p0_error IS NULL THEN',
unistr('        -- Agregado por Yguaman 2011/11/17, porque a pesar del error de formas de pago igual graba en la colecci\00C3\00B3n movimientos'),
'      ',
unistr('        --- Yguaman 2011/10/04 11:59am a\00C3\00B1adido para guardar nueva forma de pago "retenci\00C3\00B3n"'),
'        IF :p30_tfp_id =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_tfp_id_retencion'') THEN',
'        ',
'          ln_valor_retencion := :p30_mcd_valor_movimiento;',
'        ',
'          apex_collection.create_or_truncate_collection(lv_col_det_retencion);',
'        ',
'          FOR c IN cur_datos_col LOOP',
'          ',
'            apex_collection.add_member(p_collection_name => lv_col_det_retencion,',
'                                       p_c001            => c.com_id,',
'                                       p_c002            => c.fecha_ret,',
'                                       p_c003            => c.fecha_validez,',
'                                       p_c004            => c.nro_ret,',
'                                       p_c005            => c.nro_autoriza,',
'                                       p_c006            => c.nro_establecimiento,',
'                                       p_c007            => c.nro_pemision,',
'                                       p_c008            => c.porcentaje,',
'                                       p_c009            => c.base,',
'                                       p_c010            => c.valor_ret,',
'                                       p_c011            => c.uge_id,',
'                                       p_c012            => c.tse_id,',
'                                       p_blob001         => c.xml);',
'          ',
'          END LOOP;',
'        ',
'          /* begin       ',
'            -- Call the procedure             ',
'            pq_ven_pagos_cuota_retencion.pr_carga_retencion(pn_emp_id  => :f_emp_id,      ',
'                                                            pv_tipo    => ''P'',      ',
'                                                            pv_session => NULL,       ',
'                                                            pv_error   => :p0_error);       ',
'          end;*/',
'        ',
'          IF :p30_facturar = ''S'' THEN',
'            ln_com_id_retencion := :p30_ord_id; --:p30_com_id;',
'          ELSE',
'            ln_com_id_retencion := :p30_rap_com_id;',
'          END IF;',
'        END IF;',
'      ',
'        --raise_application_error(-20000, '' :P30_MCD_VALOR_MOVIMIENTO = '' || :P30_MCD_VALOR_MOVIMIENTO || '' :P30_SALDO_RETENCION = '' || :P30_SALDO_RETENCION);',
'        --agregado por Andres Calle',
'        --voy a validar que el cheque que estan queriendo registrar no esta ya ingresado;',
'        SELECT COUNT(*)',
'          INTO ln_cuenta',
'          FROM asdm_cheques_recibidos r',
'         WHERE r.cre_nro_cheque = nvl(:p30_cre_nro_cheque, 0)',
'           AND r.cre_nro_cuenta = nvl(:p30_cre_nro_cuenta, 0)',
'           AND r.cre_nro_pin = nvl(:p30_cre_nro_pin, 0)',
'           AND r.ech_id = 2;',
'      ',
'        IF ln_cuenta > 0 THEN',
'          raise_application_error(-20000,',
'                                  ''EL CHEQUE'' || :p30_cre_nro_cheque ||',
'                                  '' cuenta '' || :p30_cre_nro_cuenta ||',
'                                  '' pin '' || :p30_cre_nro_pin ||',
'                                  '' YA ESTA REGISTRADO NO PUEDE VOLVER A PAGAR CON EL MISMO CHEQUE'');',
'        END IF;',
'        IF lv_mensaje IS NULL THEN',
'',
'          pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => :p30_ede_id,',
'                                                              pn_cre_id               => :p30_cre_id,',
'                                                              pn_tfp_id               => :p30_tfp_id,',
'                                                              pn_mcd_valor_movimiento => nvl(to_number(:p30_mcd_valor_movimiento),',
'                                                                                             0), --:p30_mcd_valor_movimiento',
'                                                              pn_mcd_nro_aut_ref      => :p30_mcd_nro_aut_ref,',
'                                                              pn_mcd_nro_lote         => :p30_tj_recap,',
'                                                              pn_mcd_nro_retencion    => :p30_rap_nro_retencion,',
'                                                              pn_mcd_nro_comprobante  => :p30_mcd_nro_comprobante, --NULL,',
'                                                              pv_titular_cuenta       => :p30_cre_titular_cuenta, --',
'                                                              pv_cre_nro_cheque       => :p30_cre_nro_cheque,',
'                                                              pv_cre_nro_cuenta       => :p30_cre_nro_cuenta,',
'                                                              pv_nro_pin              => :p30_cre_nro_pin,',
'                                                              pv_nro_tarjeta          => :p30_tj_numero,',
'                                                              pv_voucher              => :p30_tj_numero_voucher,',
'                                                              pn_saldo_retencion      => nvl(to_number(:p30_saldo_retencion),',
unistr('                                                                                             0), --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pn_com_id               => ln_com_id_retencion, --:P30_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pd_rap_fecha            => :p30_rap_fecha, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pd_rap_fecha_validez    => :p30_rap_fecha_validez, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pn_rap_nro_autorizacion => :p30_rap_nro_autorizacion, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pn_rap_nro_establ_ret   => :p30_rap_nro_establ_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pn_rap_nro_pemision_ret => :p30_rap_nro_pemision_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                              pn_pre_id               => :p30_pre_id, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                              pd_fecha_deposito       => :p30_mcd_fecha_deposito,',
unistr('                                                              --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                              pv_titular_tarjeta_cre => :p30_titular_tc, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito                                                      ',
'                                                              pn_emp_id              => :f_emp_id,',
unistr('                                                              pn_nro_recap           => :p30_tj_recap,--JALVAREZ 05SEP2022  Req: MPO-3 Integraci\00F3n Medianet - AJUSTE EN PANTALLA DE PAGO DE CUOTA. PARA GUARDAR EL REGISTRO CON EL QUE SE REGISTRO EL PAGO'),
unistr('                                                              pn_rpr_id              => ln_rpr_id,--JALVAREZ 05SEP2022  Req: MPO-3 Integraci\00F3n Medianet - AJUSTE EN PANTALLA DE PAGO DE CUOTA. PARA GUARDAR EL REGISTRO CON EL QUE SE REGISTRO EL PAGO'),
'                                                              pv_error               => :p0_error);',
'        ',
'          --- Null los campos ---',
'        ',
'          :p30_seq_id               := NULL;',
'          :p30_cre_id               := NULL;',
'          :p30_cre_fecha_deposito   := SYSDATE;',
'          :p30_ede_id               := NULL;',
'          :p30_cre_titular_cuenta   := NULL;',
'          :p30_cre_nro_cheque       := NULL;',
'          :p30_cre_nro_cuenta       := NULL;',
'          :p30_cre_nro_pin          := NULL;',
'          :p30_mcd_valor_movimiento := NULL;',
'        ',
unistr('          --- Yguaman 2011/10/04 19:32pm usado para el pago con retenci\00C3\00B3n y deja en null todos los campos'),
'          :p30_mcd_fecha_deposito := NULL;',
'          :p30_tj_entidad         := NULL;',
'          :p30_bancos             := NULL;',
'          :p30_tarjeta            := NULL;',
'          :p30_tipo               := NULL;',
'          :p30_forma              := NULL;',
'          :p30_plan               := NULL;',
'          --  :P30_TJ_TITULAR := NULL;',
'          :p30_tj_numero           := NULL;',
'          :p30_tj_numero_voucher   := NULL;',
'          :p30_tj_recap            := NULL;',
'          :p30_lote                := NULL;',
'          :p30_mcd_nro_aut_ref     := NULL;',
'          :p30_mcd_nro_comprobante := NULL;',
'          :p30_modificacion        := NULL;',
'          :p30_mcd_fecha_deposito  := NULL;',
'        ',
'          --- Retencion',
'          :p30_rap_nro_retencion    := NULL;',
'          :p30_rap_nro_autorizacion := NULL;',
'          :p30_rap_fecha            := NULL;',
'          :p30_rap_fecha_validez    := NULL;',
'          :p30_rap_com_id           := NULL;',
'          :p30_saldo_retencion      := NULL;',
unistr('          :p30_rap_nro_establ_ret   := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('          :p30_rap_nro_pemision_ret := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('          :p30_pre_id               := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'        ',
'          -- tarjetas de credito',
'                    -- tarjetas de credito',
'          :p30_tipo_pago_tj        := ''M'';',
'          :p30_titular_tc          := NULL;',
'          :p30_tarjeta_emi         := NULL;',
'          :p30_tj_numero_1         := NULL;',
'          :p30_tj_numero_voucher_1 := NULL;',
'          :p30_tj_recap_1          := NULL;',
'          :p30_mcd_nro_aut_ref_1   := NULL;',
unistr('          -- este campo lo lleno solo desde la facturaci\00C3\00B3n, cuando se factura una orden con tarjeta envio directamente la entrada y luego la vacio'),
'          :p30_entrada := NULL;',
'        ELSE',
'          :p0_error := lv_mensaje;',
'        ',
'        END IF;',
'      END IF;',
'    END IF;',
'  ELSE',
'  ',
'    :p30_modificacion := NULL;',
'  ',
'  END IF;',
'  --END IF;',
'',
'  BEGIN',
'  ',
'    SELECT nvl(pa.pen_valor, 0)',
'      INTO ln_valor_par_anticipo',
'      FROM asdm_e.car_par_entidades pa',
'     WHERE pa.par_id = 83',
'       AND pa.emp_id = :f_emp_id;',
'  ',
'    SELECT nvl(SUM(to_number(c006)), 0)',
'      INTO ln_valor_anticipo',
'      FROM apex_collections co',
'     WHERE collection_name = ''CO_MOV_CAJA''',
'       AND to_number(c005) =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_tfp_id_anticipo_clientes'');',
'  ',
'    -- jandreso 22/7/2019',
'  ',
'    SELECT nvl(SUM(to_number(c006)), 0)',
'      INTO ln_valor_dev',
'      FROM apex_collections co',
'     WHERE collection_name = ''CO_MOV_CAJA''',
'       AND to_number(c005) =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_tfp_id_dev_promo'');',
'  ',
'    IF nvl(ln_valor_dev, 0) > 0 THEN',
'    ',
'      raise_application_error(-20000,',
'                              ''Por el momento NO se puede utilizar la Forma de Pago Promociones Devueltas '');',
'    ',
'    END IF;',
'  ',
'    -- jandreso                                              ',
'  ',
'    IF ln_valor_anticipo >= ln_valor_par_anticipo THEN',
'      :p30_valida_clave_anticipo := 1;',
'    ELSE',
'      :p30_valida_clave_anticipo := 0;',
'    END IF;',
'  ',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'    ',
'  END;',
'',
'EXCEPTION',
'  WHEN raise_pinpad THEN',
'  :p0_error:=''No tiene asignado un pinpad'';',
'  WHEN raise_puntaje THEN',
'    ROLLBACK;',
'    :p0_error := ''EL valor ingresado no puede ser mayor al total de puntajes'';',
'    --raise_application_error(-20000,);',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*:request = ''cargar_tfp'' and :P0_ERROR is null*/',
'(:request = ''cargar_tfp'' and :P0_ERROR is null AND :P30_TFP_ID <> pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'')) OR',
'(:request = ''cargar_tfp'' and :P0_ERROR is null AND :P30_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'') AND :P30_PUNTOS_CANCELAR > 0 AND :P30_MCD_VALOR_MOVIMIENTO > 0)'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>236232032245745957
,p_process_comment=>unistr('Se ejecuta con el submit creado en el screep del head de la p\00C3\00A1gina, este screep se ejecuta al dar click en el boton grabar, envia el url para imprimir la factura')
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(55050477416984206040)
,p_process_sequence=>15
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_bono_hogar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'     IF :p30_valor_bono IS NOT NULL AND :p30_valor_total_pagos >= :p30_valor_bono THEN',
'      :p30_mcd_valor_movimiento := :p30_valor_bono;',
'    ELSIF :p30_valor_total_pagos < :p30_valor_bono THEN',
'      :p30_mcd_valor_movimiento := :p30_valor_total_pagos;',
'    END IF;        ',
'    if :P30_MCD_VALOR_MOVIMIENTO > :P30_VALOR_BONO then',
'      :P0_ERROR:=''Seleccione el Bono a aplicar y verifique que el valor a cancelar no sea mayor que el valor de Bono Hogar.'';',
'    end if;',
' ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and :p30_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_bono_hogar'') and :P0_ERROR is null'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>55018224265714441114
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(46753872097572993)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_imp.id(46749483434572957)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_PROMOCIONES_ORDENES'
,p_attribute_03=>'POR_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
,p_internal_uid=>14500720827808067
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42321881329406435)
,p_process_sequence=>25
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_vueltos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_movimientos_caja.pr_vueltos(pn_total_pagar => to_number(:P30_VALOR_TOTAL_PAGOS),',
'                                     pn_vuelto => :p30_vuelto,',
'                                     pn_emp_id => :f_emp_id,',
'                                     pv_error => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''cargar_tfp'' and :P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>10068730059641509
,p_process_comment=>'Antes estaba :P0_ERROR is null'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42351471917800095)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_col_movimientos_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                                 ''cv_coleccion_mov_caja''));'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(42350273602772160)
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P30_FACTURAR <> ''R'' ',
'/*Cuando viene de retencion no debe borrar la coleccion de movimientos caja*/'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>10098320648035169
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(96387156129510744)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pantalla'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
':P30_EDE_ID := NULL;',
':P30_CRE_TITULAR_CUENTA := NULL;',
'--:P30_SUM_PAGOS',
'--P30_ES_PRECANCELACION',
'--P30_SALDO_PROMO',
'--P30_SALDO_MAX_APLICAR',
':P30_MODIFICACION := NULL;',
':P30_TJ_ENTIDAD := NULL;',
':P30_BANCOS := NULL;',
':P30_TARJETA := NULL;',
'if :P30_ES_POLITICA_GOBIERNO = 0 then',
'',
'',
':P30_TIPO := NULL;',
':P30_FORMA := NULL;',
':P30_PLAN := NULL;',
'',
'end if;',
':P30_TJ_NUMERO := NULL;',
':P30_TJ_NUMERO_VOUCHER := NULL;',
':P30_TJ_RECAP := NULL;',
':P30_LOTE := NULL;',
':P30_MCD_NRO_AUT_REF := NULL;',
'--:P30_CRE_ID',
'--:P30_CRE_FECHA_DEPOSITO := SYSDATE;',
':P30_CRE_NRO_CHEQUE := NULL;',
':P30_CRE_NRO_CUENTA := NULL;',
':P30_CRE_NRO_PIN := NULL;',
':P30_MCD_NRO_COMPROBANTE := NULL;',
':P30_RAP_NRO_ESTABL_RET := NULL;',
':P30_RAP_NRO_PEMISION_RET := NULL;',
':P30_RAP_NRO_RETENCION := NULL;',
':P30_RAP_NRO_AUTORIZACION := NULL;',
':P30_RAP_FECHA := NULL;',
':P30_RAP_FECHA_VALIDEZ := NULL;',
':P30_RAP_COM_ID := NULL;',
':P30_SALDO_RETENCION := NULL;',
':P30_PRE_ID  := NULL;',
':p30_entrada := null;',
':p30_titular_tc := null;',
'',
':P30_MCD_VALOR_MOVIMIENTO := 0;',
'IF NVL(:P30_ES_PRECANCELACION,''N'') <> ''S'' and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')THEN',
'-- Yguaman 2011/12/21. Valida si tiene anticipo de clientes, a obligarlo a aplicar el pago con anticipo',
'pq_ven_pagos_cuota.pr_advertencia_anticipos(pn_emp_id  => :F_EMP_ID,',
'                                     pn_cli_id    => :P30_CLI_ID,                                     ',
'                                     pn_antic_ing  => :P30_MCD_VALOR_MOVIMIENTO,',
'                                     pn_tfp_id   => :P30_TFP_ID,  ',
'                                     pn_seq_id => :P30_SEQ_ID,',
'                                      pn_facturar  => :P30_FACTURAR, -- M',
'                                     pn_uge_id  => :f_uge_id,',
'                                     pv_error =>:p0_error );',
'END IF;',
'',
'IF :p0_error is not null or :P30_SEQ_ID is not null then',
' :P30_TFP_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'');  ',
'end if;',
'',
':P30_SEQ_ID := null;',
'----------------------------------- ************************** ------------------------------------------------',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''limpia'' and :p0_error is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>unistr('Validaci\00C3\00B3n aprobada')
,p_internal_uid=>64134004859745818
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51035460089717175)
,p_process_sequence=>140
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_ven_comunes.pr_graba_identificadores_comp(pn_ode_id => 13683,',
'                                               pn_ebs_id => 16,',
'                                               pn_age_id =>:f_age_id_agencia,',
'                                               pn_emp_id =>1,',
'                                               pv_error  => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(51035678574731997)
,p_process_when_type=>'NEVER'
,p_internal_uid=>18782308819952249
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(6282234982451922543)
,p_process_sequence=>150
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_activa_boton_factura'
,p_process_sql_clob=>':P30_FACTURA_YN := ''S'';'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(46750954162572982)
,p_internal_uid=>6249981831182157617
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(11422363279977609387)
,p_process_sequence=>160
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_vaciar_promociones'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DELETE    ven_promociones_ordenes a',
'WHERE     a.ord_id = :P30_ORD_ID',
'AND       a.per_id IS NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(46750954162572982)
,p_internal_uid=>11390110128707844461
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1491297165733011369)
,p_process_sequence=>170
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_valida_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_Ret NUMBER;',
'',
'BEGIN',
'  IF :P30_BASE > 0 THEN',
'    pq_ven_pagos_cuota_retencion.pr_carga_val_retenciones(PN_EMP_ID               => :f_emp_id,',
'                                                          PN_UGE_ID               => :f_uge_id,',
'                                                          PN_TSE_ID               => :f_seg_id,',
'                                                          pn_com_id               => :P30_ORD_ID,',
'                                                          pd_rap_fecha            => :P30_RAP_FECHA,',
'                                                          pd_rap_fecha_validez    => :P30_RAP_FECHA_VALIDEZ,',
'                                                          pn_rap_nro_retencion    => :P30_RAP_NRO_RETENCION,',
'                                                          pn_rap_nro_autorizacion => :P30_RAP_NRO_AUTORIZACION,',
'                                                          pn_RAP_NRO_ESTABL_RET   => :P30_RAP_NRO_ESTABL_RET,',
'                                                          pn_RAP_NRO_PEMISION_RET => :P30_RAP_NRO_PEMISION_RET,',
'                                                          pn_porcentaje           => :P30_tRE_ID,',
'                                                          pn_base_ret             => :p30_base,',
'                                                          pv_ret_iva              => :p30_ret_iva,',
'                                                          pv_observacion          => :P30_OBSERVACIONES_RET,',
'                                                          pv_sesion               => :P30_DEA_SECCION,',
'                                                         pn_ord_id                => :P30_ORD_ID,',
'                                                          pn_rpo_id  => :P30_tRE_ID,',
'                                                          PV_ERROR                => :p0_error);',
'  END IF;',
'',
'  IF :P30_OBSERVACIONES_RET IS NULL THEN',
'  ',
'    SELECT sum(to_number(c010))',
'      INTO :P30_MCD_VALOR_MOVIMIENTO',
'      FROM apex_collections',
'     WHERE collection_name = ''COLL_VALOR_RETENCION'';',
'  ',
'  ELSE',
'    :P30_MCD_VALOR_MOVIMIENTO := NULL;',
'  ',
'  END IF;',
'',
'  :P30_TRE_ID := NULL;',
'  :P30_BASE   := 0;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CARGA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1459044014463246443
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(43175918480268256)
,p_process_sequence=>180
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_consulta_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         varchar2(250);',
'  ln_rpr_id           number;',
'BEGIN',
'',
'  -- Call the procedure',
'',
'                                                 ',
'  pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id => :p30_pinpad,',
'                                                 pn_cli_id => :p30_cli_id,',
'                                                 pn_uge_id => :f_uge_id,',
'                                                 pn_emp_id => :f_emp_id,',
'                                                 pn_pue_id => :P0_PUE_ID,',
'                                                 pv_rpr_tipo_requerimiento => ''CT'',',
'                                                 pn_rpc_id => :P30_RPC_ID,',
'                                                 pn_rpr_id_req => :p30_rpr_id_req,',
'                                                 pv_trama => lv_data_set,',
'                                                 pn_ede_id => null,',
'                                                 pv_error => :p0_error);',
'',
'  IF :p0_error IS NULL THEN',
'',
'  pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id => :p30_pinpad,',
'                                          pn_pue_id => :P0_PUE_ID,',
'                                          pn_cli_id => :p30_cli_id,',
'                                          pv_tipo_requerimiento => ''CT'',',
'                                          pv_trama_req => lv_data_set,',
'                                          pv_trama_res => :P30_TRAMA_RESP,',
'                                          pn_rpc_id => :P30_RPC_ID,',
'                                          pn_rpr_id_req => :p30_rpr_id_req,',
'                                          pn_rpr_id_res => ln_rpr_id,',
'                                          pc_error => :pc_error);',
'                                          ',
'',
'',
'  ',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(43175787965268255)
,p_internal_uid=>10922767210503330
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(43175960943268257)
,p_process_sequence=>190
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_lectura_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         varchar2(250);',
'  ln_rpr_id           number;',
'BEGIN',
'',
'  -- Call the procedure',
'',
'                                                 ',
'  pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id => :p30_pinpad,',
'                                                 pn_cli_id => :p30_cli_id,',
'                                                 pn_uge_id => :f_uge_id,',
'                                                 pn_emp_id => :f_emp_id,',
'                                                 pn_pue_id => :P0_PUE_ID,',
'                                                 pv_rpr_tipo_requerimiento => ''LT'',',
'                                                 pn_rpc_id => :P30_RPC_ID,',
'                                                 pn_rpr_id_req => :p30_rpr_id_req,',
'                                                 pv_trama => lv_data_set,',
'                                                 pn_ede_id => null,',
'                                                 pv_error => :p0_error);',
'',
'  IF :p0_error IS NULL THEN',
'',
'  pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id => :p30_pinpad,',
'                                          pn_pue_id => :P0_PUE_ID,',
'                                          pn_cli_id => :p30_cli_id,',
'                                          pv_tipo_requerimiento => ''LT'',',
'                                          pv_trama_req => lv_data_set,',
'                                          pv_trama_res => :P30_TRAMA_RESP,',
'                                          pn_rpc_id => :P30_RPC_ID,',
'                                          pn_rpr_id_req => :p30_rpr_id_req,',
'                                          pn_rpr_id_res => ln_rpr_id,',
'                                          pc_error => :pc_error);',
'                                          ',
'',
'',
'  ',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(43175688017268254)
,p_internal_uid=>10922809673503331
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15320759392914669036)
,p_process_sequence=>1
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_msg_graba_orden'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P0_ERROR := :P0_ERROR||''''|| :P0_MSG_GRABA_ORDEN;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_type=>'NEVER'
,p_internal_uid=>15288506241644904110
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(533828175797152378)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_induccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar      NUMBER;',
'  ln_com_id_retencion  ven_comprobantes.com_id%TYPE;',
'  ln_contador          NUMBER;',
'  ln_valor             VARCHAR2(100);',
'  ln_total_factura     NUMBER;',
'  ln_entrada           NUMBER;',
'  ln_validacion_precio NUMBER;',
'  cn_var_id_total      NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_var_id_total'');',
'  cn_var_id_entrada    NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_var_id_entrada'');',
'  cn_cat_id_induc_enc  NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_cat_id_induc_enc'');',
'  cn_cat_id_induc_coc  NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_cat_id_induc_coc'');',
'  ln_ite_sku_id        NUMBER;',
'BEGIN',
'',
'  ln_com_id_retencion := NULL;',
'',
'  IF :p30_modificacion IS NULL AND',
'     :p20_tve_id !=',
'     pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_contado'') THEN',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'      ---- cargo total de favtura',
'    ',
'      SELECT vor_valor_variable',
'        INTO ln_total_factura',
'        FROM ven_var_ordenes',
'       WHERE ord_id = :p30_ord_id',
'         AND var_id = cn_var_id_total;',
'    ',
'      ---- cargo valor entrada',
'    ',
'      SELECT vor_valor_variable',
'        INTO ln_entrada',
'        FROM ven_var_ordenes',
'       WHERE ord_id = :p30_ord_id',
'         AND var_id = cn_var_id_entrada;',
'    ',
'      SELECT COUNT(*)',
'        INTO ln_contador',
'        FROM apex_collections',
'       WHERE collection_name =',
'             pq_constantes.fn_retorna_constante(0, ''cv_coleccion_mov_caja'')',
'         AND c005 =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_tfp_id_plan_gobierno'');',
'    ',
'      IF ln_contador = 0 THEN',
'      ',
'        pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => :p30_plan,',
'                                                            pn_cre_id               => :p30_cre_id,',
'                                                            pn_tfp_id               => pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                                                          ''cn_tfp_id_plan_gobierno''),',
'                                                            pn_mcd_valor_movimiento => (nvl(to_number(round(ln_total_factura,',
'                                                                                                            2)),',
'                                                                                            0) -',
'                                                                                       nvl(to_number(ln_entrada),',
'                                                                                            0)), --:p30_mcd_valor_movimiento',
'                                                            pn_mcd_nro_aut_ref      => :p30_mcd_nro_aut_ref,',
'                                                            pn_mcd_nro_lote         => :p30_lote,',
'                                                            pn_mcd_nro_retencion    => :p30_rap_nro_retencion,',
'                                                            pn_mcd_nro_comprobante  => :p30_mcd_nro_comprobante, --NULL,',
'                                                            pv_titular_cuenta       => :p30_cre_titular_cuenta, --',
'                                                            pv_cre_nro_cheque       => :p30_cre_nro_cheque,',
'                                                            pv_cre_nro_cuenta       => :p30_cre_nro_cuenta,',
'                                                            pv_nro_pin              => :p30_cre_nro_pin,',
'                                                            pv_nro_tarjeta          => :p30_tj_numero,',
'                                                            pv_voucher              => :p30_tj_numero_voucher,',
'                                                            pn_saldo_retencion      => nvl(to_number(:p30_saldo_retencion),',
unistr('                                                                                           0), --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_com_id               => NULL, --:P30_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pd_rap_fecha            => :p30_rap_fecha, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pd_rap_fecha_validez    => :p30_rap_fecha_validez, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_autorizacion => :p30_rap_nro_autorizacion, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_establ_ret   => :p30_rap_nro_establ_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_pemision_ret => :p30_rap_nro_pemision_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_pre_id               => :p30_pre_id, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                            pd_fecha_deposito       => :p30_mcd_fecha_deposito,',
unistr('                                                            --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                            pv_titular_tarjeta_cre => :p30_titular_tc, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito                                                      ',
'                                                            pv_error               => :p0_error);',
'      ',
'        :p30_ede_id            := NULL;',
'        :p30_tfp_id            := NULL;',
'        :p30_tj_numero         := NULL;',
'        :p30_tj_numero_voucher := NULL;',
'        :p30_plan              := NULL;',
'        :p30_titular_tc        := NULL;',
'      ELSIF ln_contador > 1 THEN',
'        raise_application_error(-20000,',
'                                ''YA ESTA CARGADA LA FORMA DE PAGO GOBIERNO.  NO DEBE CARGARLA NUEVAMENTE'');',
'      END IF;',
'    END IF;',
'  ELSE',
'    :p30_modificacion := NULL;',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P20_ES_POLITICA_GOBIERNO = 1'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>501575024527387452
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(71910477189713604)
,p_process_sequence=>40
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_caduca_puntualito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_asdm_promociones.pr_cancela_promo(pn_emp_id       => :f_emp_id,',
'                                        pn_pro_id       => pq_constantes.fn_retorna_constante(pn_emp_id    => :f_emp_id,',
'                                                                                              pv_constante => ''cn_pro_id_promocion_pr''),',
'                                        pn_cli_id       => :p30_cli_id,',
'                                        pn_uge_id       => :f_uge_id,',
'                                        pn_usu_id       => :f_user_id,',
'                                        pn_uge_id_gasto => :f_uge_id_gasto,',
'                                        pv_error        => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>unistr('No tiene para forma de pago puntualito regal\00C3\00B3n')
,p_process_when_type=>'NEVER'
,p_internal_uid=>39657325919948678
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(98494657769887262)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_entrada_tc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar     NUMBER;',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
'',
'BEGIN',
'',
'  ln_com_id_retencion := NULL;',
'if :P30_entrada is not null then',
'  IF :P30_MODIFICACION IS NULL THEN',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'      pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                      pn_tfp_id => pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''),',
'                                                      pv_error  => :p0_error);',
'    ',
'',
'      pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => NULL,',
'                                                          pn_cre_id               => NULL,',
'                                                          pn_tfp_id               => pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''),',
'                                                          pn_mcd_valor_movimiento => nvl(to_number(:P30_ENTRADA),',
'                                                                                         0), --:p30_mcd_valor_movimiento',
'                                                          pn_mcd_nro_aut_ref      => NULL,',
'                                                          pn_mcd_nro_lote         => NULL,',
'                                                          pn_mcd_nro_retencion    => NULL,',
'                                                          pn_mcd_nro_comprobante  => NULL,',
'                                                          pv_titular_cuenta       => NULL, --:p30_tj_titular',
'                                                          pv_cre_nro_cheque       => NULL,',
'                                                          pv_cre_nro_cuenta       => NULL,',
'                                                          pv_nro_pin              => NULL,',
'                                                          pv_nro_tarjeta          => NULL,',
'                                                          pv_voucher              => NULL,',
unistr('                                                          pn_saldo_retencion      => 0, --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_com_id               => NULL, --:P30_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pd_rap_fecha            => NULL, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pd_rap_fecha_validez    => NULL, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_rap_nro_autorizacion => NULL, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_RAP_NRO_ESTABL_RET   => NULL, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_RAP_NRO_PEMISION_RET => NULL, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_PRE_ID               => NULL, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                          pd_fecha_deposito       => NULL,',
'                                                          pv_titular_tarjeta_cre  => NULL, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito',
unistr('                                                          --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                          pv_error => :p0_error);',
'    ',
'      --- Null los campos ---',
'        :P30_ENTRADA := NULL;',
'    END IF;',
'  ELSE',
'  ',
'    :P30_MODIFICACION := NULL;',
'  ',
'  END If;',
'',
'end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P30_ENTRADA and :P30_ES_POLITICA_GOBIERNO = 0'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>66241506500122336
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(118967465867095295)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_DATOS_FOLIO_CAJA_USU'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%type;',
'BEGIN',
'',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
'  ln_tgr_id  :=  pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito'');',
'  --:P0_TTR_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito'');',
'  :P0_TTR_ID := pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_nc_rebaja_puntualito'');',
'',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       LN_TGR_ID,',
'      :P0_TTR_DESCRIPCION, ',
'      :P0_PERIODO,',
'      :P0_DATFOLIO,',
'      :P30_SECUENCIA_ACTUAL,',
'      :P0_PUE_NUM_SRI  ,',
'      :P0_UGE_NUM_EST_SRI ,',
'      :P0_PUE_ID,',
'      :P0_TTR_ID,',
'      :P0_NRO_FOLIO,',
'      :P0_ERROR);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>86714314597330369
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(131078881032128250)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_col_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_int_mora''));',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_gto_cobr''));'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P30_FACTURAR = ''S'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>98825729762363324
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15332691286463409456)
,p_process_sequence=>130
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_ret'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number;',
'ln_com_id ven_comprobantes.com_id%TYPE;',
'begin',
'select count(*)',
'  into ln_cuenta',
'  from apex_collections a',
' where a.collection_name = ''COLL_VALOR_RETENCION''',
'   and a.seq_id = :P30_SEQ_ID_RET;',
'if ln_cuenta > 0 then',
'apex_collection.delete_member(p_collection_name =>   ''COLL_VALOR_RETENCION'',p_seq => :P30_SEQ_ID_RET );',
'end if;',
'',
'begin',
'select distinct(to_number(b.c001))',
'  into ln_Com_id',
'  from apex_collections b',
' where b.collection_name = ''COLL_VALOR_RETENCION'';',
'',
':p64_observacion := pq_ven_pagos_cuota_retencion.fn_valida_ret(:f_emp_id, ln_com_id);',
'',
'exception when no_data_found then',
'null;',
'when too_many_rows then',
'raise_application_error(-20000,''NO PUEDE CARGAR VARIAS FACTURAS CON UNA SOLA RETENCION'');',
'END;',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ELIMINA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>15300438135193644530
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(35329252936299998)
,p_process_sequence=>150
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_items_saldos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'  if :P30_TFP_ID =',
'     pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_dev_promo'') then',
'  ',
'    :P30_SALDO_PROMO_DEV := to_number(pq_asdm_promociones.fn_obtener_saldo_act_dev(:P30_CLI_ID,',
'                                                                                   :F_EMP_ID,',
'                                                                                   pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                                                      ''cn_pro_id_promocion_pr''),',
'                                                                                   NULL,',
'                                                                                   :P0_Error));',
'  ',
'',
'  elsif :P30_TFP_ID =',
'        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tfp_id_promocion_puntaje'') then',
'  ',
'    :P30_SALDO_PROMO_DEV := to_number(pq_ven_promociones_sql.fn_saldo_puntos(pq_ven_promociones_sql.fn_cliente_persona(:p30_cli_id, :F_EMP_ID,''P''),:P30_PRO_ID,:P30_PTA_ID));',
'  ',
'  end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>3076101666535072
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(2329020578885198168)
,p_process_sequence=>170
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_PAGINA_RETORNO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P30_PAGINA_REGRESO IS NULL THEN',
'   :P30_PAGINA_REGRESO := 6;',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>2296767427615433242
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(21229214761314822063)
,p_process_sequence=>170
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_punto_emision'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_pue_id             number;',
'  lv_tipo_punto_emision varchar2(1);',
'',
'begin',
'',
'  select ca.pue_id',
'    into ln_pue_id',
'    from ven_periodos_caja ca',
'   where ca.pca_id = :f_pca_id;',
'',
'  :P30_VALIDA_punto := pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => ln_pue_id);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>21196961610045057137
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(199707359121158957944)
,p_process_sequence=>180
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_load_text_forma_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'  Creacion:    13/07/2023',
'  Autor:       FPALOMEQUE',
'  Descripcion: Carga el valor de forma de credito en region a mostrar',
'  Historia:    CTD-106',
'*/',
'DECLARE',
'  lv_forma_credito VARCHAR2(100);',
'BEGIN',
'  SELECT o.forma_credito',
'    INTO lv_forma_credito',
'    FROM ven_ordenes o',
'   WHERE o.ord_id = :p30_ord_id;',
'',
'  -- Indica mensaje de forma de credito',
'  SELECT ''<div style="position:relative;float: left;top: 50%;left: 50%;transform: translate(-50%, -20%);"><p><span style="font-size:20;color:black;font-weight:bold">Forma de Credito: '' || CASE',
'           WHEN lv_forma_credito = ''P'' THEN',
'            ''<span style="font-size:20;color:#a99f04;font-weight:bold">"PENDIENTE"</span>''',
'           WHEN lv_forma_credito = ''I'' THEN',
'            ''<span style="font-size:20;color:#c26809;font-weight:bold">"INCONSISTENTE"</span>,''',
'           WHEN lv_forma_credito = ''M'' THEN',
'            ''<span style="font-size:20;color:#0b8e88;font-weight:bold">"MANUAL"</span>''',
'           WHEN lv_forma_credito = ''E'' THEN',
'            ''<span style="font-size:20;color:#0b8034;font-weight:bold">"ELECTRONICA"</span>''',
'           ELSE',
'            ''<span style="font-size:20;color:#9d1010;font-weight:bold">"SIN FORMA DE CREDITO"</span>''',
'         END || ''</span></p></div>''',
'    INTO :p30_show_fc',
'    FROM dual;',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    NULL;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>199675105969889193018
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51877566832499120)
,p_process_sequence=>160
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_puntos_usados'
,p_process_sql_clob=>':P30_PUNTOS_USADOS := :P30_MCD_VALOR_MOVIMIENTO;'
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P30_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_promocion_puntaje'')'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>19624415562734194
);
wwv_flow_imp.component_end;
end;
/
