prompt --application/pages/page_00139
begin
--   Manifest
--     PAGE: 00139
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>139
,p_name=>'Factuaraci&oacute;n Electr&oacute;nica'
,p_step_title=>'Factuaraci&oacute;n Electr&oacute;nica'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38312968425587295763)
,p_plug_name=>'Datos'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38312968721961295766)
,p_plug_name=>unistr('Guias de Remisi\00F3n')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sri.sco_id,',
'       sri.sco_nombre_reporte,',
'       sri.sco_notificado_error,',
'       sri.sco_fecha_emision    AS "FECHA DOCUMENTO",',
'       sri.sco_fecha            AS "FECHA COMPROBANTE",',
'       sri.sco_fecha_autorizado AS "FECHA AUTORIZACION",',
'       sri.tipo,',
'       cn.cno_destinatarios,',
'       cn.cno_fecha_registro    AS "FECHA MAIL",',
'       cn.cno_error',
'  FROM (SELECT a.sco_id,',
'               a.sco_comprobante,',
'               a.sco_nombre_reporte,',
'               a.sco_param_reporte,',
'               a.sco_notificado_error,',
'               a.tgr_id,',
'               ''REENVIO'' AS "REENVIO",',
'               a.sco_fecha_emision,',
'               a.sco_fecha,',
'               a.sco_fecha_autorizado,',
'               CASE',
'                 WHEN a.sco_factura IS NOT NULL THEN',
'                  ''F''',
'                 WHEN a.sco_nota_credito IS NOT NULL THEN',
'                  ''NC''',
'                 WHEN a.sco_guia_remision IS NOT NULL THEN',
'                  ''GR''',
'                 WHEN a.sco_nota_debito IS NOT NULL THEN',
'                  ''ND''',
'                 WHEN a.sco_retencion IS NOT NULL THEN',
'                  ''RET''',
'               END tipo',
'          FROM asdm_e.asdm_sri_comprobantes a',
'         WHERE a.sco_estado = ''AUTORIZADO''',
'           AND a.sco_estado_registro = 0',
'           AND (a.sco_notificado <> ''S'' OR a.sco_notificado IS NULL)',
'           AND a.sco_email IS NOT NULL',
'           AND nvl(a.sco_num_env_notifi, 0) >= 1',
'           AND a.sco_notificado_error IS NOT NULL) sri,',
'       inv_guias_remision gre,',
'       inv_bodegas_propias bpr,',
'       knot_e.knot_colas_notificacion cn',
' WHERE gre.gre_id = sri.sco_comprobante',
'   AND sri.tipo = ''GR''',
'   AND cn.sco_id = sri.sco_id',
'   AND cn.cno_estado_registro = 0',
'   AND cn.cno_estado_envio = ''P''',
'   AND gre.bpr_id = bpr.bpr_id',
'   AND bpr.uge_id = :f_uge_id',
'   AND gre.emp_id = :f_emp_id',
'   AND gre.gre_fecha BETWEEN :p139_desde AND :p139_hasta',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(38312968799289295767)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'ETENESACA'
,p_internal_uid=>38280715648019530841
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312968923453295768)
,p_db_column_name=>'SCO_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Sco id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969005852295769)
,p_db_column_name=>'SCO_NOMBRE_REPORTE'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Sco nombre reporte'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969060957295770)
,p_db_column_name=>'SCO_NOTIFICADO_ERROR'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Sco notificado error'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_rpt_show_filter_lov=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969202442295771)
,p_db_column_name=>'FECHA DOCUMENTO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Fecha documento'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969333805295772)
,p_db_column_name=>'FECHA COMPROBANTE'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Fecha comprobante'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969448912295773)
,p_db_column_name=>'FECHA AUTORIZACION'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Fecha autorizacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969490884295774)
,p_db_column_name=>'TIPO'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Tipo'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969609333295775)
,p_db_column_name=>'CNO_DESTINATARIOS'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Cno destinatarios'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38312969748518295776)
,p_db_column_name=>'FECHA MAIL'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Fecha mail'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315103586675526327)
,p_db_column_name=>'CNO_ERROR'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Cno error'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_rpt_show_filter_lov=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(38315117090430538018)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'382828640'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'SCO_ID:SCO_NOMBRE_REPORTE:SCO_NOTIFICADO_ERROR:FECHA DOCUMENTO:FECHA COMPROBANTE:FECHA AUTORIZACION:TIPO:CNO_DESTINATARIOS:FECHA MAIL:CNO_ERROR'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38315096387334496497)
,p_plug_name=>'Factuaraci&oacute;n Electr&oacute;nica'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>20
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sri.sco_id,',
'       sri.sco_nombre_reporte,',
'       sri.sco_notificado_error,',
'       sri.sco_fecha_emision    AS "FECHA DOCUMENTO",',
'       sri.sco_fecha            AS "FECHA COMPROBANTE",',
'       sri.sco_fecha_autorizado AS "FECHA AUTORIZACION",',
'       sri.tipo,',
'       cn.cno_destinatarios,',
'       cn.cno_fecha_registro    AS "FECHA MAIL",',
'       cn.cno_error',
'  FROM (SELECT a.sco_id,',
'               a.sco_comprobante,',
'               a.sco_nombre_reporte,',
'               a.sco_param_reporte,',
'               a.sco_notificado_error,',
'               a.tgr_id,',
'               ''REENVIO'' AS "REENVIO",',
'               a.sco_fecha_emision,',
'               a.sco_fecha,',
'               a.sco_fecha_autorizado,',
'               CASE',
'                 WHEN a.sco_factura IS NOT NULL THEN',
'                  ''F''',
'                 WHEN a.sco_nota_credito IS NOT NULL THEN',
'                  ''NC''',
'                 WHEN a.sco_guia_remision IS NOT NULL THEN',
'                  ''GR''',
'                 WHEN a.sco_nota_debito IS NOT NULL THEN',
'                  ''ND''',
'                 WHEN a.sco_retencion IS NOT NULL THEN',
'                  ''RET''',
'               END tipo',
'          FROM asdm_e.asdm_sri_comprobantes a',
'         WHERE a.sco_estado = ''AUTORIZADO''',
'           AND a.sco_estado_registro = 0',
'           AND (a.sco_notificado <> ''S'' OR a.sco_notificado IS NULL)',
'           AND a.sco_email IS NOT NULL',
'           AND nvl(a.sco_num_env_notifi, 0) >= 1',
'           AND a.sco_notificado_error IS NOT NULL) sri,',
'       ven_comprobantes co,',
'       knot_e.knot_colas_notificacion cn',
' WHERE co.com_id = sri.sco_comprobante',
'   AND sri.tipo IN (''NC'', ''ND'', ''F'')',
'   AND cn.sco_id = sri.sco_id',
'   AND cn.cno_estado_registro = 0',
'   AND cn.cno_estado_envio = ''P''',
'   AND co.uge_id = :F_UGE_ID',
'   AND co.Emp_Id = :F_EMP_ID',
'   AND co.com_fecha BETWEEN :P139_desde AND :p139_hasta'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(38315096497720496497)
,p_name=>'Factuaraci&oacute;n Electr&oacute;nica'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'ETENESACA'
,p_internal_uid=>38282843346450731571
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315096924381496616)
,p_db_column_name=>'SCO_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Sco Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315097190058496637)
,p_db_column_name=>'SCO_NOMBRE_REPORTE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Sco Nombre Reporte'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315097618869496637)
,p_db_column_name=>'SCO_NOTIFICADO_ERROR'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Sco Notificado Error'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_rpt_show_filter_lov=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315097960831496637)
,p_db_column_name=>'FECHA DOCUMENTO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Fecha Documento'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315098381303496638)
,p_db_column_name=>'FECHA COMPROBANTE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Fecha Comprobante'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315098780983496638)
,p_db_column_name=>'FECHA AUTORIZACION'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Fecha Autorizacion'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315099231642496638)
,p_db_column_name=>'TIPO'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Tipo'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315099630392496639)
,p_db_column_name=>'CNO_DESTINATARIOS'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Cno Destinatarios'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315099977574496639)
,p_db_column_name=>'FECHA MAIL'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Fecha Mail'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315100377553496640)
,p_db_column_name=>'CNO_ERROR'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Cno Error'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_rpt_show_filter_lov=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(38315116544612537997)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'382828634'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'SCO_ID:SCO_NOMBRE_REPORTE:SCO_NOTIFICADO_ERROR:FECHA DOCUMENTO:FECHA COMPROBANTE:FECHA AUTORIZACION:TIPO:CNO_DESTINATARIOS:FECHA MAIL:CNO_ERROR'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38315103662971526328)
,p_plug_name=>'Retenciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sri.sco_id,',
'       sri.sco_nombre_reporte,',
'       sri.sco_notificado_error,',
'       sri.sco_fecha_emision    AS "FECHA DOCUMENTO",',
'       sri.sco_fecha            AS "FECHA COMPROBANTE",',
'       sri.sco_fecha_autorizado AS "FECHA AUTORIZACION",',
'       sri.tipo,',
'       cn.cno_destinatarios,',
'       cn.cno_fecha_registro    AS "FECHA MAIL",',
'       cn.cno_error',
'  FROM (SELECT a.sco_id,',
'               a.sco_comprobante,',
'               a.sco_nombre_reporte,',
'               a.sco_param_reporte,',
'               a.sco_notificado_error,',
'               a.tgr_id,',
'               ''REENVIO'' AS "REENVIO",',
'               a.sco_fecha_emision,',
'               a.sco_fecha,',
'               a.sco_fecha_autorizado,',
'               CASE',
'                 WHEN a.sco_factura IS NOT NULL THEN',
'                  ''F''',
'                 WHEN a.sco_nota_credito IS NOT NULL THEN',
'                  ''NC''',
'                 WHEN a.sco_guia_remision IS NOT NULL THEN',
'                  ''GR''',
'                 WHEN a.sco_nota_debito IS NOT NULL THEN',
'                  ''ND''',
'                 WHEN a.sco_retencion IS NOT NULL THEN',
'                  ''RET''',
'               END tipo',
'          FROM asdm_e.asdm_sri_comprobantes a',
'         WHERE a.sco_estado = ''AUTORIZADO''',
'           AND a.sco_estado_registro = 0',
'           AND (a.sco_notificado <> ''S'' OR a.sco_notificado IS NULL)',
'           AND a.sco_email IS NOT NULL',
'           AND nvl(a.sco_num_env_notifi, 0) >= 1',
'           AND a.sco_notificado_error IS NOT NULL) sri,',
'       tes_retenciones ret, asdm_transacciones tra,',
'       knot_e.knot_colas_notificacion cn',
' WHERE ret.ret_id = sri.sco_comprobante',
'   AND sri.tipo = ''RET''',
'   AND cn.sco_id = sri.sco_id',
'   AND cn.cno_estado_registro = 0',
'   AND cn.cno_estado_envio = ''P''',
'   AND ret.emp_id = :f_emp_id',
'   and tra.trx_id = ret.trx_id',
'   and tra.uge_id = :F_UGE_ID',
'   AND ret.ret_fecha BETWEEN :P139_DESDE AND :P139_HASTA'))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(38315103763566526329)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'ETENESACA'
,p_internal_uid=>38282850612296761403
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315103943526526330)
,p_db_column_name=>'SCO_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Sco id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104030312526331)
,p_db_column_name=>'SCO_NOMBRE_REPORTE'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Sco nombre reporte'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104106074526332)
,p_db_column_name=>'SCO_NOTIFICADO_ERROR'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Sco notificado error'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_rpt_show_filter_lov=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104198457526333)
,p_db_column_name=>'FECHA DOCUMENTO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Fecha documento'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104288640526334)
,p_db_column_name=>'FECHA COMPROBANTE'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Fecha comprobante'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104406646526335)
,p_db_column_name=>'FECHA AUTORIZACION'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Fecha autorizacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104516241526336)
,p_db_column_name=>'TIPO'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Tipo'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104637794526337)
,p_db_column_name=>'CNO_DESTINATARIOS'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Cno destinatarios'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104667582526338)
,p_db_column_name=>'FECHA MAIL'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Fecha mail'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38315104805764526339)
,p_db_column_name=>'CNO_ERROR'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Cno error'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_rpt_show_filter_lov=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(38315117824606538021)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'382828647'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'SCO_ID:SCO_NOMBRE_REPORTE:SCO_NOTIFICADO_ERROR:FECHA DOCUMENTO:FECHA COMPROBANTE:FECHA AUTORIZACION:TIPO:CNO_DESTINATARIOS:FECHA MAIL:CNO_ERROR'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38315104930838526340)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(38312968425587295763)
,p_button_name=>'Consultar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Consultar'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38315105069326526342)
,p_branch_name=>'Go To Page 139'
,p_branch_action=>'f?p=&APP_ID.:139:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38312968471163295764)
,p_name=>'P139_DESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38312968425587295763)
,p_item_default=>'SYSDATE-5'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Desde'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38312968565511295765)
,p_name=>'P139_HASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(38312968425587295763)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Hasta'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38315105004304526341)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_fechas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_dif number;',
'',
'begin',
'SELECT TRUNC(to_date(:P139_HASTA,''dd/mm/yyyy'')-to_date(:P139_DESDE,''dd/mm/yyyy'')) into ln_dif FROM DUAL;',
' if ln_dif <= 10  and ln_dif > 0 THEN',
'  --Continue',
'  null;',
' ELSE',
' raise_application_error(-20000,''Solo Puede consultar en itervalos de 10 dias DiffActual:''||ln_dif);',
' END IF;',
' END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>38282851853034761415
);
wwv_flow_imp.component_end;
end;
/
