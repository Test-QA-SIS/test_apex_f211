prompt --application/pages/page_00666
begin
--   Manifest
--     PAGE: 00666
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>666
,p_name=>'Pagos Cuota'
,p_step_title=>'Pagos Cuota'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'',
'var numero_filas=500;',
'',
'//FUNCIONES PARA ACTUALIZAR REGIONES',
'',
'',
'',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(58959565642931327)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_pagos_cuota_retencion.fn_mostrar_div_pendientes(:P0_error);',
'',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(58959565642931327)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58959752319931336)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58959879976931337)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58959964879931337)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960075984931337)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960154753931337)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960262506931338)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960360586931338)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960452991931338)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960566102931338)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960681464931338)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960753609931338)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960878437931338)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58960983547931338)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961080594931338)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961157273931338)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961257632931338)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961382133931338)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961481996931338)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961561892931338)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961672308931338)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961784008931338)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961873405931338)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58961983562931338)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962052504931338)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962180023931338)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962265513931339)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962383231931339)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962476730931339)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962556607931339)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962660273931339)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962782693931339)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962866516931339)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58962969550931339)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963080697931339)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963166890931339)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963282611931339)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963360584931339)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963468634931339)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963564707931339)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963654468931340)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963779261931340)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963863177931340)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58963963178931340)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964052002931340)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964181731931340)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964272968931340)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964381748931340)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964462301931340)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964567259931340)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964655051931340)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964761527931340)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964852621931340)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58964951713931340)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965068909931340)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965159995931340)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965268760931340)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965381844931340)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965468052931340)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965578431931340)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965668800931340)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(58965751720931346)
,p_name=>'borrar despues de probar'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>18
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_pagos_cuota.fn_select_pagos_div_cuota(:P666_CXC_ID,:p0_error);'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT count(c.cxc_id)',
'  FROM v_car_cartera_clientes c',
' WHERE c.emp_id = :f_emp_id',
'   AND c.cli_id = :p666_cli_id )> 0'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>6
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(58965751720931346)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58965978062931347)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966060528931347)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966182185931347)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966281884931347)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966369250931347)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966478663931347)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966577842931347)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966672992931347)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966776057931347)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966878694931347)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58966974381931347)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967079427931347)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967157110931348)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967269878931348)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967356301931348)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967483336931349)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967576634931349)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967672702931349)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967771400931349)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967853256931349)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58967952899931349)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968059492931349)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968175770931349)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968269982931349)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968366084931349)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968453313931349)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968573495931349)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968681810931349)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968761286931349)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968852329931349)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58968983119931349)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969078664931349)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969159742931349)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969253050931349)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969369096931349)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969481962931350)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969574448931350)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969669029931350)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969758391931351)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969851602931352)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58969956803931352)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970057980931352)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970156191931352)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970257212931352)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970353509931352)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970479867931352)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970579183931352)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970661356931352)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970781154931352)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970874649931352)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58970951414931352)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971055068931352)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971161536931352)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971273744931352)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971377833931352)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971457256931352)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971578845931352)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971670058931352)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971754071931353)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58971862973931353)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(58971956556931353)
,p_plug_name=>'parametros'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_2'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':f_emp_id is null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(58974582982931364)
,p_plug_name=>'_'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>65
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P666_CLI_ID is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(58976761614931371)
,p_plug_name=>'CARTERA DE CLIENTE - &P666_NOMBRE.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Select',
'      seq_id,',
'      to_number(c001) cli_id,',
'      c002 per_id,',
'      to_number(c003) cxc_id,',
'      c004 div_id,',
'      to_number(c005) nro_vencimiento,',
'      to_date(c006) fecha_vencimiento,',
'(case when c007 < ''0'' then',
'             ''0'' else c007 end ) as dias_mora,      ',
'      c008 valor_cuota,                                     ',
'      c009 respaldo_cheques,',
'      to_number(c010) saldo, ',
'      c011 estado,',
'apex_item.checkbox(10,seq_id,p_attributes => ',
'',
'''onclick="pr_carga_cuotas_con_valor(''''PR_CUOTAS_VALOR'''',''''P666_F_EMP_ID'''',''''P666_TFP_ID'''',this.value,''''P666_F_SEG_ID'''',''''P666_VALOR_PAGO'''',''''R26723610345166445'''',''''R26727102869166483'''');',
'pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P666_F_EMP_ID'''',''''TP'''',''''P666_VALOR_PAGO_COLECCION'''');pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P666_F_EMP_ID'''',''''IC'''',''''P666_TOTAL_INT_COND'''');',
'pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P666_F_EMP_ID'''',''''GC'''',''''P666_TOTAL_GST_COND'''');pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P666_F_EMP_ID'''',''''TC'''',''''P666_TOTAL_CONDONAR'''')"'') pagar,',
'      --c012 pagar,',
'      c013 fac_mxpro,',
'      c014 com_id_factura,',
'      c015 comp,',
'      c016 factura_migrada,',
'      c017 ttr_id,',
'      c018 transaccion,',
'      c019 comprobante,',
'      c020 ede_dividendo,                                     ',
'      c021 vendedor,',
'      c022 segmento,',
'      c023 selecc,',
'      c024 Rol, -- yguaman 2011/12/15',
'      c025 Agencia_Creacion,',
'       trunc(SYSDATE) - to_date(c006) Dias_Vencidos -- yguaman 2012/02/28 ',
'      from apex_collections       ',
'      WHERE collection_name = ''CO_DIV_PENDIENTES''',
'',
'       --- Rango de Fechas a consultar',
'        AND (trunc(c006) >= trunc(:P666_FECHA_DESDE) OR',
'             :P666_FECHA_DESDE IS NULL)',
'         AND (to_date(c006,''dd-mm-yyyy'') <= to_date(:P666_FECHA_HASTA_CORTE,''dd-mm-yyyy'') OR',
'             :P666_FECHA_HASTA_CORTE IS NULL)',
'',
'      --AND c023 = ''N''',
'      AND c004 not in',
'       (SELECT col.c005 div_id',
'          FROM apex_collections col',
'         WHERE col.collection_name =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_coleccion_pago_cuota''))',
'      ORDER BY to_number(c001), to_number(c003), to_number(c005);',
'',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P666_CLI_ID is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_prn_output_show_link=>'Y'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(58976958909931374)
,p_name=>'Listado Facturas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y_OF_Z'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>26723807640166448
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979363349931403)
,p_db_column_name=>'ESTADO'
,p_display_order=>7
,p_column_identifier=>'L'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ESTADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979459113931403)
,p_db_column_name=>'PAGAR'
,p_display_order=>8
,p_column_identifier=>'M'
,p_column_label=>'Pagar'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_tz_dependent=>'N'
,p_static_id=>'PAGAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979579147931403)
,p_db_column_name=>'COMPROBANTE'
,p_display_order=>17
,p_column_identifier=>'Y'
,p_column_label=>'Comp.'
,p_column_link=>'f?p=211:52:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_POPUP,P52_COM_ID,P52_VALIDA_CONS_CARTERA:f?p=201,&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOG'
||'O.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,N,#COM_ID_FACTURA#,2'
,p_column_linktext=>'#COMPROBANTE#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COMPROBANTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979680489931404)
,p_db_column_name=>'TRANSACCION'
,p_display_order=>18
,p_column_identifier=>'Z'
,p_column_label=>'Trans.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TRANSACCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977059964931392)
,p_db_column_name=>'SEGMENTO'
,p_display_order=>19
,p_column_identifier=>'AA'
,p_column_label=>'Segmento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SEGMENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977164025931399)
,p_db_column_name=>'EDE_DIVIDENDO'
,p_display_order=>21
,p_column_identifier=>'AC'
,p_column_label=>'Tipo Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_DIVIDENDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977267102931399)
,p_db_column_name=>'VENDEDOR'
,p_display_order=>22
,p_column_identifier=>'AD'
,p_column_label=>'Vendedor'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VENDEDOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977370608931399)
,p_db_column_name=>'PER_ID'
,p_display_order=>24
,p_column_identifier=>'AF'
,p_column_label=>'Per Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977478564931399)
,p_db_column_name=>'DIV_ID'
,p_display_order=>26
,p_column_identifier=>'AH'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977569902931399)
,p_db_column_name=>'DIAS_MORA'
,p_display_order=>29
,p_column_identifier=>'AK'
,p_column_label=>'Dias Mora'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DIAS_MORA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977669192931399)
,p_db_column_name=>'VALOR_CUOTA'
,p_display_order=>30
,p_column_identifier=>'AL'
,p_column_label=>'Valor Cuota'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977755883931400)
,p_db_column_name=>'RESPALDO_CHEQUES'
,p_display_order=>31
,p_column_identifier=>'AM'
,p_column_label=>'Respaldo Cheq.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RESPALDO_CHEQUES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977857320931400)
,p_db_column_name=>'FAC_MXPRO'
,p_display_order=>33
,p_column_identifier=>'AO'
,p_column_label=>'Fac Mxpro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FAC_MXPRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58977982625931400)
,p_db_column_name=>'COM_ID_FACTURA'
,p_display_order=>34
,p_column_identifier=>'AP'
,p_column_label=>'Com Id Factura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978067614931400)
,p_db_column_name=>'COMP'
,p_display_order=>35
,p_column_identifier=>'AQ'
,p_column_label=>'Comp'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COMP'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978153137931400)
,p_db_column_name=>'TTR_ID'
,p_display_order=>36
,p_column_identifier=>'AR'
,p_column_label=>'Ttr Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978276854931400)
,p_db_column_name=>'SELECC'
,p_display_order=>37
,p_column_identifier=>'AS'
,p_column_label=>'Selecc'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
,p_static_id=>'SELECC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978353334931400)
,p_db_column_name=>'SEQ_ID'
,p_display_order=>38
,p_column_identifier=>'AT'
,p_column_label=>'Seq Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978462507931401)
,p_db_column_name=>'ROL'
,p_display_order=>39
,p_column_identifier=>'AU'
,p_column_label=>'Rol'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ROL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978551392931401)
,p_db_column_name=>'SALDO'
,p_display_order=>40
,p_column_identifier=>'AV'
,p_column_label=>'Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978670776931401)
,p_db_column_name=>'AGENCIA_CREACION'
,p_display_order=>41
,p_column_identifier=>'AW'
,p_column_label=>'Agencia Creacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA_CREACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978754787931401)
,p_db_column_name=>'FACTURA_MIGRADA'
,p_display_order=>42
,p_column_identifier=>'AX'
,p_column_label=>'Factura Migrada'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA_MIGRADA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978853922931402)
,p_db_column_name=>'FECHA_VENCIMIENTO'
,p_display_order=>43
,p_column_identifier=>'AY'
,p_column_label=>'Fecha Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58978952298931402)
,p_db_column_name=>'CLI_ID'
,p_display_order=>44
,p_column_identifier=>'AZ'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979083326931402)
,p_db_column_name=>'CXC_ID'
,p_display_order=>45
,p_column_identifier=>'BA'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979169480931402)
,p_db_column_name=>'NRO_VENCIMIENTO'
,p_display_order=>46
,p_column_identifier=>'BB'
,p_column_label=>'Nro Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58979253865931403)
,p_db_column_name=>'DIAS_VENCIDOS'
,p_display_order=>47
,p_column_identifier=>'BC'
,p_column_label=>'Dias Vencidos'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIAS_VENCIDOS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(58979762225931404)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'267267'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>5
,p_report_columns=>'AGENCIA_CREACION:COMPROBANTE:FACTURA_MIGRADA:CXC_ID:DIV_ID:NRO_VENCIMIENTO:FECHA_VENCIMIENTO:VALOR_CUOTA:RESPALDO_CHEQUES:SALDO:PAGAR:TRANSACCION:EDE_DIVIDENDO:VENDEDOR:SEGMENTO:ROL'
,p_sort_column_1=>'NRO_VENCIMIENTO'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'DIV_ID'
,p_sort_direction_2=>'DESC'
,p_sort_column_3=>'CLI_ID'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'CXC_ID'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
,p_break_on=>'CXC_ID:COMPROBANTE:TRANSACCION:AGENCIA_CREACION:VENDEDOR:0'
,p_break_enabled_on=>'CXC_ID:COMPROBANTE:TRANSACCION:VENDEDOR:0:AGENCIA_CREACION'
,p_sum_columns_on_break=>'SALDO'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(58979969145931407)
,p_report_id=>wwv_flow_imp.id(58979762225931404)
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'DIAS_VENCIDOS'
,p_operator=>'>'
,p_expr=>'0'
,p_condition_sql=>' (case when ("DIAS_VENCIDOS" > to_number(#APXWS_EXPR#)) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# > #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_font_color=>'#FF7755'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(58980254138931409)
,p_name=>'CUOTAS SELECCIONADAS PARA PAGAR'
,p_parent_plug_id=>wwv_flow_imp.id(58976761614931371)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_car_pruebas.fn_apex_item_col_pago_cuota(pv_error => :p0_error,',
'                                                      pn_emp_id => :f_emp_id,',
'                                                      pn_ttr_id_origen => pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota''));',
'',
'',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(58980254138931409)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58980472696931410)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>11
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58980567129931410)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>20
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58980665485931410)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>1
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58980754090931410)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>2
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58980856175931410)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>3
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58980966309931411)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>4
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981064622931411)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981177453931411)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>6
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981277851931411)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981383033931411)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>5
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981479034931411)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>9
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981551637931411)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>10
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981672619931411)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981778675931411)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>15
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981873738931411)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>16
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58981958219931411)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>17
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982065724931411)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>18
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982177847931411)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>14
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982256808931411)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982383683931411)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>21
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982482235931412)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>12
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'5'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982558078931412)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982675157931412)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982775904931412)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982854775931412)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58982975585931412)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983074939931412)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983168291931413)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983272581931413)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983364967931413)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983476143931413)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983579737931413)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983653337931413)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983777742931413)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983878698931413)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58983971973931413)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984065570931413)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984169879931413)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984251337931416)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984365707931416)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984455398931416)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984568789931416)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984675906931416)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984774559931416)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984874334931416)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58984959286931416)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985080872931416)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985156462931416)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985256852931416)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985358546931416)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985455906931416)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985565835931416)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985680571931416)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985780020931416)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985882984931417)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58985952540931417)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58986059470931417)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58986163110931417)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58986260094931417)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58986380741931417)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(58987461586931421)
,p_plug_name=>'Condonar'
,p_parent_plug_id=>wwv_flow_imp.id(58980254138931409)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(58987675224931422)
,p_plug_name=>'<SPAN STYLE="font-size: 12pt">PAGO DE CUOTA GENERAL</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(58994151835931443)
,p_name=>'col'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT c001,',
'       c002,',
'       c003,',
'       c004,',
'       c005,',
'       c006,',
'       c007,',
'       c008,',
'       c009,',
'       c010,',
'       c011,',
'       c012,',
'       c013,',
'       c014,',
'       c015,',
'       c016,',
'       c017,',
'       c018,',
'       c019,',
'       c020,',
'       c021,',
'       c022,',
'       c023,',
'       pq_car_manejo_mora.fn_calcula_int_mora(pn_ede_id          => NULL,',
'                                              pn_emp_id          => c006,',
'                                              pn_div_id          => c005,',
'                                              pn_valor_condonado => nvl(c022,',
'                                                                        0)) intmora,',
'       pq_car_manejo_mora.fn_calcula_gasto_cobranza(NULL,',
'                                                    c006,',
'                                                    c005,',
'                                                    nvl(c023,0),',
'                                                    :p666_f_seg_id) gtocob',
'',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'      --''CO_DIV_PENDIENTES''',
'   AND c001 = :p666_cli_id}'';'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>25
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(58994151835931443)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994358520931443)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>2
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994469092931443)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>1
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994580885931443)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994654449931443)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994767200931445)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994853760931445)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58994954108931445)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995060326931445)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995176734931445)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995260027931446)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995356367931446)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995465089931446)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995568030931446)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995663183931446)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995758064931446)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995882836931446)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58995983338931446)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996059833931446)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996177325931446)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996267228931446)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996379096931446)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996461269931446)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996560140931446)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996657212931446)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58996765552931446)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(58996882040931446)
,p_name=>'cant'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT count(c004)',
'         ',
'          FROM apex_collections',
'         WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_pago_cuota'')',
'           AND c002 = :p666_com_id'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(58996882040931446)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(58997077814931447)
,p_query_column_id=>1
,p_column_alias=>'COUNT(C004)'
,p_column_display_sequence=>1
,p_column_heading=>'Count(C004)'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(58974772223931364)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_button_name=>'Formas_Pago'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Seguir >>'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:30:&SESSION.:formas_pago:&DEBUG.::P30_CLI_ID,P30_CLI_IDENTIFICACION,P30_MENSAJE,P30_FACTURAR,P30_CLI_NOMBRE,P30_PAGO_REFINANCIAMIENTO,P30_VALOR_REFINANCIAMIENTO,P30_CXC_ID_REF,P30_PAGINA_REGRESO:&P666_CLI_ID.,&P666_IDENTIFICACION.,,N,&P666_NOMBRE.,&P666_PAGO_REFINANCIAMIENTO.,&P666_VALOR_REFINANCIAMIENTO.,&P666_CXC_ID_REF.,666'
,p_button_condition=>':P666_CLI_ID is not null'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(58974976941931368)
,p_button_sequence=>800
,p_button_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_button_name=>'CONDONAR_NO_VALE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CONDONAR_NO_VALE'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(58980077784931408)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(58976761614931371)
,p_button_name=>'PAGAR_SELECCIONADOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Pagar Seleccionados'
,p_button_position=>'TOP_AND_BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(59002258491931478)
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.:formas_pago:&DEBUG.::P30_CLI_ID,P30_CLI_IDENTIFICACION,P30_CLI_NOMBRE,P30_MENSAJE,P30_VALOR_TOTAL_PAGOS,P30_FACTURAR,P30_ES_PRECANCELACION:&P666_CLI_ID.,&P666_IDENTIFICACION.,&P666_NOMBRE.,,6,N,N&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(102524481216876145)
,p_branch_sequence=>1
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 07-NOV-2011 10:30 by YGUAMAN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(59001658763931474)
,p_branch_action=>'f?p=&FLOW_ID.:666:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(58980077784931408)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(59001858023931477)
,p_branch_action=>'f?p=&APP_ID.:44:&SESSION.::&DEBUG.::P44_DESDE_PAGO_CAJA:S'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(58974976941931368)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(59002080613931478)
,p_branch_action=>'f?p=&APP_ID.:666:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>30
,p_branch_comment=>'Created 22-ENE-2010 17:22 by ONARANJO'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58972177834931354)
,p_name=>'P666_DIV_SALDO_CUOTA'
,p_item_sequence=>167
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Div Saldo Cuota'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58972365420931357)
,p_name=>'P666_REQUEST'
,p_item_sequence=>171
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Request'
,p_source=>':REQUEST'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58972563426931357)
,p_name=>'P666_COM_NUMERO_COMPLETO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Com Numero Completo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58972765514931358)
,p_name=>'P666_DIV_SALDO_GASTO_COBRANZA'
,p_item_sequence=>166
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Div Saldo Gasto Cobranza'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58972957518931358)
,p_name=>'P666_EMP_ID'
,p_item_sequence=>161
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Emp Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58973164346931359)
,p_name=>'P666_DIV_FECHA_VENCIMIENTO'
,p_item_sequence=>163
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Div Fecha Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58973383692931360)
,p_name=>'P666_CXC_SALDO'
,p_item_sequence=>164
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Cxc Saldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58973583792931361)
,p_name=>'P666_DIV_SALDO_INTERES_MORA'
,p_item_sequence=>165
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Div Saldo Interes Mora'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58973763585931361)
,p_name=>'P666_COM_ID'
,p_item_sequence=>157
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58973965924931361)
,p_name=>'P666_COM_NUMERO'
,p_item_sequence=>158
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Com Numero'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58974183963931362)
,p_name=>'P666_DIV_NRO_VENCIMIENTO'
,p_item_sequence=>159
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Div Nro Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58974360742931363)
,p_name=>'P666_DIV_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(58971956556931353)
,p_prompt=>'Div Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58975169545931368)
,p_name=>'P666_VALOR_PAGO_COLECCION'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Pago'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c016),0)',
' FROM apex_collections',
'       WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24" disabled'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58975366323931369)
,p_name=>'P666_LLAVE_AUTORIZACION'
,p_item_sequence=>315
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_use_cache_before_default=>'NO'
,p_prompt=>'LLave autorizacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(c011)',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58975564177931369)
,p_name=>'P666_TOTAL_INT_COND'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Int Condonar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c022),0)',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')',
'and c006 = :F_EMP_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58975754288931369)
,p_name=>'P666_TOTAL_GST_COND'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Gst Condonar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c023),0)',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')',
'and c006 = :F_EMP_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58975971563931370)
,p_name=>'P666_TOTAL_CONDONAR'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Condonar'
,p_source=>':P666_TOTAL_INT_COND + :P666_TOTAL_GST_COND;'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58976170096931370)
,p_name=>'P666_VALOR_REFINANCIAMIENTO'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_prompt=>'Valor Refinanciamiento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:20;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P666_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58976354708931371)
,p_name=>'P666_VALOR_CAPITAL_REFIN'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_prompt=>'Valor Capital Refin'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P666_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58976573546931371)
,p_name=>'P666_CXC_ID_REF'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_imp.id(58974582982931364)
,p_prompt=>'Cxc Id Ref'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P666_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58986465133931417)
,p_name=>'P666_CONDONAR_TODO'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(58980254138931409)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'upper(''n'');',
''))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'<SPAN STYLE="font-size: 10pt">Condonar Todo el Interes de Mora: </span>'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_named_lov=>'LOV_SI_NO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(300);',
'BEGIN',
'lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_SI_NO'');',
'return (lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''act_int_condonado'')"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'RIGHT-CENTER'
,p_display_when=>':F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*:F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'') and */',
'',
'Esta en HTML Form Element Attributes para que ejecute proceso sin submit:',
'',
'onChange="pr_carga_cuotas_con_valor(''PR_ACTUALIZA_INT_CONDONADO'',''P666_F_EMP_ID'',''P666_F_SEG_ID'',this.value,''P666_F_EMP_ID'',''R70263701157111210'',''R70272713295111230'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P666_F_EMP_ID'',''TP'',''P666_VALOR_PAGO_C'
||'OLECCION'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P666_F_EMP_ID'',''IC'',''P666_TOTAL_INT_COND'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P666_F_EMP_ID'',''GC'',''P666_TOTAL_GST_COND'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P666_F_EMP_ID'',''TC'
||''',''P666_TOTAL_CONDONAR'')"'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58986658613931419)
,p_name=>'P666_SEQ_ID'
,p_item_sequence=>169
,p_item_plug_id=>wwv_flow_imp.id(58980254138931409)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58986862580931419)
,p_name=>'P666_SALDO_VALOR_PAGO'
,p_item_sequence=>181
,p_item_plug_id=>wwv_flow_imp.id(58980254138931409)
,p_item_default=>'0'
,p_prompt=>'Saldo Valor Pago'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58987076319931419)
,p_name=>'P666_ERROR'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(58980254138931409)
,p_item_default=>'NULL;'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Error'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:16; color:AA0000"'
,p_colspan=>4
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P666_ERROR'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58987259101931420)
,p_name=>'P666_P0_ERROR'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(58980254138931409)
,p_use_cache_before_default=>'NO'
,p_prompt=>'P0 Error'
,p_source=>':P0_ERROR'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58987852063931423)
,p_name=>'P666_PER_TIPO_IDENTIFICACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*IF :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')  THEN',
'return pq_constantes.fn_retorna_constante(NULL,''cv_per_tipo_iden_ced'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(NULL,''cv_per_tipo_iden_ruc''); ',
'END IF*/null'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tipo Identificacion:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58988051497931424)
,p_name=>'P666_FECHA_DESDE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Fecha Desde:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58988258196931425)
,p_name=>'P666_FECHA_HASTA'
,p_item_sequence=>25
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Fecha Hasta:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="pr_ejecuta_proc_solo_campos(''PR_CARGA_DIV_PENDIENTES'',''P6_CLI_ID'',''P6_F_EMP_ID'',''P6_F_SEG_ID'',''P6_FECHA_DESDE'',''P6_FECHA_HASTA'',''R70263701157111210'',''R70272713295111230'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_item_comment=>'No tenia condicion.'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58988483265931426)
,p_name=>'P666_CLI_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Cliente: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>5
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58988659184931426)
,p_name=>'P666_IDENTIFICACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'ID. Cliente:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT   per.per_nro_identificacion||'' - ''||per.nombre_completo ||'' - ''||per.cli_id Nombre, per.per_nro_identificacion',
'FROM     v_asdm_datos_clientes PER',
'where  per.emp_id=:f_emp_id;'))
,p_lov_display_null=>'YES'
,p_cSize=>20
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')";onKeyDown="if(event.keyCode==13) doSubmit(''cliente'');"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'POPUP'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
,p_item_comment=>'onchange="doSubmit(''cliente'')"'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58988881312931427)
,p_name=>'P666_NOMBRE'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58989069413931427)
,p_name=>'P666_CORREO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58989269738931428)
,p_name=>'P666_DIRECCION'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58989453140931428)
,p_name=>'P666_TIPO_DIRECCION'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>unistr('Direcci\00BF\00BFn')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58989655112931428)
,p_name=>'P666_TIPO_TELEFONO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>unistr('Tel\00BF\00BFfono')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58989871428931428)
,p_name=>'P666_TELEFONO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58990055682931429)
,p_name=>'P666_CLIENTE_EXISTE'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58990278691931429)
,p_name=>'P666_CXC_ID'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58990476634931429)
,p_name=>'P666_VALOR_PAGO'
,p_item_sequence=>168
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_item_default=>'0'
,p_prompt=>'Pagar lo que alcance con este valor '
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''valor_cuota'')";onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Con este valor se cancelaran las CUOTAS VENCIDAS que alcancen.'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58990964557931434)
,p_name=>'P666_TOTAL_CHEQUES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>12
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P666_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58991170105931435)
,p_name=>'P666_SALDO_ANTICIPOS'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Anticipos'
,p_source=>'pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P666_CLI_ID,:f_uge_id);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>12
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P666_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P666_CLI_ID,:f_uge_id);'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58991372655931435)
,p_name=>'P666_DIR_ID'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Dir Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58991563168931435)
,p_name=>'P666_CLI_ID_REFERIDO'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Cli Id Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58991753693931435)
,p_name=>'P666_CLI_ID_NOMBRE_REFERIDO'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Cli Id Nombre Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58991952160931436)
,p_name=>'P666_F_EMP_ID'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Emp Id'
,p_source=>':F_EMP_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58992174788931438)
,p_name=>'P666_F_SEG_ID'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Seg Id'
,p_source=>':F_SEG_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58992352611931438)
,p_name=>'P666_FECHA_HASTA_CORTE'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Fecha Hasta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_FECHAS_CORTE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c001 DESCRIPCION,',
'       a.c002 VALOR',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_FECHAS_CORTE'''))
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
,p_item_comment=>'onChange="pr_ejecuta_proc_solo_campos(''PR_FILTRA_DIV_PENDIENTES'',''P666_CLI_ID'',''P666_F_EMP_ID'',''P666_F_SEG_ID'',''P666_FECHA_DESDE'',''P666_FECHA_HASTA_CORTE'',''R70263701157111210'',''R70272713295111230'')"'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58992565206931439)
,p_name=>'P666_AGENTE_COBRADOR'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Cobrador:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58992767568931439)
,p_name=>'P666_TFP_ID'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_item_default=>'0'
,p_prompt=>'Tfp Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58992951756931439)
,p_name=>'P666_NOMBRE_INSTITUCION'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'  -  '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58993183291931441)
,p_name=>'P666_INS_ID'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>unistr('Instituci\00F3n:')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58993354813931441)
,p_name=>'P666_MENSAJE'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'<SPAN STYLE="font-size: 15pt;color:RED">APLICAR EL PAGO CON ANTICIPO DE CLIENTES</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>10
,p_rowspan=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'NVL(:P666_SALDO_ANTICIPOS,0) > 0  and :P666_CLI_ID is not null and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58993565004931442)
,p_name=>'P666_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Pago Refinanciamiento'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58993765791931442)
,p_name=>'P666_SALDO_CXC_REF'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Saldo Cxc Ref'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(58993954093931442)
,p_name=>'P666_INTERESES_NO_PAG_RE'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_imp.id(58987675224931422)
,p_prompt=>'Intereses No Pag Re'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(58997257171931451)
,p_validation_name=>'valor mayor 0'
,p_validation_sequence=>10
,p_validation=>'v(''P666_VALOR_PAGO'') > 0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese el valor pago mayor a 0'
,p_validation_condition=>'cuotas'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(58990476634931429)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(58997465322931455)
,p_validation_name=>'P666_VALOR_PAGO_COLECCION_MAYOR_A_CERO'
,p_validation_sequence=>20
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_total number;',
'begin',
'',
'SELECT NVL(SUM(c016),0)',
'into ln_total',
' FROM apex_collections',
'WHERE collection_name =       pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')',
'    and c001 = :P666_CLI_ID;',
'',
'if ln_total=0 then',
'raise_application_error(-20000,''Error'');',
'end if;',
'',
'end;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'El valor a cancelar debe ser mayor a cero'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(102524481216876145)
,p_associated_item=>wwv_flow_imp.id(58975169545931368)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(58999977538931464)
,p_name=>'pr_elimina_coleccion_mov'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'unload'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(59000252074931466)
,p_event_id=>wwv_flow_imp.id(58999977538931464)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
''))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(59000365380931469)
,p_name=>'pr_actualiza_pagina'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(59000678607931470)
,p_event_id=>wwv_flow_imp.id(59000365380931469)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(58976761614931371)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(59000767890931470)
,p_name=>'pr_actualiza_item_condonar'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P666_CONDONAR_TODO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(59001080866931470)
,p_event_id=>wwv_flow_imp.id(59000767890931470)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(58980254138931409)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(59001169124931470)
,p_name=>'ad_carga_Segmento'
,p_event_sequence=>40
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(59001475532931471)
,p_event_id=>wwv_flow_imp.id(59001169124931470)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P666_F_SEG_ID'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(288747191954824637)
,p_name=>'da_actualiza'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P666_IDENTIFICACION'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(288747334391824638)
,p_event_id=>wwv_flow_imp.id(288747191954824637)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT   per.nombre_completo,per.cli_id ',
'into :P666_NOMBRE, :P666_CLI_ID',
'FROM     v_asdm_datos_clientes PER',
'where  per.emp_id=:f_emp_id',
'and  per.per_nro_identificacion =:P666_IDENTIFICACION;'))
,p_attribute_02=>'P666_IDENTIFICACION'
,p_attribute_03=>'P666_CLI_ID,P666_NOMBRE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58997561123931455)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'',
'ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja'');',
':P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota'');',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:F_EMP_ID,',
'                                                        :F_PCA_ID,',
'                                                        ln_tgr_id ,',
'                                                        :P0_TTR_DESCRIPCION, ',
'                                                        :P0_PERIODO,',
'                                                        :P0_DATFOLIO,',
'                                                        :P0_FOL_SEC_ACTUAL,',
'                                                        :P0_pue_num_sri,',
'                                                        :P0_uge_num_est_sri,',
'                                                        :P0_PUE_ID,',
'                                                        :P0_TTR_ID,',
'                                                        :P0_NRO_FOLIO,',
'                                                        :P666_COM_NUMERO_COMPLETO);',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>26744409854166529
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58997770039931457)
,p_process_sequence=>3
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_colec_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':p666_error:=null;',
'pq_ven_pagos_cuota_retencion.pr_elimina_colec_pago_cuota(:P666_SEQ_ID, ',
'                                               :P666_CLI_ID, ',
'                                               :P666_DIV_NRO_VENCIMIENTO, ',
'                                               :f_emp_id, ',
'                                               :P666_CXC_ID, ',
'                                               :P666_VALOR_PAGO_COLECCION, ',
'                                               :P666_SALDO_VALOR_PAGO, ',
'                                               :P666_ERROR);',
':P666_SEQ_ID := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'(:REQUEST = ''eliminar_cuota'') AND (:P666_SEQ_ID IS NOT NULL) '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>26744618770166531
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58997973166931457)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'ln_age_id number;',
'ln_zon_id number;',
'lv_zona varchar2(100);',
'ln_size_id NUMBER(10);',
'BEGIN',
':P666_NOMBRE_INSTITUCION :=  NULL;',
':P666_INS_ID := NULL;',
':P666_CLI_ID := NULL;',
':P666_AGENTE_COBRADOR := NULL;',
'',
'---------------------------------------------------',
'IF :P666_IDENTIFICACION IS NOT NULL THEN',
'   SELECT   LENGTH(:P666_IDENTIFICACION)',
'   INTO     ln_size_id',
'   FROM     DUAL;',
'',
'   IF ln_size_id = 13 THEN',
'      :P666_PER_TIPO_IDENTIFICACION := ''RUC'';',
'   ELSIF ln_size_id = 10 THEN',
'      :P666_PER_TIPO_IDENTIFICACION := ''CED'';',
'   ELSE',
'      :P666_PER_TIPO_IDENTIFICACION := ''PAS'';',
'   END IF;',
'END IF;',
'----------------------------------------------------',
'',
'pq_ven_listas2.pr_datos_clientes(:F_UGE_ID,                 ',
'                                :F_EMP_ID,                 ',
'                                :P666_IDENTIFICACION, ',
'                                :P666_NOMBRE,                 ',
'                                :P666_CLI_ID,',
'                                :P666_CORREO,',
'                                :P666_DIRECCION,',
'                                :P666_TIPO_DIRECCION,',
'                                :P666_TELEFONO,',
'                                :P666_TIPO_TELEFONO,',
'                                :P666_CLIENTE_EXISTE,',
'                                :P666_DIR_ID,',
'                                :P666_CLI_ID_REFERIDO,',
'                                :P666_CLI_ID_NOMBRE_REFERIDO,',
'                                :P666_PER_TIPO_IDENTIFICACION,',
'                                :P0_ERROR);',
'',
'/*:P666_NOMBRE := replace(:P666_NOMBRE,''&'','' '');     ',
'-- Yguaman 2011/12/15. Institucion de Cliente',
'BEGIN',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_PREC_SELECCIONADOS'');',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_DIV_SELECCIONADOS'');',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''CO_DIV_PENDIENTES'');',
'',
'',
'SELECT ains.ins_id,ape.per_razon_social',
'INTO :P666_INS_ID, :P666_NOMBRE_INSTITUCION',
'FROM asdm_clientes_instituciones acin, asdm_instituciones ains, asdm_personas ape',
'WHERE acin.cli_id = :P666_CLI_ID',
'AND acin.cin_estado_registro = pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'AND acin.ins_id = ains.ins_id',
'AND ains.per_id = ape.per_id',
'AND ains.ins_estado_registro = pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'AND ape.per_estado_registro = pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'AND APE.EMP_ID = 1',
'AND acin.CIN_ID = (SELECT MAX(CI.CIN_ID)',
'                   FROM ASDM_CLIENTES_INSTITUCIONES CI',
'                   WHERE CI.CLI_ID = :P666_CLI_ID',
'                   AND CI.CIN_ESTADO_REGISTRO = 0',
'                   AND CI.EMP_ID = 1)',
';',
'',
'EXCEPTION',
'WHEN NO_DATA_FOUND THEN',
' :P666_INS_ID := NULL;',
' :P666_NOMBRE_INSTITUCION := ''NO ASIGNANDO'';',
'',
'END;',
'pq_ven_pagos_cuota.pr_carga_saldos_cliente(:P666_CLI_ID,',
'                                           :F_EMP_ID,',
'                                           :P666_TOTAL_CHEQUES,',
'                                           :P0_ERROR);',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'',
'if :P666_CLI_ID is not  null and :f_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'') then',
'',
'-- Yguaman 2011/11/23 18:16pm, agregado para grabar el cobrador cuando se va a generar la transaccion de pago de cuota',
'-- Solo para minoreo y cuando es el pago de cuota -- Jandres.',
'',
' :P666_AGENTE_COBRADOR := pq_car_cobranza.fn_retorna_cobrador(pn_emp_id =>:F_EMP_ID ,  -- cdigo empresa',
'                                                          pn_cli_id => :P666_CLI_ID,  -- codigo cliente',
'                                                          pn_uge_id => :F_UGE_ID); -- codigo unidad de gestion',
'end if;',
'',
':P666_COM_ID := NULL;',
':P666_VALOR_PAGO := 0;',
':P666_SALDO_VALOR_PAGO := 0;',
':P666_CXC_ID := null;',
'*/',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>26744821897166531
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58998181537931458)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_div_pendientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'/*pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id  => :P666_CLI_ID,',
'                                     pn_emp_id  => :f_emp_id,',
'                                     pn_seg_id => :F_SEG_ID,',
'                                     pd_fecha_desde  => :P666_FECHA_DESDE,',
'                                     pd_fecha_hasta => :P666_FECHA_HASTA_CORTE,                                   ',
'                                     pv_error  => :P0_error);*/',
'',
'',
'pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id  => :P666_CLI_ID,',
'                                     pn_emp_id  => :f_emp_id,',
'                                     pn_seg_id => :F_SEG_ID,',
'                                     pd_fecha_desde  => NULL,',
'                                     pd_fecha_hasta => NULL, ',
'                                     pn_tfp_id      => :P666_TFP_ID,                                  ',
'                                     pv_error  => :P0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>26745030268166532
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58998358365931458)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cuotas_vencidas_con_valor_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'--lb_existe BOOLEAN;',
'',
'BEGIN',
'',
'/*:P666_FECHA_DESDE := NULL;',
':P666_FECHA_HASTA := NULL;',
':P666_FECHA_HASTA_CORTE := NULL;*/',
'',
':P666_SALDO_VALOR_PAGO := :P666_VALOR_PAGO;',
'       ',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'--pq_inv_movimientos.pr_crea_borra_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_ven_pagos_cuota.pr_carga_colec_cuotas_vencidas(:P666_CLI_ID, ',
'                                                  :F_EMP_ID, ',
'                                                  :P666_SALDO_VALOR_PAGO, ',
'                                                  :P0_ERROR);',
'',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'valor_cuota'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>26745207096166532
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58998573179931459)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_coleccion_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'/*pq_ven_pagos_cuota.pr_actualiza_colec_div_pagar(:f_emp_id,:P666_ERROR);*/',
'',
'',
'pq_ven_pagos_cuota.pr_actualiza_colec_div_pagar(pn_emp_id => :F_EMP_ID,',
'                             pn_div_id => 0,',
'                             pn_total  => 0,',
'                             pn_im_cond => 0,',
'                             pn_gc_cond => 0,',
'                             pn_tse_id  => 0,',
'                             pv_error  => :P666_ERROR);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'actualiza_cuota'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'Actualiza cuota '
,p_internal_uid=>26745421910166533
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58998760030931459)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_colec_div_pagar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota.pr_carga_colec_div_pagar( pn_seq_id => NULL,',
'                                              pv_error         => :p0_error,',
'                                              pn_emp_id        => :f_emp_id,',
'                                              pn_ttr_id_origen => pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_pago_cuota''),',
'                                              pn_tse_id => :F_SEG_ID);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'paga_selec'
,p_process_when_type=>'NEVER'
,p_internal_uid=>26745608761166533
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58999569711931460)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_int_condonado'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_VEN_PAGOS_CUOTA.pr_actualiza_int_condonado(pn_emp_id => :F_EMP_ID,',
'                                       pv_selecc =>:P666_CONDONAR_TODO,                                 ',
'                                       pv_error  =>   :P0_error,',
'                                       pn_tse_id =>   :F_SEG_ID,',
'                                       pn_tfp_id =>   :P666_TFP_ID);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''act_int_condonado'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>'bien'
,p_internal_uid=>26746418442166534
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(59070775751345727)
,p_process_sequence=>5
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_vacia_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P666_CLI_ID := NULL;',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_PREC_SELECCIONADOS'');',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_DIV_SELECCIONADOS'');',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''CO_DIV_PENDIENTES'');'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>26817624481580801
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58998965463931459)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_coleccion_cuotas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_PREC_SELECCIONADOS'');',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_DIV_SELECCIONADOS'');'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P666_CLI_ID is null OR :REQUEST = ''cliente'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>26745814194166533
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58999164124931459)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_col_div_pendientes'
,p_process_sql_clob=>'pq_inv_movimientos.pr_elimina_colecciones(''CO_DIV_PENDIENTES'');'
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P666_CLI_ID is null OR :REQUEST = ''cliente'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>26746012855166533
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58999380886931459)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_FECHAS_CORTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  pq_car_credito.pr_fechas_corte_pago_cuota(pn_emp_id => :f_emp_id,',
'                              pn_tse_id => :F_SEG_ID,',
'                              pd_fecha  => SYSDATE, ',
'                              pv_error  => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P666_CLI_ID is null and :P666_IDENTIFICACION is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>26746229617166533
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(58999773742931460)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes_re'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_age_id NUMBER;',
'  ln_zon_id NUMBER;',
'  lv_zona   VARCHAR2(100);',
'',
'  ln_car_id car_abonos_refinanciamiento.car_id%TYPE;',
'',
'BEGIN',
'  :p666_nombre_institucion := NULL;',
'  :p666_ins_id             := NULL;',
'  :p666_cli_id             := NULL;',
'  :p666_agente_cobrador    := NULL;',
'  pq_ven_listas2.pr_datos_clientes(:f_uge_id,',
'                                  :f_emp_id,',
'                                  :p666_identificacion,',
'                                  :p666_nombre,',
'                                  :p666_cli_id,',
'                                  :p666_correo,',
'                                  :p666_direccion,',
'                                  :p666_tipo_direccion,',
'                                  :p666_telefono,',
'                                  :p666_tipo_telefono,',
'                                  :p666_cliente_existe,',
'                                  :p666_dir_id,',
'                                  :p666_cli_id_referido,',
'                                  :p666_cli_id_nombre_referido,',
'                                  :p666_per_tipo_identificacion,',
'                                  :p0_error);',
'',
'  :p666_nombre := REPLACE(:p666_nombre, ''&'', '' '');',
'',
'  -- Yguaman 2011/12/15. Institucion de Cliente',
'  BEGIN',
'    SELECT ains.ins_id, ape.per_razon_social',
'      INTO :p666_ins_id, :p666_nombre_institucion',
'      FROM asdm_clientes_instituciones acin,',
'           asdm_instituciones          ains,',
'           asdm_personas               ape',
'     WHERE acin.cli_id = :p666_cli_id',
'       AND acin.cin_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'       AND acin.ins_id = ains.ins_id',
'       AND ains.per_id = ape.per_id',
'       AND ains.ins_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'       AND ape.per_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'');',
'  ',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      :p666_ins_id             := NULL;',
'      :p666_nombre_institucion := ''NO ASIGNANDO'';',
'    ',
'  END;',
'  pq_ven_pagos_cuota.pr_carga_saldos_cliente(:p666_cli_id,',
'                                             :f_emp_id,',
'                                             :p666_total_cheques,',
'                                             :p0_error);',
'',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_pago_cuota''));',
'',
'  IF :p666_cli_id IS NOT NULL AND',
'     :f_seg_id =',
'     pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_minoreo'') THEN',
'  ',
'    -- Yguaman 2011/11/23 18:16pm, agregado para grabar el cobrador cuando se va a generar la transaccion de pago de cuota',
'    -- Solo para minoreo y cuando es el pago de cuota -- Jandres.',
'  ',
'    /*ln_age_id := pq_car_cobranza.fn_retorna_ata_id_cobrador(pn_emp_id =>:F_EMP_ID ,  -- cdigo empresa',
'    pn_cli_id => :P666_CLI_ID,  -- codigo cliente',
'    pn_uge_id => :F_UGE_ID); -- codigo unidad de gestion*/',
'  ',
'    :p666_agente_cobrador := pq_car_cobranza.fn_retorna_cobrador(pn_emp_id => :f_emp_id, -- cdigo empresa',
'                                                               pn_cli_id => :p666_cli_id, -- codigo cliente',
'                                                               pn_uge_id => :f_uge_id); -- codigo unidad de gestion',
'  ',
'    /*if :P666_AGENTE_COBRADOR is null then',
'      raise_application_error(-20000,''El Cliente: ''||:P666_NOMBRE||'' No tienen asignado un cobrador o una zona.... Debe asignar Cobrador al Cliente para continuar.''); ',
'    end if;*/',
'  ',
'    /*BEGIN',
'    ',
'    ',
'    ',
'    SELECT pe.per_primer_apellido || '' '' || pe.per_segundo_apellido || '' '' ||',
'           pe.per_primer_nombre || '' '' || pe.per_segundo_nombre',
'      INTO :P666_AGENTE_COBRADOR',
'      FROM asdm_agentes_tagentes ata, asdm_agentes age, asdm_personas pe',
'     WHERE ata.ata_id = ln_age_id',
'       AND ata.age_id = age.age_id',
'       AND age.per_id = pe.per_id',
'       AND age.age_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'');',
'    ',
'    ',
'      EXCEPTION',
'    WHEN OTHERS THEN',
'      raise_application_error(-20000,''Debe asignar Cobrador al Cliente para continuar.'');',
'     END;*/',
'  END IF;',
'',
'  :p666_com_id           := NULL;',
'  :p666_valor_pago       := 0;',
'  :p666_saldo_valor_pago := 0;',
'  :p666_cxc_id           := NULL;',
'',
'  BEGIN',
'    /*pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id  => :P666_CLI_ID,',
'    pn_emp_id  => :f_emp_id,',
'    pn_seg_id => :F_SEG_ID,',
'    pd_fecha_desde  => :P666_FECHA_DESDE,',
'    pd_fecha_hasta => :P666_FECHA_HASTA_CORTE,                                   ',
'    pv_error  => :P0_error);*/',
'  ',
'    pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id      => :p666_cli_id,',
'                                                    pn_emp_id      => :f_emp_id,',
'                                                    pn_seg_id      => :f_seg_id,',
'                                                    pd_fecha_desde => NULL,',
'                                                    pd_fecha_hasta => NULL,',
'                                                    pn_tfp_id      => :p666_tfp_id,',
'                                                    pv_error       => :p0_error);',
'  ',
'  END;',
'',
'  BEGIN',
'  ',
'    BEGIN',
'    ',
'      SELECT ab.car_id',
'        INTO ln_car_id',
'        FROM car_abonos_refinanciamiento ab',
'       WHERE ab.cxc_id = :p666_cxc_id_ref;',
'    ',
'    EXCEPTION',
'      WHEN no_data_found THEN',
'        ln_car_id := NULL;',
'    END;',
'  ',
'    IF ln_car_id IS NULL THEN',
'      pq_car_dml.pr_ins_car_abonos_refin(pn_car_id               => ln_car_id,',
'                                         pn_emp_id               => :f_emp_id,',
'                                         pd_car_fecha            => SYSDATE,',
'                                         pn_car_capital          => :p666_valor_capital_refin,',
'                                         pn_car_intereses        => :p666_valor_refinanciamiento,',
'                                         pn_car_abono            => 0,',
'                                         pn_car_diferencia       => 0,',
'                                         pv_car_estado_registro  => pq_constantes.fn_retorna_constante(0,',
'                                                                                                       ''cv_estado_reg_activo''),',
'                                         pn_cxc_id               => :p666_cxc_id_ref,',
'                                         pn_car_saldo_cxc_ant    => :p666_saldo_cxc_ref,',
'                                         pn_car_intereses_no_pag => :p666_intereses_no_pag_re,',
'                                         pn_car_abonos_capital   => nvl(pq_car_refin_precancelacion.pr_valida_abono_ant(pn_cxc_id => :p666_cxc_id_ref),',
'                                                                        0),',
'                                         pv_error                => :p0_error);',
'    ',
'    ELSE',
'    ',
'      pq_car_dml.pr_upd_car_abonos_refin(pn_car_id               => ln_car_id,',
'                                         pn_emp_id               => :f_emp_id,',
'                                         pd_car_fecha            => SYSDATE,',
'                                         pn_car_capital          => :p666_valor_capital_refin,',
'                                         pn_car_intereses        => :p666_valor_refinanciamiento,',
'                                         pn_car_abono            => 0,',
'                                         pn_car_diferencia       => 0,',
'                                         pv_car_estado_registro  => pq_constantes.fn_retorna_constante(0,',
'                                                                                                       ''cv_estado_reg_activo''),',
'                                         pn_cxc_id               => :p666_cxc_id_ref,',
'                                         pn_car_saldo_cxc_ant    => :p666_saldo_cxc_ref,',
'                                         pn_car_intereses_no_pag => :p666_intereses_no_pag_re,',
'                                         pn_car_abonos_capital   => nvl(pq_car_refin_precancelacion.pr_valida_abono_ant(pn_cxc_id => :p666_cxc_id_ref),',
'                                                                        0),',
'                                         pv_error                => :p0_error);',
'    END IF;',
'  ',
'  END;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''cliente'' and :P666_PAGO_REFINANCIAMIENTO = 1'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>26746622473166534
);
wwv_flow_imp.component_end;
end;
/
