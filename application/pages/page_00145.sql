prompt --application/pages/page_00145
begin
--   Manifest
--     PAGE: 00145
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>145
,p_name=>'Mantenimiento de Clientes'
,p_page_mode=>'MODAL'
,p_step_title=>'Mantenimiento de Clientes'
,p_autocomplete_on_off=>'OFF'
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#WORKSPACE_IMAGES#toastr.min.js',
'#WORKSPACE_IMAGES#ValidacionesUtiles.js'))
,p_javascript_code_onload=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/**',
' * Get the user IP throught the webkitRTCPeerConnection',
' * @param onNewIP {Function} listener function to expose the IP locally',
' * @return undefined',
' */',
'function getUserIP(onNewIP) { //  onNewIp - your listener function for new IPs',
'    //compatibility for firefox and chrome',
'    var myPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;',
'    var pc = new myPeerConnection({',
'        iceServers: []',
'    }),',
'    noop = function() {},',
'    localIPs = {},',
'    ipRegex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g,',
'    key;',
'',
'    function iterateIP(ip) {',
'        if (!localIPs[ip]) onNewIP(ip);',
'        localIPs[ip] = true;',
'    }',
'',
'     //create a bogus data channel',
'    pc.createDataChannel("");',
'',
'    // create offer and set local description',
'    pc.createOffer().then(function(sdp) {',
'        sdp.sdp.split(''\n'').forEach(function(line) {',
'            if (line.indexOf(''candidate'') < 0) return;',
'            line.match(ipRegex).forEach(iterateIP);',
'        });',
'        ',
'        pc.setLocalDescription(sdp, noop, noop);',
'    }).catch(function(reason) {',
'        // An error occurred, so handle the failure to connect',
'    });',
'',
'    //listen for candidate events',
'    pc.onicecandidate = function(ice) {',
'        if (!ice || !ice.candidate || !ice.candidate.candidate || !ice.candidate.candidate.match(ipRegex)) return;',
'        ice.candidate.candidate.match(ipRegex).forEach(iterateIP);',
'    };',
'}',
'',
'// Usage',
'',
'getUserIP(function(ip){',
'    //alert("Got IP! :" + ip);',
'    apex.item( "P145_IP" ).setValue(ip)',
'});'))
,p_css_file_urls=>'#WORKSPACE_IMAGES#toastr.min.css'
,p_step_template=>wwv_flow_imp.id(37370553897200142)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_height=>'500'
,p_dialog_width=>'800'
,p_page_component_map=>'16'
,p_last_updated_by=>'JALVAREZ'
,p_last_upd_yyyymmddhh24miss=>'20230613220815'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(68857626372770476178)
,p_plug_name=>'Datos del Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526680051046670)
,p_plug_display_sequence=>10
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(15339997669681844309)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_button_name=>'grabar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>':P145_CLI_ID IS NULL AND :P145_PAG IS NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(194470729582445079272)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_button_name=>'btn_grabar_15'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>':P145_CLI_ID IS NULL AND :P145_PAG IS NOT NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(15339998146189844309)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_button_name=>'grabar_cambios'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Grabar cambios'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>':P145_CLI_ID IS NOT NULL AND :P145_PAG IS NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(194470729924948079275)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_button_name=>'grabar_cambios_15'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Grabar cambios'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>':P145_CLI_ID IS NOT NULL AND :P145_PAG IS NOT NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(15339998452367844310)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_button_name=>'CANCELAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(206176472564847967429)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_button_name=>'BTN_REGRESAR'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=242:15:&SESSION.:cliente,payjoy:&DEBUG.:RP:P15_IDENTIFICACION,P15_CARGAR_NUEVO:&P145_PER_NRO_IDENTIFICACION.,S'
,p_button_condition=>'P145_PAG'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15339998944647844310)
,p_name=>'P145_TEL_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15339999348803844310)
,p_name=>'P145_DIR_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15339999749514844312)
,p_name=>'P145_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340000129599844312)
,p_name=>'P145_PER_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340000480698844312)
,p_name=>'P145_PER_TIPO_IDENTIFICACION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_item_default=>'CED'
,p_prompt=>'Tipo identificaci&oacute;n'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340000893302844312)
,p_name=>'P145_PER_NRO_IDENTIFICACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Nro identificaci&oacute;n'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340001344151844312)
,p_name=>'P145_PER_CORREO_ELECTRONICO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Correo electr&oacute;nico'
,p_post_element_text=>'Ejem: username@dominio.com'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340001727320844313)
,p_name=>'P145_PER_PRIMER_NOMBRE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Primer Nombre:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340001962224844314)
,p_name=>'P145_PER_SEGUNDO_NOMBRE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Segundo Nombre:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340002397330844314)
,p_name=>'P145_PER_PRIMER_APELLIDO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Primer Apellido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340002802962844314)
,p_name=>'P145_PER_SEGUNDO_APELLIDO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Segundo Apellido'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340003175393844315)
,p_name=>'P145_PER_NOMBRE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Raz&oacute;n Social:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340003608414844316)
,p_name=>'P145_TDI_ID'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340004014891844316)
,p_name=>'P145_DIRECCION'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Direcci&oacute;n:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340004426332844316)
,p_name=>'P145_TTE_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15340004770560844316)
,p_name=>'P145_TELEFONO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_prompt=>'Tel&eacute;fono:'
,p_post_element_text=>'Ejem: 072831991 or 0991856974'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15342032600961429162)
,p_name=>'P145_TIPO_PERSONA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_item_default=>'N'
,p_prompt=>'Tipo Persona'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_PERSONAS'
,p_lov=>'.'||wwv_flow_imp.id(30383992406245595975)||'.'
,p_cHeight=>1
,p_display_when=>'P145_PER_TIPO_IDENTIFICACION'
,p_display_when2=>'RUC'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63377085796291802273)
,p_name=>'P145_IP'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(194470729422497079270)
,p_name=>'P145_PAG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(68857626372770476178)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340007590821844318)
,p_name=>'pr_hide_show_rsocial_nombres_tipo_identif'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_TIPO_IDENTIFICACION'
,p_condition_element=>'P145_PER_TIPO_IDENTIFICACION'
,p_triggering_condition_type=>'IN_LIST'
,p_triggering_expression=>'CED,PAS'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340008125493844319)
,p_event_id=>wwv_flow_imp.id(15340007590821844318)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_PRIMER_NOMBRE,P145_PER_SEGUNDO_NOMBRE,P145_PER_PRIMER_APELLIDO,P145_PER_SEGUNDO_APELLIDO'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340008633534844319)
,p_event_id=>wwv_flow_imp.id(15340007590821844318)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_NOMBRE,P145_TIPO_PERSONA'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340009119275844319)
,p_event_id=>wwv_flow_imp.id(15340007590821844318)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_NOMBRE,P145_TIPO_PERSONA'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340009610476844319)
,p_event_id=>wwv_flow_imp.id(15340007590821844318)
,p_event_result=>'FALSE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_PRIMER_NOMBRE,P145_PER_SEGUNDO_NOMBRE,P145_PER_PRIMER_APELLIDO,P145_PER_SEGUNDO_APELLIDO'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15342032706169429163)
,p_name=>'pr_hide_show_rsocial_nombres_tipo_pers'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_TIPO_PERSONA'
,p_condition_element=>'P145_TIPO_PERSONA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'N'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15342032841755429164)
,p_event_id=>wwv_flow_imp.id(15342032706169429163)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_PRIMER_NOMBRE,P145_PER_SEGUNDO_NOMBRE,P145_PER_PRIMER_APELLIDO,P145_PER_SEGUNDO_APELLIDO'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15342032864868429165)
,p_event_id=>wwv_flow_imp.id(15342032706169429163)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_NOMBRE'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15342033035817429166)
,p_event_id=>wwv_flow_imp.id(15342032706169429163)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_NOMBRE'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15342033132137429167)
,p_event_id=>wwv_flow_imp.id(15342032706169429163)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_PRIMER_NOMBRE,P145_PER_SEGUNDO_NOMBRE,P145_PER_PRIMER_APELLIDO,P145_PER_SEGUNDO_APELLIDO'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340009957626844319)
,p_name=>'pr_submit_buscar_databook'
,p_event_sequence=>30
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'EXPRESSION'
,p_display_when_cond=>':request = ''nuevo_cliente'' and :P145_PER_NRO_IDENTIFICACION IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340010469766844320)
,p_event_id=>wwv_flow_imp.id(15340009957626844319)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'buscar_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340010902853844320)
,p_name=>'da_onEnter_cedula'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_NRO_IDENTIFICACION'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'this.browserEvent.keyCode == 13'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340011392363844320)
,p_event_id=>wwv_flow_imp.id(15340010902853844320)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'buscar_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340011775723844320)
,p_name=>'da_cancel_Dialog'
,p_event_sequence=>50
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(15339998452367844310)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340012327045844320)
,p_event_id=>wwv_flow_imp.id(15340011775723844320)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP:=''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340012823857844321)
,p_event_id=>wwv_flow_imp.id(15340011775723844320)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DIALOG_CANCEL'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15340018044645844324)
,p_name=>'onChangeNroIdentificacion'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_NRO_IDENTIFICACION,P145_PER_TIPO_IDENTIFICACION'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15340018547383844325)
,p_event_id=>wwv_flow_imp.id(15340018044645844324)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'fnValidaIdentificacion(document.getElementById("P145_PER_NRO_IDENTIFICACION"),document.getElementById("P145_PER_TIPO_IDENTIFICACION"))',
'',
'',
' '))
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344464292302855068)
,p_name=>'da_onClickGrabar'
,p_event_sequence=>150
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(15339997669681844309)
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementB'
||'yId("P145_DIRECCION"))==0) || (fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementByI'
||'d("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TIPO_PERSONA").value == "J")'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
,p_da_event_comment=>'(fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getEl'
||'ementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0) || (fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFO'
||'NO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementById("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TI'
||'PO_PERSONA").value == "J")'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344464381529855069)
,p_event_id=>wwv_flow_imp.id(15344464292302855068)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'grabar'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(194470729719525079273)
,p_name=>'da_onClickGrabar_1'
,p_event_sequence=>160
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(194470729582445079272)
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementB'
||'yId("P145_DIRECCION"))==0) || (fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementByI'
||'d("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TIPO_PERSONA").value == "J")'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
,p_da_event_comment=>'(fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getEl'
||'ementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0) || (fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFO'
||'NO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementById("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TI'
||'PO_PERSONA").value == "J")'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(194470729808009079274)
,p_event_id=>wwv_flow_imp.id(194470729719525079273)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'grabar_15'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344464469525855070)
,p_name=>'da_onClickGrabarCambios'
,p_event_sequence=>170
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(15339998146189844309)
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementB'
||'yId("P145_DIRECCION"))==0) || (fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementByI'
||'d("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TIPO_PERSONA").value == "J")'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
,p_da_event_comment=>'(fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getEl'
||'ementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0) || (fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFO'
||'NO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementById("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TI'
||'PO_PERSONA").value == "J")'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344464622070855071)
,p_event_id=>wwv_flow_imp.id(15344464469525855070)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'grabar_cambios'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344893134220927938)
,p_event_id=>wwv_flow_imp.id(15344464469525855070)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'fnShowError("Datos del Cliente incompletos!","Ingrese todos los datos del cliente!");'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(194470729985087079276)
,p_name=>'da_onClickGrabarCambios_1'
,p_event_sequence=>180
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(194470729924948079275)
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementB'
||'yId("P145_DIRECCION"))==0) || (fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementByI'
||'d("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TIPO_PERSONA").value == "J")'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
,p_da_event_comment=>'(fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFONO"))==0 && fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE"))==0 && fnValidaNombre(document.getEl'
||'ementById("P145_PER_PRIMER_APELLIDO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0) || (fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0 && fnValidaTelefono(document.getElementById("P145_TELEFO'
||'NO"))==0 && fnValidaDireccion(document.getElementById("P145_DIRECCION"))==0 && fnValidaNombre(document.getElementById("P145_PER_NOMBRE"))==0 && document.getElementById("P145_PER_TIPO_IDENTIFICACION").value == "RUC" && document.getElementById("P145_TI'
||'PO_PERSONA").value == "J")'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(206176472360331967427)
,p_event_id=>wwv_flow_imp.id(194470729985087079276)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'grabar_cambios_15'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(206176472497526967428)
,p_event_id=>wwv_flow_imp.id(194470729985087079276)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'fnShowError("Datos del Cliente incompletos!","Ingrese todos los datos del cliente!");'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344464733184855072)
,p_name=>'da_onChangeCorreo'
,p_event_sequence=>190
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_CORREO_ELECTRONICO'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaCorreo(document.getElementById("P145_PER_CORREO_ELECTRONICO"),''S'') == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344464808448855073)
,p_event_id=>wwv_flow_imp.id(15344464733184855072)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_CORREO_ELECTRONICO'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344464941645855074)
,p_name=>'da_onChangeNombre1'
,p_event_sequence=>200
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_PRIMER_NOMBRE'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaNombre(document.getElementById("P145_PER_PRIMER_NOMBRE")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344465001878855075)
,p_event_id=>wwv_flow_imp.id(15344464941645855074)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_PRIMER_NOMBRE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344465139766855076)
,p_name=>'da_onChangeNombre2'
,p_event_sequence=>210
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_SEGUNDO_NOMBRE'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaNombre(document.getElementById("P145_PER_SEGUNDO_NOMBRE")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344892009487927927)
,p_event_id=>wwv_flow_imp.id(15344465139766855076)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_SEGUNDO_NOMBRE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344892295603927930)
,p_name=>'da_onChangeApellido1'
,p_event_sequence=>220
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_PRIMER_APELLIDO'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaNombre(document.getElementById("P145_PER_PRIMER_APELLIDO")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344892366401927931)
,p_event_id=>wwv_flow_imp.id(15344892295603927930)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_PRIMER_APELLIDO'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344892501724927932)
,p_name=>'da_onChangeApellido2'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_SEGUNDO_APELLIDO'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaNombre(document.getElementById("P145_PER_SEGUNDO_APELLIDO")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344892553628927933)
,p_event_id=>wwv_flow_imp.id(15344892501724927932)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_SEGUNDO_APELLIDO'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344892125573927928)
,p_name=>'da_onChangeRazonSocail'
,p_event_sequence=>240
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_PER_NOMBRE'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaDireccion(document.getElementById("P145_PER_NOMBRE")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344892160897927929)
,p_event_id=>wwv_flow_imp.id(15344892125573927928)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_PER_NOMBRE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344892662329927934)
,p_name=>'da_onChangeDireccion'
,p_event_sequence=>250
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_DIRECCION'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaDireccion(document.getElementById("P145_DIRECCION")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344892774080927935)
,p_event_id=>wwv_flow_imp.id(15344892662329927934)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_DIRECCION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(15344892912544927936)
,p_name=>'da_onChangeTelefono'
,p_event_sequence=>260
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P145_TELEFONO'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'fnValidaTelefono(document.getElementById("P145_TELEFONO")) == 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(15344892956476927937)
,p_event_id=>wwv_flow_imp.id(15344892912544927936)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P145_TELEFONO'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15340006819600844318)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_borra_datos_anteriores'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'      :p145_per_id                 := NULL;',
'      :p145_per_correo_electronico := NULL;',
'      :p145_per_primer_nombre      := NULL;',
'      :p145_per_segundo_nombre     := NULL;',
'      :p145_per_primer_apellido    := NULL;',
'      :p145_per_segundo_apellido   := NULL;',
'      :p145_per_nombre             := NULL;',
'      :p145_direccion              := NULL;',
'      :p145_telefono               := NULL;',
'      :p145_per_nombre             := NULL;',
'      :p145_dir_id                 := NULL;',
'      :p145_tel_id                 := NULL;',
'      :p145_cli_id                 := NULL;',
'      :P145_TIPO_PERSONA           := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'nuevo_cliente,editar_cliente'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>15307753668331079392
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15340005205189844317)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cliente_edit'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'--raise_application_error(-20001,''ERR''||:p145_per_nro_identificacion);',
'  SELECT p.per_id,',
'         p.per_correo_electronico,',
'         p.per_primer_nombre,',
'         p.per_segundo_nombre,',
'         p.per_primer_apellido,',
'         p.per_segundo_apellido,',
'         p.per_razon_social,',
'         d.dir_descripcion,',
'         t.tel_numero,',
'         d.dir_id,',
'         t.tel_id,',
'         c.cli_id,',
'         p.PER_TIPO_PERSONA',
'    INTO :p145_per_id,',
'         :p145_per_correo_electronico,',
'         :p145_per_primer_nombre,',
'         :p145_per_segundo_nombre,',
'         :p145_per_primer_apellido,',
'         :p145_per_segundo_apellido,',
'         :p145_per_nombre,',
'         :p145_direccion,',
'         :p145_telefono,',
'         :p145_dir_id,',
'         :p145_tel_id,',
'         :p145_cli_id,',
'         :P145_TIPO_PERSONA',
'    FROM asdm_personas    p,',
'         asdm_telefonos   t,',
'         asdm_direcciones d,',
'         asdm_clientes    c',
'   WHERE p.per_nro_identificacion = :p145_per_nro_identificacion',
'     AND p.emp_id = :f_emp_id',
'     AND p.per_tipo_identificacion = :p145_per_tipo_identificacion',
'     AND t.per_id(+) = p.per_id',
'     AND t.tte_id(+) = :p145_tte_id',
'     AND t.tel_estado_registro(+) = :f_estado_reg_activo',
'     AND d.per_id(+) = p.per_id',
'     AND d.dir_estado_registro(+) = :f_estado_reg_activo',
'     AND d.tdi_id(+) = :p145_tdi_id',
'     AND c.per_id(+) = p.per_id',
'     AND c.emp_id(+) = p.emp_id;',
'  IF NVL(:P145_TIPO_PERSONA,''N'') = ''N'' THEN -- DLOPEZ solo si es persona natural toma nombres, caso contrario mantiene la razon social',
'      IF :p145_per_primer_apellido IS NOT NULL THEN',
'        :p145_per_nombre := :p145_per_primer_nombre || '' '' ||',
'                            :p145_per_segundo_nombre || '' '' ||',
'                            :p145_per_primer_apellido || '' '' ||',
'                            :p145_per_segundo_apellido;',
'                            ',
'         :P145_TIPO_PERSONA := ''N''   ;                ',
'      END IF;',
'END IF;',
' ',
'',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    --raise_application_error(-20001,''ERR'');',
'    IF :p145_per_primer_apellido IS NOT NULL THEN',
'      :p145_per_id                 := NULL;',
'      :p145_per_correo_electronico := NULL;',
'      :p145_per_primer_nombre      := NULL;',
'      :p145_per_segundo_nombre     := NULL;',
'      :p145_per_primer_apellido    := NULL;',
'      :p145_per_segundo_apellido   := NULL;',
'      :p145_per_nombre             := NULL;',
'      :p145_direccion              := NULL;',
'      :p145_telefono               := NULL;',
'      :p145_per_nombre             := NULL;',
'      :p145_dir_id                 := NULL;',
'      :p145_tel_id                 := NULL;',
'      :p145_cli_id                 := NULL;',
'      :P145_TIPO_PERSONA           := NULL;',
'    END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''editar_cliente'' and :P145_PER_NRO_IDENTIFICACION IS NOT NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>15307752053920079391
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15342029677550429133)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Carga_datos_persona_no_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--DLOPEZ CARGA DATOS CUANDO EXISTE UNA PERSONA CARGADA PERO NO ESTA GENERADO COMO CLIENTE',
'begin',
'  IF :p145_per_id IS NULL THEN',
'    BEGIN',
'      SELECT p.per_id',
'        into :p145_per_id',
'        FROM ASDM_PERSONAS P',
'       WHERE p.per_nro_identificacion = :p145_per_nro_identificacion',
'         and p.per_tipo_identificacion = :p145_per_tipo_identificacion',
'          and p.emp_id = :F_EMP_ID; -- SE AGREGA EMPRESA DL',
'    exception',
'      when others then',
'        :p145_per_id := null;',
'    END;',
'  END IF;',
'',
'  IF :p145_per_id IS NOT NULL THEN',
'    IF :p145_cli_id IS NULL THEN',
'      BEGIN',
'        SELECT c.cli_id',
'          into :p145_cli_id',
'          FROM asdm_clientes c',
'         WHERE c.per_id = :p145_per_id',
'         and c.cli_estado_registro = :f_estado_reg_activo;',
'      exception',
'        when others then',
'          :p145_cli_id := null;',
'      END;',
'    END IF;',
'    ',
'    IF :p145_dir_id IS NULL THEN',
'      BEGIN',
'        SELECT D.DIR_ID',
'          INTO :p145_dir_id',
'          FROM ASDM_DIRECCIONES D',
'         WHERE PER_ID = :p145_per_id',
'         and d.dir_estado_registro = :f_estado_reg_activo',
'         AND D.TDI_ID = :p145_tdi_id;',
'      exception',
'        when others then',
'          :p145_dir_id := null;',
'      END;',
'    END IF;',
'  ',
'    IF :p145_tel_id IS NULL THEN',
'      BEGIN',
'        SELECT T.TEL_ID',
'          INTO :p145_tel_id',
'          FROM ASDM_TELEFONOS T',
'         WHERE PER_ID = :p145_per_id',
'         and t.tel_estado_registro = :f_estado_reg_activo',
'         AND T.TTE_ID = :p145_tte_id;',
'      exception',
'        when others then',
'          :p145_tel_id := null;',
'      END;',
'    END IF;',
'  ',
'  END IF;',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>15309776526280664207
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15340007168739844318)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cliente_edit_1'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT p.per_id,',
'         p.per_correo_electronico,',
'         p.per_primer_nombre,',
'         p.per_segundo_nombre,',
'         p.per_primer_apellido,',
'         p.per_segundo_apellido,',
'         p.per_razon_social,',
'         d.dir_descripcion,',
'         t.tel_numero,',
'         d.dir_id,',
'         t.tel_id,',
'         c.cli_id,',
'         p.PER_TIPO_PERSONA    ',
'    INTO :p145_per_id,',
'         :p145_per_correo_electronico,',
'         :p145_per_primer_nombre,',
'         :p145_per_segundo_nombre,',
'         :p145_per_primer_apellido,',
'         :p145_per_segundo_apellido,',
'         :p145_per_nombre,',
'         :p145_direccion,',
'         :p145_telefono,',
'         :p145_dir_id,',
'         :p145_tel_id,',
'         :p145_cli_id,',
'          :P145_TIPO_PERSONA',
'    FROM asdm_personas    p,',
'         asdm_telefonos   t,',
'         asdm_direcciones d,',
'         asdm_clientes    c',
'   WHERE p.per_nro_identificacion = :p145_per_nro_identificacion',
'     AND p.emp_id = :f_emp_id',
'     AND p.per_tipo_identificacion = :p145_per_tipo_identificacion',
'     AND t.per_id(+) = p.per_id',
'     AND t.tte_id(+) = :p145_tte_id',
'     AND t.tel_estado_registro(+) = :f_estado_reg_activo',
'     AND d.per_id = p.per_id',
'     AND d.dir_estado_registro = :f_estado_reg_activo',
'     AND d.tdi_id(+) = :p145_tdi_id -- SE AGREGA (+) DL',
'     AND c.per_id(+) = p.per_id',
'     AND c.emp_id(+) = p.emp_id;',
'  IF NVL(:P145_TIPO_PERSONA,''N'') = ''N'' THEN -- DLOPEZ solo si es persona natural toma nombres, caso contrario mantiene la razon social',
'      IF :p145_per_primer_apellido IS NOT NULL THEN',
'        :p145_per_nombre := :p145_per_primer_nombre || '' '' ||',
'                            :p145_per_segundo_nombre || '' '' ||',
'                            :p145_per_primer_apellido || '' '' ||',
'                            :p145_per_segundo_apellido;',
'       :P145_TIPO_PERSONA := ''N''   ;                         ',
'      END IF;',
'    END IF;',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    --raise_application_error(-20001,''ERR'');',
'    IF :p145_per_primer_apellido IS NOT NULL THEN',
'      :p145_per_id                 := NULL;',
'      :p145_per_correo_electronico := NULL;',
'      :p145_per_primer_nombre      := NULL;',
'      :p145_per_segundo_nombre     := NULL;',
'      :p145_per_primer_apellido    := NULL;',
'      :p145_per_segundo_apellido   := NULL;',
'      :p145_per_nombre             := NULL;',
'      :p145_direccion              := NULL;',
'      :p145_telefono               := NULL;',
'      :p145_per_nombre             := NULL;',
'      :p145_dir_id                 := NULL;',
'      :p145_tel_id                 := NULL;',
'      :p145_cli_id                 := NULL;',
'      :P145_TIPO_PERSONA           := NULL;',
'    END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''buscar_cliente'' and :P145_PER_NRO_IDENTIFICACION IS NOT NULL '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>15307754017470079392
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15340005640455844317)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_busca_cliente_databook'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_pda_id NUMBER;',
'BEGIN',
'  asdm_p.pr_guarda_info_adicional(pn_pda_id => ln_pda_id,',
'                                  pv_cedula => :p145_per_nro_identificacion,',
'                                  pc_error  => :p0_error);',
'  SELECT REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 1), ''?'', ''?'') primer_apellido,',
'         REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 2), ''?'', ''?'') segundo_apellido,',
'         REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 3), ''?'', ''?'') primer_nombre,',
'         REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 4), ''?'', ''?'') || '' '' ||',
'         TRIM(REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 5), ''?'', ''?'') || '' '' ||',
'              REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 6), ''?'', ''?'') || '' '' ||',
'              REPLACE(regexp_substr(par_nombres, ''[^ ]+'', 1, 7), ''?'', ''?'')) segundo_nombre,',
'         d.par_email1,',
'         d.par_medio1',
'    INTO :p145_per_primer_apellido,',
'         :p145_per_segundo_apellido,',
'         :p145_per_primer_nombre,',
'         :p145_per_segundo_nombre,',
'         :p145_per_correo_electronico,',
'         :p145_telefono',
'    FROM asdm_e.asdm_partners d',
'   WHERE d.nro_ide = :p145_per_nro_identificacion',
'     AND rownum = 1;',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    :p145_per_id                 := NULL;',
'    :p145_per_correo_electronico := NULL;',
'    :p145_per_primer_nombre      := NULL;',
'    :p145_per_segundo_nombre     := NULL;',
'    :p145_per_primer_apellido    := NULL;',
'    :p145_per_segundo_apellido   := NULL;',
'    :p145_per_nombre             := NULL;',
'    :p145_direccion              := NULL;',
'    :p145_telefono               := NULL;',
'    :p145_per_nombre             := NULL;',
'    :p145_dir_id                 := NULL;',
'    :p145_tel_id                 := NULL;',
'    :p145_cli_id                 := NULL;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''buscar_cliente'' and :P145_PER_NRO_IDENTIFICACION IS NOT NULL AND :P145_PER_TIPO_IDENTIFICACION = ''CED'' AND :P145_CLI_ID IS NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>15307752489186079391
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15340006030422844317)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_ins_upd_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_ubi_id       NUMBER;',
'  ln_tipo_id      NUMBER;',
'  lv_ruc          VARCHAR2(3) := ''RUC'';',
'  lv_razon_social VARCHAR2(300);',
'BEGIN',
'  SELECT decode(:p145_per_tipo_identificacion, ''CED'', 1, ''RUC'', 2, ''PAS'', 3)',
'    INTO ln_tipo_id',
'    FROM dual;',
'',
' IF pq_car_validaciones.fn_valida_identificacion(:p145_per_nro_identificacion,',
'                                                  ln_tipo_id) = 0 THEN',
'    :p0_error := ''!!!Datos no guardados, identificaci&oacute;n incorrecta'';',
'    --RETURN 1;',
'  END IF;',
'  ',
'  IF :p145_telefono IS NULL THEN',
'    IF :p0_error IS NULL THEN',
'      :p0_error := ''Datos no guardados, Debe registrar un Tel&eacute;fono'';',
'    ELSE',
'      :p0_error := :p0_error || ''',
'                            Debe registrar un Tel&eacute;fono'';',
'    END IF;',
'  END IF;',
'   ',
'  IF :p145_direccion IS NULL THEN',
'    IF :p0_error IS NULL THEN',
'      :p0_error := ''Datos no guardados, Debe registrar una Direcci&oacute;n'';',
'    ELSE',
'      :p0_error := :p0_error || ''',
'                            Debe registrar una Direcci&oacute;n'';',
'    END IF;',
'  END IF;',
'',
'',
'  IF :p145_per_tipo_identificacion = ''RUC'' THEN',
'      IF :P145_TIPO_PERSONA = ''J'' AND :P145_PER_NOMBRE IS NULL THEN /*:p145_nombre*/',
'            IF :p0_error IS NULL THEN',
'              :p0_error := ''Datos no guardados, Debe registrar una Raz&oacute;n Social!!!'';',
'            ELSE',
'              :p0_error := :p0_error || ''',
'                            Debe registrar una Raz&oacute;n Social!!!'';',
'            END IF;',
'        ',
'      ELSIF (:P145_TIPO_PERSONA = ''N'' AND (:p145_per_primer_apellido IS NULL OR',
'        :p145_per_primer_nombre IS NULL)) THEN',
'       :p0_error := :p0_error || ''',
'                            Debe registrar por lo menos el primer nombre y primer apellido!!!'';',
'       END if; ',
'  ELSIF :p145_per_tipo_identificacion <> ''RUC'' AND',
'        (:p145_per_primer_apellido IS NULL OR',
'        :p145_per_primer_nombre IS NULL) THEN',
'    IF :p0_error IS NULL THEN',
'      :p0_error := ''Datos no guardados, Debe registrar por lo menos Nombre 1 y Apellido 1!!!'';',
'    ELSE',
'      :p0_error := :p0_error || ''',
'                            Debe registrar por lo menos Nombre 1 y Apellido 1!!!'';',
'    END IF;',
'  END IF;',
'    ',
'',
'  IF :p0_error IS NULL THEN',
'    IF :p145_per_tipo_identificacion = lv_ruc THEN',
'      IF :P145_TIPO_PERSONA = ''J'' THEN',
'      lv_razon_social            := :P145_PER_NOMBRE;--:p145_nombre;',
'      :p145_per_primer_nombre    := NULL;',
'      :p145_per_segundo_nombre   := NULL;',
'      :p145_per_primer_apellido  := NULL;',
'      :p145_per_segundo_apellido := NULL;',
'      END IF;',
'    ELSE',
'      lv_razon_social := NULL;',
'    END IF;',
'    ',
'',
'    IF :p145_per_id IS NULL THEN',
'      pq_ven_ordenes_contado_dml.pr_ins_asdm_personas(pn_per_id                  => :p145_per_id,',
'                                              pv_per_tipo_identificacion => :p145_per_tipo_identificacion,',
'                                              pv_per_nro_identificacion  => :p145_per_nro_identificacion,',
'                                              pv_per_correo_electronico  => :p145_per_correo_electronico,',
'                                              pn_emp_id                  => :f_emp_id,',
'                                              pv_per_razon_social        => UPPER(lv_razon_social),',
'                                              pv_per_primer_nombre       => UPPER(:p145_per_primer_nombre),',
'                                              pv_per_segundo_nombre      => UPPER(:p145_per_segundo_nombre),',
'                                              pv_per_primer_apellido     => UPPER(:p145_per_primer_apellido),',
'                                              pv_per_segundo_apellido    => UPPER(:p145_per_segundo_apellido),',
'                                              pv_per_tipo_persona        => :P145_TIPO_PERSONA,',
'                                              pc_error                   => :p0_error);',
'      IF :p0_error IS NULL THEN',
'       pq_ven_ordenes_contado_dml.pr_ins_asdm_clientes(pn_cli_id => :p145_cli_id,',
'                                                pn_emp_id => :f_emp_id,',
'                                                pn_per_id => :p145_per_id,',
'                                                pc_error  => :p0_error);',
'      ',
'        SELECT di.ubi_id',
'          INTO ln_ubi_id',
'          FROM asdm_unidades_gestion uge, asdm_direcciones di',
'         WHERE di.dir_id = uge.dir_id',
'           AND uge.uge_id = :f_uge_id',
'           AND di.dir_estado_registro = ''0'';',
'        pq_ven_ordenes_contado_dml.pr_ins_asdm_direcciones(pn_dir_id              => :p145_dir_id,',
'                                                   pn_emp_id              => :f_emp_id,',
'                                                   pn_ubi_id              => ln_ubi_id,',
'                                                   pn_per_id              => :p145_per_id,',
'                                                   pn_tdi_id              => :p145_tdi_id,',
'                                                   pv_dir_descripcion     => UPPER(:p145_direccion),',
'                                                   pv_dir_estado_registro => ''0'',',
'                                                   pc_error               => :p0_error);',
'        pq_ven_ordenes_contado_dml.pr_ins_asdm_telefonos(pn_tel_id              => :p145_tel_id,',
'                                                 pn_emp_id              => :f_emp_id,',
'                                                 pn_tte_id              => :p145_tte_id,',
'                                                 pn_per_id              => :p145_per_id,',
'                                                 pv_tel_numero          => :p145_telefono,',
'                                                 pv_tel_estado_registro => ''0'',',
'                                                 pc_error               => :p0_error);',
'      END IF;',
'    ELSE',
'    if :p145_per_id is not null then',
'          pr_audit_persona(pn_UGE_ID                 => :F_UGE_ID,',
'                         pn_USU_ID                 => :F_USER_ID,',
'                         pv_IP                     => :p145_ip||'' APP:''||:APP_ID||'' PAGE:''||:APP_PAGE_ID,',
'                         pn_PER_ID                 => :p145_per_id,',
'                         pv_PER_NRO_IDENTIFICACION => :p145_per_nro_identificacion,',
'                         pv_PER_PRIMER_NOMBRE      => :p145_per_primer_nombre,',
'                         pv_PER_SEGUNDO_NOMBRE     => :p145_per_segundo_nombre,',
'                         pv_PER_PRIMER_APELLIDO    => :p145_per_primer_apellido,',
'                         pv_PER_SEGUNDO_APELLIDO   => :p145_per_segundo_apellido);',
'   end if; ',
'     pq_ven_ordenes_contado_dml.pr_upd_asdm_personas(pn_per_id                  => :p145_per_id,',
'                                              pv_per_tipo_identificacion => :p145_per_tipo_identificacion,',
'                                              pv_per_nro_identificacion  => :p145_per_nro_identificacion,',
'                                              pv_per_correo_electronico  => :p145_per_correo_electronico,',
'                                              pv_per_razon_social        => UPPER(lv_razon_social),',
'                                              pv_per_primer_nombre       => UPPER(:p145_per_primer_nombre),',
'                                              pv_per_segundo_nombre      => UPPER(:p145_per_segundo_nombre),',
'                                              pv_per_primer_apellido     => UPPER(:p145_per_primer_apellido),',
'                                              pv_per_segundo_apellido    => UPPER(:p145_per_segundo_apellido),',
'                                              pv_per_tipo_persona        => :P145_TIPO_PERSONA, ',
'                                              pn_usu_id                  => :f_user_id,',
'                                              pc_error                   => :p0_error);',
'      IF :p0_error IS NULL THEN',
'        IF :p145_dir_id IS NOT NULL THEN',
'          pq_ven_ordenes_contado_dml.pr_upd_asdm_direcciones(pn_dir_id          => :p145_dir_id,',
'                                                     pv_dir_descripcion => UPPER(:p145_direccion),',
'                                                     pc_error           => :pc_error);',
'        ELSE',
'          SELECT di.ubi_id',
'            INTO ln_ubi_id',
'            FROM asdm_unidades_gestion uge, asdm_direcciones di',
'           WHERE di.dir_id = uge.dir_id',
'             AND uge.uge_id = :f_uge_id',
'             AND di.dir_estado_registro = ''0'';',
'          pq_ven_ordenes_contado_dml.pr_ins_asdm_direcciones(pn_dir_id              => :p145_dir_id,',
'                                                     pn_emp_id              => :f_emp_id,',
'                                                     pn_ubi_id              => ln_ubi_id,',
'                                                     pn_per_id              => :p145_per_id,',
'                                                     pn_tdi_id              => :p145_tdi_id,',
'                                                     pv_dir_descripcion     => UPPER(:p145_direccion),',
'                                                     pv_dir_estado_registro => ''0'',',
'                                                     pc_error               => :p0_error);',
'        END IF;',
'        IF :p145_tel_id IS NOT NULL THEN',
'         pq_ven_ordenes_contado_dml.pr_upd_asdm_telefonos(pn_tel_id     => :p145_tel_id,',
'                                                   pv_tel_numero => :p145_telefono,',
'                                                   pc_error      => :p0_error);',
'        ELSE',
'         pq_ven_ordenes_contado_dml.pr_ins_asdm_telefonos(pn_tel_id              => :p145_tel_id,',
'                                                   pn_emp_id              => :f_emp_id,',
'                                                   pn_tte_id              => :p145_tte_id,',
'                                                   pn_per_id              => :p145_per_id,',
'                                                   pv_tel_numero          => :p145_telefono,',
'                                                   pv_tel_estado_registro => ''0'',',
'                                                   pc_error               => :p0_error);',
'        END IF;',
'        ',
'       --DLOPEZ SI EXISTEN LOS DATOS DE PERSONA PERO NO DE CLIETE SE DEBE INGRESAR EN LA TABLA CLIENTES',
'        IF :p145_cli_id IS NULL THEN',
'                  pq_ven_ordenes_contado_dml.pr_ins_asdm_clientes(pn_cli_id => :p145_cli_id,',
'                                                        pn_emp_id => :f_emp_id,',
'                                                        pn_per_id => :p145_per_id,',
'                                                        pc_error  => :p0_error);',
'',
'        END IF;',
'',
'        ',
'      END IF;',
'    END IF;',
'  END IF;',
'',
'                      ',
'/*',
'PRUEBAS_DL(NULL,''ln_tipo_id ''||ln_tipo_id||'' p145_per_tipo_identificacion ''||:p145_per_tipo_identificacion||:p0_error,''ING RAP 8'') ;',
'IF :P0_ERROR IS NOT NULL THEN',
' raise_application_error(-20000,',
'                              :p0_error || SQLERRM);',
'',
'END IF;',
'*/',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    :p0_error := ''Error Grabando Cliente. ''|| sqlerrm;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'grabar_cambios, grabar,grabar_cambios_15,grabar_15'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>15307752879153079391
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7634390632888566458)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(15339998452367844310)
,p_internal_uid=>7602137481618801532
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15340006374853844318)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_CLOSE_WINDOW'
,p_process_name=>'Close Dialog'
,p_attribute_01=>'P145_PER_NRO_IDENTIFICACION,P0_ERROR,P15_IDENTIFICACION'
,p_attribute_02=>'N'
,p_process_error_message=>'No Procesado'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'grabar_cambios, grabar'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_process_success_message=>'Procesado correctamente'
,p_internal_uid=>15307753223584079392
);
wwv_flow_imp.component_end;
end;
/
