prompt --application/pages/page_00103
begin
--   Manifest
--     PAGE: 00103
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>103
,p_name=>'CRONOGRAMA REFINANCIAMIENTO'
,p_alias=>'CRONOGRAMA_REFINANCIAMIENTO'
,p_step_title=>'CRONOGRAMA REFINANCIAMIENTO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'11'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp.component_end;
end;
/
