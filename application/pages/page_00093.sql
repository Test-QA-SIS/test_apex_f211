prompt --application/pages/page_00093
begin
--   Manifest
--     PAGE: 00093
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>93
,p_name=>unistr('Reimpresi\00F3n Documentos')
,p_step_title=>unistr('Reimpresi\00F3n Documentos')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00FAmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
' ',
'',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(148809262922748963)
,p_plug_name=>unistr('REIMPRESI\00D3N <B>&P93_TTR_DESCRIPCION.   -    &P93_PERIODO.')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(148811283348749733)
,p_name=>'Factura'
,p_parent_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_display_sequence=>20
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.COM_ID,',
'       a.UGE_NUM_SRI Establecimiento,',
'       a.PUE_NUM_SRI Pto_Emision,',
'       a.COM_NUMERO Numero,',
'       a.COM_FECHA,',
'       a.CLI_ID,',
'       a.nombre_completo,',
'       a.age_id_agencia,',
'       a.age_nombre_comercial,',
'       a.com_tipo,',
'       (SELECT b.vco_valor_variable',
'          FROM ven_var_comprobantes b',
'         WHERE b.com_id = a.com_id',
'           AND b.emp_id = a.emp_id',
'           AND b.var_id =',
'               pq_constantes.fn_retorna_constante(a.emp_id,',
'                                                  ''cn_var_id_total'')) total_factura,',
'       CASE',
'         WHEN to_char(a.COM_FECHA, ''DD-MM-YYYY'') >=',
'              to_char((SYSDATE), ''DD-MM-YYYY'') THEN',
'          ''IMPRIMIR''',
'         ELSE',
'          CASE a.emp_id',
'            WHEN 8 THEN',
'             ''IMPRIMIR''',
'            WHEN 9 THEN',
'             ''IMPRIMIR''',
'            ELSE',
'             NULL',
'          END',
'       END IMPRIMIR',
'',
'  FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO = to_number(:P93_COM_NUMERO)',
' --  AND a.ttr_id = to_number(:P93_ttr)',
'   AND a.pue_num_sri = :P93_PUE_NUM_SRI',
'   AND a.uge_num_sri = :P93_UGE_NUM_EST_SRI',
'   AND a.emp_id = :f_emp_id',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(148811458499749734)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(110660368929067068)
,p_query_column_id=>2
,p_column_alias=>'ESTABLECIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'EST..'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(110660457209067165)
,p_query_column_id=>3
,p_column_alias=>'PTO_EMISION'
,p_column_display_sequence=>4
,p_column_heading=>'PTO.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(148811573929749905)
,p_query_column_id=>4
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>5
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(148811683598749905)
,p_query_column_id=>5
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>1
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
,p_column_css_style=>'font-size:16px; color: green; font-weight: bold'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(148811765559749906)
,p_query_column_id=>6
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>6
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(148811871625749906)
,p_query_column_id=>7
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>7
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(110635277840662802)
,p_query_column_id=>8
,p_column_alias=>'AGE_ID_AGENCIA'
,p_column_display_sequence=>8
,p_column_heading=>'AGE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(148811970353749906)
,p_query_column_id=>9
,p_column_alias=>'AGE_NOMBRE_COMERCIAL'
,p_column_display_sequence=>9
,p_column_heading=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(150448175206433396)
,p_query_column_id=>10
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>10
,p_column_heading=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(110635377155662865)
,p_query_column_id=>11
,p_column_alias=>'TOTAL_FACTURA'
,p_column_display_sequence=>11
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(9743845064999452894)
,p_query_column_id=>12
,p_column_alias=>'IMPRIMIR'
,p_column_display_sequence=>12
,p_column_heading=>'Imprimir'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:93:&SESSION.:REIMPRIMIR:&DEBUG.::P93_AGE_ID,P93_COM_NUMERO_ANT,P93_COM_ID:#AGE_ID_AGENCIA#,#NUMERO#,#COM_ID#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155319955421361448)
,p_name=>'GUIA DE REMISION'
,p_parent_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT gre.gre_id,',
'gre.uge_num_est_sri,',
'gre.pue_num_sri,',
'gre.gre_num,',
'gre.gre_sec_id,',
'gre.gre_fecha,',
'gre.nombre_completo,',
'gre.bodega_nombre,',
'gre.gre_motivo_traslado,',
'''IMPRIMIR'' IMPRIMIR',
'FROM v_inv_guias_remision   gre',
'WHERE gre.gre_num=:P93_COM_NUMERO',
'AND gre.pue_num_sri=:P93_PUE_NUM_SRI',
'AND gre.uge_num_est_sri=:P93_UGE_NUM_EST_SRI'))
,p_display_when_condition=>'P93_COM_TIPO'
,p_display_when_cond2=>'GR'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320283185361568)
,p_query_column_id=>1
,p_column_alias=>'GRE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'GRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320376415361581)
,p_query_column_id=>2
,p_column_alias=>'UGE_NUM_EST_SRI'
,p_column_display_sequence=>3
,p_column_heading=>' EST.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320478794361581)
,p_query_column_id=>3
,p_column_alias=>'PUE_NUM_SRI'
,p_column_display_sequence=>4
,p_column_heading=>'PTO.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320560378361581)
,p_query_column_id=>4
,p_column_alias=>'GRE_NUM'
,p_column_display_sequence=>5
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320668768361581)
,p_query_column_id=>5
,p_column_alias=>'GRE_SEC_ID'
,p_column_display_sequence=>6
,p_column_heading=>'GRE_SEC_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320764440361581)
,p_query_column_id=>6
,p_column_alias=>'GRE_FECHA'
,p_column_display_sequence=>1
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320855609361581)
,p_query_column_id=>7
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>7
,p_column_heading=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155320968033361581)
,p_query_column_id=>8
,p_column_alias=>'BODEGA_NOMBRE'
,p_column_display_sequence=>8
,p_column_heading=>'BODEGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155331482049416498)
,p_query_column_id=>9
,p_column_alias=>'GRE_MOTIVO_TRASLADO'
,p_column_display_sequence=>9
,p_column_heading=>'MOTIVO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155321078503361581)
,p_query_column_id=>10
,p_column_alias=>'IMPRIMIR'
,p_column_display_sequence=>10
,p_column_heading=>'IMPRIMIR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:93:&SESSION.:REIMPRIMIR:&DEBUG.::P93_COM_NUMERO_ANT,P93_COM_ID:#GRE_NUM#,#GRE_ID#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(1136468676742492776)
,p_name=>'Nota de Credito'
,p_parent_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.COM_ID,',
'       a.UGE_NUM_SRI Establecimiento,',
'       a.PUE_NUM_SRI Pto_Emision,',
'       a.COM_NUMERO Numero,',
'       a.COM_FECHA,',
'       a.CLI_ID,',
'       a.nombre_completo,',
'       a.age_id_agencia,',
'       a.age_nombre_comercial,',
'       a.com_tipo,',
'       (SELECT b.vco_valor_variable',
'          FROM ven_var_comprobantes b',
'         WHERE b.com_id = a.com_id',
'           AND b.emp_id = a.emp_id',
'           AND b.var_id =',
'               pq_constantes.fn_retorna_constante(a.emp_id,',
'                                                  ''cn_var_id_total'')) total_factura,',
'       CASE',
'         WHEN to_char(a.COM_FECHA, ''DD-MM-YYYY'') =',
'              to_char(SYSDATE, ''DD-MM-YYYY'') THEN',
'         ',
'          ''IMPRIMIR''',
'         ELSE',
'          CASE a.emp_id',
'            WHEN 8 THEN',
'             ''IMPRIMIR''',
'            ELSE',
'             NULL',
'          END',
'       END IMPRIMIR',
'  FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO = to_number(:P93_COM_NUMERO)',
'',
'      --   AND a.ttr_id =    to_number(:P93_ttr)',
'   AND a.pue_num_sri = :P93_PUE_NUM_SRI',
'  AND a.uge_num_sri = :P93_UGE_NUM_EST_SRI',
'   AND a.emp_id = :f_emp_id',
'  AND a.ttr_id NOT IN (285) --acalle.  excluyo la NC por pago puntual y garantia real',
''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136470355839492896)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136470576096492901)
,p_query_column_id=>2
,p_column_alias=>'ESTABLECIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'ESTABLECIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136470669862492902)
,p_query_column_id=>3
,p_column_alias=>'PTO_EMISION'
,p_column_display_sequence=>3
,p_column_heading=>'PTO_EMISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136470777426492902)
,p_query_column_id=>4
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>4
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136470876311492902)
,p_query_column_id=>5
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>5
,p_column_heading=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136470966107492902)
,p_query_column_id=>6
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>6
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136471074396492902)
,p_query_column_id=>7
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>7
,p_column_heading=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136471159925492902)
,p_query_column_id=>8
,p_column_alias=>'AGE_ID_AGENCIA'
,p_column_display_sequence=>8
,p_column_heading=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136471277312492902)
,p_query_column_id=>9
,p_column_alias=>'AGE_NOMBRE_COMERCIAL'
,p_column_display_sequence=>9
,p_column_heading=>'AGE_NOMBRE_COMERCIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136471374807492902)
,p_query_column_id=>10
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>10
,p_column_heading=>'COM_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136471482820492902)
,p_query_column_id=>11
,p_column_alias=>'TOTAL_FACTURA'
,p_column_display_sequence=>11
,p_column_heading=>'TOTAL_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1136471581099492902)
,p_query_column_id=>12
,p_column_alias=>'IMPRIMIR'
,p_column_display_sequence=>12
,p_column_heading=>'IMPRIMIR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:93:&SESSION.:REIMPRIMIR:&DEBUG.::P93_AGE_ID,P93_COM_NUMERO_ANT,P93_COM_ID:#AGE_ID_AGENCIA#,#NUMERO#,#COM_ID#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(148812076147749906)
,p_plug_name=>'NUEVA SECUENCIA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT  count(*)',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO=:P93_COM_NUMERO',
' AND a.AGE_ID_AGENCIA=:F_AGE_ID_AGENCIA',
'AND a.com_tipo=:P93_COM_TIPO)> 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(148812277066749906)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_button_name=>'REIMPRIMIR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Reimprimir'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(148809477130749286)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(9350921368237472902)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Cargar'
,p_button_position=>'TOP'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(148814568664750296)
,p_branch_action=>'f?p=&APP_ID.:93:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 27-SEP-2011 08:44 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(64671567308991799)
,p_name=>'P93_FOL_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'Folio'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  a.uge_num_est_sri|| ''-'' ||a.pue_num_sri ,',
'       fol_nro_folio',
'FROM   v_asdm_folios a',
'WHERE  a.emp_id = :f_emp_id',
'       AND a.usuario_id = :f_user_id',
'       AND a.uge_id = :f_uge_id',
'       AND',
'       a.tgr_id =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_guia_remision'')',
'       and a.fol_fecha_caducidad>=sysdate',
'       and a.fol_estado_registro=pq_constantes.fn_retorna_constante(null,''cv_estado_reg_activo'')',
'       order by a.fol_nro_folio'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P93_COM_TIPO=''GR'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(86559177404206858)
,p_name=>'P93_MSG'
,p_item_sequence=>71
,p_item_plug_id=>wwv_flow_imp.id(148811283348749733)
,p_prompt=>'IMPORTANTE:'
,p_source=>unistr('Se permite reimprimir facturas \00FAnicamente del d\00EDa, para facturas anteriores generar Nota de Cr\00E9dito')
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" style="color: red" '
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P93_COM_TIPO = ''F'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(107169867733910905)
,p_name=>'P93_TTR'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'TTR'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(107170072587910905)
,p_name=>'P93_TGR'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(107878878526828369)
,p_name=>'P93_COM_NUM'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>' -  '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p93_com_tipo IS NOT NULL AND (:P93_COM_TIPO!=''GR'' OR :P93_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148809657606749724)
,p_name=>'P93_COM_NUMERO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'DIGITE LA SECUENCIA QUE DESEA REIMPRIMIR'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit()";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P93_COM_NUM = :P93_NUEVA_SECUENCIA'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148809863209749730)
,p_name=>'P93_NUEVA_SECUENCIA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>' - '
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p93_com_tipo IS NOT NULL AND (:P93_COM_TIPO!=''GR'' OR :P93_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148810066958749731)
,p_name=>'P93_PUE_NUM_SRI'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p93_com_tipo IS NOT NULL AND (:P93_COM_TIPO!=''GR'' OR :P93_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148810272658749731)
,p_name=>'P93_UGE_NUM_EST_SRI'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'DIGITE LA NUEVA SECUENCIA A IMPRIMIR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p93_com_tipo IS NOT NULL AND (:P93_COM_TIPO!=''GR'' OR :P93_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148810455824749731)
,p_name=>'P93_COM_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148810680978749731)
,p_name=>'P93_COM_NUMERO_ANT'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'com_numero'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148810875253749732)
,p_name=>'P93_TRX_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_use_cache_before_default=>'NO'
,p_prompt=>'trx_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148811066904749732)
,p_name=>'P93_COM_TIPO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'Tipo Documento'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS'
,p_lov=>'.'||wwv_flow_imp.id(76673757103664354)||'.'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--Seleccione--'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''LIMPIA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148812461440749906)
,p_name=>'P93_DATFOLIO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_prompt=>'DATFOLIO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148812671224749907)
,p_name=>'P93_PUE_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_prompt=>'PUE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148812871091749938)
,p_name=>'P93_TTR_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_prompt=>'TTR_ID'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148813082382749953)
,p_name=>'P93_PERIODO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_prompt=>'PERIODO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148813259930749954)
,p_name=>'P93_ERROR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_prompt=>'ERROR'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148813467899749954)
,p_name=>'P93_TTR_DESCRIPCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(148812076147749906)
,p_prompt=>'TTR_DESCRIPCION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(148836256689833754)
,p_name=>'P93_AGE_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'AGE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(150391863048127156)
,p_name=>'P93_NRO_FOLIO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_prompt=>'nro folio'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155181562566253785)
,p_name=>'P93_PCA_ID'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(148809262922748963)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pca Id'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(107881356409850341)
,p_validation_name=>'P93_COM_NUM'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p93_com_num = :P93_nueva_secuencia then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>'P93_COM_NUM'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_associated_item=>wwv_flow_imp.id(107878878526828369)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(148814075475750287)
,p_process_sequence=>100
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'carga_datos_folio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   ln_tipo_ug NUMBER(10);',
'BEGIN',
'/*pruebas_pl(333222,''EMP_ID: ''||:f_emp_id||',
'                  '' / PCA_ID: ''||:F_PCA_ID||',
'                  '' / TTR_ID: ''||:P93_TTR_DESCRIPCION||',
'                  '' / PER_ID: ''||:P93_PERIODO||',
'                  '' / FOL_ID: ''||:P93_DATFOLIO||',
'                  '' / NSECUE: ''||:P93_NUEVA_SECUENCIA||',
'                  '' / PUE_ID_SRI: ''||:P93_PUE_NUM_SRI||',
'                  '' / UGE_ID_SRI: ''||:P93_UGE_NUM_EST_SRI||',
'                  '' / PUE_ID: ''||:P93_PUE_ID||',
'                  '' / NRO_FOL: ''||:P93_NRO_FOLIO||',
'                  '' / COM_TIPO: ''||:P93_COM_TIPO||',
'                  '' / UGE_ID: ''||:F_UGE_ID||',
'                  '' / TTR_ID: ''||:P93_TTR||',
'                  '' / TGR_ID: ''||:P93_TGR||',
'                  '' / USU_ID: ''||:F_USER_ID||',
'                  '' / FOL_NRO_FOL: ''||:P93_FOL_ID);*/',
'',
'IF :F_PCA_ID IS NULL THEN',
'   SELECT    uge.tug_id',
'   INTO      ln_tipo_ug',
'   FROM      asdm_unidades_gestion uge',
'   WHERE     uge.uge_id = :F_uge_id',
'   AND       uge.emp_id = :F_emp_id;',
'  ',
'   IF ln_tipo_ug = pq_constantes.fn_retorna_constante(null, ''cn_tug_id_agencia'') THEN',
'       pq_ven_movimientos_caja.pr_abrir_caja(:F_USER_ID,',
'                                             :f_uge_id, ',
'                                             :f_emp_id,',
'                                             :p0_pue_id,',
'                                             :F_PCA_ID,',
'                                             :p0_error);',
'   END IF;',
'END IF;',
'',
'-----pq_ven_listas_caja.pr_datos_folio_tgr_id',
'PQ_CON_CADUCIDAD_FOLIOS.pr_datos_folio_tgr_id(pn_emp_id          => :F_EMP_ID,',
'                      pn_pca_id          => :F_PCA_ID,',
'                      pv_ttr_descripcion => :P93_TTR_DESCRIPCION, ',
'                      pv_periodo         => :P93_PERIODO,',
'                      pv_datfolio        => :P93_DATFOLIO,',
'                      pn_nueva_secuencia => :P93_NUEVA_SECUENCIA,',
'                      pv_pue_num_sri     => :P93_PUE_NUM_SRI,',
'                      pv_uge_num_est_sri => :P93_UGE_NUM_EST_SRI,',
'                      pn_pue_id          => :P93_PUE_ID,',
'                      pn_nro_folio       => :p93_nro_folio,',
'                      pv_com_tipo        => :P93_COM_TIPO,',
'                      pn_uge_Id          => :f_uge_Id,',
'                      pn_ttr_id          => :p93_ttr,',
'                      pn_tgr_id          => :p93_tgr,',
'                      pn_usu_id          => :f_user_id,',
'                      pv_error           => :p0_error,',
'                      pn_fol_nro_folio   => :p93_fol_id);',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p93_com_tipo IS NOT NULL AND (:P93_COM_TIPO!=''GR'' OR :P93_FOL_ID IS NOT NULL)'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>116560924205985361
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(148813868361750259)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REIMPRIMIR_FAC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'if :P93_COM_TIPO <>  pq_constantes.fn_retorna_constante(NULL,''cv_com_tipo_guia_remision'') then',
'',
'',
'if :P93_COM_NUMERO_ANT is not null AND :P93_COM_ID IS NOT NULL then',
'  pq_ven_comprobantes.pr_reimprimir_mayoreo(pn_emp_id         => :F_EMP_ID,',
'                                            pn_com_id         => :P93_COM_ID,',
'                                            pn_com_numero     => :P93_NUEVA_SECUENCIA,',
'                                            pn_com_numero_ant => :P93_COM_NUMERO_ANT,',
'                                            pn_age_id         => :P93_AGE_ID,',
'                                            pn_tse_id         => :f_seg_id,',
'                                            pn_uge_id         => :f_uge_id,',
'                                            pn_uge_id_gasto   => :f_uge_id_gasto,',
'                                            pn_usu_id         => :f_user_id,',
'                                            pn_pue_id         => :P93_PUE_ID,',
'                                            pn_trx_id         => :P93_TRX_ID,',
'                                            pv_pue_num_sri     => :P93_PUE_NUM_SRI, ',
'                                            pv_uge_num_est_sri => :P93_UGE_NUM_EST_SRI,',
'                                            pv_com_tipo       => :p93_com_tipo,',
'                                            pn_tgr_id         => :p93_tgr,',
'                                            pn_ttr_id         => :p93_ttr,',
'                                            pv_error          => :P0_ERROR);',
'end if;',
'',
'else',
'',
'if :P93_COM_NUMERO_ANT is not null AND :P93_COM_ID IS NOT NULL then',
'',
'  pq_inv_guias_remision.pr_reimprimir_guias_remision (pn_emp_id         => :F_EMP_ID,',
'                                            pn_gre_id        => :P93_COM_ID,',
'                                            pn_gre_numero    => :P93_NUEVA_SECUENCIA,',
'                                            pn_gre_numero_ant => :P93_COM_NUMERO_ANT,',
'                                            pn_uge_id         => :f_uge_id,',
'                                            pn_uge_id_gasto   => :f_uge_id_gasto,',
'                                            pn_usu_id         => :f_user_id,',
'                                            pn_pue_id         => :P93_PUE_ID,',
'                                            pn_trx_id         => :P93_TRX_ID,',
'                                            pv_pue_num_sri     => :P93_PUE_NUM_SRI, ',
'                                            pv_uge_num_est_sri => :P93_UGE_NUM_EST_SRI,',
'                                            pv_com_tipo       => :p93_com_tipo,',
'                                            pn_tgr_id         => :p93_tgr,',
'                                            pn_ttr_id         => :p93_ttr,',
'                                            pv_error          => :P0_ERROR);',
'',
'end if;',
'',
'end if;',
'',
'',
'IF :P0_ERROR IS NULL THEN',
':P93_COM_NUMERO_ANT := null;',
':P93_NUEVA_SECENCIA := null;',
':p93_COM_NUMERO := NULL;',
':P93_COM_NUM := NULL;',
':P93_COM_TIPO := NULL;',
':P93_COM_ID := NULL;',
':P93_NRO_FOLIO := NULL;',
':P93_AGE_ID := NULL;',
':P93_TTR := NULL;',
':P93_TGR := NULL;',
':P93_TRX_ID := NULL;',
'END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(148812277066749906)
,p_process_when=>'REIMPRIMIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>116560717091985333
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(110667352662138058)
,p_process_sequence=>110
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P93_COM_NUMERO_ANT := null;',
':P93_NUEVA_SECENCIA := null;',
':p93_COM_NUMERO := NULL;',
':P93_COM_NUM := NULL;',
':P93_NRO_FOLIO := NULL;',
':P93_AGE_ID := NULL;',
':P93_TTR := NULL;',
':P93_TGR := NULL;',
':P93_TRX_ID := NULL;',
':P93_COM_ID := NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'LIMPIA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>78414201392373132
);
wwv_flow_imp.component_end;
end;
/
