prompt --application/pages/page_00069
begin
--   Manifest
--     PAGE: 00069
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>69
,p_name=>'Notas de credito por retiro pendientes'
,p_step_title=>'Notas de credito por retiro'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(126315451817389733)
,p_plug_name=>'N.C. por retiro pendientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (c.com_id = h.com_id AND h.estado IS NULL) THEN --es migrada',
'          ''Recalcular''',
'         ELSE',
'          null',
'       END recalcular,',
'       ',
'       CASE',
'         WHEN (h.com_id IS NULL) OR (c.com_id = h.com_id AND h.estado = ''R'')',
'         ',
'          THEN',
'          ''Generar NC''',
'         ELSE',
'          NULL',
'       END generar,',
'       h.com_id hcom,',
'       c.com_id ccom,',
'       i.cin_id,',
'       i.com_id,',
'       c.COM_FECHA,',
'       ',
'       i.cli_id,',
'       nvl(p.per_razon_social,',
'           p.per_primer_nombre || '' '' || p.per_primer_apellido || '' '' ||',
'           p.per_segundo_apellido) nombre_completo,',
'       p.per_nro_identificacion identificacion,',
'       c.UGE_NUM_SRI || '' - '' || c.PUE_NUM_SRI || '' - '' || c.COM_NUMERO factura,',
'       pq_lv_kdda.fn_obtiene_valor_lov(291, c.com_tipo) tipo,',
'       c.pol_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(55, c.pol_id) politica,',
'       ',
'       c.com_plazo     plazo,',
'       c.ord_id,',
'       c.age_id_agente,',
'       --    c.agente vendedor,',
'       pq_ven_retorna_sec_id.fn_ord_sec_id(c.ord_id) orden,',
'       pq_ven_retorna_sec_id.fn_cin_sec_id(i.cin_id) cominv',
'',
'  from inv_comprobantes_inventario  i,',
'       ven_comprobantes             c,',
'       asdm_p.asdm_homologacion_com h,',
'       inv_ordenes_devolucion       o,',
'       asdm_clientes                cl,',
'       asdm_personas                p',
'',
' where i.ttr_id =',
'       asdm_p.pq_constantes.fn_retorna_constante(NULL,',
'                                                 ''cn_ttr_id_ing_retiro_def'')',
'   and i.com_id = c.com_id',
'   and i.emp_id = c.emp_id',
'   and cl.cli_id = c.cli_id',
'   and p.per_id = cl.per_id',
'   AND c.com_id = h.com_id(+)',
'   and i.emp_id = :f_emp_id',
'   AND c.tse_id = :f_seg_id',
'   and o.com_id = c.com_id',
'   and o.cin_id = i.cin_id',
'   and o.uge_id = :f_uge_id',
' and i.cin_estado_registro = 0',
'  AND o.ode_estado_registro=0',
'   AND i.com_id NOT  IN ( select com_id_ref',
'          from ven_comprobantes nc,',
'          asdm_transacciones tr',
'          WHERE nc.trx_id=tr.trx_id',
'          AND tr.ttr_id= asdm_p.pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_nota_credito_retiro'')',
'           and nc.emp_id = nc.emp_id AND nc.cin_id=i.cin_id)',
'   ORDER BY c.COM_FECHA DESC   '))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (c.com_id = h.com_id AND h.estado IS NULL) THEN --es migrada',
'          ''Recalcular''',
'         ELSE',
'         null',
'       END recalcular,',
'       ',
'       CASE',
'         WHEN (h.com_id IS NULL) OR (c.com_id = h.com_id AND h.estado = ''R'')',
'         ',
'          THEN',
'          ''Generar NC''',
'         ELSE',
'          NULL',
'       END generar,',
'       h.com_id hcom,',
'       c.com_id ccom,',
'       i.cin_id,',
'       i.com_id,',
'       c.COM_FECHA,',
'       ',
'       i.cli_id,',
'       c.nombre_completo,',
'       c.per_nro_identificacion identificacion,',
'       c.UGE_NUM_SRI||'' - ''||c.PUE_NUM_SRI||'' - ''||c.COM_NUMERO factura,',
'       pq_lv_kdda.fn_obtiene_valor_lov(291,',
'                                       c.com_tipo) tipo,',
'       c.pol_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(55,',
'                                       c.pol_id) politica,',
'                                             ',
'       c.com_plazo plazo,',
'       c.ord_id,',
'       c.age_id_agente,',
'       c.agente vendedor,',
'       pq_ven_retorna_sec_id.fn_ord_sec_id(c.ord_id) orden,',
'       pq_ven_retorna_sec_id.fn_cin_sec_id(i.cin_id) cominv',
'       ',
'  ',
'   from inv_comprobantes_inventario i,',
'       asdm_transacciones          t,',
'       asdm_p.v_ven_comprobantes          c,',
'       asdm_p.asdm_homologacion_com  h,',
'       inv_ordenes_devolucion       o',
'',
'       ',
'       ',
' where i.trx_id = t.trx_id',
'   and t.ttr_id =',
'       asdm_p.pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_retiro_def'')',
'   and i.com_id = c.com_id',
'   and i.emp_id = c.emp_id',
'   and i.emp_id = t.emp_id',
'   AND c.com_id=h.com_id(+)',
'   and i.emp_id =:f_emp_id',
'   AND c.tse_id =:f_seg_id',
'   and o.com_id = c.com_id',
'   and o.cin_id = i.cin_id',
'   and o.uge_id = :f_uge_id',
' and i.cin_estado_registro = 0',
'  AND o.ode_estado_registro=0',
'   AND i.com_id NOT  IN ( select com_id_ref',
'          from ven_comprobantes nc,',
'          asdm_transacciones tr',
'          WHERE nc.trx_id=tr.trx_id',
'          AND tr.ttr_id= asdm_p.pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_nota_credito_retiro'')',
'           and nc.emp_id = nc.emp_id AND nc.cin_id=i.cin_id)',
'   ORDER BY c.COM_FECHA DESC   '))
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(126315571331389733)
,p_name=>'N.C. por retiro pendientes'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No existen Notas de Credito'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:78:&SESSION.:carga_det:&DEBUG.::P78_CLI_ID,P78_COM_ID_FACTURA,P78_COM_NUMERO,P78_POL_ID,P78_PER_NRO_IDENTIFICACION,P78_CIN_ID,P78_AGE_ID_AGENTE,P78_ORD_ID,P78_PLAZO: #CLI_ID#,#COM_ID#,#FACTURA#,#POL_ID#,#IDENTIFICACION#,#CIN_ID#,#AGE_ID_'
||'AGENTE#,#ORD_ID#,#PLAZO#'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'FPENAFIEL'
,p_internal_uid=>94062420061624807
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126315769593389781)
,p_db_column_name=>'CIN_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Cin Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CIN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126315880330389834)
,p_db_column_name=>'COM_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fac Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126315979386389835)
,p_db_column_name=>'CLI_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316061333389835)
,p_db_column_name=>'NOMBRE_COMPLETO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316153850389835)
,p_db_column_name=>'IDENTIFICACION'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316374526389835)
,p_db_column_name=>'TIPO'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316473204389835)
,p_db_column_name=>'POL_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316572443389836)
,p_db_column_name=>'POLITICA'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Politica'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POLITICA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316661398389836)
,p_db_column_name=>'PLAZO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316769054389836)
,p_db_column_name=>'ORD_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126316877676389836)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126317072875389836)
,p_db_column_name=>'ORDEN'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'# Orden'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126317174180389837)
,p_db_column_name=>'COMINV'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'# Comp. Inv'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COMINV'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(141140754680035734)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Fecha<br>Factura'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(141143858761131509)
,p_db_column_name=>'FACTURA'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Factura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63167769743650257)
,p_db_column_name=>'RECALCULAR'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Recalcular'
,p_column_link=>'f?p=242:42:&SESSION.::&DEBUG.:42:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_PARAMETRO,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_POPUP,P42_COM_ID:&F_LOGOUT_URL.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,'
||'&F_PARAMETRO.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_POPUP.,#COM_ID#'
,p_column_linktext=>'#RECALCULAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RECALCULAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63167876667650259)
,p_db_column_name=>'GENERAR'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Generar'
,p_column_link=>'f?p=&APP_ID.:78:&SESSION.:carga_det:&DEBUG.::P78_CLI_ID,P78_COM_ID_FACTURA,P78_COM_NUMERO,P78_POL_ID,P78_PER_NRO_IDENTIFICACION,P78_CIN_ID,P78_AGE_ID_AGENTE,P78_ORD_ID,P78_PLAZO: #CLI_ID#,#COM_ID#,#FACTURA#,#POL_ID#,#IDENTIFICACION#,#CIN_ID#,#AGE_ID_'
||'AGENTE#,#ORD_ID#,#PLAZO#'
,p_column_linktext=>'#GENERAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'GENERAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(67411251693628761)
,p_db_column_name=>'HCOM'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Hcom'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'HCOM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(67411382756628761)
,p_db_column_name=>'CCOM'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Ccom'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CCOM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(126317383938390075)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'940643'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'RECALCULAR:GENERAR:FACTURA:COM_FECHA:ORDEN:COMINV:CLI_ID:IDENTIFICACION:NOMBRE_COMPLETO:POLITICA:PLAZO:TIPO:COM_ID:CIN_ID'
);
wwv_flow_imp.component_end;
end;
/
