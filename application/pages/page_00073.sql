prompt --application/pages/page_00073
begin
--   Manifest
--     PAGE: 00073
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>73
,p_name=>'REPORTE DE NOTAS CREDITO POR REBAJA'
,p_step_title=>'REPORTE DE NOTAS CREDITO POR REBAJA'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111089566125689715)
,p_plug_name=>'Parametros '
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111146980774484763)
,p_plug_name=>'NOTAS DE CREDITO POR REBAJA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select C.*, ''ANULAR'' ANULAR',
'  from v_ven_comprobantes c',
' where c.com_tipo = :P73_TIPO_NOTA_CREDITO',
'   and c.trx_id in (select t.trx_id',
'                      from asdm_transacciones t',
'                     where t.ttr_id in (285,279,PQ_CONSTANTES.fn_retorna_constante(0,''cn_ttr_id_nota_credito_rebaja''),',
'                       PQ_CONSTANTES.fn_retorna_constante(0 ,''cn_ttr_id_nota_credito_servicios''))',
'                       and t.emp_id = c.emp_id)',
'   and (TRUNC(c.com_fecha) BETWEEN :P73_FECHA_DESDE AND :P73_FECHA_HASTA or',
'       c.COM_ID = :P73_COM_ID)',
'   --and EXTRACT(MONTH FROM sysdate) = EXTRACT(MONTH FROM c.com_fecha)',
'   AND C.COM_ESTADO_REGISTRO = :P73_ESTADO_REGISTRO_ACTIVO',
'   and EXTRACT(year FROM sysdate) = EXTRACT(year FROM c.com_fecha)',
'   --and PQ_CON_BALANCE_GENERAL.FN_VALIDA_FECHA_ACTIVA(trunc(c.COM_FECHA), c.EMP_ID) = ''S''',
' order by c.com_id;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(111147067927484763)
,p_name=>'NOTAS DE CREDITO POR REBAJA'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>78893916657719837
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147278417484800)
,p_db_column_name=>'COM_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147369001484824)
,p_db_column_name=>'EMP_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147472027484824)
,p_db_column_name=>'TRX_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Trx Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147558658484863)
,p_db_column_name=>'PUE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147677480484863)
,p_db_column_name=>'POL_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147760098484863)
,p_db_column_name=>'CLI_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147862603484863)
,p_db_column_name=>'DIR_ID_ENVIAR_FACTURA'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Dir Id Enviar Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIR_ID_ENVIAR_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111147976271484864)
,p_db_column_name=>'COM_TIPO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Com Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148063803484864)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Com Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148275100484864)
,p_db_column_name=>'UGE_NUM_SRI'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Uge Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148368976484864)
,p_db_column_name=>'PUE_NUM_SRI'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Pue Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148472924484865)
,p_db_column_name=>'COM_ESTADO_REGISTRO'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Com Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148571375484865)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148651880484865)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Age Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148774475484865)
,p_db_column_name=>'ORD_ID'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148870440484865)
,p_db_column_name=>'COM_OBSERVACION'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Com Observacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111148958299484865)
,p_db_column_name=>'COM_SALDO'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Com Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111149078482484865)
,p_db_column_name=>'COM_PLAZO'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Com Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111149153796484866)
,p_db_column_name=>'EDE_ID'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111149252062484866)
,p_db_column_name=>'COM_NRO_IMPRESION'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Com Nro Impresion'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NRO_IMPRESION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111149366758484866)
,p_db_column_name=>'CIN_ID'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Cin Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CIN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111149461066484866)
,p_db_column_name=>'COM_ID_REF'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Com Id Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_REF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111151683149498805)
,p_db_column_name=>'USU_ID'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Usu Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'USU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111151783350498807)
,p_db_column_name=>'TRX_FECHA_EJECUCION'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Trx Fecha Ejecucion'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_FECHA_EJECUCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111151879861498808)
,p_db_column_name=>'TTR_ID'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Ttr Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111151969827498808)
,p_db_column_name=>'TTR_DESCRIPCION'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Ttr Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152073654498808)
,p_db_column_name=>'POL_DESCRIPCION_LARGA'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Pol Descripcion Larga'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POL_DESCRIPCION_LARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152159152498808)
,p_db_column_name=>'PUE_NOMBRE'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Pue Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152283778498808)
,p_db_column_name=>'UGE_ID_FACTURA'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Uge Id Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152352344498808)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152481016498808)
,p_db_column_name=>'AGE_NOMBRE_COMERCIAL'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Age Nombre Comercial'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_NOMBRE_COMERCIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152560582498809)
,p_db_column_name=>'NOMBRE_COMPLETO'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Nombre Completo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152659412498809)
,p_db_column_name=>'PER_TIPO_IDENTIFICACION'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Per Tipo Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_TIPO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152754969498809)
,p_db_column_name=>'PER_NRO_IDENTIFICACION'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'Per Nro Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152864396498809)
,p_db_column_name=>'AGENTE'
,p_display_order=>36
,p_column_identifier=>'AJ'
,p_column_label=>'Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111152958529498809)
,p_db_column_name=>'TSE_ID'
,p_display_order=>37
,p_column_identifier=>'AK'
,p_column_label=>'Tse Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111272571166980651)
,p_db_column_name=>'ANULAR'
,p_display_order=>38
,p_column_identifier=>'AL'
,p_column_label=>'Anular'
,p_column_link=>'f?p=&APP_ID.:73:&SESSION.:ANULAR:&DEBUG.::P73_COM_ID_NC,P73_CLIENTE,P73_VALOR,P73_NRO_IDENTIFICACION:#COM_ID#,#NOMBRE_COMPLETO#,#COM_SALDO#,#PER_NRO_IDENTIFICACION#'
,p_column_linktext=>'#ANULAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ANULAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(131273763836823957)
,p_db_column_name=>'PER_ID'
,p_display_order=>39
,p_column_identifier=>'AM'
,p_column_label=>'Per Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PER_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111491352336982454)
,p_db_column_name=>'FOL_NRO_FOLIO'
,p_display_order=>40
,p_column_identifier=>'AN'
,p_column_label=>'Fol Nro Folio'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'FOL_NRO_FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55386655717887151)
,p_db_column_name=>'COM_NUMERO'
,p_display_order=>41
,p_column_identifier=>'AO'
,p_column_label=>'Com Numero'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(20583872082460263866)
,p_db_column_name=>'COM_SEC_ID'
,p_display_order=>42
,p_column_identifier=>'AP'
,p_column_label=>'Com Sec Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(111150274089487847)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'788972'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'COM_ID:TRX_ID:POL_DESCRIPCION_LARGA:CLI_ID:PER_NRO_IDENTIFICACION:NOMBRE_COMPLETO:PUE_NOMBRE:COM_TIPO:COM_FECHA:COM_NUMERO:UGE_NUM_SRI:PUE_NUM_SRI:AGENTE:AGE_NOMBRE_COMERCIAL:COM_OBSERVACION:COM_SALDO:COM_NRO_IMPRESION:COM_ID_REF:ANULAR:COM_SEC_ID'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111266852849928069)
,p_plug_name=>unistr('ANULACION DE NOTA DE CR\00BF\00BFDITO')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_plug_display_when_condition=>'ANULAR'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(111149977477487666)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(111269971596961794)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_button_name=>'ANULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Anular'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(118918663172649896)
,p_branch_action=>'f?p=&APP_ID.:73:&SESSION.:anular:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(111269971596961794)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 07-DEC-2011 16:38 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111132953311386081)
,p_name=>'P73_FECHA_DESDE'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_prompt=>'Fecha Desde'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P73_FILTRO=''FE'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111133259544387869)
,p_name=>'P73_FECHA_HASTA'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_prompt=>'Fecha Hasta'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P73_FILTRO=''FE'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111135358421406470)
,p_name=>'P73_ESTADO_REGISTRO_ACTIVO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_prompt=>'Estado Registro Activo'
,p_source=>'PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL, ''cv_estado_reg_activo'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111135782316413415)
,p_name=>'P73_TIPO_TRANSACCION'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_prompt=>'Tipo Transaccion'
,p_source=>'PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,''cn_ttr_id_nota_credito_rebaja'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111144753451471316)
,p_name=>'P73_TIPO_NOTA_CREDITO'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_prompt=>'Tipo Nota Credito'
,p_source=>'PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL, ''cv_com_tipo_nc'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111268680123954831)
,p_name=>'P73_COM_ID_NC'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_prompt=>'Com Id Nc'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111270858568967541)
,p_name=>'P73_CLIENTE'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111271969649970784)
,p_name=>'P73_VALOR'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_prompt=>'Valor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(112115476868395763)
,p_name=>'P73_COM_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_prompt=>'NOTA DE CREDITO'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_VEN_NOTAS_CREDITO_X_REBAJA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(2000);',
'BEGIN',
'  lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_NOTAS_CREDITO_X_REBAJA'');',
'  RETURN(lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P73_FILTRO=''NC'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(112118574622432984)
,p_name=>'P73_FILTRO'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(111089566125689715)
,p_item_default=>'NC'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC2:POR NOTA DE CREDITO;NC,POR FECHA;FE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(126461260336468246)
,p_name=>'P73_CLAVE'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_prompt=>unistr('Clave Autorizaci\00BF\00BFn')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(126464283931503441)
,p_name=>'P73_OBSERVACION'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131271379982819171)
,p_name=>'P73_NRO_IDENTIFICACION'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(111266852849928069)
,p_prompt=>unistr('Nro Identificaci\00BF\00BFn')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(112150374041640974)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CAMBIAR_FILTRO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P73_FILTRO =''NC'' THEN ',
'  :P73_FECHA_DESDE:=NULL;',
'  :P73_FECHA_HASTA:=NULL;',
'ELSE',
':P73_COM_ID:=NULL;',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(111149977477487666)
,p_internal_uid=>79897222771876048
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(111274572945009544)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ANULAR_NC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P73_OBSERVACION IS NOT NULL THEN',
'',
'PQ_VEN_NCREDITO_REBAJA.PR_ANULAR_NCREDITO(:P73_COM_ID_NC,',
'                                          :F_UGE_ID,',
'                                          :F_USER_ID,',
'                                          :P73_CLAVE,',
'                                          :P73_OBSERVACION,',
'                                          :P0_ERROR);',
'ELSE',
':P0_ERROR:=''LLENAR EL CAMPO OBSERVACION'';',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'anular'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>79021421675244618
);
wwv_flow_imp.component_end;
end;
/
