prompt --application/pages/page_00134
begin
--   Manifest
--     PAGE: 00134
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>134
,p_name=>'PagoFactura'
,p_step_title=>'PagoFactura'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>',
'',
'<script type="text/javascript" >',
'',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00C3\00BAmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'',
'function valida_numEnterTab(evt,itemid,ln_valor){',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'  if (evt.keyCode == 13) {',
'     ',
'        $x(itemid).focus();',
'        return false;',
'           ',
'   }else if(evt.keyCode == 8){',
'       return true;',
'   }else if(evt.keyCode == 9){',
'       return true;',
'   }else if(evt.keyCode == null){',
'       return true;  ',
'   }else{',
'       if (!patron_numero.test((ln_valor).value)) {',
unistr('        alert(''Ingrese solo n\00FAmeros'');'),
'        ln_valor.value = '''';',
'        html_GetElement((ln_valor).id).focus();',
'        return true;',
'        } ',
'    }',
' return false;',
'}',
'',
'function valida_numero_dec(ln_valor)',
'{',
'var patron_numero =/^^\d{1,10}(\.\d{1,2})?$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00C3\00BAmeros con dos Decimales'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function convertir_mayu(ln_valor)',
'{',
'var res_valida_letras;',
'ln_valor.value = ln_valor.value.toUpperCase();',
'res_valida_letras=valida_letras(ln_valor);',
'if (res_valida_letras==0){',
'    window.alert(''Se permite solo letras'');',
'    ln_valor.value = '''';',
'    html_GetElement(ln_valor.focus());',
'}',
'}',
'',
'</script>',
'',
''))
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771502782026202435)
,p_name=>'pruebas jaime andres'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>550
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'from apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(0,',
'                                                                               ''cv_col_gastos_gto_cobr'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504081837202447)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504180626202447)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504283513202447)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504381849202447)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504455027202447)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504565953202447)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504667279202447)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504765333202447)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504862285202447)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771504962878202447)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505070230202447)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505183831202447)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505279091202447)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505378607202447)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505463689202447)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505568553202447)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505660777202447)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505770808202447)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505878280202447)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771505961893202447)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506070528202447)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506165159202447)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506270831202447)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506379641202447)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506465162202447)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506559970202447)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506673208202447)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506771066202448)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506883165202448)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771506981390202448)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507064979202448)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507153644202448)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507255690202448)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507380993202448)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507476804202448)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507556880202448)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507683295202448)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507772010202449)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507864764202449)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771507983087202449)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508071451202449)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508164927202449)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508262523202449)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508380006202449)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508459120202449)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508567656202449)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508653221202449)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508767603202449)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508867903202449)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771508971434202449)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509068003202449)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509153470202449)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509252367202449)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509358293202449)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509451389202449)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771502962220202439)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503074132202439)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503176977202447)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503278037202447)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503356344202447)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503471741202447)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503564090202447)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503662932202447)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503760670202447)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503882273202447)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771503962584202447)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771509575787202454)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509782321202457)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:134:&SESSION.::&DEBUG.::P134_SEQ_ID,P134_TFP_ID,P134_EDE_ID,P134_PLAN,P134_MCD_VALOR_MOVIMIENTO,P134_CRE_TITULAR_CUENTA,P134_CRE_NRO_CUENTA,P134_CRE_NRO_CHEQUE,P134_CRE_NRO_PIN,P134_TJ_NUMERO,P134_MCD_NRO_AUT_REF,P134_TJ_NUMERO_VOUCHER,P'
||'134_LOTE,P134_MCD_NRO_COMPROBANTE,P134_RAP_NRO_RETENCION,P134_SALDO_RETENCION,P134_RAP_COM_ID,P134_RAP_FECHA,P134_RAP_FECHA_VALIDEZ,P134_RAP_NRO_AUTORIZACION,P134_MODIFICACION,P134_RAP_NRO_ESTABL_RET,P134_RAP_NRO_PEMISION_RET,P134_PRE_ID:#COL03#,#COL'
||'04#,#COL05#,#COL05#,#COL30#,#COL11#,#COL12#,#COL13#,#COL14#,#COL15#,#COL16#,#COL17#,#COL18#,#COL19#,#COL20#,#COL21#,#COL22#,#COL23#,#COL24#,#COL25#,S,#COL26#,#COL27#,#COL28#'
,p_column_linktext=>'Editar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509865909202457)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:134:&SESSION.:eliminar:&DEBUG.::P134_SEQ_ID:#COL03#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771509973791202457)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510079308202457)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510167335202457)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510281318202457)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510355792202457)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510471413202457)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510561324202457)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510651870202457)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510768082202457)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510872271202458)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771510966003202458)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511084024202458)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511174304202458)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511283124202458)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511383507202458)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511454590202458)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511567847202458)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511670992202458)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511755638202458)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_cattributes_element=>'style="color:RED;"'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511854526202458)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771511968089202458)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512074334202458)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512175555202458)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512268138202459)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512360879202459)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512474015202459)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512555790202459)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P134_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512666293202459)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771512758400202459)
,p_name=>'Dividendos a pagar'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001   cli_id,',
'       c002   com_id,',
'       c003   com_numero,',
'       c004   nro_ven,',
'       c005   div_id,',
'       c006   emp_id,',
'       c007   cxc_id,',
'       c008   ttr_id,',
'       c009   fecha_ven,',
'       /*       d.div_saldo_interes_mora,',
'       d.div_int_mora_condonado,',
'       d.div_gasto_cobr_generado,',
'       d.div_gasto_cob_condonado,*/',
'       c015 saldo_cuota,',
'       c016 valor_pagar',
'',
'  FROM apex_collections, car_dividendos d',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'   AND c005 = d.div_id',
''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(771512758400202459)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771512951679202461)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513070252202461)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Cli Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513176642202461)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Com Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513274395202461)
,p_query_column_id=>4
,p_column_alias=>'COM_NUMERO'
,p_column_display_sequence=>4
,p_column_heading=>'Com Numero'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513363648202461)
,p_query_column_id=>5
,p_column_alias=>'NRO_VEN'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Ven'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513454034202461)
,p_query_column_id=>6
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513574558202461)
,p_query_column_id=>7
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513682022202461)
,p_query_column_id=>8
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513758699202461)
,p_query_column_id=>9
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>9
,p_column_heading=>'Ttr Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513861119202461)
,p_query_column_id=>10
,p_column_alias=>'FECHA_VEN'
,p_column_display_sequence=>10
,p_column_heading=>'Fecha Ven'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771513979128202461)
,p_query_column_id=>11
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>11
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771514083132202461)
,p_query_column_id=>12
,p_column_alias=>'VALOR_PAGAR'
,p_column_display_sequence=>12
,p_column_heading=>'Valor Pagar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771514183091202462)
,p_plug_name=>'MENSAJE CUOTAS GRATIS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>590
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'pq_asdm_promociones.fn_promo_cuotas_gratis is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771514559697202466)
,p_plug_name=>'PROMOCIONES_APLICABLES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.POR_ID,',
'A.POR_ID POR_ID_DISPLAY,',
'A.ORD_ID,',
'A.PTA_ID,',
'A.PRO_ID,',
'A.EMP_ID,',
'A.POR_ESTADO_REGISTROS,',
'A.PER_ID,',
'A.POR_NO_ID_BENEF,',
'A.PTA_NOMBRE,',
'A.PTA_SELECCIONADO,',
'B.PRO_DESCRIPCION',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p134_ord_id'))
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'',
'select 1 a from dual;'))
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771514760434202467)
,p_name=>'Promociones Aplicables'
,p_parent_plug_id=>wwv_flow_imp.id(771514559697202466)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="font-size:12;font-family:Arial Narrow"'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_TABFORM'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.POR_ID,',
'A.POR_ID POR_ID_DISPLAY,',
'A.ORD_ID,',
'A.PTA_ID,',
'A.PRO_ID,',
'A.EMP_ID,',
'A.POR_ESTADO_REGISTROS,',
'A.PER_ID,',
'A.POR_NO_ID_BENEF,',
'A.PTA_NOMBRE,',
'A.PTA_SELECCIONADO,',
'B.PRO_DESCRIPCION,',
'A.ITE_SKU_ID,',
'C.ITE_DESCRIPCION_LARGA,',
'A.POR_PUNTOS_ACUMULAR',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B,',
'     ASDM_E.INV_ITEMS C',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p134_ord_id',
'AND c.ite_sku_id(+) = a.ite_sku_id'))
,p_display_when_condition=>'NVL(:P134_TERMINO_VENTA,11) <> 11'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
,p_comment=>'--'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771514959537202468)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515070730202468)
,p_query_column_id=>2
,p_column_alias=>'POR_ID'
,p_column_display_sequence=>9
,p_column_heading=>'Por Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515176051202468)
,p_query_column_id=>3
,p_column_alias=>'POR_ID_DISPLAY'
,p_column_display_sequence=>10
,p_column_heading=>'Por Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_ID_DISPLAY'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515260223202468)
,p_query_column_id=>4
,p_column_alias=>'ORD_ID'
,p_column_display_sequence=>15
,p_column_heading=>'Orden Venta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'ORD_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515383901202468)
,p_query_column_id=>5
,p_column_alias=>'PTA_ID'
,p_column_display_sequence=>11
,p_column_heading=>'Pta Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PTA_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515472212202468)
,p_query_column_id=>6
,p_column_alias=>'PRO_ID'
,p_column_display_sequence=>12
,p_column_heading=>'Pro Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PRO_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515572880202468)
,p_query_column_id=>7
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>13
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'EMP_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515651751202468)
,p_query_column_id=>8
,p_column_alias=>'POR_ESTADO_REGISTROS'
,p_column_display_sequence=>14
,p_column_heading=>'Por Estado Registros'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_ESTADO_REGISTROS'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515781035202468)
,p_query_column_id=>9
,p_column_alias=>'PER_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Referidor'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'POPUPKEY'
,p_named_lov=>wwv_flow_imp.id(6241387265836580872)
,p_lov_show_nulls=>'YES'
,p_column_width=>65
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PER_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515864656202468)
,p_query_column_id=>10
,p_column_alias=>'POR_NO_ID_BENEF'
,p_column_display_sequence=>7
,p_column_heading=>'ID.Beneficiario'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'POR_NO_ID_BENEF'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771515954255202468)
,p_query_column_id=>11
,p_column_alias=>'PTA_NOMBRE'
,p_column_display_sequence=>3
,p_column_heading=>unistr('Opciones de Aplicaci\00C3\00B3n')
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PTA_NOMBRE'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771516080103202468)
,p_query_column_id=>12
,p_column_alias=>'PTA_SELECCIONADO'
,p_column_display_sequence=>8
,p_column_heading=>'Aplica?'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'RADIOGROUP_FROM_LOV'
,p_named_lov=>wwv_flow_imp.id(50210174501447921)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'NO'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_attribute_01=>'2'
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_PROMOCIONES_ORDENES'
,p_ref_column_name=>'PTA_SELECCIONADO'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771516152995202469)
,p_query_column_id=>13
,p_column_alias=>'PRO_DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Promocion'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771516254419202469)
,p_query_column_id=>14
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>16
,p_column_heading=>'Ite Sku Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771516368634202469)
,p_query_column_id=>15
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>4
,p_column_heading=>'Ite Descripcion Larga'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771516463809202469)
,p_query_column_id=>16
,p_column_alias=>'POR_PUNTOS_ACUMULAR'
,p_column_display_sequence=>5
,p_column_heading=>'Por Puntos Acumular'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771520878583202485)
,p_plug_name=>'Pagar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>530
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771521254898202485)
,p_plug_name=>'Parametros Identificadores'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>540
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_when_condition=>'TO_NUMBER(:P134_NRO_REG_ITEM_IDEN) > 0'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771522451934202486)
,p_name=>'sql unpivot coleccion identificadores'
,p_parent_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>130
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'   return ',
'pq_inv_movimientos_iden.fn_sql_col_inv_ids_unpivot(pn_emp_id => :F_EMP_ID, ',
'                                                            pv_error => :P0_ERROR);',
'END;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771522665367202487)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771522763148202487)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771522865520202487)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771522981891202487)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523071446202487)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523171817202487)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523266956202487)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523355920202488)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523467456202488)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523574704202488)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523683616202488)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523774636202488)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523866583202488)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771523952291202488)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524054896202488)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524153014202488)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524273422202488)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524365371202489)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524475278202489)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524581601202489)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524662874202489)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524756718202489)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524851484202489)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771524966812202489)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525079190202489)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525155776202489)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525275205202489)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525372737202493)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525460893202493)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525576717202493)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525676794202493)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525773186202493)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525862306202493)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771525965205202493)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526061418202493)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526167313202493)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526251475202493)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526368968202493)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526482884202493)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526553999202493)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526653486202493)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526770004202493)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526876977202493)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771526964664202493)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527081123202493)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527179501202493)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527253004202493)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527377356202493)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527464811202493)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527564380202493)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527677284202493)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527777344202493)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527875752202493)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771527977163202493)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528055732202493)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528154278202493)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528271166202493)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528359759202493)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528460001202493)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528570074202493)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771528678129202494)
,p_name=>'COL_COMPROBANTE_INV'
,p_parent_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT * ',
'  FROM apex_collections c ',
' WHERE collection_name = ''COL_INV_IDENTIFICADORES1'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530766974202494)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530879341202494)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530982789202494)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531073090202494)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531152340202494)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531266150202494)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531373160202495)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531470560202495)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531576646202495)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531652302202495)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531755405202495)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531873215202495)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771531961363202495)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532066504202495)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532161384202497)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532280741202497)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532377248202497)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532457082202497)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532581936202497)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532660757202497)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532757185202497)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532883910202497)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771532966858202497)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533072673202497)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533171783202497)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533258568202497)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533365237202497)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533464205202497)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533564875202497)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533655696202498)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533766838202498)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533876774202498)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771533959610202498)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534072225202498)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534171910202498)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534272353202498)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534359970202498)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534460842202498)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534581179202498)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534652470202498)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534768572202498)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534864643202498)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771534974644202498)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771535075135202501)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771535168185202501)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771535266923202501)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771535364169202501)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528857461202494)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771528956526202494)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529077695202494)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529155475202494)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529251611202494)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529366616202494)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529479808202494)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529560200202494)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529683034202494)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529781467202494)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529876668202494)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771529979092202494)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530081686202494)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530164147202494)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530270157202494)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530356509202494)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530469188202494)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530583172202494)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771530657590202494)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771535453965202502)
,p_plug_name=>'Identificadores'
,p_parent_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>150
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'TO_NUMBER(:P134_NRO_REG_ITEM_IDEN) > 0'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771536057984202503)
,p_plug_name=>'Mensaje'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>2
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_when_condition=>':P134_MENSAJE IS NOT NULL;'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771536471871202505)
,p_plug_name=>'<SPAN STYLE="font-size: 10pt">PAGO DE CLIENTE - &P134_CLI_ID.  - &P134_CLI_NOMBRE.</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771546269133202514)
,p_plug_name=>'tfp_Cheques'
,p_parent_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>140
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771547468015202524)
,p_plug_name=>'IngresoValor'
,p_parent_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>200
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771549182485202526)
,p_plug_name=>'tfp_deposito_transferencia'
,p_parent_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>160
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771549772896202527)
,p_plug_name=>'tfp Datos Tarjeta Credito'
,p_parent_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>150
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P134_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771552355638202534)
,p_plug_name=>'tfp_retencion'
,p_parent_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771556951965202537)
,p_name=>'Retenciones'
,p_parent_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>1210
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'seq_id id,',
'to_number(c001) com_id,',
'c002 fecha_ret,',
'c003 fecha_validez,',
'c004 nro_retencion,',
'c005 nro_autorizacion,',
'c006 nro_establecimiento,',
'c007 nro_pto_emision,',
'c008 porcentaje,',
'to_number(c009) base,',
'to_number(c013) valor_calculado,',
'apex_item.text(p_idx        => 25,',
'                      p_value      => to_number(c010),',
'                      p_attributes => ''unreadonly id="f47_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c013||'',''||seq_id||'',this.value)"'',',
'                      p_size       => ''10'') valor_retencion,',
'''X'' eliminar',
'',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION''',
'UNION',
'select ',
'NULL id,',
'NULL com_id,',
'NULL fecha_ret,',
'NULL fecha_validez,',
'NULL nro_retencion,',
'NULL nro_autorizacion,',
'NULL nro_establecimiento,',
'NULL nro_pto_emision,',
'NULL porcentaje,',
'SUM(to_number(c009)) base,',
'sum(to_number(c013)) valor_calculado,',
'TO_CHAR(SUM(to_number(c010))) valor_retencion,',
'NULL eliminar',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557183179202537)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>12
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557255120202537)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ord Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557369399202537)
,p_query_column_id=>3
,p_column_alias=>'FECHA_RET'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Ret'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557470626202538)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VALIDEZ'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Validez'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557580625202538)
,p_query_column_id=>5
,p_column_alias=>'NRO_RETENCION'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Retencion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557656570202538)
,p_query_column_id=>6
,p_column_alias=>'NRO_AUTORIZACION'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Autorizacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557777523202538)
,p_query_column_id=>7
,p_column_alias=>'NRO_ESTABLECIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Nro Establecimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557863782202538)
,p_query_column_id=>8
,p_column_alias=>'NRO_PTO_EMISION'
,p_column_display_sequence=>7
,p_column_heading=>'Nro Pto Emision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771557969450202538)
,p_query_column_id=>9
,p_column_alias=>'PORCENTAJE'
,p_column_display_sequence=>8
,p_column_heading=>'Porcentaje'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771558074444202538)
,p_query_column_id=>10
,p_column_alias=>'BASE'
,p_column_display_sequence=>9
,p_column_heading=>'Base'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771558181015202538)
,p_query_column_id=>11
,p_column_alias=>'VALOR_CALCULADO'
,p_column_display_sequence=>10
,p_column_heading=>'Valor Calculado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771558258603202538)
,p_query_column_id=>12
,p_column_alias=>'VALOR_RETENCION'
,p_column_display_sequence=>11
,p_column_heading=>'Valor Retencion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771558378170202538)
,p_query_column_id=>13
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>13
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771558661331202538)
,p_plug_name=>'Datos Guardar Factura desde Orden'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771561273193202540)
,p_plug_name=>'borrar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>1200
,p_plug_display_point=>'REGION_POSITION_05'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771561657214202542)
,p_plug_name=>'Documento Relacionado'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270527564230046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P134_POLITICA_VENTA IN(48,33) AND :P134_FACTURAR = ''S'' /*pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pol_id_48''),33)*/'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771563071437202543)
,p_name=>'PRUEBA_JA'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>580
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM APEX_COLLECTIONS',
'WHERE COLLECTION_NAME = ''CO_MOV_CAJA'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563278341202544)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563351851202544)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563475801202544)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563562439202544)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563677064202544)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563782913202544)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563857202202544)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771563983018202544)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564060549202544)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564158930202544)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564279044202544)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564376996202544)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564454372202544)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564581739202544)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564666868202544)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564771710202544)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564867852202544)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771564957443202544)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565064313202544)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565153469202544)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565256413202544)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565379437202544)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565479802202544)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565568621202544)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565678314202544)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565770117202544)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565863593202544)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771565957936202544)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566079483202544)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566160596202544)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566279711202544)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566358152202544)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566467267202544)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566579334202544)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566665528202545)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566777441202545)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566874078202545)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771566978702202545)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567062705202545)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567182050202545)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567281934202545)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567382807202545)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567482881202546)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567556213202546)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567662519202546)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567768110202546)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567865844202546)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771567981077202546)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568074392202546)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568155055202546)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568276622202546)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568367063202546)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568480460202546)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568553325202546)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568652319202546)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568768444202546)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568882688202546)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771568976805202546)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569064355202546)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569160886202546)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569273334202546)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569367461202546)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569479260202546)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569577976202546)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569671995202546)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771569775289202546)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771569852595202546)
,p_plug_name=>'Consulta Puntos Acumulados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>32
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'(:P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'')) OR :P134_POLITICA_VENTA IN(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_pol_id_48''),112)'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771570458383202547)
,p_name=>'Promociones disponibles'
,p_parent_plug_id=>wwv_flow_imp.id(771569852595202546)
,p_display_sequence=>35
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    a.bip_id,',
'          a.pro_id,',
'          b.pro_descripcion,',
'          a.per_id,',
'          d.ite_sku_id_ganado || e.ite_descripcion_larga premio,--a.bip_valor_saldo,-- * b.pro_factor_conversion bip_valor_saldo,',
'          d.bpd_valor_generado bip_valor_saldo,',
'          ROUND(d.bpd_valor_generado/ d.bpd_valor_generado,2) cantidad_premios,',
'          c.pta_descripcion,',
'          c.pta_id,',
'          d.pdi_id',
'FROM      ven_bitacora_promociones a, ',
'          ven_promociones b, ',
'          ven_promos_tipos_aplica c,',
'          ven_bitacora_promos_det d,',
'          inv_items e',
'WHERE     a.per_id=NVL(:P134_PER_ID_PUNTOS,:P134_per_id)',
'AND       :P134_PER_ID_PUNTOS IS NOT NULL',
'AND       a.pro_id=b.pro_id',
'AND       c.pro_id=a.pro_id',
'AND       c.pta_id=a.pta_id',
'AND       d.bpd_valor_saldo > 0--a.bip_valor_saldo > 0',
'AND       d.pro_id = a.pro_id',
'AND       d.per_id_beneficiario = a.per_id',
'AND       d.pta_id = c.pta_id',
'AND       e.ite_sku_id(+) = d.ite_sku_id_ganado',
'AND       ((a.pta_id = 2 AND :p134_politica_venta =  pq_constantes.fn_retorna_constante(null,''cn_obsequios_puntos'')))',
'',
'UNION',
'',
'SELECT    bip_id, pro_id, pro_descripcion, per_id, premio, ',
'          SUM(bip_valor_saldo) saldo,',
'          SUM(cantidad_premios) cantidad_premios, ',
'          pta_descripcion, pta_id, pdi_id',
'FROM      ',
'',
'(SELECT    a.bip_id,',
'          a.pro_id,',
'          b.pro_descripcion,',
'          a.per_id,',
'          d.ite_sku_id_ganado || e.ite_descripcion_larga premio,--a.bip_valor_saldo,-- * b.pro_factor_conversion bip_valor_saldo,',
'          d.bpd_valor_generado bip_valor_saldo,',
'          ROUND(d.bpd_valor_generado/ d.bpd_valor_generado,2) cantidad_premios,',
'          c.pta_descripcion,',
'          c.pta_id,',
'          d.pdi_id',
'FROM      ven_bitacora_promociones a, ',
'          ven_promociones b, ',
'          ven_promos_tipos_aplica c,',
'          ven_bitacora_promos_det d,',
'          inv_items e',
'WHERE     a.per_id=NVL(:P134_PER_ID_PUNTOS,:P134_per_id)',
'AND       :P134_PER_ID_PUNTOS IS NOT NULL',
'AND       a.pro_id=b.pro_id',
'AND       c.pro_id=a.pro_id',
'AND       c.pta_id=a.pta_id',
'AND       d.bpd_valor_saldo > 0--a.bip_valor_saldo > 0',
'AND       d.pro_id = a.pro_id',
'AND       d.per_id_beneficiario = a.per_id',
'AND       d.pta_id = c.pta_id',
'AND       e.ite_sku_id(+) = d.ite_sku_id_ganado',
'AND       ((a.pta_id = 1 AND :p134_politica_venta <> pq_constantes.fn_retorna_constante(null,''cn_obsequios_puntos'')))',
')',
'GROUP BY bip_id, pro_id, pro_descripcion, per_id, premio, pta_descripcion, pta_id, pdi_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771570655078202548)
,p_query_column_id=>1
,p_column_alias=>'BIP_ID'
,p_column_display_sequence=>7
,p_column_heading=>'BIP_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771570776436202548)
,p_query_column_id=>2
,p_column_alias=>'PRO_ID'
,p_column_display_sequence=>6
,p_column_heading=>'PRO_ID'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771570881752202548)
,p_query_column_id=>3
,p_column_alias=>'PRO_DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Promocion'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_PRO_ID,P30_SALDO_MAX_APL_DEV,P30_SALDO_PROMO_DEV,P30_PTA_ID,P30_PUNTOS_CANCELAR,P30_PDI_ID:#PRO_ID#,#BIP_VALOR_SALDO#,#BIP_VALOR_SALDO#,#PTA_ID#,#BIP_VALOR_SALDO#,#PDI_ID#'
,p_column_linktext=>'#PRO_DESCRIPCION#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771570961315202548)
,p_query_column_id=>4
,p_column_alias=>'PER_ID'
,p_column_display_sequence=>5
,p_column_heading=>'PER_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771571071358202548)
,p_query_column_id=>5
,p_column_alias=>'PREMIO'
,p_column_display_sequence=>9
,p_column_heading=>'Premio'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771571153988202548)
,p_query_column_id=>6
,p_column_alias=>'BIP_VALOR_SALDO'
,p_column_display_sequence=>4
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771571256474202548)
,p_query_column_id=>7
,p_column_alias=>'CANTIDAD_PREMIOS'
,p_column_display_sequence=>8
,p_column_heading=>'Cantidad Premios'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771571368479202548)
,p_query_column_id=>8
,p_column_alias=>'PTA_DESCRIPCION'
,p_column_display_sequence=>3
,p_column_heading=>'Tipo Aplicacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771571466012202548)
,p_query_column_id=>9
,p_column_alias=>'PTA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cod.Tipo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771571576838202548)
,p_query_column_id=>10
,p_column_alias=>'PDI_ID'
,p_column_display_sequence=>10
,p_column_heading=>'Pdi Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771571858171202549)
,p_plug_name=>' PARAMETROS_PROMOCION_PUNTOS'
,p_parent_plug_id=>wwv_flow_imp.id(771569852595202546)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>570
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771573567331202555)
,p_name=>'prueba mov caja'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'      *',
'FROM   apex_collections',
'WHERE  collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771573777923202555)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771573872366202555)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771573979577202555)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574068544202555)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574152225202555)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574278855202555)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574380964202555)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574480777202555)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574564139202555)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574675672202555)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574782303202555)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574856808202555)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771574958586202555)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575055173202555)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575157709202555)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575273664202555)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575352461202555)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575466143202555)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575567237202555)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575672132202555)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575767735202555)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575869821202555)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771575966750202555)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576079159202555)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576174855202555)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576279897202556)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576355260202556)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576464483202556)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576580600202556)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576661165202556)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576779483202556)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576861272202556)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771576952154202556)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577060942202556)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577157723202556)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577282791202556)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577373696202556)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577480713202556)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577556941202556)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577671646202556)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577766827202556)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577863804202556)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771577967612202556)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578068997202556)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578160951202556)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578263606202556)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578374079202556)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578453849202556)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578582788202556)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578683410202556)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578754815202556)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578865800202556)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771578961319202556)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579078352202556)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579154948202556)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579261356202556)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579362514202556)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579469302202556)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579563780202556)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579651660202556)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579770497202556)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579872742202556)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771579969006202556)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580070033202556)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580155495202556)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580258624202557)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771580353139202557)
,p_name=>'prueba col baja inv'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>503
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                          ''cv_col_baja_inventario'');',
'',
'',
''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580569496202557)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580666235202557)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580774486202557)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580873233202557)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771580961399202557)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581067455202557)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581167093202557)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581280119202557)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581370082202557)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581467909202557)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581559064202557)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581666318202557)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581761181202557)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581868145202557)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771581983226202558)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582067582202558)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582166013202558)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582280431202558)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582353558202558)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582451643202558)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582552277202558)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582652685202558)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582761012202558)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582874886202558)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771582953894202558)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583058969202558)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583151411202558)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583264033202558)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583355138202558)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583483772202558)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583559960202558)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583665617202558)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583758145202558)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583854421202558)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771583963351202558)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584052890202558)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584158312202558)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584271449202558)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584366946202558)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584467382202558)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584561750202558)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584682218202558)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584777840202558)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584879703202558)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771584965936202558)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585061750202558)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585157007202558)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585251871202559)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585372175202559)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585477139202559)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585558381202559)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585677276202559)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585783785202559)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585854296202559)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771585958243202559)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586064776202559)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586160317202559)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586265214202559)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586376003202559)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586455425202559)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586576643202559)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586669752202559)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586756963202559)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586881285202559)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771586967379202559)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771587061200202559)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771587152617202559)
,p_name=>'prueba johanna precancelacion'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>130
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'SELECT c001 FROM apex_collections  WHERE collection_name = ''COLL_PREC_SELECCIONADOS'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771587360852202559)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771587473922202559)
,p_name=>'Identificadores'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>560
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_2'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  (SELECT ti.tid_descripcion ',
'        FROM inv_tipos_identificador ti ',
'        WHERE ti.tid_id = a.tid_id) primario,',
'        a.ebs_valor_identificador valor_primario,',
'        c.tid_descripcion secundario            ,',
'        b.ise_valor_identificador valor_secundario',
'FROM    INV_ITEMS_ESTADO_BODEGA_SERIE a,',
'        inv_iden_secundarios_ebs b ,',
'        inv_tipos_identificador c',
'WHERE   a.ebs_id     = b.ebs_id',
'        AND a.ebs_id = :p20_ebs_id',
'        AND c.tid_id = b.tid_id;'))
,p_display_when_condition=>'P20_ORD_TIPO'
,p_display_when_cond2=>'VENMO'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771587678650202560)
,p_query_column_id=>1
,p_column_alias=>'PRIMARIO'
,p_column_display_sequence=>1
,p_column_heading=>'PRIMARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771587753704202560)
,p_query_column_id=>2
,p_column_alias=>'VALOR_PRIMARIO'
,p_column_display_sequence=>2
,p_column_heading=>'VALOR_PRIMARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771587852061202560)
,p_query_column_id=>3
,p_column_alias=>'SECUNDARIO'
,p_column_display_sequence=>3
,p_column_heading=>'SECUNDARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771587965534202560)
,p_query_column_id=>4
,p_column_alias=>'VALOR_SECUNDARIO'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR_SECUNDARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771588051372202560)
,p_name=>'prueba coll pago cuota'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name=''CO_PAGO_CUOTA'''
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594062576202561)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594157257202561)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594283855202562)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594353758202562)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594468697202562)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594561846202562)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594654101202562)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771594771160202562)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588253400202560)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588361942202560)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588463714202560)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588579383202560)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588666483202560)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588766240202560)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588855943202560)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771588958436202560)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589078927202560)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589151401202560)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589255371202560)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589362976202560)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589468238202560)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589562037202560)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589677864202560)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589772866202560)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589861550202560)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771589973099202560)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590054743202560)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590168614202560)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590266659202561)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590372388202561)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590454413202561)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590565140202561)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590653555202561)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590766837202561)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590868603202561)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771590972913202561)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591083735202561)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591156603202561)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591258501202561)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591371428202561)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591478352202561)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591570150202561)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591654977202561)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591763659202561)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591876167202561)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771591974984202561)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592057907202561)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592167072202561)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592279144202561)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592353824202561)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592453063202561)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592577061202561)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592674496202561)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592760866202561)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592855440202561)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771592955031202561)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593051925202561)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593156425202561)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593271488202561)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593375250202561)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593480582202561)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593555488202561)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593652967202561)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593760129202561)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593867876202561)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771593960780202561)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771594880946202562)
,p_plug_name=>unistr('Asignaci\00C3\00B3n de Folios para :  ')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>510
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'(:P134_INT_MORA_COB>0 or :P134_GTOS_COB_COB>0 ) and (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and :P134_VALIDA_PUNTO = ''P'') or 	(:P134_ES_PRECANCELACION = ''S'' and :P134_VALIDA_PUNTO = ''P'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771595065287202563)
,p_name=>'Gastos Cobranza - Nota Debito'
,p_parent_plug_id=>wwv_flow_imp.id(771594880946202562)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>520
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{ SELECT seq_id,',
'        c001 cli_id,',
'        c003 comprob,',
'        c006 emp_id,',
'        c007 cxc_id,',
'        c010 gtoscob,',
'        /*apex_item.text(30,',
'                       nvl(c020, 0),',
'                       p_size => 8,',
'                       p_attributes => ''onChange="onSubmit(''''PR_VALIDA_FOLIOS_GASTOS'''')"'') folio,*/',
'',
'         -- Activar si se quiere que no haga submit al digitar el valor',
'        apex_item.text(30,',
'                       nvl(c020, 0),',
'                       p_size => 8,',
'                       p_attributes => ''onChange="pr_ejecuta_proc_campos_datos_2(''''PR_VALIDA_FOLIOS_GASTOS'''','' || seq_id ||',
'       '',''''GC'''', this.value ,''''P134_F_UGE_ID'''',''''P134_F_USER_ID'''',''''P134_F_EMP_ID'''',''''P134_CLI_ID'''',''''R72503601849514425'''',''''R72404504656751074'''' )"'') folio,',
'',
'c011 Establec,',
'c012 PtoEmis',
'   FROM apex_collections',
'  WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_gto_cobr'');',
'',
'',
'',
' ',
'',
'}'';'))
,p_display_when_condition=>':P134_GTOS_COB_COB > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595276633202564)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595362291202564)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595453597202564)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595563486202564)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595669364202564)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595761446202564)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Gtos.Cob.'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595854604202564)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771595959329202564)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596064815202564)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596159963202564)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596279718202565)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596370708202565)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596458296202565)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596577135202565)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596676913202565)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596766835202565)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596881272202565)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771596960369202565)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597056651202565)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597175671202565)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597258258202565)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597352136202565)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597454270202565)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597560736202565)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597671073202565)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597757985202565)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597875295202565)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771597971907202565)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598070959202565)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598177436202565)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598255404202565)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598358870202565)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598461553202565)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598555063202565)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598679781202565)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598763003202565)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598855370202565)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771598982522202565)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599074411202565)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599178059202565)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599273087202565)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599351780202565)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599466007202565)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599566054202565)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599678134202565)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599773334202565)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599859293202565)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771599974072202565)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600083125202565)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600173246202566)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600271266202566)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600353677202566)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600458458202566)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600579399202566)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600659290202567)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600782296202567)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600876246202567)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771600969534202567)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771601053286202567)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771601163968202567)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771601465005202567)
,p_name=>unistr('Inter\00C3\00A9s de Mora - Nota de D\00C3\00A9bito ')
,p_parent_plug_id=>wwv_flow_imp.id(771594880946202562)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>500
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value =>NVL(c020,0),',
'                      p_size => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos_gc(''''PR_VALIDA_FOLIOS_GASTOS'''','' || seq_id ||',
'       '',''''IM'''', this.value ,''''P134_F_UGE_ID'''',''''P134_F_USER_ID'''',''''P134_F_EMP_ID'''',''''P134_CLI_ID'''',''''R72404504656751074'''',''''R72503601849514425'''')"'') folio,',
'c011 Establec,',
'c012 PtoEmis/*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =  pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_int_mora'');',
'',
'',
'}'';'))
,p_display_when_condition=>':P134_INT_MORA_COB>0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771601681907202568)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771601781954202568)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771601851897202568)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771601952400202568)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602065600202568)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602152057202568)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Int.Mora'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602265620202568)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602373176202568)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602468854202568)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602552971202568)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602676258202568)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602775508202568)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602868020202568)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771602972125202568)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603061057202568)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603178629202568)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603256601202568)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603366207202568)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603478112202568)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603552679202568)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603671101202568)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603771804202568)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603861853202568)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771603958687202568)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604083566202568)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604166246202568)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604276579202568)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604356326202568)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604479271202568)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604573249202568)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604676738202569)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604778838202569)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604864599202569)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771604974062202569)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605074064202569)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605181876202569)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605261392202569)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605354875202569)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605455167202569)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605567359202569)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605662220202569)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605778857202569)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605858175202569)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771605983992202570)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606068392202570)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606158238202570)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606279702202570)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606382962202570)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606454043202570)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606565068202570)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606656894202570)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606782428202570)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606876470202570)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771606969632202570)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771607054591202570)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771607162252202570)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771607281121202570)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771607376239202570)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771607483149202570)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771607575374202570)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771535671169202502)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(771535453965202502)
,p_button_name=>'IDENTIFICADORES'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Identificadores'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:92:&SESSION.:identificadores:&DEBUG.::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_EMP_LOGO,F_EMP_FONDO,F_POPUP,F_APP_ID_REGRESA,F_PAG_ID_REGRESA,P92_TTR_ID,P92_LCM_ID,P92_BPR_ID_IDEN:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_EMP_LOGO.,&F_EMP_FONDO.,N,&APP_ID.,&APP_PAGE_ID.,,,&P134_BPR_ID_IDEN.'
,p_button_condition=>'TO_NUMBER(:P134_NRO_REG_ITEM_IDEN) > 0'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771516559155202469)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Insertar Registro'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771547656209202524)
,p_button_sequence=>500
,p_button_plug_id=>wwv_flow_imp.id(771547468015202524)
,p_button_name=>'CARGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:134:&SESSION.:cargar_tfp:&DEBUG.:::'
,p_button_condition=>'to_number(:P134_VALOR_TOTAL_PAGOS) > to_number(:P134_SUM_PAGOS)'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771521071718202485)
,p_button_sequence=>600
,p_button_plug_id=>wwv_flow_imp.id(771520878583202485)
,p_button_name=>'Graba_fac_pag'
,p_button_static_id=>'pagar'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'&P134_LABEL_BOTON.'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:return false;'||wwv_flow.LF||
''
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--(:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S''))',
'/*(',
'(:P134_POLITICA_VENTA <> pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') OR :P134_POLITICA_VENTA IS NULL) AND (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S'
||'''))',
') OR',
'(',
'(:P134_POLITICA_VENTA = pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') AND :P134_FACTURA_YN_R = ''S'') AND (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S''))',
')*/',
'',
'(:P134_HAY_PROMOCION = ''S'' AND :P134_FACTURA_YN = ''S'' AND ((',
'(:P134_POLITICA_VENTA <> pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') OR :P134_POLITICA_VENTA IS NULL) AND (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S'
||'''))',
') OR',
'(',
'(:P134_POLITICA_VENTA = pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') AND :P134_FACTURA_YN_R = ''S'') AND (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S''))',
'))) ',
'',
'OR',
'',
'(NVL(:P134_HAY_PROMOCION,''N'') <> ''S'' AND ((',
'(:P134_POLITICA_VENTA <> pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') OR :P134_POLITICA_VENTA IS NULL) AND (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S'
||'''))',
') OR',
'(',
'(:P134_POLITICA_VENTA = pq_constantes.fn_retorna_constante(NULL,''cn_obsequios_puntos'') AND :P134_FACTURA_YN_R = ''S'') AND (:P134_VALOR_TOTAL_PAGOS = :P134_SUM_PAGOS and ((:P134_VALOR_TOTAL_PAGOS<>0 and :P134_FACTURAR <> ''S'') or :P134_FACTURAR = ''S''))',
')))'))
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_cattributes=>'class="redirect" style="font-size:16;font-family:Tahoma;color:BLACK;font-weight:bold"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771536657947202506)
,p_button_sequence=>640
,p_button_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_button_name=>'IDEN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Iden'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771570075428202547)
,p_button_sequence=>650
,p_button_plug_id=>wwv_flow_imp.id(771569852595202546)
,p_button_name=>'BT_FILTRAR_PERSONAS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Filtrar Beneficiario'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771516783724202470)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:134:&SESSION.::&DEBUG.:::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771516970262202471)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771517171098202471)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_button_name=>'SUBMIT'
,p_button_static_id=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Grabar'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_cattributes=>'style="font-size:16;font-family:Tahoma;color:BLUE;font-weight:bold"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771536851816202506)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Pago de Cuota'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:132:&SESSION.::&DEBUG.:134::'
,p_button_condition=>':P134_FACTURAR = ''N'' and NVL(:P134_ES_PRECANCELACION,''N'') <> ''S'''
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771537062387202506)
,p_button_sequence=>610
,p_button_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Pago de Cuota con Retencion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:64:&SESSION.::&DEBUG.:134::'
,p_button_condition=>'P134_FACTURAR'
,p_button_condition2=>'R'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771537251832202507)
,p_button_sequence=>620
,p_button_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_button_name=>'REGRESAR'
,p_button_static_id=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'<< Regresar Facturacion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.:134::'
,p_button_condition=>'P134_FACTURAR'
,p_button_condition2=>'S'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_button_cattributes=>'class="redirect" style="font-size:16;font-family:Tahoma;color:balck;font-weight:bold"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771537455163202507)
,p_button_sequence=>630
,p_button_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Precancelacion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:49:&SESSION.::&DEBUG.:134::'
,p_button_condition=>'P134_ES_PRECANCELACION'
,p_button_condition2=>'S'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(337319178767074671)
,p_button_sequence=>660
,p_button_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_button_name=>'IR_A_RENEGOCIACION'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Ir A Renegociacion'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:133:&SESSION.::&DEBUG.::P133_IDENTIFICACION,P133_REN_ID:&P134_CLI_IDENTIFICACION.,&P134_REN_ID.'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(771626181207202616)
,p_branch_action=>'f?p=&APP_ID.:134:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(771626383239202617)
,p_branch_action=>'f?p=&APP_ID.:134:&SESSION.:Graba_fac_pag:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(287054276940984727)
,p_branch_sequence=>10
,p_branch_comment=>'Created 01-MAR-2010 11:11 by ONARANJO'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(771626570868202617)
,p_branch_action=>'f?p=&APP_ID.:134:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(279841965747865685)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(771626763358202617)
,p_branch_action=>'f?p=&APP_ID.:132:&SESSION.::&DEBUG.:134::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(771536851816202506)
,p_branch_sequence=>20
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 06-FEB-2011 20:05 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(771626957213202617)
,p_branch_action=>'f?p=&APP_ID.:134:&SESSION.::&DEBUG.:RP::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>50
,p_branch_condition_type=>'NEVER'
,p_branch_comment=>'Created 28-SEP-2010 11:35 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771514381631202464)
,p_name=>'P134_MENSAJE_PROMO_CUOTAS'
,p_item_sequence=>941
,p_item_plug_id=>wwv_flow_imp.id(771514183091202462)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_asdm_promociones.fn_promo_cuotas_gratis;'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:18;color:GREEN"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771517369345202472)
,p_name=>'P134_AVISO'
,p_item_sequence=>750
,p_item_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_prompt=>'AVISO:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'***ANTES DE GRABAR SELECCIONE UN REFERIDOR*** ',
'***SI EL CLIENTE NO ES REFERIDO DAR CLIC EN GRABAR SIN SELECCIONAR REFERIDOR*** ',
'***RECUERDE QUE ESTA INFORMACION ESTA SIENDO AUDITADA***'))
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'style="font-size:16;color:BLUE;font-family:Arial BLACK"'
,p_tag_attributes=>'style="font-size:12;color:RED;font-family:Arial Black"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771517583204202472)
,p_name=>'P134_AYUDA'
,p_item_sequence=>760
,p_item_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_prompt=>'Ayuda'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Causas por las que un referidor no ha sido encontrado:',
'',
unistr('1. El referidor no ha sido creado por el vendedor al momento de ingresar la orden de venta. Soluci\00F3n: Pida al vendedor que cree al referidor y vuelva a realizar la b\00FAsqueda.'),
'',
unistr('2. No se est\00E1 realizando bien la b\00FAsqueda del referidor. Soluci\00F3n: Busque al referidor por el n\00FAmero de identificaci\00F3n.'),
'',
unistr('3. El referidor que est\00E1 buscando probablemente est\00E1 en el listado de personas que no pueden ser referidores. Soluci\00F3n: Compruebe que la persona que busca no est\00E9 en el listado de no permitidos. ')))
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771518068377202474)
,p_name=>'P134_FACTURA_YN'
,p_item_sequence=>770
,p_item_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_prompt=>'Factura Yn'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771518269123202475)
,p_name=>'P134_HAY_PROMOCION'
,p_item_sequence=>780
,p_item_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_prompt=>'Hay Promocion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771518459993202475)
,p_name=>'P134_PER_ID_PROMO'
,p_item_sequence=>700
,p_item_plug_id=>wwv_flow_imp.id(771514760434202467)
,p_prompt=>'Per Id Promo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.PER_ID',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p134_ord_id',
'and a.pta_seleccionado=''S'';'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771521479456202486)
,p_name=>'P134_NRO_IDEN_NO_INGRESADOS'
,p_item_sequence=>524
,p_item_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Iden No Ingresados'
,p_source=>'pq_inv_movimientos_iden.fn_retorna_num_reg_col_ids(pv_error => :P0_ERROR);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771521664038202486)
,p_name=>'P134_NRO_REG_ITEM_IDEN'
,p_item_sequence=>514
,p_item_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Reg Item Iden'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos_iden.fn_retorna_num_reg_inv_det(pn_emp_id => :F_EMP_ID, ',
'                                                   pv_cadena_inicio_reg_iden => :P134_CADENA_INICIO_REG_IDEN,',
'                                            --       pn_ttr_id => :P134_TTR_ID_PADRE,',
'                                                   pv_error  => :P0_ERROR);'))
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771521867314202486)
,p_name=>'P134_BANDERA_IDEN'
,p_item_sequence=>554
,p_item_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_prompt=>'Bandera Iden'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771522057731202486)
,p_name=>'P134_CADENA_INICIO_REG_IDEN'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cadena Inicio Reg Iden'
,p_source=>'CHR(39) || pq_constantes.fn_retorna_constante(NULL,''cv_ite_inicio_reg_nserie_ven'') || CHR(39)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771522254987202486)
,p_name=>'P134_BPR_ID_IDEN'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_imp.id(771521254898202485)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Bpr Id Iden'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos_iden.fn_retorna_bpr_id_ven_ord(pn_ord_id => :P134_ORD_ID, ',
'                                                  pv_ord_tipo => :P134_ORD_TIPO, ',
'                                                  pv_error => :P0_ERROR);'))
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771535859403202503)
,p_name=>'P134_MENSAJE2'
,p_item_sequence=>534
,p_item_plug_id=>wwv_flow_imp.id(771535453965202502)
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:17"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos_iden.fn_valida_mensaje_col_inv_iden(pn_emp_id => :F_EMP_ID, ',
'                                                       pv_cadena_inicio_reg_iden => :P134_CADENA_INICIO_REG_IDEN, ',
'                                                       pv_error => :P0_ERROR);'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771536256022202504)
,p_name=>'P134_MENSAJE'
,p_item_sequence=>92
,p_item_plug_id=>wwv_flow_imp.id(771536057984202503)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771537672183202507)
,p_name=>'P134_SUM_PAGOS'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Sum Pagos'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(TO_NUMBER(c006)),0) suma_valor',
'  FROM apex_collections',
'WHERE collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771537870840202507)
,p_name=>'P134_VALOR_TOTAL_PAGOS'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Pagar'
,p_pre_element_text=>'<b>'
,p_format_mask=>'999G999G999G999G990D00'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT AB.REN_TOTAL_INTERESES',
'  FROM CAR_RENEGOCIACION AB',
' WHERE AB.REN_ID=:P134_REN_ID',
' AND AB.REN_ESTADO_REGISTRO=0;;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771538081913202507)
,p_name=>'P134_CRE_TITULAR_CUENTA'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Titular Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>100
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771538277033202508)
,p_name=>'P134_SEQ_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Registro a modficar (seq_id) :'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P134_SEQ_ID IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771538475990202508)
,p_name=>'P134_CLI_IDENTIFICACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>unistr('Identificaci\00C3\00B3n Cliente')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771538658734202508)
,p_name=>'P134_ES_POLITICA_GOBIERNO'
,p_item_sequence=>931
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_consultas.fn_valida_poltica_gobierno(pn_pol_id => :P134_POLITICA_VENTA,',
'                                                         pn_emp_id => :f_emp_id);'))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771538859558202509)
,p_name=>'P134_PAGINA_REGRESO'
,p_item_sequence=>891
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>'132'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Pagina Regreso'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771539072217202509)
,p_name=>'P134_JA'
,p_item_sequence=>901
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>':P134_CLI_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Ja'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771539264767202509)
,p_name=>'P134_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>810
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Pago Refinanciamiento'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771539454546202509)
,p_name=>'P134_VALOR_REFINANCIAMIENTO'
,p_item_sequence=>820
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Valor Refinanciamiento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:red"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771539659890202509)
,p_name=>'P134_CXC_ID_REF'
,p_item_sequence=>830
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Cxc Id Ref'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771539875760202509)
,p_name=>'P134_PER_ID'
,p_item_sequence=>630
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_promociones_sql.fn_cliente_persona(:p134_cli_id,:f_emp_id, ''P'');',
''))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Per Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771540052804202509)
,p_name=>'P134_REQ'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>':request'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Req'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771540266069202509)
,p_name=>'P134_TERMINO_VENTA'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>':p134_tve_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Termino Venta'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771540451945202509)
,p_name=>'P134_TOTAL_FACTURA'
,p_item_sequence=>670
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Total Factura'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    vor_valor_variable',
'FROM      ven_var_ordenes',
'WHERE     ord_id = :p134_ord_id',
'AND       var_id = 3'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771540665610202510)
,p_name=>'P134_POLITICA_VENTA'
,p_item_sequence=>690
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Politica Venta'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    pol_id',
'FROM      ven_ordenes',
'WHERE     ord_id = :p134_ord_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771540855751202510)
,p_name=>'P134_ALERTA_INTERES_MORA'
,p_item_sequence=>921
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>'''El valor de Interes de Mora que se va a cobrar es: '' || :P134_INT_MORA_COB'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:16px; color: red;"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'to_number(:P134_INT_MORA_COB) > 0 and :F_SEG_ID = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771541061852202510)
,p_name=>'P134_SALDO_ANTICIPOS'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Saldo Anticipos'
,p_source=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P134_CLI_ID,:f_uge_id))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P134_CLI_ID,:f_uge_id))'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771541261352202510)
,p_name=>'P134_ES_PRECANCELACION'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Es Precancelacion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771541468982202510)
,p_name=>'P134_SALDO_PROMO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Saldo Total Promoci\00C3\00B3n')
,p_source=>'to_number(pq_asdm_promociones.fn_obtener_saldo_actual(:P134_CLI_ID,:F_EMP_ID,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),NULL,:P0_Error))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'Saldo total de promociones'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771541975302202510)
,p_name=>'P134_VALIDA_PUNTO'
,p_item_sequence=>911
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771542155919202511)
,p_name=>'P134_SALDO_MAX_APLICAR'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Saldo M\00C3\00A1ximo a Aplicar')
,p_pre_element_text=>'<b>'
,p_source=>'pq_asdm_promociones.fn_obtener_saldo_maximo_aplica(:f_emp_id,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),:P134_cli_id,:P134_ord_id, :P0_error)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') ',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771542383662202511)
,p_name=>'P134_MODIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Modificacion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771542562528202511)
,p_name=>'P134_SALDO_PAGO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Pago'
,p_pre_element_text=>'<b>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*select nvl(:p134_valor_total_pagos,0) - (to_number(sum(nvl(c006,0))) + TO_NUMBER(sum(nvl(c018,0)))) from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')*/',
'',
'select nvl(:p134_valor_total_pagos,0) - to_number(sum(nvl(c006,0)))  from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771542774599202511)
,p_name=>'P134_COT_ID'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Cot Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771542972373202511)
,p_name=>'P134_NUM_FOLIO_NCREDITO'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'No. Folio N/C Inicial'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''validar_nc'' )"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') ',
'or ',
'',
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_promocion_puntaje'') ) and :P134_VALIDA_PUNTO = ''P'''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771543160510202512)
,p_name=>'P134_SECUENCIA_ACTUAL'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Secuencia Actual'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771543368857202512)
,p_name=>'P134_MENSAJE_SECUENCIA'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>unistr('La secuencia actual de la Nota de Cr\00C3\00A9dito es: &P134_SECUENCIA_ACTUAL.')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') AND :P134_NUM_FOLIO_NCREDITO IS NOT NULL',
'',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771543570680202512)
,p_name=>'P134_SALDO_PROMO_DEV'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Total Promocion Devuelta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
' ',
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_dev_promo'') or',
' ',
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'') '))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'Saldo Total de Promocion Devuelta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771544054160202513)
,p_name=>'P134_SALDO_MAX_APL_DEV'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Saldo Maximo a Aplicar'
,p_pre_element_text=>'<b>'
,p_source=>'pq_asdm_promociones.fn_obtener_saldo_max_apl_dev(:f_emp_id,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),:P134_cli_id,:P134_ord_id, :P0_error)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_dev_promo'') ',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771544273308202513)
,p_name=>'P134_MENSAJE_ANTICIPO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED">    APLICAR EL PAGO CON ANTICIPO DE CLIENTES</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'CENTER-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'to_number(:P134_SALDO_ANTICIPOS) > 0 and :P134_FACTURAR = ''N'' AND NVL(:P134_ES_PRECANCELACION,''N'') <> ''S'' and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771544463624202513)
,p_name=>'P134_INI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>'0'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'style="border-style:none;background-color:transparent;font-size:15px;text-align:left;color:transparent;" '
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>'style="border-style:none;background-color:transparent;font-size:1px;text-align:left;color:transparent;" '
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771544977085202513)
,p_name=>'P134_INI2'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>'0'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="border-style:none;background-color:transparent;font-size:1px;text-align:left;color:transparent;" '
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>'style="border-style:none;background-color:transparent;font-size:15px;text-align:left;color:transparent;" '
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771545478110202513)
,p_name=>'P134_CLI_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771545663280202514)
,p_name=>'P134_CLI_NOMBRE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771545878400202514)
,p_name=>'P134_TFP_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF NVL(:P134_SALDO_ANTICIPOS,0) = 0 THEN',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes''); ',
'END IF'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_FORMAS_PAGO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'        lv_lov := pq_ven_listas_caja.fn_lov_formas_pago',
'(',
':F_EMP_ID,',
':P0_TTR_ID,',
':P30_POLITICA_VENTA);',
'',
'return (lv_lov);',
'',
'END;'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P134_VALOR_TOTAL_PAGOS > 0 and :P134_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771546066544202514)
,p_name=>'P134_EDE_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_item_default=>'NULL'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Entidad Destino:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov varchar2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas_caja.fn_lov_entidad_destin_tfp_pag(:F_EMP_ID,',
'                                                             :P134_TFP_ID,',
'                                                             :P0_ERROR);',
'  return(lv_lov);',
'END;',
'',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') or ',
':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771546459622202519)
,p_name=>'P134_CRE_NRO_CUENTA'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(771546269133202514)
,p_prompt=>'Nro Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771546673082202520)
,p_name=>'P134_CRE_NRO_PIN'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(771546269133202514)
,p_prompt=>'Nro Pin:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771546877821202520)
,p_name=>'P134_CRE_FECHA_DEPOSITO'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(771546269133202514)
,p_prompt=>'Fecha Cheque:'
,p_source=>'to_char(sysdate, ''DD/MM/YYYY'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771547068262202520)
,p_name=>'P134_CRE_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(771546269133202514)
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771547265995202520)
,p_name=>'P134_CRE_NRO_CHEQUE'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(771546269133202514)
,p_prompt=>'Nro Cheque:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771547866263202524)
,p_name=>'P134_VUELTO'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(771547468015202524)
,p_prompt=>'Vuelto/Cambio Efectivo'
,p_pre_element_text=>'<b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" style="font-size:16"'
,p_tag_attributes=>'style="font-size:26"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number;',
' ',
'BEGIN',
'',
'',
' select count(*) into ln_cuenta from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                         ''cv_coleccion_mov_caja'');',
'',
' if ln_cuenta >0  and :P134_VALOR_TOTAL_PAGOS > 0 and :P134_FACTURAR <> ''R'' then',
'     return true;',
' else ',
'     return false;',
' end if;',
'',
'END;'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'FUNCTION_BODY'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771548060533202524)
,p_name=>'P134_PRECIONEENTER'
,p_item_sequence=>17
,p_item_plug_id=>wwv_flow_imp.id(771547468015202524)
,p_prompt=>'Presione Enter para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771548259864202525)
,p_name=>'P134_ENTRADA'
,p_item_sequence=>382
,p_item_plug_id=>wwv_flow_imp.id(771547468015202524)
,p_prompt=>'Entrada'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--Utilizo solo cuando la venta se ha realizado con una tarjeta de credito.',
'--aqui cargo el valor de la entrada que cancelo a parte de la tarjeta de credito'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771548462974202525)
,p_name=>'P134_COMISION'
,p_item_sequence=>392
,p_item_plug_id=>wwv_flow_imp.id(771547468015202524)
,p_prompt=>'Comision'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771548681541202525)
,p_name=>'P134_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(771547468015202524)
,p_prompt=>'Valor Recibido:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''cargar_tfp'');" '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P134_VALOR_TOTAL_PAGOS > 0 and :P134_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Presione Enter para cargar el valor'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771549372017202527)
,p_name=>'P134_MCD_NRO_COMPROBANTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771549182485202526)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Comprobante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>13
,p_cMaxlength=>12
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771549562726202527)
,p_name=>'P134_MCD_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(771549182485202526)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Deposito:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771549960878202527)
,p_name=>'P134_TJ_NUMERO'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771550164010202527)
,p_name=>'P134_TJ_NUMERO_VOUCHER'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771550367220202527)
,p_name=>'P134_TJ_TITULAR_Y'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Titular:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771550573903202528)
,p_name=>'P134_TJ_ENTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Entidad'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_TARJETA_CREDITO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    ',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_PLAN_TARJETA_CORRIENTE'');',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771550771404202528)
,p_name=>'P134_TJ_RECAP'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'RECAP:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771550980667202528)
,p_name=>'P134_BANCOS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Banco:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'SELECT a.ede_descripcion,a.ede_id FROM asdm_entidades_destinos a WHERE a.ede_tipo=''EFI'''
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771551162914202528)
,p_name=>'P134_TARJETA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Tarjeta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id FROM asdm_entidades_destinos b WHERE b.ede_tipo=''TJ''',
'AND b.ede_id_padre=:p134_bancos'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P134_BANCOS'
,p_ajax_items_to_submit=>'P134_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771551362988202528)
,p_name=>'P134_TIPO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Tipo:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p134_tarjeta'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P134_TARJETA'
,p_ajax_items_to_submit=>'P134_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771551580230202529)
,p_name=>'P134_PLAN'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Plan:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_descripcion, ''->'') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_tede_detalle_plan_tarjeta'')',
'START WITH ede.ede_id_padre =:P134_TIPO',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P134_TIPO'
,p_ajax_items_to_submit=>'P134_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771551754343202529)
,p_name=>'P134_LOTE'
,p_item_sequence=>322
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Lote'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771551957816202529)
,p_name=>'P134_MCD_NRO_AUT_REF'
,p_item_sequence=>332
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>unistr('Autorizaci\00C3\00B3n:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771552155931202529)
,p_name=>'P134_TITULAR_TC'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(771549772896202527)
,p_prompt=>'Titular Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771552573589202534)
,p_name=>'P134_RET_IVA'
,p_item_sequence=>255
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'Retiene Iva'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Si;S,No;N'
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771552763268202534)
,p_name=>'P134_TRE_ID'
,p_item_sequence=>1961
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'%'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  if :P134_RET_IVA = ''S'' then',
'    lv_lov := ''SELECT RPO_PORCENTAJE, RPO_vALOR',
'FROM CAR_RET_PORCENTAJES'';',
'  else',
'    lv_lov := ''SELECT RPO_PORCENTAJE, RPO_vALOR',
'FROM CAR_RET_PORCENTAJES',
'WHERE RPO_DESCRIPCION = ''''RET FUENTE'''''';',
'  end if;',
'  RETURN(lv_lov);',
'END;',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771552968742202535)
,p_name=>'P134_SEQ_ID_RET'
,p_item_sequence=>1971
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771553161595202535)
,p_name=>'P134_SUBTOTAL_IVA'
,p_item_sequence=>1981
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P134_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_con_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771553355858202535)
,p_name=>'P134_SUBTOTAL_SIN_IVA'
,p_item_sequence=>1991
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Sin Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P134_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_sin_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771553558485202535)
,p_name=>'P134_SUBTOTAL'
,p_item_sequence=>1992
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_item_default=>'(nvl(:P134_SUBTOTAL_IVA,0) + nvl(:P134_SUBTOTAL_SIN_IVA,0)) - nvl(:P134_FLETE, 0)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Subtotal'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771553770938202535)
,p_name=>'P134_FLETE'
,p_item_sequence=>2011
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Flete'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P134_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_servicio_manejo_producto'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771553960503202535)
,p_name=>'P134_IVA'
,p_item_sequence=>2021
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P134_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_iva_calculo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771554179659202535)
,p_name=>'P134_IVA_BIENES'
,p_item_sequence=>2031
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P134_IVA - (:P134_FLETE * 0.12)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Bienes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771554357504202535)
,p_name=>'P134_IVA_FLETE'
,p_item_sequence=>2041
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P134_FLETE * 0.14'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Flete'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771554573993202536)
,p_name=>'P134_INT_DIFERIDO'
,p_item_sequence=>2051
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Int Diferido'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P134_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_int_diferido'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771554780497202536)
,p_name=>'P134_BASE'
,p_item_sequence=>2061
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'Base'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGA_RET'');" '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771554965462202536)
,p_name=>'P134_PRECIONEENTER11'
,p_item_sequence=>2071
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'<SPAN STYLE="font-size: 10pt;color:RED;">Presione Enter para cargar el valor</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771555181917202536)
,p_name=>'P134_RAP_NRO_RETENCION'
,p_item_sequence=>262
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_AUTORIZACION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771555360039202536)
,p_name=>'P134_RAP_NRO_AUTORIZACION'
,p_item_sequence=>272
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'Nro Autorizacion:'
,p_post_element_text=>' 10 - 37 - 49 digitos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>49
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_FECHA'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771555559155202536)
,p_name=>'P134_RAP_FECHA'
,p_item_sequence=>282
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Retencion:'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771555783273202536)
,p_name=>'P134_RAP_FECHA_VALIDEZ'
,p_item_sequence=>292
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771555972169202536)
,p_name=>'P134_RAP_COM_ID'
,p_item_sequence=>302
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'Factura:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LV_VEN_COMPROBANTES_F_ND'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    lv_lov := pq_asdm_listas.fn_lov_facturas_pago_cuota(:f_emp_id);',
'',
'/*lv_lov :=kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_COMPROBANTES_F_ND_SIN_RET'');',
'lv_lov := lv_lov || ''AND vco.cli_id = :P64_CLI_ID ORDER BY vco.com_id'';*/',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Facturas Disponibles --'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p134_FACTURAR = ''N'' '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771556178511202537)
,p_name=>'P134_SALDO_RETENCION'
,p_item_sequence=>312
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_post_element_text=>'  para Anticipo de Clientes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771556351754202537)
,p_name=>'P134_RAP_NRO_ESTABL_RET'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'Nro Retencion:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_PEMISION_RET'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771556574495202537)
,p_name=>'P134_RAP_NRO_PEMISION_RET'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_RETENCION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771556765091202537)
,p_name=>'P134_PRE_ID'
,p_item_sequence=>372
,p_item_plug_id=>wwv_flow_imp.id(771552355638202534)
,p_prompt=>'Pre Id:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771558451574202538)
,p_name=>'P134_OBSERVACIONES_RET'
,p_item_sequence=>2081
,p_item_plug_id=>wwv_flow_imp.id(771556951965202537)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771558854629202539)
,p_name=>'P134_FACTURAR'
,p_item_sequence=>112
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_prompt=>'Facturar'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771559060465202539)
,p_name=>'P134_COM_ID'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771559281658202539)
,p_name=>'P134_TRX_ID'
,p_item_sequence=>132
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771559456036202539)
,p_name=>'P134_ORD_ID'
,p_item_sequence=>102
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_prompt=>'Ord Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771559671174202539)
,p_name=>'P134_ERROR'
,p_item_sequence=>142
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771559870768202539)
,p_name=>'P134_LABEL_BOTON'
,p_item_sequence=>202
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Label Boto'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE WHEN ',
'   :P134_FACTURAR = ''N'' or :P134_FACTURAR = ''R'' or :P134_FACTURAR = ''M'' THEN ''PAGAR''',
'WHEN ',
'   :P134_FACTURAR = ''S'' THEN ''FACTURAR''',
'ELSE ''ERROR'' END FROM DUAL'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771560054409202539)
,p_name=>'P134_TVE_ID'
,p_item_sequence=>222
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771560260189202539)
,p_name=>'P134_ODE_ID'
,p_item_sequence=>232
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Orden Despacho'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ode_id',
'FROM   inv_ordenes_despacho',
'WHERE  ord_id = :P134_ORD_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771560481068202540)
,p_name=>'P134_F_UGE_ID'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Uge Id'
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771560681109202540)
,p_name=>'P134_F_USER_ID'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F User Id'
,p_source=>'F_USER_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771560856828202540)
,p_name=>'P134_F_EMP_ID'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Emp Id'
,p_source=>'F_EMP_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771561078457202540)
,p_name=>'P134_ORD_TIPO'
,p_item_sequence=>600
,p_item_plug_id=>wwv_flow_imp.id(771558661331202538)
,p_prompt=>'Ord Tipo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771561475907202541)
,p_name=>'P134_BORRAR_DL'
,p_item_sequence=>1951
,p_item_plug_id=>wwv_flow_imp.id(771561273193202540)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P134_HAY_PROMOCION ||'' otR ''|| :P134_ORD_ID ||'' TVE ''|| :p134_tve_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'A'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771561864738202542)
,p_name=>'P134_TTR_ID_PADRE'
,p_item_sequence=>801
,p_item_plug_id=>wwv_flow_imp.id(771561657214202542)
,p_item_default=>':p0_ttr_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>unistr('Tipo Transacci\00F3n')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_TIPOS_TRANSACCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ASDM_TIPOS_TRANSACCIONES_MUESTRA_ID'');',
'return (lv_lov);',
'END;',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit('''')" style="font-size:18;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771562075362202542)
,p_name=>'P134_DOC_ID_PADRE'
,p_item_sequence=>805
,p_item_plug_id=>wwv_flow_imp.id(771561657214202542)
,p_prompt=>'Documento'
,p_display_as=>'PLUGIN_COM_SKILLBUILDERS_SUPER_LOV'
,p_named_lov=>'LOV_ASDM_DOCUMENTOS_5'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   lv_lov varchar2(500);',
'BEGIN',
'   lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_ASDM_DOCUMENTOS'');',
'   lv_lov := lv_lov ||'' WHERE ttr_id = ''||NVL(NVL(:P30_TTR_ID_PADRE,:p0_ttr_id),''ttr_id'');',
'   lv_lov := lv_lov ||'' AND uge_id = ''||NVL(:F_UGE_ID,''UGE_ID'');',
'   lv_lov := lv_lov ||'' AND SEGMENTO = ''||NVL(:F_SEG_ID,''SEGMENTO'');',
'--   lv_lov := lv_lov ||'' AND (TIPO_ORDEN  is not null''||'' AND ''||NVL(:P30_MOTOS_YN,CHR(39)||''N''||CHR(39))||'' = ''||CHR(39)||''S''||CHR(39)||'')'';',
'--   lv_lov := lv_lov ||'' AND (TIPO_ORDEN  = ''||CHR(39)||''VENMO''||CHR(39)||'' AND ''||NVL(:P30_MOTOS_YN,CHR(39)||''N''||CHR(39))||'' = ''||CHR(39)||''S''||CHR(39)||'')'';',
'   lv_lov := lv_lov ||'' AND Documento NOT IN (SELECT    doc_id_padre',
'                                              FROM      asdm_relaciones_documentos)'';',
'',
'   pruebas_iv(444888,lv_lov);',
'   RETURN (lv_lov);',
'END;',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_03=>'2,1'
,p_attribute_08=>'NOT_ENTERABLE'
,p_attribute_09=>'15'
,p_attribute_10=>'&nbsp;'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771562281426202543)
,p_name=>'P134_MOTOS_YN'
,p_item_sequence=>881
,p_item_plug_id=>wwv_flow_imp.id(771561657214202542)
,p_prompt=>'Motos Yn'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    DECODE(c.tre_id,1000,''S'',''N'') MOTO',
'FROM      ven_ordenes a,',
'          ven_ordenes_det b,',
'          inv_relaciones_item c,',
'          inv_tipos_relaciones d',
'WHERE     a.ord_id = :P20_ORDEN_VENTA',
'AND       b.ord_id = a.ord_id',
'AND       c.ite_sku_hijo = b.ite_sku_id',
'AND       d.tre_id = c.tre_id',
'AND       d.tre_id = 1000',
'AND       ROWNUM = 1'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771562453520202543)
,p_name=>'P134_COM_ID_PADRE'
,p_item_sequence=>851
,p_item_plug_id=>wwv_flow_imp.id(771561657214202542)
,p_prompt=>'Comprobante Padre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771562663356202543)
,p_name=>'P134_FACTURA_PADRE'
,p_item_sequence=>861
,p_item_plug_id=>wwv_flow_imp.id(771561657214202542)
,p_prompt=>'Factura Padre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771562883539202543)
,p_name=>'P134_RELACION_OBLIGATORIA'
,p_item_sequence=>871
,p_item_plug_id=>wwv_flow_imp.id(771561657214202542)
,p_prompt=>'Relacion Obligatoria'
,p_source=>'pq_ven_comunes.fn_revisa_doc_obligatorio(:P20_ORDEN_VENTA,:F_EMP_ID)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771570260043202547)
,p_name=>'P134_PER_ID_PUNTOS'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(771569852595202546)
,p_prompt=>'Cliente con Puntaje'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_PERSONAS_PROMOS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   lv_lov varchar2(5000);',
'BEGIN',
'   lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_PERSONAS_PROMOS'');',
'   lv_lov := lv_lov || '' AND a.emp_id = '' || :F_EMP_ID;',
'   return (lv_lov);',
'END;',
''))
,p_lov_display_null=>'YES'
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit('''');"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771571671463202548)
,p_name=>'P134_FACTURA_YN_R'
,p_item_sequence=>740
,p_item_plug_id=>wwv_flow_imp.id(771570458383202547)
,p_prompt=>'Factura Yn R'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771572080330202550)
,p_name=>'P134_PDI_ID'
,p_item_sequence=>800
,p_item_plug_id=>wwv_flow_imp.id(771571858171202549)
,p_prompt=>'Pdi Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771572253794202550)
,p_name=>'P134_PRO_ID'
,p_item_sequence=>620
,p_item_plug_id=>wwv_flow_imp.id(771571858171202549)
,p_prompt=>'Pro Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771572451772202550)
,p_name=>'P134_PUNTAJE_FIJO'
,p_item_sequence=>650
,p_item_plug_id=>wwv_flow_imp.id(771571858171202549)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Puntaje Fijo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.pta_id',
'      from ven_bitacora_promociones a, ven_promociones b, ven_promos_tipos_aplica c',
'      where a.per_id=:P134_per_id',
'      and a.pro_id=b.pro_id',
'       and c.pro_id=a.pro_id',
'       and c.pta_id=a.pta_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771572683455202550)
,p_name=>'P134_PTA_ID'
,p_item_sequence=>660
,p_item_plug_id=>wwv_flow_imp.id(771571858171202549)
,p_prompt=>'Pta Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771572866532202550)
,p_name=>'P134_PUNTOS_CANCELAR'
,p_item_sequence=>121
,p_item_plug_id=>wwv_flow_imp.id(771571858171202549)
,p_prompt=>'Puntos Cancelar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:12;color:blue;font-family:Arial"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P134_POLITICA_VENTA <> 112'
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_help_text=>unistr('Ingrese la cantidad de puntos que el cliente est\00E1 pagando.')
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771573363889202551)
,p_name=>'P134_PUNTOS_USADOS'
,p_item_sequence=>790
,p_item_plug_id=>wwv_flow_imp.id(771571858171202549)
,p_prompt=>'Puntos Usados'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    vor_valor_variable',
'FROM      ven_var_ordenes',
'WHERE     ord_id = :p134_ord_id',
'AND       var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total'')',
'AND       :P134_PER_ID_PUNTOS IS NOT NULL',
'AND       :P134_POLITICA_VENTA IN(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_pol_id_48''))',
'AND       :P134_PUNTOS_CANCELAR > 0',
'',
'UNION',
'',
'SELECT    TO_NUMBER(:P134_MCD_VALOR_MOVIMIENTO)',
'FROM      DUAL',
'WHERE     :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'')',
'--AND       :P134_PUNTOS_CANCELAR > 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771601257774202567)
,p_name=>'P134_GTOS_COB_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(771595065287202563)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771607680148202570)
,p_name=>'P134_INT_MORA_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(771601465005202567)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771654575314738798)
,p_name=>'P134_REN_ID'
,p_item_sequence=>2091
,p_item_plug_id=>wwv_flow_imp.id(771536471871202505)
,p_prompt=>'Ren Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771608153618202580)
,p_validation_name=>'P134_MCD_VALOR_MOVIMIENTO_num'
,p_validation_sequence=>1
,p_validation=>'P134_MCD_VALOR_MOVIMIENTO'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'Error!!! Ingrese un valor correcto'
,p_always_execute=>'Y'
,p_validation_condition=>':request = ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771548681541202525)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
,p_validation_comment=>'AND :P30_TFP_ID <> pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771608355200202582)
,p_validation_name=>'P134_MCD_VALOR_MOVIMIENTO_0'
,p_validation_sequence=>3
,p_validation=>'(:P134_MCD_VALOR_MOVIMIENTO) > 0'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese un valor mayor a 0'
,p_validation_condition=>'cargar_tfp'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(771548681541202525)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771607962797202579)
,p_validation_name=>'P134_MCD_VALOR_MOVIMIENTO'
,p_validation_sequence=>5
,p_validation=>'to_number(:P134_MCD_VALOR_MOVIMIENTO) > to_number(:P134_SALDO_PROMO_DEV)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Valor aplicar no puede ser mayor a total de puntaje....'
,p_validation_condition=>'cargar_tfp'
,p_validation_condition_type=>'NEVER'
,p_associated_item=>wwv_flow_imp.id(771548681541202525)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771612773135202588)
,p_validation_name=>'P134_EDE_ID'
,p_validation_sequence=>20
,p_validation=>'P134_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione Una Entidad'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:REQUEST = ''cargar_tfp'') ',
'AND ((:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')) or (:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'')))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771546066544202514)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771518762873202475)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'ORD_ID not null'
,p_validation_sequence=>30
,p_validation=>'ORD_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'ORD_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771518968322202483)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'ORD_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'ORD_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'ORD_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771613552187202589)
,p_validation_name=>'P134_CRE_TITULAR_CUENTA'
,p_validation_sequence=>30
,p_validation=>'P134_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la cuenta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771538081913202507)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771519158385202483)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'PTA_ID must be numeric'
,p_validation_sequence=>40
,p_validation=>'PTA_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'PTA_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771613359382202588)
,p_validation_name=>'P134_CRE_NRO_CHEQUE'
,p_validation_sequence=>40
,p_validation=>'P134_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cheque'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771547265995202520)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771519365749202483)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'PRO_ID must be numeric'
,p_validation_sequence=>50
,p_validation=>'PRO_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'PRO_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771612982377202588)
,p_validation_name=>'P134_CRE_NRO_CUENTA'
,p_validation_sequence=>50
,p_validation=>'P134_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cuenta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771546459622202519)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771519556192202483)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'EMP_ID not null'
,p_validation_sequence=>60
,p_validation=>'EMP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'EMP_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771519751895202483)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'EMP_ID must be numeric'
,p_validation_sequence=>60
,p_validation=>'EMP_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'EMP_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771613165305202588)
,p_validation_name=>'P134_CRE_NRO_PIN'
,p_validation_sequence=>60
,p_validation=>'P134_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el PIN'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771546673082202520)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771519958098202484)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'POR_ESTADO_REGISTROS not null'
,p_validation_sequence=>70
,p_validation=>'POR_ESTADO_REGISTROS'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'POR_ESTADO_REGISTROS'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771520153221202484)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'POR_ESTADO_REGISTROS must be numeric'
,p_validation_sequence=>70
,p_validation=>'POR_ESTADO_REGISTROS'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'POR_ESTADO_REGISTROS'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771613781641202589)
,p_validation_name=>'P134_TFP_ID'
,p_validation_sequence=>70
,p_validation=>'P134_TFP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione una forma de pago'
,p_validation_condition=>':P134_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_contado'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771545878400202514)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771520375247202484)
,p_tabular_form_region_id=>wwv_flow_imp.id(771514760434202467)
,p_validation_name=>'PER_ID must be numeric'
,p_validation_sequence=>80
,p_validation=>'PER_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(771517171098202471)
,p_associated_column=>'PER_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771608553944202583)
,p_validation_name=>'P134_MCD_VALOR_MOVIMIENTO_ANT'
,p_validation_sequence=>90
,p_validation=>'to_number(:P134_MCD_VALOR_MOVIMIENTO) <= to_number(:P134_SALDO_ANTICIPOS);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese un valor menor al disponible en Anticipo de Clientes'
,p_always_execute=>'Y'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes'') ',
'AND NVL(:P134_MCD_VALOR_MOVIMIENTO,0) > 0'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771548681541202525)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771608751545202583)
,p_validation_name=>'P134_MCD_NRO_COMPROBANTE'
,p_validation_sequence=>100
,p_validation=>'P134_MCD_NRO_COMPROBANTE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de comprobante de dep\00C3\00B3sito o transferencia')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771549372017202527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771608972053202583)
,p_validation_name=>'P134_MCD_VALOR_MOVIMIENTO_PR'
,p_validation_sequence=>110
,p_validation=>'round(to_number(nvl(:P134_SALDO_MAX_APLICAR,0)),4)>= round(to_number(nvl(:P134_MCD_VALOR_MOVIMIENTO,0)),4);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El valor no debe ser mayor al "Saldo M\00E1ximo a Aplicar....')
,p_validation_condition=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'')  and :P134_MCD_VALOR_MOVIMIENTO > 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771548681541202525)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771609161066202583)
,p_validation_name=>'P134_RAP_NRO_AUTORIZACION'
,p_validation_sequence=>130
,p_validation=>'to_number(:P134_RAP_NRO_AUTORIZACION)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de autorizaci\00C3\00B3n de la retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555360039202536)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771609358383202583)
,p_validation_name=>'P134_RAP_FECHA'
,p_validation_sequence=>140
,p_validation=>':P134_RAP_FECHA IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la fecha de la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555559155202536)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771609557784202583)
,p_validation_name=>'P134_RAP_FECHA_VALIDEZ'
,p_validation_sequence=>150
,p_validation=>':P134_RAP_FECHA_VALIDEZ IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la fecha de validez de la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555783273202536)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771609755708202583)
,p_validation_name=>'P134_RAP_COM_ID'
,p_validation_sequence=>160
,p_validation=>':P134_RAP_COM_ID IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la factura a la que pertenece la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'') and :P134_FACTURAR = ''N'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555972169202536)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771609971781202586)
,p_validation_name=>'P134_RAP_NRO_RETENCION'
,p_validation_sequence=>170
,p_validation=>'to_number(:P134_RAP_NRO_RETENCION)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555972169202536)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771610156844202586)
,p_validation_name=>'P134_RAP_NRO_ESTABL_RET'
,p_validation_sequence=>180
,p_validation=>'to_number(:P134_RAP_NRO_ESTABL_RET)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero de establecimiento de la retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771556351754202537)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771610368056202586)
,p_validation_name=>'P134_RAP_NRO_PEMISION_RET'
,p_validation_sequence=>190
,p_validation=>'to_number(:P134_RAP_NRO_PEMISION_RET)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00C3\00BAmero del punto de emisi\00C3\00B3n de la retenci\00C3\00B3n')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771556574495202537)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771610554931202586)
,p_validation_name=>'P134_PLAN'
,p_validation_sequence=>200
,p_validation=>'P134_PLAN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe Seleccionar el Plan'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771551580230202529)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771610766607202586)
,p_validation_name=>'P134_TITULAR_TC'
,p_validation_sequence=>210
,p_validation=>'P134_TITULAR_TC'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el titular de la Tarjeta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771552155931202529)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771610959994202586)
,p_validation_name=>'P134_TJ_NUMERO'
,p_validation_sequence=>220
,p_validation=>'P134_TJ_NUMERO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el numero de la Tarjeta'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771549960878202527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771611156232202586)
,p_validation_name=>'P134_TJ_NUMERO_VOUCHER'
,p_validation_sequence=>230
,p_validation=>'P134_TJ_NUMERO_VOUCHER'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Numero del Voucher'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771550164010202527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771611368454202586)
,p_validation_name=>'P134_MCD_NRO_AUT_REF'
,p_validation_sequence=>240
,p_validation=>'P134_MCD_NRO_AUT_REF'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Autorizacion'
,p_validation_condition=>':request = ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771551957816202529)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771611557388202586)
,p_validation_name=>'P134_RAP_NRO_AUTORIZACION_MAX'
,p_validation_sequence=>250
,p_validation=>'LENGTH(:P134_RAP_NRO_AUTORIZACION) = 10'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El n\00C3\00BAmero de autorizaci\00C3\00B3n de la retenci\00C3\00B3n debe ser de 10 d\00C3\00ADgitos.')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'') and :P134_ES_POLITICA_GOBIERNO = 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555360039202536)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771611754216202586)
,p_validation_name=>'P134_RAP_FECHA_VALIDEZ_MAYOR_ACTUAL'
,p_validation_sequence=>260
,p_validation=>'trunc(to_date(:P134_RAP_FECHA_VALIDEZ)) >= trunc(SYSDATE)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'La fecha de validez no debe ser menor a la fecha actual'
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555783273202536)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771611983229202587)
,p_validation_name=>'P134_RAP_FECHA_VALIDEZ_MAYOR_FECHA'
,p_validation_sequence=>270
,p_validation=>'trunc(to_date(:P134_RAP_FECHA_VALIDEZ)) >= trunc(to_date(:P134_RAP_FECHA))'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('La fecha de validez no puede ser menor a la fecha de ingreso de la retenci\00C3\00B3n')
,p_validation_condition=>':request= ''cargar_tfp'' AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771555783273202536)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771612166310202588)
,p_validation_name=>'P134_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>280
,p_validation=>'P134_MCD_FECHA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la fecha de deposito'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771549562726202527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771612359233202588)
,p_validation_name=>'P134_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>290
,p_validation=>'trunc(to_date(:P134_MCD_FECHA_DEPOSITO)) <= trunc(sysdate)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Fecha deposito no puede ser mayor a la fecha actual'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771549562726202527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771612575722202588)
,p_validation_name=>'P134_NUM_FOLIO_NCREDITO'
,p_validation_sequence=>300
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Declare ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'   lv_ttr_id varchar2 (30);',
'',
'',
'BEGIN  ',
'  if  :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_puntualito_regalon'') then',
'  ',
'   --lv_ttr_id := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito'');',
'   lv_ttr_id := pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_nc_rebaja_puntualito'');',
'  ',
'  elsif :P134_TFP_ID =',
'        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tfp_id_promocion_puntaje'') then',
'',
'     lv_ttr_id := pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_nc_rebaja_puntaje'');',
'     --lv_ttr_id := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntaje'');',
'',
'  end if;',
'',
'',
'--ln_tgr_id  :=  pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito'');',
' -- :P0_TTR_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito'');',
'',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito''),',
'      :P0_TTR_DESCRIPCION, ',
'      :P0_PERIODO,',
'      :P0_DATFOLIO,',
'      :P134_SECUENCIA_ACTUAL,',
'      :P0_PUE_NUM_SRI  ,',
'      :P0_UGE_NUM_EST_SRI ,',
'      :P0_PUE_ID,',
'      lv_ttr_id ,',
'      --pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito''),',
'      :P0_NRO_FOLIO,',
'      :P0_ERROR);',
'',
'',
'',
'if :P134_NUM_FOLIO_NCREDITO = :P134_SECUENCIA_ACTUAL then',
'return null;',
'else',
'return ''Secuencia Incorrecta'';--||'' ''||:p134_secuencia_actual;',
'end if;',
'END;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>':request= ''validar_nc'' AND (:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_promocion_puntaje''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771542972373202511)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(771613958467202589)
,p_validation_name=>'P134_MCD_NRO_COMPROBANTE'
,p_validation_sequence=>310
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select mcd.mcd_nro_comprobante',
'                from asdm_movimientos_caja         mca,',
'                     asdm_movimientos_caja_detalle mcd',
'               where mcd.mca_id = mca.mca_id',
'                 and mcd.emp_id = mca.emp_id',
'                 and mcd.tfp_id IN',
'                     (pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                         ''cn_tfp_id_deposito''),',
'                      pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                         ''cn_tfp_id_transferencia''))',
'                 and mcd.mcd_nro_comprobante = to_number(:P134_MCD_NRO_COMPROBANTE)',
'                 and mca.mca_estado_mc is null',
'                 and mca.mca_id_referencia is null',
'                 and mca.emp_id = :F_emp_id',
'                 and not exists',
'               (select null',
'                        from asdm_movimientos_caja ca',
'                       where ca.mca_id_referencia = mca.mca_id);'))
,p_validation_type=>'NOT_EXISTS'
,p_error_message=>'YA EXISTE UNA PAPELETA INGRESADA CON ESTE NUMERO'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(771549372017202527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771620863980202609)
,p_name=>'pr_refrescar'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771621170460202611)
,p_event_id=>wwv_flow_imp.id(771620863980202609)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(771601465005202567)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771621353402202611)
,p_event_id=>wwv_flow_imp.id(771620863980202609)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(771595065287202563)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771621570553202611)
,p_event_id=>wwv_flow_imp.id(771620863980202609)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(771536471871202505)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771621756079202611)
,p_event_id=>wwv_flow_imp.id(771620863980202609)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(771552355638202534)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771621853241202611)
,p_name=>'AD_GRABACION'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#pagar'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771622173056202611)
,p_event_id=>wwv_flow_imp.id(771621853241202611)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_INI2'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'INI2'
,p_attribute_09=>'N'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771622362311202611)
,p_event_id=>wwv_flow_imp.id(771621853241202611)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_INI'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'INI'
,p_stop_execution_on_error=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771622468041202611)
,p_name=>'AD_EJECUTA_SUBMIT'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P134_INI'
,p_condition_element=>'P134_INI'
,p_triggering_condition_type=>'IN_LIST'
,p_triggering_expression=>'INI'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771622756812202612)
,p_event_id=>wwv_flow_imp.id(771622468041202611)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'doSubmit(''&P134_INI2.'');'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771623665749202612)
,p_name=>'CIERRA_PROCESO'
,p_event_sequence=>60
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'unload'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771623976579202612)
,p_event_id=>wwv_flow_imp.id(771623665749202612)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_INI'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'FIN'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771624172246202612)
,p_event_id=>wwv_flow_imp.id(771623665749202612)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_INI2'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'FIN2'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771624282654202612)
,p_name=>'AD_VALIDA_COMPROBANTE_PADRE'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P134_COM_ID_PADRE'
,p_condition_element=>'P134_COM_ID_PADRE'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771624562951202615)
,p_event_id=>wwv_flow_imp.id(771624282654202612)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_FACTURA_PADRE'
,p_attribute_01=>'FUNCTION_BODY'
,p_attribute_06=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_comunes.fn_busca_documento_relacionado(:p134_ttr_id_padre,',
'                                                     :p134_com_id_padre,',
'                                                     :P134_CLI_ID);'))
,p_attribute_07=>'P134_COM_ID_PADRE'
,p_attribute_08=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771624663090202615)
,p_name=>'AD_REFRESH_VALIDA_PUNTO'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P134_VALIDA_PUNTO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771624952137202615)
,p_event_id=>wwv_flow_imp.id(771624663090202615)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P134_VALIDA_PUNTO'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771625069461202615)
,p_name=>'ad_submit_tiene_iva_ret'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P134_RET_IVA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771625380174202615)
,p_event_id=>wwv_flow_imp.id(771625069461202615)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P134_RET_IVA'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(771625451528202615)
,p_name=>'ad_muestra_items_iva_s'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P134_RET_IVA'
,p_condition_element=>'P134_RET_IVA'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771625769301202616)
,p_event_id=>wwv_flow_imp.id(771625451528202615)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_INT_DIFERIDO,P134_IVA_FLETE,P134_IVA_BIENES,P134_IVA,P134_FLETE,P134_SUBTOTAL,P134_SUBTOTAL_IVA,P134_SUBTOTAL_SIN_IVA'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(771625974431202616)
,p_event_id=>wwv_flow_imp.id(771625451528202615)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P134_IVA_FLETE,P134_IVA_BIENES,P134_IVA'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771616053122202594)
,p_process_sequence=>60
,p_process_point=>'AFTER_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_total_gastos_para_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_pagos_cuota.pr_total_gastos_para_folios(',
'pn_cli_id => :P134_CLI_ID,',
'pn_emp_id => :F_EMP_ID,',
'pn_seg_id => :F_SEG_ID,',
'pv_facturar => :P134_FACTURAR,',
'pn_int_mora => :P134_INT_MORA_COB,',
'pn_gto_cob  => :P134_GTOS_COB_COB);'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739362901852437668
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771620062118202607)
,p_process_sequence=>160
,p_process_point=>'AFTER_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_VERIFICAR_PROMO_APLICA'
,p_process_sql_clob=>':P134_HAY_PROMOCION := ''S'';'
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'A.POR_ID,',
'A.POR_ID POR_ID_DISPLAY,',
'A.ORD_ID,',
'A.PTA_ID,',
'A.PRO_ID,',
'A.EMP_ID,',
'A.POR_ESTADO_REGISTROS,',
'A.PER_ID,',
'A.POR_NO_ID_BENEF,',
'A.PTA_NOMBRE,',
'A.PTA_SELECCIONADO,',
'B.PRO_DESCRIPCION',
'from ASDM_E.VEN_PROMOCIONES_ORDENES A,',
'     ASDM_E.VEN_PROMOCIONES B',
'WHERE b.pro_id = a.pro_id',
'AND a.emp_id = :F_EMP_ID',
'AND a.ord_id = :p134_ord_id'))
,p_process_when_type=>'EXISTS'
,p_internal_uid=>739366910848437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771619251371202607)
,p_process_sequence=>150
,p_process_point=>'AFTER_FOOTER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_VALIDAR_POL_REGALOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P134_POLITICA_VENTA = 112 AND :P134_PER_ID_PUNTOS IS NOT NULL AND :P134_PUNTOS_CANCELAR IS NOT NULL THEN',
'   :P134_FACTURA_YN_R := ''S'';',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739366100101437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771619672137202607)
,p_process_sequence=>150
,p_process_point=>'AFTER_FOOTER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_persona_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'BEGIN',
'   SELECT    PER_ID',
'   INTO      :F_PER_ID_P',
'   FROM      ASDM_CLIENTES',
'   WHERE     CLI_ID = :P134_CLI_ID;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739366520867437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771616283714202594)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_gastos_para_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
' :P134_INI := NULL;',
' :P134_INI2 := NULL;',
'',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_int_mora''));',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_gto_cobr''));',
'',
'pq_ven_pagos_cuota.pr_carga_gastos_para_folios(pn_cli_id => :P134_CLI_ID,',
'                                       pn_emp_id => :F_EMP_ID,',
'                                       pn_seg_id => :F_SEG_ID,',
'                                       pv_error => :P0_ERROR',
'                                       );',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''formas_pago'' '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739363132444437668
,p_process_comment=>'Existia un problema cuando se hacia este procedimiento desde esta pantalla, porque al inicio se quedaba el request cargado con ''formas_pago'' y si se hacia un refresh F5 se volvia a cargar el proceso, es por esta razon que se puso en al App:211 pag: 6'
||unistr(' para que se carga cuando hagan clic en pagar all\00C3\00AD.')
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771617656343202597)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_linea_coleccion_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
' lv_nombre_coleccion VARCHAR2(50);',
'BEGIN',
'',
'lv_nombre_coleccion := pq_constantes.fn_retorna_constante(NULL,',
'                                                              ''cv_coleccion_mov_caja'');',
'',
'pq_inv_movimientos.pr_borra_reg_colecciones(:p134_seq_id ,lv_nombre_coleccion);',
'',
'pq_ven_movimientos_caja.pr_vueltos(pn_total_pagar => to_number(:P134_VALOR_TOTAL_PAGOS),',
'                                     pn_vuelto => :p134_vuelto,',
'                                     pn_emp_id => :f_emp_id,',
'                                     pv_error => :p0_error);',
'',
':P134_SEQ_ID := NULL;',
':P134_MCD_VALOR_MOVIMIENTO := NULL;',
':P134_TFP_ID_ELIMINAR := NULL;',
':P134_MODIFICACION := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>739364505073437671
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771618059749202597)
,p_process_sequence=>80
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_colec_cuota_cero'
,p_process_sql_clob=>'pq_ven_pagos_cuota.pr_carga_colec_cuota_cero(:P134_CLI_ID,:f_emp_id,:P134_ORD_ID,:p0_error);'
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P134_FACTURAR = ''S'' AND',
':P134_COM_ID IS NULL'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739364908479437671
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771616459597202595)
,p_process_sequence=>85
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_existe_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota.pr_valida_existe_folios(pn_emp_id => :F_EMP_ID,',
'                        pn_cli_id => :P134_CLI_ID,',
'                        pn_gasto  => ''IM'',',
'                        pv_error  => :P0_ERROR);',
'',
'pq_ven_pagos_cuota.pr_valida_existe_folios(pn_emp_id => :F_EMP_ID,',
'                        pn_cli_id => :P134_CLI_ID,',
'                        pn_gasto  => ''GC'',',
'                        pv_error  => :P0_ERROR);',
'',
'IF :P0_ERROR is not null then',
'  :p134_ini := NULL;',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P0_ERROR is null and :p134_ini = ''INI'' and :P134_VALIDA_PUNTO = ''P'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739363308327437669
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771618267491202598)
,p_process_sequence=>90
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_factura_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  LN_AGE_ID_AGENTE   ASDM_AGENTES.AGE_ID%TYPE;',
'  LN_MCA_ID          ASDM_MOVIMIENTOS_CAJA.MCA_ID%TYPE;',
'  LN_REGISTROS       NUMBER;',
'  LN_COM_ID          VEN_COMPROBANTES.COM_ID%TYPE;',
'  LN_RAP_ID          ASDM_RETENCIONES_APLICADAS.RAP_ID%TYPE; -- Yguaman 2011/10/05 11:40am usado para pago con retencion ',
'  LN_TTR_ID_PAGO     ASDM_TIPOS_TRANSACCIONES.TTR_ID%TYPE;',
'  LN_TTR_ID_CAR      ASDM_TIPOS_TRANSACCIONES.TTR_ID%TYPE;',
'  LN_FOL_SEC_ACTUAL  VEN_COMPROBANTES.COM_NUMERO%TYPE;',
'  LN_UGE_NUM_EST_SRI VEN_COMPROBANTES.UGE_NUM_SRI%TYPE;',
'  LN_PUE_NUM_SRI     VEN_COMPROBANTES.PUE_NUM_SRI%TYPE;',
'  LN_PUE_ID          VEN_COMPROBANTES.PUE_NUM_SRI%TYPE;',
'  LN_NRO_FOLIO       ASDM_FOLIOS.FOL_NRO_FOLIO%TYPE;',
'  LN_SEG_ID          NUMBER;',
'  LN_PUNTOS_CANCELAR NUMBER(17, 4);',
'  LN_TVE_ID          NUMBER := PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                                                  ''cn_tve_id_regalos'');',
'  LV_ERROR           VARCHAR2(4000);',
'  ERR EXCEPTION;',
'  LN_VALOR_PAGO          NUMBER;',
'  LN_CAR_ID              CAR_ABONOS_REFINANCIAMIENTO.CAR_ID%TYPE;',
'  LV_ABONO_RENEGOCIACION CAR_RENEGOCIACION%ROWTYPE;',
'  LN_CXC_ID_REF          NUMBER;',
'  LN_INTERES_MORA        NUMBER := 0;',
'  LN_GASTO_COB           NUMBER := 0;',
'  LV_VALIDA_PISTOLA      VARCHAR2(10);',
'  LN_CANTIDAD_LEER       NUMBER;',
'  LN_CANTIDAD_LEIDOS     NUMBER;',
'  LN_CON_FORMAS_PAGO     NUMBER;',
'  LV_TIPO                VARCHAR2(5);',
'  LN_CONT_CXC_GOBIERNO   NUMBER;',
'  LV_RESULTADO           VARCHAR2(3000);',
'  LV_VALOR               VARCHAR2(3000);',
'  LV_OBSERVACION         VARCHAR2(3000);',
'',
'  LN_VALOR_A_PAGAR NUMBER;',
'',
'BEGIN',
'  --- JANDRESO 18/02/2015',
'  BEGIN',
'    SELECT NVL(MAX(TO_NUMBER(CO.C005)), 0)',
'      INTO LN_CON_FORMAS_PAGO',
'      FROM APEX_COLLECTIONS CO',
'     WHERE CO.COLLECTION_NAME = ''CO_MOV_CAJA''',
'       AND TO_NUMBER(CO.C005) NOT IN (16, 4, 26, 22)',
'     GROUP BY TO_NUMBER(CO.C005)',
'    HAVING COUNT(TO_NUMBER(CO.C005)) > 1;',
'  ',
'    IF LN_CON_FORMAS_PAGO > 0 THEN',
'      RAISE_APPLICATION_ERROR(-20000,',
'                              ''No se debe repetir la forma de pago en el detalle del movimiento de caja'');',
'    END IF;',
'  EXCEPTION',
'    WHEN NO_DATA_FOUND THEN',
'      NULL;',
'  END;',
'',
'  SELECT SUM(TO_NUMBER(TO_NUMBER(C006)))',
'    INTO LN_VALOR_PAGO',
'    FROM APEX_COLLECTIONS',
'   WHERE COLLECTION_NAME =',
'         PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL, ''cv_coleccion_mov_caja'');',
'',
'  SELECT AB.REN_TOTAL_INTERESES',
'    INTO LN_VALOR_A_PAGAR',
'    FROM CAR_RENEGOCIACION AB',
'   WHERE AB.REN_ID = :P134_REN_ID',
'     AND AB.REN_ESTADO_REGISTRO = 0;',
'',
'  IF LN_VALOR_PAGO < LN_VALOR_A_PAGAR THEN',
'    RAISE_APPLICATION_ERROR(-20000,',
'                            ''El valor a cancelar no cubre el valor de los intereses para poder realizar la renegociacion'');',
'  ELSE',
'  ',
'    BEGIN',
'      SELECT *',
'        INTO LV_ABONO_RENEGOCIACION',
'        FROM CAR_RENEGOCIACION AB',
'       WHERE AB.REN_ID = :P134_REN_ID',
'         AND AB.REN_ESTADO_REGISTRO = 0;',
'    ',
'',
'',
'',
'      PQ_CAR_DML.PR_UPD_CAR_RENEGOCIACION(PN_REN_ID                     => :P134_REN_ID,',
'                                          PN_CLI_ID                     => LV_ABONO_RENEGOCIACION.CLI_ID,',
'                                          PD_REN_FECHA                  => LV_ABONO_RENEGOCIACION.REN_FECHA,',
'                                          PN_REN_TOTAL_CAPITAL          => LV_ABONO_RENEGOCIACION.REN_TOTAL_CAPITAL,',
'                                          PN_REN_TOTAL_INTERESES        => LV_ABONO_RENEGOCIACION.REN_TOTAL_INTERESES,',
'                                          PN_REN_TOTAL_ABONO            => LN_VALOR_PAGO,',
'                                          PN_REN_TOTAL_DIFERENCIA       => LV_ABONO_RENEGOCIACION.REN_TOTAL_INTERESES-(LV_ABONO_RENEGOCIACION.REN_TOTAL_ABONO +',
'                                                                           LN_VALOR_PAGO),',
'                                          PN_REN_ESTADO_REGISTRO        => LV_ABONO_RENEGOCIACION.REN_ESTADO_REGISTRO,',
'                                          PN_REN_TOTAL_SALDO_CXC_ANT    => LV_ABONO_RENEGOCIACION.REN_TOTAL_SALDO_CXC_ANT,',
'                                          PN_REN_TOTAL_INTERESES_NO_PAG => LV_ABONO_RENEGOCIACION.REN_TOTAL_INTERESES_NO_PAG,',
'                                          PN_REN_TOTAL_ABONOS_CAPITAL   => LV_ABONO_RENEGOCIACION.REN_TOTAL_ABONOS_CAPITAL,',
'                                          PV_REN_ACTIVADA               => LV_ABONO_RENEGOCIACION.REN_ACTIVADA,',
'                                          PV_ERROR                      => :P0_ERROR);',
'    ',
'    EXCEPTION',
'      WHEN NO_DATA_FOUND THEN',
'        NULL;',
'    END;',
'  END IF;',
'',
'  LN_TTR_ID_PAGO := PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,',
'                                                       ''cn_ttr_id_pago_cuota'');',
'  LN_TTR_ID_CAR  := PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,',
'                                                       ''cn_ttr_id_credito_car_pago_cuota'');',
'',
'  SELECT M.TSE_ID',
'    INTO LN_SEG_ID',
'    FROM ASDM_AGENCIAS M',
'   WHERE M.AGE_ID = :F_AGE_ID_AGENCIA',
'     AND M.EMP_ID = :F_EMP_ID;',
'',
'  IF :P0_ERROR IS NULL THEN',
unistr('    --- Yessica Guam\00C3\00A1n 211/11/16. Debe grabarse los datos de la retenci\00C3\00B3n, primero ya que se reemplaza el dato de # de retenci\00C3\00B3n por el id de la retenci\00C3\00B3n para continuar'),
'    LV_ERROR := 100;',
'    PQ_VEN_PAGOS_CUOTA.PR_GRABA_PAGO_RETENCION(PN_EMP_ID   => :F_EMP_ID,',
'                                               PV_FACTURAR => :P134_FACTURAR,',
'                                               PV_ERROR    => :P0_ERROR);',
'  ',
'    LV_ERROR := ''pr_graba_pago_retencion'';',
'    IF :P0_ERROR IS NOT NULL THEN',
'      RAISE ERR;',
'    END IF;',
'  ',
'    LN_AGE_ID_AGENTE := PQ_CAR_CREDITO_INTERFAZ.FN_RETORNA_AGENTE_CONECTADO(:F_USER_ID,',
'                                                                            :F_EMP_ID);',
'  ',
'  ',
'    LV_ERROR := ''pr_graba_pago_cuota'';',
'    PQ_CAR_RENEGOCIACION.PR_GRABA_PAGO_CUOTA(PN_MCA_ID                => LN_MCA_ID,',
'                                           PN_EMP_ID                => :F_EMP_ID,',
'                                           PN_UGE_ID                => :F_UGE_ID,',
'                                           PN_UGE_ID_GASTO          => :F_UGE_ID_GASTO,',
'                                           PN_USER_ID               => :F_USER_ID,',
'                                           PN_PCA_ID                => :F_PCA_ID,',
'                                           PN_AGE_ID_GESTIONADO_POR => LN_AGE_ID_AGENTE, --:p134_age_id_gestionado_por,',
'                                           PN_CLI_ID                => :P134_CLI_ID,',
'                                           PN_MCA_TOTAL             => :P134_SUM_PAGOS,',
'                                           PN_TSE_ID                => :F_SEG_ID,',
'                                           PV_APP_USER              => :APP_USER,',
'                                           PV_SESSION               => :SESSION,',
'                                           PV_TOKEN                 => :F_TOKEN,',
'                                           PN_ROL                   => :P0_ROL,',
'                                           PV_ROL_DESC              => :P0_ROL_DESC,',
'                                           PN_TREE_ROT              => :P0_TREE_ROOT,',
'                                           PN_OPCION                => :F_OPCION_ID,',
'                                           PV_PARAMETRO             => :F_PARAMETRO,',
'                                           PV_POPUP                 => :F_POPUP,',
'                                           PV_EMPRESA               => :F_EMPRESA,',
'                                           PV_EMP_LOGO              => :F_EMP_LOGO,',
'                                           PV_EMP_FONDO             => :F_EMP_FONDO,',
'                                           PV_UGESTION              => :F_UGESTION,',
'                                           PN_AGE_ID_AGENTE         => LN_AGE_ID_AGENTE,',
'                                           PN_AGE_ID_AGENCIA        => :F_AGE_ID_AGENCIA,',
'                                           PN_TTR_ID_ORIGEN         => LN_TTR_ID_PAGO, --- pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_pago_cuota'')',
'                                           PN_TTR_ID_CARTERA        => LN_TTR_ID_CAR, --- pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_credito_car_pago_cuota'')',
'                                           PN_MCA_ID_REVERSAR       => NULL, -- yguaman 2012/01/11',
'                                           PN_REN_ID                => :P134_REN_ID,',
'                                           PV_ERROR                 => :P0_ERROR);',
'',
'  ',
'    IF :P0_ERROR IS NOT NULL THEN',
'      RAISE ERR;',
'    END IF;',
'    --  *****JARIAS 23/02/2011 CXC TARJETAS DE CREDITO*****',
'    BEGIN',
'      LV_ERROR := ''pr_genera_cxc_tarjetas***'';',
'      IF :P134_ES_POLITICA_GOBIERNO = 0 THEN',
'        LV_TIPO := ''TJ'';',
'      ELSIF :P134_ES_POLITICA_GOBIERNO = 1 THEN',
'        IF :P134_TVE_ID =',
'           PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,',
'                                              ''cn_tve_id_contado'') THEN',
'          LV_TIPO := ''TJ'';',
'        ELSE',
'          LV_TIPO := ''GO'';',
'        END IF;',
'      END IF;',
'      PQ_CAR_CREDITO.PR_GENERA_CARTERA_PRO(PN_EMP_ID       => :F_EMP_ID,',
'                                           PN_MCA_ID       => LN_MCA_ID,',
'                                           PN_UGE_ID       => :F_UGE_ID,',
'                                           PN_USU_ID       => :F_USER_ID,',
'                                           PN_UGE_ID_GASTO => :F_UGE_ID_GASTO,',
'                                           PN_TVE_ID       => PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,',
'                                                                                                 ''cn_tve_id_credito_propio''),',
'                                           PN_TSE_ID       => :F_SEG_ID,',
'                                           PV_TIPO         => LV_TIPO,',
'                                           PN_CLI_ID       => :P134_CLI_ID,',
'                                           PN_COM_ID       => LN_COM_ID,',
'                                           PV_ERROR        => :P0_ERROR);',
'    ',
'      IF :P134_ES_POLITICA_GOBIERNO = 1 AND',
'         :P134_TVE_ID !=',
'         PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID, ''cn_tve_id_contado'') THEN',
'      ',
'        SELECT COUNT(*)',
'          INTO LN_CONT_CXC_GOBIERNO',
'          FROM CAR_CUENTAS_POR_COBRAR CX',
'         WHERE CX.COM_ID = LN_COM_ID',
'           AND CX.POL_ID IN',
'               (SELECT POL_ID',
'                  FROM PPR_POLITICAS_FACTORES PF',
'                 WHERE PF.FAC_ID =',
'                       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,',
'                                                          ''cn_fac_id_valida_pol_gobierno'')',
'                   AND PF.PFA_ESTADO_REGISTRO =',
'                       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                                          ''cv_estado_reg_activo''));',
'        IF LN_CONT_CXC_GOBIERNO != 2 THEN',
'          RAISE_APPLICATION_ERROR(-20000,',
'                                  ''La cartera de Induccion se esta Generando incorrectamente por favor Revisar'');',
'        END IF;',
'      END IF;',
'    ',
'      IF :P0_ERROR IS NOT NULL THEN',
'        RAISE ERR;',
'      END IF;',
'    END;',
'  ',
'    IF :P0_ERROR IS NULL THEN',
'      LV_ERROR := ''promociones.aplica_promocion'';',
'      PQ_ASDM_PROMOCIONES.PR_APLICA_PROMOCION(PN_EMP_ID           => :F_EMP_ID,',
'                                              PN_PRO_ID           => PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,',
'                                                                                                        ''cn_pro_id_promocion_pr''),',
'                                              PN_ORD_ID           => :P134_ORD_ID,',
'                                              PN_CLI_ID           => :P134_CLI_ID,',
'                                              PN_UGE_ID           => :F_UGE_ID,',
'                                              PN_USU_ID           => :F_USER_ID,',
'                                              PN_UGE_ID_GASTO     => :F_UGE_ID_GASTO,',
'                                              PN_AGE_ID_AGENTE    => LN_AGE_ID_AGENTE,',
'                                              PN_AGE_ID_AGENCIA   => :F_AGE_ID_AGENCIA,',
'                                              PN_SECUENCIA_ACTUAL => :P134_SECUENCIA_ACTUAL,',
'                                              PN_NRO_FOLIO        => :P0_NRO_FOLIO,',
'                                              PV_ERROR            => :P0_ERROR);',
'    ',
'      IF :P0_ERROR IS NOT NULL THEN',
'        RAISE ERR;',
'      END IF;',
'    END IF;',
'    --aplica promocion puntaje',
'    IF :P0_ERROR IS NULL THEN',
'      LV_ERROR := ''promociones.aplica_puntos'';',
'      PQ_ASDM_PROMOCIONES.PR_APLICA_PUNTOS(PN_EMP_ID           => :F_EMP_ID,',
'                                           PN_PRO_ID           => PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,',
'                                                                                                     ''cn_pro_id_promocion_pr''),',
'                                           PN_ORD_ID           => :P134_ORD_ID,',
'                                           PN_PER_ID           => :P134_CLI_ID,',
'                                           PN_UGE_ID           => :F_UGE_ID,',
'                                           PN_USU_ID           => :F_USER_ID,',
'                                           PN_UGE_ID_GASTO     => :F_UGE_ID_GASTO,',
'                                           PN_AGE_ID_AGENTE    => LN_AGE_ID_AGENTE,',
'                                           PN_AGE_ID_AGENCIA   => :F_AGE_ID_AGENCIA,',
'                                           PN_SECUENCIA_ACTUAL => :P134_SECUENCIA_ACTUAL,',
'                                           PN_NRO_FOLIO        => :P0_NRO_FOLIO,',
'                                           PV_ERROR            => :P0_ERROR);',
'    ',
'      --***',
'      IF NVL(:P134_PUNTOS_USADOS, :P134_PUNTOS_CANCELAR) IS NOT NULL THEN',
'        BEGIN',
'          IF :P134_PUNTOS_USADOS IS NULL THEN',
'            LN_PUNTOS_CANCELAR := :P134_PUNTOS_CANCELAR;',
'          ELSE',
'            LN_PUNTOS_CANCELAR := :P134_PUNTOS_USADOS;',
'          END IF;',
'          PQ_VEN_OTROS_DML.PR_ACT_DETALLE_PROMOCION(:P134_PER_ID_PUNTOS,',
'                                                    NVL(:P134_PUNTAJE_FIJO,',
'                                                        ''S''),',
'                                                    LN_PUNTOS_CANCELAR, --NVL(:P134_PUNTOS_USADOS,:P134_PUNTOS_CANCELAR),',
'                                                    :P134_PRO_ID,',
'                                                    :P134_PER_ID,',
'                                                    :P134_COM_ID,',
'                                                    :P134_PTA_ID,',
'                                                    :P134_PDI_ID,',
'                                                    :P0_ERROR);',
'        EXCEPTION',
'          WHEN OTHERS THEN',
'            LV_ERROR := ''PQ_VEN_OTROS_DML.PR_ACT_DETALLE_PROMOCION'';',
'            RAISE_APPLICATION_ERROR(-20001,',
'                                    ''XXXXXXXX XXXXXX XXXXXX.'' || SQLERRM);',
'        END;',
'      END IF;',
'      IF :P0_ERROR IS NOT NULL THEN',
'        RAISE ERR;',
'      END IF;',
'    END IF;',
'    IF (:P134_PRO_ID IS NOT NULL OR NVL(:P134_PUNTOS_CANCELAR, 0) > 0) AND',
'       :P134_FACTURAR = ''S'' THEN',
'      PQ_VEN_COMPROBANTES.PR_IMPRIMIR_ACTA_REFERIDOR(:F_EMP_ID,',
'                                                     :P134_COM_ID);',
'    END IF;',
'    --***Ma. Paula Barros 17/12/2011 Proceso para aplicar el valor de un puntualito devuelto  ***--',
'    IF :P0_ERROR IS NULL THEN',
'      LV_ERROR := ''pr_aplica_promo_dev'';',
'      PQ_ASDM_PROMOCIONES.PR_APLICA_PROMO_DEV(PN_EMP_ID           => :F_EMP_ID,',
'                                              PN_PRO_ID           => PQ_CONSTANTES.FN_RETORNA_CONSTANTE(:F_EMP_ID,',
'                                                                                                        ''cn_pro_id_promocion_pr''),',
'                                              PN_ORD_ID           => :P134_ORD_ID,',
'                                              PN_CLI_ID           => :P134_CLI_ID,',
'                                              PN_UGE_ID           => :F_UGE_ID,',
'                                              PN_USU_ID           => :F_USER_ID,',
'                                              PN_UGE_ID_GASTO     => :F_UGE_ID_GASTO,',
'                                              PN_AGE_ID_AGENTE    => LN_AGE_ID_AGENTE,',
'                                              PN_AGE_ID_AGENCIA   => :F_AGE_ID_AGENCIA,',
'                                              PN_SECUENCIA_ACTUAL => :P134_SECUENCIA_ACTUAL,',
'                                              PN_PCA_ID           => :F_PCA_ID,',
'                                              PV_ERROR            => :P0_ERROR);',
'    ',
'      IF :P0_ERROR IS NOT NULL THEN',
'        RAISE ERR;',
'      END IF;',
'    END IF;',
unistr('    --- Yessica Guam\00C3\00A1n 211/11/23. En caso de existir saldos de la retenci\00C3\00B3n del pago de cuota estos se grabaran como anticipo de clientes'),
'    BEGIN',
'      PQ_VEN_PAGOS_CUOTA.PR_GRABA_ANTICIPO_RETENCION(PN_EMP_ID => :F_EMP_ID,',
'                                                     --- Para anticipo de clientes',
'                                                     PN_UGE_ID                => :F_UGE_ID,',
'                                                     PN_UGE_ID_GASTO          => :F_UGE_ID_GASTO,',
'                                                     PN_USU_ID                => :F_USER_ID,',
'                                                     PN_PCA_ID                => :F_PCA_ID,',
'                                                     PN_AGE_ID_GESTIONADO_POR => LN_AGE_ID_AGENTE,',
'                                                     PN_CLI_ID                => :P134_CLI_ID,',
'                                                     PN_REVERSO               => ''N'', -- yguaman 2011/12/27',
'                                                     PV_ERROR                 => :P0_ERROR);',
'    ',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        ROLLBACK;',
'        RAISE_APPLICATION_ERROR(-20000, ''error '' || SQLERRM);',
'    END;',
'  ',
'    IF :P0_ERROR IS NOT NULL THEN',
'      RAISE ERR;',
'    END IF;',
unistr('    ------- Fin - Yessica Guam\00C3\00A1n 211/11/23. '),
'  ',
'    :P134_MCD_VALOR_MOVIMIENTO := NULL;',
'    :P134_VUELTO               := NULL;',
'  ',
'    PQ_INV_MOVIMIENTOS.PR_ELIMINA_COLECCIONES(PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,',
'                                                                                 ''cv_coleccion_mov_caja''));',
'    PQ_INV_MOVIMIENTOS.PR_ELIMINA_COLECCIONES(PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,',
'                                                                                 ''cv_coleccion_pago_cuota''));',
'  ',
'  END IF;',
'',
'  IF :P0_ERROR IS NULL THEN',
'    IF NVL(:P134_PAGO_REFINANCIAMIENTO, 0) = 1 THEN',
'      PQ_VEN_COMPROBANTES.PR_REDIRECT(PV_DESDE        => ''R'',',
'                                      PN_APP_ID       => NULL,',
'                                      PN_PAG_ID       => NULL,',
'                                      PN_EMP_ID       => :F_EMP_ID,',
'                                      PV_SESSION      => :SESSION,',
'                                      PV_TOKEN        => :F_TOKEN,',
'                                      PN_USER_ID      => :F_USER_ID,',
'                                      PN_ROL          => :P0_ROL,',
'                                      PV_ROL_DESC     => :P0_ROL_DESC,',
'                                      PN_TREE_ROT     => :P0_TREE_ROOT,',
'                                      PN_OPCION       => :F_OPCION_ID,',
'                                      PV_PARAMETRO    => :F_PARAMETRO,',
'                                      PV_EMPRESA      => :F_EMPRESA,',
'                                      PN_UGE_ID       => :F_UGE_ID,',
'                                      PN_UGE_ID_GASTO => :F_UGE_ID_GASTO,',
'                                      PV_UGESTION     => :F_UGESTION,',
'                                      PV_ERROR        => :P0_ERROR);',
'    END IF;',
'  END IF;',
'',
'  --  pq_ven_promociones_comunes.pr_generar_promociones(ln_com_id, :f_emp_id);',
'  IF :P134_FACTURAR != ''S'' THEN',
'    LV_ERROR   := ''final'';',
'    :P134_INI  := ''FIN'';',
'    :P134_INI2 := ''FIN2'';',
'  END IF;',
'',
'EXCEPTION',
'  WHEN ERR THEN',
'    LV_ERROR := :P0_ERROR || '' ERROR EN APEX 1:'' || SQLERRM;',
'    ROLLBACK;',
'    RAISE_APPLICATION_ERROR(-20000, LV_ERROR);',
'  WHEN OTHERS THEN',
'    LV_ERROR := SQLERRM;',
'  ',
'    :P134_INI  := ''FIN'';',
'    :P134_INI2 := ''FIN2'';',
'    :P0_ERROR  := :P0_ERROR || '' ERROR EN APEX 2:'' || LV_ERROR || SQLERRM ||',
'                  '' *** '' || LV_ERROR;',
'    ROLLBACK;',
'    RAISE_APPLICATION_ERROR(-20000, :P0_ERROR);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'pr_graba_factura_pago_cuota'
,p_process_when=>':p134_ini = ''INI'' and :p134_com_id is null '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>'REALIZO'
,p_internal_uid=>739365116221437672
,p_process_comment=>'and :request = ''Graba_fac_pag'''
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771619479053202607)
,p_process_sequence=>170
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_buro_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'lv_resultado varchar2(3000);',
'lv_valor varchar2(3000);',
'lv_observacion varchar2(3000);',
'',
'',
'begin',
'',
'IF :f_emp_id= 1 and    :P134_CLI_ID <> 239062 THEN --etenesaca valida todos excepto cliente marcimex',
'  /*pq_car_analisis_cliente.pr_buro_interno_cliente(pn_cli_id      => :P134_CLI_ID,',
'                                                  pn_emp_id      => :f_emp_id,',
'                                                  pn_ttr_id      => pq_constantes.fn_retorna_constante(0,',
'                                                                                                       ''cn_ttr_id_debito_car_facturacion''),',
'                                                  pb_resultado   => lv_resultado,',
'                                                  pv_valor       => lv_valor,',
'                                                  pv_observacion => lv_observacion,',
'                                                  pn_tse_id      => :f_seg_id,',
'                                                  pn_ord_id      => :P134_ORD_ID,',
'                                                pv_error       => :p0_error);             */',
'                                                ',
'                                                ',
'      ',
unistr(' -----JALVAREZ 17JUN2022 SE LLAMA AL NUEVO PROCESO DE SEGMENTACI\00D3N DEL CLIENTE '),
' /*',
' no se debe perfilar ya que no se afecta si el cliente paga o factura depende solo del modelo de equifax',
'  pq_car_segmentacion_cliente.pr_valida_equifax(pn_emp_id => :F_EMP_ID,',
'                                              pn_cli_id => :P134_CLI_ID,',
'                                              pn_uge_id => :F_UGE_ID,',
'                                              pn_usu_id => :F_USER_ID,',
'                                              pn_linea => null,',
'                                              pn_monto => null,',
'                                              pv_error  => :P0_ERROR);  */     null;                                                    ',
'           END IF;  ',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P134_FACTURAR = ''S'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739366327783437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771620464675202607)
,p_process_sequence=>170
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_estado_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_com_id number;',
'  ln_cli_id number;',
'',
'  cv_estado_reg_activo varchar2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                         ''cv_estado_reg_activo'');',
'  lv_error             varchar2(3000);',
'begin',
'',
'  select co.cli_id, co.com_id',
'    into ln_cli_id, ln_com_id',
'    from ven_comprobantes co',
'   where co.ord_id = :p134_ord_id',
'     and co.com_estado_registro = cv_estado_reg_activo;',
'',
'  pq_car_cartera.pr_estado_cliente_venta(pn_com_id => ln_com_id,',
'                                         pn_cli_id => ln_cli_id,',
'                                         pn_usu_id => :f_user_id,',
'                                         pn_emp_id => :f_emp_id,',
'                                         pn_uge_id => :f_uge_id,',
'                                         pn_ord_id => :p134_ord_id,',
'                                         pv_error  => :p0_error);',
'',
'  :p134_ini  := ''FIN'';',
'  :p134_ini2 := ''FIN2'';',
'',
'exception',
'  when no_data_found then',
'    null;',
'  when others then',
'  ',
'    :p134_ini  := ''FIN'';',
'    :p134_ini2 := ''FIN2'';',
'    lv_error  := sqlerrm;',
'    raise_application_Error(-20000, :p0_error || '' - '' || lv_error);',
'  ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p134_facturar = ''S'' AND :p134_ini = ''INI'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739367313405437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771617865836202597)
,p_process_sequence=>500
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'BEGIN',
'',
':P0_TTR_DESCRIPCION := NULL;',
':P0_DATFOLIO := NULL;',
':P0_FOL_SEC_ACTUAL := NULL;',
':P0_PERIODO := null;',
'',
'IF :P134_FACTURAR = ''S'' THEN',
'   :P0_TTR_ID := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id); ',
'   ln_tgr_id  := pq_ven_listas_caja.fn_tipo_grupo_transaccion_seg (:f_emp_id, :f_seg_id); ',
'ELSE',
'   ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja'');',
'   :P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota'');',
'',
'   -- Yguaman 2012/01/23. Para precancelacion',
'   IF NVL(:P134_ES_PRECANCELACION,''N'') = ''S'' THEN',
'       :P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota_precancelacion''); ',
'   END IF; ',
'END IF;',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'                       :F_EMP_ID,',
'                       :F_PCA_ID,',
'                        LN_TGR_ID,',
'                       :P0_TTR_DESCRIPCION, ',
'                       :P0_PERIODO,',
'                       :P0_DATFOLIO,',
'                       :P0_FOL_SEC_ACTUAL,',
'                       :P0_PUE_NUM_SRI,',
'                       :P0_UGE_NUM_EST_SRI,',
'                       :P0_PUE_ID,',
'                       :P0_TTR_ID,',
'                       :P0_NRO_FOLIO,',
'                       :P0_ERROR);',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739364714566437671
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771616683465202595)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_anticipo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--- VALIDA QUE EL VALOR A PAGAR CON ANTICIPO NO SOBREPASE EL TOTAL DE ANTICIPOS',
'',
'  pq_ven_pagos_cuota.pr_validar_pago_anticipo(pn_emp_id => :F_EMP_ID,',
'                                     pn_cli_id => :P134_CLI_ID, ',
'                                     pn_antic_ing => :P134_MCD_VALOR_MOVIMIENTO,',
'                                     pn_uge_id  => :f_uge_id,--agregado ac',
'                                     pv_error  => :P0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and',
':P134_TFP_ID =pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'') and :P0_ERROR is null'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739363532195437669
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771618881405202606)
,p_process_sequence=>3
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_subtotal'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_subtotal NUMBER;',
'  ln_flete    NUMBER;',
'  ',
'  cn_var_id_subtotal_con_iva NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                               ''cn_var_id_subtotal_con_iva'');',
'                                               ',
'  cn_var_id_subtotal_sin_iva NUMBER :=    pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                               ''cn_var_id_subtotal_sin_iva'');       ',
'                                               ',
' cn_var_id_ser_manejo_produ  NUMBER :=     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_var_id_servicio_manejo_producto'');                                                                                   ',
'',
'BEGIN',
'',
'  BEGIN',
'  ',
'    SELECT SUM(a.vor_valor_variable)',
'      INTO ln_subtotal',
'      FROM ven_var_ordenes a',
'     WHERE a.ord_id = :P134_ord_id',
'       AND a.emp_id = :f_emp_id',
'       AND a.var_id IN',
'           (cn_var_id_subtotal_con_iva,',
'            cn_var_id_subtotal_sin_iva);',
'  ',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ln_subtotal := 0;',
'  END;',
'',
'  BEGIN',
'  ',
'    SELECT a.vor_valor_variable',
'      INTO ln_flete',
'      FROM ven_var_ordenes a',
'     WHERE a.ord_id = :P134_ord_id',
'       AND a.emp_id = :f_emp_id',
'       AND a.var_id = cn_var_id_ser_manejo_produ;',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ln_flete := 0;',
'  END;',
'',
'  --raise_application_error(-20000, ''ln_subtotal: '' || ln_subtotal || '' ln_flete: '' || ln_flete);',
'',
'  :P134_SUBTOTAL := nvl(ln_subtotal, 0) - nvl(ln_flete, 0);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'limpia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>739365730135437680
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771615258304202592)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   ln_comprobante ven_comprobantes.com_id%TYPE;',
'',
'begin',
'',
'IF :P134_FACTURAR = ''S'' THEN',
'      ln_comprobante := :P134_ORD_ID;',
'ELSE',
'      ln_comprobante := :P134_RAP_COM_ID;',
'END IF;',
'',
'--pruebas_car_aux(pn_number =>:P134_MCD_VALOR_MOVIMIENTO ,pv_dato => '':P134_MCD_VALOR_MOVIMIENTO: '' || :P134_MCD_VALOR_MOVIMIENTO);',
'',
'pq_ven_pagos_cuota.pr_valida_pago_retencion(pn_nro_retencion        => :P134_RAP_NRO_RETENCION,',
'                         pn_emp_id               => :F_EMP_ID,',
'                         pn_com_id               => ln_comprobante,',
'                         pv_facturar             => :P134_FACTURAR, --- Para ver si viene de la facturacion o del pago de cuota                  ',
'                         pv_RAP_NRO_ESTABL_RET   => :P134_RAP_NRO_ESTABL_RET ,',
'                         pv_RAP_NRO_PEMISION_RET => :P134_RAP_NRO_PEMISION_RET,',
'                         pn_saldo_retencion      => :P134_SALDO_RETENCION,',
'                         pn_mcd_valor_movimiento => :P134_MCD_VALOR_MOVIMIENTO,',
'                         pn_valor_total_pagos    => :P134_VALOR_TOTAL_PAGOS, -- Usado cuando viene de facturacion',
'                         pv_modificacion         => :P134_MODIFICACION,',
'                         pn_pre_id               => :P134_PRE_ID,',
'                         pv_dejar_saldo          => ''S'',',
'                         PV_RAP_NRO_AUTORIZACION => :P134_RAP_NRO_AUTORIZACION,',
'                         pv_error                => :P0_error);',
'',
'/*exception',
'when others then',
'  raise_application_error(-20000,''Error en la validacion'' || :P0_error);*/',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and :p134_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'') and :P0_ERROR is null'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739362107034437666
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771616854803202595)
,p_process_sequence=>6
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_total_a_pagar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_pagos_cuota.pr_validar_pago(pn_emp_id      => :F_emp_id,',
'                                   pn_seq_id      => :P134_SEQ_ID,',
'                                   pn_tfp_id      => :P134_TFP_ID,',
'                                   pn_valor_ing   => :P134_MCD_VALOR_MOVIMIENTO,',
'                                   pn_total_pagar => :P134_VALOR_TOTAL_PAGOS,',
'                                   pv_error       => :P0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739363703533437669
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771615467692202592)
,p_process_sequence=>8
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_Actualiza_Coleccion_Movimiento_Detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF NVL(:P134_ES_PRECANCELACION,''N'') <> ''S'' THEN',
'  --- Valida si tiene anticipo de clientes, a obligarlo a aplicar el pago con anticipo',
'  pq_ven_pagos_cuota.pr_advertencia_anticipos(pn_emp_id    => :f_emp_id,',
'                                              pn_cli_id    => :p134_cli_id,',
'                                              pn_antic_ing => :P134_MCD_VALOR_MOVIMIENTO,',
'                                              pn_tfp_id    => :P134_TFP_ID,',
'                                              pn_seq_id    => :p134_seq_id,',
'                                              pn_facturar  => :p134_facturar,',
'                                              pn_uge_id    => :f_uge_id,',
'                                              pv_error     => :p0_error);',
' END IF;',
'',
'  IF :p0_error IS NOT NULL THEN',
'   -- Porq no salia el error en pantalla',
'   raise_application_error(-20000, :p0_error);',
'  ELSE',
'    /*Agregado por Andres Calle',
'    07/septiembre/2011',
'    Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'    */',
'    IF :p134_tfp_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_tarjeta_credito'') THEN',
'      :p134_ede_id := :p134_plan;',
'    END IF;',
'  ',
'    /*Agregado por Andres Calle',
'    14/Octubre/2011',
'    valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'    */',
'    IF :p134_comision IS NOT NULL and :P134_ES_POLITICA_GOBIERNO = 0 THEN',
'      pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                  pn_ede_id   => :p134_ede_id,',
'                                                  pn_comision => :p134_comision,',
'                                                  pv_error    => :p0_error);',
'    END IF;',
'  ',
unistr('    ---Yguaman 2011/10/05 - 06 Se a\00C3\00B1adieron parametros en pq_ven_movimientos_caja.pr_actualiza_col_mov_caja para pago de cuota con retenci\00C3\00B3n'),
'  ',
'    pq_ven_movimientos_caja.pr_actualiza_col_mov_caja(pn_ede_id               => :p134_ede_id,',
'                                                      pn_cre_id               => :p134_cre_id, --null,',
'                                                      pn_tfp_id               => :p134_tfp_id,',
'                                                      pn_mcd_valor_movimiento => nvl(to_number(:p134_mcd_valor_movimiento),',
'                                                                                     0),',
'                                                      pn_mcd_nro_aut_ref      => :p134_mcd_nro_aut_ref,',
'                                                      pn_mcd_nro_lote         => :p134_lote,',
'                                                      pn_mcd_nro_retencion    => :p134_rap_nro_retencion,',
'                                                      pn_mcd_nro_comprobante  => :p134_mcd_nro_comprobante,',
'                                                      pv_titular_cuenta       => :p134_cre_titular_cuenta, -- :P134_TJ_TITULAR',
'                                                      pv_cre_nro_cheque       => :p134_cre_nro_cheque,',
'                                                      pv_cre_nro_cuenta       => :p134_cre_nro_cuenta,',
'                                                      pv_nro_pin              => :p134_cre_nro_pin,',
'                                                      pv_nro_tarjeta          => :p134_tj_numero,',
'                                                      pv_mcd_voucher          => :p134_tj_numero_voucher,',
'                                                      pn_seq_id               => :p134_seq_id,',
unistr('                                                      -----Para pago de cuota con retenci\00C3\00B3n'),
'                                                      pn_saldo_retencion      => nvl(to_number(:p134_saldo_retencion),',
'                                                                                     0), ---- yguaman 2011/10/05 ',
'                                                      pn_com_id               => :p134_rap_com_id, ---- yguaman 2011/10/05',
'                                                      pd_rap_fecha            => :p134_rap_fecha, --- Yguaman 2011/10/05 ',
'                                                      pd_rap_fecha_validez    => :p134_rap_fecha_validez, --- Yguaman 2011/10/05',
'                                                      pn_rap_nro_autorizacion => :p134_rap_nro_autorizacion, -- Yguaman 2011/10/05',
'                                                      pn_rap_nro_establ_ret   => :p134_rap_nro_establ_ret, -- Yguaman  2011/10/06',
'                                                      pn_rap_nro_pemision_ret => :p134_rap_nro_pemision_ret, -- Yguaman  2011/10/06',
'                                                      pn_pre_id               => :p134_pre_id, -- Yguaman  2011/10/06 ',
'                                                      pd_fecha_deposito       => :p134_mcd_fecha_deposito,',
'                                                      pn_emp_id               => :f_emp_id, --- Yguaman 2011/10/06  ',
'                                                      pv_titular_tarjeta_cre  => :p134_titular_tc, -- Andres Calle',
'                                                      pv_error                => :p0_error);',
'  ',
'    :p134_seq_id               := NULL;',
'    :p134_ede_id               := NULL;',
'    :p134_tfp_id               := NULL;',
'    :p134_mcd_valor_movimiento := NULL;',
'    :p134_tj_titular           := NULL;',
'    :p134_cre_nro_cheque       := NULL;',
'    :p134_cre_nro_cuenta       := NULL;',
'    :p134_cre_nro_pin          := NULL;',
'    :p134_mcd_nro_aut_ref      := NULL;',
'    :p134_tj_numero_voucher    := NULL;',
'    :p134_mcd_nro_comprobante  := NULL;',
'    :p134_tj_numero            := NULL;',
'    :p134_plan                 := NULL;',
'    :p134_mcd_fecha_deposito   := NULL;',
'  ',
'    -- NULL RETENCIONES ---',
'    :p134_rap_nro_retencion    := NULL; -- yguaman 2011/10/05 ',
'    :p134_saldo_retencion      := NULL; -- yguaman 2011/10/05 ',
'    :p134_rap_com_id           := NULL; -- yguaman 2011/10/05',
'    :p134_rap_fecha            := NULL; -- Yguaman 2011/10/05 ',
'    :p134_rap_fecha_validez    := NULL; -- Yguaman 2011/10/05',
'    :p134_rap_nro_autorizacion := NULL; -- yguaman 2011/10/05',
'    :p134_rap_nro_establ_ret   := NULL; --Yguaman  2011/10/06',
'    :p134_rap_nro_pemision_ret := NULL; --Yguaman  2011/10/06',
'    :p134_pre_id               := NULL; -- Yguaman  2011/10/06',
'  ',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P134_SEQ_ID IS NOT NULL and :request = ''cargar_tfp'' and :P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739362316422437666
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771520480365202484)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_imp.id(771514760434202467)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_PROMOCIONES_ORDENES'
,p_attribute_03=>'POR_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_imp.id(771517171098202471)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
,p_internal_uid=>739267329095437558
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771617483651202595)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar     NUMBER;',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
unistr('  ln_valor_retencion  asdm_retenciones_aplicadas.rap_valor_retencion%TYPE; -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n'),
'',
'  lv_col_val_retencion VARCHAR2(50) := ''COLL_VALOR_RETENCION'';',
'',
'  ln_cuenta  number;',
'  lv_anio    varchar2(4) := ''2011'';',
'  lv_mensaje varchar2(3000);',
'',
'  /*  --Agregado por Andres Calle',
'  --08/octubre/2012',
'  --voy a hacer el recalculo del puntualito en esta parte para probar si no da errores.',
'  cursor factura_puntualito is',
'  select a.com_id_fact_origen, a.cli_id, to_char(c.com_fecha,''YYYY'') anio, b.codigo_sucursal, b.secuencia_factura',
'    from asdm_movimientos_promociones a,',
'         asdm_homologacion_com        b,',
'         ven_comprobantes             c',
'   where b.emp_id = a.emp_id',
'     and b.com_id = a.com_id_fact_origen',
'     and c.com_id = b.com_id',
'     and c.emp_id = b.emp_id',
'     --and c.cli_id = b.cli_id',
'     --and b.cli_id = a.cli_id',
'     and a.mpr_saldo > 0',
'     and a.mpr_estado_registro = 0',
'     and a.cli_id = :P134_CLI_ID',
'     and a.emp_id = :f_emp_id',
'     and b.estado is null;*/',
'',
'  raise_puntaje exception;',
'',
'  lv_col_det_retencion VARCHAR2(50) := ''COLL_DET_RETENCION'';',
'',
'  CURSOR cur_datos_col IS',
'    SELECT to_number(co.c001) com_id,',
'           co.c002 fecha_ret,',
'           co.c003 fecha_validez,',
'           co.c004 nro_ret,',
'           co.c005 nro_autoriza,',
'           co.c006 nro_establecimiento,',
'           co.c007 nro_pemision,',
'           co.c008 porcentaje,',
'           co.c009 base,',
'           co.c010 valor_ret,',
'           to_number(co.c011) uge_id,',
'           to_number(co.c012) tse_id,',
'           blob001 xml',
'      FROM apex_collections co',
'     WHERE co.collection_name = lv_col_val_retencion;',
'',
'BEGIN',
'  ln_valor_retencion  := 0;',
'  ln_com_id_retencion := NULL;',
'',
'  IF :P134_MODIFICACION IS NULL THEN',
'  ',
'    if :P134_TFP_ID = 50 then',
'      if to_number(:P134_MCD_VALOR_MOVIMIENTO) >',
'         to_number(:P134_SALDO_PROMO_DEV) then',
'        :P134_MCD_VALOR_MOVIMIENTO := :P134_SALDO_PROMO_DEV;',
'      ',
'        raise raise_puntaje;',
'      ',
'      end if;',
'    ',
'    end if;',
'  ',
'    /*if :P134_TFP_ID = 18 and',
'       :F_SEG_ID =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_minoreo'') then',
'      for x in factura_puntualito loop',
'      ',
'        if apex_collection.collection_exists(p_collection_name => ''CO_VARIABLES_DETALLE'') then',
'          apex_collection.delete_collection(p_collection_name => ''CO_VARIABLES_DETALLE'');',
'        END IF;',
'      ',
'        if x.anio < lv_anio then',
'        ',
'          pq_ven_migracion.pr_actualiza_comprobante(pn_emp_id            => :f_emp_id,',
'                                                    pn_codigo_sucursal   => x.codigo_sucursal,',
'                                                    pn_secuencia_factura => x.secuencia_factura,',
'                                                    pv_error             => :p0_error);',
'        end if;',
'      ',
'        pq_ven_comprobantes.pr_recalculo_var_migracion(pn_com_id  => x.com_id_fact_origen,',
'                                                       pn_emp_id  => :f_emp_id,',
'                                                       pn_pol_id  => 61, --politica de migracion,',
'                                                       pv_mensaje => lv_mensaje,',
'                                                       pv_error   => :p0_error);',
'      end loop;',
'    end if;*/',
'  ',
'    /*Agregado por Andres Calle',
'    07/septiembre/2011',
'    Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'    */',
'    if :P134_TFP_ID =',
'       pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                          ''cn_tfp_id_tarjeta_credito'') then',
'      :p134_ede_id             := :p134_plan;',
'      :P134_CRE_TITULAR_CUENTA := :P134_TITULAR_TC; -- Para que se muestre en el mismo titular del cheque pero igual se guarde en el titular de TC',
'    end if;',
'  ',
'    /*Agregado por Andres Calle',
'    14/Octubre/2011',
'    valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'    */',
'    if :P134_TFP_ID =',
'       pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                          ''cn_tfp_id_tarjeta_credito'') and',
'       :P134_COMISION > 0 then',
'      pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                  pn_ede_id   => :p134_ede_id,',
'                                                  pn_comision => :p134_comision,',
'                                                  pv_error    => :p0_error);',
'    ',
'    end if;',
'  ',
'    IF NVL(:P134_ES_PRECANCELACION, ''N'') <> ''S''',
'      --- Yguaman 2012/03/28',
'       and :F_SEG_ID =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_minoreo'') THEN',
'      --- Valida si tiene anticipo de clientes, a obligarlo a aplicar el pago con anticipo',
'      pq_ven_pagos_cuota.pr_advertencia_anticipos(pn_emp_id    => :F_EMP_ID,',
'                                                  pn_cli_id    => :P134_CLI_ID,',
'                                                  pn_antic_ing => 0,',
'                                                  pn_tfp_id    => NULL,',
'                                                  pn_seq_id    => :P134_SEQ_ID,',
'                                                  pn_facturar  => :P134_FACTURAR,',
'                                                  pn_uge_id    => :f_uge_id,',
'                                                  pv_error     => :p0_error);',
'    ',
'    END IF;',
'    IF :p0_error IS NULL THEN',
'    ',
'      pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                      pn_tfp_id => :p134_tfp_id,',
'                                                      pv_error  => :p0_error);',
'    ',
'      IF :p0_error IS NULL THEN',
unistr('        -- Agregado por Yguaman 2011/11/17, porque a pesar del error de formas de pago igual graba en la colecci\00C3\00B3n movimientos'),
'      ',
unistr('        --- Yguaman 2011/10/04 11:59am a\00C3\00B1adido para guardar nueva forma de pago "retenci\00C3\00B3n"'),
'        IF :p134_tfp_id =',
'           pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                              ''cn_tfp_id_retencion'') THEN',
'        ',
'          ln_valor_retencion := :p134_mcd_valor_movimiento;',
'        ',
'          apex_collection.create_or_truncate_collection(lv_col_det_retencion);',
'        ',
'          FOR c IN cur_datos_col LOOP',
'          ',
'            apex_collection.add_member(p_collection_name => lv_col_det_retencion,',
'                                       p_c001            => c.com_id,',
'                                       p_c002            => c.fecha_ret,',
'                                       p_c003            => c.fecha_validez,',
'                                       p_c004            => c.nro_ret,',
'                                       p_c005            => c.nro_autoriza,',
'                                       p_c006            => c.nro_establecimiento,',
'                                       p_c007            => c.nro_pemision,',
'                                       p_c008            => c.porcentaje,',
'                                       p_c009            => c.base,',
'                                       p_c010            => c.valor_ret,',
'                                       p_c011            => c.uge_id,',
'                                       p_c012            => c.tse_id,',
'                                       p_blob001         => c.xml);',
'          ',
'          END LOOP;',
'        ',
'          /* begin       ',
'            -- Call the procedure             ',
'            pq_ven_pagos_cuota_retencion.pr_carga_retencion(pn_emp_id  => :f_emp_id,      ',
'                                                            pv_tipo    => ''P'',      ',
'                                                            pv_session => NULL,       ',
'                                                            pv_error   => :p0_error);       ',
'          end;*/',
'        ',
'          IF :p134_facturar = ''S'' THEN',
'            ln_com_id_retencion := :P134_ORD_ID; --:p134_com_id;',
'          ELSE',
'            ln_com_id_retencion := :P134_RAP_COM_ID;',
'          END IF;',
'        END IF;',
'      ',
'        --raise_application_error(-20000, '' :P134_MCD_VALOR_MOVIMIENTO = '' || :P134_MCD_VALOR_MOVIMIENTO || '' :P134_SALDO_RETENCION = '' || :P134_SALDO_RETENCION);',
'        --agregado por Andres Calle',
'        --voy a validar que el cheque que estan queriendo registrar no esta ya ingresado;',
'        select count(*)',
'          into ln_cuenta',
'          from asdm_cheques_recibidos r',
'         where r.cre_nro_cheque = NVL(:p134_cre_nro_cheque, 0)',
'           and r.cre_nro_cuenta = NVL(:p134_cre_nro_cuenta, 0)',
'           and r.cre_nro_pin = NVL(:p134_cre_nro_pin, 0)',
'           and r.ech_id = 2;',
'      ',
'        if ln_cuenta > 0 then',
'          raise_application_error(-20000,',
'                                  ''EL CHEQUE'' || :p134_cre_nro_cheque ||',
'                                  '' cuenta '' || :p134_cre_nro_cuenta ||',
'                                  '' pin '' || :p134_cre_nro_pin ||',
'                                  '' YA ESTA REGISTRADO NO PUEDE VOLVER A PAGAR CON EL MISMO CHEQUE'');',
'        END IF;',
'      ',
'        pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => :p134_ede_id,',
'                                                            pn_cre_id               => :p134_cre_id,',
'                                                            pn_tfp_id               => :p134_tfp_id,',
'                                                            pn_mcd_valor_movimiento => nvl(to_number(:P134_MCD_VALOR_MOVIMIENTO),',
'                                                                                           0), --:p134_mcd_valor_movimiento',
'                                                            pn_mcd_nro_aut_ref      => :P134_MCD_NRO_AUT_REF,',
'                                                            pn_mcd_nro_lote         => :P134_LOTE,',
'                                                            pn_mcd_nro_retencion    => :P134_RAP_NRO_RETENCION,',
'                                                            pn_mcd_nro_comprobante  => :P134_MCD_NRO_COMPROBANTE, --NULL,',
'                                                            pv_titular_cuenta       => :P134_CRE_TITULAR_CUENTA, --',
'                                                            pv_cre_nro_cheque       => :p134_cre_nro_cheque,',
'                                                            pv_cre_nro_cuenta       => :p134_cre_nro_cuenta,',
'                                                            pv_nro_pin              => :p134_cre_nro_pin,',
'                                                            pv_nro_tarjeta          => :p134_tj_numero,',
'                                                            pv_voucher              => :p134_tj_numero_voucher,',
'                                                            pn_saldo_retencion      => nvl(to_number(:P134_SALDO_RETENCION),',
unistr('                                                                                           0), --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_com_id               => ln_com_id_retencion, --:P134_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pd_rap_fecha            => :P134_RAP_FECHA, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pd_rap_fecha_validez    => :P134_RAP_FECHA_VALIDEZ, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_autorizacion => :P134_RAP_NRO_AUTORIZACION, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_RAP_NRO_ESTABL_RET   => :P134_RAP_NRO_ESTABL_RET, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_RAP_NRO_PEMISION_RET => :P134_RAP_NRO_PEMISION_RET, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_PRE_ID               => :P134_PRE_ID, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                            pd_fecha_deposito       => :P134_MCD_FECHA_DEPOSITO,',
unistr('                                                            --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                            pv_titular_tarjeta_cre => :P134_TITULAR_TC, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito                                                      ',
'                                                            pv_error               => :p0_error);',
'      ',
'        --- Null los campos ---',
'      ',
'        :p134_seq_id               := NULL;',
'        :p134_cre_id               := NULL;',
'        :p134_cre_fecha_deposito   := SYSDATE;',
'        :p134_ede_id               := NULL;',
'        :p134_cre_titular_cuenta   := NULL;',
'        :p134_cre_nro_cheque       := NULL;',
'        :p134_cre_nro_cuenta       := NULL;',
'        :p134_cre_nro_pin          := NULL;',
'        :p134_mcd_valor_movimiento := NULL;',
'      ',
unistr('        --- Yguaman 2011/10/04 19:32pm usado para el pago con retenci\00C3\00B3n y deja en null todos los campos'),
'        :P134_MCD_FECHA_DEPOSITO := NULL;',
'        :P134_TJ_ENTIDAD         := NULL;',
'        :P134_BANCOS             := NULL;',
'        :P134_TARJETA            := NULL;',
'        :P134_TIPO               := NULL;',
'        :P134_PLAN               := NULL;',
'        --  :P134_TJ_TITULAR := NULL;',
'        :P134_TJ_NUMERO           := NULL;',
'        :P134_TJ_NUMERO_VOUCHER   := NULL;',
'        :P134_TJ_RECAP            := NULL;',
'        :P134_LOTE                := NULL;',
'        :P134_MCD_NRO_AUT_REF     := NULL;',
'        :P134_MCD_NRO_COMPROBANTE := NULL;',
'        :P134_MODIFICACION        := NULL;',
'        :P134_MCD_FECHA_DEPOSITO  := NULL;',
'      ',
'        --- Retencion',
'        :P134_RAP_NRO_RETENCION    := NULL;',
'        :P134_RAP_NRO_AUTORIZACION := NULL;',
'        :P134_RAP_FECHA            := NULL;',
'        :P134_RAP_FECHA_VALIDEZ    := NULL;',
'        :P134_RAP_COM_ID           := NULL;',
'        :P134_SALDO_RETENCION      := NULl;',
unistr('        :P134_RAP_NRO_ESTABL_RET   := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('        :P134_RAP_NRO_PEMISION_RET := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('        :P134_PRE_ID               := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'      ',
'        -- tarjetas de credito',
unistr('        -- este campo lo lleno solo desde la facturaci\00C3\00B3n, cuando se factura una orden con tarjeta envio directamente la entrada y luego la vacio'),
'        :p134_entrada := null;',
'      ',
'      END IF;',
'    END IF;',
'  ELSE',
'  ',
'    :P134_MODIFICACION := NULL;',
'  ',
'  END IF;',
'  --END IF;',
'',
'exception',
'  when raise_puntaje then',
'    rollback;',
'    :p0_error := ''EL valor ingresado no puede ser mayor al total de puntajes'';',
'    --raise_application_error(-20000,);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*:request = ''cargar_tfp'' and :P0_ERROR is null*/',
'(:request = ''cargar_tfp'' and :P0_ERROR is null AND :P134_TFP_ID <> pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'')) OR',
'(:request = ''cargar_tfp'' and :P0_ERROR is null AND :P134_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_promocion_puntaje'') AND :P134_PUNTOS_CANCELAR > 0 AND :P134_MCD_VALOR_MOVIMIENTO > 0)'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739364332381437669
,p_process_comment=>unistr('Se ejecuta con el submit creado en el screep del head de la p\00C3\00A1gina, este screep se ejecuta al dar click en el boton grabar, envia el url para imprimir la factura')
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771520658482202485)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_imp.id(771514760434202467)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_PROMOCIONES_ORDENES'
,p_attribute_03=>'POR_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
,p_internal_uid=>739267507212437559
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771614268972202589)
,p_process_sequence=>25
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_vueltos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_movimientos_caja.pr_vueltos(pn_total_pagar => to_number(:P134_VALOR_TOTAL_PAGOS),',
'                                     pn_vuelto => :p134_vuelto,',
'                                     pn_emp_id => :f_emp_id,',
'                                     pv_error => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''cargar_tfp'' and :P0_ERROR is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739361117702437663
,p_process_comment=>'Antes estaba :P0_ERROR is null'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771614466428202589)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_col_movimientos_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                                 ''cv_coleccion_mov_caja''));'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(771536851816202506)
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P134_FACTURAR <> ''R'' ',
'/*Cuando viene de retencion no debe borrar la coleccion de movimientos caja*/'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739361315158437663
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771615656573202593)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pantalla'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
':P134_EDE_ID := NULL;',
':P134_CRE_TITULAR_CUENTA := NULL;',
'--:P134_SUM_PAGOS',
'--P134_ES_PRECANCELACION',
'--P134_SALDO_PROMO',
'--P134_SALDO_MAX_APLICAR',
':P134_MODIFICACION := NULL;',
':P134_TJ_ENTIDAD := NULL;',
':P134_BANCOS := NULL;',
':P134_TARJETA := NULL;',
'if :P134_ES_POLITICA_GOBIERNO = 0 then',
'',
'',
':P134_TIPO := NULL;',
':P134_PLAN := NULL;',
'',
'end if;',
':P134_TJ_NUMERO := NULL;',
':P134_TJ_NUMERO_VOUCHER := NULL;',
':P134_TJ_RECAP := NULL;',
':P134_LOTE := NULL;',
':P134_MCD_NRO_AUT_REF := NULL;',
'--:P134_CRE_ID',
'--:P134_CRE_FECHA_DEPOSITO := SYSDATE;',
':P134_CRE_NRO_CHEQUE := NULL;',
':P134_CRE_NRO_CUENTA := NULL;',
':P134_CRE_NRO_PIN := NULL;',
':P134_MCD_NRO_COMPROBANTE := NULL;',
':P134_RAP_NRO_ESTABL_RET := NULL;',
':P134_RAP_NRO_PEMISION_RET := NULL;',
':P134_RAP_NRO_RETENCION := NULL;',
':P134_RAP_NRO_AUTORIZACION := NULL;',
':P134_RAP_FECHA := NULL;',
':P134_RAP_FECHA_VALIDEZ := NULL;',
':P134_RAP_COM_ID := NULL;',
':P134_SALDO_RETENCION := NULL;',
':P134_PRE_ID  := NULL;',
':p134_entrada := null;',
':p134_titular_tc := null;',
'',
':P134_MCD_VALOR_MOVIMIENTO := 0;',
'IF NVL(:P134_ES_PRECANCELACION,''N'') <> ''S'' and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')THEN',
'-- Yguaman 2011/12/21. Valida si tiene anticipo de clientes, a obligarlo a aplicar el pago con anticipo',
'pq_ven_pagos_cuota.pr_advertencia_anticipos(pn_emp_id  => :F_EMP_ID,',
'                                     pn_cli_id    => :P134_CLI_ID,                                     ',
'                                     pn_antic_ing  => :P134_MCD_VALOR_MOVIMIENTO,',
'                                     pn_tfp_id   => :P134_TFP_ID,  ',
'                                     pn_seq_id => :P134_SEQ_ID,',
'                                      pn_facturar  => :P134_FACTURAR, -- M',
'                                     pn_uge_id  => :f_uge_id,',
'                                     pv_error =>:p0_error );',
'END IF;',
'',
'IF :p0_error is not null or :P134_SEQ_ID is not null then',
' :P134_TFP_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'');  ',
'end if;',
'',
':P134_SEQ_ID := null;',
'----------------------------------- ************************** ------------------------------------------------',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''limpia'' and :p0_error is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>unistr('Validaci\00C3\00B3n aprobada')
,p_internal_uid=>739362505303437667
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771614676120202590)
,p_process_sequence=>140
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_ven_comunes.pr_graba_identificadores_comp(pn_ode_id => 13683,',
'                                               pn_ebs_id => 16,',
'                                               pn_age_id =>:f_age_id_agencia,',
'                                               pn_emp_id =>1,',
'                                               pv_error  => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(51035678574731997)
,p_process_when_type=>'NEVER'
,p_internal_uid=>739361524850437664
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771619865619202607)
,p_process_sequence=>150
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_activa_boton_factura'
,p_process_sql_clob=>':P134_FACTURA_YN := ''S'';'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(46750954162572982)
,p_internal_uid=>739366714349437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771620252378202607)
,p_process_sequence=>160
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_vaciar_promociones'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DELETE    ven_promociones_ordenes a',
'WHERE     a.ord_id = :P134_ORD_ID',
'AND       a.per_id IS NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(46750954162572982)
,p_internal_uid=>739367101108437681
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771618672766202606)
,p_process_sequence=>170
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_valida_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_Ret NUMBER;',
'',
'BEGIN',
'  IF :P134_BASE > 0 THEN',
'    pq_ven_pagos_cuota_retencion.pr_carga_val_retenciones(PN_EMP_ID               => :f_emp_id,',
'                                                          PN_UGE_ID               => :f_uge_id,',
'                                                          PN_TSE_ID               => :f_seg_id,',
'                                                          pn_com_id               => :P134_ORD_ID,',
'                                                          pd_rap_fecha            => :P134_RAP_FECHA,',
'                                                          pd_rap_fecha_validez    => :P134_RAP_FECHA_VALIDEZ,',
'                                                          pn_rap_nro_retencion    => :P134_RAP_NRO_RETENCION,',
'                                                          pn_rap_nro_autorizacion => :P134_RAP_NRO_AUTORIZACION,',
'                                                          pn_RAP_NRO_ESTABL_RET   => :P134_RAP_NRO_ESTABL_RET,',
'                                                          pn_RAP_NRO_PEMISION_RET => :P134_RAP_NRO_PEMISION_RET,',
'                                                          pn_porcentaje           => :P134_tRE_ID,',
'                                                          pn_base_ret             => :p134_base,',
'                                                          pv_ret_iva              => :p134_ret_iva,',
'                                                          pv_observacion          => :P134_OBSERVACIONES_RET,',
'                                                          pv_sesion               => :P134_DEA_SECCION_1,',
'                                                         pn_ord_id               => :P134_ORD_ID,',
'                                                          PV_ERROR                => :p0_error);',
'  END IF;',
'',
'  IF :P134_OBSERVACIONES_RET IS NULL THEN',
'  ',
'    SELECT sum(to_number(c010))',
'      INTO :P134_MCD_VALOR_MOVIMIENTO',
'      FROM apex_collections',
'     WHERE collection_name = ''COLL_VALOR_RETENCION'';',
'  ',
'  ELSE',
'    :P134_MCD_VALOR_MOVIMIENTO := NULL;',
'  ',
'  END IF;',
'',
'  :P134_TRE_ID := NULL;',
'  :P134_BASE   := 0;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CARGA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>739365521496437680
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771618481670202605)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_induccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar      NUMBER;',
'  ln_com_id_retencion  ven_comprobantes.com_id%TYPE;',
'  ln_contador          NUMBER;',
'  ln_valor             VARCHAR2(100);',
'  ln_total_factura     NUMBER;',
'  ln_entrada           NUMBER;',
'  ln_validacion_precio NUMBER;',
'  cn_var_id_total      NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_var_id_total'');',
'  cn_var_id_entrada    NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_var_id_entrada'');',
'  cn_cat_id_induc_enc  NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_cat_id_induc_enc'');',
'  cn_cat_id_induc_coc  NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_cat_id_induc_coc'');',
'  ln_ite_sku_id        NUMBER;',
'BEGIN',
'',
'  ln_com_id_retencion := NULL;',
'',
'  IF :p134_modificacion IS NULL AND',
'     :p20_tve_id !=',
'     pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tve_id_contado'') THEN',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'      ---- cargo total de favtura',
'    ',
'      SELECT vor_valor_variable',
'        INTO ln_total_factura',
'        FROM ven_var_ordenes',
'       WHERE ord_id = :p134_ord_id',
'         AND var_id = cn_var_id_total;',
'    ',
'      ---- cargo valor entrada',
'    ',
'      SELECT vor_valor_variable',
'        INTO ln_entrada',
'        FROM ven_var_ordenes',
'       WHERE ord_id = :p134_ord_id',
'         AND var_id = cn_var_id_entrada;',
'    ',
'      SELECT COUNT(*)',
'        INTO ln_contador',
'        FROM apex_collections',
'       WHERE collection_name =',
'             pq_constantes.fn_retorna_constante(0, ''cv_coleccion_mov_caja'')',
'         AND c005 =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_tfp_id_plan_gobierno'');',
'    ',
'      IF ln_contador = 0 THEN',
'      ',
'        pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => :p134_plan,',
'                                                            pn_cre_id               => :p134_cre_id,',
'                                                            pn_tfp_id               => pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                                                          ''cn_tfp_id_plan_gobierno''),',
'                                                            pn_mcd_valor_movimiento => (nvl(to_number(round(ln_total_factura,',
'                                                                                                            2)),',
'                                                                                            0) -',
'                                                                                       nvl(to_number(ln_entrada),',
'                                                                                            0)), --:p134_mcd_valor_movimiento',
'                                                            pn_mcd_nro_aut_ref      => :p134_mcd_nro_aut_ref,',
'                                                            pn_mcd_nro_lote         => :p134_lote,',
'                                                            pn_mcd_nro_retencion    => :p134_rap_nro_retencion,',
'                                                            pn_mcd_nro_comprobante  => :p134_mcd_nro_comprobante, --NULL,',
'                                                            pv_titular_cuenta       => :p134_cre_titular_cuenta, --',
'                                                            pv_cre_nro_cheque       => :p134_cre_nro_cheque,',
'                                                            pv_cre_nro_cuenta       => :p134_cre_nro_cuenta,',
'                                                            pv_nro_pin              => :p134_cre_nro_pin,',
'                                                            pv_nro_tarjeta          => :p134_tj_numero,',
'                                                            pv_voucher              => :p134_tj_numero_voucher,',
'                                                            pn_saldo_retencion      => nvl(to_number(:p134_saldo_retencion),',
unistr('                                                                                           0), --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_com_id               => NULL, --:P134_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pd_rap_fecha            => :p134_rap_fecha, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pd_rap_fecha_validez    => :p134_rap_fecha_validez, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_autorizacion => :p134_rap_nro_autorizacion, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_establ_ret   => :p134_rap_nro_establ_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_rap_nro_pemision_ret => :p134_rap_nro_pemision_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                            pn_pre_id               => :p134_pre_id, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                            pd_fecha_deposito       => :p134_mcd_fecha_deposito,',
unistr('                                                            --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                            pv_titular_tarjeta_cre => :p134_titular_tc, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito                                                      ',
'                                                            pv_error               => :p0_error);',
'      ',
'        :p134_ede_id            := NULL;',
'        :p134_tfp_id            := NULL;',
'        :p134_tj_numero         := NULL;',
'        :p134_tj_numero_voucher := NULL;',
'        :p134_plan              := NULL;',
'        :p134_titular_tc        := NULL;',
'      ELSIF ln_contador > 1 THEN',
'        raise_application_error(-20000,',
'                                ''YA ESTA CARGADA LA FORMA DE PAGO GOBIERNO.  NO DEBE CARGARLA NUEVAMENTE'');',
'      END IF;',
'    END IF;',
'  ELSE',
'    :p134_modificacion := NULL;',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P20_ES_POLITICA_GOBIERNO = 1'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739365330400437679
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771615073278202592)
,p_process_sequence=>40
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_caduca_puntualito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_asdm_promociones.pr_cancela_promo(pn_emp_id       => :f_emp_id,',
'                                        pn_pro_id       => pq_constantes.fn_retorna_constante(pn_emp_id    => :f_emp_id,',
'                                                                                              pv_constante => ''cn_pro_id_promocion_pr''),',
'                                        pn_cli_id       => :p134_cli_id,',
'                                        pn_uge_id       => :f_uge_id,',
'                                        pn_usu_id       => :f_user_id,',
'                                        pn_uge_id_gasto => :f_uge_id_gasto,',
'                                        pv_error        => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>unistr('No tiene para forma de pago puntualito regal\00C3\00B3n')
,p_process_when_type=>'NEVER'
,p_internal_uid=>739361922008437666
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771615867493202594)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_entrada_tc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar     NUMBER;',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
'',
'BEGIN',
'',
'  ln_com_id_retencion := NULL;',
'if :P134_entrada is not null then',
'  IF :P134_MODIFICACION IS NULL THEN',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'      pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                      pn_tfp_id => pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''),',
'                                                      pv_error  => :p0_error);',
'    ',
'',
'      pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => NULL,',
'                                                          pn_cre_id               => NULL,',
'                                                          pn_tfp_id               => pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''),',
'                                                          pn_mcd_valor_movimiento => nvl(to_number(:P134_ENTRADA),',
'                                                                                         0), --:p134_mcd_valor_movimiento',
'                                                          pn_mcd_nro_aut_ref      => NULL,',
'                                                          pn_mcd_nro_lote         => NULL,',
'                                                          pn_mcd_nro_retencion    => NULL,',
'                                                          pn_mcd_nro_comprobante  => NULL,',
'                                                          pv_titular_cuenta       => NULL, --:p134_tj_titular',
'                                                          pv_cre_nro_cheque       => NULL,',
'                                                          pv_cre_nro_cuenta       => NULL,',
'                                                          pv_nro_pin              => NULL,',
'                                                          pv_nro_tarjeta          => NULL,',
'                                                          pv_voucher              => NULL,',
unistr('                                                          pn_saldo_retencion      => 0, --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_com_id               => NULL, --:P134_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pd_rap_fecha            => NULL, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pd_rap_fecha_validez    => NULL, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_rap_nro_autorizacion => NULL, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_RAP_NRO_ESTABL_RET   => NULL, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_RAP_NRO_PEMISION_RET => NULL, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_PRE_ID               => NULL, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                          pd_fecha_deposito       => NULL,',
'                                                          pv_titular_tarjeta_cre  => NULL, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito',
unistr('                                                          --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                          pv_error => :p0_error);',
'    ',
'      --- Null los campos ---',
'        :P134_ENTRADA := NULL;',
'    END IF;',
'  ELSE',
'  ',
'    :P134_MODIFICACION := NULL;',
'  ',
'  END If;',
'',
'end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P134_ENTRADA and :P134_ES_POLITICA_GOBIERNO = 0'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>739362716223437668
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771617073483202595)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_DATOS_FOLIO_CAJA_USU'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%type;',
'BEGIN',
'',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
'  ln_tgr_id  :=  pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito'');',
'  --:P0_TTR_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nc_rebaja_puntualito'');',
'  :P0_TTR_ID := pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_nc_rebaja_puntualito'');',
'',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       LN_TGR_ID,',
'      :P0_TTR_DESCRIPCION, ',
'      :P0_PERIODO,',
'      :P0_DATFOLIO,',
'      :P134_SECUENCIA_ACTUAL,',
'      :P0_PUE_NUM_SRI  ,',
'      :P0_UGE_NUM_EST_SRI ,',
'      :P0_PUE_ID,',
'      :P0_TTR_ID,',
'      :P0_NRO_FOLIO,',
'      :P0_ERROR);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>739363922213437669
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771617272635202595)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_col_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_int_mora''));',
' pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_gastos_gto_cobr''));'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P134_FACTURAR = ''S'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739364121365437669
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771614053798202589)
,p_process_sequence=>150
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_items_saldos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'  if :P134_TFP_ID =',
'     pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_dev_promo'') then',
'  ',
'    :P134_SALDO_PROMO_DEV := to_number(pq_asdm_promociones.fn_obtener_saldo_act_dev(:P134_CLI_ID,',
'                                                                                   :F_EMP_ID,',
'                                                                                   pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                                                                                      ''cn_pro_id_promocion_pr''),',
'                                                                                   NULL,',
'                                                                                   :P0_Error));',
'  ',
'',
'  elsif :P134_TFP_ID =',
'        pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                           ''cn_tfp_id_promocion_puntaje'') then',
'  ',
'    :P134_SALDO_PROMO_DEV := to_number(pq_ven_promociones_sql.fn_saldo_puntos(pq_ven_promociones_sql.fn_cliente_persona(:p134_cli_id, :F_EMP_ID,''P''),:P134_PRO_ID,:P134_PTA_ID));',
'  ',
'  end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739360902528437663
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771619067179202606)
,p_process_sequence=>170
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_PAGINA_RETORNO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P134_PAGINA_REGRESO IS NULL THEN',
'   :P134_PAGINA_REGRESO := 6;',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739365915909437680
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771620665926202608)
,p_process_sequence=>170
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_punto_emision'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_pue_id             number;',
'  lv_tipo_punto_emision varchar2(1);',
'',
'begin',
'',
'  select ca.pue_id',
'    into ln_pue_id',
'    from ven_periodos_caja ca',
'   where ca.pca_id = :f_pca_id;',
'',
'  :P134_VALIDA_punto := pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => ln_pue_id);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739367514656437682
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771614878274202590)
,p_process_sequence=>160
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_puntos_usados'
,p_process_sql_clob=>':P134_PUNTOS_USADOS := :P134_MCD_VALOR_MOVIMIENTO;'
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P134_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_promocion_puntaje'')'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>739361727004437664
);
wwv_flow_imp.component_end;
end;
/
