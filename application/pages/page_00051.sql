prompt --application/pages/page_00051
begin
--   Manifest
--     PAGE: 00051
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>51
,p_name=>'Impresion Masiva de Facturas'
,p_step_title=>'Impresion Masiva de Facturas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'',
'</script>'))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(72751864279257849)
,p_name=>'IMPRESION DE FACTURAS'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select seq_id Numero,',
'''Cargar'' Cargar,',
'c001 Com_id,',
'c002 Fecha,',
'c003 Cli_id,',
'c004 Cliente,',
'c005 Uge_id,',
'c006 Unidad_Gestion,',
'c007 TOTAL,',
'c009 ord_id',
'from apex_collections where collection_name=''COLL_FAC_MAYOREO'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No hay Facturas pendientes de imprimir.'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(74682370546816944)
,p_query_column_id=>1
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>1
,p_column_heading=>'RANGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291070519229695728)
,p_query_column_id=>2
,p_column_alias=>'CARGAR'
,p_column_display_sequence=>2
,p_column_heading=>'Cargar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:51:&SESSION.:CARGAR:&DEBUG.:RP:P51_COM_ID,P51_ORD_ID,P51_FAC_DESDE,P51_FAC_HASTA:#COM_ID#,#ORD_ID#,#NUMERO#,#NUMERO#'
,p_column_linktext=>'#CARGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(74683964700835365)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'ITEM_IS_NULL'
,p_display_when_condition=>'P51_COM_ID'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(74683868730835354)
,p_query_column_id=>4
,p_column_alias=>'FECHA'
,p_column_display_sequence=>4
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(74684164840835365)
,p_query_column_id=>5
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(74684282449835365)
,p_query_column_id=>6
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>6
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(153291156356666993)
,p_query_column_id=>7
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(153291377678667012)
,p_query_column_id=>8
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>8
,p_column_heading=>'UNIDAD GESTION'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(153291471744667012)
,p_query_column_id=>9
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>9
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291070675103695730)
,p_query_column_id=>10
,p_column_alias=>'ORD_ID'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(72745182324215708)
,p_plug_name=>' <B>  NUMERO DE FOLIO <B>&P51_NRO_FOLIO. '
,p_parent_plug_id=>wwv_flow_imp.id(72751864279257849)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P51_COM_ID'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<B>SECUENCIA ACTUAL<B>  &P51_COM_NUMERO.',
'condicion',
'SQL Expression',
'(select count(*)',
'from apex_collections',
'where collection_name=''COLL_FAC_MAYOREO'')>0'))
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(153251065332442584)
,p_plug_name=>'RANGOS DE IMPRESION'
,p_parent_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P51_COM_NUM'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(291071136227695734)
,p_name=>'FACTURAS A IMPRIMIR'
,p_parent_plug_id=>wwv_flow_imp.id(72751864279257849)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>45
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.seq_id Numero,',
'c.c001 Com_id,',
'c.c002 Fecha,',
'c.c003 Cli_id,',
'c.c004 Cliente,',
'c.c005 Uge_id,',
'c.c006 Unidad_Gestion,',
'c.c007 TOTAL,',
'case when c.seq_id = ',
'  (select max(c.seq_id)',
'from apex_collections c, tmp_com_fac_imp d',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and d.com_id = to_number(c.c001)',
'and d.emp_id = :f_emp_id',
'and d.uge_id = :P51_UGE_ID_COM)',
'then ''X'' ',
'  else null',
'    end Eliminar ',
'from apex_collections c, tmp_com_fac_imp d',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and d.com_id = to_number(c.c001)',
'and d.emp_id = :f_emp_id',
'and d.uge_id = :P51_UGE_ID_COM',
'ORDER BY C.SEQ_ID'))
,p_display_when_condition=>'P51_COM_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071167987695735)
,p_query_column_id=>1
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>1
,p_column_heading=>'Numero'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071350090695736)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Com id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071364227695737)
,p_query_column_id=>3
,p_column_alias=>'FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071485541695738)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Cli id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071562211695739)
,p_query_column_id=>5
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>5
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071722063695740)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Uge id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071760980695741)
,p_query_column_id=>7
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>7
,p_column_heading=>'Unidad gestion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291071934001695742)
,p_query_column_id=>8
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>8
,p_column_heading=>'Total'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(291072029950695743)
,p_query_column_id=>9
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>9
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:51:&SESSION.:ELIMINAR:&DEBUG.:RP:P51_COM_ELIMINA:#COM_ID#'
,p_column_linktext=>'X'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':p51_fac_desde is not null and :p51_fac_hasta is not null'
,p_display_when_condition2=>'SQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(217074768016601123)
,p_name=>unistr('FacturasNegociaci\00F3n')
,p_template=>wwv_flow_imp.id(270526680051046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
unistr('SELECT ''ParaNegociaci\00F3n'','),
'       a.com_id,',
'       a.com_fecha,',
'       a.cli_id,',
'       a.nombre_completo,',
'       a.Unidad_Gestion,',
'       (select b.vco_valor_variable',
'          from ven_var_comprobantes b',
'         where b.emp_id = a.emp_id',
'           and b.com_id = a.com_id',
'           and b.var_id =',
'               pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_var_id_total'')) TOTAL',
'  FROM v_ven_comprobantes a',
' WHERE a.com_numero IS NULL',
'   and a.uge_id_factura = :F_UGE_ID',
'   and pq_ven_comprobantes.fn_aplica_negociacion(pn_cli_id => a.cli_id,',
'                                                 pn_com_id => a.com_id, pn_emp_id => :f_emp_id) = ''S''',
'   and a.emp_id = :F_EMP_ID',
'   and a.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_comprobante_may'')',
' ORDER BY a.com_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075056678601196)
,p_query_column_id=>1
,p_column_alias=>unistr('''PARANEGOCIACI\00D3N''')
,p_column_display_sequence=>1
,p_column_heading=>unistr('''PARANEGOCIACI\00D3N''')
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075169646601201)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075271889601202)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075364083601202)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075470300601202)
,p_query_column_id=>5
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>5
,p_column_heading=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075569053601202)
,p_query_column_id=>6
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>6
,p_column_heading=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(217075679044601202)
,p_query_column_id=>7
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>7
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(72750572459250708)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(153251065332442584)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition_type=>'NEVER'
,p_button_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'item is not null',
'p51_com_num'))
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(291072052918695744)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(291071136227695734)
,p_button_name=>'impresion'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select count(*)',
'from apex_collections c, tmp_com_fac_imp d',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and d.com_id = to_number(c.c001)',
'and d.emp_id = :f_emp_id',
'and d.uge_id = :p51_uge_id_com'))
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(75771458184756041)
,p_branch_name=>'Go To Page 51'
,p_branch_action=>'f?p=&APP_ID.:51:&SESSION.:impresion:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(291072052918695744)
,p_branch_sequence=>20
,p_branch_comment=>'Created 21-JUL-2011 18:40 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(74557554338241803)
,p_branch_action=>'f?p=&APP_ID.:51:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 19-JUL-2011 12:28 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(72746083494225551)
,p_name=>'P51_FAC_DESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(153251065332442584)
,p_prompt=>'Desde'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="valida_numero(this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'p51_com_num'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(72746272134225590)
,p_name=>'P51_FAC_HASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(153251065332442584)
,p_prompt=>'Hasta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="valida_numero(this)"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'p51_com_num'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153054059490075964)
,p_name=>'P51_UGE_NUM_EST_SRI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_prompt=>'DIGITE LA NUEVA SECUENCIA A IMPRIMIR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153054260616076001)
,p_name=>'P51_PUE_NUM_SRI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153054466434076024)
,p_name=>'P51_COM_NUMERO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153054680453076024)
,p_name=>'P51_COM_NUM'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P51_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153056378106100268)
,p_name=>'P51_PUE_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_prompt=>'PUE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153056573747100269)
,p_name=>'P51_NRO_FOLIO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_prompt=>'NRO_FOLIO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153352258752507510)
,p_name=>'P51_COM_ELIMINA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(153251065332442584)
,p_prompt=>'com_borrar'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(291070605663695729)
,p_name=>'P51_COM_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(291070816670695731)
,p_name=>'P51_ORD_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(291071024637695733)
,p_name=>'P51_UGE_ID_COM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(20933431966428973559)
,p_name=>'P51_PUE_ELECTRONICO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_prompt=>'Pue Electronico	'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(74875043176606835227)
,p_name=>'P51_APLICA_NEG'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(72745182324215708)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(77383474392185641)
,p_validation_name=>'P51_FAC_HASTA'
,p_validation_sequence=>10
,p_validation=>'P51_FAC_HASTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Tiene que ingresar un valor'
,p_when_button_pressed=>wwv_flow_imp.id(72750572459250708)
,p_associated_item=>wwv_flow_imp.id(72746272134225590)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(77385070713193983)
,p_validation_name=>'P51_FAC_DESDE'
,p_validation_sequence=>20
,p_validation=>'P51_FAC_DESDE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Tiene que ingresar un valor'
,p_when_button_pressed=>wwv_flow_imp.id(72750572459250708)
,p_associated_item=>wwv_flow_imp.id(72746083494225551)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(153067757514207824)
,p_validation_name=>'P51_COM_NUM'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p51_com_num = :P51_com_numero then',
'return null;',
'else',
':P51_com_num := null;',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_imp.id(153054680453076024)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(291070450178695727)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'  SELECT ag.uge_id',
'  INTO :P51_UGE_ID_COM',
'  FROM ven_ordenes vo, asdm_agencias ag',
' WHERE vo.ord_id = :p51_ord_id',
'   AND vo.age_id_agencia = ag.age_id;',
'',
'',
'pq_ven_Comprobantes.pr_datos_folio(pn_emp_id          => :F_EMP_ID,',
'               pn_usu_id          => :F_USER_ID,',
'               pn_uge_id          => :P51_UGE_ID_COM,--:F_uge_id,',
'               pn_ttr_id          => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'               pn_tgr_id          => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'               pn_pue_id          => :P51_PUE_ID,',
'               pv_pue_num_sri     => :P51_pue_num_sri,',
'               pv_uge_num_est_sri => :P51_uge_num_est_sri,',
'               pn_fol_sec_actual  => :P51_COM_NUMERO,',
'               pn_nro_folio       => :P51_NRO_FOLIO,',
'               pv_error           => :P0_error);',
'               ',
'               ',
':P51_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P51_PUE_ID);',
'iF :P51_PUE_ELECTRONICO= ''E'' THEN ',
':P51_COM_NUM := :P51_COM_NUMERO;',
'',
'',
'end if;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      raise_application_error(-20000,',
'                              :P0_error ||  ',
'                              SQLERRM);',
'      ROLLBACK;',
'END;',
'               '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>258817298908930801
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(153317179633945953)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cursor cur_col_facturas is',
'select to_number(c.seq_id) Numero,',
'to_number(c.c001) Com_id,',
'c.c002 Fecha,',
'to_number(c.c003) Cli_id,',
'c.c004 Cliente,',
'to_number(c.c005) Uge_id,',
'c.c006 Unidad_Gestion,',
'to_number(c.c007) TOTAL,',
'c.c008 aplica_neg',
'from apex_collections c',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and to_number(c.c005) = :f_uge_id',
'and to_number(c.seq_id) BETWEEN :P51_FAC_DESDE AND :P51_FAC_HASTA;',
'',
'ln_cuantos number;',
'begin',
'',
'',
'delete from tmp_com_fac_imp fi ',
'where fi.com_id in (select to_number(c001) ',
'                      from apex_collections',
'                     where collection_name=''COLL_FAC_MAYOREO'');',
'',
'for x in cur_col_facturas loop',
'    pq_ven_Comprobantes.pr_datos_folio(pn_emp_id => :F_EMP_ID,',
'               pn_usu_id          => :F_USER_ID,',
'               pn_uge_id          => :P51_UGE_ID_COM,--:F_uge_id,',
'               pn_ttr_id          => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'               pn_tgr_id          => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'               pn_pue_id          => :P51_PUE_ID,',
'               pv_pue_num_sri     => :P51_pue_num_sri,',
'               pv_uge_num_est_sri => :P51_uge_num_est_sri,',
'               pn_fol_sec_actual  => :P51_COM_NUMERO,',
'               pn_nro_folio       => :P51_NRO_FOLIO,',
'               pv_error           => :P0_error,',
'               pv_negociacion     => x.aplica_neg);',
'               ',
'  :P51_APLICA_NEG := x.aplica_neg;',
'               ',
'  :P51_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P51_PUE_ID);',
'  iF :P51_PUE_ELECTRONICO= ''E'' THEN ',
'    :P51_COM_NUM := :P51_COM_NUMERO;',
'  end if;',
'  select count(*)',
'  into ln_cuantos',
'  from tmp_com_fac_imp a',
'  where a.com_id = x.com_id',
'  and a.emp_id = :f_emp_id',
'  and a.uge_Id = :P51_UGE_ID_COM--x.uge_id;',
'  AND A.ESTADO_REGISTRO = ''0'';',
'  ',
'  if ln_cuantos > 0 then',
'    null;',
'  else',
'pq_ven_comprobantes_dml.pr_ins_tmp_fac_impresion(PN_COM_ID =>x.com_id,',
'                         PN_EMP_ID => :f_emp_id,',
'                         PN_UGE_ID => :P51_UGE_ID_COM,--x.Uge_id,',
'                         PV_ESTADO_REGISTRO => pq_constantes.fn_retorna_constante(null,''cv_estado_reg_activo''),',
'                         pv_error  => :p0_error);',
'  end if;',
'end loop;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>121064028364181027
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(74716180889141603)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_FAC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cursor cur_fac_temp is',
'      select *',
'        from tmp_com_fac_imp a',
'       where a.emp_id = :f_emp_id',
'         and a.uge_id = :P51_UGE_ID_COM;--:f_uge_id;',
'',
'BEGIN',
'pq_ven_comprobantes.pr_impresion_mayoreo(',
'pn_emp_id =>:F_EMP_ID,',
'pn_pca_id =>:F_PCA_ID,',
'pn_tse_id =>:f_seg_id,',
'pn_uge_id =>:P51_UGE_ID_COM,--:F_UGE_ID,',
'pn_usu_id => :F_USER_ID,',
'pn_com_numero => :p51_com_numero,',
'pn_fac_desde =>:P51_FAC_DESDE,',
'pn_fac_hasta =>:P51_FAC_HASTA,',
'pv_com_aplica_negociacion => ''N'',',
'pv_error =>:P0_ERROR);',
':P51_FAC_DESDE:='''';',
':P51_FAC_HASTA:='''';',
':p51_com_num := null;',
':p51_com_numero := null;',
'',
'for c in cur_fac_temp loop    ',
'      --vacio la tabla temporal con las facturas a imprimir',
'      pq_ven_comprobantes_dml.pr_upd_tmp_fac_impresion(PN_COM_ID => c.com_id,',
'                                                       PV_ESTADO_REGISTRO => pq_constantes.fn_retorna_constante(null,''cv_estado_reg_inactivo''),',
'                                                       pv_error  => :p0_error);    ',
'end loop;',
'/*pq_ven_Comprobantes.pr_datos_folio(pn_emp_id          => :F_EMP_ID,',
'               pn_usu_id          => :F_USER_ID,',
'               pn_uge_id          => :F_uge_id,',
'               pn_ttr_id          => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'               pn_tgr_id          => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'               pn_pue_id          => :P51_PUE_ID,',
'               pv_pue_num_sri     => :P51_pue_num_sri,',
'               pv_uge_num_est_sri => :P51_uge_num_est_sri,',
'               pn_fol_sec_actual  => :P51_COM_NUMERO,',
'               pn_nro_folio       => :P51_NRO_FOLIO,',
'               pv_error           => :P0_error);',
'',
'pq_ven_comprobantes.pr_col_impresion_may(pn_emp_id => :f_emp_id,',
'                     pn_uge_id => :f_uge_id,',
'                     pv_error  => :p0_error);',
'',
':P51_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P51_PUE_ID);',
'iF :P51_PUE_ELECTRONICO= ''E'' THEN ',
':P51_COM_NUM := :P51_COM_NUMERO;',
'end if;*/',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(291072052918695744)
,p_process_when=>'impresion'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>42463029619376677
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(101569911628712798029)
,p_process_sequence=>50
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpiarcampos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P51_FAC_DESDE := null;',
':P51_UGE_NUM_EST_SRI := null;',
':P51_COM_ID := null;',
':P51_UGE_ID_COM := null;',
':P51_ORD_ID := null;',
':P51_FAC_HASTA := null;',
':P51_PUE_NUM_SRI := null;',
':P51_COM_NUM := null;',
':P51_COM_NUMERO := null;',
':P51_PUE_ID := null;',
':P51_NRO_FOLIO := null;',
':P51_PUE_ELECTRONICO := null; ',
':P51_APLICA_NEG := null;',
'',
'pq_ven_comprobantes.pr_col_impresion_may(pn_emp_id => :f_emp_id,',
'                     pn_uge_id => :f_uge_id,',
'                     pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(291072052918695744)
,p_process_when=>'impresion'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101537658477443033103
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(153352765809519015)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_eliminar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'      pq_ven_comprobantes_dml.pr_del_tmp_fac_impresion(PN_COM_ID => :p51_com_elimina,',
'                                                       pv_error  => :p0_error);    ',
'                                                       ',
'     :P51_FAC_DESDE := null;',
':P51_UGE_NUM_EST_SRI := null;',
':P51_COM_ID := null;',
':P51_UGE_ID_COM := null;',
':P51_ORD_ID := null;',
':P51_FAC_HASTA := null;',
':P51_PUE_NUM_SRI := null;',
':P51_COM_NUM := null;',
':P51_COM_NUMERO := null;',
':P51_PUE_ID := null;',
':P51_NRO_FOLIO := null;',
':P51_PUE_ELECTRONICO := null; ',
':P51_APLICA_NEG := null;',
'',
'pq_ven_comprobantes.pr_col_impresion_may(pn_emp_id => :f_emp_id,',
'                     pn_uge_id => :f_uge_id,',
'                     pv_error  => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>121099614539754089
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(74670670153788446)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'cargar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comprobantes.pr_col_impresion_may(pn_emp_id => :f_emp_id,',
'                     pn_uge_id => :f_uge_id,',
'                     pv_error  => :p0_error);',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>42417518884023520
);
wwv_flow_imp.component_end;
end;
/
