prompt --application/pages/page_00004
begin
--   Manifest
--     PAGE: 00004
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>4
,p_name=>'Anulacion de Cheques'
,p_alias=>'POSTERGACIONCHEQUES'
,p_step_title=>'Anulacion de Cheques'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44929063245627756)
,p_name=>'CHEQUES CON NOTAS DE CREDITO'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>13
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.com_id_ref          com_id_factura,',
'       b.cxc_id              credito,',
'       c.com_id              com_id_nc,',
'       b.div_nro_vencimiento nro_vencimiento,',
'       b.div_fecha_vencimiento fecha_vencimiento,',
'       b.div_saldo_cuota     saldo_cuota,',
'       a.rdi_valor_respaldado valor_respaldado',
'  from car_respaldos_dividendos a, car_dividendos b, ven_comprobantes c',
' where b.div_id = a.div_id',
'   and b.emp_id = a.emp_id',
'   and b.div_estado_registro = 0',
'   and c.com_id = a.com_id',
'   and c.com_estado_registro = 0',
'   and c.emp_id = a.emp_id',
'   and a.com_id is not null',
'   and a.rdi_estado_registro = 0',
'   and a.emp_id = :f_emp_id',
'   and a.rca_id = :P4_RCA_ID'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select null',
'  from car_respaldos_dividendos',
' where com_id is not null',
'   and rdi_estado_registro = 0',
'   and emp_id = :f_emp_id',
'   and rca_id = :P4_RCA_ID'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44940969320743059)
,p_query_column_id=>1
,p_column_alias=>'COM_ID_FACTURA'
,p_column_display_sequence=>1
,p_column_heading=>'Com Id Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44941078236743062)
,p_query_column_id=>2
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>2
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44941180392743062)
,p_query_column_id=>3
,p_column_alias=>'COM_ID_NC'
,p_column_display_sequence=>3
,p_column_heading=>'Com Id Nc'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44941283166743062)
,p_query_column_id=>4
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44941380817743062)
,p_query_column_id=>5
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44941476394743062)
,p_query_column_id=>6
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44941576786743062)
,p_query_column_id=>7
,p_column_alias=>'VALOR_RESPALDADO'
,p_column_display_sequence=>7
,p_column_heading=>'Valor Respaldado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102552758882863198)
,p_plug_name=>'Anulacion  de Cheques'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>22
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'P4_CRE_ID'
,p_plug_display_when_cond2=>'1'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102750258923776014)
,p_plug_name=>'Anulacion de Cheques'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>2
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cre.cre_id,',
'       cre.rca_id,',
'       cre.cre_nro_cheque,',
'       cre.cre_nro_cuenta,',
'       cre.cre_nro_pin,',
'       ede.ede_descripcion,',
'       cre.cre_titular_cuenta,',
'       cre.cre_valor,',
'       cre.cre_fecha_deposito,',
'       cre.cre_observaciones,',
'       cre.cre_id id',
'  FROM asdm_cheques_recibidos  cre,',
'       asdm_entidades_destinos ede,',
'       asdm_transacciones      trx',
' WHERE cre.emp_id = :f_emp_id',
'   AND cre.cre_estado_registro = 0',
'   AND cre.cli_id = :P4_CLI_ID',
'   and cre.ede_id = ede.ede_id',
'   AND cre.ech_id in',
'       (pq_constantes.fn_retorna_constante(NULL, ''cn_ech_id_ingresado''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_ech_id_postergado''))',
'   AND cre_tipo =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_tch_posfechado'')',
'   AND cre.trx_id = trx.trx_id',
'   AND trx.uge_id = :f_uge_id',
'   AND not exists(select null from apex_collections co where co.collection_name = ''COL_CHEQUES_ANULAR''',
'                 and n002 = cre.cre_id)'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(102750357681776014)
,p_name=>'Cheques por cliente'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#ws/small_page.gif" alt="" />'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>70497206412011088
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102750966648776018)
,p_db_column_name=>'CRE_ID'
,p_display_order=>1
,p_column_identifier=>'E'
,p_column_label=>'Anular'
,p_column_link=>'f?p=&APP_ID.:4:&SESSION.:CARGAR:&DEBUG.::P4_CRE_ID,P4_CRE_NRO_CHEQUE,P4_CRE_FECHA_DEPOSITO,P4_RCA_ID,P4_TITULAR:#CRE_ID#,#CRE_NRO_CHEQUE#,#CRE_FECHA_DEPOSITO#,#RCA_ID#,#CRE_TITULAR_CUENTA#'
,p_column_linktext=>'Anular'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102750558054776017)
,p_db_column_name=>'CRE_NRO_CHEQUE'
,p_display_order=>2
,p_column_identifier=>'A'
,p_column_label=>'Nro.<br>Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102750660032776018)
,p_db_column_name=>'CRE_TITULAR_CUENTA'
,p_display_order=>3
,p_column_identifier=>'B'
,p_column_label=>'Titular<br>Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_TITULAR_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102750767385776018)
,p_db_column_name=>'CRE_VALOR'
,p_display_order=>4
,p_column_identifier=>'C'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102750854556776018)
,p_db_column_name=>'CRE_FECHA_DEPOSITO'
,p_display_order=>5
,p_column_identifier=>'D'
,p_column_label=>'Fecha<br>Deposito'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104414871301763672)
,p_db_column_name=>'CRE_OBSERVACIONES'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(105665063596335491)
,p_db_column_name=>'EDE_DESCRIPCION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'EDE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77677963717229514)
,p_db_column_name=>'CRE_NRO_CUENTA'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Nro.<br>Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77678070971229515)
,p_db_column_name=>'CRE_NRO_PIN'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Nro.<br>Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44931667186638355)
,p_db_column_name=>'RCA_ID'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Rca Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'RCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34246750811499065740)
,p_db_column_name=>'ID'
,p_display_order=>20
,p_column_identifier=>'K'
,p_column_label=>'Id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(102751158492776194)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1274912239919693'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_NRO_CHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_VALOR:CRE_FECHA_DEPOSITO:CRE_OBSERVACIONES:EDE_DESCRIPCION:CRE_ID:'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102813769735367637)
,p_plug_name=>'Error'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P4_PV_ERROR'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(34246750362463065736)
,p_name=>'CHEQUES X ANULAR'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>12
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SEQ_ID ID, ',
'n001 Cli_id, ',
'n002 Cre_id, ',
'c002 titular,',
'C001 Nro_Cheque, ',
'd001 Fecha_Deposito, ',
'''ELIMINA'' elimina',
'FROM APEX_COLLECTIONS ',
'WHERE COLLECTION_NAME = ''COL_CHEQUES_ANULAR'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>50
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246751231367065744)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246751571797065748)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cli id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246751651437065749)
,p_query_column_id=>3
,p_column_alias=>'CRE_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Cre id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246752199305065754)
,p_query_column_id=>4
,p_column_alias=>'TITULAR'
,p_column_display_sequence=>4
,p_column_heading=>'Titular'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246751757709065750)
,p_query_column_id=>5
,p_column_alias=>'NRO_CHEQUE'
,p_column_display_sequence=>5
,p_column_heading=>'Nro cheque'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246751927526065751)
,p_query_column_id=>6
,p_column_alias=>'FECHA_DEPOSITO'
,p_column_display_sequence=>6
,p_column_heading=>'Fecha deposito'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34246751025460065742)
,p_query_column_id=>7
,p_column_alias=>'ELIMINA'
,p_column_display_sequence=>7
,p_column_heading=>'Elimina'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:4:&SESSION.:ELIMINA:&DEBUG.:RP:P4_SEQ_ID:#ID#'
,p_column_linktext=>'#ELIMINA#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(102581464797442022)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(34246750362463065736)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Anular'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>'select * from apex_collections where collection_name = ''COL_CHEQUES_ANULAR'''
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(102581761418442024)
,p_branch_action=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(102581464797442022)
,p_branch_sequence=>3
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(102695678057395558)
,p_branch_action=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>4
,p_branch_comment=>'Created 03-DIC-2009 16:15 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44931459221636066)
,p_name=>'P4_RCA_ID'
,p_item_sequence=>44
,p_item_plug_id=>wwv_flow_imp.id(102552758882863198)
,p_prompt=>'Rca Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44943561139750138)
,p_name=>'P4_OBSERVACION'
,p_item_sequence=>54
,p_item_plug_id=>wwv_flow_imp.id(44929063245627756)
,p_use_cache_before_default=>'NO'
,p_item_default=>'EL CHEQUE TIENE RESPALDOS DE CARTERA VIGENTES, SI ANULA EL CHEQUE NO SE VA A CANCELAR EL VALOR DE LA CUOTA'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(64336380681828015)
,p_name=>'P4_RCA_FECHA_INGRESO'
,p_item_sequence=>34
,p_item_plug_id=>wwv_flow_imp.id(102750258923776014)
,p_prompt=>'Rca Fecha Ingreso'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102553067539865630)
,p_name=>'P4_CLI_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(102750258923776014)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cli Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT cli.cli_id',
'FROM   v_asdm_datos_clientes cli',
'WHERE  cli.per_nro_identificacion = :P4_PER_NRO_IDENTIFICACION',
'and cli.emp_id = :f_emp_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102553776197868115)
,p_name=>'P4_CRE_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(102750258923776014)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102554156242871807)
,p_name=>'P4_CRE_OBSERVACIONES'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(34246750362463065736)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Motivo Anulacion'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>50
,p_cMaxlength=>800
,p_cHeight=>2
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102667379499150043)
,p_name=>'P4_PV_ERROR'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(102813769735367637)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102762875379801672)
,p_name=>'P4_PER_NRO_IDENTIFICACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(102750258923776014)
,p_prompt=>'Nro Identificacion Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_CHEQUES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas_caja.fn_lov_cliente_con_cheques(:f_emp_id,:f_uge_id, :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cSize=>50
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit()"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102822871906425085)
,p_name=>'P4_PER_NOMBRE'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(102750258923776014)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cliente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TRIM(p.per_razon_social || '' '' || p.per_primer_apellido || '' '' ||',
'            p.per_segundo_apellido || '' '' || p.per_primer_nombre || '' '' ||',
'            p.per_segundo_nombre || '' '' || p.per_primer_apellido) nombre',
'  FROM asdm_personas p',
' WHERE p.per_nro_identificacion = :p4_per_nro_identificacion',
' and p.emp_id = :F_EMP_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104422256240797161)
,p_name=>'P4_CRE_NRO_CHEQUE'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(102552758882863198)
,p_prompt=>'Nro Cheque'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104981469376861468)
,p_name=>'P4_CRE_FECHA_DEPOSITO'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(102552758882863198)
,p_prompt=>'Fecha Deposito'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34246751101976065743)
,p_name=>'P4_SEQ_ID'
,p_item_sequence=>54
,p_item_plug_id=>wwv_flow_imp.id(102552758882863198)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34246752068055065753)
,p_name=>'P4_TITULAR'
,p_item_sequence=>64
,p_item_plug_id=>wwv_flow_imp.id(102552758882863198)
,p_prompt=>'Titular'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(34246752015409065752)
,p_validation_name=>'New'
,p_validation_sequence=>10
,p_validation=>'P4_CRE_OBSERVACIONES'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'DIGITE UNA OBSERVACION'
,p_when_button_pressed=>wwv_flow_imp.id(102581464797442022)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(34246750682835065739)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cheques'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number := 0;',
'begin',
'IF NOT APEX_COLLECTION.COLLECTION_EXISTS(''COL_CHEQUES_ANULAR'') THEN',
'APEX_COLLECTION.CREATE_COLLECTION(''COL_CHEQUES_ANULAR'');',
'END IF;',
'select count(*)',
'into ln_cuenta',
'from apex_collections where collection_name = ''COL_CHEQUES_ANULAR''',
'and n002 = :p4_cre_id;',
'if ln_cuenta = 0 then',
'apex_collection.add_member(p_collection_name => ''COL_CHEQUES_ANULAR'',',
'                           p_c001            => :P4_CRE_NRO_CHEQUE,',
'                           P_c002            => :P4_TITULAR,',
'                           p_n001            => :P4_CLI_ID,',
'                           p_n002            => :P4_CRE_ID,',
'                           p_d001            => :P4_CRE_FECHA_DEPOSITO);',
'end if;',
'end;',
'                           '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>34214497531565300813
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(34246751266168065745)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_eliminar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number := 0;',
'begin',
'select count(*)',
'into ln_cuenta',
'from apex_collections',
'where collection_name = ''COL_CHEQUES_ANULAR''',
'and seq_id = :p4_seq_id;',
'if ln_cuenta > 0 then',
'apex_collection.delete_member(''COL_CHEQUES_ANULAR'',:p4_seq_id);',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>34214498114898300819
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102667452272151592)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Graba_Anula_Cheques'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cursor datos is',
'select n002 cre_id, d001 fecha, :p4_cre_observaciones observacion',
'from apex_collections co',
'where co.collection_name = ''COL_CHEQUES_ANULAR'';',
'begin',
'for reg in datos loop',
'pq_ven_movimientos_caja.pr_posterga_anula_cheques(',
'                          reg.cre_id,--:P4_CRE_ID,              ',
'                          reg.fecha,--:P4_CRE_FECHA_DEPOSITO,',
'                          reg.observacion,--:P4_CRE_OBSERVACIONES, ',
'                          :F_EMP_ID,              ',
'                          :F_UGE_ID, ',
'                          :F_UGE_ID_GASTO,             ',
'                          :F_USER_ID, ',
'                          pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_anular_cheques''),',
'                          pq_constantes.fn_retorna_constante(NULL,''cn_ech_id_anulado''),',
'                          :P0_ERROR);',
'end loop;',
'apex_collection.delete_collection(''COL_CHEQUES_ANULAR'');',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(102581464797442022)
,p_process_success_message=>'Datos guardados correctamente'
,p_internal_uid=>70414301002386666
);
wwv_flow_imp.component_end;
end;
/
