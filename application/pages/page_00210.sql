prompt --application/pages/page_00210
begin
--   Manifest
--     PAGE: 00210
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>210
,p_name=>'Formularios Licitud de Fondos y Conozca a su Cliente'
,p_step_title=>'Formularios Licitud de Fondos y Conozca a su Cliente'
,p_reload_on_submit=>'A'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_required_role=>'MUST_NOT_BE_PUBLIC_USER'
,p_protection_level=>'C'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(52200558872726965)
,p_plug_name=>'Formularios LICITUD DE FONDOS y CONOZCA A SU CLIENTE'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN :p210_lfc_id_fac IS NOT NULL AND lfc.lfc_id = :p210_lfc_id_fac THEN',
'          ''<p style="color: #000000; background-color: #ffdddd; font-weight:bold">'' ||',
'          ''&#128681; '' || to_char((SELECT cx.com_id',
'                                    FROM car_cuentas_por_cobrar cx,',
'                                         car_movimientos_cab    cm,',
'                                         car_licit_fondos_cab   lf',
'                                   WHERE cx.cxc_id = cm.cxc_id',
'                        AND ((cm.mca_id_movcaja = lf.mca_com_id',
'                        AND lf.lfc_tipo = ''LDF'')',
'                        OR (lf.lfc_tipo = ''CSC'' AND cx.com_id = lf.mca_com_id))',
'                                     AND lf.lfc_id = lfc.lfc_id',
'                                   FETCH FIRST ROW ONLY)) || ''</p>''',
'         ELSE',
'          ''<p style="color: #000000; background-color: #ffdddd; font-weight:bold">'' ||',
'          ''&#128681; '' || to_char((SELECT cx.com_id',
'                                    FROM car_cuentas_por_cobrar cx,',
'                                         car_movimientos_cab    cm,',
'                                         car_licit_fondos_cab   lf',
'                                   WHERE cx.cxc_id = cm.cxc_id',
'                        AND ((cm.mca_id_movcaja = lf.mca_com_id',
'                        AND lf.lfc_tipo = ''LDF'')',
'                        OR (lf.lfc_tipo = ''CSC'' AND cx.com_id = lf.mca_com_id))',
'                                     AND lf.lfc_id = lfc.lfc_id',
'                                   FETCH FIRST ROW ONLY)) || ''</p>''',
'       END com_id,',
'       CASE WHEN lfc.lfc_tipo = ''LDF'' THEN',
'       ''<p style="color: #000000; background-color: #ff8533; font-weight:bold">'' ||',
'          ''LICITUD DE FONDOS'' || ''</p>''',
'          WHEN lfc.lfc_tipo = ''CSC'' THEN ',
'       ''<p style="color: #000000; background-color: #ff66b3; font-weight:bold">'' ||',
'          ''CONOZCA SU CLIENTE'' || ''</p>''',
'          END lfc_tipo,',
'       lfc.lfc_id,',
'       lfc.cli_id,',
'       lfc.mca_com_id,',
'       lfc.lfc_observacion obs,',
'       (SELECT u.uge_id || '' - '' || u.uge_nombre',
'          FROM asdm_unidades_gestion u',
'         WHERE u.uge_id = lfc.uge_id) unidad_gestion,',
'       (SELECT upper(u.username)',
'          FROM kseg_e.kseg_usuarios u',
'         WHERE u.usuario_id = lfc.usu_id) usuario,',
'       lfc.lfc_fecha,',
'       round(lfc.lfc_valor, 2) valor,',
'       apex_item.textarea(p_idx        => 1,',
'                          p_value      => lfc.lfc_observacion,',
'                          p_rows       => 1,',
'                          p_cols       => 20,',
'                          p_attributes => ''readonly="readonly"'') observacion,',
'       lfc.emp_id,',
'       CASE nvl(lfc.lfc_estado, ''I'')',
'         WHEN ''I'' THEN',
'          ''<p style="color: #000000; background-color: #fffcaa; font-weight:bold">'' ||',
'          ''&#9203; Ingresado'' || ''</p>''',
'         WHEN ''R'' THEN',
'          ''<p style="color: #000000; background-color: #d9f6ff; font-weight:bold">'' ||',
'          ''&#128270; Revision'' || ''</p>''',
'         WHEN ''C'' THEN',
'          ''<p style="color: #000000; background-color: #baff98; font-weight:bold">'' ||',
'          ''&#9989; Completado'' || ''</p>''',
'         WHEN ''D'' THEN',
'          ''<p style="color: #000000; background-color: #ffb9b9; font-weight:bold">'' ||',
'          ''&#9940; Devuelto'' || ''</p>''',
'       END estado,',
'       CASE nvl(lfc.lfc_estado_registro, ''0'')',
'         WHEN ''0'' THEN',
'          ''<p style="color: #000000; background-color: #e8ffdd; font-weight:bold">'' ||',
'          ''Activo'' || ''</p>''',
'         WHEN ''9'' THEN',
'          ''<p style="color: #000000; background-color: #ffdddd; font-weight:bold">'' ||',
'          ''Inactivo'' || ''</p>''',
'       END estado_registro,',
'       (SELECT a.age_nombre_comercial',
'          FROM asdm_agencias a',
'         WHERE a.age_id = lfc.age_id_agencia) agencia',
'  FROM asdm_e.car_licit_fondos_cab lfc',
' WHERE lfc_estado_registro = ''0''',
'   AND trunc(lfc.lfc_fecha) BETWEEN :P210_DESDE AND :P210_HASTA',
'   AND lfc.emp_id = :f_emp_id',
'   AND lfc.age_id_agencia = :f_age_id_agencia',
'   AND lfc.lfc_estado IN (''I'',''D'')',
' ORDER BY 1 DESC;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(52200708512726966)
,p_max_row_count=>'1000000'
,p_allow_report_saving=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_rows_per_page=>'N'
,p_show_sort=>'N'
,p_show_control_break=>'N'
,p_show_highlight=>'N'
,p_show_computation=>'N'
,p_show_aggregate=>'N'
,p_show_chart=>'N'
,p_show_group_by=>'N'
,p_show_pivot=>'N'
,p_show_flashback=>'N'
,p_show_reset=>'N'
,p_show_help=>'N'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:210:&SESSION.:cargar_data_cli:&DEBUG.:RP:P210_LFC_ID,P210_LFC,P210_VALOR,P210_OBSERVACION,P210_CLI_ID:#LFC_ID#,#LFC_ID#,#VALOR#,#OBS#,#CLI_ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="">'
,p_owner=>'RALVARADO'
,p_internal_uid=>19947557242962040
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(54873826770435274)
,p_db_column_name=>'LFC_ID'
,p_display_order=>10
,p_column_identifier=>'O'
,p_column_label=>'Lfc id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405156610847327)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>40
,p_column_identifier=>'R'
,p_column_label=>'UNIDAD GESTION'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405341819847328)
,p_db_column_name=>'USUARIO'
,p_display_order=>50
,p_column_identifier=>'S'
,p_column_label=>'USUARIO'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405526450847330)
,p_db_column_name=>'VALOR'
,p_display_order=>70
,p_column_identifier=>'U'
,p_column_label=>'MONTO ($)'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405614568847331)
,p_db_column_name=>'OBSERVACION'
,p_display_order=>80
,p_column_identifier=>'V'
,p_column_label=>'OBSERVACION'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405763164847333)
,p_db_column_name=>'EMP_ID'
,p_display_order=>100
,p_column_identifier=>'X'
,p_column_label=>'EMPRESA'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405852482847334)
,p_db_column_name=>'ESTADO'
,p_display_order=>110
,p_column_identifier=>'Y'
,p_column_label=>'ESTADO'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55405972232847335)
,p_db_column_name=>'ESTADO_REGISTRO'
,p_display_order=>120
,p_column_identifier=>'Z'
,p_column_label=>'ESTADO REGISTRO'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55406114250847336)
,p_db_column_name=>'AGENCIA'
,p_display_order=>130
,p_column_identifier=>'AA'
,p_column_label=>'AGENCIA'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55451695263109052)
,p_db_column_name=>'OBS'
,p_display_order=>140
,p_column_identifier=>'AB'
,p_column_label=>'Obs'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55756269700134530)
,p_db_column_name=>'COM_ID'
,p_display_order=>150
,p_column_identifier=>'AC'
,p_column_label=>'COM ID'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55756742866134534)
,p_db_column_name=>'CLI_ID'
,p_display_order=>160
,p_column_identifier=>'AG'
,p_column_label=>'CLIENTE ID'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55756921328134536)
,p_db_column_name=>'LFC_FECHA'
,p_display_order=>180
,p_column_identifier=>'AI'
,p_column_label=>'FECHA'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55076637307016743)
,p_db_column_name=>'MCA_COM_ID'
,p_display_order=>190
,p_column_identifier=>'AJ'
,p_column_label=>'MCA ID/COM ID'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(55076839463016745)
,p_db_column_name=>'LFC_TIPO'
,p_display_order=>200
,p_column_identifier=>'AK'
,p_column_label=>'TIPO FORMULARIO'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(55210255433285558)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'229572'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'LFC_TIPO:COM_ID:CLI_ID:UNIDAD_GESTION:AGENCIA:VALOR:USUARIO:ESTADO:ESTADO_REGISTRO:LFC_FECHA:MCA_COM_ID:'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(55406202897847337)
,p_plug_name=>'Carga Archivo'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_plug_display_when_condition=>'cargar_data_cli'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(55757033456134537)
,p_plug_name=>'Filtros'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(55408696454847362)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_button_name=>'BT_GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar Archivo'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55075279275016730)
,p_name=>'P210_LFC_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55406278983847338)
,p_name=>'P210_ARCHIVO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_prompt=>'Seleccione archivo'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'APEX_APPLICATION_TEMP_FILES'
,p_attribute_09=>'SESSION'
,p_attribute_10=>'N'
,p_attribute_12=>'NATIVE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55450928643109044)
,p_name=>'P210_LFC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_prompt=>'LFC_ID'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-size:15;font-weight: bold;color:RED;font-family:Arial" readonly="readonly"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55451026771109045)
,p_name=>'P210_CLIENTE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-size:15;font-weight: bold;color:BLUE;font-family:Arial" readonly="readonly"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55451116575109046)
,p_name=>'P210_IDENTIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_prompt=>'Identificacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-size:15;font-weight: bold;color:BLUE;font-family:Arial" readonly="readonly"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55451217554109047)
,p_name=>'P210_VALOR'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_prompt=>'Valor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-size:15;font-weight: bold;color:BLUE;font-family:Arial" readonly="readonly"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55451396871109049)
,p_name=>'P210_OBSERVACION'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_prompt=>'Observacion'
,p_placeholder=>'Ingrese observacion...'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cMaxlength=>250
,p_cHeight=>3
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55451514942109050)
,p_name=>'P210_CLI_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(55406202897847337)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55756059984134528)
,p_name=>'P210_LFC_ID_FAC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(52200558872726965)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55757065073134538)
,p_name=>'P210_DESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(55757033456134537)
,p_prompt=>'Desde'
,p_source=>'sysdate - 5'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_tag_attributes=>'readonly="readonly"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55757218536134539)
,p_name=>'P210_HASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(55757033456134537)
,p_prompt=>'Hasta'
,p_source=>'to_date(nvl(:P210_DESDE,sysdate),''dd/mm/yyyy'') + 5'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'readonly="readonly"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(55757311580134540)
,p_name=>'da_chn_date_des'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P210_DESDE'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(55757361708134541)
,p_event_id=>wwv_flow_imp.id(55757311580134540)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P210_HASTA'
,p_attribute_01=>'PLSQL_EXPRESSION'
,p_attribute_04=>'to_date(:P210_DESDE,''dd/mm/yyyy'') + 5'
,p_attribute_07=>'P210_DESDE'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(55757522072134542)
,p_event_id=>wwv_flow_imp.id(55757311580134540)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(55757833801134545)
,p_name=>'da_pl_change_desde_ini'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(55757861758134546)
,p_event_id=>wwv_flow_imp.id(55757833801134545)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P210_HASTA'
,p_attribute_01=>'PLSQL_EXPRESSION'
,p_attribute_04=>'to_date(:P210_DESDE,''dd/mm/yyyy'') + 5'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(55451607263109051)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_data_cli'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT p.per_nro_identificacion,',
'         CASE',
'           WHEN p.per_razon_social IS NOT NULL THEN',
'            p.per_razon_social',
'           ELSE',
'            p.per_primer_apellido || '' '' || p.per_segundo_apellido || '' '' ||',
'            p.per_primer_nombre || '' '' || p.per_segundo_nombre',
'         END cliente',
'    INTO :p210_identificacion, :p210_cliente',
'    FROM asdm_clientes c, asdm_personas p',
'   WHERE c.per_id = p.per_id',
'     AND c.cli_id = :p210_cli_id',
'     AND p.per_estado_registro = 0',
'     AND p.emp_id = :f_emp_id',
'   FETCH FIRST ROW ONLY;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    :p210_identificacion := NULL;',
'    :p210_cliente        := NULL;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cargar_data_cli'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>23198455993344125
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(55075598151016733)
,p_process_sequence=>10
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar_archivo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
unistr(' Creaci\00F3n:    08/03/22'),
' Autor:       FPALOMEQUE',
' Descripcion: Proceso de actualizacion de archivo y observacion en tabla',
' Historia:    R3603-02',
'*/',
'DECLARE',
'  l_clob CLOB;',
'  l_blob BLOB;',
'BEGIN',
'  IF length(:p210_observacion) > 298 THEN',
'    apex_application.g_print_success_message := ''<span style="color:RED">'' ||',
'                                                ''Campo observacion es demasiado largo!</span>'';',
'  ELSE',
'    BEGIN',
'      SELECT blob_content',
'        INTO l_blob',
'        FROM apex_application_temp_files',
'       WHERE NAME = :p210_archivo',
'         AND application_id = 211',
'       FETCH FIRST ROW ONLY;',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        l_blob := NULL;',
'    END;',
'  ',
'    IF l_blob IS NOT NULL THEN',
'      BEGIN',
'        l_clob := apex_web_service.blob2clobbase64(l_blob);',
'      EXCEPTION',
'        WHEN OTHERS THEN',
'          apex_application.g_print_success_message := ''<span style="color:RED">'' ||',
'                                                      ''No se pudo cargar archivo, error al convertir BLOB a CLOB BASE64</span>'';',
'      END;',
'    ',
'      IF :p210_lfc_id IS NOT NULL THEN',
'        pq_car_licit_fondos_cab_dml.pr_upd_car_licit_fondos_cab(pn_lfc_id          => :p210_lfc_id,',
'                                                                pv_lfc_observacion => :p210_observacion,',
'                                                                pc_lfc_archivo     => l_clob,',
'                                                                pv_lfc_estado      => ''R'',',
'                                                                pn_usu_id          => :f_user_id,',
'                                                                pc_error           => :p0_error);',
'      ',
'        IF :p0_error IS NULL THEN',
'          apex_application.g_print_success_message := ''<span style="color:GREEN">'' ||',
'                                                      ''Archivo cargado correctamente, lfc_id: '' ||',
'                                                      :p210_lfc_id ||',
'                                                      ''</span>'';',
'        ELSE',
'          apex_application.g_print_success_message := ''<span style="color:RED">'' ||',
'                                                      ''Error al cargar archivo: '' ||',
'                                                      :p0_error ||',
'                                                      ''</span>'';',
'        ',
'        END IF;',
'      END IF;',
'    ELSE',
'      apex_application.g_print_success_message := ''<span style="color:RED">'' ||',
'                                                  ''No se pudo cargar archivo, error al convertir archivo a BLOB</span>'';',
'    END IF;',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(55408696454847362)
,p_internal_uid=>22822446881251807
);
wwv_flow_imp.component_end;
end;
/
