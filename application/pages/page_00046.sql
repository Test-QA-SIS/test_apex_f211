prompt --application/pages/page_00046
begin
--   Manifest
--     PAGE: 00046
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>46
,p_name=>'Forma ASDM_CHEQUES_RECIBIDOS'
,p_step_title=>'Forma Asdm Cheques Recibidos'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script>',
'history.forward(); ',
'</script> ',
'<script type="text/javascript">',
'var numero_filas=300;',
'',
'function refresh_ir_report()',
'   {',
'jQuery(''#R63167345773750382346_ir'').data(''apex-interactiveReport'').refresh ();',
'//apex.event.trigger(''#R403532932735321084_ir'', ''apexrefresh'');',
'  }',
'',
'function actualiza_region()',
'{ ',
'//refresh_ir_report();',
'$(''#respaldo'').trigger(''apexrefresh'');',
'//$a_report(''11633812170107847'',''1'',numero_filas,''15'');',
'}',
'function actualiza_collection(fila,seq_id)',
'{',
'    ',
'//alert('' aqui pantalla '');    ',
'    ',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_CARGA_COLL_DIV_RES'',0);',
'  ',
'  get.addParam(''x10'',seq_id);',
'  get.addParam(''x03'',$x(''f43_'' + fila).value);',
'  get.addParam(''x04'',$x(''P46_CRE_VALOR'').value);',
'  gReturn = get.get();',
'  get = null;',
'  $x_Value(''P46_VALOR_TOTAL'',gReturn);',
'',
'  actualiza_region();',
'}',
'',
'function enter2tab(evt,itemid)',
'{',
'    if (evt.keyCode == 13)',
'   {',
'     $x(itemid).focus();',
'     return false;',
'   }',
' return true;',
'}',
'',
'function valida_numEnterTab(evt,itemid,ln_valor){',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'  if (evt.keyCode == 13) {',
'     ',
'        $x(itemid).focus();',
'        return false;',
'           ',
'   }else if(evt.keyCode == 8){',
'       return true;',
'   }else if(evt.keyCode == 9){',
'       return true;',
'   }else if(evt.keyCode == null){',
'       return true;  ',
'   }else{',
'       if (!patron_numero.test((ln_valor).value)) {',
unistr('        alert(''Ingrese solo n\00FAmeros'');'),
'        ln_valor.value = '''';',
'        html_GetElement((ln_valor).id).focus();',
'        return true;',
'        } ',
'    }',
' return false;',
'}',
'',
'function actualiza(fila, respaldo, valor_cuota, seq_id, valor_presente)',
'{',
'if (valor_presente < 0){alert(''Debe ingresar valores mayores a cero'');}',
'ln_resta = valor_cuota - respaldo;',
'var ln_decimal=parseFloat(ln_resta);',
'var ln_resultado=Math.round(ln_decimal*100)/100 ;',
'',
'if (valor_presente > ln_resultado){',
'alert(''Valor no puede ser superior a ''+ ln_resultado);',
'actualiza_collection(fila);',
'}else{',
'actualiza_collection(fila,seq_id);}',
'var ln_total = parseFloat(html_GetElement(''P46_VALOR_TOTAL'').value);',
'var ln_total_forma = parseFloat(html_GetElement(''P46_CRE_VALOR'').value);',
'if ( ln_total > ln_total_forma){',
'alert(ln_total + '' supera el valor del cheque ''+ln_total_forma);',
'}',
'}',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function cambiaTamanio(){',
'  logo = document.getElementById(''P46_FIRMA'');',
'  logo.width = 200;',
'  logo.height = 200;',
'}',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20240110153235'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(43886963439872773)
,p_name=>'Respaldo Dividendos'
,p_region_name=>'respaldo'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(to_number(c003)) total_respaldado, (NVL(:P46_CRE_VALOR,0)-sum(to_number(c003))) saldo    ',
'from apex_collections where collection_name=''COLL_RESPALDO_DIV'''))
,p_display_when_condition=>':P46_ECH_ID=pq_constantes.fn_retorna_constante(null,''cn_ech_id_ingresado'') or :P46_ECH_ID=pq_constantes.fn_retorna_constante(null,''cn_ech_id_postergado'')'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43894273145951288)
,p_query_column_id=>1
,p_column_alias=>'TOTAL_RESPALDADO'
,p_column_display_sequence=>1
,p_column_heading=>'Total Respaldado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77104573178697053)
,p_query_column_id=>2
,p_column_alias=>'SALDO'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(74140071860800560)
,p_plug_name=>'respaldos'
,p_parent_plug_id=>wwv_flow_imp.id(43886963439872773)
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c002 div_id,',
'       c001 cxc_id,',
'       c008 factura,',
'       to_date(c005,''dd-mm-yyyy'') fec_vencimiento,',
'       c004 nro_vencimiento,',
'       c007 respaldo,',
'       to_number(c006) valor_cuota,',
'       ',
'       ',
'       apex_item.text(p_idx        => 25,',
'                      p_value      => c003,',
'                      p_attributes => ''unreadonly id="f43_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c007||'',''||c006||'',''||seq_id||'',this.value)"''/*,',
'                      p_size       => ''10''*/) valor_respaldado,',
'      c009 sec_fact,',
'      c010 cod_cred',
'                      ',
'                      ',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_RESPALDO_DIV'''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'NEVER'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(72635690932514645336)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'RULLOA'
,p_internal_uid=>72603437781244880410
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691024433645337)
,p_db_column_name=>'DIV_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Div id'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691069393645338)
,p_db_column_name=>'CXC_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cxc id'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691173348645339)
,p_db_column_name=>'FACTURA'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Factura'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691340147645340)
,p_db_column_name=>'FEC_VENCIMIENTO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Fec vencimiento'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691372157645341)
,p_db_column_name=>'NRO_VENCIMIENTO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Nro vencimiento'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691542206645342)
,p_db_column_name=>'RESPALDO'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Respaldo'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691591267645343)
,p_db_column_name=>'VALOR_CUOTA'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Valor cuota'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691735953645344)
,p_db_column_name=>'VALOR_RESPALDADO'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Valor respaldado'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691847650645345)
,p_db_column_name=>'SEC_FACT'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Sec fact'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72635691907904645346)
,p_db_column_name=>'COD_CRED'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Cod cred'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(72699532405781986941)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'726672793'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'DIV_ID:CXC_ID:FACTURA:FEC_VENCIMIENTO:NRO_VENCIMIENTO:RESPALDO:VALOR_CUOTA:VALOR_RESPALDADO:SEC_FACT:COD_CRED'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(63199598925020147272)
,p_plug_name=>'respaldos'
,p_parent_plug_id=>wwv_flow_imp.id(43886963439872773)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c002 div_id,',
'       c001 cxc_id,',
'       c004 nro_vencimiento,',
'       to_date(c005,''dd-mm-yyyy'') fec_vencimiento,',
'       c007 respaldo,',
'       to_number(c006) valor_cuota,',
'       c008 factura,',
'       ',
'       apex_item.text(p_idx        => 25,',
'                      p_value      => c003,',
'                      p_attributes => ''unreadonly id="f43_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c007||'',''||c006||'',''||seq_id||'',this.value)"''/*,',
'                      p_size       => ''10''*/) valor_respaldado,',
'      c009 sec_fact,',
'      c010 cod_cred',
'                      ',
'                      ',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_RESPALDO_DIV'''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(63199599013768147273)
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_owner=>'RULLOA'
,p_internal_uid=>63167345862498382347
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63199599067906147274)
,p_db_column_name=>'DIV_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63199599182032147275)
,p_db_column_name=>'CXC_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63199599269403147276)
,p_db_column_name=>'NRO_VENCIMIENTO'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Nro Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721168398604327)
,p_db_column_name=>'VALOR_RESPALDADO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Valor Respaldado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'VALOR_RESPALDADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721346454604328)
,p_db_column_name=>'RESPALDO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Respaldo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'RESPALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721439283604329)
,p_db_column_name=>'FACTURA'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Factura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721497430604330)
,p_db_column_name=>'SEC_FACT'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Sec Fact'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'SEC_FACT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721553483604331)
,p_db_column_name=>'COD_CRED'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Cod Cred'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'COD_CRED'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721666197604332)
,p_db_column_name=>'FEC_VENCIMIENTO'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Fec Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FEC_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72634721775314604333)
,p_db_column_name=>'VALOR_CUOTA'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Valor Cuota'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(72634837162390609026)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'726025841'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'SEC_FACT:COD_CRED:FACTURA:CXC_ID:DIV_ID:NRO_VENCIMIENTO:FEC_VENCIMIENTO:RESPALDO:VALOR_CUOTA:VALOR_RESPALDADO::APXWS_CC_001'
);
wwv_flow_imp_page.create_worksheet_computation(
 p_id=>wwv_flow_imp.id(72651272880384359907)
,p_report_id=>wwv_flow_imp.id(72634837162390609026)
,p_db_column_name=>'APXWS_CC_001'
,p_column_identifier=>'C01'
,p_computation_expr=>'J -  E'
,p_column_type=>'NUMBER'
,p_column_label=>'Saldo'
,p_report_label=>'Saldo'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(72649063580908257959)
,p_application_user=>'RULLOA'
,p_name=>'R1'
,p_description=>'R1'
,p_report_seq=>10
,p_report_alias=>'726168105'
,p_status=>'PUBLIC'
,p_report_columns=>'SEC_FACT:COD_CRED:FACTURA:CXC_ID:DIV_ID:NRO_VENCIMIENTO:FEC_VENCIMIENTO:RESPALDO:VALOR_CUOTA:VALOR_RESPALDADO::APXWS_CC_001'
);
wwv_flow_imp_page.create_worksheet_computation(
 p_id=>wwv_flow_imp.id(72649993436887300265)
,p_report_id=>wwv_flow_imp.id(72649063580908257959)
,p_db_column_name=>'APXWS_CC_001'
,p_column_identifier=>'C01'
,p_computation_expr=>'J -  E'
,p_column_type=>'NUMBER'
,p_column_label=>'Saldo'
,p_report_label=>'Saldo'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44072162260064593)
,p_name=>'Cuotas Respaldadas'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cxc.cxc_id                cxc_id,',
'       div.div_id                div_id,',
'       rdi.rdi_valor_respaldado  valor_respaldado,',
'       div.div_nro_vencimiento   nro_vencimiento,',
'       div.div_fecha_vencimiento fecha_vencimiento',
'  FROM car_cuentas_por_cobrar   cxc,',
'       car_dividendos           div,',
'       car_respaldos_dividendos rdi',
' WHERE cxc.cxc_id = div.cxc_id',
'   AND cxc.emp_id = :f_emp_id',
'   AND cxc.cli_id = :p46_cli_id',
'   AND rdi.div_id = div.div_id',
'   AND nvl(rdi.rca_id, 0) = nvl(:p46_rca_id, 0)'))
,p_display_when_condition=>':P46_ECH_ID not in (pq_constantes.fn_retorna_constante(null,''cn_ech_id_ingresado'') , pq_constantes.fn_retorna_constante(null,''cn_ech_id_postergado''))'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44072458282064762)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44072562005064899)
,p_query_column_id=>2
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>2
,p_column_heading=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44072672827064899)
,p_query_column_id=>3
,p_column_alias=>'VALOR_RESPALDADO'
,p_column_display_sequence=>3
,p_column_heading=>'VALOR_RESPALDADO'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44072782482064899)
,p_query_column_id=>4
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44072879827064899)
,p_query_column_id=>5
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(62238457640963228)
,p_plug_name=>'Datos del Cheque'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(76148371182340369)
,p_name=>'Firmas Clientes'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT rel.per_id,',
'       rel.cli_id,',
'       trim(pers.per_razon_social || '' '' || pers.per_primer_nombre || '' '' || pers.per_segundo_nombre || '' '' ||',
'       pers.per_primer_apellido || '' '' || pers.per_segundo_apellido) nombre,',
'            --  dbms_lob.getlength(rel.rpe_imagen_firma) rpe_imagen_firma,',
'       decode(nvl(dbms_lob.getlength(rel.rpe_imagen_firma), 0),',
'              0,',
'              NULL,',
'              ''<img src="'' ||',
'              apex_util.get_blob_file_src(''P54_RPE_IMAGEN_FIRMA'', rel.rpe_id) ||',
'              ''" height="150" width="200"/>'') Firma',
'',
'  FROM ASDM_RELACIONES_PERSONAS rel, asdm_personas pers',
' WHERE rel.per_id = pers.per_id',
'   and pers.PER_ESTADO_REGISTRO =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and rel.cli_id = :p46_cli_id',
'   and (rel.rpe_es_firma_cheque = ''S'' or rel.rpe_es_firma_conjunta = ''S'');',
'',
' '))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270529077351046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(76239280625099994)
,p_query_column_id=>1
,p_column_alias=>'PER_ID'
,p_column_display_sequence=>1
,p_column_heading=>'ID Persona'
,p_use_as_row_header=>'N'
,p_print_col_width=>'22'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(76257759088264064)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_print_col_width=>'22'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(76257859076264065)
,p_query_column_id=>3
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>3
,p_column_heading=>'Nombres'
,p_use_as_row_header=>'N'
,p_print_col_width=>'22'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(76257970628264065)
,p_query_column_id=>4
,p_column_alias=>'FIRMA'
,p_column_display_sequence=>4
,p_column_heading=>'Firma'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_cattributes=>'onMouseOver= ''cambiatamanio()'''
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'22'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(77817774686441480)
,p_name=>unistr('Facturas Pagar\00BF\00BF')
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''NroFactura: '' || com.com_numero || ''   Fecha: '' || com.com_fecha ||',
'       ''   Total: '' || vco.vco_valor_variable factura,',
'       com.com_id referencia, APEX_ITEM.CHECKBOX(11,COM.COM_ID) FACTURA_PAGARE,',
'       APEX_ITEM.HIDDEN(13,vco.vco_valor_variable)VALOR,',
'       APEX_ITEM.HIDDEN(15,to_date(add_months(com.com_fecha, com.com_plazo),''DD-MM-YYYY''))FECHA_VENCE',
'  from ven_comprobantes com, ven_var_comprobantes vco',
' where com.cli_id = nvl(:p46_cli_id, 0)',
'and vco.com_id = com.com_id',
' and vco.emp_id = com.emp_id',
' and vco.var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total'')',
' and com.com_estado_registro =',
' pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
' and com.emp_id = :f_emp_id',
'and not exists (select ''x''',
'          from car_otros_respaldos ore',
'         where com.com_id = ore.com_id)',
''))
,p_display_when_condition=>':f_seg_id = 1'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77836381902689605)
,p_query_column_id=>1
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>3
,p_column_heading=>'Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77836479702690313)
,p_query_column_id=>2
,p_column_alias=>'REFERENCIA'
,p_column_display_sequence=>4
,p_column_heading=>'Referencia'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77849469631225252)
,p_query_column_id=>3
,p_column_alias=>'FACTURA_PAGARE'
,p_column_display_sequence=>5
,p_column_heading=>'Factura Pagare'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(79241082261546000)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(79241176263546002)
,p_query_column_id=>5
,p_column_alias=>'FECHA_VENCE'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(77836963333693619)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name = ''COLL_RESPALDO_DIV'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837270860693635)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837374827693642)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837483102693642)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837558920693642)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837654350693642)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837780853693642)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837874952693642)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77837962530693642)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838063569693642)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838152249693642)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838254643693642)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838365485693642)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838451397693642)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838566019693642)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838681253693642)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838761073693642)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838860921693642)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77838979109693643)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839075919693643)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839170654693643)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839254723693643)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839382542693643)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839463453693643)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839581513693643)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839680115693643)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839770538693643)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839877627693643)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77839983851693643)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840052474693643)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840152953693643)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840269096693643)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840382999693643)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840460887693643)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840560883693643)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840672959693643)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840777659693644)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840852291693644)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77840978842693644)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841057410693644)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841176859693644)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841260445693644)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841382753693644)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841470954693644)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841580873693644)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841665304693644)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841751613693644)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841876845693644)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77841966055693644)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842059299693644)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842166537693644)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842259471693644)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842362656693645)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842461464693645)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842570144693645)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842653484693645)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842770661693645)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842866089693645)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77842955317693645)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843057796693645)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843166565693645)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843260040693645)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843359734693645)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843475079693645)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843575503693645)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843662678693645)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(77843779809693645)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(62239761548963237)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_button_name=>'CANCEL'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'CLOSE'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(62239975341963239)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'CREATE'
,p_button_condition=>':P46_CRE_ID IS NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_database_action=>'INSERT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(62240153792963239)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_button_name=>'GRABAR_CAMBIOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar Cambios'
,p_button_position=>'CREATE'
,p_button_condition=>':P46_CRE_ID IS NOT NULL AND (:P46_ECH_ID=pq_constantes.fn_retorna_constante(null,''cn_ech_id_ingresado'') OR :P46_ECH_ID=pq_constantes.fn_retorna_constante(null,''cn_ech_id_postergado''))'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(62246462547963257)
,p_branch_action=>'f?p=&APP_ID.:45:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(62240153792963239)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(62436171743058114)
,p_branch_action=>'f?p=&APP_ID.:45:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(62239761548963237)
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 21-OCT-2010 11:22 by PLOPEZ'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(44112654125800169)
,p_branch_action=>'f?p=&APP_ID.:45:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(62239975341963239)
,p_branch_sequence=>210
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 14-FEB-2011 16:58 by JARIAS'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(62246672197963257)
,p_branch_action=>'f?p=&APP_ID.:46:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1000
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62240382837963239)
,p_name=>'P46_ECH_ID'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_item_default=>'pq_constantes.fn_retorna_constante(null,''cn_ech_id_ingresado'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Ech Id'
,p_source=>'ECH_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62240564551963240)
,p_name=>'P46_CRE_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cheque Id'
,p_source=>'CRE_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62240760051963240)
,p_name=>'P46_EMP_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_item_default=>':f_emp_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Emp Id'
,p_source=>'EMP_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62240983418963240)
,p_name=>'P46_EDE_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Entidad Financiera'
,p_source=>'EDE_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ENTIDADES_FINANCIERAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'RETURN (',
'pq_car_credito_interfaz.fn_retorna_entidad_destino',
'                  (pq_constantes.fn_retorna_constante(null,''cv_tede_entidad_financiera''),',
'                   :f_emp_id));',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--Seleccione--'
,p_cSize=>30
,p_tag_attributes=>'onkeyup="return enter2tab(event,''P46_CRE_NRO_CUENTA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62241155133963241)
,p_name=>'P46_CRE_ID_REEMPLAZADO'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Id Reemplazado'
,p_source=>'CRE_ID_REEMPLAZADO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62241351843963242)
,p_name=>'P46_RCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Respaldo Id'
,p_source=>'RCA_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62241577252963242)
,p_name=>'P46_CRE_NRO_CHEQUE'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Nro Cheque'
,p_source=>'CRE_NRO_CHEQUE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>20
,p_cHeight=>1
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P46_CRE_NRO_PIN'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62241770006963242)
,p_name=>'P46_CRE_NRO_CUENTA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Nro Cuenta'
,p_source=>'CRE_NRO_CUENTA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>20
,p_cHeight=>1
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P46_CRE_NRO_CHEQUE'', this)"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62241978718963242)
,p_name=>'P46_CRE_NRO_PIN'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Nro Pin'
,p_source=>'CRE_NRO_PIN'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>20
,p_cHeight=>1
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P46_CRE_TITULAR_CUENTA'', this)"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62242152431963242)
,p_name=>'P46_CRE_VALOR'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>'0'
,p_prompt=>'Valor'
,p_source=>'CRE_VALOR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_tag_attributes=>'onchange="javascript: patronfecha = /^\d{1,2}\-\d{1,2}\-\d{2,4}$/; if (!patronfecha.test(this.value)) { 	alert('' Fecha Incorrecta ''); 	this.value = '''';  {    html_GetElement(''P46_CRE_FECHA_DEPOSITO'').focus();  } }else{  {    html_GetElement(''P46_NNNN'
||''').focus();  }   }"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'p46_cre_id'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62242372995963243)
,p_name=>'P46_CRE_TITULAR_CUENTA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>':P46_NOMBRE_CLIENTE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Titular Cuenta'
,p_source=>'CRE_TITULAR_CUENTA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>100
,p_cHeight=>1
,p_tag_attributes=>'onkeyup="return enter2tab(event,''P46_CRE_FECHA_DEPOSITO'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62242572979963243)
,p_name=>'P46_CRE_TIPO'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_item_default=>'pq_constantes.fn_retorna_constante(null,''cv_tch_posfechado'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tipo'
,p_source=>'CRE_TIPO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62242771237963243)
,p_name=>'P46_CRE_FECHA_DEPOSITO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Fecha Deposito'
,p_source=>'CRE_FECHA_DEPOSITO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>255
,p_cHeight=>1
,p_tag_attributes=>'onchange="javascript: patronfecha = /^\d{1,2}\-\d{1,2}\-\d{2,4}$/; if (!patronfecha.test(this.value)) { 	alert('' Fecha Incorrecta ''); 	this.value = '''';  {    html_GetElement(''P46_CRE_FECHA_DEPOSITO'').focus();  } }else{  {    html_GetElement(''P46_CRE_'
||'VALOR'').focus();  }   }"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when=>'p46_cre_id'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62242981236963243)
,p_name=>'P46_CRE_ESTADO_REGISTRO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cre Estado Registro'
,p_source=>'CRE_ESTADO_REGISTRO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTADO_REGISTRO1'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
'lv_lov:=''select ''''--Estado Registro--'''' d, null r from dual',
'union  '';',
' lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ESTADO_REG'');',
'return (lv_lov);',
'END;',
''))
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>'esta como no visible porque se va a utilizar el estado registro de respaldos cartera'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62243162218963244)
,p_name=>'P46_CLI_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>':P3_CLI_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Cli Id'
,p_source=>'CLI_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>5
,p_cMaxlength=>255
,p_cHeight=>1
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62243367262963245)
,p_name=>'P46_CRE_BENEFICIARIO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_beneficiario asdm_cheques_recibidos.cre_beneficiario%TYPE;',
'begin',
'select emp_razon_social ',
'into ln_beneficiario',
'from asdm_empresas',
'where emp_id = :f_emp_id;',
'return ln_beneficiario;',
'end;'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Beneficiario'
,p_source=>'CRE_BENEFICIARIO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>100
,p_cHeight=>1
,p_tag_attributes=>'onkeyup="return enter2tab(event,''P46_CRE_FECHA_DEPOSITO'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62243552744963245)
,p_name=>'P46_RCA_ID_REEMPLAZADO'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Respaldo Reemplazado'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62243776685963245)
,p_name=>'P46_RCA_TIPO_RESPALDO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>'pq_constantes.fn_retorna_constante(null,''cv_rca_tipo_cheque'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tipo Respaldo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62243983803963245)
,p_name=>'P46_RCA_FECHA_INGRESO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Ingreso'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62244178036963246)
,p_name=>'P46_RCA_ESTADO_REGISTRO'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_item_default=>'0'
,p_prompt=>'Estado Registro'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'disabled = ''disabled'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62244380430963246)
,p_name=>'P46_NOMBRE_CLIENTE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cliente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli.nombre_completo',
'  FROM v_asdm_datos_clientes cli',
' WHERE cli_id = :p46_cli_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62244951604963248)
,p_name=>'P46_CRE_OBSERVACIONES'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Observaciones'
,p_source=>'CRE_OBSERVACIONES'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>40
,p_cMaxlength=>2000
,p_cHeight=>2
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75132359873810762)
,p_name=>'P46_VALOR_TOTAL'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_use_cache_before_default=>'NO'
,p_item_default=>'0'
,p_prompt=>'Valor Total'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(77129269573960918)
,p_name=>'P46_MENSAJE'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(62238457640963228)
,p_prompt=>'Presione Enter <br>para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" style="color: red" '
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(62245259011963248)
,p_computation_sequence=>10
,p_computation_item=>'P46_NOMBRE_CLIENTE'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'QUERY'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli.nombre_completo',
'  FROM v_asdm_datos_clientes cli',
' WHERE cli_id = :p46_cli_id'))
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(79073252838162824)
,p_validation_name=>'P46_EDE_ID'
,p_validation_sequence=>10
,p_validation=>'P46_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe seleccionar una entidad'
,p_when_button_pressed=>wwv_flow_imp.id(62239975341963239)
,p_associated_item=>wwv_flow_imp.id(62240983418963240)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(79073876517179123)
,p_validation_name=>'P46_CRE_FECHA_DEPOSITO'
,p_validation_sequence=>20
,p_validation=>'to_date(:P46_CRE_FECHA_DEPOSITO,''DD-MM-YYYY'') >= to_date(SYSDATE,''DD-MM-YYYY'')'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Debe ingresar una fecha mayor o igual a hoy'
,p_when_button_pressed=>wwv_flow_imp.id(62239975341963239)
,p_associated_item=>wwv_flow_imp.id(62242771237963243)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(75392552760462173)
,p_name=>'prueba'
,p_event_sequence=>10
,p_triggering_element_type=>'REGION'
,p_triggering_region_id=>wwv_flow_imp.id(43886963439872773)
,p_triggering_condition_type=>'GREATER_THAN'
,p_triggering_expression=>':p46_cred_valor'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(75392870858462185)
,p_event_id=>wwv_flow_imp.id(75392552760462173)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_ALERT'
,p_attribute_01=>'mayore'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(62245976987963257)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from ASDM_CHEQUES_RECIBIDOS'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'ASDM_CHEQUES_RECIBIDOS'
,p_attribute_03=>'P46_CRE_ID'
,p_attribute_04=>'CRE_ID'
,p_process_error_message=>'Unable to fetch row.'
,p_internal_uid=>29992825718198331
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(62246181249963257)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_resp_cheque'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'',
'BEGIN',
'',
' pq_car_cartera.pr_graba_cheques_posfechados(pn_emp_id              => :f_emp_id,',
'                                              pn_rca_id_reemplazado  => :p46_rca_id_reemplazado,',
'                                              pv_rca_tipo_respaldo   => :p46_rca_tipo_respaldo,',
'                                              pd_rca_fecha_ingreso   => :p46_rca_fecha_ingreso,',
'                                              pv_rca_estado_registro => :p46_rca_estado_registro,',
'                                              pn_cli_id              => :p46_cli_id,',
'                                              pn_ede_id              => :p46_ede_id,',
'                                              pn_cre_id_reemplazado  => :p46_cre_id_reemplazado,',
'                                              pn_ech_id              => :p46_ech_id,',
'                                              pv_cre_nro_cheque      => :p46_cre_nro_cheque,',
'                                              pv_cre_nro_cuenta      => :p46_cre_nro_cuenta,',
'                                              pv_cre_nro_pin         => :p46_cre_nro_pin,',
'                                              pn_cre_valor           => :p46_cre_valor,',
'                                              pv_cre_titular_cuenta  => :p46_cre_titular_cuenta,',
'                                              pv_cre_observaciones   => :p46_cre_observaciones,',
'                                              pv_cre_tipo            => :p46_cre_tipo,',
'                                              pd_cre_fecha_deposito  => :p46_cre_fecha_deposito,',
'                                              pv_cre_estado_registro => :p46_rca_estado_registro,',
'                                              pv_cre_beneficiario    => :p46_cre_beneficiario,',
'                                              pv_error               => :P0_error,',
'                                              pn_uge_id              => :f_uge_id,',
'                                              pn_usu_id              => :f_user_id,',
'                                              pn_uge_id_gasto        => :f_uge_id_gasto,',
'                                              pn_pue_id              => :p0_pue_id,',
'                                              pv_nombre_coll_div     => ''COLL_RESPALDO_DIV'');',
'',
'if :p0_error is null then ',
'--aumentado por Andres Calle',
'  ',
'  pq_car_cartera.pr_graba_factura_pagare(pn_emp_id               => :f_emp_id,',
'                                          pn_rca_id_reemplazado   => :p46_rca_id_reemplazado,',
'                                          pv_rca_tipo_respaldo    => pq_constantes.fn_retorna_constante(0,''cv_rca_tipo_otros''),',
'                                          pv_rca_estado_registro  => :p46_rca_estado_registro,',
'                                          pn_cli_id               => :p46_cli_id,',
'                                          pv_ore_tipo_respaldo    => ''FP'',                                          ',
'                                          pn_uge_id               => :f_uge_id,',
'                                          pn_usu_id               => :f_user_id,',
'                                          pn_uge_id_gasto         => :f_uge_id_gasto,',
'                                          pv_error                => :P0_error);',
'end if;',
'  --no se va a utilizar el estado registro del cheque, ',
'  --es para tener un solo estado registro en la pantalla',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(62239975341963239)
,p_internal_uid=>29993029980198331
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(62245767587963256)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualizar_resp_cheque'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'BEGIN',
'  pq_car_cartera.pr_upd_cheques_posfechados(pv_rca_estado_registro => :p46_rca_estado_registro,',
'                                            pn_ede_id              => :p46_ede_id,',
'                                            pv_cre_nro_cheque      => :p46_cre_nro_cheque,',
'                                            pv_cre_nro_cuenta      => :p46_cre_nro_cuenta,',
'                                            pv_cre_nro_pin         => :p46_cre_nro_pin,',
'                                            pn_cre_valor           => :p46_cre_valor,',
'                                            pv_cre_titular_cuenta  => :p46_cre_titular_cuenta,',
'                                            pv_cre_observaciones   => :p46_cre_observaciones,',
'                                            pd_cre_fecha_deposito  => :p46_cre_fecha_deposito,',
'                                            pv_cre_beneficiario    => :p46_cre_beneficiario,',
'                                            pv_error               => :p0_error,',
'                                            pn_rca_id              => :p46_rca_id,',
'                                            pn_cre_id              => :p46_cre_id,',
'                                            pv_nombre_coll_div     => ''COLL_RESPALDO_DIV'',',
'                                            pn_emp_id              => :f_emp_id);',
'if :p0_error is null then ',
'--aumentado por Andres Calle',
'  ',
'  pq_car_cartera.pr_graba_factura_pagare(pn_emp_id               => :f_emp_id,',
'                                          pn_rca_id_reemplazado   => :p46_rca_id_reemplazado,',
'                                          pv_rca_tipo_respaldo    => pq_constantes.fn_retorna_constante(0,''cv_rca_tipo_otros''),',
'                                          pv_rca_estado_registro  => :p46_rca_estado_registro,',
'                                          pn_cli_id               => :p46_cli_id,',
'                                          pv_ore_tipo_respaldo    => ''FP'',                                          ',
'                                          pn_uge_id               => :f_uge_id,',
'                                          pn_usu_id               => :f_user_id,',
'                                          pn_uge_id_gasto         => :f_uge_id_gasto,',
'                                          pv_error                => :P0_error);',
'end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(62240153792963239)
,p_internal_uid=>29992616318198330
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(43767477742959239)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_car_cartera_interfaz.pr_carga_coll_cheq_posfec(pn_cli_id => :p46_cli_id,',
'                                                    pn_emp_id => :f_emp_id,',
'                                                    pn_rca_id => :p46_rca_id,',
'                                                    pn_tse_id => :f_seg_id,',
'                                                    pn_uge_id => :f_uge_id,',
'                                                    pv_error  => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'nuevo, consulta'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>11514326473194313
);
wwv_flow_imp.component_end;
end;
/
