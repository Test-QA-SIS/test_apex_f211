prompt --application/pages/page_00132
begin
--   Manifest
--     PAGE: 00132
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>132
,p_name=>'PAGO INTERESES MAYOREO'
,p_step_title=>'PAGO INTERESES MAYOREO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771492766015976609)
,p_plug_name=>'CREDITOS A CANCELAR'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT D.CXC_ID,',
'       D.DIV_ID,',
'       D.DIV_NRO_VENCIMIENTO,',
'       D.DIV_FECHA_VENCIMIENTO,',
'       D.DIV_VALOR_CUOTA,',
'       D.DIV_SALDO_CUOTA,',
'       EDE.EDE_DESCRIPCION TIPO_CARTERA,',
'       CASE',
'         WHEN D.DIV_SALDO_CUOTA >= D.DIV_VALOR_CAPITAL THEN',
'          D.DIV_VALOR_CAPITAL',
'         ELSE',
'          D.DIV_SALDO_CUOTA',
'       END DIV_VALOR_CAPITAL,',
'       CASE',
'         WHEN (D.DIV_VALOR_CUOTA - D.DIV_SALDO_CUOTA) >= D.DIV_VALOR_INTERES THEN',
'          0',
'         ELSE',
'          D.DIV_VALOR_INTERES - (D.DIV_VALOR_CUOTA - D.DIV_SALDO_CUOTA)',
'       END DIV_VALOR_INTERES,',
'       CASE',
'         WHEN D.DIV_SALDO_CUOTA >= D.DIV_VALOR_CAPITAL THEN',
'          D.DIV_VALOR_CAPITAL',
'         ELSE',
'          D.DIV_SALDO_CUOTA',
'       END DEUDA,',
'       cxc.com_id',
'      ',
'  FROM CAR_DIVIDENDOS D,CAR_CUENTAS_POR_COBRAR CXC, ASDM_ENTIDADES_DESTINOS EDE',
' WHERE D.CXC_ID IN',
'       (SELECT D.CXC_ID FROM CAR_RENEGOCIACION_DET D WHERE D.REN_ID = :P132_REN_ID)',
'   AND D.DIV_SALDO_CUOTA > 0',
'   AND D.DIV_VALOR_INTERES > 0',
'   AND D.EMP_ID = :F_EMP_ID',
'   AND D.DIV_ESTADO_REGISTRO = 0',
'   AND (D.DIV_VALOR_INTERES - (D.DIV_VALOR_CUOTA - D.DIV_SALDO_CUOTA)) > 0',
'   AND D.CXC_ID=CXC.CXC_ID',
'   AND CXC.EDE_ID=EDE.EDE_ID',
'      AND to_date(to_char(d.div_fecha_vencimiento, ''dd-mm-yyyy'')) <= to_date(to_char(SYSDATE, ''dd-mm-yyyy''))',
'   --AND trunc(d.div_fecha_vencimiento, ''mm'') <= trunc(SYSDATE, ''mm'')',
' ORDER BY D.CXC_ID, D.DIV_ID'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(771492861222976609)
,p_name=>'CREDITOS A CANCELAR'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_owner=>'JALVAREZ'
,p_internal_uid=>739239709953211683
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493069837976610)
,p_db_column_name=>'CXC_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493183781976610)
,p_db_column_name=>'DIV_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493255006976610)
,p_db_column_name=>'DIV_NRO_VENCIMIENTO'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Div Nro Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493373444976610)
,p_db_column_name=>'DIV_FECHA_VENCIMIENTO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Div Fecha Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493457805976611)
,p_db_column_name=>'DIV_VALOR_CUOTA'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Div Valor Cuota'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493568330976611)
,p_db_column_name=>'DIV_SALDO_CUOTA'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Div Saldo Cuota'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_SALDO_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493672547976611)
,p_db_column_name=>'TIPO_CARTERA'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Tipo Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO_CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493751544976611)
,p_db_column_name=>'DIV_VALOR_CAPITAL'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Div Valor Capital'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_VALOR_CAPITAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493872687976611)
,p_db_column_name=>'DIV_VALOR_INTERES'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Div Valor Interes'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_VALOR_INTERES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771493967144976611)
,p_db_column_name=>'DEUDA'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Deuda'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DEUDA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15320626256291634631)
,p_db_column_name=>'COM_ID'
,p_display_order=>20
,p_column_identifier=>'K'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(771494160790977048)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'7392411'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100000
,p_report_columns=>'CXC_ID:DIV_ID:DIV_NRO_VENCIMIENTO:DIV_FECHA_VENCIMIENTO:DIV_VALOR_CUOTA:DIV_SALDO_CUOTA:TIPO_CARTERA:DIV_VALOR_CAPITAL:DIV_VALOR_INTERES:DEUDA:COM_ID'
,p_break_on=>'CXC_ID:0:0:0:0:0'
,p_break_enabled_on=>'CXC_ID:0:0:0:0:0'
,p_sum_columns_on_break=>'DIV_VALOR_INTERES'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771641280549955409)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_button_name=>'PAGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Pagar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:134:&SESSION.:formas_pago:&DEBUG.::P134_CLI_ID,P134_CLI_IDENTIFICACION,P134_MENSAJE,P134_FACTURAR,P134_CLI_NOMBRE,P134_PAGO_REFINANCIAMIENTO,P134_VALOR_REFINANCIAMIENTO,P134_CXC_ID_REF,P134_ES_PRECANCELACION,P134_REN_ID:&P132_CLI_ID.,&P132_CLI_IDENTIFICACION.,&P132_MENSAJE.,&P132_FACTURAR.,&P132_CLI_NOMBRE.,&P132_PAGO_REFINANCIAMIENTO.,&P132_VALOR_REFINANCIAMIENTO.,&P132_CXC_ID_REF.,&P132_ES_PRECANCELACION.,&P132_REN_ID.'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771496165283020345)
,p_name=>'P132_REN_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_prompt=>'Ren Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771632167636307885)
,p_name=>'P132_CLI_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_prompt=>'Cli Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'SELECT REN.CLI_ID FROM CAR_RENEGOCIACION REN WHERE REN.REN_ID=:P132_REN_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771632758363312243)
,p_name=>'P132_CLI_IDENTIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_prompt=>'Cli Identificacion'
,p_source=>'SELECT PE.PER_NRO_IDENTIFICACION FROM CAR_RENEGOCIACION REN, ASDM_CLIENTES CL, ASDM_PERSONAS PE WHERE REN.REN_ID=:P132_REN_ID AND REN.CLI_ID=CL.CLI_ID AND CL.PER_ID=PE.PER_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771633154265314175)
,p_name=>'P132_MENSAJE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771633382073316397)
,p_name=>'P132_FACTURAR'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_item_default=>'''N'''
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771633578407318126)
,p_name=>'P132_CLI_NOMBRE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_prompt=>'Cli Nombre'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TRIM(PE.PER_PRIMER_NOMBRE || '' '' || PE.PER_SEGUNDO_NOMBRE || '' '' ||',
'            PE.PER_PRIMER_APELLIDO || '' '' || PE.PER_SEGUNDO_APELLIDO || '' '' ||',
'            PE.PER_RAZON_SOCIAL)',
'  FROM CAR_RENEGOCIACION REN, ASDM_CLIENTES CL, ASDM_PERSONAS PE',
' WHERE REN.REN_ID = :P132_REN_ID',
'   AND REN.CLI_ID = CL.CLI_ID',
'   AND CL.PER_ID = PE.PER_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771633972153321009)
,p_name=>'P132_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_prompt=>'Pago Refinanciamiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771634166977323419)
,p_name=>'P132_VALOR_REFINANCIAMIENTO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771634360723326337)
,p_name=>'P132_CXC_ID_REF'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771634555331328877)
,p_name=>'P132_ES_PRECANCELACION'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(771492766015976609)
,p_item_default=>'''N'''
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
