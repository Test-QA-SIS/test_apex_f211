prompt --application/pages/page_00019
begin
--   Manifest
--     PAGE: 00019
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>19
,p_name=>'Report_Movimientos_caja'
,p_step_title=>'Report_Movimientos_caja'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script>',
'history.forward(); ',
'</script> '))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'JTORRES'
,p_last_upd_yyyymmddhh24miss=>'20240126161804'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(135523373782022682)
,p_name=>'Movimientos Caja Detalle'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_movimientos_caja.fn_movimientos_detalle(:f_emp_id, :f_uge_id, :f_pca_id, :p0_error);'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS_INITCAP'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Descargar a Excel'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_exp_separator=>';'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135523574111022686)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135523681843022694)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135523761288022694)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135523859985022694)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135523951325022694)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524058136022694)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524158111022694)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525067084022695)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524276908022694)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524352897022694)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524481441022694)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524563624022694)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524671353022694)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524779688022694)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524853558022694)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135524981435022694)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525158542022695)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525278100022695)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525368964022695)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525461332022695)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525577278022695)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525663662022695)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525775304022695)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525867547022695)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135525980093022695)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526056387022695)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526176568022695)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526282177022695)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526358647022695)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526472128022695)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526560630022695)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526664565022695)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526753770022696)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526882520022696)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135526977637022696)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527068212022696)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527152785022696)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527272034022696)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527354550022696)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527455430022696)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527580435022696)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527666674022696)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527773104022696)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527857910022696)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135527976877022696)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528051387022696)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528157503022696)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528251409022696)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528357341022697)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528470053022697)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528562752022697)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528676001022697)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528781392022697)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528864682022697)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135528977164022697)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135529068395022697)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135529167630022697)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135529275382022697)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135529371203022697)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135529468838022697)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(248469376598522422)
,p_plug_name=>'Movimientos  <b>&P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca.pca_id,',
'       mca.mca_fecha,',
'       mca.mca_id,',
'       ttr.ttr_descripcion,',
'       mca.cli_id,',
'       (SELECT TRIM(p.per_primer_apellido || '' '' || p.per_segundo_apellido || '' '' ||',
'                    p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'                    p.per_razon_social)',
'          FROM asdm_personas p, asdm_clientes c',
'         WHERE c.per_id = p.per_id',
'           AND c.cli_id = mca.cli_id',
'           AND c.cli_estado_registro = 0',
'           AND p.per_estado_registro = 0',
'           AND c.emp_id = :f_emp_id',
'           AND p.emp_id = :f_emp_id) cliente,       ',
'       decode(ttr.ttr_funcion, ''D'', ''Debito'', ''C'', ''Credito'') funcion,',
'       mca.mca_estado_mc,',
'       mca.mca_id_referencia,        ',
'       mca.mca_estado_registro,',
'       ''Detalle Movimiento'' detalle,',
'       (SELECT trx.mca_id',
'          FROM asdm_transacciones trx',
'         WHERE trx.trx_id = mca.trx_id) comprobante_contable,',
'       CASE',
'         WHEN ttr.ttr_funcion = ''D'' THEN',
'          mca.mca_total',
'       END ingreso,',
'       CASE',
'         WHEN ttr.ttr_funcion = ''C'' THEN',
'          mca.mca_total',
'       END egreso,',
'       SUM(CASE',
'             WHEN ttr.ttr_funcion = ''D'' THEN',
'              mca.mca_total',
'             WHEN ttr.ttr_funcion = ''C'' THEN',
'              mca.mca_total * -1',
'           END) over(PARTITION BY pca_id ORDER BY mca_fecha rows unbounded preceding) saldo,',
'           (select username from kseg_e.kseg_usuarios us where us.usuario_id = mca.usu_id) usuario',
'  FROM asdm_movimientos_caja mca, asdm_tipos_transacciones ttr',
' WHERE ttr.ttr_id = mca.ttr_id',
'   AND mca.emp_id = :f_emp_id',
'   AND mca.pca_id = :f_pca_id',
'UNION --agregado por andres calle para que conste dentro de los movimientos el saldo inicial de la caja',
'SELECT pca.pca_id,',
'       pca.pca_fechahora_inicio,',
'       NULL,',
'       ''SALDO INICIAL CAJA'',',
'       NULL,',
'       NULL,       ',
'       ''Debito'',',
'       NULL,',
'       NULL,       ',
'       pue.pue_estado_registro,',
'       ''Detalle Movimiento'' detalle,',
'       NULL,',
'       pue.pue_saldo_inicial ingreso,',
'       NULL egreso,',
'       pue.pue_saldo_inicial saldo,',
'       null usuario',
'  FROM asdm_puntos_emision pue, ven_periodos_caja pca',
' WHERE pca.emp_id = pue.emp_id',
'   AND pca.pue_id = pue.pue_id',
'   AND pca.emp_id = :f_emp_id',
'   AND pca.pca_id = :f_pca_id',
'   ',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(248469476069522422)
,p_name=>'Movimientos Caja'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No existen movimientos'
,p_allow_report_categories=>'N'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>216216324799757496
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248469682591522424)
,p_db_column_name=>'PCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>unistr('Per\00BF\00BFodo<br>Caja')
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248469779569522425)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248469878721522425)
,p_db_column_name=>'MCA_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'#'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248469957846522425)
,p_db_column_name=>'TTR_DESCRIPCION'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>unistr('Descripci\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'TTR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470080369522425)
,p_db_column_name=>'CLI_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Id. Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470179345522425)
,p_db_column_name=>'FUNCION'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>unistr('Funci\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'FUNCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470252775522425)
,p_db_column_name=>'MCA_ESTADO_MC'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_ESTADO_MC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470368646522426)
,p_db_column_name=>'MCA_ESTADO_REGISTRO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470477461522426)
,p_db_column_name=>'INGRESO'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Ingreso'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'INGRESO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470578817522426)
,p_db_column_name=>'EGRESO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Egreso'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EGRESO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248470678822522426)
,p_db_column_name=>'SALDO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248475880549590070)
,p_db_column_name=>'DETALLE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Detalle'
,p_column_link=>'f?p=&APP_ID.:22:&SESSION.::&DEBUG.::P22_MCA_ID,P22_NOMBRE_CLIENTE,P22_NOMBREMOVIMIENTO,P22_PAGINA:#MCA_ID#,#CLIENTE#,#TTR_DESCRIPCION#,19'
,p_column_linktext=>'Detalle Movimiento'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'DETALLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(262941575146205595)
,p_db_column_name=>'CLIENTE'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(127797055770747382)
,p_db_column_name=>'MCA_ID_REFERENCIA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Mca Id Referencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_ID_REFERENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136915383379513365)
,p_db_column_name=>'COMPROBANTE_CONTABLE'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Comprobante <br>Contable'
,p_column_link=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.::P19_MCA_ID_CON:#COMPROBANTE_CONTABLE#'
,p_column_linktext=>'#COMPROBANTE_CONTABLE#'
,p_column_link_attr=>'style="color: #0269ff;"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'COMPROBANTE_CONTABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(221752379278706726528)
,p_db_column_name=>'USUARIO'
,p_display_order=>25
,p_column_identifier=>'P'
,p_column_label=>'Usuario'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(248472662726530265)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1276315321919693'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'MCA_ID:PCA_ID:MCA_FECHA:TTR_DESCRIPCION:CLIENTE:CLI_ID:FUNCION:INGRESO:EGRESO:SALDO:DETALLE:MCA_ESTADO_MC:MCA_ID_REFERENCIA:COMPROBANTE_CONTABLE:USUARIO'
,p_sort_column_1=>'MCA_ID'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'MCA_FECHA'
,p_sort_direction_2=>'DESC'
,p_sum_columns_on_break=>'INGRESO:EGRESO'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(1563839497958496770)
,p_plug_name=>'Estado Dispositivo'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>30
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'  FROM (SELECT a.pac_id,',
'               a.ppa_id,',
'               a.pve_id,',
'               a.pac_titulo,',
'               a.pac_json_envio,',
'               a.pac_json_respu,',
'               a.pac_fecha,',
'               CASE a.pac_estado',
'                 WHEN ''I'' THEN',
'                  ''Ingresado''',
'                 WHEN ''C'' THEN',
'                    ''Completado''',
'                    --''<a  href="f?p=&APP_ID.:19:&SESSION.:activar:NO::">'' ||''Reenviar'' || ''</a>''',
'                 WHEN ''E'' THEN',
'                  --''Error''',
'                    ''<a  href="f?p=&APP_ID.:19:&SESSION.:activar:NO::">'' ||''Reenviar'' || ''</a>''',
'               END estado,',
'               a.pac_estado_registro,',
'               a.pac_fecha_envio,',
'               a.pac_mensaje',
'          FROM inv_paybahn_acciones a',
'         WHERE a.pve_id =',
'               (SELECT pve_id FROM inv_paybahn_ventas WHERE com_id = :P19_COM_ID)',
'         ORDER BY 1 DESC)',
' WHERE rownum = 1;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header=>'Estado Dispositivo'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#EEEEEE'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'bold'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#FFFFFF'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_prn_border_color=>'#666666'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(1563839649555496771)
,p_max_row_count=>'1000000'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'MFIDROVO'
,p_internal_uid=>1547713073920614308
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563839752975496772)
,p_db_column_name=>'PAC_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Pac Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563839857028496773)
,p_db_column_name=>'PPA_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Ppa Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563839935422496774)
,p_db_column_name=>'PVE_ID'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Pve Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840020597496775)
,p_db_column_name=>'PAC_TITULO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Pac Titulo'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840115459496776)
,p_db_column_name=>'PAC_JSON_ENVIO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Pac Json Envio'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840239817496777)
,p_db_column_name=>'PAC_JSON_RESPU'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Pac Json Respu'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840368481496778)
,p_db_column_name=>'PAC_FECHA'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Pac Fecha'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840384955496779)
,p_db_column_name=>'ESTADO'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Estado'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840481504496780)
,p_db_column_name=>'PAC_ESTADO_REGISTRO'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Pac Estado Registro'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840652925496781)
,p_db_column_name=>'PAC_FECHA_ENVIO'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Pac Fecha Envio'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1563840740770496782)
,p_db_column_name=>'PAC_MENSAJE'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Pac Mensaje'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(1453293539531193816)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'14371670'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'PAC_ID:PPA_ID:PVE_ID:PAC_TITULO:PAC_JSON_ENVIO:PAC_JSON_RESPU:PAC_FECHA:ESTADO:PAC_ESTADO_REGISTRO:PAC_FECHA_ENVIO:PAC_MENSAJE'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63188081144807381061)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_button_name=>'Imprimir_Recibos'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir recibos'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=283:66:&SESSION.:busca:&DEBUG.::F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT:&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL_DESC.,&P0_TREE_ROOT.'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(857575312616138401)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_button_name=>'Imprimir_Recibos_1'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir recibos '
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=283:66:&SESSION.:busca:&DEBUG.::P0_ROL_DESC,P0_TREE_ROOT:&P0_ROL_DESC.,P0_TREE_ROOT'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(49358265004840601)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=cierre_caja.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&pn_pca_id=&F_PCA_ID.&pn_emp_id=&F_EMP_ID.'', 850, 500,85));'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1453464463006107277)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_button_name=>'IMPRIMIR2'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:19:&SESSION.:imprimir_caja:&DEBUG.:::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(13469011968492719559)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_button_name=>'IR_REFEINANCIAMENTO'
,p_button_static_id=>'IR_REFEINANCIAMENTO'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'>> Ir Refeinanciamento'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:102:&SESSION.:carga:&DEBUG.:::'
,p_button_condition=>':P19_PAGO_REFINANCIAMIENTO = 1'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37070968104772205)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_button_name=>'COMPROBANTES'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Listado Comprobantes'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'javascript:popupURL(''../../reports/rwservlet?module=listado_comprobantes.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PD_FECHA_FIN=&P19_FECHA_FIN.&PD_FECHA_INI=&P19_FECHA_INI.&PN_AGE_ID=&F_AGE_ID_AGENCIA.&PN_EMP_ID=&F_EMP_ID.&PV_USU_ID=&F_USER_ID.'')'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(946613752559977384)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_button_name=>'PAGO_CUOTA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'>>IR AL PAGO DE CUOTA'
,p_button_position=>'TOP_AND_BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:6::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(41471441589678530)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_button_name=>'new_pago'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo Pago de Cuota'
,p_button_position=>'TOP_AND_BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:7:&SESSION.::&DEBUG.:RP,7::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38275596453606386848)
,p_branch_name=>'Go To Page 19'
,p_branch_action=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.:RP:P19_MCA_ID:&P19_MCA_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104664391792013441)
,p_name=>'P19_MCA_ID_CON'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(136959264377886288)
,p_name=>'P19_MCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(708540763349827598)
,p_name=>'P19_FECHA_INI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_use_cache_before_default=>'NO'
,p_item_default=>'trunc(sysdate)'
,p_source=>'to_char(sysdate,''dd-mm-yyyy'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(708540974735827600)
,p_name=>'P19_FECHA_FIN'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_use_cache_before_default=>'NO'
,p_item_default=>'trunc(sysdate)'
,p_source=>'to_char(sysdate,''dd-mm-yyyy'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13468894561435708134)
,p_name=>'P19_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(248469376598522422)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840792422121029545)
,p_name=>'P19_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(135523373782022682)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38275595923315386842)
,p_name=>'subMit'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38275596000240386843)
,p_event_id=>wwv_flow_imp.id(38275595923315386842)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(136959582516901028)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_COMPROBANTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_mca_id_con NUMBER;',
'  ln_cont       NUMBER;',
'  ln_mca_id_nc  NUMBER;',
'',
'  CURSOR cu_mov_caja IS',
'    SELECT cx.*',
'      FROM asdm_E.Car_Movimientos_Cab   ca,',
'           car_cuentas_por_cobrar       cx,',
'           asdm_movimientos_promociones pro',
'     WHERE ca.cxc_id = cx.cxc_id',
'       AND ca.mca_id_movcaja = :P19_MCA_ID',
'       AND cx.com_id = pro.com_id_fact_origen;',
'',
'BEGIN',
'',
'  IF :p19_mca_id IS NOT NULL THEN',
'  ',
'    pq_ven_movimientos_caja.pr_imprimir_mov_caja(pn_emp_id => :f_emp_id,',
'                                                 pn_mca_id => :P19_MCA_ID,',
'                                                 pv_error  => :P0_ERROR);',
'  END IF;',
'',
'  pq_con_imprimir.pr_imprimir(pn_emp_id => :f_emp_id,',
'                              pn_mca_id => :P19_MCA_ID_CON,',
'                              pn_mon_id => 1,',
'                              pv_error  => :p0_error);',
'',
'  FOR reg IN cu_mov_caja LOOP',
'  ',
'    pq_asdm_promociones.pr_imprimir_puntualito(pn_emp_id => reg.emp_id,',
'                                               pn_cli_id => reg.cli_id,',
'                                               pn_com_id => reg.com_id,',
'                                               pn_ttr_id => pq_constantes.fn_retorna_constante(0,',
'                                                                                               ''cn_ttr_id_genera_promo''),',
'                                               pv_error  => :p0_error);',
'  ',
'  END LOOP;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P19_MCA_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>104706431247136102
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38275595804994386841)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_mov_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_ven_movimientos_caja.pr_imprimir_mov_caja(pn_emp_id => :f_emp_id,',
'                                                     pn_mca_id => :P19_MCA_ID,',
'                                                     pv_error  => :P0_ERROR);   ',
'                                                     ',
'                                                     ',
':P19_MCA_ID:=null;                                                     ',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_type=>'NEVER'
,p_internal_uid=>38243342653724621915
,p_process_comment=>'P19_MCA_ID'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1453464356960107276)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_CIERRE_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_error VARCHAR2(250);',
'BEGIN',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CIERRE_CAJA'',',
'                                            pv_nombre_parametros => ''pn_emp_id:pn_pca_id'',',
'                                            pv_valor_parametros  => :f_emp_id || '':'' ||',
'                                                                    :F_PCA_ID,',
'                                            pn_emp_id            => :f_emp_id,',
'                                            pv_formato           => ''pdf'',',
'                                            pv_error             => lv_error);',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprimir_caja'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1437337781325224813
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(857575580768138404)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'New'
,p_process_sql_clob=>'asdm_p.pq_ven_movimientos_caja.pr_imprimir_recibos;'
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(857575312616138401)
,p_internal_uid=>841449005133255941
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1563840784042496783)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACTUALIZA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    ln_pve_id number;',
'',
'begin',
'',
'    SELECT pve_id ',
'    INTO ln_pve_id',
'    FROM inv_paybahn_ventas ',
'    WHERE com_id = :P19_COM_ID;',
'    --raise_application_error(-20000, ''Llego'' || ln_pve_id);',
'    --ln_pve_id := 0;',
'    if nvl(ln_pve_id, 0) != 0 then',
'    ',
'        -- Call the procedure',
'        pq_car_paybahn.pr_activacion_cliente(pn_pve_id => ln_pve_id,',
'                                             pn_emp_id => :F_EMP_ID);',
'    else',
'        apex_application.g_print_success_message := ''<span style="color:RED">No se pudo recuperar una venta.</span>'';',
'                                                ',
'    end if;',
' EXCEPTION',
'    WHEN OTHERS THEN ',
'        apex_application.g_print_success_message := ''<span style="color:RED">Error al enviar a Actualizar el Dispositivo: '' ||',
'                                                SQLERRM || ''</span>'';',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'activar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1547714208407614320
);
wwv_flow_imp.component_end;
end;
/
