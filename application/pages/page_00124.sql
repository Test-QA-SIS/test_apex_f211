prompt --application/pages/page_00124
begin
--   Manifest
--     PAGE: 00124
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>124
,p_name=>'PRECANCELACION'
,p_alias=>'PRECANCELACION'
,p_step_title=>'PRECANCELACION'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(79196681208851613)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>230
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'             c001   age_id_agente,',
'             c002   ieb_id,',
'             c003   ite_sku_id,',
'             c004   pol_id,',
'             c005   cei_id,',
'             c006   item_relacionado,',
'             c031   bpr_id,',
'             c010   llave,',
'             c012   cantidad',
'        FROM apex_collections',
'       WHERE collection_name =',
'             pq_constantes.fn_retorna_constante(0, ''cv_colecciondetalle'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001366786877766)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001453367877773)
,p_query_column_id=>2
,p_column_alias=>'AGE_ID_AGENTE'
,p_column_display_sequence=>2
,p_column_heading=>'Age Id Agente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001552906877773)
,p_query_column_id=>3
,p_column_alias=>'IEB_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Ieb Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001682489877773)
,p_query_column_id=>4
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Ite Sku Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001751576877773)
,p_query_column_id=>5
,p_column_alias=>'POL_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Pol Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001869390877774)
,p_query_column_id=>6
,p_column_alias=>'CEI_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Cei Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94001982039877774)
,p_query_column_id=>7
,p_column_alias=>'ITEM_RELACIONADO'
,p_column_display_sequence=>7
,p_column_heading=>'Item Relacionado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94002077382877774)
,p_query_column_id=>8
,p_column_alias=>'BPR_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Bpr Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94002181754877774)
,p_query_column_id=>9
,p_column_alias=>'LLAVE'
,p_column_display_sequence=>9
,p_column_heading=>'Llave'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94002273523877774)
,p_query_column_id=>10
,p_column_alias=>'CANTIDAD'
,p_column_display_sequence=>10
,p_column_heading=>'Cantidad'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476522068392691525)
,p_plug_name=>'Datos Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(476531478930855433)
,p_name=>'creditos vigentes'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.cxc_id,',
'       a.uge_id,',
'       (SELECT u.uge_nombre',
'          FROM asdm_unidades_gestion u',
'         WHERE u.uge_id = a.uge_id) unidad_gestion,',
'       b.uge_num_sri || ''-'' || b.pue_num_sri || ''-'' || b.com_numero factura,',
'       b.com_id,',
'       a.cxc_fecha_adicion,',
'       a.cxc_saldo,',
'       a.ede_id,',
'       (SELECT e.ede_abreviatura',
'          FROM asdm_entidades_destinos e',
'         WHERE e.ede_id = a.ede_id) entidad,',
'       CASE a.cxc_saldo',
'         WHEN 0 THEN',
'          NULL',
'         ELSE',
'          ''CARGAR''',
'       END cargar,',
'b.pol_id',
'  FROM car_cuentas_por_cobrar a, ven_comprobantes b',
' WHERE b.com_id = a.com_id',
'   AND a.cxc_saldo > 0',
'   AND a.cxc_estado_registro = 0',
'   AND b.com_estado_registro = 0',
'   AND a.cli_id = :p124_cli_id',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476535554835897274)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476531783950855476)
,p_query_column_id=>2
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476531862745855484)
,p_query_column_id=>3
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>3
,p_column_heading=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476531955492855484)
,p_query_column_id=>4
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>4
,p_column_heading=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476532081504855484)
,p_query_column_id=>5
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>5
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476532153049855492)
,p_query_column_id=>6
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>7
,p_column_heading=>'FECHA_VENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476532282080855492)
,p_query_column_id=>7
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>8
,p_column_heading=>'SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476532357909855492)
,p_query_column_id=>8
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>9
,p_column_heading=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476532763917862672)
,p_query_column_id=>9
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>10
,p_column_heading=>'CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476532456917855492)
,p_query_column_id=>10
,p_column_alias=>'CARGAR'
,p_column_display_sequence=>11
,p_column_heading=>'CARGAR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:124:&SESSION.:CARGAR:&DEBUG.::P124_CXC_ID,P124_POL_ID,P124_CONTINUAR:#CXC_ID#,#POL_ID#,N'
,p_column_linktext=>'#CARGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476558753669535999)
,p_query_column_id=>11
,p_column_alias=>'POL_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Pol Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(476533456597886852)
,p_name=>'CRONOGRAMA ACTUAL'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c001 cxc_id,',
'       c002 div_id,',
'       c003 nro_vencimiento,',
'       c004 fecha_vencimiento,',
'       c005 valor_cuota,',
'       to_number(c006) saldo_cuota,',
'       to_number(c007) capital,',
'       to_number(c008) interes,',
'       to_number(c009) cap_promocional,',
'       c010 observacion,',
'       c011 dias_retraso',
'  FROM apex_collections',
' WHERE collection_name = ''COL_CXC_PRECANCELA''',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>30
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476546584029050872)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476546672201050872)
,p_query_column_id=>2
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476546971737056601)
,p_query_column_id=>3
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476547068166056602)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476547181804056602)
,p_query_column_id=>5
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476547276916056602)
,p_query_column_id=>6
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476547369739056603)
,p_query_column_id=>7
,p_column_alias=>'CAPITAL'
,p_column_display_sequence=>7
,p_column_heading=>'Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476547482814056603)
,p_query_column_id=>8
,p_column_alias=>'INTERES'
,p_column_display_sequence=>8
,p_column_heading=>'Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476547581226056603)
,p_query_column_id=>9
,p_column_alias=>'CAP_PROMOCIONAL'
,p_column_display_sequence=>9
,p_column_heading=>'Cap Promocional'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476551153017277960)
,p_query_column_id=>10
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>10
,p_column_heading=>'Observacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476557873751496283)
,p_query_column_id=>11
,p_column_alias=>'DIAS_RETRASO'
,p_column_display_sequence=>11
,p_column_heading=>'Dias Retraso'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476615653125228071)
,p_plug_name=>'PAGO PRECANCELACION'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'p124_continuar'
,p_plug_display_when_cond2=>'S'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476625474819228084)
,p_plug_name=>'tfp_Cheques'
,p_parent_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>140
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476626667365228085)
,p_plug_name=>'IngresoValor'
,p_parent_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>200
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476628365067228087)
,p_plug_name=>'tfp_deposito_transferencia'
,p_parent_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>160
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476628955453228088)
,p_plug_name=>'tfp Datos Tarjeta Credito'
,p_parent_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>150
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P124_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476631578768228091)
,p_plug_name=>'tfp_retencion'
,p_parent_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(476650571723451068)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>210
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:p0_error)',
''))
,p_display_when_condition=>'p124_continuar'
,p_display_when_cond2=>'S'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476668880227548010)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476668969458548010)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:124:&SESSION.:ELIMINA:&DEBUG.::P124_SEQ_ID:#COL03#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669064866548010)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669163660548010)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669271137548010)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669351897548010)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669454730548011)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669555514548011)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669659004548011)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669777103548011)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669876767548011)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476669959356548011)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670073089548011)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670156911548011)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670273604548011)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670367924548011)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670463378548011)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670558071548011)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670682186548011)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670770811548011)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670869924548011)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476670955130548011)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671073416548011)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671177318548011)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671268409548011)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671354054548012)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671475768548012)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671583579548012)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671672539548012)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671765400548012)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671857555548012)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476671960725548012)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672074458548012)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672177535548012)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672274736548012)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672357300548012)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672476102548012)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672565774548012)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672660634548012)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672766728548012)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672854831548012)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476672956870548012)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673059377548012)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673180703548012)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673283484548012)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673355330548013)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673482819548013)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673577613548013)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673654985548013)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673780388548013)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673865067548013)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476673971963548013)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674083622548013)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674154560548013)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674277417548013)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674372210548013)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674482828548013)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674554760548013)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674665410548013)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(476674772969548013)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(476676177928575884)
,p_plug_name=>'pagar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>220
,p_plug_display_point=>'REGION_POSITION_07'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(476570258610775464)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_button_name=>'CONTINUAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Continuar'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>':p124_cxc_id is not null and nvl(:P124_GENERA_PREC,''S'') =''S'''
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(476655764066496132)
,p_button_sequence=>650
,p_button_plug_id=>wwv_flow_imp.id(476676177928575884)
,p_button_name=>'PAGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'PAGAR'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_condition=>'(select sum(to_number(c006)) from apex_collections where collection_name = Pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja'')) = :P124_VALOR_TOTAL_PAGOS'
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(476677156264589527)
,p_branch_name=>'br_pagar'
,p_branch_action=>'f?p=&APP_ID.:124:&SESSION.:PAGAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(476655764066496132)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(183393355658463389)
,p_name=>'P124_GENERA_PREC'
,p_item_sequence=>412
,p_item_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_prompt=>'Genera Precancelacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476522556075699124)
,p_name=>'P124_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onChange="doSubmit(''limpia'')"'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476522778271699132)
,p_name=>'P124_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Nro Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onChange="doSubmit(''cliente'')"'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476522972137699133)
,p_name=>'P124_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476523162404699134)
,p_name=>'P124_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476523355892699134)
,p_name=>'P124_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476523566106699134)
,p_name=>'P124_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476523756042699135)
,p_name=>'P124_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476523982582699135)
,p_name=>'P124_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476524153488699135)
,p_name=>'P124_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(476522068392691525)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476534971225889675)
,p_name=>'P124_CXC_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476555467152392965)
,p_name=>'P124_OBSERVACION'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476562972093679314)
,p_name=>'P124_OBS'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_item_default=>'POLITICA CON PROMOCION CUOTAS GRATIS'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;color:GREEN"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'select * From car_cuotas_promo where cxc_id = :P124_CXC_ID'
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476574468009930732)
,p_name=>'P124_POL_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476616859029228073)
,p_name=>'P124_VALOR_TOTAL_PAGOS'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Pagar'
,p_pre_element_text=>'<b>'
,p_format_mask=>'999G999G999G999G990D00'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(to_number(c007))',
'      FROM apex_collections c',
'     WHERE c.collection_name = ''COL_CXC_PRECANCELA'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476617071809228073)
,p_name=>'P124_CRE_TITULAR_CUENTA'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_prompt=>'Titular Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>100
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476617658548228074)
,p_name=>'P124_SALDO_ANTICIPOS'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_prompt=>'Saldo Anticipos'
,p_source=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P124_CLI_ID,:f_uge_id))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P124_FACTURAR <> ''R'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P124_CLI_ID,:f_uge_id))'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476620070268228076)
,p_name=>'P124_MENSAJE_ANTICIPO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED">    APLICAR EL PAGO CON ANTICIPO DE CLIENTES</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'CENTER-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'to_number(:P124_SALDO_ANTICIPOS) > 0 and :P124_FACTURAR = ''N'' AND NVL(:P124_ES_PRECANCELACION,''N'') <> ''S'' and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476620271798228077)
,p_name=>'P124_SEQ_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_prompt=>'Registro a modficar (seq_id) :'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P124_SEQ_ID IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476623081944228080)
,p_name=>'P124_TFP_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tfp_descripcion , a.tfp_id',
'  FROM asdm_tipos_formas_pago a, asdm_formpag_tipotrans b',
' WHERE b.tfp_id = a.tfp_id',
'   AND a.emp_id = 1',
'   AND b.ttr_id = 267',
''))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476623279903228080)
,p_name=>'P124_EDE_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_item_default=>'NULL'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Entidad Destino:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.EDE_DESCRIPCION, A.EDE_ID',
'FROM asdm_entidades_destinos A',
'WHERE A.EMP_ID=:F_EMP_ID',
'AND A.EDE_ESTADO_REGISTRO=''0'';'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') or ',
':P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P124_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476623466106228081)
,p_name=>'P124_SUM_PAGOS'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Sum Pagos'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(TO_NUMBER(c006)),0) suma_valor',
'  FROM apex_collections',
'WHERE collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476623659839228081)
,p_name=>'P124_MODIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_prompt=>'Modificacion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476623869448228082)
,p_name=>'P124_SALDO_PAGO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(476615653125228071)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Pago'
,p_pre_element_text=>'<b>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*select nvl(:p124_valor_total_pagos,0) - (to_number(sum(nvl(c006,0))) + TO_NUMBER(sum(nvl(c018,0)))) from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')*/',
'',
'select nvl(:p124_valor_total_pagos,0) - to_number(sum(nvl(c006,0)))  from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476625674801228084)
,p_name=>'P124_CRE_FECHA_DEPOSITO'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(476625474819228084)
,p_prompt=>'Fecha Cheque:'
,p_source=>'to_char(sysdate, ''DD/MM/YYYY'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476625880353228084)
,p_name=>'P124_CRE_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(476625474819228084)
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476626069069228084)
,p_name=>'P124_CRE_NRO_CHEQUE'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(476625474819228084)
,p_prompt=>'Nro Cheque:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476626270069228084)
,p_name=>'P124_CRE_NRO_CUENTA'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(476625474819228084)
,p_prompt=>'Nro Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476626478230228085)
,p_name=>'P124_CRE_NRO_PIN'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(476625474819228084)
,p_prompt=>'Nro Pin:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476627060768228085)
,p_name=>'P124_PRECIONEENTER'
,p_item_sequence=>17
,p_item_plug_id=>wwv_flow_imp.id(476626667365228085)
,p_prompt=>'Presione Enter para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476627277061228086)
,p_name=>'P124_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(476626667365228085)
,p_prompt=>'Valor Recibido:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''cargar_tfp'');" '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Presione Enter para cargar el valor'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476627755126228086)
,p_name=>'P124_ENTRADA'
,p_item_sequence=>382
,p_item_plug_id=>wwv_flow_imp.id(476626667365228085)
,p_prompt=>'Entrada'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--Utilizo solo cuando la venta se ha realizado con una tarjeta de credito.',
'--aqui cargo el valor de la entrada que cancelo a parte de la tarjeta de credito'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476627983646228086)
,p_name=>'P124_COMISION'
,p_item_sequence=>392
,p_item_plug_id=>wwv_flow_imp.id(476626667365228085)
,p_prompt=>'Comision'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476628180690228087)
,p_name=>'P124_VUELTO'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(476626667365228085)
,p_prompt=>'Vuelto/Cambio Efectivo'
,p_pre_element_text=>'<b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" style="font-size:16"'
,p_tag_attributes=>'style="font-size:26"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476628571186228087)
,p_name=>'P124_MCD_NRO_COMPROBANTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(476628365067228087)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Comprobante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>13
,p_cMaxlength=>12
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476628769159228087)
,p_name=>'P124_MCD_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(476628365067228087)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Deposito:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476629168773228088)
,p_name=>'P124_TJ_NUMERO'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P124_ES_POLITICA_GOBIERNO = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476629372600228088)
,p_name=>'P124_TJ_NUMERO_VOUCHER'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P124_ES_POLITICA_GOBIERNO = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476629559273228088)
,p_name=>'P124_TJ_TITULAR_Y'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Titular:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476629780607228089)
,p_name=>'P124_TJ_ENTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Entidad'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_TARJETA_CREDITO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    ',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_PLAN_TARJETA_CORRIENTE'');',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476629952169228089)
,p_name=>'P124_TJ_RECAP'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'RECAP:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476630167495228089)
,p_name=>'P124_BANCOS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Banco:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'SELECT a.ede_descripcion,a.ede_id FROM asdm_entidades_destinos a WHERE a.ede_tipo=''EFI'''
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476630376228228089)
,p_name=>'P124_TARJETA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Tarjeta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id FROM asdm_entidades_destinos b WHERE b.ede_tipo=''TJ''',
'AND b.ede_id_padre=:p124_bancos'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P124_BANCOS'
,p_ajax_items_to_submit=>'P124_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476630560915228090)
,p_name=>'P124_TIPO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Tipo:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p124_tarjeta'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P124_TARJETA'
,p_ajax_items_to_submit=>'P124_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476630753987228090)
,p_name=>'P124_PLAN'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Plan:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_descripcion, ''->'') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_tede_detalle_plan_tarjeta'')',
'START WITH ede.ede_id_padre =:P124_TIPO',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P124_TIPO'
,p_ajax_items_to_submit=>'P124_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476630976115228090)
,p_name=>'P124_LOTE'
,p_item_sequence=>322
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Lote'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476631160526228090)
,p_name=>'P124_MCD_NRO_AUT_REF'
,p_item_sequence=>332
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>unistr('Autorizaci\00C3\00B3n:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P124_ES_POLITICA_GOBIERNO = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476631358596228090)
,p_name=>'P124_TITULAR_TC'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(476628955453228088)
,p_prompt=>'Titular Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P124_ES_POLITICA_GOBIERNO = 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476631757989228091)
,p_name=>'P124_RAP_NRO_RETENCION'
,p_item_sequence=>262
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476631953071228091)
,p_name=>'P124_RAP_NRO_AUTORIZACION'
,p_item_sequence=>272
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>unistr('Nro Autorizaci\00C3\00B3n:')
,p_post_element_text=>' 10 digitos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>12
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476632166903228091)
,p_name=>'P124_RAP_FECHA'
,p_item_sequence=>282
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Retencion:'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476632376261228092)
,p_name=>'P124_RAP_FECHA_VALIDEZ'
,p_item_sequence=>292
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476632556184228092)
,p_name=>'P124_RAP_COM_ID'
,p_item_sequence=>302
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>'Factura:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LV_VEN_COMPROBANTES_F_ND'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    lv_lov := pq_asdm_listas.fn_lov_facturas_pago_cuota(:f_emp_id);',
'',
'/*lv_lov :=kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_COMPROBANTES_F_ND_SIN_RET'');',
'lv_lov := lv_lov || ''AND vco.cli_id = :P64_CLI_ID ORDER BY vco.com_id'';*/',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Facturas Disponibles --'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p124_FACTURAR = ''N'' '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476632770115228092)
,p_name=>'P124_SALDO_RETENCION'
,p_item_sequence=>312
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_post_element_text=>'  para Anticipo de Clientes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476632966303228092)
,p_name=>'P124_RAP_NRO_ESTABL_RET'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>unistr('Nro Retenci\00C3\00B3n:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476633172360228092)
,p_name=>'P124_RAP_NRO_PEMISION_RET'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476633367131228093)
,p_name=>'P124_PRE_ID'
,p_item_sequence=>372
,p_item_plug_id=>wwv_flow_imp.id(476631578768228091)
,p_prompt=>'Pre Id:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(476696482211759855)
,p_name=>'P124_CONTINUAR'
,p_item_sequence=>402
,p_item_plug_id=>wwv_flow_imp.id(476533456597886852)
,p_item_default=>'N'
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476536668072918586)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_datos_prec'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_col_mov_caja VARCHAR2(60) := pq_constantes.fn_retorna_constante(0,',
'                                                                     ''cv_coleccion_mov_caja'');',
'BEGIN',
'  pq_car_precancelacion.pr_carga_valores(pn_emp_id       => :f_emp_id,',
'                                         pn_cxc_id       => :p124_cxc_id,',
'                                         pn_tse_id       => :f_seg_id,',
'                                         pv_observacion  => :p124_observacion,',
'                                         pv_genera_prec  => :P124_GENERA_PREC,',
'                                         pv_error        => :p0_error);',
'  IF apex_collection.collection_exists(lv_col_mov_caja) THEN',
'    apex_collection.delete_collection(lv_col_mov_caja);',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>444283516803153660
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476526153725753531)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_cliente_existe      VARCHAR2(100);',
'  ln_dir_id              asdm_direcciones.dir_id%TYPE;',
'  lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'  ln_cli_id_referido     asdm_clientes.cli_id%TYPE;',
'lv_col_mov_caja varchar2(60):= pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja'');',
'BEGIN',
'',
'  pq_ven_listas2.pr_datos_clientes(188, --le envio la agencia matriz ya que el rol no tiene agencia y da error y no necesitan la agencia',
'                                  :f_emp_id,',
'                                  :P124_IDENTIFICACION,',
'                                  :P124_NOMBRE,',
'                                  :P124_CLI_ID,',
'                                  :P124_CORREO,',
'                                  :P124_DIRECCION,',
'                                  :P124_TIPO_DIRECCION,',
'                                  :P124_TELEFONO,',
'                                  :P124_TIPO_TELEFONO,',
'                                  lv_cliente_existe,',
'                                  ln_dir_id,',
'                                  ln_cli_id_referido,',
'                                  lv_nombre_cli_referido,',
'                                  :P124_TIPO_IDENTIFICACION,',
'                                  :P0_ERROR);',
':P124_CXC_ID := NULL;',
':P124_OBSERVACION := null;',
':P124_OBS := null;',
':P124_CONTINUAR := ''N'';',
' IF apex_collection.collection_exists(''COL_CXC_PRECANCELA'') THEN',
'      apex_collection.delete_collection(''COL_CXC_PRECANCELA'');',
' END IF;',
' IF apex_collection.collection_exists(lv_col_mov_caja) THEN',
'      apex_collection.delete_collection(lv_col_mov_caja);',
' END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>444273002455988605
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476527857019792400)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_col_mov_caja varchar2(60):= pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja'');',
'begin',
':P124_IDENTIFICACION := NULL;',
'	:P124_CLI_ID	:= NULL;',
'	:P124_NOMBRE	:= NULL;',
'	:P124_TIPO_DIRECCION	:= NULL;',
'	:P124_DIRECCION	:= NULL;',
'	:P124_TIPO_TELEFONO	:= NULL;',
'	:P124_TELEFONO	:= NULL;',
'	:P124_CORREO := NULL;',
':P124_CXC_ID := NULL;',
' IF apex_collection.collection_exists(''COL_CXC_PRECANCELA'') THEN',
'      apex_collection.delete_collection(''COL_CXC_PRECANCELA'');',
' END IF;',
'',
' IF apex_collection.collection_exists(lv_col_mov_caja) THEN',
'      apex_collection.delete_collection(lv_col_mov_caja);',
' END IF;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'limpia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>444274705750027474
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476648959125437941)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_valor_cargar     NUMBER;',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
unistr('  ln_valor_retencion  asdm_retenciones_aplicadas.rap_valor_retencion%TYPE; -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n'),
'',
'  ln_cuenta  NUMBER;',
'  lv_anio    VARCHAR2(4) := ''2011'';',
'  lv_mensaje VARCHAR2(3000);',
'  raise_puntaje EXCEPTION;',
'',
'BEGIN',
'',
'  ln_valor_retencion  := 0;',
'  ln_com_id_retencion := NULL;',
'',
'  IF :p124_tfp_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tfp_id_tarjeta_credito'') OR',
'     :p124_tfp_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tfp_id_plan_gobierno'') THEN',
'    :p124_ede_id             := :p124_plan;',
'    :p124_cre_titular_cuenta := :p124_titular_tc; -- Para que se muestre en el mismo titular del cheque pero igual se guarde en el titular de TC',
'  END IF;',
'',
'  /*Agregado por Andres Calle',
'  14/Octubre/2011',
'  valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'  */',
'  IF :p124_tfp_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tfp_id_tarjeta_credito'') OR',
'     :p124_tfp_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tfp_id_plan_gobierno'') AND',
'     :p124_comision > 0 THEN',
'  ',
'    IF :p124_es_politica_gobierno = 0 THEN',
'      pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                  pn_ede_id   => :p124_ede_id,',
'                                                  pn_comision => :p124_comision,',
'                                                  pv_error    => :p0_error);',
'    END IF;',
'  END IF;',
'',
'   IF :p0_error IS NULL THEN',
'  ',
'    pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                    pn_tfp_id => :p124_tfp_id,',
'                                                    pv_error  => :p0_error);',
'  ',
'    IF :p0_error IS NULL THEN',
unistr('      -- Agregado por Yguaman 2011/11/17, porque a pesar del error de formas de pago igual graba en la colecci\00C3\00B3n movimientos'),
'    ',
unistr('      --- Yguaman 2011/10/04 11:59am a\00C3\00B1adido para guardar nueva forma de pago "retenci\00C3\00B3n"    '),
'      SELECT COUNT(*)',
'        INTO ln_cuenta',
'        FROM asdm_cheques_recibidos r',
'       WHERE r.cre_nro_cheque = nvl(:p124_cre_nro_cheque, 0)',
'         AND r.cre_nro_cuenta = nvl(:p124_cre_nro_cuenta, 0)',
'         AND r.cre_nro_pin = nvl(:p124_cre_nro_pin, 0)',
'         AND r.ech_id = 2;',
'    ',
'      IF ln_cuenta > 0 THEN',
'        raise_application_error(-20000,',
'                                ''EL CHEQUE'' || :p124_cre_nro_cheque ||',
'                                '' cuenta '' || :p124_cre_nro_cuenta ||',
'                                '' pin '' || :p124_cre_nro_pin ||',
'                                '' YA ESTA REGISTRADO NO PUEDE VOLVER A PAGAR CON EL MISMO CHEQUE'');',
'      END IF;',
'    ',
'      pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => :p124_ede_id,',
'                                                          pn_cre_id               => :p124_cre_id,',
'                                                          pn_tfp_id               => :p124_tfp_id,',
'                                                          pn_mcd_valor_movimiento => nvl(to_number(:p124_mcd_valor_movimiento),',
'                                                                                         0), --:p124_mcd_valor_movimiento',
'                                                          pn_mcd_nro_aut_ref      => :p124_mcd_nro_aut_ref,',
'                                                          pn_mcd_nro_lote         => :p124_lote,',
'                                                          pn_mcd_nro_retencion    => :p124_rap_nro_retencion,',
'                                                          pn_mcd_nro_comprobante  => :p124_mcd_nro_comprobante, --NULL,',
'                                                          pv_titular_cuenta       => :p124_cre_titular_cuenta, --',
'                                                          pv_cre_nro_cheque       => :p124_cre_nro_cheque,',
'                                                          pv_cre_nro_cuenta       => :p124_cre_nro_cuenta,',
'                                                          pv_nro_pin              => :p124_cre_nro_pin,',
'                                                          pv_nro_tarjeta          => :p124_tj_numero,',
'                                                          pv_voucher              => :p124_tj_numero_voucher,',
'                                                          pn_saldo_retencion      => nvl(to_number(:p124_saldo_retencion),',
unistr('                                                                                         0), --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_com_id               => ln_com_id_retencion, --:p124_RAP_COM_ID, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pd_rap_fecha            => :p124_rap_fecha, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pd_rap_fecha_validez    => :p124_rap_fecha_validez, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_rap_nro_autorizacion => :p124_rap_nro_autorizacion, --- Yguaman 2011/10/04 pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_rap_nro_establ_ret   => :p124_rap_nro_establ_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_rap_nro_pemision_ret => :p124_rap_nro_pemision_ret, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('                                                          pn_pre_id               => :p124_pre_id, -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'                                                          pd_fecha_deposito       => :p124_mcd_fecha_deposito,',
unistr('                                                          --                                                    pn_rap_valor_retencion  => ln_valor_retencion  , -- Yguaman  2011/10/10 para pago de cuota con retenci\00C3\00B3n '),
'                                                          pv_titular_tarjeta_cre => :p124_titular_tc, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito                                                      ',
'                                                          pv_error               => :p0_error);',
'    ',
'      --- Null los campos ---',
'    ',
'      :p124_seq_id               := NULL;',
'      :p124_cre_id               := NULL;',
'      :p124_cre_fecha_deposito   := SYSDATE;',
'      :p124_ede_id               := NULL;',
'      :p124_cre_titular_cuenta   := NULL;',
'      :p124_cre_nro_cheque       := NULL;',
'      :p124_cre_nro_cuenta       := NULL;',
'      :p124_cre_nro_pin          := NULL;',
'      :p124_mcd_valor_movimiento := NULL;',
'    ',
unistr('      --- Yguaman 2011/10/04 19:32pm usado para el pago con retenci\00C3\00B3n y deja en null todos los campos'),
'      :p124_mcd_fecha_deposito := NULL;',
'      :p124_tj_entidad         := NULL;',
'      :p124_bancos             := NULL;',
'      :p124_tarjeta            := NULL;',
'      :p124_tipo               := NULL;',
'      :p124_plan               := NULL;',
'      --  :p124_TJ_TITULAR := NULL;',
'      :p124_tj_numero           := NULL;',
'      :p124_tj_numero_voucher   := NULL;',
'      :p124_tj_recap            := NULL;',
'      :p124_lote                := NULL;',
'      :p124_mcd_nro_aut_ref     := NULL;',
'      :p124_mcd_nro_comprobante := NULL;',
'      :p124_modificacion        := NULL;',
'      :p124_mcd_fecha_deposito  := NULL;',
'    ',
'      --- Retencion',
'      :p124_rap_nro_retencion    := NULL;',
'      :p124_rap_nro_autorizacion := NULL;',
'      :p124_rap_fecha            := NULL;',
'      :p124_rap_fecha_validez    := NULL;',
'      :p124_rap_com_id           := NULL;',
'      :p124_saldo_retencion      := NULL;',
unistr('      :p124_rap_nro_establ_ret   := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('      :p124_rap_nro_pemision_ret := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
unistr('      :p124_pre_id               := NULL; -- Yguaman  2011/10/06 para pago de cuota con retenci\00C3\00B3n'),
'    ',
'      -- tarjetas de credito',
unistr('      -- este campo lo lleno solo desde la facturaci\00C3\00B3n, cuando se factura una orden con tarjeta envio directamente la entrada y luego la vacio'),
'      :p124_entrada := NULL;',
'    ',
'    END IF;',
'  END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cargar_tfp'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>444395807855673015
,p_process_comment=>unistr('Se ejecuta con el submit creado en el screep del head de la p\00C3\00A1gina, este screep se ejecuta al dar click en el boton grabar, envia el url para imprimir la factura')
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476696570505762964)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_continuar'
,p_process_sql_clob=>':p124_continuar := ''S'';'
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(476570258610775464)
,p_internal_uid=>444443419235998038
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(79192277559737046)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_Inicio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_col_mov_caja varchar2(60):= pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja'');',
'begin',
':P124_IDENTIFICACION := NULL;',
'	:P124_CLI_ID	:= NULL;',
'	:P124_NOMBRE	:= NULL;',
'	:P124_TIPO_DIRECCION	:= NULL;',
'	:P124_DIRECCION	:= NULL;',
'	:P124_TIPO_TELEFONO	:= NULL;',
'	:P124_TELEFONO	:= NULL;',
'	:P124_CORREO := NULL;',
':P124_CXC_ID := NULL;',
' IF apex_collection.collection_exists(''COL_CXC_PRECANCELA'') THEN',
'      apex_collection.delete_collection(''COL_CXC_PRECANCELA'');',
' END IF;',
'',
' IF apex_collection.collection_exists(lv_col_mov_caja) THEN',
'      apex_collection.delete_collection(lv_col_mov_caja);',
' END IF;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P124_CLI_ID'
,p_process_when_type=>'ITEM_IS_NULL'
,p_internal_uid=>46939126289972120
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476676782215586558)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_precancelacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_precancelacion.pr_graba_precancelacion(pn_emp_id         => :f_emp_id,',
'                        pn_cxc_id         => :p124_cxc_id,',
'                        pn_tse_id         => :f_seg_id,',
'                        pn_uge_id         => :f_uge_id,',
'                        pn_cli_id         => :p124_clI_id,',
'                        pn_user_id        => :f_user_id,',
'                        pn_age_id_agencia => :f_age_id_agencia,',
'                        pv_error          => :p0_error);',
'',
'',
':P124_OBSERVACION := null;',
':P124_GENERA_PREC := null;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'PAGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'PROCESO REALIZADO CORRECTAMENTE'
,p_internal_uid=>444423630945821632
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(476690380539690150)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number;',
'begin',
'',
'select count(*) ',
'into ln_cuenta',
'from apex_collections where collection_name = pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja'')',
'and seq_id = :p124_seq_id;',
'if ln_cuenta > 0 then',
'apex_collection.delete_member(Pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja''),:p124_seq_id);',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>444437229269925224
);
wwv_flow_imp.component_end;
end;
/
