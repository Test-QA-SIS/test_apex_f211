prompt --application/pages/page_00153
begin
--   Manifest
--     PAGE: 00153
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>153
,p_name=>'NC Devolucion Simple'
,p_step_title=>'NC Devolucion Simple'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'function imprimir(url){',
'  ',
'   doSubmit(''graba_nc'');',
'   window.open(url,"", "width=808, height=565, top=85");',
'		} ',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44357358572591957)
,p_name=>'variables calculadas'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'        from (SELECT distinct a.var_id,',
'                              b.mca_id,',
'                              b.ttr_id,',
'                              c.pol_id,',
'                              b.vtt_orden,',
'                              a.var_descripcion,',
'                              b.vtt_mostrar_detalle,',
'                              b.vtt_mostrar_cabecera,',
'                              b.vtt_ingr_detalle,',
'                              b.vtt_ingr_cabecera,',
'                              b.vtt_ant_etiqueta || '' '' || b.vtt_etiqueta vtt_etiqueta,',
'                              b.vtt_prorrateo,',
'                              b.vtt_distribucion,',
'                              b.vtt_obtiene_de_documento,',
'                              b.vtt_func_graba_cab,',
'                              d.ite_sku_id sku_id,',
'                              to_number(e.ite_sku_id) sku_rel_id,',
'                              e.tre_id,',
'                              i.bpr_id,',
'                              d.cei_id,',
'                              null ieb_id,',
'                              f.vcd_valor_variable',
'              ',
'                FROM ppr_variables               a,',
'                     ppr_variables_ttransaccion  b,',
'                     ven_comprobantes            c,',
'                     ven_comprobantes_det        d,',
'                     ven_var_comprobantes_det    f,',
'                     ven_comprobantes_det_rel    e,',
'                     inv_comprobantes_inventario i,',
'                     apex_collections            l',
'               WHERE a.emp_id = :f_emp_id',
'                 AND a.emp_id = b.emp_id',
'                 AND a.var_id = b.var_id',
'                 AND a.var_id = f.var_id',
'                 AND b.ttr_id = 103',
'                 AND f.cde_id = d.cde_id',
'                 AND d.emp_id = a.emp_id',
'                 AND c.com_id = d.com_id',
'                 AND c.emp_id = d.emp_id',
'                 AND d.cde_id = e.cde_id(+)',
'                 AND i.com_id = c.com_id',
'                 AND d.com_id = i.com_id',
'                 AND c.com_id = 23632',
'                 AND i.cin_id = 362044',
'                 and l.collection_name = ''CO_DETALLE''',
'                 and d.ite_sku_id = l.c003',
'                 and i.bpr_id = l.c031 --unificar diferentes bodegas tomas solo de la bodega dond se ingreso la mercaderIA',
'                 and l.c006 is null',
'              union',
'              -- GEX',
'              SELECT distinct a.var_id,',
'                              b.mca_id,',
'                              b.ttr_id,',
'                              c.pol_id,',
'                              b.vtt_orden,',
'                              a.var_descripcion,',
'                              b.vtt_mostrar_detalle,',
'                              b.vtt_mostrar_cabecera,',
'                              b.vtt_ingr_detalle,',
'                              b.vtt_ingr_cabecera,',
'                              b.vtt_ant_etiqueta || '' '' || b.vtt_etiqueta vtt_etiqueta,',
'                              b.vtt_prorrateo,',
'                              b.vtt_distribucion,',
'                              b.vtt_obtiene_de_documento,',
'                              b.vtt_func_graba_cab,',
'                              d.ite_sku_id sku_id,',
'                              to_number(l.c006) sku_rel_id,',
'                              to_number(l.c007) tre_id,',
'                              i.bpr_id bpr_id,',
'                              d.cei_id,',
'                              null ieb_id,',
'                              f.vcd_valor_variable',
'                FROM ppr_variables               a,',
'                     ppr_variables_ttransaccion  b,',
'                     ven_comprobantes            c,',
'                     ven_comprobantes_det        d,',
'                     ven_var_comprobantes_det    f,',
'                     ven_comprobantes_det_rel    e,',
'                     inv_comprobantes_inventario i,',
'                     ',
'                     apex_collections l',
'               WHERE a.emp_id = :f_emp_id',
'                 AND a.emp_id = b.emp_id',
'                 AND a.var_id = b.var_id',
'                 AND a.var_id = f.var_id',
'                 AND b.ttr_id = 103',
'                 AND f.cde_id = d.cde_id',
'                 AND d.emp_id = a.emp_id',
'                 AND c.com_id = d.com_id',
'                 AND d.com_id = i.com_id',
'                 AND i.cin_id = 362044',
'                 AND c.emp_id = d.emp_id',
'                 AND d.cde_id = e.cde_id',
'                 AND c.com_id = 23632',
'                 and l.collection_name = ''CO_DETALLE''',
'                 and d.ite_sku_id = l.c003',
'                 and e.ite_sku_id = l.c006',
'                 and i.bpr_id = l.c031 --unificar diferentes bodegas tomas solo de la bodega dond se ingreso la mercaderIA',
'                 and l.c006 is not null',
'              union',
'              --items tipo regalo',
'              SELECT distinct a.var_id,',
'                              b.mca_id,',
'                              b.ttr_id,',
'                              c.pol_id,',
'                              b.vtt_orden,',
'                              a.var_descripcion,',
'                              b.vtt_mostrar_detalle,',
'                              b.vtt_mostrar_cabecera,',
'                              b.vtt_ingr_detalle,',
'                              b.vtt_ingr_cabecera,',
'                              b.vtt_ant_etiqueta || '' '' || b.vtt_etiqueta vtt_etiqueta,',
'                              b.vtt_prorrateo,',
'                              b.vtt_distribucion,',
'                              b.vtt_obtiene_de_documento,',
'                              b.vtt_func_graba_cab,',
'                              d.ite_sku_id sku_id,',
'                              to_number(e.ite_sku_id) sku_rel_id,',
'                              e.tre_id,',
'                              null bpr_id,',
'                              d.cei_id,',
'                              null ieb_id,',
'                              f.vcd_valor_variable',
'                FROM ppr_variables              a,',
'                     ppr_variables_ttransaccion b,',
'                     ven_comprobantes           c,',
'                     ven_comprobantes_det       d,',
'                     ven_var_comprobantes_det   f,',
'                     ven_comprobantes_det_rel   e,',
'                     apex_collections           l',
'               WHERE a.emp_id = :f_emp_id',
'                 AND a.emp_id = b.emp_id',
'                 AND a.var_id = b.var_id',
'                 AND a.var_id = f.var_id',
'                 AND b.ttr_id = 103',
'                 AND f.cde_id = d.cde_id',
'                 AND d.emp_id = a.emp_id',
'                 AND c.com_id = d.com_id',
'                 AND c.emp_id = d.emp_id',
'                 AND d.cde_id = e.cde_id(+)',
'                 AND c.com_id = 23632',
'                 and l.collection_name = ''CO_DETALLE''',
'                 and d.ite_sku_id = l.c003',
'                 and l.c006 is null',
'                 and 0 = 0 -- para que en las parciales no cargue los regalos',
'              )',
'       order by vtt_orden'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38951130409274851)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38951544182274860)
,p_query_column_id=>2
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>2
,p_column_heading=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38951764180274860)
,p_query_column_id=>3
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>3
,p_column_heading=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38952169593274861)
,p_query_column_id=>4
,p_column_alias=>'POL_ID'
,p_column_display_sequence=>4
,p_column_heading=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38952590828274861)
,p_query_column_id=>5
,p_column_alias=>'VTT_ORDEN'
,p_column_display_sequence=>5
,p_column_heading=>'VTT_ORDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38953034368274862)
,p_query_column_id=>6
,p_column_alias=>'VAR_DESCRIPCION'
,p_column_display_sequence=>6
,p_column_heading=>'VAR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38953439589274862)
,p_query_column_id=>7
,p_column_alias=>'VTT_MOSTRAR_DETALLE'
,p_column_display_sequence=>7
,p_column_heading=>'VTT_MOSTRAR_DETALLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38953816625274862)
,p_query_column_id=>8
,p_column_alias=>'VTT_MOSTRAR_CABECERA'
,p_column_display_sequence=>8
,p_column_heading=>'VTT_MOSTRAR_CABECERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38954249722274863)
,p_query_column_id=>9
,p_column_alias=>'VTT_INGR_DETALLE'
,p_column_display_sequence=>9
,p_column_heading=>'VTT_INGR_DETALLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38954557881274863)
,p_query_column_id=>10
,p_column_alias=>'VTT_INGR_CABECERA'
,p_column_display_sequence=>10
,p_column_heading=>'VTT_INGR_CABECERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38954956624274863)
,p_query_column_id=>11
,p_column_alias=>'VTT_ETIQUETA'
,p_column_display_sequence=>11
,p_column_heading=>'VTT_ETIQUETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38955383938274864)
,p_query_column_id=>12
,p_column_alias=>'VTT_PRORRATEO'
,p_column_display_sequence=>12
,p_column_heading=>'VTT_PRORRATEO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38955831908274864)
,p_query_column_id=>13
,p_column_alias=>'VTT_DISTRIBUCION'
,p_column_display_sequence=>13
,p_column_heading=>'VTT_DISTRIBUCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38956211228274864)
,p_query_column_id=>14
,p_column_alias=>'VTT_OBTIENE_DE_DOCUMENTO'
,p_column_display_sequence=>14
,p_column_heading=>'VTT_OBTIENE_DE_DOCUMENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38956556386274865)
,p_query_column_id=>15
,p_column_alias=>'VTT_FUNC_GRABA_CAB'
,p_column_display_sequence=>15
,p_column_heading=>'VTT_FUNC_GRABA_CAB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38957028821274865)
,p_query_column_id=>16
,p_column_alias=>'SKU_ID'
,p_column_display_sequence=>16
,p_column_heading=>'SKU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38957411191274866)
,p_query_column_id=>17
,p_column_alias=>'SKU_REL_ID'
,p_column_display_sequence=>17
,p_column_heading=>'SKU_REL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38957757134274867)
,p_query_column_id=>18
,p_column_alias=>'TRE_ID'
,p_column_display_sequence=>18
,p_column_heading=>'TRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38958158099274867)
,p_query_column_id=>19
,p_column_alias=>'BPR_ID'
,p_column_display_sequence=>19
,p_column_heading=>'BPR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38958614366274869)
,p_query_column_id=>20
,p_column_alias=>'CEI_ID'
,p_column_display_sequence=>20
,p_column_heading=>'CEI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38959051044274869)
,p_query_column_id=>21
,p_column_alias=>'IEB_ID'
,p_column_display_sequence=>21
,p_column_heading=>'IEB_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38959419074274869)
,p_query_column_id=>22
,p_column_alias=>'VCD_VALOR_VARIABLE'
,p_column_display_sequence=>22
,p_column_heading=>'VCD_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44359777537591961)
,p_plug_name=>'ADICIONALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44360959205591965)
,p_plug_name=>'Datos Originales Factura'
,p_parent_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>9
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44361573407591966)
,p_name=>'DatosCredito'
,p_parent_plug_id=>wwv_flow_imp.id(44360959205591965)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (CASE',
'         WHEN d.div_nro_vencimiento = 0 THEN',
'          ''ENTRADA''',
'         ELSE',
'          ''CUOTA''',
'       END) TIPO,',
'      ',
'       d.div_valor_cuota valor',
'  FROM car_dividendos d, car_cuentas_por_cobrar c',
' WHERE c.com_id = :P153_COM_ID_FACTURA',
'   AND c.cxc_id = d.cxc_id',
'   ANd c.cxc_id = :P153_CXC_ID',
'   AND div_nro_vencimiento IN (0, 1);',
'',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38964666581274907)
,p_query_column_id=>1
,p_column_alias=>'TIPO'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38965139937274907)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(2808698404149286590)
,p_name=>'Cartera'
,p_parent_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.cxc_id,',
'       c.cxc_id_refin,',
'       c.cxc_saldo,',
'       c.cxc_fecha_adicion,',
'       c.cxc_plazo,',
'       c.cxc_estado,',
'       f.ref_saldo_interes_np,',
'       f.valor_nota_debito',
'  FROM CAR_CUENTAS_POR_COBRAR c, car_refinanciamientos f',
' WHERE C.com_id = :P153_COM_ID_FACTURA',
'   and f.cxc_id_ant(+) = c.cxc_id',
' order by 1',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38965813430274909)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38966217102274910)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID_REFIN'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id refin'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38966601825274910)
,p_query_column_id=>3
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>3
,p_column_heading=>'Cxc saldo'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38967035396274910)
,p_query_column_id=>4
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>4
,p_column_heading=>'Cxc fecha adicion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38967413330274911)
,p_query_column_id=>5
,p_column_alias=>'CXC_PLAZO'
,p_column_display_sequence=>5
,p_column_heading=>'Cxc plazo'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38967766522274911)
,p_query_column_id=>6
,p_column_alias=>'CXC_ESTADO'
,p_column_display_sequence=>6
,p_column_heading=>'Cxc estado'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38968229946274911)
,p_query_column_id=>7
,p_column_alias=>'REF_SALDO_INTERES_NP'
,p_column_display_sequence=>7
,p_column_heading=>'Ref saldo interes np'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38968586051274911)
,p_query_column_id=>8
,p_column_alias=>'VALOR_NOTA_DEBITO'
,p_column_display_sequence=>8
,p_column_heading=>'Valor nota debito'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(2808699315423286599)
,p_name=>'Notas de Credito y Notas de Debito ya generadas'
,p_parent_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>12
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COM_ID,',
'       COM_TIPO,',
'       COM_FECHA,',
'       (CASE',
'         WHEN VAL = 0 THEN',
'          VAL_ANT',
'         ELSE',
'          VAL',
'       END) Valor,',
'       COM_OBSERVACION,',
'       TTR_ID,',
'       TTR_DESCRIPCION,',
'       MNC_DESCRIPCION',
'  from (SELECT r.com_id,',
'               r.com_tipo,',
'               r.com_fecha,',
'               r.com_observacion,',
'               m.mnc_descripcion,',
'               r.ttr_id,',
'               t.ttr_descripcion,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total_cred_entrada'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) Val,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) VAL_ANT',
'          FROM VEN_COMPROBANTES         R,',
'               asdm_motivos_nc          m,',
'               asdm_tipos_transacciones t',
'         ',
'         WHERE R.COM_ID_REF = :P153_COM_ID_FACTURA',
'           and m.mnc_id(+) = r.mnc_id',
'        and r.com_estado_registro= 0',
'           and t.ttr_id(+) = r.ttr_id)'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38969281645274912)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Com id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38969666093274913)
,p_query_column_id=>2
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>2
,p_column_heading=>'Com tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38970132054274913)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'Com fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38970457046274913)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38970868877274913)
,p_query_column_id=>5
,p_column_alias=>'COM_OBSERVACION'
,p_column_display_sequence=>5
,p_column_heading=>'Com observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38971344150274914)
,p_query_column_id=>6
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Ttr id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38971685859274914)
,p_query_column_id=>7
,p_column_alias=>'TTR_DESCRIPCION'
,p_column_display_sequence=>7
,p_column_heading=>'Ttr descripcion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38972114687274914)
,p_query_column_id=>8
,p_column_alias=>'MNC_DESCRIPCION'
,p_column_display_sequence=>8
,p_column_heading=>'Mnc descripcion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(10402551662743612924)
,p_name=>'Notas de Credito y Notas de Debito ya generadas'
,p_parent_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>150
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COM_ID,',
'       COM_TIPO,',
'       COM_FECHA,',
'       (CASE',
'         WHEN VAL = 0 THEN',
'          VAL_ANT',
'         ELSE',
'          VAL',
'       END) Valor,',
'       COM_OBSERVACION,',
'       TTR_ID,',
'       TTR_DESCRIPCION,',
'       MNC_DESCRIPCION,',
'       com_estado_registro',
'  from (SELECT r.com_id,',
'               r.com_tipo,',
'               r.com_fecha,',
'               r.com_observacion,',
'               m.mnc_descripcion,',
'               r.ttr_id,',
'               t.ttr_descripcion,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total_cred_entrada'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) Val,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) VAL_ANT,',
'        r.com_estado_registro',
'          FROM VEN_COMPROBANTES         R,',
'               asdm_motivos_nc          m,',
'               asdm_tipos_transacciones t',
'         WHERE R.COM_ID_REF = :P153_COM_ID_FACTURA',
'           and m.mnc_id(+) = r.mnc_id',
'           and t.ttr_id(+) = r.ttr_id)'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen ND o NC generadas para esta factura'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38972782060274918)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38973242164274918)
,p_query_column_id=>2
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>2
,p_column_heading=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38973626530274919)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38973953781274919)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38974428903274919)
,p_query_column_id=>5
,p_column_alias=>'COM_OBSERVACION'
,p_column_display_sequence=>5
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38974775113274919)
,p_query_column_id=>6
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>6
,p_column_heading=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38975243653274920)
,p_query_column_id=>7
,p_column_alias=>'TTR_DESCRIPCION'
,p_column_display_sequence=>7
,p_column_heading=>'DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38975586471274920)
,p_query_column_id=>8
,p_column_alias=>'MNC_DESCRIPCION'
,p_column_display_sequence=>8
,p_column_heading=>'MOTIVO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38976029954274920)
,p_query_column_id=>9
,p_column_alias=>'COM_ESTADO_REGISTRO'
,p_column_display_sequence=>9
,p_column_heading=>'Com estado registro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44364448694591968)
,p_plug_name=>'<B>&P0_TTR_DESCRIPCION.   -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>'&P0_DATFOLIO.'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44370260252591976)
,p_name=>'Detalle Nota Credito'
,p_display_sequence=>20
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return  pq_ven_nota_credito.fn_apex_item_notacre_det(pn_emp_id => :f_emp_id,',
'                                                     pn_com_id => :P153_COM_ID_FACTURA,',
'                                                     pn_ttr_id =>  pq_constantes.fn_retorna_constante(0,''cn_ttr_id_nota_credito''),',
'                                                     pv_error => :p0_error);'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>120
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'LEFT'
,p_prn_page_footer_alignment=>'LEFT'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38989434173274936)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38989762260274937)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38990225818274937)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38990592062274938)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38991009441274938)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38991445129274938)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38991752200274939)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38992224810274939)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38992554104274939)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38992967322274940)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38993369558274940)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38993797527274940)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39008571272274952)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38994241409274941)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38994625155274941)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38995004275274941)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38995431716274941)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38995784604274942)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38996180831274943)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38996568001274943)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38997038447274943)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38997386852274943)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38997800973274944)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38998239970274944)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38998584447274944)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38998965060274944)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38999388381274945)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38999769119274945)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39000209781274945)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39000604869274946)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39000958699274946)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39001426256274946)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39001795420274946)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39002207843274947)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39002563645274947)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39003012937274947)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39003352157274948)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39003803390274948)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39004207638274948)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39004632994274949)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39004967344274950)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39005414543274950)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39005807682274950)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39006235314274950)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39006630278274951)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39007007244274951)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39007415421274951)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39007756591274951)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39008167831274952)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39008980903274952)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39009441031274952)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39009769000274953)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39010229373274953)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39010650143274953)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39010982943274954)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39011442557274954)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39011786233274954)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39012224366274954)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39012609063274955)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39012890867274955)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39013348657274955)
,p_query_column_id=>61
,p_column_alias=>'COL61'
,p_column_display_sequence=>61
,p_column_heading=>'Col61'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39013678591274955)
,p_query_column_id=>62
,p_column_alias=>'COL62'
,p_column_display_sequence=>62
,p_column_heading=>'Col62'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39014129372274956)
,p_query_column_id=>63
,p_column_alias=>'COL63'
,p_column_display_sequence=>63
,p_column_heading=>'Col63'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39014469989274956)
,p_query_column_id=>64
,p_column_alias=>'COL64'
,p_column_display_sequence=>64
,p_column_heading=>'Col64'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39014905479274956)
,p_query_column_id=>65
,p_column_alias=>'COL65'
,p_column_display_sequence=>65
,p_column_heading=>'Col65'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39015331050274956)
,p_query_column_id=>66
,p_column_alias=>'COL66'
,p_column_display_sequence=>66
,p_column_heading=>'Col66'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39015682119274957)
,p_query_column_id=>67
,p_column_alias=>'COL67'
,p_column_display_sequence=>67
,p_column_heading=>'Col67'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39016059997274957)
,p_query_column_id=>68
,p_column_alias=>'COL68'
,p_column_display_sequence=>68
,p_column_heading=>'Col68'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39016525647274957)
,p_query_column_id=>69
,p_column_alias=>'COL69'
,p_column_display_sequence=>69
,p_column_heading=>'Col69'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39016865119274957)
,p_query_column_id=>70
,p_column_alias=>'COL70'
,p_column_display_sequence=>70
,p_column_heading=>'Col70'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39017281632274958)
,p_query_column_id=>71
,p_column_alias=>'COL71'
,p_column_display_sequence=>71
,p_column_heading=>'Col71'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39017707897274958)
,p_query_column_id=>72
,p_column_alias=>'COL72'
,p_column_display_sequence=>72
,p_column_heading=>'Col72'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39018120494274958)
,p_query_column_id=>73
,p_column_alias=>'COL73'
,p_column_display_sequence=>73
,p_column_heading=>'Col73'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39018452997274959)
,p_query_column_id=>74
,p_column_alias=>'COL74'
,p_column_display_sequence=>74
,p_column_heading=>'Col74'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39018930509274959)
,p_query_column_id=>75
,p_column_alias=>'COL75'
,p_column_display_sequence=>75
,p_column_heading=>'Col75'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39019292354274960)
,p_query_column_id=>76
,p_column_alias=>'COL76'
,p_column_display_sequence=>76
,p_column_heading=>'Col76'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39019738316274960)
,p_query_column_id=>77
,p_column_alias=>'COL77'
,p_column_display_sequence=>77
,p_column_heading=>'Col77'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39020133025274960)
,p_query_column_id=>78
,p_column_alias=>'COL78'
,p_column_display_sequence=>78
,p_column_heading=>'Col78'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39020536042274961)
,p_query_column_id=>79
,p_column_alias=>'COL79'
,p_column_display_sequence=>79
,p_column_heading=>'Col79'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39020856635274961)
,p_query_column_id=>80
,p_column_alias=>'COL80'
,p_column_display_sequence=>80
,p_column_heading=>'Col80'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39021281737274964)
,p_query_column_id=>81
,p_column_alias=>'COL81'
,p_column_display_sequence=>81
,p_column_heading=>'Col81'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39021744866274964)
,p_query_column_id=>82
,p_column_alias=>'COL82'
,p_column_display_sequence=>82
,p_column_heading=>'Col82'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39022103631274964)
,p_query_column_id=>83
,p_column_alias=>'COL83'
,p_column_display_sequence=>83
,p_column_heading=>'Col83'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39022542799274964)
,p_query_column_id=>84
,p_column_alias=>'COL84'
,p_column_display_sequence=>84
,p_column_heading=>'Col84'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39022908307274965)
,p_query_column_id=>85
,p_column_alias=>'COL85'
,p_column_display_sequence=>85
,p_column_heading=>'Col85'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39023262099274965)
,p_query_column_id=>86
,p_column_alias=>'COL86'
,p_column_display_sequence=>86
,p_column_heading=>'Col86'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39023706112274965)
,p_query_column_id=>87
,p_column_alias=>'COL87'
,p_column_display_sequence=>87
,p_column_heading=>'Col87'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39024098753274965)
,p_query_column_id=>88
,p_column_alias=>'COL88'
,p_column_display_sequence=>88
,p_column_heading=>'Col88'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39024493438274966)
,p_query_column_id=>89
,p_column_alias=>'COL89'
,p_column_display_sequence=>89
,p_column_heading=>'Col89'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39024935567274966)
,p_query_column_id=>90
,p_column_alias=>'COL90'
,p_column_display_sequence=>90
,p_column_heading=>'Col90'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39025306299274966)
,p_query_column_id=>91
,p_column_alias=>'COL91'
,p_column_display_sequence=>91
,p_column_heading=>'Col91'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39025676403274966)
,p_query_column_id=>92
,p_column_alias=>'COL92'
,p_column_display_sequence=>92
,p_column_heading=>'Col92'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39026096819274967)
,p_query_column_id=>93
,p_column_alias=>'COL93'
,p_column_display_sequence=>93
,p_column_heading=>'Col93'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39026461420274967)
,p_query_column_id=>94
,p_column_alias=>'COL94'
,p_column_display_sequence=>94
,p_column_heading=>'Col94'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39026884638274967)
,p_query_column_id=>95
,p_column_alias=>'COL95'
,p_column_display_sequence=>95
,p_column_heading=>'Col95'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39027328818274967)
,p_query_column_id=>96
,p_column_alias=>'COL96'
,p_column_display_sequence=>96
,p_column_heading=>'Col96'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39027704545274968)
,p_query_column_id=>97
,p_column_alias=>'COL97'
,p_column_display_sequence=>97
,p_column_heading=>'Col97'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39028113364274968)
,p_query_column_id=>98
,p_column_alias=>'COL98'
,p_column_display_sequence=>98
,p_column_heading=>'Col98'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39028470606274968)
,p_query_column_id=>99
,p_column_alias=>'COL99'
,p_column_display_sequence=>99
,p_column_heading=>'Col99'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39028943261274968)
,p_query_column_id=>100
,p_column_alias=>'COL100'
,p_column_display_sequence=>100
,p_column_heading=>'Col100'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39029271651274969)
,p_query_column_id=>101
,p_column_alias=>'COL101'
,p_column_display_sequence=>101
,p_column_heading=>'Col101'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39029722500274969)
,p_query_column_id=>102
,p_column_alias=>'COL102'
,p_column_display_sequence=>102
,p_column_heading=>'Col102'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39030054154274969)
,p_query_column_id=>103
,p_column_alias=>'COL103'
,p_column_display_sequence=>103
,p_column_heading=>'Col103'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39030504487274970)
,p_query_column_id=>104
,p_column_alias=>'COL104'
,p_column_display_sequence=>104
,p_column_heading=>'Col104'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39030947251274970)
,p_query_column_id=>105
,p_column_alias=>'COL105'
,p_column_display_sequence=>105
,p_column_heading=>'Col105'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39031298104274970)
,p_query_column_id=>106
,p_column_alias=>'COL106'
,p_column_display_sequence=>106
,p_column_heading=>'Col106'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39031707032274970)
,p_query_column_id=>107
,p_column_alias=>'COL107'
,p_column_display_sequence=>107
,p_column_heading=>'Col107'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39032091583274971)
,p_query_column_id=>108
,p_column_alias=>'COL108'
,p_column_display_sequence=>108
,p_column_heading=>'Col108'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39032477350274971)
,p_query_column_id=>109
,p_column_alias=>'COL109'
,p_column_display_sequence=>109
,p_column_heading=>'Col109'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39032851446274971)
,p_query_column_id=>110
,p_column_alias=>'COL110'
,p_column_display_sequence=>110
,p_column_heading=>'Col110'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39033335590274971)
,p_query_column_id=>111
,p_column_alias=>'COL111'
,p_column_display_sequence=>111
,p_column_heading=>'Col111'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39033678623274972)
,p_query_column_id=>112
,p_column_alias=>'COL112'
,p_column_display_sequence=>112
,p_column_heading=>'Col112'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39034108122274972)
,p_query_column_id=>113
,p_column_alias=>'COL113'
,p_column_display_sequence=>113
,p_column_heading=>'Col113'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39034514930274972)
,p_query_column_id=>114
,p_column_alias=>'COL114'
,p_column_display_sequence=>114
,p_column_heading=>'Col114'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39034870770274973)
,p_query_column_id=>115
,p_column_alias=>'COL115'
,p_column_display_sequence=>115
,p_column_heading=>'Col115'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39035264824274973)
,p_query_column_id=>116
,p_column_alias=>'COL116'
,p_column_display_sequence=>116
,p_column_heading=>'Col116'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39035742827274973)
,p_query_column_id=>117
,p_column_alias=>'COL117'
,p_column_display_sequence=>117
,p_column_heading=>'Col117'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39036138272274973)
,p_query_column_id=>118
,p_column_alias=>'COL118'
,p_column_display_sequence=>118
,p_column_heading=>'Col118'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39036521341274974)
,p_query_column_id=>119
,p_column_alias=>'COL119'
,p_column_display_sequence=>119
,p_column_heading=>'Col119'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39036938781274974)
,p_query_column_id=>120
,p_column_alias=>'COL120'
,p_column_display_sequence=>120
,p_column_heading=>'Col120'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44384274702591987)
,p_name=>'Variables_detalle'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
' from ',
'apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                             ''cv_coleccionvardet'') ',
'--and c007 in (955,3,79,80,1,4,10,14,1014,3203,3202,3195)'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>5000
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'DescargarExcel'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_exp_filename=>'VariablesNotCred'
,p_plug_query_exp_separator=>','
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39041171783274977)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39041572883274978)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39042002520274978)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39042379025274978)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39042826210274978)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39043152170274979)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39043610904274979)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39044033476274979)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39044429141274979)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39044806541274980)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39045168095274980)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39045563950274980)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39045960966274980)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39046442673274981)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39046754141274981)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39047184067274981)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39047573172274981)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39048018076274982)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39048451248274982)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39048816922274982)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39049164078274982)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39049646793274983)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39049996303274983)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39050355771274983)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39050807068274984)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39051184382274988)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39051577680274989)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39051986633274989)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39052413049274989)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39052789908274989)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39053209971274990)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39053608153274990)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39053981027274990)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39054389977274990)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39054839322274990)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39055221466274991)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39055644985274991)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39056015175274991)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39056355029274992)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39056826922274992)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39057226663274992)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39057559961274992)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39058014319274993)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39058395299274993)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39058839501274993)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39059202094274993)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39059626583274994)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39059982821274994)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39060421703274994)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39060769498274994)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39061207955274995)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39061625855274995)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39061955238274995)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39062421778274995)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39062751521274996)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39063165858274996)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39063564916274996)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39064049254274996)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39064367985274997)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39064849440274997)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39065160179274997)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39065580390274997)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39066039746274998)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39066406679274998)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39066821786274998)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39067239344274998)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44391058171591993)
,p_name=>'CO_DETALLE'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                    ''cv_colecciondetalle'') '))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39067890387274999)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39068284291275000)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39068740210275000)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39069093633275000)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39069498030275000)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39069916500275001)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39070340371275001)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39070695253275001)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39071138950275001)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39071510565275002)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39071935946275002)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39072331445275002)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39072737621275002)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39073131901275003)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39073548345275005)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39073869419275005)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39074260459275005)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39074654720275005)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39075105110275006)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39075549815275006)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39075937627275006)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39076350218275006)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39076713903275007)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39077103343275007)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39077520669275007)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39077877091275007)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39078272995275008)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39078714274275008)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39079106284275008)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39079466382275008)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39079894638275008)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39080312664275009)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39080653407275009)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39081093115275009)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39081550124275009)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39081915286275010)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39082313345275010)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39082668306275010)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39083094023275010)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39083460445275011)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39083920216275011)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39084266132275011)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39084698764275011)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39085149907275012)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39085523309275012)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39085911936275012)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39086287392275012)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39086675805275013)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39087130507275013)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39087476004275013)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39087895673275013)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39088341554275014)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39088681116275014)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39089114494275014)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39089479019275014)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39089921552275015)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39090348563275015)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39090687756275015)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39091092184275015)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39091461330275016)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39091873493275016)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39092274257275016)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39092731576275016)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39093080878275017)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39093550623275017)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39093918434275017)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44397855646591997)
,p_name=>'VAR ENTRADA'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name  = ''CO_VARIABLES_ENTRADA'''
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'DescargarExcel'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_exp_filename=>'VariablesNotCredEntrada'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39094648294275018)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39094981663275018)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39095395980275019)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39095796931275019)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39096227899275019)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39096613283275019)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39096962266275020)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39097359772275020)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39097822308275020)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39098234856275021)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39098638194275021)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39098964357275021)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39099421990275022)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39099772130275022)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39100192158275022)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39100556353275023)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39101021562275023)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39101430381275023)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39101848183275023)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39102223915275024)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39102588747275024)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39102968088275024)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39103391333275024)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39103781006275025)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39104209704275025)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39104557947275025)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39104952772275026)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39105366926275026)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39105770363275026)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39106215447275026)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39106612749275027)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39106961296275027)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39107450913275027)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39107770476275027)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39108171083275028)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39108641972275028)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39108968862275028)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39109392592275028)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39109788718275029)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39110188170275029)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39110611245275029)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39110983526275029)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39111418585275030)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39111768597275030)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39112191155275030)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39112615132275030)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39113018281275031)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39113427793275031)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39113792160275031)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39114211419275032)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39114586295275032)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39115024136275032)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39115404208275032)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39115804622275032)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39116165548275033)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39116628498275033)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39117009363275033)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39117374280275034)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39117759455275034)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39118170055275034)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39118585608275034)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39118979643275035)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39119439799275035)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39119773660275035)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39120242967275035)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39120613635275036)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44404652590592001)
,p_name=>'TOTALES'
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_tot_nc(:P0_ERROR,:f_emp_id);'
,p_display_when_condition=>'CALCULAR'
,p_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270527874080046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'NO_HEADINGS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39121252424275037)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39121676450275037)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39122111649275037)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39122547810275038)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39122932058275038)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39123328942275038)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39123711024275038)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39124058663275039)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39124499779275039)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39124921860275039)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39125294896275039)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39125723770275040)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39126122326275040)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39126536640275040)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39126889899275041)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39127266788275041)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39127708931275042)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39128088112275042)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39128524510275042)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39128903793275043)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39129320058275043)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39129695309275043)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39130099382275044)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39130499925275044)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39130937810275044)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39131299640275044)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39131655514275045)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39132091053275045)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39132514547275046)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39132947323275046)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39133313164275046)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39133691393275047)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39134099681275047)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39134499978275047)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39134938561275048)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39135303313275048)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39135684689275048)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39136095668275049)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39136480090275049)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39136912004275049)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39137312122275050)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39137738854275050)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39138106423275050)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39138452308275051)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39138852880275051)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39139287840275051)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39139672719275052)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39140141147275052)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39140524324275052)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39140916544275053)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39141340406275053)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39141744295275053)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39142092019275054)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39142503947275054)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39142935009275054)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39143351036275055)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39143667483275055)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39144139294275055)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39144462322275055)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39144925347275056)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44411478424592006)
,p_plug_name=>'Clave de Autorizacion'
,p_parent_plug_id=>wwv_flow_imp.id(44404652590592001)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>110
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P153_VALIDA_PAGO_TARJETA > 0'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(160440377455821627)
,p_plug_name=>'Solicitar clave anticipo'
,p_parent_plug_id=>wwv_flow_imp.id(44404652590592001)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>130
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44412076524592007)
,p_name=>'Mov_caja_nota_credito'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'from apex_collections ',
'where collection_name=pq_constantes.fn_retorna_constante(NULL,',
'                                                           ''cv_coleccion_mov_caja'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39149822774275063)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39150166404275064)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39150599532275064)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>6
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39151048228275064)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>7
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39151433738275064)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>8
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39151775975275065)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>9
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39152156310275065)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>1
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39152643786275065)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>2
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39153003871275065)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>10
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39153391774275066)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>11
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39153835791275066)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>3
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39154216530275066)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39154585346275066)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39155014254275067)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39155382662275067)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39155752580275067)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39156223470275067)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39156618375275068)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39157001042275068)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39157447811275068)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39157805262275068)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39158157385275069)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39158596448275069)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39159005596275069)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39159445180275069)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39159781528275070)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39160229117275070)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39160583832275070)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39160971415275070)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39161444839275071)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39161849428275071)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39162204949275071)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39162645376275072)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39162966674275072)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39163414800275072)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39163778120275072)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39164186456275073)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39164622101275073)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39165044046275073)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39165435458275073)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39165798173275074)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39166183925275074)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39166641619275075)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39167030355275075)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39167410135275075)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39167768752275075)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39168240232275076)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39168633281275076)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39168986303275076)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39169428823275076)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39169844668275077)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39170195433275077)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39170596357275077)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39171031115275077)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39171397754275078)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39171843241275078)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39172212735275078)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39172556401275078)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39172999248275079)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39173360099275079)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39173803646275079)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39174209851275079)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39174579008275080)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39175043725275080)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39175379395275080)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39175753487275081)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(28590877003474805883)
,p_name=>'No Negativos'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'REGION_POSITION_05'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT c007 var_id, round(c012, 4) valor, v.vtt_etiqueta , a.c001, a.c002  ,a.c003,a.c004,a.c005,a.c006,a.c007,a.c008,a.c012   ',
'        FROM apex_collections a, ppr_variables_ttransaccion v',
'       WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'         AND a.c007 = v.var_id',
'         AND v.vtt_permite_negativos = ''N''',
'         AND v.ttr_id = 103',
'       ORDER BY seq_id;'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39176491529275082)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Var id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39176920521275082)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39177338111275082)
,p_query_column_id=>3
,p_column_alias=>'VTT_ETIQUETA'
,p_column_display_sequence=>3
,p_column_heading=>'Vtt etiqueta'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39177724522275082)
,p_query_column_id=>4
,p_column_alias=>'C001'
,p_column_display_sequence=>4
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39178108230275083)
,p_query_column_id=>5
,p_column_alias=>'C002'
,p_column_display_sequence=>5
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39178512868275083)
,p_query_column_id=>6
,p_column_alias=>'C003'
,p_column_display_sequence=>6
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39178883920275083)
,p_query_column_id=>7
,p_column_alias=>'C004'
,p_column_display_sequence=>7
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39179308983275083)
,p_query_column_id=>8
,p_column_alias=>'C005'
,p_column_display_sequence=>8
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39179669377275084)
,p_query_column_id=>9
,p_column_alias=>'C006'
,p_column_display_sequence=>9
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39180096741275084)
,p_query_column_id=>10
,p_column_alias=>'C007'
,p_column_display_sequence=>10
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39180452795275084)
,p_query_column_id=>11
,p_column_alias=>'C008'
,p_column_display_sequence=>11
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(39180863405275085)
,p_query_column_id=>12
,p_column_alias=>'C012'
,p_column_display_sequence=>12
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39037350095274974)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_button_name=>'CARGARNUEVACANTIDAD'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CargarNuevaCantidad'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39037726880274974)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_button_name=>'ELIMINAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39038056358274974)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39145266551275056)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_imp.id(44404652590592001)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'GRABAR NOTA DE CREDITO'
,p_button_position=>'BOTTOM'
,p_button_condition=>'CALCULAR'
,p_button_condition_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38960118056274877)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_button_name=>'CALCULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'** CALCULAR NOTA DE CREDITO **'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38960452339274885)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_button_name=>'ACTUALIZAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Actualizar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(39185411731275123)
,p_branch_action=>'f?p=&APP_ID.:153:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(39145266551275056)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(39185839993275127)
,p_branch_action=>'f?p=&APP_ID.:153:&SESSION.:CALCULAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(37662654421082113)
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 11-FEB-2012 17:48 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38960884921274885)
,p_name=>'P153_VALOR_CASTIGO'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_prompt=>'Valor a &P153_DESCRIPCION_CASTIGO.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*:P153_TOT_PAR = 0 and  :f_seg_id = pq_constantes.fn_retorna_constante(null,''cn_tse_id_minoreo'')*/',
'',
':P153_PCO_CASTIGO_INGRESABLE=''S''',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38961257824274900)
,p_name=>'P153_CASTIGO_RECOMENDADO'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Castigo Recomendado'
,p_source=>'return pq_ven_nota_credito.fn_retorna_valor_sugerido(:f_emp_id,:P153_COM_ID_FACTURA,:P153_CXC_ID,:P0_ERROR);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:16px; color: green;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id=2'
,p_display_when2=>'0'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38961685320274903)
,p_name=>'P153_X'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(44359777537591961)
,p_item_default=>':P153_TVE_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'X'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38962371375274904)
,p_name=>'P153_TOTAL_FACTURA'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(44360959205591965)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Factura'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round(sum(v.vcd_valor_variable), 2)',
'  from ven_var_comprobantes_det v, ven_comprobantes_det d',
' where v.cde_id = d.cde_id',
'   and d.com_id = :P153_COM_ID_FACTURA',
'   and var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total'')',
'   and d.ite_sku_id not in',
'       (select ite_sku_padre',
'          from inv_relaciones_item d',
'         where d.tre_id =',
'               pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                  ''cn_tre_id_combo''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38962842843274905)
,p_name=>'P153_TOTAL_FACTURA_FIN'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(44360959205591965)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Factura Fin'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round(sum(v.vcd_valor_variable), 2)',
'  from ven_var_comprobantes_det v, ven_comprobantes_det d',
' where v.cde_id = d.cde_id',
'   and d.com_id = :P153_COM_ID_FACTURA',
'   and var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total_cred_entrada'')',
'   and d.ite_sku_id not in',
'       (select ite_sku_padre',
'          from inv_relaciones_item d',
'         where d.tre_id =',
'               pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                  ''cn_tre_id_combo''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38963215290274905)
,p_name=>'P153_TOTAL_ABONADO'
,p_item_sequence=>269
,p_item_plug_id=>wwv_flow_imp.id(44360959205591965)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Abonado'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (SUM(b.div_valor_cuota) - SUM(div_saldo_cuota)) abonado',
'          FROM car_cuentas_por_cobrar a, car_dividendos b',
'         WHERE b.cxc_id = a.cxc_id',
'           AND a.com_id = :p153_com_id_factura',
'           AND a.cxc_estado = ''GEN'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round((select round(sum(v.vcd_valor_variable), 2)',
'                from ven_var_comprobantes_det v, ven_comprobantes_det d',
'               where v.cde_id = d.cde_id',
'                 and d.com_id = :P153_COM_ID_FACTURA',
'                 and var_id =',
'                     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                        ''cn_var_id_total_credito'')',
'                 and d.ite_sku_id not in',
'                     (select ite_sku_padre',
'                        from inv_relaciones_item d',
'                       where d.tre_id =',
'                             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                ''cn_tre_id_combo''))) -',
'             (select sum(cxc_saldo)',
'                from car_cuentas_por_cobrar',
'               where com_id = :P153_COM_ID_FACTURA),',
'             2) val',
'  from dual'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38963633393274906)
,p_name=>'P153_SALDO_CARTERA'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(44360959205591965)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Cartera'
,p_source=>'select sum(round(cxc_saldo,2)) from car_cuentas_por_cobrar where com_id = :P153_COM_ID_FACTURA AND cxc_estado = ''GEN'''
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38964021669274906)
,p_name=>'P153_DESCRIPCION_CASTIGO'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_imp.id(44360959205591965)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38976734468274921)
,p_name=>'P153_UGE_NUM_EST_SRI'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Establecimiento:'
,p_source=>':P0_UGE_NUM_EST_SRI'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38977051302274922)
,p_name=>'P153_PUE_NUM_SRI'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Punto de Emisi&oacute;n:'
,p_source=>':P0_PUE_NUM_SRI'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38977536105274923)
,p_name=>'P153_COM_NUM'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Secuencia NC:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P153_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38977933429274923)
,p_name=>'P153_FAC_NUMERO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'# Factura:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge_num_sri||'' - ''||pue_num_sri||'' - ''||com_numero',
'FROM VEN_COMPROBANTES WHERE COM_ID = nvl(:P153_COM_ID_FACTURA,0)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38978333983274924)
,p_name=>'P153_COM_ID_FACTURA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'#Com Ref SIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38978712530274924)
,p_name=>'P153_FACTURA_ORACLE'
,p_item_sequence=>22
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Fac Oracle'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38979121634274924)
,p_name=>'P153_COM_NUMERO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Com Numero Factura'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge_num_sri||'' - ''||pue_num_sri||'' - ''||com_numero',
'FROM VEN_COMPROBANTES WHERE COM_ID = nvl(:P153_COM_ID_FACTURA,0)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38979513846274924)
,p_name=>'P153_CXC_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38979892487274925)
,p_name=>'P153_CLI_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38980343925274925)
,p_name=>'P153_PER_NRO_IDENTIFICACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'# Identificaci&oacute;n:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38980708134274925)
,p_name=>'P153_NOMBRE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cliente:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.nombre_cliente',
'  from v_asdm_clientes_facturar c',
' where c.cli_id = :P153_CLI_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>50
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38981098102274926)
,p_name=>'P153_POL_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Pol Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38981463644274926)
,p_name=>'P153_POLITICA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Politica:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.POL_DESCRIPCION_LARGA',
'FROM PPR_POLITICAS A',
'WHERE POL_ID = NVL(:P153_POL_ID,0)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38981871479274926)
,p_name=>'P153_TVE_ID'
,p_item_sequence=>95
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tve Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT var.vco_valor_variable',
'  FROM ven_var_comprobantes var',
' WHERE var.com_id = :P153_COM_ID_FACTURA',
'   AND var.var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_tve'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38982264281274926)
,p_name=>'P153_FORMA_VENTA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Forma Venta:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT t.tve_descripcion',
'  FROM ven_terminos_venta t',
' WHERE t.emp_id = :f_emp_id',
'   AND t.tve_id = :P153_TVE_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38982729435274927)
,p_name=>'P153_PLAZO'
,p_item_sequence=>104
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Plazo:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38983109085274927)
,p_name=>'P153_COM_ID_NCR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Ncr Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38983491816274927)
,p_name=>'P153_TRX_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38983906434274927)
,p_name=>'P153_MNC_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Motivo nota de cr\00C3\00A9dito:')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_MNC_ID'
,p_lov=>'SELECT  MO.MNC_DESCRIPCION, MO.MNC_ID FROM ASDM_MOTIVOS_NC MO, VEN_PARAMETRIZACION_COMP PC WHERE MO.MNC_ID=PC.MNC_ID AND PC.PCO_ID = (SELECT PCO_ID FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P33_CIN_ID)'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38984266376274930)
,p_name=>'P153_MCA_OBSERVACION'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>unistr('Observaci\00C3\00B3n:')
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cMaxlength=>200
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38984697846274930)
,p_name=>'P153_MCA_ID'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38985128602274930)
,p_name=>'P153_MCA_ID_ANT_CLI'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38985472337274931)
,p_name=>'P153_CIN_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Cin Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38985903412274931)
,p_name=>'P153_AGE_ID_AGENTE'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Age Id Agente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38986270377274931)
,p_name=>'P153_ORD_ID'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_prompt=>'Ord Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38986745660274931)
,p_name=>'P153_TOT_PAR'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tot Par'
,p_source=>'RETURN PQ_VEN_COMUNES.fn_retorna_nc_partot(pn_com_id =>:P153_COM_ID_FACTURA, pn_emp_id => :f_emp_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38987086657274932)
,p_name=>'P153_PCO_ID'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pco Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PCO_ID FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P153_CIN_ID',
'',
'/*declare',
'ln_pco_id number;',
'begin',
'SELECT PCO_ID into ln_pco_id FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P153_CIN_ID;',
':P153_PCO_ID := ln_pco_id;',
'end;*/'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38987526309274932)
,p_name=>'P153_PCO_CASTIGO_INGRESABLE'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pco Castigo Ingresable'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.pco_castigo_ingresable',
'   ',
'      from ven_parametrizacion_comp f',
'     where f.pco_id = :P153_PCO_ID',
'       and f.emp_id = :f_emp_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38987924967274932)
,p_name=>'P153_PUE_ELECTRONICO'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38988270372274933)
,p_name=>'P153_VALIDA_PAGO_TARJETA'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valida Pago Tarjeta'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_car_tarjetas_credito.fn_valida_pago_con_tarjeta(pn_com_id => :P153_COM_ID_FACTURA,',
'                                                                pn_emp_id => :f_emp_id);'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38988739861274933)
,p_name=>'P153_PAGINA_ORIGEN'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_imp.id(44364448694591968)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39038524336274975)
,p_name=>'P153_SEQ_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39038940572274975)
,p_name=>'P153_ITE_SKU_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39039288895274975)
,p_name=>'P153_IEB_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39039653670274976)
,p_name=>'P153_CEI_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39040060885274976)
,p_name=>'P153_CDE_ID'
,p_item_sequence=>105
,p_item_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39040513774274976)
,p_name=>'P153_CANTIDAD'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(44370260252591976)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(d.vcd_valor_variable,0)',
'  FROM ven_var_comprobantes_det d',
' WHERE d.cde_id = :P153_CDE_ID',
'   AND d.var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_cantidad'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39145667495275056)
,p_name=>'P153_MCA_TOTAL'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(44404652590592001)
,p_prompt=>'Mca Total'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39146083083275057)
,p_name=>'153_33_111_SQL'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(44404652590592001)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Sql'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_tot_nc(:P0_ERROR,:f_emp_id);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39146782503275059)
,p_name=>'P153_CLAVE_AUTORIZACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(44411478424592006)
,p_prompt=>'Calve Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39147221401275059)
,p_name=>'P153_ALERTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(44411478424592006)
,p_item_default=>'Solicitar Clave de Autorizacion a Cartera'
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:12;color:RED;font-family:Arial Narrow"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39147870571275061)
,p_name=>'P153_IDENTIFICACION'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(160440377455821627)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Identificacion'
,p_source=>'P153_PER_NRO_IDENTIFICACION'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;Color:Blue"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39148340760275062)
,p_name=>'P153_COMPROBANTE'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(160440377455821627)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Factura_interna'
,p_source=>'P153_COM_ID_FACTURA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;Color:Blue"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39148676599275062)
,p_name=>'P153_VALOR_ANTICIPO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(160440377455821627)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Anticipo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round((SELECT nvl(sum (d.c012),0)',
'  FROM APEX_COLLECTIONS D',
' WHERE D.collection_name =',
'       asdm_p.pq_constantes.fn_retorna_constante(null, ''cv_coleccionvardet'') and d.c007= asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total_cred_entrada''))-nvl((',
'select ROUND(SUM(cxc_saldo),2) from car_cuentas_por_cobrar where com_id = :p153_com_id_factura AND cxc_estado = ''GEN''),2),0) from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:20;Color:Red"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39149093193275062)
,p_name=>'P153_CLAVE'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(160440377455821627)
,p_prompt=>'Clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;Color:Blue"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(39181386257275105)
,p_validation_name=>'P153_VALOR_CASTIGO'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P153_VALOR_CASTIGO = ''10'';',
'END'))
,p_validation_type=>'PLSQL_ERROR'
,p_error_message=>'EL VALOR NO PUEDE SER MAYOR QUE LA ENTRADA'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(121144064669822974)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(39181842458275110)
,p_validation_name=>'P153_COM_NUMERO'
,p_validation_sequence=>20
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p153_com_num = :P0_FOL_SEC_ACTUAL then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_imp.id(38979121634274924)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39184919721275119)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_compra_protegida'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  LV_ERROR VARCHAR2(4000);',
'BEGIN',
' IF NVL(:P153_PAGINA_ORIGEN,0) = 0 OR NVL(:P153_PAGINA_ORIGEN,0) <> 60 THEN',
'  ASDM_P.PQ_VEN_INTANGIBLES.pr_verifica_efectivizacion_sjs(pn_emp_id => :F_EMP_ID,',
'                                                           pn_com_id   => :P153_COM_ID_FACTURA,',
'                                                           pn_pagina   => :APP_PAGE_ID,',
'                                                           pc_error    => LV_ERROR);',
' END IF;                                                           ',
'END;                '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_type=>'NEVER'
,p_internal_uid=>6931768451510193
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39182101766275110)
,p_process_sequence=>70
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_NOTA_CREDITO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  valida_tarjetas    EXCEPTION;',
'  valida_tarj_dia    EXCEPTION;',
'  valida_tarj_diasig EXCEPTION;',
'  ln_clave         NUMBER;',
'  ln_total_factura NUMBER;',
'  lv_error         LONG;',
'  ld_fecha         DATE;',
'  ln_ttr_id        asdm_transacciones.ttr_id%TYPE;',
'  ln_pol_id        NUMBER;',
'',
'  cn_var_id_total NUMBER := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                      ''cn_var_id_total'');',
'                                                                      ',
' ln_cxc_id car_cuentas_por_cobrar.cxc_id%TYPE;                                                                      ',
'  ln_tipo_identificacion asdm_personas.per_tipo_identificacion%TYPE;',
'  ln_nro_identificacion asdm_personas.per_nro_identificacion%TYPE;',
'  ',
'BEGIN',
'',
unistr('  --- AGREGADO POR JAIME ANDRES ORDO\00D1EZ -- VALIDAR CLAVES DE AUTORIZACION CUAN LA FORMA DE PAGO ES TARJETA DE CREDITO'),
'',
'  BEGIN',
'  ',
'    SELECT round(co.vco_valor_variable, 2)',
'      INTO ln_total_factura',
'      FROM ven_var_comprobantes co',
'     WHERE co.com_id = to_number(:p153_com_id_factura)',
'       AND co.var_id = cn_var_id_total;',
'    SELECT vco.com_fecha, vco.pol_id',
'      INTO ld_fecha, ln_pol_id',
'      FROM ASDM_E.VEN_COMPROBANTES vco',
'     WHERE vco.com_id = to_number(:p153_com_id_factura);',
'  ',
'  ',
' ',
'    IF (trunc(ld_fecha) = trunc(SYSDATE)) AND',
'       (:P153_VALIDA_PAGO_TARJETA > 0) THEN',
'      pq_car_tarjetas_credito.pr_clave_nota_tarje_mismo_dia(pn_cli_id     => to_number(:p153_cli_id),',
'                                                            pn_per_id     => NULL,',
'                                                            pn_emp_id     => :f_emp_id,',
'                                                            pn_valor      => ln_total_factura,',
'                                                            pn_resultado  => ln_clave,',
'                                                            pn_uge_id     => :f_uge_id,',
'                                                            pn_usu_id     => :f_user_id,',
'                                                            pn_tipo_clave => 8,',
'                                                            pn_tipo_uso   => 0,',
'                                                            pv_error      => :p0_error);',
'    ',
'      IF (to_number(ln_clave) = to_number(:p153_clave_autorizacion)) AND',
'         (:P153_VALIDA_PAGO_TARJETA > 0) THEN',
'        NULL;',
'      ELSE',
'        RAISE valida_tarj_dia;',
'      END IF;',
'    ELSIF (trunc(ld_fecha) < trunc(SYSDATE) AND :P153_ES_POL_GOBIERNO = 0) OR',
'          (:P153_VALIDA_PAGO_TARJETA > 0) THEN',
'    ',
'      pq_car_tarjetas_credito.pr_clave_nota_credito_tarje(pn_cli_id     => to_number(:p153_cli_id),',
'                                                          pn_per_id     => NULL,',
'                                                          pn_emp_id     => :f_emp_id,',
'                                                          pn_valor      => ln_total_factura,',
'                                                          pn_resultado  => ln_clave,',
'                                                          pn_uge_id     => :f_uge_id,',
'                                                          pn_usu_id     => :f_user_id,',
'                                                          pn_tipo_clave => 4,',
'                                                          PN_TIPO_USO   => 0,',
'                                                          pv_error      => :p0_error);',
'    ',
'      IF (to_number(ln_clave) = to_number(:p153_clave_autorizacion)) AND',
'         (:P153_VALIDA_PAGO_TARJETA > 0) THEN',
'        NULL;',
'      ELSE',
'        RAISE valida_tarj_diasig;',
'      END IF;',
'    ELSE',
'      IF :P153_ES_POL_GOBIERNO = 0 OR :P153_VALIDA_PAGO_TARJETA > 0 THEN',
'        RAISE valida_tarjetas;',
'      ELSE',
'        NULL;',
'      END IF;',
'    END IF;',
'  ',
'',
'  EXCEPTION',
'    WHEN valida_tarj_dia THEN',
'      ROLLBACK;',
'      lv_error := ''1 N/C - Forma de Pago - Tarjeta de Credito Clave Incorrecta Solicitar Clave al Jefe/a de Agencia o Cartera '' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    ',
'    WHEN valida_tarj_diasig THEN',
'      ROLLBACK;',
'      lv_error := ''2 N/C - Forma de Pago - Tarjeta de Credito Clave Incorrecta Solicitar Clave a Cartera'' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    ',
'    WHEN valida_tarjetas THEN',
'      ROLLBACK;',
'      lv_error := ''3 N/C - Forma de Pago - La fecha de la factura es mayor a la fecha actual, solicite la clave a Cartera! 2'' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    WHEN OTHERS THEN',
'    ',
'      ROLLBACK;',
'      lv_error := ''4 N/C - Forma de Pago - Tarjeta de Credito Clave Incorrecta Solicitar Clave a Cartera 3'' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    ',
'  END;',
'',
'  --- TERMINA JAIME ANDRES   ',
'',
'  IF :pv_error IS NULL THEN',
'  ',
'  /* ASDM_P.PRUEBAS_PAOB(pn_numero  => 99999999999,',
'                    pc_texto1  => NULL,',
'                    pv_texto2  => NULL,',
'                    pn_numero1 => :P153_COM_ID_FACTURA);*/',
'                    ',
'   -- INICIO PAOBERNAL 03/DICIEMBRE/2018',
unistr('   -- LLAMO AL PROCEDIMIENTO <<PR_CANCELA_CUEN_ORD>>  QUE VALIDA SI LA ORDEN DE VENTA TIENE ALGUN ITEM DE INDUCCI\00D3N PARA LIBERARLO '),
'   -- DE LA TABLA <<VEN_ORDENES_CUEN>>    ',
'       ASDM_P.PQ_VEN_ORDENES.PR_CANCELA_CUEN_COM(pn_emp_id => :f_emp_id,                                                 ',
'                                                 PN_COM_ID => :P153_COM_ID_FACTURA,',
'                                                 pv_error  => :p0_error);',
'   -- FIN PAOBERNAL 03/DICIEMBRE/2018',
'    ',
'    ---GRABA TRANSACCION NOTA DE CREDITO DEV',
'    ----FPENAFIEL GRABA NOTA CREDITO DEV 17/12/2011',
'  ',
'    pq_asdm_dml.pr_ins_asdm_transacciones(pn_trx_id              => :p153_trx_id,',
'                                          pn_emp_id              => :f_emp_id,',
'                                          pn_uge_id              => :f_uge_id,',
'                                          pn_usu_id              => :f_user_id,',
'                                          pn_ttr_id              => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                       ''cn_ttr_id_nota_credito''),',
'                                          pd_trx_fecha_ejecucion => to_date(SYSDATE,',
'                                                                            ''DD/MM/YYYY HH24:MI:SS''),',
'                                          pv_trx_estado_registro => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                       ''cv_estado_reg_activo''),',
'                                          pn_uge_id_gasto        => :f_uge_id_gasto,',
'                                          pv_error               => :p0_error);',
'  ',
'    ---GRABA COMPROBANTE NOTA DE CREDITO DEVOLUCION',
'    pq_ven_comprobantes.pr_graba_comprobantes(pn_com_id                => :p153_com_id_ncr,',
'                                              pn_emp_id                => :f_emp_id,',
'                                              pn_trx_id                => :p153_trx_id,',
'                                              pn_pue_id                => :p0_pue_id,',
'                                              pn_pol_id                => :p153_pol_id,',
'                                              pn_cli_id                => :p153_cli_id,',
'                                              pn_dir_id_enviar_factura => NULL,',
'                                              pv_com_tipo              => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                             ''cv_com_tipo_nc''),',
'                                              pd_com_fecha             => to_date(SYSDATE,',
'                                                                                  ''DD/MM/YYYY HH24:MI:SS''),',
'                                              pn_com_numero            => :P153_COM_NUM,',
'                                              pv_uge_num_sri           => :p0_uge_num_est_sri,',
'                                              pv_pue_num_sri           => :p0_pue_num_sri,',
'                                              pn_age_id_agente         => :p153_age_id_agente,',
'                                              pn_age_id_agencia        => :f_age_id_agencia,',
'                                              pn_ord_id                => :p153_ord_id,',
'                                              pv_com_observacion       => :p153_mca_observacion,',
'                                              pn_com_saldo             => NULL,',
'                                              pn_com_plazo             => NULL,',
'                                              pn_ede_id                => NULL,',
'                                              pn_com_nro_impresion     => NULL,',
'                                              pn_ttr_id                => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                             ''cn_ttr_id_nota_credito''),',
'                                              pn_uge_id                => :f_uge_id,',
'                                              pn_uge_id_gasto          => :f_uge_id_gasto,',
'                                              pn_usu_id                => :f_user_id,',
'                                              pn_cin_id                => :p153_cin_id,',
'                                              pn_com_id_ref            => :p153_com_id_factura,',
'                                              pn_mnc_id                => :p153_mnc_id,',
'                                              pn_plazo_cre             => NULL,',
'                                              pn_entrada_cre           => NULL,',
'                                              pn_valor_financiar_cre   => NULL,',
'                                              pn_tasa_cre              => NULL,',
'                                              pn_cuota_cre             => NULL,',
'                                              pn_total_cre             => NULL,',
'                                              pn_termino_venta         => NULL,',
'                                              pn_nro_folio             => :p0_nro_folio,',
'                                              pv_error                 => :p0_error);',
'  ',
'    --IVEGA.',
unistr('    --SE AGREGA PROCEDIMIENTO DE INACTIVACI\00D3N DE ITEMS LEIDOS.'),
'    --08/06/2015 18:39',
'    --IF :p0_error IS NULL THEN',
'    pq_lectores_slm.pr_inactivar_lista(:P153_ORD_ID);',
'    --END IF;',
'  ',
'    pq_ven_comprobantes.pr_imprimir(pn_emp_id    => :f_emp_id,',
'                                    pn_com_id    => :p153_com_id_ncr,',
'                                    pn_ttr_id    => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cn_ttr_id_nota_credito''),',
'                                    pn_age_id    => :f_age_id_agencia,',
'                                    pn_fac_desde => NULL,',
'                                    pn_fac_hasta => NULL,',
'                                    pv_error     => :p0_error);',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'     /*',
'    Andres Calle',
'    25/05/2017',
'    proceso para enviar a la pantalla de refinanciamiento cada vez que haya una nota de credito parcial        ',
' ',
'    */',
'    begin',
'    select cxc_id, c.per_tipo_identificacion, c.per_nro_identificacion',
'    into ln_cxc_id, ln_tipo_identificacion, ln_nro_identificacion',
'    from ven_nc_parciales a, asdm_clientes b, asdm_personas c',
'                       where a.com_id_nc = :p153_com_id_ncr',
'                      and b.cli_id = a.cli_id',
'                      and c.per_id = b.per_id',
'                      and a.cxc_saldo > 0;    ',
'    ',
'    exception when no_data_found then',
'    ln_cxc_id := 0;',
'    end;',
'    if ln_cxc_id > 0 AND :F_SEG_ID = 2 then',
'   /*  pr_url(pn_app_origen => 211,',
'         pn_app_destino => 211,',
'         pn_pag_origen => 33,',
'         pn_pag_destino => 102,',
'         pv_sesion => :SESSION,',
'         pv_request => ''REFINNC'',',
'         pv_parametros => ''P102_TIPO_IDENTIFICACION,P102_IDENTIFICACION,P102_CXC_NC'',',
'         pv_valores => ln_tipo_identificacion||'',''||ln_nro_identificacion||'',''||ln_cxc_id,',
'         pv_error => :p0_error);*/',
'         ',
'          pr_url( ',
'         pn_app_destino => 211,',
'         pn_pag_destino => 102,',
'         pv_request => ''REFINNC'',',
'         pv_parametros => ''P102_TIPO_IDENTIFICACION,P102_IDENTIFICACION,P102_CXC_NC'',',
'         pv_valores => ln_tipo_identificacion||'',''||ln_nro_identificacion||'',''||ln_cxc_id);',
'         ',
'           ',
'         ',
'         ',
'    else    ',
'    ',
'      pq_ven_comprobantes.pr_redirect(pv_desde        => ''NC'',',
'                                      pn_app_id       => NULL,',
'                                      pn_pag_id       => NULL,',
'                                      pn_emp_id       => :f_emp_id,',
'                                      pv_session      => :session,',
'                                      pv_token        => :f_token,',
'                                      pn_user_id      => :f_user_id,',
'                                      pn_rol          => :p0_rol,',
'                                      pv_rol_desc     => :p0_rol_desc,',
'                                      pn_tree_rot     => :p0_tree_root,',
'                                      pn_opcion       => :f_opcion_id,',
'                                      pv_parametro    => :f_parametro,',
'                                      pv_empresa      => :f_empresa,',
'                                      pn_uge_id       => :f_uge_id,',
'                                      pn_uge_id_gasto => :f_uge_id_gasto,',
'                                      pv_ugestion     => :f_ugestion,',
'                                      pv_error        => :p0_error);',
'    end if;                                      ',
'    ',
'    END IF;',
'    pq_ven_comunes.pr_elimina_coleccion(:p0_error);',
'  ',
'  END IF;',
'',
'  --raise_application_error(-20000, ''aqui'');',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(39145266551275056)
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request =''GRABAR''',
''))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>6928950496510184
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39182505222275117)
,p_process_sequence=>80
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_detalle_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  --raise_application_error(-20000,''El sistema de NC no se encuentra disponible'');',
'  pq_ven_nota_credito.pr_carga_nota_credito_simple(:p153_com_id_factura,',
'                                            :f_emp_id,',
'                                            :p0_ttr_id,',
'                                            :p153_cin_id,',
'                                            nvl(:p153_valor_castigo, 0),',
'                                            :p0_error);',
'',
'  BEGIN',
'    IF pq_constantes.fn_retorna_constante(0, ''cn_emp_id_marcimex'') =',
'       :f_emp_id THEN',
'      pr_upd_var_2252_fa(pn_com_id => :p153_com_id_factura,',
'                         pn_emp_id => :f_emp_id);',
'    END IF;',
'  END;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'carga_det',
''))
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>6929353952510191
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39182886482275117)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_RECALCULAR_NC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  valida_tarjetas  EXCEPTION;',
'  ln_clave         NUMBER;',
'  ln_total_factura NUMBER;',
'  lv_error         LONG;',
'  lb_existe        BOOLEAN;',
'  ln_carga_manual  NUMBER;',
'  cn_var_id_total  NUMBER := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                       ''cn_var_id_total'');',
'BEGIN',
unistr('  --- AGREGADO POR JAIME ANDRES ORDO\00D1EZ -- VALIDAR CLAVES DE AUTORIZACION CUAN LA FORMA DE PAGO ES TARJETA DE CREDITO'),
'  ',
'  ',
'   pq_ven_comunes.pr_carga_coleccion_totalizadas(pn_emp_id => :f_emp_id,',
'                                                  pn_pol_id => :P153_POL_ID,',
'                                                  pv_error  => :p0_error);',
'',
'  ',
'   /* pq_ven_nota_credito.pr_calcula_castigo(:p153_valor_castigo,',
'                                           :p153_tve_id,',
'                                           :f_emp_id,',
'                                           :p153_com_id_factura,',
'                                           :p153_cxc_id,',
'                                           :p0_ttr_id,',
'                                           :p0_error);*/',
'  ',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(37662654421082113)
,p_internal_uid=>6929735212510191
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39183349112275118)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ACTUALIZAR SEGURO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'BEGIN',
'UPDATE VEN_COMPROBANTES_DET_REL X',
'SET X.ITE_SKU_ID=3864',
'WHERE X.CDR_ID=244748;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(1587222568402082478)
,p_process_when_type=>'NEVER'
,p_internal_uid=>6930197842510192
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39184141691275118)
,p_process_sequence=>100
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'prueba'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare ',
'',
'lv_query varchar(15000);',
'begin ',
'',
'  lv_query:=  ''create table co_detalle as select * from apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                    ''''cv_colecciondetalle'''')'' ;',
'                                           EXECUTE IMMEDIATE (lv_query);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(37662865996082114)
,p_internal_uid=>6930990421510192
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39183749070275118)
,p_process_sequence=>100
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_tgr_id             asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'  lv_pue_tipo_impresion asdm_puntos_emision.pue_tipo_impresion%TYPE;',
'  ln_politica_gobierno  NUMBER;',
'  ln_cxc_min            NUMBER;',
'  ln_cxc_max            NUMBER;',
'  lv_pasa               VARCHAR2(5) := NULL;',
'BEGIN',
'  :p0_ttr_id := pq_constantes.fn_retorna_constante(NULL,',
'                                                   ''cn_ttr_id_nota_credito'');',
'  ln_tgr_id  := pq_constantes.fn_retorna_constante(NULL,',
'                                                   ''cn_tgr_id_nota_credito'');',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_mov_caja''));',
'  lv_pasa := ''a'';',
'  pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                  :f_pca_id,',
'                                                  ln_tgr_id,',
'                                                  --:p0_ttr_id,',
'                                                  :p0_ttr_descripcion,',
'                                                  :p0_periodo,',
'                                                  :p0_datfolio,',
'                                                  :p0_fol_sec_actual,',
'                                                  :p0_pue_num_sri,',
'                                                  :p0_uge_num_est_sri,',
'                                                  :p0_pue_id,',
'                                                  :p0_ttr_id,',
'                                                  :p0_nro_folio,',
'                                                  :p0_error);',
'',
'  lv_pasa               := ''b'';',
'  :p153_pue_electronico := pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :p0_pue_id);',
'  IF :p153_pue_electronico = ''E'' THEN',
'    :p153_com_num := :p0_fol_sec_actual;',
'  END IF;',
'',
'  lv_pasa := ''c'';',
'  SELECT MIN(cx.cxc_id), MAX(cx.cxc_id)',
'    INTO ln_cxc_min, ln_cxc_max',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.com_id = :p153_com_id_factura;',
'  lv_pasa := ''d'';',
'',
'  IF :p153_pol_id IN',
'     (pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_pol_id_cuota_balon''),',
'      pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                         ''cn_pol_id_cuota_balon_mp'')) THEN',
'    :p153_cxc_id := ln_cxc_min;',
'  ELSE',
'    :p153_cxc_id := ln_cxc_max;',
'  END IF;',
'  lv_pasa := ''e'';',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    raise_application_error(-20000,',
'                            ''Error Nota de Credito. '' || lv_pasa ||',
'                            :p0_error || '' com_id: '' ||',
'                            :p153_com_id_factura || SQLERRM);',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>6930597800510192
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39184452021275118)
,p_process_sequence=>110
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_castigo_o_devolucion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  --etenesaca 22/07/2016 NC Castigo a Mayoreo',
'  IF :f_seg_id = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'') THEN',
'    :p153_descripcion_castigo := ''castigar'';',
'  ELSE',
'    :p153_descripcion_castigo := ''devolver'';',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>6931300751510192
);
null;
wwv_flow_imp.component_end;
end;
/
