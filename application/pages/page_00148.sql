prompt --application/pages/page_00148
begin
--   Manifest
--     PAGE: 00148
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>148
,p_name=>unistr('Reverso Pagos de tesorer\00EDa en caja')
,p_step_title=>unistr('Reverso Pagos de tesorer\00EDa en caja')
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(89649924508503806619)
,p_plug_name=>unistr('Reverso Pagos de tesorer\00EDa en caja')
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.MOV_ID,',
'       A.MDE_ID,',
'       A.MDE_VALOR,',
'       A.UGE_ID,',
'       mov.pro_id,',
'       nvl(pe.per_razon_social,pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'       pe.per_primer_apellido || '' '' || pe.per_segundo_apellido) nombre,',
'       A.MDE_TIPO,',
'       CASE WHEN A.MDE_TIPO=30 THEN',
'         565',
'       WHEN A.MDE_TIPO=31 THEN',
'         565--572',
'         END TTR_ID,',
'       ''Pagar'' pagar',
'  FROM TES_MOVIMIENTOS_DETALLES A,',
'       tes_movimientos          mov,',
'       asdm_proveedores         pr,',
'       asdm_personas            pe',
' WHERE A.MDE_TIPO in (30,31)',
'   AND A.EMP_ID = :F_EMP_ID',
'   AND A.MDE_ESTADO_REGISTRO = 0',
'   AND UGE_ID IS NOT NULL',
'   and mov.mov_id = a.mov_id',
'   and a.uge_id=:f_uge_id',
'   and mov.pro_id = pr.pro_id',
'   and pr.per_id = pe.per_id',
'   and a.mde_pago_caja = ''S''',
'   and A.MDE_AUTORIZA_PAGO=''S'';'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(89649924624615806620)
,p_name=>unistr('Pagos de tesorer\00EDa en caja')
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'JALVAREZ'
,p_internal_uid=>89617671473346041694
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842832464048909259)
,p_db_column_name=>'MOV_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Mov Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842832855259909260)
,p_db_column_name=>'MDE_VALOR'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Valor'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842833282252909260)
,p_db_column_name=>'UGE_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Uge Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842833664505909260)
,p_db_column_name=>'PRO_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Pro Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842834106922909260)
,p_db_column_name=>'NOMBRE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Nombre'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842832124735909259)
,p_db_column_name=>'MDE_ID'
,p_display_order=>25
,p_column_identifier=>'G'
,p_column_label=>'Mde id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842834477711909261)
,p_db_column_name=>'MDE_TIPO'
,p_display_order=>35
,p_column_identifier=>'H'
,p_column_label=>'Mde tipo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842834926388909261)
,p_db_column_name=>'TTR_ID'
,p_display_order=>45
,p_column_identifier=>'I'
,p_column_label=>'Ttr id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44842831708408909258)
,p_db_column_name=>'PAGAR'
,p_display_order=>46
,p_column_identifier=>'F'
,p_column_label=>'Pagar'
,p_column_link=>'f?p=&APP_ID.:146:&SESSION.::&DEBUG.:RP:P146_TTR_ID,P146_VALOR_A_PAGAR,P146_MDE_ID,P146_MDE_TIPO:#TTR_ID#,#MDE_VALOR#,#MDE_ID#,#MDE_TIPO#'
,p_column_linktext=>'#PAGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(89649927161305808781)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'448105821'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'MOV_ID:MDE_VALOR:NOMBRE:PAGAR:'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44842532369725862846)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(89649924508503806619)
,p_button_name=>'Regresar'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:56:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_imp.component_end;
end;
/
