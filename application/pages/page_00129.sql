prompt --application/pages/page_00129
begin
--   Manifest
--     PAGE: 00129
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>129
,p_name=>'SIMULADOR REFINANCIAMIENTO'
,p_step_title=>'SIMULADOR REFINANCIAMIENTO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(497014662992527067)
,p_plug_name=>'Simulador de Refinanciamiento - Datos Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(497017273997527143)
,p_plug_name=>'Cartera Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli_id,',
'       (SELECT cli.nombre_completo',
'          FROM v_asdm_datos_clientes cli',
'         WHERE cli.cli_id = cxc.cli_id',
'           AND cli.emp_id = cxc.emp_id) nombre,',
'       cxc.cxc_id,',
'       cxc.cxc_saldo,',
'       (SELECT ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cxc.ede_id',
'           AND ede.emp_id = cxc.emp_id) cartera,',
'       (SELECT uge.uge_nombre',
'          FROM asdm_unidades_gestion uge',
'         WHERE uge.uge_id = cxc.uge_id',
'           AND uge.emp_id = cxc.emp_id) unidad_gestion,',
'  /*     CASE cxc.ede_id',
'         WHEN',
'          to_number(pq_constantes.fn_retorna_constante(cxc.emp_id,',
'                                                       ''cn_ede_id_por_defecto'')) THEN',
'          ''CARGAR''',
'         WHEN',
'          to_number(pq_constantes.fn_retorna_constante(cxc.emp_id,',
'                                                       ''cn_ede_id_por_defecto_may'')) THEN',
'          ''CARGAR''',
'         ELSE',
'          NULL',
'       END*/ ''CARGAR'' cargar,',
'   CXC.CXC_FECHA_ADICION',
'  FROM car_cuentas_por_cobrar cxc',
' WHERE cxc.com_id IS NOT NULL',
'   AND cxc.cli_id IS NOT NULL',
'   AND cxc.cxc_id_refin IS NULL',
'   AND cxc.cxc_saldo > 0',
'   AND cxc.cli_id = :p129_cli_id',
'   AND NOT EXISTS (SELECT col.c002',
'          FROM apex_collections col',
'         WHERE col.collection_name = ''COL_CREDITOS_REFIN''',
'           AND col.c002 = cxc.cxc_id)',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(497017479631527145)
,p_name=>'cartera cliente'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>464764328361762219
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497017555346527152)
,p_db_column_name=>'CLI_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497017674473527162)
,p_db_column_name=>'NOMBRE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497017776392527163)
,p_db_column_name=>'CXC_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497018259911527164)
,p_db_column_name=>'CXC_FECHA_ADICION'
,p_display_order=>4
,p_column_identifier=>'H'
,p_column_label=>'Fecha Adicion'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_FECHA_ADICION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497017879294527163)
,p_db_column_name=>'CXC_SALDO'
,p_display_order=>5
,p_column_identifier=>'D'
,p_column_label=>'Cxc Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497017967599527163)
,p_db_column_name=>'CARTERA'
,p_display_order=>6
,p_column_identifier=>'E'
,p_column_label=>'Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497018070227527163)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>7
,p_column_identifier=>'F'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(497018153358527164)
,p_db_column_name=>'CARGAR'
,p_display_order=>8
,p_column_identifier=>'G'
,p_column_label=>'Cargar'
,p_column_link=>'f?p=&APP_ID.:129:&SESSION.:CARGAR:&DEBUG.::P129_CXC_ID:#CXC_ID#'
,p_column_linktext=>'#CARGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CARGAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(497018372975527165)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'4647653'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:NOMBRE:CXC_ID:CXC_FECHA_ADICION:CXC_SALDO:CARTERA:UNIDAD_GESTION:CARGAR'
,p_sort_column_1=>'CXC_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(497018975783527171)
,p_name=>'Cartera a Refinanciar'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>25
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001 credito,',
'c002 Nro_Vencimiento,',
'c003 Fecha_Vencimiento,',
'to_number(c004) Valor_Capital,',
'to_number(c005) Valor_Interes,',
'to_number(c006) Valor_Cuota,',
'to_number(c007) Deuda_Presente,',
'to_number(c008) Saldo_Cuota',
'from apex_collections where collection_name = ''COL_CARTERA_REFIN''',
'ORDER BY TO_NUMBER(C002)'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019181807527181)
,p_query_column_id=>1
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>1
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019255192527187)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019374714527187)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019462684527187)
,p_query_column_id=>4
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019579920527187)
,p_query_column_id=>5
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019677080527187)
,p_query_column_id=>6
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019782088527187)
,p_query_column_id=>7
,p_column_alias=>'DEUDA_PRESENTE'
,p_column_display_sequence=>8
,p_column_heading=>'Deuda Presente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497019858426527187)
,p_query_column_id=>8
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(497019982629527187)
,p_name=>'Valores a Refinanciar'
,p_parent_plug_id=>wwv_flow_imp.id(497018975783527171)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT col.seq_id id,',
'       to_number(col.c001) com_id,',
'       to_number(col.c002) cxc_id,',
'       to_number(col.c003) cli_id,',
'       to_number(col.c004) total,',
'       col.c006 uge_id,',
'       col.c007 uge_nombre,',
'       ''ELIMINAR'' eliminar',
'  FROM apex_collections col',
' WHERE col.collection_name = ''COL_CREDITOS_REFIN'''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020176893527190)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020261312527190)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020370992527190)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020466134527190)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020570844527190)
,p_query_column_id=>5
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>7
,p_column_heading=>'Valor Presente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020680422527190)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020758538527193)
,p_query_column_id=>7
,p_column_alias=>'UGE_NOMBRE'
,p_column_display_sequence=>6
,p_column_heading=>'Uge Nombre'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_column_width=>60
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497020881716527194)
,p_query_column_id=>8
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>8
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:129:&SESSION.:ELIMINA:&DEBUG.::P129_SEQ_ID:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(497021169677527194)
,p_plug_name=>'Refinanciar'
,p_parent_plug_id=>wwv_flow_imp.id(497019982629527187)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>51
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(497025357983527210)
,p_name=>unistr('Interes Diferido No Cobrado - Nota de D\00E9bito')
,p_parent_plug_id=>wwv_flow_imp.id(497018975783527171)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>66
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''ND'''', this.value ,''''P129_UGE_ID'''',''''P129_USER_ID'''',''''P129_EMP_ID'''',''''P129_CLI_ID'''',''''R25127418309089251'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'');}'';'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026061188527214)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026161821527214)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026261156527215)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026372588527215)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026463560527215)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026557225527217)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Int. Dif'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026667713527217)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026780782527217)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026866732527217)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497026956480527217)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027076293527217)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027180122527217)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027264067527217)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027353007527217)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027477471527217)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027568271527217)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027669461527217)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027769137527218)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027867257527218)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497027979544527218)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028079420527226)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028181504527226)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028267829527226)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028356958527226)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028468228527226)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028561692527226)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028664110527226)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028780687527226)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028864323527227)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497028961499527227)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029076332527227)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029171273527227)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029277493527227)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029359154527227)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029480872527227)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029568395527227)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029656092527227)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029752898527227)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029865026527227)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497029972446527227)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030069847527227)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030152742527227)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030259073527227)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030358208527227)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030478273527227)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030563580527227)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030670858527227)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030765393527227)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030870093527227)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497030970930527227)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497031061154527227)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497031157825527227)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497031264735527227)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497031356946527227)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497031481640527227)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497025568839527214)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497025660524527214)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497025754996527214)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497025857732527214)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497025967926527214)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(497031757969527228)
,p_name=>unistr('Interes Diferido  - Nota de Cr\00E9dito')
,p_parent_plug_id=>wwv_flow_imp.id(497018975783527171)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>67
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''NC'''', this.value ,''''P129_UGE_ID'''',''''P129_USER_ID'''',''''P129_EMP_ID'''',''''P129_CLI_ID'''',''''R131206729918723367'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_display_when_condition=>'NVL(:P129_INTERESES_NO_PAG,0) > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497031965448527230)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032066390527230)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032161876527231)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032263575527231)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032382673527231)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032481026527231)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INT DIF REFINANCIADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032551428527231)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>9
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032666902527231)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>7
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497032772211527231)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>8
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(497032852412527232)
,p_name=>'prueba ddd'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CXC_ID,',
'               CRE_NRO_VENCIMIENTO,',
'               CRE_FECHA_VENCIMIENTO,',
'               CRE_VALOR_CAPITAL,',
'               CRE_VALOR_INTERES,',
'               CRE_VALOR_COUTA,',
'               CRE_DEUDA,',
'               CRE_SALDO_CUOTA',
'          FROM CAR_CARTERA_REFINANCIAMIENTO',
'         WHERE CXC_ID = :p129_CXC_ID;'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033074789527233)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033176569527233)
,p_query_column_id=>2
,p_column_alias=>'CRE_NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Cre Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033261260527233)
,p_query_column_id=>3
,p_column_alias=>'CRE_FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Cre Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033368911527233)
,p_query_column_id=>4
,p_column_alias=>'CRE_VALOR_CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Cre Valor Capital'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033474916527233)
,p_query_column_id=>5
,p_column_alias=>'CRE_VALOR_INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Cre Valor Interes'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033568707527233)
,p_query_column_id=>6
,p_column_alias=>'CRE_VALOR_COUTA'
,p_column_display_sequence=>6
,p_column_heading=>'Cre Valor Couta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033659608527233)
,p_query_column_id=>7
,p_column_alias=>'CRE_DEUDA'
,p_column_display_sequence=>7
,p_column_heading=>'Cre Deuda'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497033758055527233)
,p_query_column_id=>8
,p_column_alias=>'CRE_SALDO_CUOTA'
,p_column_display_sequence=>8
,p_column_heading=>'Cre Saldo Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(497033871667527234)
,p_name=>'Refinanciamiento'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>71
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'C001 Nro_Vencimiento,',
'c002 Fecha_Vencimiento,',
'to_number(c003) Valor_Capital,',
'to_number(c004) Valor_Interes,',
'to_number(c005) Valor_Cuota',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_display_when_condition=>':P129_EDE_ID_CARTERA=''MAR'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497034057853527234)
,p_query_column_id=>1
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497034164411527234)
,p_query_column_id=>2
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497034255858527234)
,p_query_column_id=>3
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497034355613527234)
,p_query_column_id=>4
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(497034467369527234)
,p_query_column_id=>5
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(497034572328527234)
,p_plug_name=>'Valores Credito'
,p_parent_plug_id=>wwv_flow_imp.id(497033871667527234)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>110
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(497035158412527236)
,p_plug_name=>unistr('Total del cr\00E9dito despu\00E9s de Refinanciar')
,p_parent_plug_id=>wwv_flow_imp.id(497034572328527234)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>120
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(497034763882527235)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(497034572328527234)
,p_button_name=>'CRONOGRAMA'
,p_button_static_id=>'CRONOGRAMA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'CRONOGRAMA DE PAGOS'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:103:&SESSION.:NUEVO:&DEBUG.::P103_CLI_ID,P103_TOTAL_A_FINANCIAR:&P129_CLI_ID.,&P129_TOTAL_FINANCIAR.'
,p_button_condition=>':P129_INTERESES_NO_PAG > 0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(497021368682527199)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_button_name=>'CALCULAR_CRONOGRA'
,p_button_static_id=>'CALCULAR_CRONOGRA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Calcular'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(497035365110527239)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_button_name=>'GENERA'
,p_button_static_id=>'GENERA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Generar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(497014863877527089)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_button_name=>'Crear_Garante'
,p_button_static_id=>'Crear_Garante'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Crear Garante'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=147:44:&SESSION.::&DEBUG.::F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_POPUP,P44_PAGINA,P44_APLICACION,F_EMP_FONDO:&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,N,211,102,&F_EMP_FONDO.'
,p_button_condition_type=>'NEVER'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(497021575171527200)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_button_name=>'Ir_pago_cuota'
,p_button_static_id=>'Ir_pago_cuota'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'>> Ir Pago Cuota'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.:cliente:&DEBUG.::P6_PER_TIPO_IDENTIFICACION,P6_IDENTIFICACION,P6_PAGO_REFINANCIAMIENTO,P6_VALOR_REFINANCIAMIENTO,P6_VALOR_CAPITAL_REFIN,P6_CXC_ID_REF,P6_SALDO_CXC_REF,P6_INTERESES_NO_PAG_RE:&P129_TIPO_IDENTIFICACION.,&P129_IDENTIFICACION.,1,&P129_VALOR_A_PAGAR_CAJA.,&P129_VALOR_CAPITAL.,&P129_CXC_ID.,&P129_SALDO_CXC_ORIGEN.,&P129_INTERESES_NO_PAG.'
,p_button_condition_type=>'NEVER'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(497038667862527278)
,p_branch_action=>'f?p=&APP_ID.:129:&SESSION.:graba:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(497035365110527239)
,p_branch_sequence=>5
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 20-FEB-2013 15:13 by JORDONEZ'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(497038875601527285)
,p_branch_action=>'f?p=&APP_ID.:129:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 12-DEC-2012 08:54 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497015053054527119)
,p_name=>'P129_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497015275867527128)
,p_name=>'P129_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497015473628527129)
,p_name=>'P129_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Telofono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497015658077527130)
,p_name=>'P129_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497015865742527131)
,p_name=>'P129_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>80
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497016071704527131)
,p_name=>'P129_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_CALIF_AA_A_B'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (PE.PER_RAZON_SOCIAL || PE.PER_PRIMER_NOMBRE || '' '' ||',
'          PE.PER_SEGUNDO_NOMBRE || '' '' || PE.PER_PRIMER_APELLIDO || '' '' ||',
'          PE.PER_SEGUNDO_APELLIDO) || '' - Cod: '' || CL.CLI_ID || '' - Id: '' ||',
'          PE.PER_NRO_IDENTIFICACION NOMBRE,',
'          PE.PER_NRO_IDENTIFICACION IDENTIFICACION',
'     FROM ASDM_CLIENTES CL, ASDM_PERSONAS PE, CAR_CALIF_INI_CLIENTES C',
'    WHERE CL.PER_ID = PE.PER_ID',
'      AND CL.EMP_ID = 1',
'      AND CL.CLI_ID=C.CLI_ID',
'      AND C.CALIFICACION IN (''AA'',''A'',''B'')',
'      AND CL.CLI_ESTADO_REGISTRO = 0',
'      AND PE.PER_ESTADO_REGISTRO = 0'))
,p_lov_cascade_parent_items=>'P129_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P129_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497016282796527133)
,p_name=>'P129_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497016468153527134)
,p_name=>'P129_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>80
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497016659373527134)
,p_name=>'P129_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497016878874527135)
,p_name=>'P129_GARANTE'
,p_item_sequence=>186
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>'Garante'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497017060369527136)
,p_name=>'P129_CALIFICACION_VALIDA'
,p_item_sequence=>256
,p_item_plug_id=>wwv_flow_imp.id(497014662992527067)
,p_prompt=>unistr('D\00EDas Mora')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round(SYSDATE-MIN(DIV.DIV_FECHA_VENCIMIENTO),0)',
'        FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_DIVIDENDOS DIV',
'       WHERE CXC.CXC_ID = DIV.CXC_ID',
'         AND CXC.CLI_ID =:P129_CLI_ID',
'         AND CXC.CXC_ID=:P129_CXC_ID',
'         AND DIV.DIV_SALDO_CUOTA != 0',
'AND cxc.cxc_estado!=''TMP'';'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497018566895527170)
,p_name=>'P129_CXC_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(497017273997527143)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497018766122527170)
,p_name=>'P129_EDE_ID_CARTERA'
,p_item_sequence=>216
,p_item_plug_id=>wwv_flow_imp.id(497017273997527143)
,p_prompt=>'Ede Id Cartera'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497020965389527194)
,p_name=>'P129_SEQ_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(497019982629527187)
,p_prompt=>'SEQ ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497021769094527200)
,p_name=>'P129_TOTAL_FINANCIAR'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Refinanciar'
,p_source=>':P129_VALOR_CAPITAL - NVL(:P129_VALOR_ABONADO,0)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497021951567527202)
,p_name=>'P129_TASA'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_item_default=>'RETURN ROUND(pq_car_refin_precancelacion.fn_retorna_tasa_refin(:p0_Error),4);'
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tasa Interes'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-BOTTOM'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497022174847527203)
,p_name=>'P129_PLAZO'
,p_item_sequence=>126
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Plazo Credito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) PLAZO, TO_NUMBER(PEN_VALOR) ID',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 1 AND :P129_PLAZO_APLICA',
' ORDER BY 1;'))
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497022352986527203)
,p_name=>'P129_FECHA_CORTE'
,p_item_sequence=>136
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Fecha Corte'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT C002',
'       ',
'        FROM apex_collections c',
'       WHERE c.collection_name = ''COLL_FECHAS_CORTE'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'READONLY style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497022583558527203)
,p_name=>'P129_VALOR_CAJA'
,p_item_sequence=>176
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Caja'
,p_source=>'return pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p129_cxc_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497022768961527203)
,p_name=>'P129_ALERTA_ABONO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>' '
,p_source=>unistr('''Para realizar el refinanciamiento deber\00EDa cancelar por lo menos: ''')
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:red;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497022958508527204)
,p_name=>'P129_VALOR_CAPITAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Pendiente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497023180988527204)
,p_name=>'P129_VALOR_ABONADO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Diferencia Abono Intereses'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (AB.CAS_ABONO - AB.CAS_INTERESES) < 0 THEN',
'          0',
'         ELSE',
'         ',
'          (AB.CAS_ABONO - AB.CAS_INTERESES)',
'       END ',
'  FROM car_abonos_simulador ab',
' WHERE ab.cxc_id = :p129_cxc_id',
'   AND ab.cas_estado_registro = 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ab.car_abono-ab.car_intereses--(ab.car_diferencia  + ab.car_abonos_capital)',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = 1827941',
'   AND ab.car_estado_registro = 0;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497023370351527205)
,p_name=>'P129_VALOR_A_PAGAR_CAJA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor A Pagar Caja'
,p_source=>'RETURN pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p129_cxc_id)'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497023558738527205)
,p_name=>'P129_SALDO_CXC_ORIGEN'
,p_item_sequence=>196
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Cxc Origen'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_saldo saldo_cxc_origen',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cx.cxc_id = :p129_cxc_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497023758469527205)
,p_name=>'P129_INTERESES_NO_PAG'
,p_item_sequence=>206
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Intereses No Pag'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'               WHEN (SUM(di.div_valor_cuota) - sum(di.div_saldo_cuota)) >=',
'                    sum(di.div_valor_interes) THEN',
'                0',
'               ELSE',
'                sum(di.div_valor_interes) -',
'                (sum(di.div_valor_cuota) - sum(di.div_saldo_cuota))',
'             END INTERESES_NO_PAG',
'  FROM car_dividendos di',
' WHERE di.cxc_id = :p129_cxc_id',
'   AND di.div_estado_dividendo = ''P''',
'   AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'   AND di.div_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497023979195527206)
,p_name=>'P129_PUE_NUM_SRI'
,p_item_sequence=>246
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Pue Num Sri'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497024158823527206)
,p_name=>'P129_PUE_ID'
,p_item_sequence=>226
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Pue Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497024380309527206)
,p_name=>'P129_VALIDA_PUNTO'
,p_item_sequence=>236
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Valida Punto'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497024557601527206)
,p_name=>'P129_MESES_GRACIA'
,p_item_sequence=>266
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Meses Gracia'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_MESES_GRACIA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) ||'' MESES'' D, TO_NUMBER(PEN_VALOR) R',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 0 AND',
'       (SELECT PMG.PMG_MESES_GRACIA D',
'          FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_PARAMETROS_MESES_GRACIA PMG',
'         WHERE CXC.CXC_ID = :P102_CXC_ID',
'           AND CXC.UGE_ID = PMG.UGE_ID',
'           AND PMG.PMG_ESTADO_REGISTRO=0)',
' ORDER BY 1'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497024755020527208)
,p_name=>'P129_PLAZO_APLICA'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Plazo Aplica'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497024955354527209)
,p_name=>'P129_DIFERENCIA'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Valor Abonado'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT AB.CAS_ABONO',
'  FROM car_abonos_simulador ab',
' WHERE ab.cxc_id = :p129_cxc_id',
'   AND ab.cas_estado_registro = 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497025166147527209)
,p_name=>'P129_CAR_CAPITAL'
,p_item_sequence=>276
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Car Capital'
,p_source=>'SELECT A.CAS_CAPITAL FROM CAR_ABONOS_SIMULADOR A WHERE A.CXC_ID = :P129_CXC_ID AND A.CAS_ESTADO_REGISTRO=0;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497031578617527227)
,p_name=>'P129_INT_MORA_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(497025357983527210)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497034952596527235)
,p_name=>'P129_ALERTA_2'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(497034572328527234)
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:20;color:red;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497035582246527240)
,p_name=>'P129_TOTAL_CREDITO_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Credito'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(to_number(c005)) total_credito',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497035773354527240)
,p_name=>'P129_CLAVE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_prompt=>'Clave Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497035980667527240)
,p_name=>'P129_ALERTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_item_default=>'Solicitar Clave de Autorizacion al Departamento de Cartera'
,p_prompt=>'Alerta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:12;color:RED;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497036155380527241)
,p_name=>'P129_EMP_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_prompt=>'emp_id'
,p_source=>'f_emp_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497036379671527241)
,p_name=>'P129_UGE_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_prompt=>'uge_id'
,p_source=>'f_uge_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497036562130527241)
,p_name=>'P129_USER_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(497035158412527236)
,p_prompt=>'user_id'
,p_source=>'f_user_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(497059263084600487)
,p_name=>'P129_ING_ABONO'
,p_item_sequence=>286
,p_item_plug_id=>wwv_flow_imp.id(497021169677527194)
,p_prompt=>'Ingrese valor abono'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497036761405527249)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_cliente_existe varchar2(100);',
'ln_dir_id  asdm_direcciones.dir_id%TYPE;',
'lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'ln_cli_id_referido asdm_clientes.cli_id%TYPE;',
'',
'',
'begin',
':P129_CALIFICACION_VALIDA:=null;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'',
'',
'  IF apex_collection.collection_exists(''COL_CREDITOS_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'  ',
'  END IF;',
'',
'',
'pq_ven_listas2.pr_datos_clientes(',
':f_uge_id,',
':f_emp_id,',
':P129_IDENTIFICACION,',
':P129_NOMBRE,',
':P129_CLI_ID,',
':P129_CORREO,',
':P129_DIRECCION,',
':P129_TIPO_DIRECCION,        ',
':P129_TELEFONO,              ',
':P129_TIPO_TELEFONO,        ',
'lv_cliente_existe,',
'ln_dir_id ,',
'ln_cli_id_referido ,',
'lv_nombre_cli_referido ,',
':P129_TIPO_IDENTIFICACION,',
':P0_ERROR);',
'',
'',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists( pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'  ',
'  END IF;',
'',
'',
':P129_ALERTA_2 := '' '';',
'',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>464783610135762323
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497037352036527271)
,p_process_sequence=>11
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_cro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_age_id_agencia     NUMBER;',
'  ld_fecha_corte        DATE;',
'  a                     NUMBER;',
'  b                     NUMBER;',
'  ln_cuota              NUMBER(15, 2) := 0;',
'  ln_cuota_minima       NUMBER;',
'  ln_plazo_cxc_original NUMBER;',
'  ln_com_id           NUMBER;',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'ln_int_nd_refi number;',
'  lv_estado_reg_activo  VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                          ''cv_estado_reg_activo'');',
'',
'  lv_div_estado_div_pendi VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                            ''cv_div_estado_div_pendiente'');',
'',
'ln_cas_id number;',
'ln_intereses_no_pag number;',
'ln_intereses number;',
'BEGIN',
'',
'select s.cas_intereses into ln_intereses ',
'from car_abonos_simulador s where s.cxc_id=:p129_cxc_id;',
'',
'if :P129_ING_ABONO >= ln_intereses then',
'',
'UPDATE car_abonos_simulador s ',
'SET s.cas_abono=:P129_ING_ABONO, s.cas_diferencia=:P129_ING_ABONO-s.cas_intereses ',
'WHERE s.cxc_id=:p129_cxc_id;',
'',
'',
'',
'   SELECT ab.cas_id,ab.cas_intereses_no_pag',
'        INTO ln_cas_id,ln_intereses_no_pag',
'        FROM car_abonos_simulador ab',
'       WHERE ab.cxc_id = :p129_cxc_id',
'          AND AB.CAS_ESTADO_REGISTRO = 0;',
'',
'',
'if ln_intereses_no_pag=0 then',
'  SELECT nvl(sum(di.div_valor_interes),0) intereses_no_pag',
'into ln_intereses_no_pag',
'  FROM car_dividendos di',
' WHERE di.cxc_id = :p129_cxc_id',
'   AND di.div_estado_dividendo = ''P''',
'   AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'   AND di.div_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'',
'update car_abonos_simulador re set re.cas_intereses_no_pag=ln_intereses_no_pag where re.cas_id=ln_cas_id;',
'',
'end if;',
'',
'',
'  --- obtenemos el numero de dividendos pendientes de pago  ',
'',
'  SELECT COUNT(di.div_id)',
'    INTO ln_plazo_cxc_original',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p129_cxc_id',
'     AND di.div_estado_dividendo = lv_div_estado_div_pendi',
'     AND di.div_estado_registro = lv_estado_reg_activo',
'     AND di.div_saldo_cuota > 0',
'   GROUP BY di.cxc_id;',
'',
'  IF ln_plazo_cxc_original < to_number(:p129_plazo) THEN',
'  ',
'    SELECT age.age_id',
'      INTO ln_age_id_agencia',
'      FROM asdm_agencias age',
'     WHERE age.uge_id = :f_uge_id;',
'  ',
'    ln_cuota := pq_car_refin_precancelacion.fn_valor_cuota(tasa => :p129_tasa,',
'                                                           nper => :p129_plazo,',
'                                                           va   => :p129_total_financiar);',
'  ',
'    IF :f_seg_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'') THEN',
'    ',
'      ln_cuota_minima := to_number(pq_car_general.fn_obtener_parametro(pn_par_id => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                       ''cn_par_id_cuota_minima_minoreo''),',
'                                                                       pn_ede_id => NULL,',
'                                                                       pn_neg_id => NULL,',
'                                                                       pn_emp_id => :f_emp_id));',
'    ',
'      IF ln_cuota < ln_cuota_minima THEN',
'      ',
'        :p129_alerta_2 := ''La Valor de la Cuota NO debe ser menor a '' ||',
'                          ln_cuota_minima || '' dolares'';',
'      ',
'      ELSE',
'      ',
'        :p129_alerta_2 := '' '';',
'      ',
'      END IF;',
'    ',
'    ELSE',
'    ',
'      :p129_alerta_2 := '' '';',
'    ',
'    END IF;',
'  ',
'    pq_car_refin_precancelacion.pr_cronograma_refin(pn_emp_id    => :f_emp_id,',
'                                                    pn_total_ref => :p129_total_financiar,',
'                                                    pn_plazo     => :p129_plazo,',
'                                                    pn_tasa      => :p129_tasa,',
'                                                    pn_age_id    => ln_age_id_agencia,',
'                                                    pn_cli_id    => :p129_cli_id,',
'                                                    pn_tse_id    => :f_seg_id,',
'                                                    pn_meses_gracia => :P129_MESES_GRACIA,',
'                                                    pv_error     => :p0_error);',
'  ',
'',
'----- cargo coleccion de intereses para la nueva nota de CREDITO',
'',
'',
'',
'',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p129_cxc_id;',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = :p129_pue_id;',
'',
'',
'select ',
'SUM(to_number(c004)) Valor_Interes',
'into ln_int_nd_refi',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'  pq_car_refin_precancelacion.PR_CARGA_COLECCION_ND_REF(pn_cli_id        => :p129_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p129_cxc_id,',
'                                                        pn_valor_interes => ln_int_nd_refi,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'',
'  ELSE',
'  ',
'    :p129_alerta_2 := ''El nuevo plazo de refinanciamiento debe ser MAYOR al # de Dividendos Pendientes de Pago'';',
'  ',
'  END IF;',
'',
'else',
'',
' :p0_error := ''El valor abonado tiene que ser mayor o igual al valor pendiente de pago''; ',
'end If;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(42563059819987679)
,p_internal_uid=>464784200766762345
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497037565958527272)
,p_process_sequence=>10
,p_process_point=>'BEFORE_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_genera_refinan'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_cuota             NUMBER;',
'  ln_valor_nota_credito NUMBER;',
'  ln_age_id_agente     NUMBER;',
'  ln_valor_debito number;',
'ln_valor_credito number;',
'LN_VALOR_CAPITAL_CRO NUMBER;',
'LN_CAPITAL_ABONO NUMBER;',
'BEGIN',
'',
'/*IF :P129_VALOR_A_PAGAR_CAJA!=0 THEN',
':p0_error:=''Existe un valor pendiente de pagar en caja'';',
'RAISE_APPLICATION_ERROR(-20000,:p0_error);',
'END IF;*/',
'',
'',
'select ',
'SUM(to_number(c003)) Valor_Capital',
'INTO LN_VALOR_CAPITAL_CRO',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'SELECT (R.CAR_CAPITAL - R.CAR_DIFERENCIA) ',
'INTO LN_CAPITAL_ABONO ',
'FROM car_abonos_refinanciamiento r WHERE r.cxc_id=:p129_cxc_id AND R.CAR_ESTADO_REGISTRO=0;',
'',
'IF LN_VALOR_CAPITAL_CRO !=LN_CAPITAL_ABONO THEN',
':p0_error:=''HAY UNA DIFERENCIA ENTRE EL VALOR CALCULADO DEL CRONOGRAMA: ''|| LN_VALOR_CAPITAL_CRO ||'' Y EL VALOR A REFINANCIAR :''|| LN_CAPITAL_ABONO;',
'RAISE_APPLICATION_ERROR(-20000,:p0_error);',
'END IF;',
'',
'',
'---debito',
'SELECT (a.car_capital-a.car_diferencia )',
'into ln_valor_debito ',
'FROM asdm_e.car_abonos_refinanciamiento a ',
'WHERE a.cxc_id =:p129_cxc_id',
'AND a.car_estado_registro=0;',
'',
'---credito',
'SELECT  (a.car_saldo_cxc_ant - a.car_abono)  -nvl(car_intereses_no_pag,0)',
'into ln_valor_credito ',
'FROM asdm_e.car_abonos_refinanciamiento a ',
'WHERE a.cxc_id =:p129_cxc_id',
'AND a.car_estado_registro=0;',
'	',
'  ',
'if ln_valor_debito=ln_valor_credito  then',
'',
'  ln_cuota := round(pq_car_refin_precancelacion.fn_valor_cuota(tasa => :p129_tasa,',
'                                                         nper => :p129_plazo,',
'                                                         va   => :p129_total_financiar), 4);',
'',
' --- ln_valor_nota_debito := :p129_total_credito_ref - :p129_total_financiar;',
'SELECT c010 ',
'    INTO   ln_valor_nota_credito',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'');',
'',
'  ln_age_id_agente := pq_car_credito_interfaz.fn_retorna_age_conectado(:f_user_id,',
'                                                                       :f_emp_id,',
'                                                                       ''cn_tag_id_vendedor'');',
'',
'',
' --  RAISE_APPLICATION_ERROR(-20000,  ln_valor_nota_debito );',
'',
'  pq_car_refin_precancelacion.pr_genera_refianciamien(pn_emp_id            => :f_emp_id,',
'                                                      pn_uge_id            => :f_uge_id,',
'                                                      pn_usu_id            => :f_user_id,',
'                                                      pn_cli_id            => :p129_cli_id,',
'                                                      pn_tse_id            => :f_seg_id,',
'                                                      pn_uge_id_gasto      => :f_uge_id_gasto,',
'                                                      pn_clave             => :p129_clave,',
'                                                      pn_valor_cuota       => ln_cuota,',
'                                                      pn_cli_id_ga         => :p129_garante,',
'                                                      pn_age_id_agencia    => :f_age_id_agencia,',
'                                                      pn_age_id_agentes    => ln_age_id_agente,',
'                                                      PN_VALOR_NOTA_DEBITO => ln_valor_nota_credito,',
'                                                      pv_pue_num_sri => :P129_PUE_NUM_SRI,',
'                                                      pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'                                                      pn_pue_id => :P129_PUE_ID,',
'                                                      pv_error             => :p0_error);',
'',
'',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COL_REFIN'');',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nd_refinanciamiento''));',
'',
':P129_TOTAL_CREDITO_REF := null;',
'',
'',
'else ',
'',
':p0_error:=''Los valores no coinciden debito:''|| ln_valor_debito ||'' credito: ''|| ln_valor_credito;',
'RAISE_APPLICATION_ERROR(-20000,:p0_error);',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'graba'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Datos Procesados'
,p_internal_uid=>464784414688762346
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497036954119527257)
,p_process_sequence=>11
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  LN_VALOR_ADEUDA NUMBER;',
'',
'  LN_VALIDA_PAGO_CAJA NUMBER;',
'  LN_COM_ID           NUMBER;',
'  LN_PUE_ID           NUMBER;',
'  LN_PCA_ID           NUMBER;',
'',
'  LV_PUE_SRI      VARCHAR2(5);',
'  LV_UGE_SRI      VARCHAR2(5);',
'  LN_INTERESES_NC NUMBER;',
'  LN_EDE_ID       ASDM_ENTIDADES_DESTINOS.EDE_ID%TYPE;',
'  LV_ABREVIATURA  ASDM_ENTIDADES_DESTINOS.EDE_ABREVIATURA%TYPE;',
'  LN_INT_ND_REFI  NUMBER;',
'',
'  LN_INTERESES_NO_PAG NUMBER;',
'  LN_CAS_ID           CAR_ABONOS_SIMULADOR.CAS_ID%TYPE;',
'  LN_SALDO_ANT        CAR_CUENTAS_POR_COBRAR.CXC_SALDO%TYPE;',
'  LN_VALOR_CAPITAL    CAR_ABONOS_SIMULADOR.CAS_CAPITAL%TYPE;',
'  LN_VALOR_INTERES    CAR_ABONOS_SIMULADOR.CAS_INTERESES%TYPE;',
'  LN_CAS_ABONO        NUMBER;',
'  LN_PAR_DIAS_MORA    NUMBER;',
'  LN_UGE_ID           NUMBER;',
'  LV_UGE_NOMBRE       ASDM_UNIDADES_GESTION.UGE_NOMBRE%TYPE;',
'  LN_DIAS_MORA        NUMBER;',
'BEGIN',
'',
'  BEGIN',
'  ',
'    SELECT PMG.PMG_DIAS_MORA',
'      INTO LN_PAR_DIAS_MORA',
'      FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_PARAMETROS_MESES_GRACIA PMG',
'     WHERE CXC.CXC_ID = :P129_CXC_ID',
'       AND CXC.UGE_ID = PMG.UGE_ID',
'       AND PMG.PMG_ESTADO_REGISTRO = 0;',
'  ',
'  EXCEPTION',
'    WHEN NO_DATA_FOUND THEN',
'      SELECT CXC.UGE_ID, UGE.UGE_NOMBRE',
'        INTO LN_UGE_ID, LV_UGE_NOMBRE',
'        FROM CAR_CUENTAS_POR_COBRAR CXC, ASDM_UNIDADES_GESTION UGE',
'       WHERE CXC.UGE_ID = UGE.UGE_ID',
'         AND CXC.CXC_ID = :P129_CXC_ID;',
'    ',
'      RAISE_APPLICATION_ERROR(-20000,',
'                              ''FALTA PARAMETRIZAR LOS DIAS MORA PARA LA AGENCIA: '' ||',
'                              LN_UGE_ID || '' - '' || LV_UGE_NOMBRE);',
'  END;',
'',
'  SELECT MAX((SELECT ROUND(SYSDATE - MIN(DIV.DIV_FECHA_VENCIMIENTO), 0)',
'               FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_DIVIDENDOS DIV',
'              WHERE CXC.CXC_ID = DIV.CXC_ID',
'                AND CXC.CLI_ID = CXC1.CLI_ID',
'                AND CXC.CXC_ID = CXC1.CXC_ID',
'                AND DIV.DIV_SALDO_CUOTA != 0',
'                AND CXC.CXC_ESTADO != ''TMP''))',
'    INTO LN_DIAS_MORA',
'    FROM CAR_CUENTAS_POR_COBRAR CXC1',
'   WHERE CXC1.CLI_ID = :P129_CLI_ID',
'     AND CXC1.CXC_SALDO > 0',
'     AND CXC1.CXC_ESTADO = ''GEN'';',
'',
'  -----INICIA VALIDACION----------------',
'',
'  IF LN_DIAS_MORA < LN_PAR_DIAS_MORA THEN',
'  ',
'   ',
'      DELETE FROM CAR_ABONOS_SIMULADOR AB',
'       WHERE AB.CXC_ID = :P129_CXC_ID',
'         AND AB.CAS_ESTADO_REGISTRO = 0;',
'    ',
' ',
'    :P129_ALERTA_ABONO := NULL;',
'  ',
'    SELECT COM_ID, EDE_ID',
'      INTO LN_COM_ID, LN_EDE_ID',
'      FROM CAR_CUENTAS_POR_COBRAR X',
'     WHERE X.CXC_ID = :P129_CXC_ID;',
'  ',
'    SELECT EDE.EDE_ABREVIATURA',
'      INTO LV_ABREVIATURA',
'      FROM ASDM_ENTIDADES_DESTINOS EDE',
'     WHERE EDE.EDE_ID = LN_EDE_ID;',
'    :P129_EDE_ID_CARTERA := LV_ABREVIATURA;',
'    ----- obtengo los datos del periodo de caja',
'  ',
'    PQ_VEN_MOVIMIENTOS_CAJA.PR_ABRIR_CAJA(PN_USU_ID => :F_USER_ID,',
'                                          PN_UGE_ID => :F_UGE_ID,',
'                                          PN_EMP_ID => :F_EMP_ID,',
'                                          PN_PUE_ID => LN_PUE_ID,',
'                                          PN_PCA_ID => LN_PCA_ID,',
'                                          PV_ERROR  => :P0_ERROR);',
'  ',
'    SELECT PU.PUE_NUM_SRI, UG.UGE_NUM_EST_SRI',
'      INTO LV_PUE_SRI, LV_UGE_SRI',
'      FROM ASDM_PUNTOS_EMISION PU, ASDM_UNIDADES_GESTION UG',
'     WHERE PU.UGE_ID = UG.UGE_ID',
'       AND PU.PUE_ID = LN_PUE_ID;',
'  ',
'    :P129_PUE_ID       := LN_PUE_ID;',
'    :P129_PUE_NUM_SRI  := LV_PUE_SRI;',
'    :P129_VALIDA_PUNTO := PQ_ASDM_DOCS_XML.FN_TIPO_PUNTO(PN_PUE_ID => LN_PUE_ID);',
'  ',
'    ---ROUND((add_months(MAX(di.div_fecha_vencimiento),6)-SYSDATE)/30,0) formula',
'    SELECT ROUND((ADD_MONTHS(MAX(DI.DIV_FECHA_VENCIMIENTO), 11) - SYSDATE) / 30,',
'                 0) ---jalvarez 05-ABR-20116: se pide cambiar que ya no sean 6 sino 12 meses',
'      INTO :P129_PLAZO_APLICA',
'      FROM CAR_DIVIDENDOS DI',
'     WHERE DI.CXC_ID = :P129_CXC_ID',
'       AND DI.DIV_ESTADO_DIVIDENDO = ''P''',
'       AND DI.DIV_ESTADO_REGISTRO = 0',
'       AND DI.DIV_SALDO_CUOTA > 0;',
'  ',
'    SELECT SUM(NVL(DI.DIV_VALOR_INTERES, 0)) INTERESES_NC',
'      INTO LN_INTERESES_NC',
'      FROM CAR_DIVIDENDOS DI',
'     WHERE DI.CXC_ID = :P129_CXC_ID',
'       AND DI.DIV_ESTADO_DIVIDENDO = ''P''',
'       AND TRUNC(DI.DIV_FECHA_VENCIMIENTO, ''mm'') > TRUNC(SYSDATE, ''mm'')',
'       AND DI.DIV_ESTADO_REGISTRO =',
'           PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0, ''cv_estado_reg_activo'');',
'  ',
'    ----- cargo coleccion de intereses diferidos',
'  ',
'    PQ_CAR_REFIN_PRECANCELACION.PR_CARGA_COLECCION_ND_REF(PN_CLI_ID        => :P129_CLI_ID,',
'                                                          PN_COM_ID        => LN_COM_ID,',
'                                                          PN_EMP_ID        => :F_EMP_ID,',
'                                                          PN_CXC_ID        => :P129_CXC_ID,',
'                                                          PN_VALOR_INTERES => NVL(LN_INTERESES_NC,',
'                                                                                  0),',
'                                                          PN_PUE_SRI       => LV_PUE_SRI,',
'                                                          PN_UGE_SRI       => LV_UGE_SRI,',
'                                                          PV_ERROR         => :P0_ERROR);',
'  ',
'    PQ_CAR_REFIN_PRECANCELACION.PR_CARGA_COLL_CRED_REFIN(PN_EMP_ID => :F_EMP_ID,',
'                                                         PN_CLI_ID => :P129_CLI_ID,',
'                                                         PN_CXC_ID => :P129_CXC_ID,',
'                                                         PN_TSE_ID => :F_SEG_ID,',
'                                                         PV_ERROR  => :P0_ERROR);',
'  ',
'    LN_VALIDA_PAGO_CAJA := PQ_CAR_REFIN_PRECANCELACION.PR_VALIDA_CAJA(PN_CXC_ID => :P129_CXC_ID);',
'  ',
'    IF LV_ABREVIATURA != ''MAR'' THEN',
'    ',
'      :P129_ALERTA_ABONO := ''Para realizar el refinanciamiento necesita hacer un cambio de cartera'';',
'    END IF;',
'  ',
'    IF LN_VALIDA_PAGO_CAJA > 0 THEN',
'    ',
'      IF :P129_ALERTA_ABONO IS NULL THEN',
'        :P129_ALERTA_ABONO := :P129_ALERTA_ABONO ||',
'                              '' Para realizar el refinanciamiento debe cancelar: '' ||',
'                              LN_VALIDA_PAGO_CAJA;',
'      ELSE',
'        :P129_ALERTA_ABONO := :P129_ALERTA_ABONO || '' y  debe cancelar: '' ||',
'                              LN_VALIDA_PAGO_CAJA;',
'      END IF;',
'    ELSE',
'    ',
'      :P129_ALERTA_ABONO := '' '';',
'    ',
'    END IF;',
'  ',
'    BEGIN',
'    ',
'      BEGIN',
'      ',
'        SELECT AB.CAS_ID',
'          INTO LN_CAS_ID',
'          FROM CAR_ABONOS_SIMULADOR AB',
'         WHERE AB.CXC_ID = :P129_CXC_ID',
'           AND AB.CAS_ESTADO_REGISTRO = 0;',
'      ',
'      EXCEPTION',
'        WHEN NO_DATA_FOUND THEN',
'          LN_CAS_ID := NULL;',
'      END;',
'    ',
'      IF LN_CAS_ID IS NULL THEN',
'      ',
'        SELECT SUM(TO_NUMBER(C004)) CAPITAL, SUM(TO_NUMBER(C005)) INTERESES',
'          INTO LN_VALOR_CAPITAL, LN_VALOR_INTERES',
'          FROM APEX_COLLECTIONS',
'         WHERE COLLECTION_NAME = ''COL_CREDITOS_REFIN'';',
'      ',
'        ---jalvarez 16/jun/2016 se cambia para obtener los datos de la foto   ',
'        /*',
'        SELECT sum(c.cre_valor_capital), SUM(c.cre_valor_interes) ',
'        into LN_VALOR_CAPITAL,LN_VALOR_INTERES',
'        FROM asdm_p.CAR_CARTERA_REFINANCIAMIENTO c WHERE  c.cxc_id=:P129_CXC_ID;',
'        */',
'      ',
'        SELECT CXC.CXC_SALDO',
'          INTO LN_SALDO_ANT',
'          FROM CAR_CUENTAS_POR_COBRAR CXC',
'         WHERE CXC.CXC_ID = :P129_CXC_ID;',
'      ',
'        --jalvarez 26-may-2016 se cambia la forma de obtener los interes pendientes de pago',
'        SELECT CASE',
'                 WHEN (SUM(DI.DIV_VALOR_CUOTA) - SUM(DI.DIV_SALDO_CUOTA)) >=',
'                      SUM(DI.DIV_VALOR_INTERES) THEN',
'                  0',
'                 ELSE',
'                  SUM(DI.DIV_VALOR_INTERES) -',
'                  (SUM(DI.DIV_VALOR_CUOTA) - SUM(DI.DIV_SALDO_CUOTA))',
'               END INTERESES_NO_PAG',
'          INTO LN_INTERESES_NO_PAG',
'          FROM CAR_DIVIDENDOS DI',
'         WHERE DI.CXC_ID = :P129_CXC_ID',
'           AND DI.DIV_ESTADO_DIVIDENDO = ''P''',
'           AND TRUNC(DI.DIV_FECHA_VENCIMIENTO, ''mm'') > TRUNC(SYSDATE, ''mm'')',
'           AND DI.DIV_ESTADO_REGISTRO =',
'               PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0, ''cv_estado_reg_activo'');',
'      ',
'        PQ_CAR_DML.PR_INS_CAR_ABONOS_SIMUL(PN_CAS_ID               => LN_CAS_ID,',
'                                           PN_EMP_ID               => :F_EMP_ID,',
'                                           PD_CAS_FECHA            => SYSDATE,',
'                                           PN_CAS_CAPITAL          => LN_VALOR_CAPITAL,',
'                                           PN_CAS_INTERESES        => PQ_CAR_REFIN_PRECANCELACION.PR_VALIDA_CAJA(PN_CXC_ID => :P129_CXC_ID),',
'                                           PN_CAS_ABONO            => 0,',
'                                           PN_CAS_DIFERENCIA       => 0,',
'                                           PV_CAS_ESTADO_REGISTRO  => PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                                                                                         ''cv_estado_reg_activo''),',
'                                           PN_CXC_ID               => :P129_CXC_ID,',
'                                           PN_CAS_SALDO_CXC_ANT    => LN_SALDO_ANT,',
'                                           PN_CAS_INTERESES_NO_PAG => NVL(LN_INTERESES_NO_PAG,',
'                                                                          0),',
'                                           PN_CAS_ABONOS_CAPITAL   => NVL(PQ_CAR_REFIN_PRECANCELACION.PR_VALIDA_ABONO_ANT(PN_CXC_ID => :P129_CXC_ID),',
'                                                                          0),',
'                                           PV_ERROR                => :P0_ERROR);',
'      ',
'      ELSE',
'      ',
'        UPDATE CAR_ABONOS_SIMULADOR AR',
'           SET AR.CAS_NUEVO_CAPITAL = LN_VALOR_CAPITAL',
'         WHERE AR.CAS_ID = LN_CAS_ID;',
'      ',
'      END IF;',
'    END;',
'  ',
'  ELSE',
'    RAISE_APPLICATION_ERROR(-20000,',
unistr('                            ''La calificaci\00F3n del cliente esta en el rango C,D,E,E1,E2'');'),
unistr('    :P0_ERROR := ''La calificaci\00F3n del cliente esta en el rango C,D,E,E1,E2'';'),
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>464783802849762331
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'',
'BEGIN',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p102_cxc_id;',
'',
'  ----- obtengo los datos del periodo de caja',
'',
'  pq_ven_movimientos_caja.pr_abrir_caja(pn_usu_id => :f_user_id,',
'                                        pn_uge_id => :f_uge_id,',
'                                        pn_emp_id => :f_emp_id,',
'                                        pn_pue_id => ln_pue_id,',
'                                        pn_pca_id => ln_pca_id,',
'                                        pv_error  => :p0_error);',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = ln_pue_id;',
'',
'  SELECT SUM(di.div_valor_interes) intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'',
'  ----- cargo coleccion de intereses diferidos',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nd_ref(pn_cli_id        => :p102_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p102_cxc_id,',
'                                                        pn_valor_interes => ln_intereses_nc,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'  pq_car_refin_precancelacion.pr_carga_coll_cred_refin(pn_emp_id => :f_emp_id,',
'                                                       pn_cli_id => :p102_cli_id,',
'                                                       pn_cxc_id => :p102_cxc_id,',
'                                                       pn_tse_id => :f_seg_id,',
'                                                       pv_error  => :p0_error);',
'',
'  ln_valida_pago_caja := pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p102_cxc_id);',
'',
'  IF ln_valida_pago_caja > 0 THEN',
'  ',
'    :p102_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_valida_pago_caja;',
'  ',
'  ELSE',
'  ',
'    :p102_alerta_abono := '' '';',
'  ',
'  END IF;',
'',
'END;'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497037163921527271)
,p_process_sequence=>11
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cuenta number;',
'begin',
'',
'select count(*)',
'into ln_cuenta ',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN''',
'and seq_id = :p129_seq_id;',
'if ln_cuenta > 0 then',
' apex_collection.delete_member(p_collection_name => ''COL_CREDITOS_REFIN'',',
'                               p_seq => :p129_seq_id);',
'end if;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'',
'',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'',
'  IF apex_collection.collection_exists( pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_gastos_int_mora'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_gastos_int_mora''));',
'  ',
'  END IF;',
':P129_ALERTA_2 := '' '';',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>464784012651762345
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497037771865527272)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_pruba_impresion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE ',
'',
'e VARCHAR2(3000);',
'',
'begin',
'  -- Call the procedure',
'  pq_car_refin_precancelacion.pr_imprimir(pn_emp_id => 1,',
'                                          pn_com_id => 881752,',
'                                          pn_ttr_id => 386,',
'                                          pn_age_id => 15,',
'                                          pn_fac_desde => NULL,',
'                                          pn_fac_hasta => NULL,',
'                                          pv_error => e);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>464784620595762346
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497038152381527273)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion_id'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'ln_int_nc_refi number;',
'BEGIN',
'',
'IF :P0_ERROR IS NULL THEN',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p129_cxc_id;',
'',
'  ----- obtengo los datos del periodo de caja',
'',
'  pq_ven_movimientos_caja.pr_abrir_caja(pn_usu_id => :f_user_id,',
'                                        pn_uge_id => :f_uge_id,',
'                                        pn_emp_id => :f_emp_id,',
'                                        pn_pue_id => ln_pue_id,',
'                                        pn_pca_id => ln_pca_id,',
'                                        pv_error  => :p0_error);',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = ln_pue_id;',
'',
'  /*SELECT SUM(di.div_valor_interes) intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p129_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');*/',
'---jalvarez 26-may-2016 se cambia la consulta de los intereses cuando hay saldos adicionales',
'',
'SELECT  CASE',
'               WHEN (SUM(di.div_valor_cuota) - sum(di.div_saldo_cuota)) >=',
'                    sum(di.div_valor_interes) THEN',
'                0',
'               ELSE',
'                sum(di.div_valor_interes) -',
'                (sum(di.div_valor_cuota) - sum(di.div_saldo_cuota))',
'             END intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p129_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'',
'',
'  ----- cargo coleccion de intereses diferidos',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nc_ref(pn_cli_id        => :p129_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p129_cxc_id,',
'                                                        pn_valor_interes => nvl(ln_intereses_nc,0),',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
unistr(' ----- cargo coleccion de intereses para la nota de d\00E9bito'),
'select ',
'SUM(to_number(c004)) Valor_Interes',
'into ln_int_nc_refi',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'  pq_car_refin_precancelacion.PR_CARGA_COLECCION_ND_REF(pn_cli_id        => :p129_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p129_cxc_id,',
'                                                        pn_valor_interes => ln_int_nc_refi,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'',
'END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>464785001111762347
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497038354929527275)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINA_COLECCION_DIVIDENDOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  ',
'APEX_COLLECTION.create_or_truncate_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'carga'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>464785203659762349
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(497037953035527273)
,p_process_sequence=>31
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_intereses NUMBER;',
'  ln_abono     NUMBER;',
'  ln_diferencia NUMBER;',
'',
'BEGIN',
'',
'  SELECT ab.cas_abono, ab.cas_intereses',
'    INTO ln_abono, ln_intereses',
'    FROM car_abonos_simulador ab',
'   WHERE ab.cxc_id = :p129_cxc_id',
'     AND ab.cas_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'         ',
'  IF NVL(ln_abono,0) >= ln_intereses THEN',
'    ',
'    :P129_ALERTA_ABONO := NULL;',
'    ',
'  ELSE',
'    ',
'  ln_diferencia := ln_intereses - ln_abono;',
'    if :P129_EDE_ID_CARTERA is null then',
'',
unistr(' :p129_alerta_abono := ''Para realizar el refinanciamiento deber\00EDa cancelar por lo menos: '' ||'),
'                          ln_intereses;    ',
'  ',
'',
'    ELSif :P129_EDE_ID_CARTERA =''MAR'' then',
'',
unistr(' :p129_alerta_abono := ''Para realizar el refinanciamiento deber\00EDa cancelar por lo menos: '' ||'),
'                          ln_intereses;    ',
'  ',
'ELSE',
unistr(' :p129_alerta_abono := ''Para realizar el refinanciamiento deber\00EDa hacer un cambio de cartera y cancelar: '' ||'),
'                          ln_intereses;    ',
'end if;',
'  END IF;',
' ',
'   ',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>464784801765762347
);
wwv_flow_imp.component_end;
end;
/
