prompt --application/pages/page_00084
begin
--   Manifest
--     PAGE: 00084
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>84
,p_name=>unistr('Autenticaci\00BF\00BFn Usuario Reverso Pago')
,p_step_title=>unistr('Autenticaci\00BF\00BFn Usuario Reverso Pago')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'16'
,p_last_updated_by=>'RULLOA'
,p_last_upd_yyyymmddhh24miss=>'20231025112944'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(129486160003064112)
,p_plug_name=>'CLAVE PARA REVERSO DE PAGO DE CUOTA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(129486352940064130)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_button_name=>'INGRESAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Ejecutar Reverso'
,p_button_position=>'BOTTOM'
,p_button_condition=>'nvl(:P84_VALOR,0) >0 /*and :P84_TOTAL_MORA = :P84_INT_MORA*/'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(129505264008164766)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Reverso'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:23:&SESSION.::&DEBUG.:84::'
,p_button_condition=>':P23_TIPO_REVERSO <> pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_traspaso_pago_cuota'')'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(129517472759224140)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar Traspaso'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:79:&SESSION.::&DEBUG.:84::'
,p_button_condition=>':P23_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_traspaso_pago_cuota'')'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(129693853966528681)
,p_branch_action=>'f?p=&APP_ID.:84:&SESSION.:ejecutar:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(129486352940064130)
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 11-JAN-2012 10:55 by YGUAMAN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(129487071128064149)
,p_branch_action=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
,p_branch_condition=>':P0_ERROR is null'
,p_branch_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P84_CLAVE = ''1234'' and :request = ''ejecutar''',
''))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(35968477333186151)
,p_name=>'P84_TTR_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_prompt=>'Ttr Id:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(35974672232241467)
,p_name=>'P84_NRO_IDENTIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_prompt=>'Nro Identificacion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(35974879504243603)
,p_name=>'P84_VALOR'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_prompt=>'Valor a Reversar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36048164153081325)
,p_name=>'P84_TFP_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_prompt=>'Tfp Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(46740178015924181)
,p_name=>'P84_TOTAL_MORA'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_prompt=>'Total Mora'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(46740358407928018)
,p_name=>'P84_INT_MORA'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Int Mora'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select nvl(sum(to_number(c010)),0)',
' FROM apex_collections',
' WHERE collection_name =  pq_constantes.fn_retorna_constante(NULL,''cv_col_folios_nc_gastos'')',
'AND TO_NUMBER(C013) = 266'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(129486561686064131)
,p_name=>'P84_CLAVE'
,p_item_sequence=>500
,p_item_plug_id=>wwv_flow_imp.id(129486160003064112)
,p_prompt=>unistr('Contrase\00F1a: ')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(129519367134241400)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_genera_reverso_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_efectivo       NUMBER := 0;',
'  ln_cheque         NUMBER := 0;',
'  ln_tarjeta        NUMBER := 0;',
'  ln_mora_ingresada NUMBER := 0;',
'  ln_valor_pago number := 0;',
'  ln_valor_pago_seleccionado number := 0;',
'BEGIN',
'',
'',
'',
'execute immediate ''',
'select sum(to_number(c016))',
'from apex_collections',
'where collection_name = ''''CO_PAGO_CUOTA''''''',
'into ln_valor_pago_seleccionado;',
'',
'execute immediate ''SELECT ca.mca_total',
'FROM asdm_movimientos_caja ca',
'WHERE ca.mca_id = :1''',
'INTO ln_valor_pago',
'using :p23_mca_id_pago_cuota;',
'   ',
'   if ln_valor_pago_seleccionado != ln_valor_pago then',
'   ',
'      raise_application_error(-20000, ''Existen Diferencias entre el Valor Total del Pago ''|| ln_valor_pago_seleccionado||'' y el valor Reversar ''||ln_valor_pago);',
'   ',
'   end if;',
'',
'',
'',
'  SELECT SUM(nvl(to_number(c010), 0))',
'    INTO ln_mora_ingresada',
'    FROM apex_collections',
'   WHERE collection_name =',
'         pq_constantes.fn_retorna_constante(NULL, ''cv_col_folios_nc_gastos'')',
'     AND TO_NUMBER(C013) = 266;',
'  IF ln_mora_ingresada != :p84_total_mora THEN',
'',
'    :p0_error := ''DEBE REVERSAR TODAS LAS CUOTAS QUE TENGAN MORA PARA GENERAR LA NOTA DE CREDITO DE LA NOTA DE DEBITO'';',
'    raise_application_error(-20000, :p0_error);',
'',
'  END IF;',
'',
'  pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id         => :f_emp_id,',
'                                                  pn_pca_id         => :P23_PCA_ID,',
'                                                  pn_valor_efectivo => ln_efectivo,',
'                                                  pn_valor_cheque   => ln_cheque,',
'                                                  pn_valor_tarjeta  => ln_tarjeta,',
'                                                  pv_error          => :p0_error);',
'',
'if :f_seg_id = 2 then',
'  IF ln_efectivo < nvl(to_number(:P23_MCA_TOTAL), 0) THEN',
'if :f_uge_id not in(1054, 274, 1238,1601) then',
'    :p0_error := ''NO TIENE DINERO EN EFECTIVO DISPONIBLE EN CAJA PARA PODER REALIZAR EL REVERSO...'';',
'    raise_application_error(-20000, :p0_error);',
'  end if;',
'  END IF;',
' end if;',
' ',
'',
'  pq_car_credito.pr_valida_clave(pn_clave              => nvl(:P84_CLAVE, 0),',
'                                 pn_emp_id             => :F_EMP_ID,',
'                                 pn_ttr_id             => :P84_TTR_ID,',
'                                 pn_tfp_id             => :P84_TFP_ID,',
'                                 pn_nro_identificacion => :P84_NRO_IDENTIFICACION,',
'                                 pn_valor              => :P84_VALOR,',
'                                 pv_error              => :P0_ERROR);',
'if :f_emp_id in(8,9) then',
':p0_error := null;',
'end if;',
'',
'  IF :P0_ERROR IS NULL THEN',
'  ',
'  -- RAISE_APPLICATION_ERROR(-20000, ''AQUI 1'' || :p0_ERROR);',
'    --IF :P84_CLAVE = ''1234'' then',
'    pq_car_credito.pr_reverso_pago(pn_mca_id_movcaja_reversar => :P23_MCA_ID_PAGO_CUOTA,',
'                                   pn_uge_id                  => :F_UGE_ID,',
'                                   pn_uge_id_gasto            => :F_UGE_ID_GASTO,',
'                                   pn_usu_id                  => :F_USER_ID,',
'                                   pn_emp_id                  => :F_EMP_ID,',
'                                   pn_cli_id                  => :P23_CLI_ID,',
'                                   pn_tipo_reverso            => :P23_TIPO_REVERSO,',
'                                   pv_observacion             => :P23_NDE_OBSERVACION,',
'                                   pn_mca_total               => :P23_MCA_TOTAL,',
'                                   pn_pca_id                  => :P23_PCA_ID,',
'                                   pn_tse_id                  => :F_SEG_ID,',
'                                   pn_age_id_agencia          => :F_AGE_ID_AGENCIA,',
'                                   pv_error                   => :P0_error);',
'  ',
'    IF :P0_error IS NULL THEN',
'      pq_ven_comprobantes.pr_redirect(pv_desde        => ''P'',',
'                                      pn_app_id       => NULL,',
'                                      pn_pag_id       => NULL,',
'                                      pn_emp_id       => :f_emp_id,',
'                                      pv_session      => :session,',
'                                      pv_token        => :f_token,',
'                                      pn_user_id      => :f_user_id,',
'                                      pn_rol          => :p0_rol,',
'                                      pv_rol_desc     => :p0_rol_desc,',
'                                      pn_tree_rot     => :p0_tree_root,',
'                                      pn_opcion       => :f_opcion_id,',
'                                      pv_parametro    => :f_parametro,',
'                                      pv_empresa      => :f_empresa,',
'                                      pn_uge_id       => :f_uge_id,',
'                                      pn_uge_id_gasto => :f_uge_id_gasto,',
'                                      pv_ugestion     => :f_ugestion,',
'                                      pv_error        => :p0_error);',
'    ',
'    END IF;',
'    --end if;',
'  ELSE',
' --  RAISE_APPLICATION_ERROR(-20000, ''AQUI 2'' || :p0_ERROR);  ',
'  ',
'    :P0_ERROR := ''La clave Ingresada es incorrecta...'';',
'    raise_application_error(-20000, :p0_error);',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p0_error is null   and :request = ''ejecutar'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>97266215864476474
,p_process_comment=>'and :request = ''ejecutar'''
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(129486777881064142)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_clave'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_credito.pr_valida_clave(pn_clave              => :P84_CLAVE,',
'                               pn_emp_id             => :F_EMP_ID,',
'                               pn_ttr_id             => :P84_TTR_ID,',
'                               pn_tfp_id             => :P84_TFP_ID,',
'                               pn_nro_identificacion => :P84_NRO_IDENTIFICACION,',
'                               pn_valor              => :P84_VALOR,',
'                               pv_error => :P0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''valida'''
,p_process_when_type=>'NEVER'
,p_internal_uid=>97233626611299216
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(36063282695200206)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_valor_a_reversar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'If :P84_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_devolucion_pago_cuota'') then',
'',
'select NVL(SUM(NVL(to_number(c019),0)),0) ',
'into :P84_VALOR',
'from apex_collections a ',
'where a.collection_name = pq_constantes.fn_retorna_constante(0,''cv_coleccion_pago_cuota'');',
'end if;',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>3810131425435280
);
wwv_flow_imp.component_end;
end;
/
