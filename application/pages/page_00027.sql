prompt --application/pages/page_00027
begin
--   Manifest
--     PAGE: 00027
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>27
,p_name=>'Reporte Ordenes de Venta'
,p_step_title=>'Reporte Ordenes de Venta'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(107837768775371386)
,p_name=>'col_orden_venta'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name = ''COL_FACTURAS'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838061565371429)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838179331371457)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838262571371457)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838352535371457)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838455021371457)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838556046371457)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838673019371457)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838767550371457)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838873321371457)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107838971951371458)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839062739371458)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839154031371458)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839262293371458)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839381630371465)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839459775371465)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839582599371465)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839662717371465)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839763170371465)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839867692371465)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107839974683371465)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840081620371465)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840163571371465)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840274624371465)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840354951371470)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840479084371471)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840575279371471)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840656505371471)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840762292371471)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840877749371471)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107840958177371471)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841055370371471)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841174697371471)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841258106371471)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841377358371471)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841469554371471)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841557589371471)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841672729371471)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841751843371471)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841866079371555)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107841964955371555)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842078722371555)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842167579371555)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842281410371555)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842376863371555)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842452594371555)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842561781371555)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842665523371555)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842759760371555)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842883441371555)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107842961301371571)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843058055371573)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843164214371573)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843258378371573)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843377017371573)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843456559371573)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843560061371573)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843677898371573)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843783263371573)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843867348371574)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107843977283371574)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107844068220371574)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107844169013371574)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107844259377371574)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107844357662371574)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107844455971371574)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(107844565569371574)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(180435574855333975)
,p_plug_name=>'DETALLE FACTURAS NO GENERADAS - MOVIL'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>80
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--R2409-07 TMontalvan',
'--R2409-07 ILIMA: SE AGREGA OPCIONES PARA VOLVER A GENERAR ORDEN O CAMBIARLA A COTIZACION. 4/09/2019',
'',
'SELECT REPLACE(mvc_id,'':'',''|'') mvc_id,',
'       mc.cli_id_destino cli_id,',
'       mc.cli_tipo_identificacion tipo_identificacion,',
'       mc.cli_nro_identificacion nro_identificacion,',
'       mc.mvc_tipo,',
'       (SELECT usu.nombre',
'          FROM kseg_e.kseg_usuarios usu',
'         WHERE usu.cedula = mc.cli_nro_identificacion) nombres,',
'       mc.mvc_dias_vigencia dias_vigencia,',
'       mc.mvc_observaciones observaciones,',
'       mc.age_id_agencia,',
'       mc.age_id_agente,',
'       (SELECT t.tve_descripcion',
'          FROM ven_terminos_venta t',
'         WHERE t.tve_id = mc.tve_id) termino_venta,',
'       mc.pol_id politica,',
'       mc.plazo,',
'       (SELECT ts.tse_descripcion',
'          FROM asdm_e.asdm_tipos_segmento ts',
'         WHERE ts.tse_id = mc.tse_id) tipo_segmento,',
'       mc.entrada,',
'       mc.mvc_tipo_venta tipo_venta,',
'       mc.mvc_fecha fecha,',
'       mc.mvc_error_generacion error_generado,',
'       (CASE mc.mvc_entrega_domicilio',
'         WHEN ''S'' THEN',
'          ''Si''',
'         WHEN ''N'' THEN',
'          ''No''',
'       END) entrega_domicilio,',
'       -- INICIA R2409-07 ILIMA',
'       ''VOLVER A GENERAR'' generar, ',
'       CASE',
'         WHEN mc.mvc_tipo = ''OVE'' THEN',
'          ''CAMBIAR A COTIZACION''',
'         ELSE',
'          ''''',
'       END cambiar',
'       -- FIN R2409-07 ILIMA',
'  FROM asdm_e.mov_ven_cotizaciones mc',
' WHERE mc.mvc_estado = ''E''',
'   AND mc.emp_id = :f_emp_id',
'   AND mc.age_id_agencia = :f_age_id_agencia -- R2409-07 ILIMA',
'   AND mc.mvc_tipo = ''OVE''',
'   AND mc.mvc_estado_registro = 0',
'   AND to_date(mc.mvc_fecha, ''dd-mm-yyyy hh24:mi:ss'')  BETWEEN to_date(:P27_FECHA_INI_NOGENERA, ''dd-mm-yyyy hh24:mi:ss'')',
'   AND to_date(:P27_FECHA_FIN_NOGENERA, ''dd-mm-yyyy hh24:mi:ss'')',
'   order by mc.mvc_fecha desc;   ',
'   '))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'TO_DATE(:P27_FECHA_FIN_NOGENERA) - TO_DATE(:P27_FECHA_INI_NOGENERA) <= 30'
,p_plug_display_when_cond2=>'SQL'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(180435690722333976)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'TMONTALVAN'
,p_internal_uid=>148182539452569050
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969469702496977827)
,p_db_column_name=>'MVC_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Mvc id'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969469812686977828)
,p_db_column_name=>'CLI_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cli id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969469932636977829)
,p_db_column_name=>'TIPO_IDENTIFICACION'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Tipo identificacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470039634977830)
,p_db_column_name=>'NRO_IDENTIFICACION'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Nro identificacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470085397977831)
,p_db_column_name=>'MVC_TIPO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Mvc tipo'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470195476977832)
,p_db_column_name=>'NOMBRES'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Nombres'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470260633977833)
,p_db_column_name=>'DIAS_VIGENCIA'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Dias vigencia'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470428780977834)
,p_db_column_name=>'OBSERVACIONES'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Observaciones'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470541858977835)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Age id agencia'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470555486977836)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Age id agente'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470740511977837)
,p_db_column_name=>'TERMINO_VENTA'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Termino venta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470788504977838)
,p_db_column_name=>'POLITICA'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'Politica'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969470902035977839)
,p_db_column_name=>'PLAZO'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'Plazo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471018458977840)
,p_db_column_name=>'TIPO_SEGMENTO'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'Tipo segmento'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471062867977841)
,p_db_column_name=>'ENTRADA'
,p_display_order=>150
,p_column_identifier=>'O'
,p_column_label=>'Entrada'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471208855977842)
,p_db_column_name=>'TIPO_VENTA'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>'Tipo venta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471346827977843)
,p_db_column_name=>'FECHA'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'Fecha'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471412124977844)
,p_db_column_name=>'ERROR_GENERADO'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'Error generado'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471515671977845)
,p_db_column_name=>'ENTREGA_DOMICILIO'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'Entrega domicilio'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471598534977846)
,p_db_column_name=>'GENERAR'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'Generar'
,p_column_link=>'f?p=&APP_ID.:27:&SESSION.:cambia_estado:&DEBUG.:RP:P27_MVC_ID:#MVC_ID#'
,p_column_linktext=>'Volver a generar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101969471732268977847)
,p_db_column_name=>'CAMBIAR'
,p_display_order=>210
,p_column_identifier=>'U'
,p_column_label=>'Cambiar'
,p_column_link=>'f?p=&APP_ID.:27:&SESSION.:cambia_tipo:&DEBUG.:RP::'
,p_column_linktext=>'CAMBIAR A COTIZACION'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(101981628542988490147)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1019493755'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'MVC_ID:NRO_IDENTIFICACION:NOMBRES:DIAS_VIGENCIA:TERMINO_VENTA:POLITICA:PLAZO:ENTRADA:FECHA:ERROR_GENERADO:GENERAR:CAMBIAR:'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(259765254459455744)
,p_plug_name=>'Reporte Ordenes de Venta'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'		/*',
'		Modificacion: 16/05/23',
'		Autor: RALVARADO',
'		Descripcion: Se reemplaza estructura anterior de biometria por nuevas estructuras CAR_ECREDIT_BIO_CAB',
'		Historia: CTD-58',
'		*/',
'',
'SELECT to_number(a.c001) ord_id,',
'       to_number(a.c002) ord_sec_id,',
'       a.c003 ord_tipo,',
'       to_date(a.c004, ''dd/mm/yyyy'') ord_fecha,',
'       to_number(a.c005) cli_id,',
'       a.c006 per_identificacion,',
'       a.c007 cliente,',
'       to_number(a.c008) age_id_agente,',
'       a.c009 agente,',
'       to_number(a.c010) ord_plazo,',
'       to_number(a.c011) cot_id,',
'       to_number(a.c012) pol_id,',
'       a.c013 pol_descripcion,',
'       to_number(a.c014) tve_id,',
'       a.c015 tve_descripcion,',
'       to_number(a.c016) cli_id_destino,',
'       CASE',
'         WHEN to_date(a.c004, ''dd/mm/yyyy'') <',
'              to_date(''01/06/2016'', ''dd/mm/yyyy'') THEN',
'          NULL',
'       ',
'         ELSE',
'          a.c017',
'       END facturar,',
'       a.c018 modificar,',
'       to_number(a.c019) teo_id,',
'       a.c020 Seguro,',
'       a.c021 Estado_Cupo,',
'       (SELECT COUNT(ide.ide_cantidad_despachada)',
'          FROM mov_inv_despachos       ide,',
'               inv_items_identificador ii,',
'               inv_tipos_identificador ti',
'         WHERE ide.ide_ite_sku_id = ii.ite_sku_id',
'           AND ide.ide_ord_id = to_number(a.c001)',
'           AND ii.iid_primario = ''S''',
'           AND ti.tid_id = ii.tid_id',
'           AND ti.tid_unico = ''S''',
'           AND ide.ide_cantidad_despachada = 0) val_id,',
'       ',
'       CASE',
'         WHEN to_number(a.c014) =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tve_id_credito_propio'') AND',
'              (SELECT COUNT(*)',
'                 FROM ven_ordenes orb',
'                WHERE orb.ord_id = to_number(a.c001)',
'                  AND orb.forma_credito IN (''E'')',
'                  AND EXISTS (SELECT NULL',
'                         FROM CAR_ECREDIT_BIO_CAB b',
'                        WHERE b.ebc_identificacion = a.c006',
'                          AND b.ebc_estado =',
'                              pq_constantes.fn_retorna_constante(0,',
'                                                                 ''cv_activo_bio'')',
'                          AND trunc(b.ebc_fecha_verificacion) >=',
'                              trunc(SYSDATE)',
'                        ORDER BY 1 DESC',
'                        FETCH FIRST 1 ROW ONLY)) > 0 THEN',
'          1',
'         WHEN to_number(a.c014) =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tve_id_credito_propio'') AND',
'              (SELECT COUNT(*)',
'                 FROM ven_ordenes orb',
'                WHERE orb.ord_id = to_number(a.c001)',
'                  AND orb.forma_credito IN (''I'', ''P'')) > 0 THEN',
'          0',
'         WHEN to_number(a.c014) =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tve_id_credito_propio'') AND',
'              (SELECT COUNT(*)',
'                 FROM ven_ordenes orb',
'                WHERE orb.ord_id = to_number(a.c001)',
'                  AND orb.forma_credito IN (''M'')) > 0 THEN',
'          1',
'         WHEN to_number(a.c014) !=',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tve_id_credito_propio'') THEN',
'          1',
'         ELSE',
'          0',
'       END dat_bio,',
'       -----------',
'       --else ',
'       --1',
'       --end dat_bio',
'       --Fin FPLAOMEQUE 23/07/2021 ''Valida si tiene correcto el proceso biometrico''',
'       a.c022 venta_payjoy',
'',
'/*(SELECT COUNT(ide.ide_cantidad_despachada)',
' FROM asdm_e.mov_inv_despachos ide,',
'      ASDM_E.MOV_INV_COMPROBANTES_serie CS',
'WHERE ide.ide_ord_id = to_number(a.c001)',
'  and cs.ics_valor_identificador is not null',
'  and ide.ide_id = cs.ide_id',
'  AND ide.ide_cantidad_despachada = 0)val_id --R2394-10 mfidrovo 27/09/2019 */',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COL_FACTURAS'';',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'UNION ALL ',
'select',
'      to_number(a.ord_id) ord_id,',
'      to_number(a.ord_sec_id) ord_sec_id,',
'      a.ord_tipo ord_tipo,',
'      to_date(a.ord_fecha,''dd/mm/yyyy'') ord_fecha,',
'       to_number(c.cli_id) cli_id,',
'       p.per_nro_identificacion per_identificacion,',
'       NVL(p.per_razon_social,',
'           p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'           p.per_primer_apellido || '' '' || p.per_segundo_apellido) cliente,',
'       to_number(a.age_id_agente) age_id_agente,',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(g.usu_id) agente,',
'       to_number(a.ord_plazo) ord_plazo,',
'       to_number(a.cot_id) cot_id,',
'       to_number(a.pol_id) pol_id,',
'       l.pol_descripcion_larga pol_descripcion,',
'       to_number((select distinct (v.vod_valor_variable)',
'          from ven_var_ordenes_det v, ven_ordenes_det d',
'         where v.ode_id = d.ode_id',
'           and d.ord_id = a.ord_id',
'           and v.var_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_tve''))) tve_id,',
'        '' '' tve_descripcion, ',
'       a.cli_id_destino cli_id_destino,',
'       ''Facturar'' facturar,',
'       ''Modificar'' modificar,       ',
'       to_number(a.teo_id)',
'  FROM ven_ordenes a,',
'       asdm_agencias n,',
'       asdm_clientes c,',
'       asdm_personas p,',
'       asdm_agentes g,',
'       ppr_politicas l,',
'       ven_ordenes_det d,',
'       (SELECT t.ite_sku_id',
'          FROM inv_categorias c, inv_items_categorias t',
'         where c.cat_id = t.cat_id',
'           and c.emp_id = :f_emp_id',
'           and c.cat_estado_registro =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_estado_reg_activo'')',
'        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'         START WITH c.cat_id =',
'                    pq_constantes.fn_retorna_constante(c.emp_id,',
'                                                       ''cn_cat_id_svs'')) c',
' WHERE a.ord_id = d.ord_id',
'   and n.age_id = a.age_id_agencia',
'   and c.cli_id = a.cli_id_destino',
'   and p.per_id = c.per_id',
'   and g.age_id = a.age_id_agente',
'   and l.pol_id = a.pol_id',
'   and c.ite_sku_id = d.ite_sku_id',
'   and teo_id IN',
'       (pq_constantes.fn_retorna_constante(NULL,',
'                                           ''cn_teo_id_aprob_financiera''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_teo_id_aprob_comercial''))',
'   and a.age_id_agencia = :F_AGE_ID_AGENCIA',
'   AND a.ord_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND a.ord_tipo =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_ord_tipo_orden_venta'')',
'   AND a.emp_id = :f_emp_id',
'',
''))
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(259765381668455744)
,p_name=>'Reporte Ordenes de Venta'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_calendar=>'N'
,p_show_reset=>'N'
,p_show_help=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_owner=>'MURGILES'
,p_internal_uid=>227512230398690818
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(259766081910455756)
,p_db_column_name=>'AGENTE'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(259783164093651036)
,p_db_column_name=>'FACTURAR'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Facturar'
,p_column_link=>'f?p=&APP_ID.:20:&SESSION.:orden_venta:&DEBUG.:20:P20_ORDEN_VENTA,P20_IDENTIFICACION,P20_POL_ID,P20_TVE_ID,P20_CLI_ID,P20_PLAZO_FACTURA,P20_AGE_ID_AGENTE,P20_ORD_TIPO,P20_TEO_ID,P20_COT_ID,P20_VALIDA_IDENTIFICADOR,P20_ORD_FECHA,P20_VENTA_PAYJOY:#ORD_I'
||'D#,#PER_IDENTIFICACION#,#POL_ID#,#TVE_ID#,#CLI_ID_DESTINO#,#ORD_PLAZO#,#AGE_ID_AGENTE#,#ORD_TIPO#,#TEO_ID#,#COT_ID#,#VAL_ID#,#ORD_FECHA#,#VENTA_PAYJOY#'
,p_column_linktext=>'#FACTURAR#'
,p_column_link_attr=>'style="color: #0269ff;"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'FACTURAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(49557971806598159)
,p_db_column_name=>'ORD_TIPO'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65332271036826138)
,p_db_column_name=>'MODIFICAR'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Modificar'
,p_column_link=>'f?p=242:20:&SESSION.:orden_venta:&DEBUG.::F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_POPUP,P20_ORD_ID,P20_IDENTIFICACION,P20_POL_ID,P20_TVE_ID,P20_CLI_ID,P20_PLAZO_FACTURA,P20_AGE_ID_AGENTE_'
||'CAB,F_PARAMETRO:&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,N,#ORD_ID#,#PER_NRO_IDENTIFICACION#,#POL_ID#,#TVE_ID#,#CLI_ID_DESTINO#,#ORD_PLAZO#,#AGE_ID_AGENTE#,#ORD_TIPO#'
,p_column_linktext=>'#MODIFICAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
,p_static_id=>'MODIFICAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72471276500700616)
,p_db_column_name=>'PER_IDENTIFICACION'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>unistr('Identificaci\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72471382015700625)
,p_db_column_name=>'CLIENTE'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72471464330700625)
,p_db_column_name=>'POL_DESCRIPCION'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Politica'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'POL_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(72471580895700625)
,p_db_column_name=>'TVE_DESCRIPCION'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Termino<br>de venta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TVE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(107851259134472709)
,p_db_column_name=>'ORD_ID'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(107851372478472729)
,p_db_column_name=>'ORD_SEC_ID'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Ord Sec Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(107851462231472729)
,p_db_column_name=>'CLI_ID'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(107851569229472729)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715500363564919819)
,p_db_column_name=>'ORD_FECHA'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'Ord Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715501066726919855)
,p_db_column_name=>'ORD_PLAZO'
,p_display_order=>36
,p_column_identifier=>'AJ'
,p_column_label=>'Ord Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715501177167919855)
,p_db_column_name=>'COT_ID'
,p_display_order=>37
,p_column_identifier=>'AK'
,p_column_label=>'Cot Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COT_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715501267937919855)
,p_db_column_name=>'POL_ID'
,p_display_order=>38
,p_column_identifier=>'AL'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715501352460919856)
,p_db_column_name=>'TVE_ID'
,p_display_order=>39
,p_column_identifier=>'AM'
,p_column_label=>'Tve Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TVE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715501475012919856)
,p_db_column_name=>'CLI_ID_DESTINO'
,p_display_order=>40
,p_column_identifier=>'AN'
,p_column_label=>'Cli Id Destino'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID_DESTINO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(30715501571216919856)
,p_db_column_name=>'TEO_ID'
,p_display_order=>41
,p_column_identifier=>'AO'
,p_column_label=>'Teo Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TEO_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(10627453669533856845)
,p_db_column_name=>'SEGURO'
,p_display_order=>42
,p_column_identifier=>'AP'
,p_column_label=>'Seguro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SEGURO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(91812371756476067)
,p_db_column_name=>'ESTADO_CUPO'
,p_display_order=>43
,p_column_identifier=>'AQ'
,p_column_label=>'Estado Cupo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ESTADO_CUPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45813256488270272)
,p_db_column_name=>'VAL_ID'
,p_display_order=>53
,p_column_identifier=>'AR'
,p_column_label=>'Val id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(1089945200279727632)
,p_db_column_name=>'DAT_BIO'
,p_display_order=>63
,p_column_identifier=>'AS'
,p_column_label=>'Dat bio'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(171514337919362543656)
,p_db_column_name=>'VENTA_PAYJOY'
,p_display_order=>73
,p_column_identifier=>'AT'
,p_column_label=>'Venta payjoy'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(259766467032456111)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292212932919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'ESTADO_CUPO:FACTURAR:ORD_SEC_ID:ORD_TIPO:CLIENTE:AGENTE:POL_DESCRIPCION:ORD_FECHA:ORD_PLAZO:COT_ID:SEGURO::VAL_ID:DAT_BIO:VENTA_PAYJOY'
,p_sort_column_1=>'ORD_SEC_ID'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'ORD_ID'
,p_sort_direction_2=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(192578770140773001)
,p_report_id=>wwv_flow_imp.id(259766467032456111)
,p_name=>unistr('FALTA APROBACI\00D3N FINANCIERA')
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'APXWS_CC_001'
,p_operator=>'='
,p_expr=>'2'
,p_condition_sql=>' (case when ((#APXWS_CC_EXPR#) = to_number(#APXWS_EXPR#)) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# = #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#FFFF99'
);
wwv_flow_imp_page.create_worksheet_computation(
 p_id=>wwv_flow_imp.id(192579213418773002)
,p_report_id=>wwv_flow_imp.id(259766467032456111)
,p_db_column_name=>'APXWS_CC_001'
,p_column_identifier=>'C01'
,p_computation_expr=>'AM * AO'
,p_column_type=>'NUMBER'
,p_column_label=>'APR_FIN'
,p_report_label=>'APR_FIN'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(10627716583103875646)
,p_name=>'SEGUROS PENDIENTES DE EMITIR'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.cli_id,',
'       c.per_nro_identificacion nro_identificacion,',
'       c.nombre_completo nombres,',
'       b.pse_descripcion plan_seguro,',
'       a.pse_valor_mensual valor_mensual,',
'       a.ose_plazo_cobertura plazo,',
'       CASE',
'         WHEN cu.ces_id = 1 THEN',
'          ''EMITIR SEGURO''',
'         ELSE',
'          ''EMITIR SEGURO''--NULL',
'       END emitir,',
'       a.ose_id,',
'       ''CANCELAR ORDEN'' cancelar,',
'       (SELECT esd.ces_descripcion',
'          FROM car_estados_solicitud esd',
'         WHERE esd.ces_id = cu.ces_id) cupo,',
'         ord.ord_sec_id',
'  FROM ven_ordenes_seguros   a,',
'       asdm_plan_seguros     b,',
'       v_asdm_datos_clientes c,',
'       car_cupos_clientes    cu,',
'       ven_ordenes    ord',
' WHERE b.pse_id = a.pse_id',
'   AND c.cli_id = a.cli_id   ',
'   AND a.cse_estado_registro = 0',
'   AND a.ord_id IS NULL',
'   AND cse_id IS NULL',
'   and a.ord_origen = ord.ord_id(+)',
'   AND cu.cli_id(+) = a.cli_id',
'   AND cu.cup_estado_registro(+) = 0',
'   AND cu.cup_tipo_cupo(+) = ''M''',
'   AND a.uge_id = :f_uge_id',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen seguros pendientes de Emitir'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627716854648875649)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>3
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627716974456875650)
,p_query_column_id=>2
,p_column_alias=>'NRO_IDENTIFICACION'
,p_column_display_sequence=>4
,p_column_heading=>'NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627717059553875650)
,p_query_column_id=>3
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>5
,p_column_heading=>'NOMBRES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627717180241875650)
,p_query_column_id=>4
,p_column_alias=>'PLAN_SEGURO'
,p_column_display_sequence=>6
,p_column_heading=>'PLAN_SEGURO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627717253541875650)
,p_query_column_id=>5
,p_column_alias=>'VALOR_MENSUAL'
,p_column_display_sequence=>7
,p_column_heading=>'VALOR_MENSUAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627717373389875650)
,p_query_column_id=>6
,p_column_alias=>'PLAZO'
,p_column_display_sequence=>8
,p_column_heading=>'PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627717460516875650)
,p_query_column_id=>7
,p_column_alias=>'EMITIR'
,p_column_display_sequence=>2
,p_column_heading=>'EMITIR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:121:&SESSION.:NUEVO:&DEBUG.:121:P121_CLI_ID,P121_OSE_ID:#CLI_ID#,#OSE_ID#'
,p_column_linktext=>'#EMITIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10627717552276875650)
,p_query_column_id=>8
,p_column_alias=>'OSE_ID'
,p_column_display_sequence=>9
,p_column_heading=>'OSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268418952111112051)
,p_query_column_id=>9
,p_column_alias=>'CANCELAR'
,p_column_display_sequence=>10
,p_column_heading=>'Cancelar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:27:&SESSION.:CANCELAR:&DEBUG.::P27_OSE_ID:#OSE_ID#'
,p_column_linktext=>'#CANCELAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(227703965213827224)
,p_query_column_id=>10
,p_column_alias=>'CUPO'
,p_column_display_sequence=>1
,p_column_heading=>'Estado Cupo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(150206036320752663666)
,p_query_column_id=>11
,p_column_alias=>'ORD_SEC_ID'
,p_column_display_sequence=>11
,p_column_heading=>'Ord sec id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(30710914478391656518)
,p_name=>'Ordenes de Servicio'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT to_number(a.c001) ord_id,',
'       to_number(a.c002) ord_sec_id,',
'       a.c003 ord_tipo,',
'to_date(a.c004,''dd/mm/yyyy'') ord_fecha,',
'       to_number(a.c005) cli_id,',
'       a.c006 per_identificacion,',
'       a.c007 cliente,',
'       to_number(a.c008) age_id_agente,',
'       a.c009 agente,',
'       to_number(a.c010) ord_plazo,',
'       to_number(a.c011) cot_id,',
'       to_number(a.c012) pol_id,',
'       a.c013 pol_descripcion,',
'       to_number(a.c014) tve_id,',
'       a.c015 tve_descripcion,',
'       to_number(a.c016) cli_id_destino,',
'       ''Facturar'' facturar,',
'       ''Modificar'' modificar,',
'       to_number(a.c019) teo_id',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COL_FACTURAS''',
'UNION ALL ',
'select',
'      to_number(a.ord_id) ord_id,',
'      to_number(a.ord_sec_id) ord_sec_id,',
'      a.ord_tipo ord_tipo,',
'      to_date(a.ord_fecha,''dd/mm/yyyy'') ord_fecha,',
'       to_number(c.cli_id) cli_id,',
'       p.per_nro_identificacion per_identificacion,',
'       NVL(p.per_razon_social,',
'           p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'           p.per_primer_apellido || '' '' || p.per_segundo_apellido) cliente,',
'       to_number(a.age_id_agente) age_id_agente,',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(g.usu_id) agente,',
'       to_number(a.ord_plazo) ord_plazo,',
'       to_number(a.cot_id) cot_id,',
'       to_number(a.pol_id) pol_id,',
'       l.pol_descripcion_larga pol_descripcion,',
'       to_number((select distinct (v.vod_valor_variable)',
'          from ven_var_ordenes_det v, ven_ordenes_det d',
'         where v.ode_id = d.ode_id',
'           and d.ord_id = a.ord_id',
'           and v.var_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_tve''))) tve_id,',
'        '' '' tve_descripcion, ',
'       a.cli_id_destino cli_id_destino,',
'       ''Facturar'' facturar,',
'       ''Modificar'' modificar,       ',
'       to_number(a.teo_id)',
'  FROM ven_ordenes a,',
'       asdm_agencias n,',
'       asdm_clientes c,',
'       asdm_personas p,',
'       asdm_agentes g,',
'       ppr_politicas l,',
'       ven_ordenes_det d,',
'       (SELECT t.ite_sku_id',
'          FROM inv_categorias c, inv_items_categorias t',
'         where c.cat_id = t.cat_id',
'           and c.emp_id = :f_emp_id',
'           and c.cat_estado_registro =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_estado_reg_activo'')',
'        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'         START WITH c.cat_id =',
'                    pq_constantes.fn_retorna_constante(c.emp_id,',
'                                                       ''cn_cat_id_svs'')) c',
' WHERE a.ord_id = d.ord_id',
'   and n.age_id = a.age_id_agencia',
'   and c.cli_id = a.cli_id_destino',
'   and p.per_id = c.per_id',
'   and g.age_id = a.age_id_agente',
'   and l.pol_id = a.pol_id',
'   and c.ite_sku_id = d.ite_sku_id',
'   and teo_id IN',
'       (pq_constantes.fn_retorna_constante(NULL,',
'                                           ''cn_teo_id_aprob_financiera''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_teo_id_aprob_comercial''))',
'   and a.age_id_agencia = 158',
'   AND a.ord_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND a.ord_tipo =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_ord_tipo_orden_venta'')',
'   AND a.emp_id = :f_emp_id',
'',
'',
'',
'}'';'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712294868027735230)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295077120735233)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295165792735233)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295281553735233)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295366626735233)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295454048735233)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295576487735233)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295679700735233)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295761091735233)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295857365735233)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712295976749735233)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296083116735233)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296169185735233)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296273199735236)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296372088735236)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296473478735236)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296567995735236)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296674295735236)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296763164735236)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296879988735236)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712296980514735236)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297072861735236)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297155098735236)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297252156735236)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297362363735237)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297465954735237)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297573241735237)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297675175735237)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297754552735237)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297856710735237)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712297969699735237)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298070792735237)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298168786735237)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298256180735237)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298380034735237)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298476298735237)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298572938735237)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298652460735237)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298755675735237)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298880703735237)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712298967458735237)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299081266735237)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299159146735237)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299251942735237)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299363286735237)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299456949735237)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299583807735237)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299651709735237)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299783136735237)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299855170735237)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712299954136735237)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300055944735237)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300162219735237)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300283625735237)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300354688735237)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300465696735237)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300565206735238)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300664816735238)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300765799735238)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(30712300860862735238)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(101969471777925977848)
,p_plug_name=>'FACTURAS NO GENERADAS - MOVIL'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(101969472081422977851)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(101969471777925977848)
,p_button_name=>'P27_CARGA_NO_GENERADAS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_alignment=>'LEFT-CENTER'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1089945137555727631)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(259765254459455744)
,p_button_name=>'BT_VALID_BIOME'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Validacion Biometrica  /  Aceptacion SMS Cliente'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=242:110:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(268419668705119536)
,p_name=>'P27_OSE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(10627716583103875646)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(101969471855750977849)
,p_name=>'P27_FECHA_INI_NOGENERA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(101969471777925977848)
,p_prompt=>'Fecha Inicio:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(101969472045461977850)
,p_name=>'P27_FECHA_FIN_NOGENERA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(101969471777925977848)
,p_prompt=>'Fecha Fin:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(101969472238715977852)
,p_name=>'P27_MVC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(180435574855333975)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(101969472252867977853)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cambia_estado_orden'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--R2409-07 ILIMA: SE AGREGA OPCIONES PARA VOLVER A GENERAR ORDEN O CAMBIARLA A COTIZACION. 4/09/2019',
'pq_mov_ventas.pr_update_estado_cotizacion(pn_emp_id => :F_EMP_ID,',
'                                  pn_mvc_id => REPLACE(:P27_MVC_ID,''|'','':''),',
'                                  pv_estado => ''C'' ,',
'                                  pv_error  => :P0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cambia_estado'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101937219101598212927
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(101969472364989977854)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cambia_orden_a_cotizacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--R2409-07 ILIMA: SE AGREGA OPCIONES PARA VOLVER A GENERAR ORDEN O CAMBIARLA A COTIZACION. 4/09/2019',
'pq_mov_ventas.pr_update_tipo_cotizacion(pn_emp_id => :F_EMP_ID,',
'                                  pn_mvc_id => REPLACE(:P27_MVC_ID,''|'','':''),',
'                                  pv_tipo => ''COT'',',
'                                  pv_error  => :P0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cambia_tipo'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101937219213720212928
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(101969472546948977855)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if (TO_DATE(:P27_FECHA_FIN_NOGENERA) - TO_DATE(:P27_FECHA_INI_NOGENERA) ) >30 then',
'    raise_application_error (-20000, ''El rango es mayor a 30 dias!'');',
'end if;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(101969472081422977851)
,p_internal_uid=>101937219395679212929
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(107836783881366263)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_coleccion_facturas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN                              ',
'pq_ven_comprobantes.pr_col_facturacion(pn_emp_id => :f_emp_id,',
'                                       pn_tse_id => :f_seg_id,',
'                                       pn_uge_id => :f_uge_id,',
'                                       pv_Error  => :p0_error);                                   ',
'                                     ',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>75583632611601337
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(268420582711128526)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cancelar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_Seguros.pr_cancela_orden(pn_ord_id => null,',
'                 pn_ose_id => :P27_OSE_ID,',
'                 pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CANCELAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>236167431441363600
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1430732580883477166)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'lv_query VARCHAR2(10000);',
'ln_agencia NUMBER(5);',
'cn_ttr_id_orden_venta NUMBER(5);',
'cv_estado_reg_activo VARCHAR2(1);',
'cn_tse_id_mayoreo NUMBER(1);',
'cn_tse_id_minoreo NUMBER(1);',
'cn_teo_apr_com NUMBER(5);',
'cn_teo_apr_fin NUMBER(5);',
'cn_tse_id NUMBER(2);',
'cn_var_id_tve NUMBER(5);',
'cv_ord_tipo_cons VARCHAR2(5);',
'cv_ord_tipo_como VARCHAR2(5);',
'cv_ord_tipo_venta VARCHAR2(5);',
'cv_ord_tipo_ventam VARCHAR2(5);',
'la_nombres           apex_application_global.vc_arr2;',
'la_valores           apex_application_global.vc_arr2;',
'lv_nombres VARCHAR2(500);',
'lv_valores VARCHAR2(500);',
'',
'f_tse_id NUMBER := :f_seg_id;',
'pn_emp_id NUMBER := :f_emp_id;',
'pn_uge_id NUMBER := :f_uge_id;',
'',
'BEGIN',
'  ',
'  EXECUTE IMMEDIATE ''select l.age_id',
'      from asdm_agencias l',
'     where l.uge_id = :pn_uge_id'' INTO ln_agencia USING pn_uge_id;',
'  ',
'  cn_ttr_id_orden_venta:=pq_constantes.fn_retorna_constante(0,''cn_ttr_id_orden_venta'');',
'  cv_estado_reg_activo := pq_constantes.fn_retorna_constante(0,''cv_estado_reg_activo'');',
'  cn_tse_id_mayoreo:= pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'');',
'  cn_tse_id_minoreo:=pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'');',
'  cn_teo_apr_com:=asdm_p.pq_constantes.fn_retorna_constante(0, ''cn_teo_id_aprob_comercial'');',
'  cn_teo_apr_fin:=asdm_p.pq_constantes.fn_retorna_constante(0, ''cn_teo_id_aprob_financiera'');',
'  cn_var_id_tve:=asdm_p.pq_constantes.fn_retorna_constante(pn_emp_id, ''cn_var_id_tve''); ',
'  ',
'    IF f_tse_id = cn_tse_id_mayoreo THEN',
'       cn_tse_id:=cn_tse_id_mayoreo;',
'       cv_ord_tipo_cons:=asdm_p.pq_constantes.fn_retorna_constante(0,''cv_ord_tipo_orden_factura_consig'');',
'       cv_ord_tipo_como:=asdm_p.pq_constantes.fn_retorna_constante(0,''cv_ord_tipo_orden_factura_consig_moto'');    ',
'      lv_query := ''SELECT ord.ord_id,',
'       ord.ord_sec_id,',
'       ord.ord_tipo,',
'       ord.ord_fecha,',
'       vcli.cli_id,',
'       u.per_nro_identificacion per_identificacion,',
'       nvl(u.per_razon_social,',
'           u.per_primer_nombre || '''' '''' || u.per_segundo_nombre || '''' '''' ||',
'           u.per_primer_apellido || '''' '''' || u.per_segundo_apellido) cliente,',
'       ord.age_id_agente,       ',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(age.usu_id) agente,',
'       ord.ord_plazo,',
'       ord.cot_id,',
'       ord.pol_id,',
'       pol.pol_descripcion_larga pol_descripcion,',
'       vor.vor_valor_variable tve_id,',
'       NULL tve_descripcion,',
'       ord.cli_id_destino,       ',
'       ''''Facturar'''' facturar,',
'       ''''Modificar'''' modificar,',
'       ord.teo_id',
'  FROM ven_ordenes                ord,',
'       ven_var_ordenes            vor,',
'       ppr_variables_ttransaccion vtt,',
'       asdm_clientes              vcli,',
'       asdm_agentes               age,',
'       ppr_politicas              pol,',
'       asdm_agencias              agenc,',
'       asdm_personas              u',
' WHERE vor.ord_id = ord.ord_id',
'   AND vtt.var_id = vor.var_id',
'   AND vtt.ttr_id = :cn_ttr_id_orden_venta ',
'   AND vcli.cli_id = ord.cli_id_destino',
'   AND age.age_id = ord.age_id_agente',
'   AND pol.pol_id = ord.pol_id',
'   AND pol.mca_id = vtt.mca_id',
'   AND pol.pol_estado_registro = :cv_estado_reg_activo',
'   AND pol.tse_id = agenc.tse_id',
'   AND pol.emp_id = ord.emp_id',
'   AND ord.age_id_agencia = agenc.age_id',
'   AND ord.ord_estado_registro = :cv_estado_reg_activo',
'   AND u.per_id = vcli.per_id',
'   AND u.emp_id = vcli.emp_id',
'   AND  agenc.tse_id = :cn_tse_id',
'   AND ord.teo_id IN (:cn_teo_apr_com,:cn_teo_apr_fin) --mcaguana 30/04/2015',
'   AND vor.var_id = :cn_var_id_tve       ',
'       AND agenc.uge_id = :pn_uge_id       ',
'       AND (ord.ord_tipo = :cv_ord_tipo_cons  OR ord.ord_tipo =:cv_ord_tipo_como )       ',
'       AND ord.emp_id = :pn_emp_id'';      ',
'   lv_nombres:=''cn_tse_id:cv_ord_tipo_cons:cv_ord_tipo_como'';',
'   lv_valores:= cn_tse_id||'':''||cv_ord_tipo_cons||'':''||cv_ord_tipo_como;   ',
' ELSIF f_tse_id =cn_tse_id_minoreo THEN--2 VMOSCOSO 22042015 LLAMO A LA CONSTANTE',
'     cn_tse_id:=cn_tse_id_minoreo;',
'     cv_ord_tipo_venta :=asdm_p.pq_constantes.fn_retorna_constante(0,''cv_ord_tipo_orden_venta'');',
'     cv_ord_tipo_ventam :=asdm_p.pq_constantes.fn_retorna_constante(0,''cv_ord_tipo_orden_venta_moto'');',
'      lv_query := ''SELECT ord.ord_id,',
'       ord.ord_sec_id,',
'       ord.ord_tipo,',
'       ord.ord_fecha,',
'       vcli.cli_id,',
'       u.per_nro_identificacion per_identificacion,',
'       nvl(u.per_razon_social,',
'           u.per_primer_nombre || '''' '''' || u.per_segundo_nombre || '''' '''' ||',
'           u.per_primer_apellido || '''' '''' || u.per_segundo_apellido) cliente,',
'       ord.age_id_agente,       ',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(age.usu_id) agente,',
'       ord.ord_plazo,',
'       ord.cot_id,',
'       ord.pol_id,',
'       pol.pol_descripcion_larga pol_descripcion,',
'       vor.vor_valor_variable tve_id,',
'       NULL tve_descripcion,',
'       ord.cli_id_destino,       ',
'       ''''Facturar'''' facturar,',
'       ''''Modificar'''' modificar,',
'       ord.teo_id',
'  FROM ven_ordenes                ord,',
'       ven_var_ordenes            vor,',
'       ppr_variables_ttransaccion vtt,',
'       asdm_clientes              vcli,',
'       asdm_agentes               age,',
'       ppr_politicas              pol,',
'       asdm_agencias              agenc,',
'       asdm_personas              u',
' WHERE vor.ord_id = ord.ord_id',
'   AND vtt.var_id = vor.var_id',
'   AND vtt.ttr_id = :cn_ttr_id_orden_venta',
'   AND vcli.cli_id = ord.cli_id_destino',
'   AND age.age_id = ord.age_id_agente',
'   AND pol.pol_id = ord.pol_id',
'   AND pol.mca_id = vtt.mca_id',
'   AND pol.pol_estado_registro = :cv_estado_reg_activo',
'   AND pol.tse_id = agenc.tse_id',
'   AND pol.emp_id = ord.emp_id',
'   AND ord.age_id_agencia = agenc.age_id',
'   AND agenc.age_id = :cn_agencia /*dlopez 12 oct 2015 Se agrega la agencia para mejorar el selec */',
'   AND ord.ord_estado_registro = :cv_estado_reg_activo',
'   AND u.per_id = vcli.per_id',
'   AND u.emp_id = vcli.emp_id',
'   AND  agenc.tse_id = :cn_tse_id',
'   AND ord.teo_id IN  (:cn_teo_apr_com,:cn_teo_apr_fin) --mcaguana 30/04/2015',
'   AND vor.var_id = :cn_var_id_tve        ',
'   AND agenc.uge_id = :pn_uge_id      ',
'   AND (ord.ord_tipo = :cv_ord_tipo_venta OR ord.ord_tipo = :cv_ord_tipo_ventam)       ',
'   AND ord.emp_id = :pn_emp_id''; ',
'   lv_nombres:=''cn_tse_id:cv_ord_tipo_venta:cv_ord_tipo_ventam:cn_agencia'';',
'   lv_valores:= cn_tse_id||'':''||cv_ord_tipo_venta||'':''||cv_ord_tipo_ventam||'':''||ln_agencia;        ',
' END IF;',
'  lv_nombres:= lv_nombres||'':cn_ttr_id_orden_venta:cv_estado_reg_activo:cn_teo_apr_com:cn_teo_apr_fin:cn_var_id_tve:pn_emp_id:pn_uge_id'';',
'  lv_valores:= lv_valores||'':''||cn_ttr_id_orden_venta||'':''||cv_estado_reg_activo||'':''||cn_teo_apr_com||'':''||cn_teo_apr_fin||'':''||cn_var_id_tve||'':''||pn_emp_id||'':''||pn_uge_id;',
'  ',
' la_nombres := apex_util.string_to_table(lv_nombres,'':'');',
' la_valores := apex_util.string_to_table(lv_valores,'':'');',
'    IF apex_collection.collection_exists(p_collection_name => ''COL_FACTURAS'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COL_FACTURAS'');',
'    END IF;',
'   ',
'       apex_collection.create_collection_from_query_b(p_collection_name => ''COL_FACTURAS'',',
'                                                 p_query           => lv_query,',
'                                                 p_names           => la_nombres,',
'                                                 p_values          => la_valores);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>1398479429613712240
);
wwv_flow_imp.component_end;
end;
/
