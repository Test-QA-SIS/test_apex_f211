prompt --application/pages/page_00008
begin
--   Manifest
--     PAGE: 00008
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>8
,p_name=>'Ingreso por Anticipo de Clientes'
,p_step_title=>'Ingreso por Anticipo de Clientes'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" >',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function setFocus(campo){',
'//alert(campo.value);',
'doSubmit(''SUBMIT'');',
'document.getElementById(campo).focus();',
'//document.getElementById(campo).select();',
'',
'}',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(33701761601270369)
,p_plug_name=>'<B>&P0_TTR_DESCRIPCION.   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(33984077035955600)
,p_plug_name=>'Movimientos'
,p_parent_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P0_TTR_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(34418164513883509)
,p_plug_name=>'Datos Cheque'
,p_parent_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>35
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_cheque'')',
' and',
':P8_TTR_ID != pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_egr_anticipo_cli'')'))
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(49193977977025945)
,p_plug_name=>'tfp_deposito'
,p_parent_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'')',
'or :P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(49203977387158235)
,p_plug_name=>'Ingreso Valor'
,p_parent_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>80
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(74319365150750094)
,p_plug_name=>'tfp Datos Tarjeta Credito'
,p_parent_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>75
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P8_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(97178254880989824)
,p_plug_name=>' tfp_retencion'
,p_parent_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>0
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(754771878501603023)
,p_plug_name=>'&P0_TTR_DESCRIPCION.'
,p_parent_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>77
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_retiros_vec_pic'') OR :P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_depositos_vec_pic'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_plug_header=>'<center><img src="#WORKSPACE_IMAGES#pichincha.png"/></center>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(34002261376395741)
,p_name=>'DetalleMovimientoCaja'
,p_parent_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select count(*) ',
'FROM apex_collections',
'WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')) > 0',
'and :P8_TTR_ID is not null'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS_INITCAP'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34009757530441951)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.::P8_SEQ_ID,P8_TFP_ID,P8_EDE_ID,P8_PLAN,P8_MCD_VALOR_MOVIMIENTO,P8_CRE_TITULAR_CUENTA,P8_CRE_NRO_CUENTA,P8_CRE_NRO_CHEQUE,P8_CRE_NRO_PIN,P8_NRO_TARJETA,P8_MCD_NRO_AUT_REF,P8_MCD_VOUCHER,P8_LOTE,P8_MCD_NRO_COMPROBANTE,'
||'P8_RAP_NRO_RETENCION,P8_SALDO_RETENCION,P8_RAP_COM_ID,P8_RAP_FECHA,P8_RAP_FECHA_VALIDEZ,P8_RAP_NRO_AUTORIZACION,P8_MODIFICACION,P8_RAP_NRO_ESTABL_RET,P8_RAP_NRO_PEMISION_RET,P8_PRE_ID,P8_MCD_FECHA_DEPOSITO:#COL03#,#COL04#,#COL05#,#COL05#,#COL30#,#COL'
||'11#,#COL12#,#COL13#,#COL14#,#COL15#,#COL16#,#COL17#,#COL18#,#COL19#,#COL20#,#COL21#,#COL22#,#COL23#,#COL24#,#COL25#,S,#COL26#,#COL27#,#COL28#,#COL29#'
,p_column_linktext=>'#COL01#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_alignment=>'CENTER'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_report_column_width=>2
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34009872300441951)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:8:&SESSION.:eliminar:&DEBUG.::P8_SEQ_ID:#COL03#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34009964064441951)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>4
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010079359441951)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>5
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010159520441951)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>6
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010274588441951)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>7
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010362881441951)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>8
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010455361441951)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>9
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010557944441951)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>10
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010676988441952)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>11
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010775543441952)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>12
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010861566441952)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>13
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34010957879441952)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>14
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011051809441952)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>15
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011180399441952)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>16
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011252038441952)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>3
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011383830441952)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011464674441952)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011552900441952)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011655375441952)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011752019441952)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011853880441952)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34011969990441952)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012081850441952)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012176933441953)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012280085441953)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012378189441953)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012464131441953)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012568294441954)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34012662277441954)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(79340379865489174)
,p_plug_name=>'Datos Clientes'
,p_parent_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_ing_anticipo_cli'') or',
':P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_egr_anticipo_cli'')'))
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(34160773157641658)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(41020358626438483)
,p_name=>'coll mov caja'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections ',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                              ''cv_coleccion_mov_caja'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41020652133438503)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41020756625438512)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41020860055438512)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41020978160438512)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021071214438512)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021179840438512)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021257971438512)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021370357438512)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021475017438512)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021568949438512)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021660686438512)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021770939438512)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021864033438513)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41021966903438518)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022075854438518)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022178376438518)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022273934438518)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022354012438519)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022459353438519)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022557211438519)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022675268438519)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022764970438519)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022857307438519)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41022970217438519)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023082108438519)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023175797438519)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023281431438520)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023371416438520)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023479522438520)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023552884438520)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023656620438520)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023755261438520)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023851457438520)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41023981222438520)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024065066438520)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024161234438520)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024279341438520)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024379236438520)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024478933438520)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024573819438521)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024672111438521)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024765895438521)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024861240438521)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41024970746438521)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025081013438521)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025161533438521)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025252974438521)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025362864438521)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025471739438521)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025563750438521)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025677484438521)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025783638438522)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025874484438522)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41025964597438522)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026070035438522)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026169916438522)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026276030438522)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026358852438522)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026456757438522)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026583488438522)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026682885438522)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026775159438522)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026869989438522)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41026961425438522)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41027080053438522)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41027181525438522)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(33990760207035904)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_imp.id(49203977387158235)
,p_button_name=>'P8_CARGAR'
,p_button_static_id=>'P8_CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_alignment=>'LEFT'
,p_button_condition=>':P8_AUTORIZACION is not null or :P8_TTR_ID != pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_egr_anticipo_cli'')'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_request_source=>'IngresarValor'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(34152359296561976)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(34160773157641658)
,p_button_name=>'CANCELARNUEVO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar / Nuevo'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(33701968546270371)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(34160773157641658)
,p_button_name=>'GENERAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Generar Movimiento'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P8_CLI_ID is not null or :P8_TTR_ID is not null)',
'and ',
'(select count(*) ',
'FROM apex_collections',
'WHERE collection_name = ''CO_MOV_CAJA'') > 0 and :P8_MCA_TOTAL > 0'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(34152671488561980)
,p_branch_action=>'f?p=&APP_ID.:56:&SESSION.::&DEBUG.:8,56::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(34152359296561976)
,p_branch_sequence=>30
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(65313052740224824)
,p_branch_action=>'f?p=&APP_ID.:8:&SESSION.:graba_imprime:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(33701968546270371)
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 16-JUN-2011 10:11 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(33704252084270386)
,p_branch_action=>'f?p=&APP_ID.:12:&SESSION.::&DEBUG.:8:P12_CLI_ID:&P8_CLI_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(33701968546270371)
,p_branch_sequence=>50
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(97437680692561093)
,p_branch_action=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>60
,p_branch_comment=>'Created 10-OCT-2011 19:29 by YGUAMAN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(107985477959987718)
,p_branch_action=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(33990760207035904)
,p_branch_sequence=>70
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 17-NOV-2011 14:32 by YGUAMAN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33702177302270371)
,p_name=>'P8_MCA_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'mca_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33702363803270373)
,p_name=>'P8_TTR_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Tipo Movimiento'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33702555839270373)
,p_name=>'P8_MCA_FECHA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Movimiento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>25
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33702779055270374)
,p_name=>'P8_MCA_OBSERVACION'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(49203977387158235)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>58
,p_cMaxlength=>200
,p_cHeight=>2
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when=>'P8_TTR_ID'
,p_read_only_when2=>'381'
,p_read_only_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33702956610270374)
,p_name=>'P8_MCA_TOTAL'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(49203977387158235)
,p_use_cache_before_default=>'NO'
,p_item_default=>'0'
,p_prompt=>'Valor Total Movimiento'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'       sum (c006) valor',
'FROM   apex_collections',
'WHERE  collection_name  = ''CO_MOV_CAJA'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33703162777270374)
,p_name=>'P8_TRX_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33703383666270374)
,p_name=>'P8_PCA_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pca Id'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33716674763365616)
,p_name=>'P8_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P8_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P8_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33718269138382952)
,p_name=>'P8_TFP_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_item_default=>'1'
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_FORMAS_PAGO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'        lv_lov := pq_ven_listas_caja.fn_lov_formas_pago',
'(',
':F_EMP_ID,',
':P0_TTR_ID,',
':P30_POLITICA_VENTA);',
'',
'return (lv_lov);',
'',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_MCA_TOTAL > 0 and',
':P8_TRX_ID = pq_constantes.fn_retorna_constante(null,''cn_ttr_id_egr_anticipo_cli'')'))
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33812276673403966)
,p_name=>'P8_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33921178385603187)
,p_name=>'P8_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33945882251708318)
,p_name=>'P8_SALDO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Anticipos'
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_source=>'return pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P8_CLI_ID,:f_uge_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(33986472579982708)
,p_name=>'P8_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(49203977387158235)
,p_prompt=>'Valor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34061962115124348)
,p_name=>'P8_SEQ_ID'
,p_item_sequence=>355
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34276957831522978)
,p_name=>'P8_ERROR'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Pv Error'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34418479751887878)
,p_name=>'P8_EDE_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_prompt=>'Entidad Destino'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_FORMA_PAGO_ANT'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'lv_lov := pq_ven_listas_caja.fn_lov_entidad_destin_tfp_pag',
'(',
':F_EMP_ID,',
':P8_TFP_ID,',
':P8_ERROR);',
'return (lv_lov);',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') or ',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'') '))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34418966984912526)
,p_name=>'P8_CRE_NRO_CHEQUE'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(34418164513883509)
,p_prompt=>'Nro Cheque'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34419158263912527)
,p_name=>'P8_CRE_NRO_CUENTA'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(34418164513883509)
,p_prompt=>'Nro Cuenta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34419362943912527)
,p_name=>'P8_CRE_NRO_PIN'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(34418164513883509)
,p_prompt=>'Nro Pin'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34419575960912527)
,p_name=>'P8_CRE_TITULAR_CUENTA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_prompt=>'Titular Cuenta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34419765612912527)
,p_name=>'P8_CRE_FECHA_DEPOSITO'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(34418164513883509)
,p_prompt=>'Fecha Cheque'
,p_source=>'sysdate'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(49194560100030184)
,p_name=>'P8_MCD_NRO_COMPROBANTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(49193977977025945)
,p_prompt=>'Nro Comprobante'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''PAPELETA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(73647265367463440)
,p_name=>'P8_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(74319567426750131)
,p_name=>'P8_NRO_TARJETA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>unistr('N\00BF\00BFmero Tarjeta ')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>20
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(74319777439750156)
,p_name=>'P8_MCD_VOUCHER'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>'Voucher'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>15
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(74320367058750159)
,p_name=>'P8_MCD_NRO_AUT_REF'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>unistr('Autorizaci\00BF\00BFn')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>15
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76133668533273391)
,p_name=>'P8_PRO_ID'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Pro Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76211260370781892)
,p_name=>'P8_VALOR_ORDEN_PAGO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Orden Pago'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sum(c006)',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cv_coleccion_mov_caja'')',
'   AND c005 =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_orden_pago'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(88780855059177161)
,p_name=>'P8_BANCOS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>'BANCO'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'SELECT a.ede_descripcion,a.ede_id FROM asdm_entidades_destinos a WHERE a.ede_tipo=''EFI'''
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-Seleccione-'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(88781076033177185)
,p_name=>'P8_TARJETA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>'TARJETA'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id FROM asdm_entidades_destinos b WHERE b.ede_tipo=''TJ''',
'AND b.ede_id_padre=:p8_bancos'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-Seleccione-'
,p_lov_cascade_parent_items=>'P8_BANCOS'
,p_ajax_items_to_submit=>'P8_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(88781268295177192)
,p_name=>'P8_TIPO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>'TIPO'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p8_tarjeta'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-Seleccione-'
,p_lov_cascade_parent_items=>'P8_TARJETA'
,p_ajax_items_to_submit=>'P8_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(88781475091177192)
,p_name=>'P8_PLAN'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>'PLAN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_descripcion, '' -> '') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_tede_detalle_plan_tarjeta'')',
'START WITH ede.ede_id_padre =:P8_TIPO',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-Seleccione-'
,p_lov_cascade_parent_items=>'P8_TIPO'
,p_ajax_items_to_submit=>'P8_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94599067674426454)
,p_name=>'P8_MCD_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(49193977977025945)
,p_prompt=>'Fecha Movimiento'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(96024654336463208)
,p_name=>'P8_LOTE'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Lote'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97179879468996892)
,p_name=>'P8_RAP_NRO_ESTABL_RET'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_prompt=>'Nro Establecimiento:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97180359514000585)
,p_name=>'P8_RAP_NRO_PEMISION_RET'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97181170250003687)
,p_name=>'P8_RAP_NRO_RETENCION'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_prompt=>' - '
,p_post_element_text=>' No digite los ceros de la izquierda'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this);if(this.value>0) doSubmit(''busca_retencion'');html_GetElement(''P8_RAP_NRO_RETENCION'').focus()";'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97182381332006918)
,p_name=>'P8_RAP_NRO_AUTORIZACION'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_prompt=>unistr('Autorizaci\00BF\00BFn:')
,p_post_element_text=>' 10 digitos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>11
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="valida_numero(this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97182757568009477)
,p_name=>'P8_RAP_FECHA'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_item_default=>'SYSDATE()'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha:'
,p_format_mask=>'DD-MM-RRRR'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97183366572012073)
,p_name=>'P8_RAP_FECHA_VALIDEZ'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_item_default=>'SYSDATE()'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_format_mask=>'DD-MM-RRRR'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97183781463016405)
,p_name=>'P8_RAP_COM_ID'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_prompt=>'# Comprob.:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LV_VEN_COMPROBANTES_F_ND_ANTICIPO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_COMPROBANTES_F_ND_SIN_RET'');',
'lv_lov := lv_lov || '' AND vco.cli_id = :P8_CLI_ID ORDER BY vco.com_id'';',
'    RETURN lv_lov;',
'end;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--Seleccione--'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97184659084019429)
,p_name=>'P8_SALDO_RETENCION'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_item_default=>'0'
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97184866703021563)
,p_name=>'P8_PRE_ID'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(97178254880989824)
,p_prompt=>'Pre Id:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(97269267366194895)
,p_name=>'P8_MODIFICACION'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Modificacion:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102792481971793877)
,p_name=>'P8_TITULAR_TC'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(74319365150750094)
,p_prompt=>'Titular Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(140728473759050978)
,p_name=>'P8_AUTORIZACION'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(49203977387158235)
,p_prompt=>'Clave de Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''AUTORIZA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_egr_anticipo_cli'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260520064299142116)
,p_name=>'P8_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260520273303144629)
,p_name=>'P8_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260520483000147440)
,p_name=>'P8_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260520658197149719)
,p_name=>'P8_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260520865123151768)
,p_name=>'P8_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260521074127154373)
,p_name=>'P8_CLIENTE_EXISTE'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Cliente Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260521280707156285)
,p_name=>'P8_DIR_ID'
,p_item_sequence=>25
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Dir Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(277361767321625574)
,p_name=>'P8_CLI_ID_REFERIDO'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Cli Id Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>'utilizado para el comisariato, necesito porque llamo al mismo procedimiento y tengo que mandar el mismo numero de parametros'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(277362174247627592)
,p_name=>'P8_NOMBRE_CLI_REFERIDO'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'Nombre Cli Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>'utilizado para el comisariato, necesito porque llamo al mismo procedimiento y tengo que mandar el mismo numero de parametros'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(754773561017615712)
,p_name=>'P8_NUMERO_CONTROL'
,p_item_sequence=>385
,p_item_plug_id=>wwv_flow_imp.id(754771878501603023)
,p_prompt=>'Numero Control'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15818261269956053223)
,p_name=>'P8_EMP_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(79340379865489174)
,p_item_default=>':f_emp_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(27457912870709308836)
,p_name=>'P8_ALERTA'
,p_item_sequence=>375
,p_item_plug_id=>wwv_flow_imp.id(33701761601270369)
,p_prompt=>'  '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;color:red;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P8_TTR_ID = pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_ing_anticipo_cli'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(171623173351508308161)
,p_name=>'P8_NOMBRE_1'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P8_TTR_ID'
,p_display_when2=>'381'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(171623173507922308162)
,p_name=>'P8_IDENTIFICACION_1'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(33984077035955600)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''nombres'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P8_TTR_ID'
,p_display_when2=>'381'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(33703674560270378)
,p_validation_name=>'P8_MCA_TOTAL'
,p_validation_sequence=>10
,p_validation=>'P8_MCA_TOTAL'
,p_validation_type=>'ITEM_NOT_ZERO'
,p_error_message=>'Debe Cargar el Valor'
,p_when_button_pressed=>wwv_flow_imp.id(33701968546270371)
,p_associated_item=>wwv_flow_imp.id(33702956610270374)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(33703855252270378)
,p_validation_name=>'P8_MCA_OBSERVACION'
,p_validation_sequence=>20
,p_validation=>'P8_MCA_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese la observaci\00BF\00BFn')
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(33702779055270374)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(34145059371467378)
,p_validation_name=>'valor 0 o null'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_MCD_VALOR_MOVIMIENTO is not null and',
':P8_MCD_VALOR_MOVIMIENTO > 0'))
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'El valor debe ser mayor a 0'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(33986472579982708)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(34168954297099722)
,p_validation_name=>'P8_TTR_ID'
,p_validation_sequence=>40
,p_validation=>'P8_TTR_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione la transaccion'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(33702363803270373)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244415964257524669)
,p_validation_name=>'P8_CRE_FECHA_DEPOSITO'
,p_validation_sequence=>50
,p_validation=>'P8_CRE_FECHA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese la fecha de dep\00BF\00BFsito')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'') ',
' AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(34419765612912527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244416279495529125)
,p_validation_name=>'P8_EDE_ID'
,p_validation_sequence=>60
,p_validation=>'P8_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione la Entidad Destino'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'') or ',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_deposito'') or',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_transferencia'') ',
' )AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(34418479751887878)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244416455731531665)
,p_validation_name=>'P8_CRE_TITULAR_CUENTA'
,p_validation_sequence=>70
,p_validation=>'P8_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la Cuenta'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'') ',
' )AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(34419575960912527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244416664388534191)
,p_validation_name=>'P8_CRE_NRO_CHEQUE'
,p_validation_sequence=>80
,p_validation=>'P8_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de cheque')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'') ',
' AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(34418966984912526)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244417079972538665)
,p_validation_name=>'P8_CRE_NRO_CUENTA'
,p_validation_sequence=>90
,p_validation=>'P8_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de cuenta')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'') ',
' AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(34419158263912527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(247013656704873561)
,p_validation_name=>'P8_IDENTIFICACION'
,p_validation_sequence=>100
,p_validation=>'P8_IDENTIFICACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione un cliente'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_ing_anticipo_cli'') or',
':P8_TTR_ID= pq_constantes.fn_retorna_constante(0,''cn_ttr_id_egr_anticipo_cli'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(33716674763365616)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(41432366516045679)
,p_validation_name=>'P8_ANTICIPO_CLIENTES'
,p_validation_sequence=>110
,p_validation=>'to_number(:P8_SALDO) >= to_number(:P8_MCD_VALOR_MOVIMIENTO)'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'El valor debe ser menor o igual a Total Anticipos'
,p_validation_condition=>':P8_TTR_ID =  pq_constantes.fn_retorna_constante(0,''cn_ttr_id_egr_anticipo_cli'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(33945882251708318)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(49197757508067281)
,p_validation_name=>'P8_MCD_NRO_COMPROBANTE'
,p_validation_sequence=>120
,p_validation=>'P8_MCD_NRO_COMPROBANTE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de comprobante de dep\00BF\00BFsito o transferencia')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') OR',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(49194560100030184)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(74288564119808811)
,p_validation_name=>'P8_CRE_NRO_PIN'
,p_validation_sequence=>130
,p_validation=>'P8_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Pin'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'') ',
' AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(34419362943912527)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(75150357542866797)
,p_validation_name=>'P8_NRO_TARJETA'
,p_validation_sequence=>140
,p_validation=>'P8_NRO_TARJETA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Nro de Tarjeta'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')',
'AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(74319567426750131)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(75150570702870599)
,p_validation_name=>'P8_MCD_NRO_AUT_REF'
,p_validation_sequence=>150
,p_validation=>'P8_MCD_NRO_AUT_REF'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el Nro de Autorizaci\00BF\00BFn')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')',
'AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(74319777439750156)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(75150882130873970)
,p_validation_name=>'P8_MCD_VOUCHER'
,p_validation_sequence=>160
,p_validation=>'P8_MCD_VOUCHER'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Vaucher'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')',
'AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(74320367058750159)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(76684973126725772)
,p_validation_name=>'P8_TFP_ID'
,p_validation_sequence=>180
,p_validation=>'P8_TFP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccion la forma de pago'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(33718269138382952)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(77364180307064382)
,p_validation_name=>'P8_MCD_NRO_AUT_REF'
,p_validation_sequence=>190
,p_validation=>'P8_MCD_NRO_AUT_REF'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>unistr('En la autorizaci\00BF\00BFn debe escribir unicamente numeros')
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(74320367058750159)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(88849854094132420)
,p_validation_name=>'P8_PLAN'
,p_validation_sequence=>200
,p_validation=>'P8_PLAN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe seleccionar el plan'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')',
' )AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(88781475091177192)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97238451586001113)
,p_validation_name=>'P8_RAP_NRO_PEMISION_RET'
,p_validation_sequence=>210
,p_validation=>'to_number(:P8_RAP_NRO_PEMISION_RET)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero del punto de emisi\00BF\00BFn de la retencion')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97180359514000585)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97240781368009716)
,p_validation_name=>'P8_RAP_NRO_ESTABL_RET'
,p_validation_sequence=>220
,p_validation=>'to_number(:P8_RAP_NRO_ESTABL_RET)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de establecimiento de la retenci\00BF\00BFn')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97179879468996892)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97241869378015711)
,p_validation_name=>'P8_RAP_NRO_RETENCION'
,p_validation_sequence=>230
,p_validation=>'to_number(:P8_RAP_NRO_RETENCION) > 0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de retenci\00BF\00BFn')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97181170250003687)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97242955311021110)
,p_validation_name=>'P8_RAP_COM_ID'
,p_validation_sequence=>240
,p_validation=>'P8_RAP_COM_ID'
,p_validation_type=>'ITEM_NOT_NULL_OR_ZERO'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de comprobante')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97183781463016405)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97243562583023261)
,p_validation_name=>'P8_RAP_FECHA_VALIDEZ'
,p_validation_sequence=>250
,p_validation=>'P8_RAP_FECHA_VALIDEZ'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese fecha de validez de la retencion'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97183366572012073)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97243769509025254)
,p_validation_name=>'P8_RAP_FECHA'
,p_validation_sequence=>260
,p_validation=>'P8_RAP_FECHA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese fecha de la retencion'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97182757568009477)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(97244776435027234)
,p_validation_name=>'P8_RAP_NRO_AUTORIZACION'
,p_validation_sequence=>270
,p_validation=>'to_number(:P8_RAP_NRO_AUTORIZACION)>0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de autorizaci\00BF\00BFn de la retenci\00BF\00BFn')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97182381332006918)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(102585466249004148)
,p_validation_name=>'P8_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>290
,p_validation=>'P8_MCD_FECHA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe Ingresar la Fecha de Deposito o Transferencia'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') OR',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(94599067674426454)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(102620256964181206)
,p_validation_name=>'P8_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>300
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if to_date(:p8_mcd_fecha_deposito,''dd-mm-yyyy'') > to_date(sysdate,''dd-mm-yyyy'') then',
'return ''La fecha no puede ser mayor al dia de hoy'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'Error'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') OR',
':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(94599067674426454)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(102908253068899112)
,p_validation_name=>'P8_TITULAR_TC'
,p_validation_sequence=>310
,p_validation=>'P8_TITULAR_TC'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la Tarjeta'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:P8_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')',
' )AND',
':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_ing_anticipo_cli'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(102792481971793877)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(107798268472499684)
,p_validation_name=>'P8_RAP_FECHA_VALIDEZ_MAYOR_ACTUAL'
,p_validation_sequence=>320
,p_validation=>'trunc(to_date(:P8_RAP_FECHA_VALIDEZ)) >= trunc(SYSDATE)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'La fecha de validez no debe ser menor a la fecha actual'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97183366572012073)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109336572634114310)
,p_validation_name=>'P8_RAP_NRO_AUTORIZACION_MAX'
,p_validation_sequence=>330
,p_validation=>'LENGTH(:P8_RAP_NRO_AUTORIZACION) = 10'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El n\00BF\00BFmero de autorizaci\00BF\00BFn de la retenci\00BF\00BFn debe ser de 10 d\00BF\00BFgitos.')
,p_validation_condition=>':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97182381332006918)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109337065623131276)
,p_validation_name=>'P8_RAP_FECHA_VALIDEZ_MAYOR_FECHA'
,p_validation_sequence=>340
,p_validation=>'trunc(to_date(:P8_RAP_FECHA_VALIDEZ)) >= trunc(to_date(:P8_RAP_FECHA))'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('La fecha de validez no puede ser menor a la fecha de ingreso de la retenci\00BF\00BFn')
,p_validation_condition=>':P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(97183366572012073)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(140736070259097259)
,p_validation_name=>'P8_AUTORIZACION'
,p_validation_sequence=>350
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p8_autorizacion = pq_ven_movimientos_caja.fn_claves_caja(:f_emp_id,:P8_TTR_ID,TO_NUMBER(regexp_replace(:P8_IDENTIFICACION, ''[^0-9]'', '''')),:P8_TFP_ID,:P8_MCD_VALOR_MOVIMIENTO)',
'--if :p8_autorizacion = pq_ven_movimientos_caja.fn_claves_caja(:f_emp_id,:P8_TTR_ID,TO_NUMBER(substr(lpad(:P8_IDENTIFICACION, 13, 1),10,4)),:P8_TFP_ID,:P8_MCD_VALOR_MOVIMIENTO)',
' then',
'return null;',
'else ',
':p8_AUTORIZACION := null;',
'return ''CLAVE INCORRECTA '';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>'AUTORIZA'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(140728473759050978)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(141014677117736677)
,p_validation_name=>'P8_MCD_NRO_COMPROBANTE'
,p_validation_sequence=>360
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if pq_ven_movimientos_caja.fn_papeletas_ingresadas(:f_emp_id,:P8_MCD_NRO_COMPROBANTE) IS NULL',
' then',
'return null;',
'else ',
':P8_MCD_NRO_COMPROBANTE := null;',
'return ''YA EXISTE UNA PAPELETA INGRESADA CON ESE NUMERO'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'YA EXISTE UNA PAPELETA INGRESADA CON ESE NUMERO DE PAPELETA'
,p_validation_condition=>'PAPELETA'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(49194560100030184)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(7811549858461623651)
,p_validation_name=>'P8_DIRECCION'
,p_validation_sequence=>370
,p_validation=>'P8_DIRECCION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'El cliente no tiene una direccion principal asignada.  En el mantenimiento de clientes asignele una direccion principal'
,p_validation_condition=>':P8_TTR_ID= pq_constantes.fn_retorna_constante(null,''cn_ttr_id_egr_anticipo_cli'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(260520273303144629)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(754779054436755462)
,p_validation_name=>'P8_NUMERO_CONTROL'
,p_validation_sequence=>380
,p_validation=>'P8_NUMERO_CONTROL'
,p_validation2=>'^[0-9]+$'
,p_validation_type=>'REGULAR_EXPRESSION'
,p_error_message=>'Ingrese solo valores numericos'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(754773561017615712)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(754781879415789537)
,p_validation_name=>'P8_NUMERO_CONTROL'
,p_validation_sequence=>390
,p_validation=>'P8_NUMERO_CONTROL'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'INGRESE EL NUMERO DE CONTROL'
,p_validation_condition=>':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_retiros_vec_pic'') OR :P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_depositos_vec_pic'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(754773561017615712)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(754783464472872360)
,p_validation_name=>'P8_NUMERO_CONTROL'
,p_validation_sequence=>400
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if pq_ven_movimientos_caja.fn_control_numero_pic(:p8_numero_control,:f_emp_id) > 0 then',
'   return ''Ya se encuentra registrado ese numero de comprobante'';',
'else',
'   return null;',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'Numero de Control existente'
,p_validation_condition=>':P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_retiros_vec_pic'') OR :P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_depositos_vec_pic'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(33990760207035904)
,p_associated_item=>wwv_flow_imp.id(754773561017615712)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(107938658213206286)
,p_name=>'ad_refresca_region'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P8_CARGAR'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107938959324206287)
,p_event_id=>wwv_flow_imp.id(107938658213206286)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(97178254880989824)
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(34069983689263079)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'EliminaLineaColeccionDetalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_borra_reg_colecciones(',
'                  :p8_seq_id ,',
'                   pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_mov_caja''));',
':p8_seq_id := null;',
':P8_MODIFICACION := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1816832419498153
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(33703971533270378)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_generar_movimiento'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_rap_id asdm_retenciones_aplicadas.rap_id%TYPE; -- Yguaman 2011/10/22 10:53am ',
'',
'  Ln_eje_eve_id number;',
'  Lv_Error      varchar2(250);',
'  cursor cu_autorizaciones is',
'    select c001 usu_aut, c002 ttr_id, c003 valor, c004 Est_aut',
'      from apex_collections',
'     where collection_name = pq_asdm_seguridad.cv_coleccionAut;',
'  ln_mca_id      con_movimientos_cabecera.mca_id%type;',
'  lv_observacion varchar2(2000);',
'',
'  ln_cuenta number;',
'',
'BEGIN',
'',
'  select count(*)',
'    into ln_cuenta',
'    from apex_collections col',
'   where col.collection_name = ''CO_MOV_CAJA''',
'     and to_number(col.c005) =',
'         pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                            ''cn_tfp_id_orden_pago'');',
'',
'  lv_observacion := replace(:P8_MCA_OBSERVACION, '','', ''.'');',
'',
'  IF :P8_TTR_ID =',
'     pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_ing_anticipo_cli'') THEN',
'  ',
unistr('    --- Yessica Guam\00BF\00BFn 2011/10/22. Cuando es ingreso de anticipo por retenci\00BF\00BFn debe grabarse los datos de la retenci\00BF\00BFn primero ya que se reemplaza el # de retenci\00BF\00BFn por el id de retenci\00BF\00BFn generado'),
'    pq_ven_pagos_cuota.pr_graba_pago_retencion(pn_emp_id   => :f_emp_id,',
'                                               pv_facturar => ''N'',',
'                                               pv_error    => :p0_error);',
'  ',
'  END IF;',
'',
'  ------------------------***********************************---------------------------------------------------------',
'',
'  Lv_Error := null;',
'',
'  IF :P8_TTR_ID =',
'     pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_ing_anticipo_cli'') or',
'     :P8_TTR_ID =',
'     pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_egr_anticipo_cli'') THEN',
'  ',
'    pq_ven_movimientos_caja.pr_crea_proveedor_desd_dev_ant(:P8_CLI_ID,',
'                                                           :F_EMP_ID,',
'                                                           :P8_DIR_ID,',
'                                                           :P8_PRO_ID,',
'                                                           :P0_ERROR);',
'  ',
'    PQ_VEN_MOVIMIENTOS_CAJA.PR_GRABA_MOVIMIENTO_CAJA(pn_emp_id                => :f_emp_id,',
'                                                     pn_uge_id                => :f_uge_id,',
'                                                     pn_uge_id_gasto          => :F_UGE_ID_GASTO,',
'                                                     pn_usu_id                => :f_user_id,',
'                                                     pn_ttr_id                => :P8_TTR_ID,',
'                                                     pn_pca_id                => :P8_PCA_ID,',
'                                                     pn_mca_id_referencia     => null,',
'                                                     pn_age_id_gestionado_por => null,',
'                                                     pn_cli_id                => :P8_CLI_ID,',
'                                                     pd_mca_fecha             => TO_DATE(sysdate, ''DD/MM/YYYY HH24:MI:SS''),',
'                                                     pv_mca_observacion       => lv_observacion,',
'                                                     pv_mca_estado_mc         => null,',
'                                                     pn_mca_total             => :P8_MCA_TOTAL,',
'                                                     pn_mca_id                => :P8_MCA_ID,',
'                                                     pn_trx_id                => :P8_TRX_ID,',
'                                                     pv_error                 => :p0_error);',
'  ',
'  ',
'',
'  ',
'    PQ_VEN_MOVIMIENTOS_CAJA.PR_GRABA_ANTICIPO_CLIENTES(pn_cli_id            => :P8_CLI_ID,',
'                                                       pn_emp_id            => :f_emp_id,',
'                                                       pn_ttr_id            => :P8_TTR_ID,',
'                                                       pn_mca_total         => :P8_MCA_TOTAL,',
'                                                       pv_acl_observaciones => lv_observacion,',
'                                                       pn_mca_id            => :P8_MCA_ID,',
'                                                       pn_com_id            => null,',
'                                                       pn_trx_id            => :P8_TRX_ID,',
'                                                       pd_fecha             => sysdate,',
'                                                       pn_uge_id            => :f_uge_id,',
'                                                       pv_error             => :p0_error);',
'                                                       ',
' -- raise_application_Error(-20000, ''aquiii 3 '');',
'',
'  ',
'    /*pr_pruebas_ja(pn_number  => 999,',
'                  pv_varchar => '' :P8_MCA_ID  '' || :P8_MCA_ID,',
'                  pc_clob    => :p0_error); */',
'  ',
'    PQ_VEN_MOVIMIENTOS_CAJA.PR_IMPRIMIR_MOV_CAJA(pn_emp_id => :f_emp_id,',
'                                                 pn_mca_id => :P8_MCA_ID,',
'                                                 pv_error  => :p0_error);',
'  ',
'    pr_url(pn_app_destino => 211,',
'           pn_pag_destino => 12,',
'           pv_request     => ''carga_ventas'',',
'           pv_parametros  => ''P12_MCA_ID'',',
'           pv_valores     => :P8_MCA_ID);',
'  ',
'    /*',
'    ',
'    pq_swi_gen.pr_iniciar_ejecucion_evento(''ANT_CLIENTES'',',
'                                           Ln_eje_eve_id,',
'                                           Lv_Error);',
'    if Lv_Error is null then',
'    ',
'      --transaccion',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_emp_id'',',
'                                         :F_EMP_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_uge_id'',',
'                                         :F_UGE_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_uge_id_gasto'',',
'                                         :F_UGE_ID_GASTO,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_usu_id'',',
'                                         :F_USER_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_ttr_id'',',
'                                         :P8_TTR_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_trx_id'',',
'                                         :P8_TRX_ID,',
'                                         Lv_Error);',
'    ',
'      --Movimiento caja',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_ID'',',
'                                         :P8_MCA_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_PCA_ID'',',
'                                         :P8_PCA_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_ID_REFERENCIA'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_AGE_ID_GESTIONADO_POR'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_CLI_ID'',',
'                                         :P8_CLI_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PD_MCA_FECHA'',',
'                                         TO_DATE(sysdate,',
'                                                 ''DD/MM/YYYY HH24:MI:SS''),',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_MCA_OBSERVACION'',',
'                                         lv_observacion,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_MCA_ESTADO_MC'',',
'                                         :NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_TOTAL'',',
'                                         :P8_MCA_TOTAL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_COM_ID'',',
'                                         :NULL,',
'                                         Lv_Error);',
'    ',
'      -- Anticipo de Clientes',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_ACL_OBSERVACIONES'',',
'                                         lv_observacion,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pv_error'',',
'                                         :P0_error,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PD_FECHA'',',
'                                         SYSDATE,',
'                                         Lv_Error);',
'    ',
'      -- Redirect despues grabar ant clientes',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_DESDE'',',
'                                         ''MC'',',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_APP_ID'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_PAG_ID'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_SESSION'',',
'                                         :session,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_TOKEN'',',
'                                         :f_token,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_USER_ID'',',
'                                         :f_user_id,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_ROL'',',
'                                         :p0_rol,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_ROL_DESC'',',
'                                         :p0_rol_desc,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_TREE_ROT'',',
'                                         :p0_tree_root,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_OPCION'',',
'                                         :f_opcion_id,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_PARAMETRO'',',
'                                         :f_parametro,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pv_empresa'',',
'                                         :f_empresa,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_UGESTION'',',
'                                         :f_ugestion,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_PRO_ID'',',
'                                         :P8_PRO_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_VALOR'',',
'                                         :P8_VALOR_ORDEN_PAGO,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_UUG_ID'',',
'                                         :F_UUG_ID,',
'                                         Lv_Error);',
'    ',
'      if Lv_Error is null then',
'        pq_swi_gen.pr_ejecutar_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'    ',
'      if Lv_Error is null then',
'        pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                           ''PN_MCA_ID'',',
'                                           :P8_MCA_ID,',
'                                           Lv_Error);',
'        pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                           ''pn_trx_id'',',
'                                           :P8_TRX_ID,',
'                                           Lv_Error);',
'        pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                           ''pv_error'',',
'                                           :lv_error,',
'                                           Lv_Error);',
'      ',
'      end if;',
'    ',
'      if Lv_Error is null then',
'        pq_swi_gen.pr_cerrar_ejecucion_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'      ',
'      ',
'    end if;',
'    ',
'    */',
'  ELSE',
'    ---------------------nuevo-----------------------------    ',
'  ',
'    pq_swi_gen.pr_iniciar_ejecucion_evento(''MOV_CAJ'',',
'                                           Ln_eje_eve_id,',
'                                           Lv_Error);',
'    if Lv_Error is null then',
'    ',
'      --transaccion',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_emp_id'',',
'                                         :F_EMP_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_uge_id'',',
'                                         :F_UGE_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_uge_id_gasto'',',
'                                         :F_UGE_ID_GASTO,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_usu_id'',',
'                                         :F_USER_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_ttr_id'',',
'                                         :P8_TTR_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_trx_id'',',
'                                         :P8_TRX_ID,',
'                                         Lv_Error);',
'    ',
'      --Movimiento caja',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_ID'',',
'                                         :P8_MCA_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_PCA_ID'',',
'                                         :P8_PCA_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_ID_REFERENCIA'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_AGE_ID_GESTIONADO_POR'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_CLI_ID'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PD_MCA_FECHA'',',
'                                         TO_DATE(sysdate,',
'                                                 ''DD/MM/YYYY HH24:MI:SS''),',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_MCA_OBSERVACION'',',
'                                         lv_observacion,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_MCA_ESTADO_MC'',',
'                                         :NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_TOTAL'',',
'                                         :P8_MCA_TOTAL,',
'                                         Lv_Error);',
'    ',
'      -- Redirect despues grabar ant clientes',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_DESDE'',',
'                                         ''MC'',',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_APP_ID'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_PAG_ID'',',
'                                         NULL,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_SESSION'',',
'                                         :session,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_TOKEN'',',
'                                         :f_token,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_USER_ID'',',
'                                         :f_user_id,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_ROL'',',
'                                         :p0_rol,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_ROL_DESC'',',
'                                         :p0_rol_desc,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_TREE_ROT'',',
'                                         :p0_tree_root,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_OPCION'',',
'                                         :f_opcion_id,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_PARAMETRO'',',
'                                         :f_parametro,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pv_empresa'',',
'                                         :f_empresa,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PV_UGESTION'',',
'                                         :f_ugestion,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_PRO_ID'',',
'                                         :P8_PRO_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_VALOR'',',
'                                         :P8_VALOR_ORDEN_PAGO,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_UUG_ID'',',
'                                         :F_UUG_ID,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                         ''pv_error'',',
'                                         :P0_error,',
'                                         Lv_Error);',
'    ',
'      if Lv_Error is null then',
'        pq_swi_gen.pr_ejecutar_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'    ',
'      if Lv_Error is null then',
'        pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                           ''PN_MCA_ID'',',
'                                           :P8_MCA_ID,',
'                                           Lv_Error);',
'        pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                           ''pn_trx_id'',',
'                                           :P8_TRX_ID,',
'                                           Lv_Error);',
'        pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                           ''pv_error'',',
'                                           :lv_error,',
'                                           Lv_Error);',
'      ',
'      end if;',
'    ',
'      if Lv_Error is null then',
'        pq_swi_gen.pr_cerrar_ejecucion_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'    end if;',
'  ',
'  END IF;',
'',
'',
'  --------------------------------------------------------------------------------------',
'  for reg_aud in cu_autorizaciones loop',
'    insert into kseg_e.kseg_autorizaciones',
'    values',
'      (null,',
'       sysdate,',
'       :P8_TRX_ID,',
'       reg_aud.valor,',
'       :F_USER_ID,',
'       reg_aud.usu_aut,',
'       reg_aud.ttr_id,',
'       reg_aud.est_aut);',
'  end loop;',
'',
'  :P0_ERROR := Lv_Error;',
'',
'  IF :P8_TTR_ID =',
'     pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_ing_anticipo_cli'') THEN',
'    pq_con_funciones_rubros.pr_cargar_transacciones(pn_trx_id    => :P8_TRX_ID,',
'                                                    pn_mca_id    => ln_mca_id,',
'                                                    pn_mon_id    => 1,',
'                                                    pd_rep_fecha => sysdate,',
'                                                    pv_error     => :p0_error);',
'  ',
'  elsif :P8_TTR_ID =',
'        pq_constantes.fn_retorna_constante(null,',
'                                           ''cn_ttr_id_egr_anticipo_cli'') and',
'        ln_cuenta = 0 THEN',
'    pq_con_funciones_rubros.pr_cargar_transacciones(pn_trx_id    => :P8_TRX_ID,',
'                                                    pn_mca_id    => ln_mca_id,',
'                                                    pn_mon_id    => 1,',
'                                                    pd_rep_fecha => sysdate,',
'                                                    pv_error     => :p0_error);',
'  ',
'  elsif :P8_TTR_ID !=',
'        pq_constantes.fn_retorna_constante(null,',
'                                           ''cn_ttr_id_ing_anticipo_cli'') and',
'        :P8_TTR_ID !=',
'        pq_constantes.fn_retorna_constante(null,',
'                                           ''cn_ttr_id_egr_anticipo_cli'') then',
'    pq_con_funciones_rubros.pr_cargar_transacciones(pn_trx_id    => :P8_TRX_ID,',
'                                                    pn_mca_id    => ln_mca_id,',
'                                                    pn_mon_id    => 1,',
'                                                    pd_rep_fecha => sysdate,',
'                                                    pv_error     => :p0_error);',
'  END IF;',
'  -------****-------',
'',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_mov_caja''));',
'',
'end;',
'if :P0_ERROR is null then',
'  :P8_CLI_ID               := null;',
'  :P8_IDENTIFICACION       := null;',
'  :P8_NOMBRE               := null;',
'  :P8_TIPO_DIRECCION       := null;',
'  :P8_DIRECCION            := null;',
'  :P8_TIPO_TELEFONO        := null;',
'  :P8_TELEFONO             := null;',
'  :P8_CORREO               := null;',
'  :P8_CLIENTE_EXISTE       := null;',
'  :P8_DIR_ID               := null;',
'  :P8_SALDO                := null;',
'  :P8_MCA_OBSERVACION      := null;',
'  :P8_MCA_TOTAL            := null;',
'  :P8_CLI_ID_REFERIDO      := null;',
'  :P8_NOMBRE_CLI_REFERIDO  := null;',
'  :P8_SEQ_ID               := null;',
'  :P8_EDE_ID               := null;',
'  :P8_TFP_ID               := null;',
'  :P8_CRE_FECHA_DEPOSITO   := null;',
'  :P8_CRE_TITULAR_CUENTA   := null;',
'  :P8_CRE_NRO_CHEQUE       := null;',
'  :P8_CRE_NRO_CUENTA       := null;',
'  :P8_CRE_NRO_PIN          := null;',
'  :P8_MCD_VALOR_MOVIMIENTO := null;',
'  :P8_CARGAR               := null;',
'  :P8_MCD_NRO_COMPROBANTE  := null;',
'  :P8_PRO_ID               := null;',
'  :P8_MCD_VOUCHER          := null;',
'  :P8_MCD_FECHA_DEPOSITO   := null;',
'  :P8_TITULAR_TC           := null;',
'  :P8_AUTORIZACION         := NULL;',
'end if;',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(33701968546270371)
,p_process_when=>'graba_imprime'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1450820263505452
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260565758579802655)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_listas2.pr_datos_clientes(',
':f_uge_id,',
':f_emp_id,',
':P8_IDENTIFICACION,',
':P8_NOMBRE,',
':P8_CLI_ID,',
':P8_CORREO,',
':P8_DIRECCION,',
':P8_TIPO_DIRECCION,        ',
':P8_TELEFONO,              ',
':P8_TIPO_TELEFONO,        ',
':P8_CLIENTE_EXISTE,',
':P8_DIR_ID,',
':P8_CLI_ID_REFERIDO,',
':P8_NOMBRE_CLI_REFERIDO,',
':P8_TIPO_IDENTIFICACION,',
':P0_ERROR);',
'--RAISE_APPLICATION_ERROR(-20000,:P8_DIR_ID);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>228312607310037729
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(97272080310208065)
,p_process_sequence=>2
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_ven_pagos_cuota.pr_valida_pago_retencion(pn_nro_retencion        => :P8_RAP_NRO_RETENCION,',
'                         pn_emp_id               => :F_EMP_ID,',
'                         pn_com_id               => :P8_RAP_COM_ID, ---ln_comprobante,',
'                         pv_RAP_NRO_ESTABL_RET   => :P8_RAP_NRO_ESTABL_RET ,',
'                         pv_RAP_NRO_PEMISION_RET => :P8_RAP_NRO_PEMISION_RET,',
'                         pv_facturar             => ''N'', --- Para ver si viene de la facturacion o del pago de cuota                  ',
'                         pn_saldo_retencion      => :P8_SALDO_RETENCION,',
'                         pn_mcd_valor_movimiento => :P8_MCD_VALOR_MOVIMIENTO,',
'                         pn_valor_total_pagos    => :P8_VALOR_TOTAL_PAGOS, -- Usado cuando viene de facturacion',
'                         pv_modificacion         => :P8_MODIFICACION,',
'                         pn_pre_id               => :P8_PRE_ID,',
'                         pv_dejar_saldo           => ''N'',',
'                         pv_rap_nro_autorizacion => :P8_RAP_NRO_AUTORIZACION,',
'                         pv_error                => :P0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(33990760207035904)
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p8_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>65018929040443139
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(247250774582028624)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'CargaColeccionMovimientoDetalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Declare',
'ln_efectivo NUMBER(15,2);',
'ln_cheque NUMBER(15,2);',
'ln_tarjeta NUMBER(15,2);',
'Begin',
'',
'IF (:P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_egr_anticipo_cli'') or :P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_retiros_vec_pic'')) AND :P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_efec'
||'tivo'') THEN',
'pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id       =>  :f_emp_id,',
'                        pn_pca_id         => :f_pca_Id,',
'                        pn_valor_efectivo => ln_efectivo,',
'                        pn_valor_cheque   => ln_cheque,',
'                        pn_valor_tarjeta  => ln_tarjeta,',
'                        pv_error          => :p0_error);',
'',
'if ln_efectivo < nvl(to_number(:P8_MCD_VALOR_MOVIMIENTO),0) then',
'  :p0_error := ''NO TIENE DINERO EN EFECTIVO DISPONIBLE EN CAJA'';',
'raise_application_error(-20000,',
'                              :p0_error);',
'END IF;',
'end if;',
'',
'IF :P8_MODIFICACION IS NULL and :p0_error IS NULL THEN',
'',
' pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                    pn_tfp_id => :P8_TFP_ID,',
'                                                    pv_error => :p0_error);',
'',
'  ',
'if :p0_error IS NULL THEN',
'',
'/*Agregado por Andres Calle',
'07/septiembre/2011',
'Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'*/',
'',
'  if :P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_tarjeta_credito'') then',
'     :p8_ede_id := :p8_plan;',
'  end if;',
'',
'if :P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_retiros_vec_pic'') OR :P8_TTR_ID = pq_constantes.fn_retorna_constante(0,''cn_ttr_id_depositos_vec_pic'') then',
':P8_MCD_NRO_COMPROBANTE := :P8_NUMERO_CONTROL;',
'end if;',
'',
'  pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => :p8_ede_id,',
'                                                      pn_cre_id               => :p8_cre_id, --null,',
'                                                      pn_tfp_id               => :p8_tfp_id,',
'                                                      pn_mcd_valor_movimiento => nvl(to_number(:P8_MCD_VALOR_MOVIMIENTO),0),',
'                                                      pn_mcd_nro_aut_ref      => :P8_MCD_NRO_AUT_REF,',
'                                                      pn_mcd_nro_lote         => :P8_LOTE, --null,',
'                                                      pn_mcd_nro_retencion    => :P8_RAP_NRO_RETENCION, --null,',
'                                                      pn_mcd_nro_comprobante  => :P8_MCD_NRO_COMPROBANTE,',
'                                                      pv_titular_cuenta       => :p8_cre_titular_cuenta,',
'                                                      pv_cre_nro_cheque       => :p8_cre_nro_cheque,',
'                                                      pv_cre_nro_cuenta       => :p8_cre_nro_cuenta,',
'                                                      pv_nro_pin              => :p8_cre_nro_pin,',
'                                                      pv_nro_tarjeta          => :P8_NRO_TARJETA,',
'                                                      pv_voucher              => :P8_MCD_VOUCHER,',
unistr('                                                      pn_saldo_retencion      => nvl(to_number(:P8_SALDO_RETENCION),0), --- Yguaman  2011/10/04 19:32pm pago de cuota con retenci\00BF\00BFn'),
unistr('                                                      pn_com_id               => NVL(:P8_RAP_COM_ID,0), --- Yguaman 2011/10/10 pago de cuota con retenci\00BF\00BFn'),
unistr('                                                      pd_rap_fecha            => :P8_RAP_FECHA,--- Yguaman 2011/10/10 pago de cuota con retenci\00BF\00BFn'),
unistr('                                                      pd_rap_fecha_validez    => :P8_RAP_FECHA_VALIDEZ,--- Yguaman 2011/10/10 pago de cuota con retenci\00BF\00BFn'),
unistr('                                                      pn_rap_nro_autorizacion => :P8_RAP_NRO_AUTORIZACION,--- Yguaman 2011/10/10 pago de cuota con retenci\00BF\00BFn'),
unistr('                                                      pn_RAP_NRO_ESTABL_RET   => :P8_RAP_NRO_ESTABL_RET, -- Yguaman  2011/10/10 para pago de cuota con retenci\00BF\00BFn'),
unistr('                                                      pn_RAP_NRO_PEMISION_RET => :P8_RAP_NRO_PEMISION_RET, -- Yguaman  2011/10/10 para pago de cuota con retenci\00BF\00BFn'),
'                                                      pn_PRE_ID               => :P8_PRE_ID,',
'                                                      pd_fecha_deposito       => :P8_MCD_FECHA_DEPOSITO, --Andres Calle 07/Oct/2011 fecha del deposito de una papeleta',
'                                                      pv_titular_tarjeta_cre  => :P8_TITULAR_TC, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito                                                      ',
unistr('                                                      --pn_rap_valor_retencion  => ln_valor_retencion, -- Yguaman  2011/10/10 para pago de cuota con retenci\00BF\00BFn '),
'                                                      pv_error => :p0_error);',
'',
'',
':P8_SEQ_ID:= null;',
':P8_EDE_ID := null;',
':P8_TFP_ID := null;',
':P8_MCD_VALOR_MOVIMIENTO := null;',
':P8_CRE_TITULAR_CUENTA	:= null;',
':P8_CRE_NRO_CHEQUE	:= null;',
':P8_CRE_NRO_CUENTA:= null;',
':P8_CRE_NRO_PIN:= null;',
':P8_MCD_NRO_AUT_REF:= null;                                                    ',
':P8_MCD_NRO_COMPROBANTE:= null;',
':P8_NRO_TARJETA:= null;',
':P8_MCD_VOUCHER:= null;',
':P8_PLAN := NULL;',
':P8_MCD_FECHA_DEPOSITO := NULL;',
':P8_NUMERO_CONTROL := null;',
'',
unistr(' --- Retencion -- Yguaman  2011/10/10 para pago de cuota con retenci\00BF\00BFn'),
':P8_RAP_NRO_RETENCION   := NULL;',
':P8_RAP_NRO_AUTORIZACION := NULL;',
':P8_RAP_FECHA := NULL;',
':P8_RAP_FECHA_VALIDEZ := NULL;',
':P8_RAP_COM_ID := NULL;',
':P8_SALDO_RETENCION      := NULl;',
':P8_RAP_NRO_ESTABL_RET   := NULL; ',
':P8_RAP_NRO_PEMISION_RET := NULL; ',
':P8_PRE_ID               := NULL; ',
'ELSE',
'',
':P8_MODIFICACION := NULL;',
'',
'END IF;',
'END If;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(33990760207035904)
,p_process_when=>'P8_SEQ_ID'
,p_process_when_type=>'ITEM_IS_NULL'
,p_process_success_message=>'CargaColeccionMovimientoDetalle'
,p_internal_uid=>214997623312263698
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(34068853944207115)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ActualizaColeccionMovimientoDetalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'/*',
'',
'Agregado por Andres Calle',
'07/septiembre/2011',
'Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'*/',
'   if :P8_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_tarjeta_credito'') then',
'      :p8_ede_id := :p8_plan;',
'   end if;',
'',
unistr('   ---Yguaman 2011/10/05 - 06 Se a\00BF\00BFadieron parametros en pq_ven_movimientos_caja.pr_actualiza_col_mov_caja para pago de cuota con retenci\00BF\00BFn'),
'',
'   pq_ven_movimientos_caja.pr_actualiza_col_mov_caja(pn_ede_id               => :p8_ede_id,',
'                                                  pn_cre_id               => :P8_CRE_ID,--null,',
'                                                  pn_tfp_id               => :P8_TFP_ID,',
'                                                  pn_mcd_valor_movimiento => nvl(to_number(:p8_mcd_valor_movimiento),0),',
'                                                  pn_mcd_nro_aut_ref      => :P8_MCD_NRO_AUT_REF,',
'                                                  pn_mcd_nro_lote         => :P8_LOTE,',
'                                                  pn_mcd_nro_retencion    => :P8_RAP_NRO_RETENCION,',
'                                                  pn_mcd_nro_comprobante  => :P8_MCD_NRO_COMPROBANTE,',
'                                                  pv_titular_cuenta       => :P8_CRE_TITULAR_CUENTA, -- :P8_TJ_TITULAR',
'                                                  pv_cre_nro_cheque       => :P8_CRE_NRO_CHEQUE,',
'                                                  pv_cre_nro_cuenta       => :P8_CRE_NRO_CUENTA,',
'                                                  pv_nro_pin              => :P8_CRE_NRO_PIN,',
'                                                  pv_nro_tarjeta          => :P8_TJ_NUMERO,',
'                                                  pv_mcd_voucher          => :P8_TJ_NUMERO_VOUCHER,',
'                                                  pn_seq_id               => :P8_seq_id,',
unistr('                                                  -----Para pago de cuota con retenci\00BF\00BFn'),
'                                                  pn_saldo_retencion      => nvl(to_number(:P8_SALDO_RETENCION),0), ---- yguaman 2011/10/05 ',
'                                                  pn_com_id               => :P8_RAP_COM_ID, ---- yguaman 2011/10/05',
'                                                  pd_rap_fecha            => :P8_RAP_FECHA,--- Yguaman 2011/10/05 ',
'                                                  pd_rap_fecha_validez    => :P8_RAP_FECHA_VALIDEZ,--- Yguaman 2011/10/05',
'                                                  pn_rap_nro_autorizacion => :P8_RAP_NRO_AUTORIZACION,-- Yguaman 2011/10/05',
'                                                  pn_RAP_NRO_ESTABL_RET   => :P8_RAP_NRO_ESTABL_RET, -- Yguaman  2011/10/06',
'                                                  pn_RAP_NRO_PEMISION_RET => :P8_RAP_NRO_PEMISION_RET, -- Yguaman  2011/10/06',
'                                                  pn_PRE_ID               => :P8_PRE_ID,-- Yguaman  2011/10/06 ',
'                                                  pd_fecha_deposito       => :P8_MCD_FECHA_DEPOSITO, ',
'                                                  pn_emp_id               => :f_emp_id, --- Yguaman 2011/10/06   ',
'                                                  pv_titular_tarjeta_cre  => :P8_TITULAR_TC, --Andres Calle 26/Oct/2011 Hay que grabar el titular de la tarjeta de Credito',
'                                                  pv_error                => :P0_ERROR);',
'',
'',
'',
'                                                   ',
':P8_SEQ_ID:= null;',
':P8_MODIFICACION := null;',
':P8_EDE_ID := null;',
':P8_TFP_ID := null;',
':P8_MCD_VALOR_MOVIMIENTO := null;',
':P8_CRE_TITULAR_CUENTA	:= null;',
':P8_CRE_NRO_CHEQUE	:= null;',
':P8_CRE_NRO_CUENTA:= null;',
':P8_CRE_NRO_PIN:= null;',
':P8_MCD_NRO_AUT_REF_VOUCHER:= null;                                                    ',
':P8_MCD_NRO_COMPROBANTE:= null;',
':P8_NRO_TARJETA:= null;',
':P8_MCD_VOUCHER := null;',
':P8_PLAN := NULL;',
':P8_MCD_FECHA_DEPOSITO := NULL;',
'',
'--- NULL DATOS DE RETENCION --',
':P8_SALDO_RETENCION := NULL;',
':P8_RAP_COM_ID :=NULL;',
':P8_RAP_FECHA :=NULL;',
':P8_RAP_FECHA_VALIDEZ :=NULL;',
':P8_RAP_NRO_AUTORIZACION := NULL;',
':P8_RAP_NRO_ESTABL_RET := NULL;',
':P8_RAP_NRO_PEMISION_RET :=NULL;',
':P8_PRE_ID := NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(33990760207035904)
,p_process_when=>':P8_SEQ_ID  is not null and :P8_MODIFICACION = ''S'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>1815702674442189
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(34157053025607416)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Limpia'
,p_process_sql_clob=>'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(34152359296561976)
,p_internal_uid=>1903901755842490
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(97436462422536895)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pantalla'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
':P8_EDE_ID := NULL;',
':P8_CRE_TITULAR_CUENTA := NULL;',
'--:P8_SUM_PAGOS',
'--P8_ES_PRECANCELACION',
'--P8_SALDO_PROMO',
'--P8_SALDO_MAX_APLICAR',
':P8_MODIFICACION := NULL;',
':P8_TJ_ENTIDAD := NULL;',
':P8_BANCOS := NULL;',
':P8_TARJETA := NULL;',
':P8_TIPO := NULL;',
':P8_PLAN := NULL;',
':P8_NRO_TARJETA:= NULL;',
':P8_MCD_VOUCHER := NULL;',
'--:P8_TJ_RECAP := NULL;',
':P8_LOTE := NULL;',
':P8_MCD_NRO_AUT_REF := NULL;',
'--:P8_CRE_ID',
'--:P8_CRE_FECHA_DEPOSITO := SYSDATE;',
':P8_CRE_NRO_CHEQUE := NULL;',
':P8_CRE_NRO_CUENTA := NULL;',
':P8_CRE_NRO_PIN := NULL;',
':P8_MCD_NRO_COMPROBANTE := NULL;',
':P8_RAP_NRO_ESTABL_RET := NULL;',
':P8_RAP_NRO_PEMISION_RET := NULL;',
':P8_RAP_NRO_RETENCION := NULL;',
':P8_RAP_NRO_AUTORIZACION := NULL;',
':P8_RAP_FECHA := NULL;',
':P8_RAP_FECHA_VALIDEZ := NULL;',
':P8_RAP_COM_ID := NULL;',
':P8_SALDO_RETENCION := NULL;',
':P8_PRE_ID  := NULL;',
':P8_SEQ_ID := null;',
':P8_TITULAR_TC := null;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''limpia'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>65183311152771969
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(101477352095408042)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_busca_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_pagos_cuota. pr_busca_retencion_aplica(--pn_emp_id => :F_EMP_ID,',
'                          pn_rap_nro_establ_ret   => :P8_RAP_NRO_ESTABL_RET,',
'                          pn_rap_nro_pemision_ret => :P8_RAP_NRO_PEMISION_RET,',
'                          pn_rap_nro_retencion    => :P8_RAP_NRO_RETENCION,                                      ',
'                          pn_rap_nro_autorizacion => :P8_RAP_NRO_AUTORIZACION,',
'                          pd_rap_fecha            => :P8_RAP_FECHA,',
'                          pd_rap_fecha_validez    => :P8_RAP_FECHA_VALIDEZ,',
'                          pv_error                => :P0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>69224200825643116
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(27458190452964322104)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_alerta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'ln_contador NUMBER;',
'',
'BEGIN',
'',
'',
'SELECT COUNT(*)',
'INTO ln_contador ',
'FROM car_cuentas_por_cobrar ',
'WHERE ede_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_ede_id_recsa'')',
'AND cli_id = :P8_CLI_ID',
'AND cxc_saldo > 0;',
'',
'',
'IF ln_contador > 0 THEN',
'  ',
':P8_ALERTA := ''El cliente tiene creditos con el Tipo de Cartera RECSA, comuniquese con el Dep. Cartera'';',
'',
'else',
'',
'',
'',
':P8_ALERTA := null;',
'',
'END IF;',
'',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':P8_TTR_ID = pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_ing_anticipo_cli'') and :request = ''cliente'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>27425937301694557178
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(171623173564876308163)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_nombre_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT pe.per_nro_identificacion||'' - ''||pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'         pe.per_primer_apellido || '' '' || pe.per_segundo_apellido nombres',
'         INTO :P8_NOMBRE_1',
'    FROM asdm_personas pe',
'   WHERE pe.per_nro_identificacion = :P8_IDENTIFICACION_1',
'     AND pe.emp_id = :F_EMP_ID;',
'     ',
'     :P8_MCA_OBSERVACION:=:P8_NOMBRE_1;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'nombres'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>171590920413606543237
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117101764695612692)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  LN_TGR_ID asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'',
'--PARA LA AUTORIZACION',
'/*IF :P8_TTR_ID= pq_constantes.fn_retorna_constante(null ,''cn_ttr_id_egr_anticipo_cli'') ',
'AND :P8_AUTORIZACION != pq_ven_movimientos_caja.fn_claves_caja(:f_emp_id,:P8_TTR_ID,TO_NUMBER(:P8_IDENTIFICACION),:P8_TFP_ID,:P8_MCD_VALOR_MOVIMIENTO) THEN',
':P8_AUTORIZACION := NULL;',
'END IF;*/',
'',
'',
':P0_TTR_ID := :P8_TTR_ID;',
':P0_TTR_DESCRIPCION := NULL;',
':P0_DATFOLIO := NULL;',
':P0_FOL_SEC_ACTUAL := NULL;',
':P0_PERIODO := null;',
'',
'ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja_otros'');',
' pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                  :f_pca_id,',
'                                                   ln_tgr_id,',
'                                                  :p0_ttr_descripcion,',
'                                                  :p0_periodo,',
'                                                  :p0_datfolio,',
'                                                  :p0_fol_sec_actual,',
'                                                  :p0_pue_num_sri,',
'                                                  :p0_uge_num_est_sri,',
'                                                  :p0_pue_id,',
'                                                  :p0_ttr_id,',
'                                                  :p0_nro_folio,',
'                                                  :p0_error);',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>84848613425847766
);
wwv_flow_imp.component_end;
end;
/
