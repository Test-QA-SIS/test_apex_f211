prompt --application/pages/page_00163
begin
--   Manifest
--     PAGE: 00163
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>163
,p_name=>unistr('Detalle Promoci\00F3n Factura')
,p_page_mode=>'MODAL'
,p_step_title=>unistr('Detalle Promoci\00F3n Factura')
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_height=>'650'
,p_dialog_width=>'1500'
,p_page_component_map=>'18'
,p_last_updated_by=>'ETENESACA'
,p_last_upd_yyyymmddhh24miss=>'20230807083110'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(167432546776762455)
,p_plug_name=>'Promociones Detalle'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>90
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_1'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'  FROM (SELECT a.pre_id,',
'                a.emp_id,',
'                a.com_id,',
'                a.cde_id,',
'                x.ite_sec_id,',
'                a.ite_sku_id,',
'                x.ite_descripcion_larga,',
'                a.cde_id_padre,',
'                a.pre_principal,',
'                a.pre_poce,',
'                a.pre_costo,',
'                a.pre_costo_promedio,',
'                a.tpr_id,',
'                y.tpr_nombre,',
'                a.fpr_id,',
'                z.fpr_nombre,',
'                a.pre_mg_inicial,',
'                a.pre_tdesc_aut,',
'                a.pre_val_aut,',
'                a.usu_id_aut,',
'                a.usu_id_sol,',
'                a.dso_detalle,',
'                a.pre_tdesc_manual,',
'                a.pre_val_manual,',
'                a.pre_tdesc_promociones,',
'                a.pre_val_promociones,',
'                a.pre_abono,',
'                a.pre_desc_distribuido,',
'                a.pre_aplica_porcentaje,',
'                a.pre_nuevo_poce,',
'                a.pre_mg_final,',
'                a.pre_estado_registro,',
'                c.ite_sec_id            sec_id,',
'                c.ite_descripcion_larga descripcion,',
'                a.pet_id,',
'                o.pet_descripcion',
'           FROM ven_comprobantes_det    b,',
'                ven_promociones_resumen a,',
'                inv_items               c,',
'                inv_items               x,',
'                ven_tipos_promociones   y,',
'                ven_formas_promociones  z,',
'                ven_pro_etiquetas       o',
'          WHERE  b.com_id = :p163_com_id',
'       and   a.cde_id = b.cde_id',
'       AND c.ite_sku_id = b.ite_sku_id',
'       AND x.ite_sku_id = a.ite_sku_id',
'       AND y.tpr_id = a.tpr_id',
'       AND y.emp_id = a.emp_id',
'       AND y.tpr_estado_registro = 0',
'       AND z.fpr_id = a.fpr_id',
'       AND z.emp_id = a.emp_id',
'       AND z.fpr_estado_registro = 0',
'       AND o.pet_id(+) = a.pet_id)',
' ORDER BY cde_id;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(167432567992762456)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'ENARANJO'
,p_internal_uid=>135179416722997530
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167432749834762457)
,p_db_column_name=>'PRE_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Pre id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167432828086762458)
,p_db_column_name=>'EMP_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Emp id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167432950644762459)
,p_db_column_name=>'COM_ID'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167432966803762460)
,p_db_column_name=>'CDE_ID'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Cde id'
,p_column_type=>'NUMBER'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433070585762461)
,p_db_column_name=>'ITE_SKU_ID'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Ite sku id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433248380762462)
,p_db_column_name=>'ITE_DESCRIPCION_LARGA'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>unistr('Descripci\00F3n')
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433342384762463)
,p_db_column_name=>'CDE_ID_PADRE'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Cde id padre'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433380114762464)
,p_db_column_name=>'PRE_PRINCIPAL'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Principal'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433466882762465)
,p_db_column_name=>'PRE_POCE'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Poce'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433643040762466)
,p_db_column_name=>'PRE_COSTO'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Costo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433700809762467)
,p_db_column_name=>'TPR_ID'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Tpr id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433770673762468)
,p_db_column_name=>'TPR_NOMBRE'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'Nombre Promocion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433896044762469)
,p_db_column_name=>'FPR_ID'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'Fpr id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167433994530762470)
,p_db_column_name=>'FPR_NOMBRE'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'Tipo Promo'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167434113748762471)
,p_db_column_name=>'PRE_MG_INICIAL'
,p_display_order=>150
,p_column_identifier=>'O'
,p_column_label=>'% MG Inicial'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167434164701762472)
,p_db_column_name=>'PRE_TDESC_AUT'
,p_display_order=>160
,p_column_identifier=>'P'
,p_column_label=>'Tip Desc Aut'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167434256457762473)
,p_db_column_name=>'PRE_VAL_AUT'
,p_display_order=>170
,p_column_identifier=>'Q'
,p_column_label=>'Val Desc Aut'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167434399591762474)
,p_db_column_name=>'USU_ID_AUT'
,p_display_order=>180
,p_column_identifier=>'R'
,p_column_label=>'Usu Autoriza'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167434461960762475)
,p_db_column_name=>'USU_ID_SOL'
,p_display_order=>190
,p_column_identifier=>'S'
,p_column_label=>'Usu Solicita'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(167434650392762476)
,p_db_column_name=>'DSO_DETALLE'
,p_display_order=>200
,p_column_identifier=>'T'
,p_column_label=>'Det Manual'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662172418732327)
,p_db_column_name=>'PRE_TDESC_MANUAL'
,p_display_order=>210
,p_column_identifier=>'U'
,p_column_label=>'Tipo Desc Manual'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662314027732328)
,p_db_column_name=>'PRE_VAL_MANUAL'
,p_display_order=>220
,p_column_identifier=>'V'
,p_column_label=>'Val Desc Manual'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662446082732329)
,p_db_column_name=>'PRE_TDESC_PROMOCIONES'
,p_display_order=>230
,p_column_identifier=>'W'
,p_column_label=>'Tipo Desc Combo'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662513988732330)
,p_db_column_name=>'PRE_VAL_PROMOCIONES'
,p_display_order=>240
,p_column_identifier=>'X'
,p_column_label=>'Valor Desc Combo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662620175732331)
,p_db_column_name=>'PRE_ABONO'
,p_display_order=>250
,p_column_identifier=>'Y'
,p_column_label=>'Solo por'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662716010732332)
,p_db_column_name=>'PRE_DESC_DISTRIBUIDO'
,p_display_order=>260
,p_column_identifier=>'Z'
,p_column_label=>unistr('Distribuci\00F3n')
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662763284732333)
,p_db_column_name=>'PRE_APLICA_PORCENTAJE'
,p_display_order=>270
,p_column_identifier=>'AA'
,p_column_label=>unistr('% Distribuci\00F3n')
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180662940958732334)
,p_db_column_name=>'PRE_NUEVO_POCE'
,p_display_order=>280
,p_column_identifier=>'AB'
,p_column_label=>'Nuevo Poce'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180663030414732335)
,p_db_column_name=>'PRE_MG_FINAL'
,p_display_order=>290
,p_column_identifier=>'AC'
,p_column_label=>'% MG Final'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180663127684732336)
,p_db_column_name=>'PRE_ESTADO_REGISTRO'
,p_display_order=>300
,p_column_identifier=>'AD'
,p_column_label=>'Pre estado registro'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180663432355732339)
,p_db_column_name=>'ITE_SEC_ID'
,p_display_order=>310
,p_column_identifier=>'AG'
,p_column_label=>'Sec id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180663491302732340)
,p_db_column_name=>'SEC_ID'
,p_display_order=>320
,p_column_identifier=>'AH'
,p_column_label=>'Sec id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180663606081732341)
,p_db_column_name=>'DESCRIPCION'
,p_display_order=>330
,p_column_identifier=>'AI'
,p_column_label=>'Descripcion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180664412723732349)
,p_db_column_name=>'PRE_COSTO_PROMEDIO'
,p_display_order=>340
,p_column_identifier=>'AJ'
,p_column_label=>'Costo Promedio'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180664536647732350)
,p_db_column_name=>'PET_ID'
,p_display_order=>350
,p_column_identifier=>'AK'
,p_column_label=>'Pet id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(180664596515732351)
,p_db_column_name=>'PET_DESCRIPCION'
,p_display_order=>360
,p_column_identifier=>'AL'
,p_column_label=>'Promocion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(180677770307733073)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1484247'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'FPR_NOMBRE:PET_DESCRIPCION:TPR_NOMBRE:ITE_SEC_ID:ITE_DESCRIPCION_LARGA:PRE_PRINCIPAL:PRE_POCE:PRE_COSTO:PRE_COSTO_PROMEDIO:PRE_MG_INICIAL:PRE_VAL_AUT:PRE_VAL_MANUAL:PRE_VAL_PROMOCIONES:PRE_ABONO:PRE_APLICA_PORCENTAJE:PRE_DESC_DISTRIBUIDO:PRE_NUEVO_PO'
||'CE:PRE_MG_FINAL:SEC_ID:DESCRIPCION:'
,p_break_on=>'SEC_ID:DESCRIPCION:0:0:0:0'
,p_break_enabled_on=>'SEC_ID:DESCRIPCION:0:0:0:0'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(180663939344732344)
,p_plug_name=>'Parametros'
,p_plug_display_sequence=>100
,p_plug_display_point=>'BODY_1'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(180664331265732348)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(167432546776762455)
,p_button_name=>'BR_REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(180664006833732345)
,p_name=>'P163_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(180663939344732344)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(180664085537732346)
,p_name=>'P163_CDE_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(180663939344732344)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
