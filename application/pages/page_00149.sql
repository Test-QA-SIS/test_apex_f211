prompt --application/pages/page_00149
begin
--   Manifest
--     PAGE: 00149
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>149
,p_name=>unistr('Reporte Listado Facturas por Identificaci\00F3n')
,p_step_title=>unistr('Reporte Listado Facturas por Identificaci\00F3n')
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7637003541675840933)
,p_plug_name=>'<span style="font-size: 12pt;font-weight: bold">Consulta</span>'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7637082881891429097)
,p_plug_name=>'<span style="font-size: 12pt;font-weight: bold">Listado Facturas</span>'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>30
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT com.com_id,',
'       com.com_tipo tipo_comprobante,',
'       com.com_fecha fecha_venta,',
'       com.cli_id,',
'       nvl(TRIM(per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'                per.per_primer_nombre || '' '' || per.per_segundo_nombre),',
'           nvl(per.per_razon_social, ''SIN/NOMBRE'')) nombre_cliente,',
'       cde.ite_sku_id,',
'       (SELECT ite.ite_descripcion_larga',
'          FROM asdm_e.inv_items ite',
'         WHERE ite.ite_sku_id = cde.ite_sku_id) descripcion_item,',
'       (SELECT cei.cei_descripcion',
'          FROM asdm_e.inv_categorias_estado_item cei',
'         WHERE cei.cei_id = cde.cei_id',
'           AND cei.cei_estado_registro = 0) estado_item,',
'       (SELECT vco.vco_valor_variable',
'          FROM asdm_e.ven_var_comprobantes vco',
'         WHERE vco.com_id = com.com_id',
'           AND vco.var_id = 955',
'           AND vco.vco_estado_registro = 0) total_factura,',
'       (SELECT age.age_nombre_comercial',
'          FROM asdm_e.asdm_agencias age',
'         WHERE age.age_id = com.age_id_agencia',
'           AND age.age_estado_registro = 0) agencia',
'',
'  FROM asdm_e.ven_comprobantes     com,',
'       asdm_e.ven_comprobantes_det cde,',
'       asdm_e.asdm_clientes        cli,',
'       asdm_e.asdm_personas        per',
'',
' WHERE per.per_nro_identificacion = :P149_NRO_IDENTIFICACION',
'   AND com.com_id = cde.com_id',
'   AND com.emp_id = :F_EMP_ID',
'   AND com.com_estado_registro = 0',
'   AND cli.cli_id = com.cli_id',
'   AND cli.per_id = per.per_id',
'   AND cli.emp_id = :F_EMP_ID',
'   AND cli.cli_estado_registro = 0',
'   AND per.per_estado_registro = 0'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(7637083023130429097)
,p_name=>unistr('Reporte Listado Facturas por Identificaci\00F3n')
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:RP:P52_COM_ID,P52_REGRESAR_PAGINA,P52_VALIDA_CONS_CARTERA:#COM_ID#,0,'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="">'
,p_owner=>'PBRITO'
,p_internal_uid=>7604829871860664171
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637083445860429132)
,p_db_column_name=>'COM_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Com Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637083667049429141)
,p_db_column_name=>'TIPO_COMPROBANTE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Tipo Comprobante'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637084134149429142)
,p_db_column_name=>'FECHA_VENTA'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Fecha Venta'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637084472904429142)
,p_db_column_name=>'CLI_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Cli Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637084934999429142)
,p_db_column_name=>'NOMBRE_CLIENTE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Nombre Cliente'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637085290661429142)
,p_db_column_name=>'ITE_SKU_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Ite Sku Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637085653240429142)
,p_db_column_name=>'DESCRIPCION_ITEM'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Descripcion Item'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637086066781429146)
,p_db_column_name=>'ESTADO_ITEM'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Estado Item'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637086474271429146)
,p_db_column_name=>'TOTAL_FACTURA'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Total Factura'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7637086902867429148)
,p_db_column_name=>'AGENCIA'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Agencia'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(7637088695945520927)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'76048356'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'COM_ID:TIPO_COMPROBANTE:FECHA_VENTA:CLI_ID:NOMBRE_CLIENTE:ITE_SKU_ID:DESCRIPCION_ITEM:ESTADO_ITEM:TOTAL_FACTURA:AGENCIA'
,p_break_on=>'NOMBRE_CLIENTE:0:0:0:0:0'
,p_break_enabled_on=>'NOMBRE_CLIENTE:0:0:0:0:0'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(34612203665535567539)
,p_plug_name=>'New'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select pre.*',
'',
'  from asdm_e.ref_cobro_premios pre;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'NEVER'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(34612204774438567550)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'MCAGUANA'
,p_internal_uid=>34579951623168802624
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612204873325567551)
,p_db_column_name=>'CPR_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Cpr id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612204962017567552)
,p_db_column_name=>'ORD_ID_REFERIDO'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Ord id referido'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612205106346567553)
,p_db_column_name=>'ORD_ID_REFERIDOR'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Ord id referidor'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612205206961567554)
,p_db_column_name=>'COM_ID_REFERIDO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Com id referido'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612205265841567555)
,p_db_column_name=>'COM_ID_REFERIDOR'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Com id referidor'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612205377255567556)
,p_db_column_name=>'CPR_ESTADO_REGISTRO'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Cpr estado registro'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612205465390567557)
,p_db_column_name=>'CLI_ID_REFERIDOR'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Cli id referidor'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(34612205581388567558)
,p_db_column_name=>'EMP_ID'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Emp id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(41203914677287558271)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'411716616'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CPR_ID:ORD_ID_REFERIDO:ORD_ID_REFERIDOR:COM_ID_REFERIDO:COM_ID_REFERIDOR:CPR_ESTADO_REGISTRO:CLI_ID_REFERIDOR:EMP_ID'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(34612206083432567563)
,p_plug_name=>'pre'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>'select * from asdm_e.ref_premio_referido'
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'NEVER'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(34612206980604567572)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'MCAGUANA'
,p_internal_uid=>34579953829334802646
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572211785004660430)
,p_db_column_name=>'EMP_ID'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Emp id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214033556660452)
,p_db_column_name=>'PRE_ID'
,p_display_order=>90
,p_column_identifier=>'AD'
,p_column_label=>'Pre id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214128195660453)
,p_db_column_name=>'ITE_SKU_ID'
,p_display_order=>100
,p_column_identifier=>'AE'
,p_column_label=>'Ite sku id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214188549660454)
,p_db_column_name=>'CEI_ID'
,p_display_order=>110
,p_column_identifier=>'AF'
,p_column_label=>'Cei id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214264750660455)
,p_db_column_name=>'BPR_ID'
,p_display_order=>120
,p_column_identifier=>'AG'
,p_column_label=>'Bpr id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214411765660456)
,p_db_column_name=>'CPR_ID'
,p_display_order=>130
,p_column_identifier=>'AH'
,p_column_label=>'Cpr id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214530432660457)
,p_db_column_name=>'DIR_IR_REFERIDOR'
,p_display_order=>140
,p_column_identifier=>'AI'
,p_column_label=>'Dir ir referidor'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(41572214558063660458)
,p_db_column_name=>'PRE_ESTADO_REGISTRO'
,p_display_order=>150
,p_column_identifier=>'AJ'
,p_column_label=>'Pre estado registro'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(41572222075499660857)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'415399690'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'EMP_ID:PRE_ID:ITE_SKU_ID:CEI_ID:BPR_ID:CPR_ID:DIR_IR_REFERIDOR:PRE_ESTADO_REGISTRO'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7637003714758840935)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(7637003541675840933)
,p_button_name=>'CARGAR_INFORMACION'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>unistr('Cargar informaci\00F3n')
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7637003588154840934)
,p_name=>'P149_NRO_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(7637003541675840933)
,p_prompt=>unistr('N\00FAmero de Identificaci\00F3n')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pe.per_nro_identificacion ||''-''|| nvl(pe.per_razon_social,trim(pe.per_primer_apellido || '' '' || pe.per_segundo_apellido ||'' ''|| pe.per_primer_nombre || '' '' || pe.per_segundo_nombre)) d,',
'       pe.per_nro_identificacion c',
'  FROM asdm_clientes cl, asdm_personas pe',
' WHERE cl.per_id = pe.per_id',
'   AND cl.emp_id = :F_EMP_ID'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp.component_end;
end;
/
