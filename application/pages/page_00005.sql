prompt --application/pages/page_00005
begin
--   Manifest
--     PAGE: 00005
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>5
,p_name=>unistr('Postergaci\00BF\00BFn de Cheques')
,p_step_title=>unistr('Postergaci\00BF\00BFn de Cheques')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112520'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(104427655984811534)
,p_plug_name=>'Datos Cheque'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>3
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P5_CRE_ID'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(104427854635811540)
,p_plug_name=>'Error'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P5_PV_ERROR'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(104428078205811541)
,p_plug_name=>'Cambio fecha Cheques'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>2
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  ''Posterga'',',
'        cre.cre_id,',
'        cre.cli_id,',
'      cre.cre_nro_cuenta,',
'      cre.cre_nro_pin,',
'        cre.cre_nro_cheque,',
'        ede.ede_descripcion,',
'        cre.cre_titular_cuenta,',
'        cre.cre_valor,',
'        cre.cre_fecha_deposito,',
'        cre.cre_observaciones,',
'        ''historial''',
'FROM   asdm_cheques_recibidos cre,',
'       asdm_entidades_destinos ede,',
'       asdm_transacciones trx',
'WHERE  cre.emp_id = :f_emp_id',
'       AND cre.cre_estado_registro = pq_constantes.fn_retorna_constante(0,''cv_estado_reg_activo'')',
'       AND cre.cli_id = :P5_CLI_ID',
' and cre.ede_id = ede.ede_id',
'      AND cre.ech_id in ( pq_constantes.fn_retorna_constante(0,''cn_ech_id_ingresado''),',
'pq_constantes.fn_retorna_constante(0,''cn_ech_id_postergado''))',
'       AND cre_tipo =pq_constantes.fn_retorna_constante(0,''cv_tch_posfechado'')',
'       AND  trx.trx_id=cre.trx_id',
'       AND trx.uge_id=:f_uge_id',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(104428267854811543)
,p_name=>'Cheques por cliente'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#ws/small_page.gif" alt="" />'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>72175116585046617
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104989378127920771)
,p_db_column_name=>'''POSTERGA'''
,p_display_order=>1
,p_column_identifier=>'G'
,p_column_label=>unistr('Postergaci\00BF\00BFn')
,p_column_link=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.::P5_CRE_ID,P5_CRE_FECHA_DEPOSITO,P5_CRE_NRO_CHEQUE:#CRE_ID#,#CRE_FECHA_DEPOSITO#,#CRE_NRO_CHEQUE#'
,p_column_linktext=>'Postergar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'''POSTERGA'''
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104428375741811547)
,p_db_column_name=>'CRE_NRO_CHEQUE'
,p_display_order=>3
,p_column_identifier=>'A'
,p_column_label=>'Nro<br>Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104428469038811549)
,p_db_column_name=>'CRE_TITULAR_CUENTA'
,p_display_order=>4
,p_column_identifier=>'B'
,p_column_label=>'Titular<br>Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_TITULAR_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104428560003811549)
,p_db_column_name=>'CRE_VALOR'
,p_display_order=>5
,p_column_identifier=>'C'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104428658961811549)
,p_db_column_name=>'CRE_FECHA_DEPOSITO'
,p_display_order=>6
,p_column_identifier=>'D'
,p_column_label=>'Fecha<br>Deposito'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(104428864392811551)
,p_db_column_name=>'CRE_OBSERVACIONES'
,p_display_order=>7
,p_column_identifier=>'F'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(105009552866414861)
,p_db_column_name=>'CRE_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Cre Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(105662776493320325)
,p_db_column_name=>'EDE_DESCRIPCION'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'EDE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(105679082399473393)
,p_db_column_name=>'''HISTORIAL'''
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Historial'
,p_column_link=>'f?p=&APP_ID.:10:&SESSION.::&DEBUG.::P10_CRE_NRO_CHEQUE,P10_CLI_ID,P10_NUM_CUENTA,P10_PIN:#CRE_NRO_CHEQUE#,#CLI_ID#,#CRE_NRO_CUENTA#,#CRE_NRO_PIN#'
,p_column_linktext=>'Historial'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'''HISTORIAL'''
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77639979740940882)
,p_db_column_name=>'CLI_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77640070989940882)
,p_db_column_name=>'CRE_NRO_CUENTA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Nro.<br>Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77640151695940883)
,p_db_column_name=>'CRE_NRO_PIN'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Nro Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(104428979718811552)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1276008055919693'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'''POSTERGA'':CRE_NRO_CHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_VALOR:CRE_FECHA_DEPOSITO:CRE_OBSERVACIONES:EDE_DESCRIPCION:''HISTORIAL'''
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(104429051333811556)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(104427655984811534)
,p_button_name=>'Grabar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(104431374383811571)
,p_branch_action=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>4
,p_branch_comment=>'Created 03-DIC-2009 16:15 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(104431555649811573)
,p_branch_action=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:5::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(104429051333811556)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104429279006811560)
,p_name=>'P5_CRE_NRO_CHEQUE'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(104427655984811534)
,p_prompt=>'Nro Cheque'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104429471806811562)
,p_name=>'P5_CRE_OBSERVACIONES'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(104427655984811534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>36
,p_cMaxlength=>2000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104429680114811562)
,p_name=>'P5_PV_ERROR'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(104427854635811540)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104429859709811563)
,p_name=>'P5_CRE_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(104428078205811541)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104430078459811564)
,p_name=>'P5_CRE_FECHA_DEPOSITO'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(104427655984811534)
,p_prompt=>unistr('Fecha actual dep\00BF\00BFsito')
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104430275817811564)
,p_name=>'P5_CLI_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(104428078205811541)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cli Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT cli.cli_id',
'FROM   v_asdm_datos_clientes cli',
'WHERE  cli.per_nro_identificacion = :P5_PER_NRO_IDENTIFICACION',
'and cli.emp_id = :f_emp_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104430469927811566)
,p_name=>'P5_PER_NOMBRE'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(104428078205811541)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cliente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select per.nombre_completo  from v_asdm_clientes_personas per',
'where per.per_nro_identificacion = :p5_per_nro_identificacion',
'AND PER.EMP_ID = :F_EMP_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104430656634811566)
,p_name=>'P5_PER_NRO_IDENTIFICACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(104428078205811541)
,p_prompt=>'Nro Identificacion Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_CHEQUES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas_caja.fn_lov_cliente_con_cheques(:f_emp_id,:f_uge_id, :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cSize=>50
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit()"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(104430865696811567)
,p_name=>'P5_NUEVAFECHA'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(104427655984811534)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nueva Fecha'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(104964576481551294)
,p_validation_name=>'nueva fecha'
,p_validation_sequence=>10
,p_validation=>'to_date(sysdate,''DD-MM-YYYY'') <= to_date(:P5_NUEVAFECHA,''DD-MM-YYYY'')'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('La fecha de dep\00BF\00BFsito no puede ser menor a hoy')
,p_when_button_pressed=>wwv_flow_imp.id(104429051333811556)
,p_associated_item=>wwv_flow_imp.id(104430865696811567)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(104431066436811568)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Graba_Posterga_Cheques'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p5_pv_error := null;',
'',
'pq_ven_movimientos_caja.pr_posterga_anula_cheques(:P5_CRE_ID,              ',
'                          :P5_NUEVAFECHA,',
'                          :P5_CRE_OBSERVACIONES, ',
'                          :F_EMP_ID, ',
'                          :F_UGE_ID,:F_UGE_ID_GASTO,               ',
'                          :F_USER_ID, ',
'                          pq_constantes.fn_retorna_constante(0,''cn_ttr_id_postergar_cheques''),',
'                          pq_constantes.fn_retorna_constante(0,''cn_ech_id_postergado''),',
'                          :P5_PV_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(104429051333811556)
,p_process_success_message=>'Datos guardados correctamente'
,p_internal_uid=>72177915167046642
);
wwv_flow_imp.component_end;
end;
/
