prompt --application/pages/page_00085
begin
--   Manifest
--     PAGE: 00085
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>85
,p_name=>'DEPOSITOS REALES'
,p_alias=>'DEP_REAL_EFEC_CHEQUE'
,p_step_title=>'DEPOSITOS REALES'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240129104923'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(131180974629214364)
,p_name=>'Depositos Transitorios Diarios'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''DEPOSITO TRANSITORIO '' DEPOSITO,',
'       to_CHAR(MCA.MCA_FECHA, ''DD-MM-YYYY'') FECHA,',
'       SUM(MCD.MCD_VALOR_MOVIMIENTO) VALOR,',
'       mca.pca_id',
'  from asdm_movimientos_caja         mca,',
'       asdm_movimientos_caja_detalle mcd,',
'       asdm_transacciones            trx,',
'       asdm_tipos_transacciones      ttr,',
'       ven_periodos_caja             pca,',
'       asdm_puntos_emision           pue',
' where ttr.ttr_id = trx.ttr_id',
'   and trx.trx_id = mca.trx_id',
'   and mcd.mca_id = mca.mca_id',
'   and pca.pca_id = mca.pca_id',
'   and pca.emp_id = mca.emp_id',
'   and pue.pue_id = pca.pue_id',
'   and pue.uge_id = trx.uge_id',
'   and pue.emp_id = mca.emp_id',
'   and trx.uge_id = :f_uge_id',
'   and mca.emp_id = :f_emp_id',
'   and pue.pue_id = :p0_pue_id',
'   and mcd.tfp_id != :P85_TFP_TARJETA',
'   and trx.ttr_id IN (:P85_TTR_CIERRE_CAJA,:P85_TTR_EGR_CAJA)',
'   and not exists',
' (select drd.mca_id',
'          from ven_depositos_reales dre, ven_depositos_reales_det drd',
'         where drd.dre_id = dre.dre_id',
'           and drd.mca_id = mca.mca_id)',
' group by to_CHAR(MCA.MCA_FECHA, ''DD-MM-YYYY''), mca.pca_id',
' ORDER BY FECHA'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131182477876224712)
,p_query_column_id=>1
,p_column_alias=>'DEPOSITO'
,p_column_display_sequence=>1
,p_column_heading=>'Deposito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131181254450214607)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:85:&SESSION.:DEPDIA:&DEBUG.::P85_PCA_ID,P85_FECHA:#PCA_ID#,#FECHA#'
,p_column_linktext=>'#FECHA#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131181375267214627)
,p_query_column_id=>3
,p_column_alias=>'VALOR'
,p_column_display_sequence=>3
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131181452512214627)
,p_query_column_id=>4
,p_column_alias=>'PCA_ID'
,p_column_display_sequence=>4
,p_column_heading=>'PERIODO CAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(131183070782260539)
,p_name=>'Detalle Deposito Diario'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select MCA.MCA_ID,',
'       to_CHAR(MCA.MCA_FECHA, ''DD-MM-YYYY'') FECHA,',
'       mca.mca_observacion OBSERVACION,',
'       sum(MCD.MCD_VALOR_MOVIMIENTO) VALOR_DEPOSITO, mca.pca_id,',
'       APEX_ITEM.HIDDEN(11, MCA.MCA_ID) DEPOSITO',
'  from asdm_movimientos_caja         mca,',
'       asdm_movimientos_caja_detalle mcd,',
'       asdm_transacciones            trx,',
'       asdm_tipos_transacciones      ttr,',
'       ven_periodos_caja             pca,',
'       asdm_puntos_emision           pue',
' where ttr.ttr_id = trx.ttr_id',
'   and trx.trx_id = mca.trx_id',
'   and mcd.mca_id = mca.mca_id',
'   and pca.pca_id = mca.pca_id',
'   and pca.emp_id = mca.emp_id',
'   and pue.pue_id = pca.pue_id',
'   and pue.uge_id = trx.uge_id',
'   and pue.emp_id = mca.emp_id',
'   and trx.uge_id = :f_uge_Id',
'   and mca.emp_id = :f_emp_id',
'   and pue.pue_id = :p0_pue_id',
'   and mca.pca_id = :p85_pca_id',
'   and to_DATE(MCA.MCA_FECHA, ''DD-MM-YYYY'') = to_DATE(:p85_fecha, ''DD-MM-YYYY'')',
'   and mcd.tfp_id != pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito'')',
'   and trx.ttr_id IN',
'       (pq_constantes.fn_retorna_constante(NULL,',
'                                           ''cn_ttr_id_egr_caj_dep_tran''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_cierre_caja''))',
'   and not exists',
' (select drd.mca_id',
'          from ven_depositos_reales dre, ven_depositos_reales_det drd',
'         where drd.dre_id = dre.dre_id',
'           and drd.mca_id = mca.mca_id)',
'   AND MCA.MCA_ID NOT IN(',
'   SELECT TO_NUMBER(COL.C001) MCA_ID',
'FROM APEX_COLLECTIONS COL',
'WHERE COL.COLLECTION_NAME = ''COLL_DEP_TRA_REAL'')',
' GROUP BY MCA.MCA_ID, to_CHAR(MCA.MCA_FECHA, ''DD-MM-YYYY''),mca.mca_observacion,',
'       mca.pca_id',
' ORDER BY FECHA           '))
,p_display_when_condition=>'P85_PCA_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131183353119260557)
,p_query_column_id=>1
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'MCA_ID'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131183475328260569)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131343375003158323)
,p_query_column_id=>3
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>3
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131183570041260569)
,p_query_column_id=>4
,p_column_alias=>'VALOR_DEPOSITO'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR_DEPOSITO'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131183658525260569)
,p_query_column_id=>5
,p_column_alias=>'PCA_ID'
,p_column_display_sequence=>5
,p_column_heading=>'PERIODO CAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131198377043489421)
,p_query_column_id=>6
,p_column_alias=>'DEPOSITO'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_default=>'1'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(131211180478613395)
,p_name=>'Depositos Reales'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MCA.MCA_ID, MCA.MCA_FECHA, MCA.MCA_OBSERVACION, ''X'' ELIMINAR, MCA.MCA_TOTAL',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_display_when_condition=>'nvl(:P85_existe,0) > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131370159409428247)
,p_query_column_id=>1
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>2
,p_column_heading=>'MCA ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131370269849428255)
,p_query_column_id=>2
,p_column_alias=>'MCA_FECHA'
,p_column_display_sequence=>1
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131370364913428255)
,p_query_column_id=>3
,p_column_alias=>'MCA_OBSERVACION'
,p_column_display_sequence=>3
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131406767234695330)
,p_query_column_id=>4
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>5
,p_column_heading=>'ELIMINAR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:85:&SESSION.:ELIMINA:&DEBUG.::P85_MCA_ID_ELIMINAR:#MCA_ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131417081787775288)
,p_query_column_id=>5
,p_column_alias=>'MCA_TOTAL'
,p_column_display_sequence=>4
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(131386251585511134)
,p_plug_name=>'Ingreso Papeletas'
,p_parent_plug_id=>wwv_flow_imp.id(131211180478613395)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(131869273661636652)
,p_name=>'detalle depositos reales'
,p_parent_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select (SELECT sys_connect_by_path(upper(regexp_replace(ede.ede_descripcion, ''[^A-Za-z0-9]'', '''')), '' -> '') DETALLE',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_tipo =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_tede_cuenta_bancaria'')',
'           and ede.ede_estado_registro =',
'               pq_constantes.fn_retorna_constante(null,',
'                                                  ''cv_estado_reg_activo'')',
'           and ede.emp_id = :F_EMP_ID',
'           and ede.ede_id = C003',
'         START WITH ede.ede_id_padre IS NULL',
'        CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre) Entidad,',
'       C004 FECHA_PAPELETA,',
'       C005 NUMERO_PAPELETA,',
'       to_number(C006) VALOR',
'  from apex_collections',
' where collection_name = ''COLL_DEP_REAL'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131893280596714374)
,p_query_column_id=>1
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>1
,p_column_heading=>'ENTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131893353977714374)
,p_query_column_id=>2
,p_column_alias=>'FECHA_PAPELETA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA PAPELETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131893473699714374)
,p_query_column_id=>3
,p_column_alias=>'NUMERO_PAPELETA'
,p_column_display_sequence=>3
,p_column_heading=>'NUMERO PAPELETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(131893567265714395)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(131930380576913039)
,p_plug_name=>'datos deposito real'
,p_parent_plug_id=>wwv_flow_imp.id(131869273661636652)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133630058617426233)
,p_plug_name=>'Depositos de Varios Dias con una sola Papeleta'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(12370002968737781510)
,p_name=>'preubas'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT *',
'        FROM APEX_COLLECTIONS COL',
'       WHERE COL.collection_name = ''COLL_DEP_REAL''',
'         AND to_number(COL.c002) = :f_uge_id--pn_uge_id',
'         AND to_number(COL.c001) = 1--pn_emp_id'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370049882200785232)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052374657785354)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052455647785354)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052581381785354)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052672291785354)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052752412785354)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052865212785354)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370052975393785354)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053057745785354)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053154437785354)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053279102785354)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053353039785354)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053466249785354)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053573337785354)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053655337785354)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053765531785354)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053873427785355)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370053952488785355)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054064636785355)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054178522785355)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054261338785355)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054355433785355)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054454029785355)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054561260785355)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054668584785355)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054756447785355)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054878758785355)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370054956880785355)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055058974785355)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055176262785355)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055283935785355)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055481231785369)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055574675785369)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055659811785369)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055770524785369)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055874497785369)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370055974798785369)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056056107785369)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056154792785370)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056280740785370)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056356421785372)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056464370785372)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056565971785372)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056660383785372)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056755607785372)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056873750785372)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370056982793785372)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057067826785372)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057168360785372)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057253964785372)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057355349785372)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057468160785372)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057578908785372)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057663755785372)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057776757785372)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057859614785373)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370057971414785373)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058070648785373)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058164669785373)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058370622785374)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058481426785374)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058574898785374)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058665681785374)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058775543785374)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058864865785374)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(12370058965567785374)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1430837014436812877)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(131180974629214364)
,p_button_name=>'bt_regresar'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_alignment=>'LEFT-CENTER'
,p_button_redirect_url=>'f?p=&APP_ID.:37:&SESSION.::&DEBUG.:::'
,p_grid_new_row=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(131205182939566836)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(131183070782260539)
,p_button_name=>'CARGA_DT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(131672868279821498)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_button_name=>'CARGA_DR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(132084651572602562)
,p_button_sequence=>35
,p_button_plug_id=>wwv_flow_imp.id(131930380576913039)
,p_button_name=>'CANCELAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(132006365378228131)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(131930380576913039)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select count(*)',
'from apex_collections where collection_name = ''COLL_DEP_REAL''',
'and to_number(c001) = :f_emp_id',
'and to_number(c002) = :f_uge_id) > 0'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133638275979459551)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(133630058617426233)
,p_button_name=>'INGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Ingresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:87:&SESSION.:INGRESAR:&DEBUG.:87::'
,p_button_condition=>':p87_clave = :p87_calculo'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(132339261126620451)
,p_branch_action=>'f?p=&APP_ID.:85:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(132006365378228131)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 18-JAN-2012 15:25 by ACALLE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(131348965484193395)
,p_branch_action=>'f?p=&APP_ID.:85:&SESSION.::&DEBUG.:::'
,p_branch_point=>'BEFORE_HEADER'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 17-JAN-2012 10:27 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131183882556263919)
,p_name=>'P85_PCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(131183070782260539)
,p_prompt=>'Pca Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131184554290265221)
,p_name=>'P85_FECHA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(131183070782260539)
,p_prompt=>'FECHA'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131386682684511181)
,p_name=>'P85_VALOR_DEPOSITO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_prompt=>'Valor Deposito'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>unistr('onchange="javascript:patronRe = /^\005Cd+\005C.?\005Cd*$/;if (!patronRe.test(this.value)) {alert(''Debe Ingresar un valor v\00BF\00BFlido'');this.value = '''';{html_GetElement(''P85_VALOR_DEPOSITO'').focus();}}" ')
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P85_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131386869288511186)
,p_name=>'P85_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_prompt=>'Fecha Papeleta'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P85_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131387082536511186)
,p_name=>'P85_NUMERO_PAPELETA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_prompt=>'Numero Papeleta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P85_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
,p_item_comment=>'onChange="doSubmit(''PAPELETA'')"'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131387265234511189)
,p_name=>'P85_CUENTA_DEPOSITO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_prompt=>'Cuenta Deposito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion d,',
'       c.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  A.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id = :f_uge_id',
'       AND d.ede_id = :p85_banco_deposito   AND A.UCB_ESTADO_REGISTRO =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'ORDER  BY a.ucb_prioridad'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccione Cuenta---'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P85_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131387460377511199)
,p_name=>'P85_BANCO_DEPOSITO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(131386251585511134)
,p_prompt=>'Banco Dep&oacute;sito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENT_DESTINO_X_UGE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccion Banco---'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131409171867706151)
,p_name=>'P85_MCA_ID_ELIMINAR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(131211180478613395)
,p_prompt=>'Mca Id Eliminar'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131422661748797894)
,p_name=>'P85_VALOR_DESCUADRE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(131930380576913039)
,p_use_cache_before_default=>'NO'
,p_prompt=>'DESCUADRE'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(MCA.MCA_TOTAL),0) - :P85_TOTAL_PAPELETA',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'CARGA_DT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131452860072901452)
,p_name=>'P85_OBSERVACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(131930380576913039)
,p_prompt=>'MOTIVO DESCUADRE'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>45
,p_cMaxlength=>100
,p_cHeight=>2
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P85_VALOR_DESCUADRE != 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(131938963570955392)
,p_name=>'P85_TOTAL_PAPELETA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(131930380576913039)
,p_use_cache_before_default=>'NO'
,p_prompt=>'TOTAL DEPOSITO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select nvl(SUM(to_number(c006)),0)',
'from apex_collections ',
'where collection_name = ''COLL_DEP_REAL'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132010476376250235)
,p_name=>'P85_EXISTE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(131183070782260539)
,p_use_cache_before_default=>'NO'
,p_prompt=>'EXISTE'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT count(*)',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133634259440435897)
,p_name=>'P85_CLAVE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133630058617426233)
,p_use_cache_before_default=>'NO'
,p_source=>'INGRESAR VARIOS DEPOSITOS TRANSITORIOS DE VARIOS DIAS. ES NECESARIO CLAVE DE AUTORIZACION'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1430836620094812873)
,p_name=>'P85_TFP_TARJETA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(131180974629214364)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1430836739311812874)
,p_name=>'P85_TTR_EGR_CAJA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(131180974629214364)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1430836832175812875)
,p_name=>'P85_TTR_CIERRE_CAJA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(131180974629214364)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(131610063515801190)
,p_validation_name=>'P85_OBSERVACION'
,p_validation_sequence=>10
,p_validation=>'P85_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el motivo del descuadre'
,p_validation_condition=>':P85_VALOR_DESCUADRE != 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(132006365378228131)
,p_associated_item=>wwv_flow_imp.id(131452860072901452)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(131891074316684141)
,p_validation_name=>'P85_BANCO_DEPOSITO'
,p_validation_sequence=>20
,p_validation=>'P85_BANCO_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Banco'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(131387460377511199)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(131891758517689106)
,p_validation_name=>'P85_CUENTA_DEPOSITO'
,p_validation_sequence=>40
,p_validation=>'P85_CUENTA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Cuenta'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(131387265234511189)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(131891966829691439)
,p_validation_name=>'P85_NUMERO_PAPELETA'
,p_validation_sequence=>50
,p_validation=>'P85_NUMERO_PAPELETA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Papeleta'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(131387082536511186)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(131892376179694166)
,p_validation_name=>'P85_VALOR_DEPOSITO'
,p_validation_sequence=>60
,p_validation=>'P85_VALOR_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Valor'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(131386682684511181)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(141048668796008606)
,p_validation_name=>'P85_NUMERO_PAPELETA'
,p_validation_sequence=>70
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if pq_ven_movimientos_caja.fn_papeletas_ingresadas_1(:f_emp_id,:P85_NUMERO_PAPELETA) IS NULL',
' then',
'return null;',
'else ',
':P85_NUMERO_PAPELETA := null;',
'return ''YA EXISTE UNA PAPELETA INGRESADA CON ESE NUMERO'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>'CARGA_DR'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(131387082536511186)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52945270718473863)
,p_validation_name=>'P85_FECHA_DEPOSITO'
,p_validation_sequence=>80
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if /*to_date(:P85_FECHA_DEPOSITO,''dd-mm-yyyy'') < to_date(:P85_FECHA,''dd-mm-yyyy'') or */to_date(:P85_FECHA_DEPOSITO,''dd-mm-yyyy'') > trunc(SYSDATE) OR  :P85_FECHA_DEPOSITO IS NULL then',
'   return ''LA FECHA DE LA PAPELETA NO PUEDE SER MENOR A LA DEL CIERRE O MAYOR A LA FECHA ACTUAL'';',
'else',
'   return null;',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'ERROR'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(131386869288511186)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(131210464201608702)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_dep_tran'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_depositos_reales.pr_col_dep_tra_real(pv_error  => :p0_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(131205182939566836)
,p_internal_uid=>98957312931843776
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(131835272735494462)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_dep_real'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_depositos_reales.pr_col_dep_real(pn_emp_id         => :f_emp_id,',
'                pn_uge_id         => :f_uge_id,',
'                pn_ede_id         => :p85_cuenta_deposito,',
'                pn_fecha_papeleta => :p85_fecha_deposito,',
'                pn_num_papeleta   => :p85_numero_papeleta,',
'                pn_valor_papeleta => :p85_valor_deposito,',
'                pv_error          => :p0_error);',
':P85_BANCO_DEPOSITO := NULL;',
':P85_FECHA_DEPOSITO := NULL;',
':P85_CUENTA_DEPOSITO := NULL;',
':P85_NUMERO_PAPELETA := NULL;',
':P85_VALOR_DEPOSITO := NULL;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(131672868279821498)
,p_process_when=>'CARGA_DR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>99582121465729536
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(131346860505182508)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'    END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'    END IF;',
'',
'',
':P85_BANCO_DEPOSITO := NULL;',
':P85_FECHA_DEPOSITO := NULL;',
':P85_CUENTA_DEPOSITO := NULL;',
':P85_NUMERO_PAPELETA := NULL;',
':P85_VALOR_DEPOSITO := NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'DEPDIA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>99093709235417582
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(131412263602732120)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_quita_seleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_seq_id number;',
'begin',
'',
'select to_number(seq_id)',
'into ln_seq_id',
'from apex_collections WHERE COLLECTION_NAME = ''COLL_DEP_TRA_REAL''',
'AND c001 = :P85_MCA_ID_ELIMINAR;',
'',
'apex_collection.delete_member(p_collection_name => ''COLL_DEP_TRA_REAL'',p_seq => ln_seq_id);',
'',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'    END IF;',
':P85_BANCO_DEPOSITO := NULL;',
':P85_FECHA_DEPOSITO := NULL;',
':P85_CUENTA_DEPOSITO := NULL;',
':P85_NUMERO_PAPELETA := NULL;',
':P85_VALOR_DEPOSITO := NULL;',
'',
'EXCEPTION WHEN NO_DATA_FOUND THEN',
'NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>99159112332967194
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(132338580388616547)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_deposito_real'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'',
'pq_ven_depositos_reales.pr_deposito_real_efe_che(pn_emp_id              => :f_emp_id,',
'                         pn_uge_id              => :f_uge_id,',
'                         pn_user_id             => :f_user_id,',
'                         pn_uge_id_gasto        => :f_uge_id_gasto,',
'                         pv_dre_razon_descuadre => :p85_observacion,',
'                         pn_dre_valor_descuadre => :p85_valor_descuadre,',
'                         pn_pca_id              => :P85_PCA_ID,--:f_pca_id,',
'                         pv_error               => :p0_error);',
'if :p0_error is null then',
'',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'END IF;',
'',
'',
':P85_BANCO_DEPOSITO := NULL;',
':P85_FECHA_DEPOSITO := NULL;',
':P85_CUENTA_DEPOSITO := NULL;',
':P85_NUMERO_PAPELETA := NULL;',
':P85_VALOR_DEPOSITO := NULL;',
':P85_PCA_ID := NULL;',
':P85_existe := NULL;',
':P85_OBSERVACION := NULL;',
'',
'',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>100085429118851621
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1430836949853812876)
,p_process_sequence=>40
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                            ''cn_tfp_id_tarjeta_credito'')',
'    INTO :p85_tfp_tarjeta',
'    FROM dual;',
'',
'  SELECT pq_constantes.fn_retorna_constante(NULL,',
'                                            ''cn_ttr_id_egr_caj_dep_tran'')',
'    INTO :p85_ttr_egr_caja',
'    FROM dual;',
'',
'  SELECT pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_cierre_caja'')',
'    INTO :p85_ttr_cierre_caja',
'    FROM dual;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P85_TFP_TARJETA is null or :P85_TTR_CIERRE_CAJA is null or :P85_TTR_EGR_CAJA is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>1414710374218930413
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(132085477545610103)
,p_process_sequence=>30
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cancelar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'END IF;',
'',
'',
':P85_BANCO_DEPOSITO := NULL;',
':P85_FECHA_DEPOSITO := NULL;',
':P85_CUENTA_DEPOSITO := NULL;',
':P85_NUMERO_PAPELETA := NULL;',
':P85_VALOR_DEPOSITO := NULL;',
':P85_PCA_ID := NULL;',
':P85_existe := NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CANCELAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>99832326275845177
);
wwv_flow_imp.component_end;
end;
/
