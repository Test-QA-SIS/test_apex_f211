prompt --application/pages/page_00007
begin
--   Manifest
--     PAGE: 00007
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>7
,p_name=>'PAGO_CUOTA'
,p_step_title=>'PAGO_CUOTA'
,p_autocomplete_on_off=>'OFF'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript1.1" type="text/javascript">',
'',
'function carga_cuotas(div_id)',
'{    ',
'    var ajaxResult= new htmldb_Get(null,$x(''pFlowId'').value,''APPLICATION_PROCESS=PR_CARGA_CUOTA'',0);',
'        ajaxResult.add(''P1'',div_id);',
'    ',
'    var v_result_xml = ajaxResult.get(); ',
'    var error_plsql = v_result_xml.indexOf(''Error'');   ',
'',
'',
'    if (error_plsql > 0 ) {',
'        alert(v_result_xml);',
'    }',
'    ',
'    //jQuery(''#R38454175733736091514_ir'').data(''apex-interactiveReport'').refresh ();    ',
'    //apex.jQuery(''#R38419303825032878029'').trigger(''apexrefresh'');',
'    //apex.jQuery(''#R44785275878003620109'').trigger(''apexrefresh'');   ',
'    $(''#cartera'').trigger(''apexrefresh'');',
'    $(''#dividendos'').trigger(''apexrefresh'');',
'    $(''#totales'').trigger(''apexrefresh'');',
'    ',
'    //apex.jQuery(''#''+id_grilla2).trigger(''apexrefresh'');',
'} ',
'    ',
'function baja_cuotas(div_id)',
'{    ',
'    var ajaxResult= new htmldb_Get(null,$x(''pFlowId'').value,''APPLICATION_PROCESS=PR_BAJA_CUOTA'',0);',
'        ajaxResult.add(''P1'',div_id);',
'    ',
'    var v_result_xml = ajaxResult.get(); ',
'    var error_plsql = v_result_xml.indexOf(''Error'');   ',
'',
'',
'    if (error_plsql > 0 ) {',
'        alert(v_result_xml);',
'    }',
'    ',
'    //jQuery(''#R38454175733736091514_ir'').data(''apex-interactiveReport'').refresh ();    ',
'    //apex.jQuery(''#R38419303825032878029'').trigger(''apexrefresh'');',
'    //apex.jQuery(''#R44785275878003620109'').trigger(''apexrefresh'');   ',
'    $(''#cartera'').trigger(''apexrefresh'');',
'    $(''#dividendos'').trigger(''apexrefresh'');',
'    $(''#totales'').trigger(''apexrefresh''); ',
'    //apex.jQuery(''#''+id_grilla2).trigger(''apexrefresh'');',
'}',
'    ',
'</script>',
'',
''))
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function open_dialog(){',
'        $(''#com'').dialog(''open''); ',
'       //apex.region(''TIPO_FACTORES'').refresh();     ',
'       }',
' ',
'function close_dialog(){',
'        $(''#com'').dialog(''close''); ',
'       }',
'       ',
'',
'function valida_accion(){',
'    ',
'        var proveedor = $v("P7_PROVEEDOR").toUpperCase();     ',
'        if (proveedor == ''KNOX''){',
'            var pageId =  $v("pFlowId");',
'            var targetPageId = 165;',
'            var pitId = $v("P7_PIT_ID");',
'            var pitIde =  $v("P7_PIT_IDE1");',
'            var session =  $v(''pInstance''); ',
'            ',
'            var sessionInstance = $v(''pInstance'');',
'            ',
'            //var dialogUrl = "f?p=" + pageId + ":" + targetPageId + ":"+$v("APP_SESSION")+""::NO:RP," + targetPageId + ":P165_PIT_ID,P165_PIT_IDE1:" + pitId + "," + pitIde+"\u0026p_dialog_cs=x8nQb77mEpsWa4ugaPSM-j7TA8edhQyYSookYC2YXsIyQyoPP6SCZ8PLCi'
||'0d1y2nDg1_ygjy1SfQb13-SUXaFw''";',
'            //var dialogUrl = "f?p=" + pageId + ":" + targetPageId + ":"+session+"::NO:RP," + targetPageId + ":P165_PIT_ID,P165_PIT_IDE1:" + pitId + "," + pitIde+"&p_dialog_cs=o6Cju7fdNJK8AjuPEdqtk_WZCZi1whZv_J96trvMG8hgFRI-bfQWrM1rKxYcZlanqh6y3Pi5zH'
||'d7OfKKFXTCnA";',
'            var dialogUrl = "f?p=" + pageId + ":" + targetPageId + ":"+session+"::NO:RP," + targetPageId + ":P165_PIT_ID,P165_PIT_IDE1,F_POPUP:" + pitId + "," + pitIde + ",''N''";',
'            var dialogOptions = {',
'              title: ''Desbloquear Dispositivo Samsung'',',
'              height: ''500'',',
'              width: ''720'',',
'              maxWidth: ''900'',',
'              modal: true,',
'              dialog: null',
'            };',
'',
'            var modalPositionClass = ''modalPositionClass'';',
'            var dialogElement = apex.jQuery(''#R165021326767206049820'');',
'',
'            apex.navigation.dialog(dialogUrl, dialogOptions, modalPositionClass, dialogElement);',
'            ',
'',
'        }else if (proveedor != ''PAYBAHN''){',
'            //alert(''Otro Tipo'');',
'            apex.event.trigger(document, ''actualiza-clave-paybahn'');',
'        }            ',
'    }',
'',
'function close_dialog(){',
'        $(''#region_id'').dialog(''close''); ',
'       }'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240206104255'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(128216627752705462)
,p_name=>'ORDENES CUOTA COMODIN'
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.occ_id,',
'       p.per_tipo_identificacion,',
'       p.per_nro_identificacion,',
'       trim(p.per_primer_apellido||'' ''||p.per_segundo_apellido||'' ''||p.per_primer_nombre||'' ''||p.per_segundo_nombre||'' ''||p.per_razon_social) nombres,',
'       a.cxc_id,',
'       a.div_nro_vencimiento,',
'       kseg_p.pq_kseg_devuelve_datos.fn_nombre_usuario(a.occ_usu_id_genera) usuario_contac,',
'       a.occ_fecha_genera fecha',
'  from car_orden_cuota_comodin a, asdm_clientes b, asdm_personas p',
'  where p.per_id = b.per_id',
'  and b.cli_id = a.cli_id',
'  AND a.uge_id = :F_UGE_ID',
'  and occ_estado is null',
'  and com_id_nc is null'))
,p_display_when_condition=>'F_UGE_ID'
,p_display_when_cond2=>'1601'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
,p_comment=>'1601 -- uge del contac center'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128216724470705463)
,p_query_column_id=>1
,p_column_alias=>'OCC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Occ id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128216772134705464)
,p_query_column_id=>2
,p_column_alias=>'PER_TIPO_IDENTIFICACION'
,p_column_display_sequence=>2
,p_column_heading=>'Per tipo identificacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128216901640705465)
,p_query_column_id=>3
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>3
,p_column_heading=>'Per nro identificacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128217004290705466)
,p_query_column_id=>4
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>4
,p_column_heading=>'Nombres'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128217097436705467)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128217239860705468)
,p_query_column_id=>6
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Div nro vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128217311050705469)
,p_query_column_id=>7
,p_column_alias=>'USUARIO_CONTAC'
,p_column_display_sequence=>7
,p_column_heading=>'Usuario contac'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(128217443513705470)
,p_query_column_id=>8
,p_column_alias=>'FECHA'
,p_column_display_sequence=>8
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(15362541449531963359)
,p_plug_name=>'New'
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P7_CLI_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(38451556976302642955)
,p_name=>'Valores a Pagar'
,p_region_name=>'dividendos'
,p_parent_plug_id=>wwv_flow_imp.id(15362541449531963359)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT p.com_id,',
'       p.cxc_id,',
'       p.div_id,',
'       p.div_nro_vencimiento,',
'       p.div_fecha_vencimiento,',
'       p.div_valor_cuota,',
'       p.div_saldo_cuota,',
'       p.div_valor_pago,',
'       p.int_mora,',
'       p.div_mora_pagada,',
'       p.gto_cob,',
'       p.div_gasto_pagado,',
'       p.respaldo_cheques,',
'       p.ede_id,',
'       p.div_descuento_x_rol,',
'       p.uge_id,',
'       p.cli_id,',
'       apex_item.checkbox(10,',
'                          p.div_id,',
'                          p_attributes => ''onClick="baja_cuotas(this.value)"'') eliminar,',
'       (select d.ede_descripcion',
'       from asdm_entidades_destinos d',
'       where d.ede_id = p.ede_id) cartera,',
'       case when p.div_fecha_vencimiento < trunc(sysdate) then',
'        ''red''--''#FF7755''',
'       end color',
'  FROM car_cuotas_pagar p',
' WHERE p.cli_id = :p7_cli_Id',
' and p.usu_id = :f_user_id',
' order by p.cxc_id, p.div_nro_vencimiento'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557095815642956)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557231825642957)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:143:&SESSION.::&DEBUG.:RP,:P143_CXC_ID,F_POPUP:#CXC_ID#,N'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'style="color: #0269ff;"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557292113642958)
,p_query_column_id=>3
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557415184642959)
,p_query_column_id=>4
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_NRO_VENCIMIENTO#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557529722642960)
,p_query_column_id=>5
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'Fecha Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_FECHA_VENCIMIENTO#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557575181642961)
,p_query_column_id=>6
,p_column_alias=>'DIV_VALOR_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_VALOR_CUOTA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557664150642962)
,p_query_column_id=>7
,p_column_alias=>'DIV_SALDO_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_SALDO_CUOTA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817531171335385057)
,p_query_column_id=>8
,p_column_alias=>'DIV_VALOR_PAGO'
,p_column_display_sequence=>8
,p_column_heading=>'Cuota a Pagar'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:14;">#DIV_VALOR_PAGO#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557811635642963)
,p_query_column_id=>9
,p_column_alias=>'INT_MORA'
,p_column_display_sequence=>9
,p_column_heading=>'Int Mora'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#INT_MORA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817531309200385058)
,p_query_column_id=>10
,p_column_alias=>'DIV_MORA_PAGADA'
,p_column_display_sequence=>10
,p_column_heading=>'Mora a Pagar'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:14;">#DIV_MORA_PAGADA#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557856551642964)
,p_query_column_id=>11
,p_column_alias=>'GTO_COB'
,p_column_display_sequence=>11
,p_column_heading=>'Gto Cobranza'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="color:#COLOR#;">#GTO_COB#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817531376567385059)
,p_query_column_id=>12
,p_column_alias=>'DIV_GASTO_PAGADO'
,p_column_display_sequence=>12
,p_column_heading=>'Gto a Pagar'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span style="font-weight:bold;font-size:14;">#DIV_GASTO_PAGADO#</span>',
'',
''))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451557988766642965)
,p_query_column_id=>13
,p_column_alias=>'RESPALDO_CHEQUES'
,p_column_display_sequence=>13
,p_column_heading=>'Respaldo Cheques'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451558051915642966)
,p_query_column_id=>14
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451558157005642967)
,p_query_column_id=>15
,p_column_alias=>'DIV_DESCUENTO_X_ROL'
,p_column_display_sequence=>15
,p_column_heading=>'Descuento Rol'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451558274820642968)
,p_query_column_id=>16
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38451558408829642969)
,p_query_column_id=>17
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38492172676317231429)
,p_query_column_id=>18
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>18
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38525615677796238576)
,p_query_column_id=>19
,p_column_alias=>'CARTERA'
,p_column_display_sequence=>19
,p_column_heading=>'Cartera'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41473022476678546)
,p_query_column_id=>20
,p_column_alias=>'COLOR'
,p_column_display_sequence=>20
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38486428885005856440)
,p_plug_name=>'CARTERA CLIENTE'
,p_region_name=>'cartera'
,p_parent_plug_id=>wwv_flow_imp.id(15362541449531963359)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>60
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cc.com_id,',
'       cc.cxc_id,',
'       cc.div_id,',
'       cc.div_nro_vencimiento,',
'       cc.div_fecha_vencimiento,',
'       cc.div_valor_cuota,',
'       cc.div_saldo_cuota,',
'       cc.int_mora,',
'       cc.gto_cob,',
'       cc.respaldo_cheques,',
'       cc.ede_id,',
'       cc.div_descuento_x_rol,',
'       cc.uge_id,',
'       cc.cli_id,',
'       apex_item.checkbox(10,',
'                          cc.div_id,',
'                          p_attributes => ''onClick="carga_cuotas(this.value)"'') pagar,',
'       (SELECT u.uge_nombre',
'          FROM asdm_unidades_gestion u',
'         WHERE u.uge_id = cc.uge_id) unidad_gestion,',
'       (SELECT e.ede_DESCRIPCION',
'          FROM asdm_entidades_destinos e',
'         WHERE e.ede_id = cc.ede_id) cartera,',
'       cc.cxc_excluida,',
'       case when cc.dias_mora > 0 then',
'       cc.dias_mora',
'       else',
'       0',
'       end dias_mora,',
'       (select c.c004',
'       from apex_collections c',
'       where c.collection_name = ''COL_CXC_APLICAN''',
'       and to_number(c.c001) = cc.cxc_id) promocion',
'  FROM car_cuotas_cliente cc',
' WHERE cc.cli_id = :p7_cli_id',
'   AND cc.usu_id = :f_user_id',
'   AND not exists(select null from car_cuotas_pagar p where p.div_id = cc.div_id and p.usu_id = cc.usu_id)',
' ORDER BY cc.cxc_id, cc.div_nro_vencimiento',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(38486429021833856441)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'HTML:CSV'
,p_download_filename=>'cartera_pagos'
,p_enable_mail_download=>'N'
,p_csv_output_separator=>';'
,p_owner=>'ACALLE'
,p_internal_uid=>38454175870564091515
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429109281856442)
,p_db_column_name=>'COM_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429165878856443)
,p_db_column_name=>'CXC_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cxc id'
,p_column_link=>'f?p=&APP_ID.:143:&SESSION.::&DEBUG.:RP:P143_CXC_ID:#CXC_ID#'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429329672856444)
,p_db_column_name=>'DIV_ID'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Div id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429360368856445)
,p_db_column_name=>'DIV_NRO_VENCIMIENTO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Div nro vencimiento'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429453157856446)
,p_db_column_name=>'DIV_FECHA_VENCIMIENTO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Div fecha vencimiento'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429641926856447)
,p_db_column_name=>'DIV_VALOR_CUOTA'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Div valor cuota'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429733197856448)
,p_db_column_name=>'DIV_SALDO_CUOTA'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Div saldo cuota'
,p_column_html_expression=>'<span style="color:#COLOR#;">#DIV_SALDO_CUOTA#</span> '
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429775870856449)
,p_db_column_name=>'INT_MORA'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Int mora'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486429866348856450)
,p_db_column_name=>'GTO_COB'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Gto cob'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486430043110856451)
,p_db_column_name=>'RESPALDO_CHEQUES'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Respaldo cheques'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486430147836856452)
,p_db_column_name=>'EDE_ID'
,p_display_order=>110
,p_column_identifier=>'K'
,p_column_label=>'Ede id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486430193131856453)
,p_db_column_name=>'DIV_DESCUENTO_X_ROL'
,p_display_order=>120
,p_column_identifier=>'L'
,p_column_label=>'Div descuento x rol'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486430281345856454)
,p_db_column_name=>'UGE_ID'
,p_display_order=>130
,p_column_identifier=>'M'
,p_column_label=>'Uge id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486430446999856455)
,p_db_column_name=>'CLI_ID'
,p_display_order=>140
,p_column_identifier=>'N'
,p_column_label=>'Cli id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38486430520931856456)
,p_db_column_name=>'PAGAR'
,p_display_order=>170
,p_column_identifier=>'O'
,p_column_label=>'Pagar'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44817531634145385061)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>180
,p_column_identifier=>'P'
,p_column_label=>'Unidad gestion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44817531739157385062)
,p_db_column_name=>'CARTERA'
,p_display_order=>190
,p_column_identifier=>'Q'
,p_column_label=>'Cartera'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44817532965934385075)
,p_db_column_name=>'CXC_EXCLUIDA'
,p_display_order=>200
,p_column_identifier=>'R'
,p_column_label=>'Observacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44817533092480385076)
,p_db_column_name=>'DIAS_MORA'
,p_display_order=>210
,p_column_identifier=>'S'
,p_column_label=>'Dias mora'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38525611877388238538)
,p_db_column_name=>'PROMOCION'
,p_display_order=>220
,p_column_identifier=>'T'
,p_column_label=>'Promocion'
,p_column_link=>'f?p=&APP_ID.:131:&SESSION.:NUEVO:&DEBUG.:RP:P131_CXC_ID,P131_COM_ID,P131_PAG:#CXC_ID#,#COM_ID#,7'
,p_column_linktext=>'#PROMOCION#'
,p_column_link_attr=>'class="lock_ui_row" '
,p_column_type=>'STRING'
,p_display_condition_type=>'EXISTS'
,p_display_condition=>'select * From apex_collections where collection_name = ''COL_CXC_APLICAN'''
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(38486468615673137612)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'384542155'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'COM_ID:CXC_ID:DIV_ID:DIV_NRO_VENCIMIENTO:DIV_FECHA_VENCIMIENTO:DIV_VALOR_CUOTA:DIV_SALDO_CUOTA:INT_MORA:GTO_COB:RESPALDO_CHEQUES:EDE_ID:DIV_DESCUENTO_X_ROL:UGE_ID:CLI_ID:PAGAR:UNIDAD_GESTION:CARTERA:CXC_EXCLUIDA:DIAS_MORA:PROMOCION'
,p_sort_column_1=>'DIV_FECHA_VENCIMIENTO'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'DIV_NRO_VENCIMIENTO'
,p_sort_direction_2=>'ASC'
,p_break_on=>'COM_ID:CXC_ID:CARTERA:UNIDAD_GESTION:0:0'
,p_break_enabled_on=>'COM_ID:CXC_ID:CARTERA:UNIDAD_GESTION:0:0'
,p_sum_columns_on_break=>'DIV_SALDO_CUOTA'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(60176249595875826184)
,p_report_id=>wwv_flow_imp.id(38486468615673137612)
,p_name=>'Vencidos'
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'DIAS_MORA'
,p_operator=>'>'
,p_expr=>'0'
,p_condition_sql=>' (case when ("DIAS_MORA" > to_number(#APXWS_EXPR#)) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# > #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_font_color=>'#FF7755'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(80172202275317956875)
,p_name=>'CREDITOS EXCLUIDOS POR CONDONACION'
,p_parent_plug_id=>wwv_flow_imp.id(38486428885005856440)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.cxc_id,',
'       trunc(a.cec_fecha_registro) fecha_exclusion,',
'       b.cxc_saldo saldo_cxc,',
'       b.ede_id,',
'       (select c.ede_descripcion',
'       from asdm_e.asdm_entidades_destinos c',
'       where c.ede_id = b.ede_id) cartera',
'from asdm_e.car_cxc_excluidos_condonacion a,',
'     asdm_e.car_cuentas_por_cobrar b',
'     where a.cxc_id = b.cxc_id',
'           and a.cec_estado_registro = ''0''',
'           and a.cli_id = :P7_CLI_ID'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.cxc_id,',
'       trunc(a.cec_fecha_registro) fecha_exclusion,',
'       b.cxc_saldo saldo_cxc,',
'       b.ede_id,',
'       (select c.ede_descripcion',
'       from asdm_e.asdm_entidades_destinos c',
'       where c.ede_id = b.ede_id) cartera',
'from asdm_e.car_cxc_excluidos_condonacion a,',
'     asdm_e.car_cuentas_por_cobrar b',
'     where a.cxc_id = b.cxc_id',
'           and a.cec_estado_registro = ''0''',
'           and a.cli_id = :P7_CLI_ID'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80172202400482956876)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(86125262977104542627)
,p_query_column_id=>2
,p_column_alias=>'FECHA_EXCLUSION'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha exclusion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(86125263053023542628)
,p_query_column_id=>3
,p_column_alias=>'SALDO_CXC'
,p_column_display_sequence=>3
,p_column_heading=>'Saldo cxc'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(86125263217739542629)
,p_query_column_id=>4
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Ede id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(86125263280551542630)
,p_query_column_id=>5
,p_column_alias=>'CARTERA'
,p_column_display_sequence=>5
,p_column_heading=>'Cartera'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38492172841703231430)
,p_plug_name=>'TOTALES A PAGAR'
,p_parent_plug_id=>wwv_flow_imp.id(15362541449531963359)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(54963131232895350476)
,p_name=>'<SPAN STYLE="font-size: 16pt; color:RED;">ACUERDOS DE PAGO</span>'
,p_parent_plug_id=>wwv_flow_imp.id(15362541449531963359)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>15
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apc.acp_id, apc.cli_id, apc.age_id, apc.acp_fecha_acuerdo,',
'       apc.acp_observacion, apc.uge_id, apc.acp_modo, apc.acp_total, ',
'       apc.acp_creditos, apD.acD_valor cuota,--, apc.acp_valor_1 couta_inicial,',
'       apc.acp_dias cada,',
'       (CASE ',
'         WHEN apc.acp_tipo = 1 THEN',
'           ''DIAS''',
'         ELSE ',
'           ''DEL MES''',
'        END) Tipo,',
'        rownum nro_cuota,          ',
'        apd.acd_fecha fechas_pago     ',
'FROM car_acuerdos_pago_cab apc,',
'     car_acuerdos_pago_det apd',
'WHERE apc.acp_id = apd.acp_id',
'AND   apc.acp_estado_registro =0',
'AND   apd.acd_estado_registro = 0 ',
'AND   apc.acp_id = :P7_ACUERDO_ID',
'ORDER BY apd.acd_fecha ASC'))
,p_display_when_condition=>':P7_ACUERDO_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829932503926651833)
,p_query_column_id=>1
,p_column_alias=>'ACP_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829932892940651835)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829933296301651835)
,p_query_column_id=>3
,p_column_alias=>'AGE_ID'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829933692785651835)
,p_query_column_id=>4
,p_column_alias=>'ACP_FECHA_ACUERDO'
,p_column_display_sequence=>4
,p_column_heading=>'ACP_FECHA_ACUERDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829934117844651835)
,p_query_column_id=>5
,p_column_alias=>'ACP_OBSERVACION'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829934519999651836)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829934941025651836)
,p_query_column_id=>7
,p_column_alias=>'ACP_MODO'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829935334525651836)
,p_query_column_id=>8
,p_column_alias=>'ACP_TOTAL'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829935688343651836)
,p_query_column_id=>9
,p_column_alias=>'ACP_CREDITOS'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829937744639651837)
,p_query_column_id=>10
,p_column_alias=>'CUOTA'
,p_column_display_sequence=>14
,p_column_heading=>'VALOR CUOTAS'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829936096830651836)
,p_query_column_id=>11
,p_column_alias=>'CADA'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829936502686651837)
,p_query_column_id=>12
,p_column_alias=>'TIPO'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829936947315651837)
,p_query_column_id=>13
,p_column_alias=>'NRO_CUOTA'
,p_column_display_sequence=>12
,p_column_heading=>'NRO_CUOTA'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829937321306651837)
,p_query_column_id=>14
,p_column_alias=>'FECHAS_PAGO'
,p_column_display_sequence=>15
,p_column_heading=>'FECHAS_PAGO'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(54963254539606361896)
,p_name=>'DETALLE ACUERDO - ITEMS PARA LA DEVOLUCION'
,p_parent_plug_id=>wwv_flow_imp.id(54963131232895350476)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>110
,p_region_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apa.apa_id, apa.com_id factura, apa.cxc_id, aid.cantidad,',
'       aid.ite_sku_id item_id, aid.item  ',
'FROM car_acuerdo_pago_cxc apa,',
'     car_acuerdo_item_dev aid',
'WHERE apa.apa_id = aid.apa_id',
'AND   apa.apa_estado_registro = 0',
'AND   aid.aid_estado_registro = 0',
'AND   apa.acp_id = :P7_ACUERDO_ID'))
,p_display_when_condition=>':P7_ACUERDO_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_row_count_max=>500
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829938809412651841)
,p_query_column_id=>1
,p_column_alias=>'APA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'APA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829939244542651841)
,p_query_column_id=>2
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>2
,p_column_heading=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829939627855651842)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>3
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829940027464651842)
,p_query_column_id=>4
,p_column_alias=>'CANTIDAD'
,p_column_display_sequence=>4
,p_column_heading=>'CANTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829940447100651842)
,p_query_column_id=>5
,p_column_alias=>'ITEM_ID'
,p_column_display_sequence=>5
,p_column_heading=>'ITEM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44829940795785651842)
,p_query_column_id=>6
,p_column_alias=>'ITEM'
,p_column_display_sequence=>6
,p_column_heading=>'ITEM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(15362541506248963360)
,p_plug_name=>'DATOS DEL CLIENTE'
,p_region_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="font-size:14;font-family:Tahoma"'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_2'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(34498910242852052248)
,p_plug_name=>'Datos del Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(15362541506248963360)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(15362541610107963361)
,p_name=>'Cupo'
,p_parent_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_template=>wwv_flow_imp.id(270523665656046668)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pq_car_manejo_cupos.fn_retorna_cupo_utilizado(pn_cli_id => cu.cli_id,',
'                                                     pn_tse_id => 2) cupo_utilizado,',
'       cu.cup_cupo_disponible',
'  FROM car_cupos_clientes cu',
' WHERE cu.cli_id = :p7_cli_id',
'   AND cu.cup_tipo_cupo = ''M''',
'   AND cu.cup_estado_registro = 0'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15362541682701963362)
,p_query_column_id=>1
,p_column_alias=>'CUPO_UTILIZADO'
,p_column_display_sequence=>1
,p_column_heading=>'Cupo utilizado'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<font style=font-size:200%> #CUPO_UTILIZADO# </font>',
''))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15362541834972963363)
,p_query_column_id=>2
,p_column_alias=>'CUP_CUPO_DISPONIBLE'
,p_column_display_sequence=>2
,p_column_heading=>'Cupo disponible'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<font style="background:#5FA827;font-family:Times New Roman;font-size:300%; color:#FFF6CF"> #CUP_CUPO_DISPONIBLE# </font>',
''))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38525615329531238572)
,p_plug_name=>'CALCULADORA'
,p_parent_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_plug_display_sequence=>30
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(117884452101880893728)
,p_plug_name=>'<p><font size="20" face="verdana" color="RED"><b>ACTUALIZAR MAIL Y CORREO</b></font></p>'
,p_parent_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(206176474017073967443)
,p_name=>unistr('Actualizaci\00F3n Datos Clientes')
,p_parent_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
unistr('select ''Actualizar los datos del Cliente, \00FAltima actualizaci\00F3n: '' ||'),
'       TO_CHAR(c.cli_fecha_actualizacion, ''DD/MM/YYYY'') ALERTA',
'  from asdm_clientes c',
' where cli_id = :P7_CLI_ID'))
,p_display_when_condition=>':P7_VALIDA_ACT_CLI = 1'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(206176474072223967444)
,p_query_column_id=>1
,p_column_alias=>'ALERTA'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<font style ="font-family:Times New Roman;font-size:250%; color:RED">#ALERTA# </font>'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(34498912363093052270)
,p_plug_name=>'Perfil Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(15362541506248963360)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>2
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(34498912649847052272)
,p_name=>'PERFIL - CREDITO'
,p_parent_plug_id=>wwv_flow_imp.id(34498912363093052270)
,p_template=>wwv_flow_imp.id(270523665656046668)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select csc_id,',
'       csc_descripcion,',
'       csc_color,',
'       csc_nombre_comercial,',
'       emp_id,',
'       csc_estado_registro,',
'       dbms_lob.getlength("CSC_IMAGEN") csc_imagen',
'',
'  FROM ASDM_e.Car_Segmentacion_Credito',
' WHERE CSC_ID = :p7_csc_id'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498912843656052274)
,p_query_column_id=>1
,p_column_alias=>'CSC_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498912892333052275)
,p_query_column_id=>2
,p_column_alias=>'CSC_DESCRIPCION'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498912965130052276)
,p_query_column_id=>3
,p_column_alias=>'CSC_COLOR'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15362538165798963327)
,p_query_column_id=>4
,p_column_alias=>'CSC_NOMBRE_COMERCIAL'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15362538281487963328)
,p_query_column_id=>5
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15362538406859963329)
,p_query_column_id=>6
,p_column_alias=>'CSC_ESTADO_REGISTRO'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15362538540164963330)
,p_query_column_id=>7
,p_column_alias=>'CSC_IMAGEN'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_column_format=>'IMAGE:CAR_SEGMENTACION_CREDITO:CSC_IMAGEN:CSC_ID::::::::'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(63195253078700826632)
,p_name=>'DOCUMENTOS POR REGULARIZAR'
,p_region_name=>'reg'
,p_parent_plug_id=>wwv_flow_imp.id(15362541506248963360)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''DOCUMENTACION CUPO: '' || a.fecha_adicion TIPO_DOC,',
'       (SELECT listagg(x.descripcion||'':''||c.tdd_descripcion, ''; '') within GROUP(ORDER BY b.dcu_id DESC)',
'          FROM car_documentos_cupos b, car_tipos_docs_detalle c, car_tipos_documentos x',
'         WHERE b.cli_id = a.cli_id',
'           AND c.tdd_id = b.tdd_id',
'           and x.tdo_id = b.tdo_id) observacion,',
'   null Reimpresion_Credito,',
'   (SELECT case when x.tpd_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tpd_id_tipo_doc_cupos'')',
'            then ''reimprimir''',
'            else',
'              null',
'       end',
'          FROM car_documentos_cupos b, car_tipos_docs_detalle c, car_tipos_documentos x',
'         WHERE b.cli_id = a.cli_id',
'           AND c.tdd_id = b.tdd_id',
'           and x.tdo_id = b.tdo_id',
'   and x.tdo_id=pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tdo_id_solicitud_cupo'')) Reimpresion_Cupo,',
'   null com_id,',
'   null age_id',
'  FROM car_documentacion_cupos a,car_cupos_clientes C',
' WHERE cdc_estado = 0',
'   AND a.cli_id = :p7_cli_id',
'  AND C.CUP_ESTADO_REGISTRO=0',
' AND A.SCR_ID=C.SCR_ID',
' AND A.CLI_ID=C.CLI_ID',
' --AND A.CDC_FECHA_REGULARIZA IS NULL ',
'   UNION',
'SELECT ''DOCUMENTACION CREDITO: '' || e.ced_fecha_ingreso||'' cxc:''||f.cxc_id||'' com:''||f.com_id TIPO_DOC,',
'       (SELECT listagg(x.descripcion||'':''||d.cdo_observacion, ''; '') within GROUP(ORDER BY d.cdo_id DESC)',
'          FROM car_cxc_documentacion d, car_tipos_docs_detalle c, car_tipos_documentos x',
'         WHERE d.cxc_id = e.cxc_id',
'           and d.cdo_estado = 0',
'           AND c.tdd_id(+) = d.tdd_id',
'           and x.tdo_id(+) = d.tdo_id) observacion,',
'  ''reimprimir'' Reimpresion_Credito,',
'  null Reimpresion_Cupo,',
'  f.com_id,',
'  (select g.age_id_agente',
'  from ven_comprobantes g',
'  where g.com_id = f.com_id) age_id',
'  FROM car_cxc_estados_doc e, car_cuentas_por_cobrar f',
' WHERE ced_estado = 0',
' and f.cxc_id = e.cxc_id',
' and sign(f.cxc_saldo) = 1',
' and f.cli_id = :p7_cli_id',
'',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''DOCUMENTACION CUPO: '' || a.fecha_adicion TIPO_DOC',
'  FROM car_documentacion_cupos a',
' WHERE cdc_estado = 0',
'   AND a.cli_id = :p7_cli_id',
'   and :f_uge_id not in (''218'',''196'')--(''218''=:f_uge_id or ''196''=:f_uge_id)',
'   UNION',
'SELECT ''DOCUMENTACION CREDITO: '' || e.ced_fecha_ingreso TIPO_DOC',
'  FROM car_cxc_estados_doc e, car_cuentas_por_cobrar f',
' WHERE ced_estado = 0',
' and f.cxc_id = e.cxc_id',
' and sign(f.cxc_saldo) = 1',
' and f.cli_id = :p7_cli_id',
' and (''218''!=:f_uge_id or ''196''!=:f_uge_id)'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63195253178456826633)
,p_query_column_id=>1
,p_column_alias=>'TIPO_DOC'
,p_column_display_sequence=>1
,p_column_heading=>'Tipo doc'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63195253307243826634)
,p_query_column_id=>2
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>2
,p_column_heading=>'Observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63195253374211826635)
,p_query_column_id=>3
,p_column_alias=>'REIMPRESION_CREDITO'
,p_column_display_sequence=>5
,p_column_heading=>unistr('Reimpresi\00F3n credito')
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:RP:P52_PAG_ID,P52_COM_ID,P52_VALIDA_CONS_CARTERA:7,#COM_ID#,0'
,p_column_linktext=>'#REIMPRESION_CREDITO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63195253506013826636)
,p_query_column_id=>4
,p_column_alias=>'REIMPRESION_CUPO'
,p_column_display_sequence=>6
,p_column_heading=>'Reimpresion cupo'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:7:&SESSION.:solicitud_cupo:&DEBUG.:RP::'
,p_column_linktext=>'#REIMPRESION_CUPO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63195253602619826637)
,p_query_column_id=>5
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63195253700427826638)
,p_query_column_id=>6
,p_column_alias=>'AGE_ID'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(80167287361545943368)
,p_name=>'DOCUMENTOS POR REGULARIZAR 2'
,p_region_name=>'reg'
,p_parent_plug_id=>wwv_flow_imp.id(15362541506248963360)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''DOCUMENTACION CUPO: '' || a.fecha_adicion TIPO_DOC,',
'       (SELECT listagg(x.descripcion||'':''||c.tdd_descripcion, ''; '') within GROUP(ORDER BY b.dcu_id DESC)',
'          FROM car_documentos_cupos b, car_tipos_docs_detalle c, car_tipos_documentos x',
'         WHERE b.cli_id = a.cli_id',
'           AND c.tdd_id = b.tdd_id',
'           and x.tdo_id = b.tdo_id) observacion,',
'   null Reimpresion_Credito,',
'   (SELECT case when x.tpd_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tpd_id_tipo_doc_cupos'')',
'            then ''reimprimir''',
'            else',
'              null',
'       end',
'          FROM car_documentos_cupos b, car_tipos_docs_detalle c, car_tipos_documentos x',
'         WHERE b.cli_id = a.cli_id',
'           AND c.tdd_id = b.tdd_id',
'           and x.tdo_id = b.tdo_id',
'   and x.tdo_id=pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tdo_id_solicitud_cupo'')) Reimpresion_Cupo,',
'   null com_id,',
'   null age_id',
'  FROM car_documentacion_cupos a,car_cupos_clientes C',
' WHERE cdc_estado = 0',
'   AND a.cli_id = :p7_cli_id',
'  AND C.CUP_ESTADO_REGISTRO=0',
' AND A.SCR_ID=C.SCR_ID',
' AND A.CLI_ID=C.CLI_ID',
' AND A.CDC_FECHA_REGULARIZA IS NULL ',
'   UNION',
'SELECT ''DOCUMENTACION CREDITO: '' || e.ced_fecha_ingreso||'' cxc:''||f.cxc_id||'' com:''||f.com_id TIPO_DOC,',
'       (SELECT listagg(x.descripcion||'':''||d.cdo_observacion, ''; '') within GROUP(ORDER BY d.cdo_id DESC)',
'          FROM car_cxc_documentacion d, car_tipos_docs_detalle c, car_tipos_documentos x',
'         WHERE d.cxc_id = e.cxc_id',
'           and d.cdo_estado = 0',
'           AND c.tdd_id(+) = d.tdd_id',
'           and x.tdo_id(+) = d.tdo_id) observacion,',
'  ''reimprimir'' Reimpresion_Credito,',
'  null Reimpresion_Cupo,',
'  f.com_id,',
'  (select g.age_id_agente',
'  from ven_comprobantes g',
'  where g.com_id = f.com_id) age_id',
'  FROM car_cxc_estados_doc e, car_cuentas_por_cobrar f',
' WHERE ced_estado = 0',
' and f.cxc_id = e.cxc_id',
' and sign(f.cxc_saldo) = 1',
' and f.cli_id = :p7_cli_id',
'',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''DOCUMENTACION CUPO: '' || a.fecha_adicion TIPO_DOC',
'  FROM car_documentacion_cupos a',
' WHERE cdc_estado = 0',
'   AND a.cli_id = :p7_cli_id',
'   and (''218''=:f_uge_id or ''196''=:f_uge_id)',
'   UNION',
'SELECT ''DOCUMENTACION CREDITO: '' || e.ced_fecha_ingreso TIPO_DOC',
'  FROM car_cxc_estados_doc e, car_cuentas_por_cobrar f',
' WHERE ced_estado = 0',
' and f.cxc_id = e.cxc_id',
' and sign(f.cxc_saldo) = 1',
' and f.cli_id = :p7_cli_id',
' and (''218''=:f_uge_id or ''196''=:f_uge_id)'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80167287535709943369)
,p_query_column_id=>1
,p_column_alias=>'TIPO_DOC'
,p_column_display_sequence=>1
,p_column_heading=>'Tipo doc'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80167287557046943370)
,p_query_column_id=>2
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>2
,p_column_heading=>'Observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80167287663441943371)
,p_query_column_id=>3
,p_column_alias=>'REIMPRESION_CREDITO'
,p_column_display_sequence=>3
,p_column_heading=>unistr('Reimpresi\00F3n credito')
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:RP:P52_PAG_ID,P52_COM_ID,P52_VALIDA_CONS_CARTERA:7,#COM_ID#,0'
,p_column_linktext=>'#REIMPRESION_CREDITO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80167287809896943372)
,p_query_column_id=>4
,p_column_alias=>'REIMPRESION_CUPO'
,p_column_display_sequence=>4
,p_column_heading=>'Reimpresion cupo'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:7:&SESSION.:solicitud_cupo:&DEBUG.:RP::'
,p_column_linktext=>'#REIMPRESION_CUPO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80167287858565943373)
,p_query_column_id=>5
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(80167288047064943374)
,p_query_column_id=>6
,p_column_alias=>'AGE_ID'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38525614339038238562)
,p_plug_name=>'INFORMATIVO'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525167944046669)
,p_plug_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_2'
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P7_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44817529029273385035)
,p_name=>'TOTALES'
,p_region_name=>'totales'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_2'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apex_item.hidden(p_idx        => 01,',
'                        p_value      => a.cli_id) cli_id,',
'       apex_item.hidden(p_idx        => 02,',
'                        p_value      => a.usu_id) usu_id,',
'       apex_item.text(p_idx => 03,',
'                      p_value => a.cpc_TOTAL_pago, ',
'                      p_attributes => ''style="font-size:50px" class="lv_total" readonly'',',
'                      p_size => 6) total_pago,       ',
'       apex_item.text(p_idx => 04,',
'                      p_value => a.cpc_TOTAL_mora, ',
'                      p_attributes => '' style="font-size:50px" class="lv_total"'',',
'                      p_size => 4) total_mora,',
'       apex_item.text(p_idx => 05,',
'                      p_value => a.cpc_TOTAL_cobranza, ',
'                      p_attributes => ''style="font-size:50px" class="lv_total"'',',
'                      p_size => 4) total_cobranza, ',
'       apex_item.text(p_idx => 07,',
'                      p_value => a.CPC_seg_desgravamen, ',
'                      p_attributes => ''style="font-size:50px" "'',',
'                      p_size => 4) CPC_seg_desgravamen,  ',
'       apex_item.text(p_idx => 06,',
'                      p_value => a.cpc_total, ',
'                      p_attributes => ''style="font-size:50px" class="lv_total_general"'',',
'                      p_size => 4) TOTAL',
'  FROM car_cuotas_pagar_cab a',
' WHERE usu_id = :F_USER_ID',
' and cli_id = :p7_cli_id'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817530021123385045)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817530084867385046)
,p_query_column_id=>2
,p_column_alias=>'USU_ID'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817530155247385047)
,p_query_column_id=>3
,p_column_alias=>'TOTAL_PAGO'
,p_column_display_sequence=>3
,p_column_heading=>'Abono Cuota'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817530283007385048)
,p_query_column_id=>4
,p_column_alias=>'TOTAL_MORA'
,p_column_display_sequence=>4
,p_column_heading=>'Abono Mora'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817530445375385049)
,p_query_column_id=>5
,p_column_alias=>'TOTAL_COBRANZA'
,p_column_display_sequence=>5
,p_column_heading=>'Abono Cobranza'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(150206036183850663665)
,p_query_column_id=>6
,p_column_alias=>'CPC_SEG_DESGRAVAMEN'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817530533386385050)
,p_query_column_id=>7
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>7
,p_column_heading=>'Total Pago'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<font style="font-family:Times New Roman;font-size:50px; color:red"> #TOTAL# </font>',
''))
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44840792529634029546)
,p_plug_name=>'CONDONACIONES'
,p_parent_plug_id=>wwv_flow_imp.id(44817529029273385035)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select sum(int_mora) + sum(gto_cob)',
'from car_cuotas_pagar',
'where cli_id = :p7_cli_id',
'and usu_id = :f_user_id) > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44840791065579029532)
,p_plug_name=>'PORCENTAJE DE CONDONACION'
,p_parent_plug_id=>wwv_flow_imp.id(44840792529634029546)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select sum(int_mora) + sum(gto_cob)',
'from car_cuotas_pagar',
'where cli_id = :p7_cli_id',
'and usu_id = :f_user_id) > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44840792633139029547)
,p_plug_name=>'VALORES A COBRAR'
,p_parent_plug_id=>wwv_flow_imp.id(44840792529634029546)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(45077084511495260738)
,p_name=>'Creditos Negociados'
,p_template=>wwv_flow_imp.id(270525167944046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_2'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (CASE',
'         WHEN COUNT(*) > 0 and MAX(ed.ede_id) !=809 THEN',
'          ''El cliente tiene el credito: '' || cx.cxc_id || '' Negociado con '' ||',
'          ed.ede_descripcion ||',
'          '', los cobros en caja se podran realizar hasta '' ||',
'          (ne.neg_fecha_recepcion +',
'          pq_car_negociaciones.fn_ret_valor_parametro_ede(pn_ede_id => ne.ede_id,',
'                                                           pn_par_id => pq_constantes.fn_retorna_constante(0,',
'                                                                                                           ''cn_par_id_num_dias_cobro_neg_mor'')))',
'       ',
'          when COUNT(*) > 0 and MAX(ed.ede_id) =809 then ',
'          ''El cliente tiene el credito: '' || cx.cxc_id || '' Negociado con '' ||',
'          ed.ede_descripcion ||',
unistr('          '', no se puede realizar ning\00FAn tipo de cobro '' '),
'       ',
'       END) ALERTA',
'  FROM car_cuentas_por_cobrar  cx,',
'       CAR_CXC_NEGOCIACIONES   CN,',
'       CAR_NEGOCIACIONES       NE,',
'       asdm_entidades_destinos ed',
' WHERE EXISTS',
'       (SELECT ''x''',
'          FROM asdm_E.Car_Tipo_Negociacion_Entidad e',
'         WHERE ctn_id =',
'               pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'           AND e.cte_estado_registro = 0',
'           AND e.ede_id = cx.ede_id)',
'   AND cx.cxc_saldo > 0',
'   AND cx.cxc_estado = ''GEN''',
'   AND CX.CXC_ESTADO_REGISTRO = 0',
'   AND cne_estado_cxc = ''NEGOCIADO''',
'   AND CLI_ID = :P7_CLI_ID',
'   AND CX.CXC_ID = CN.CNE_CXC_ID',
'   AND CN.NEG_ID = NE.NEG_ID',
'   AND cx.ede_id = ed.ede_id',
' GROUP BY cx.cxc_id, ne.neg_fecha_recepcion, ed.ede_descripcion, ne.ede_id'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P7_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_row_count_max=>500
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38525614499669238564)
,p_query_column_id=>1
,p_column_alias=>'ALERTA'
,p_column_display_sequence=>1
,p_column_heading=>'ALERTA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<font color=RED>#ALERTA#</font>'
,p_heading_alignment=>'LEFT'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(80186743472119349057)
,p_plug_name=>'CREDITOS REGISTRADOS PARA DEBITO BANCARIO'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_2'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select d.cli_id,',
'       d.cxc_id,',
'       decode(d.cpd_estado_docs,1,''ACTIVO'',0,''INACTIVO'') estado_debito,',
'       d.cpd_observacion_inactivacion observacion,',
'       d.cpd_fecha_registro fecha_registro,',
'       a.per_nro_identificacion identificacion_titular,',
'       a.per_primer_apellido||'' ''||a.per_segundo_apellido||'' ''||',
'       a.per_primer_nombre||'' ''||a.per_segundo_nombre titular_cuenta,',
'       e.ede_descripcion entidad_financiera,',
'       c.ncu_tipo tipo_cuenta,',
'       c.ncu_numero nro_cuenta',
'from asdm_e.asdm_personas a,',
'     asdm_e.car_titulares_cuentas b,',
'     asdm_e.car_numeros_cuentas c,',
'     asdm_e.car_comprobantes_pagos_debito d,',
'     asdm_e.asdm_entidades_destinos e',
'where a.per_id = b.per_id',
'      and b.tcu_id = c.tcu_id',
'      and c.ncu_id = d.ncu_id',
'      and e.ede_id = c.ede_id',
'      and d.cli_id = :p7_cli_id'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select d.cli_id,',
'       d.cxc_id,',
'       decode(d.cpd_estado_docs,1,''ACTIVO'',0,''INACTIVO'') estado_debito,',
'       d.cpd_observacion_inactivacion observacion,',
'       d.cpd_fecha_registro fecha_registro,',
'       a.per_nro_identificacion identificacion_titular,',
'       a.per_primer_apellido||'' ''||a.per_segundo_apellido||'' ''||',
'       a.per_primer_nombre||'' ''||a.per_segundo_nombre titular_cuenta,',
'       e.ede_descripcion entidad_financiera,',
'       c.ncu_tipo tipo_cuenta,',
'       c.ncu_numero nro_cuenta',
'from asdm_e.asdm_personas a,',
'     asdm_e.car_titulares_cuentas b,',
'     asdm_e.car_numeros_cuentas c,',
'     asdm_e.car_comprobantes_pagos_debito d,',
'     asdm_e.asdm_entidades_destinos e',
'where a.per_id = b.per_id',
'      and b.tcu_id = c.tcu_id',
'      and c.ncu_id = d.ncu_id',
'      and e.ede_id = c.ede_id',
'      and d.cli_id = :p7_cli_id'))
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(80186743630654349058)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'MBARIAS'
,p_internal_uid=>80154490479384584132
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186743719421349059)
,p_db_column_name=>'CLI_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Cli id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186743833146349060)
,p_db_column_name=>'CXC_ID'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Cxc id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186743891679349061)
,p_db_column_name=>'ESTADO_DEBITO'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Estado debito'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744014052349062)
,p_db_column_name=>'OBSERVACION'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Observacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744088769349063)
,p_db_column_name=>'FECHA_REGISTRO'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Fecha registro'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744237046349064)
,p_db_column_name=>'IDENTIFICACION_TITULAR'
,p_display_order=>60
,p_column_identifier=>'F'
,p_column_label=>'Identificacion titular'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744321208349065)
,p_db_column_name=>'TITULAR_CUENTA'
,p_display_order=>70
,p_column_identifier=>'G'
,p_column_label=>'Titular cuenta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744366645349066)
,p_db_column_name=>'ENTIDAD_FINANCIERA'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Entidad financiera'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744536643349067)
,p_db_column_name=>'TIPO_CUENTA'
,p_display_order=>90
,p_column_identifier=>'I'
,p_column_label=>'Tipo cuenta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80186744600495349068)
,p_db_column_name=>'NRO_CUENTA'
,p_display_order=>100
,p_column_identifier=>'J'
,p_column_label=>'Nro cuenta'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(94295704591688599879)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'942634515'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CLI_ID:CXC_ID:ESTADO_DEBITO:OBSERVACION:FECHA_REGISTRO:IDENTIFICACION_TITULAR:TITULAR_CUENTA:ENTIDAD_FINANCIERA:TIPO_CUENTA:NRO_CUENTA'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(165053579589418814743)
,p_plug_name=>'Items Tracking'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_03'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(165053579918475814746)
,p_name=>'Lista de Items'
,p_parent_plug_id=>wwv_flow_imp.id(165053579589418814743)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pit.pit_uuid              uuid,',
'       pit.pit_ide1              imei,',
'       pit.pit_clave             clave,',
'       ite.ite_sec_id            ite_sec_id,',
'       ite.ite_descripcion_larga descripcion,',
'       pve.cxc_id,',
'       pit.pit_id,',
'       pit.tpr_id               proveedor,',
'       (select tpr_proveedor from inv_track_proveedores a where a.tpr_id = pit.tpr_id)  desc_prov,',
'       CASE ',
'        WHEN pit.tpr_id = (SELECT tpr_id FROM inv_track_proveedores a WHERE tpr_proveedor = ''KNOX'') ',
'        THEN ',
'        ''ACTUALIZAR CLAVE''',
'      ELSE NULL',
'    END AS ',
'    Actualiza_clave',
'  FROM inv_paybahn_ventas pve, inv_paybahn_items pit, inv_items ite',
' WHERE pve.pit_id = pit.pit_id',
'   AND pit.ite_sku_id = ite.ite_sku_id',
'   AND pve.cli_id = :P7_CLI_ID',
'   AND pit.pit_estado_registro = 0;'))
,p_display_when_condition=>'P7_EJECUTAR'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(165053580025363814747)
,p_query_column_id=>1
,p_column_alias=>'UUID'
,p_column_display_sequence=>20
,p_column_heading=>'Uuid'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1460252340823868482)
,p_query_column_id=>2
,p_column_alias=>'IMEI'
,p_column_display_sequence=>70
,p_column_heading=>'Imei'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(165053580083799814748)
,p_query_column_id=>3
,p_column_alias=>'CLAVE'
,p_column_display_sequence=>30
,p_column_heading=>'Clave'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(165053580180179814749)
,p_query_column_id=>4
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>40
,p_column_heading=>'Ite sec id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(165053580288472814750)
,p_query_column_id=>5
,p_column_alias=>'DESCRIPCION'
,p_column_display_sequence=>50
,p_column_heading=>'Descripcion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(165053580383388814751)
,p_query_column_id=>6
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>60
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1460252399438868483)
,p_query_column_id=>7
,p_column_alias=>'PIT_ID'
,p_column_display_sequence=>90
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1460252525000868484)
,p_query_column_id=>8
,p_column_alias=>'PROVEEDOR'
,p_column_display_sequence=>10
,p_column_heading=>'Proveedor'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_heading_alignment=>'RIGHT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT_FROM_LOV_ESC'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tpr_id || '' - '' || tpr_proveedor, tpr_id ',
'FROM inv_track_proveedores'))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1460252661652868485)
,p_query_column_id=>9
,p_column_alias=>'DESC_PROV'
,p_column_display_sequence=>100
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1460252769859868486)
,p_query_column_id=>10
,p_column_alias=>'ACTUALIZA_CLAVE'
,p_column_display_sequence=>80
,p_column_heading=>'Actualiza Clave'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:165:&SESSION.::&DEBUG.:165:P165_PIT_ID,P165_PIT_IDE1,F_POPUP:#PIT_ID#,#IMEI#,S'
,p_column_linktext=>'#ACTUALIZA_CLAVE#'
,p_column_link_attr=>'style="color: #0269ff;"'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44817531479228385060)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(44817529029273385035)
,p_button_name=>'Actualizar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'REFRESCAR PAGINA'
,p_button_position=>'ABOVE_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44829938061453651838)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(54963131232895350476)
,p_button_name=>'Imprimir_Detalle_Acuerdo'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Detalle Acuerdo'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=CAR_ACUERDO_DE_PAGO_DET.rdf&userid=asdm_p/asdm_p@sicpro01&destype=cache&desformat=pdf&PN_ACP_ID=&P7_ACUERDO_ID.''));'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44840792043182029541)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(44840791065579029532)
,p_button_name=>'APLICAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Aplicar'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(48423597134485614234)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(44817529029273385035)
,p_button_name=>'PAGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'PAGAR  >>'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:38:&SESSION.:PAGAR:&DEBUG.:RP:P38_CLI_ID,P38_NOMBRES,P38_CXC_ID_REF:&P7_CLI_ID.,&P7_NOMBRE_CLIENTE.,&P7_CXC_ID_REF.'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38511504796460976602)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(15362541610107963361)
,p_button_name=>'orden_seguro'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Orden Emision Seguro'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT cu.cup_cupo_disponible',
'  FROM asdm_e.car_cupos_clientes cu',
' WHERE cu.cli_id = :p7_cli_Id',
'   AND cu.cup_tipo_cupo = ''M''',
'   AND cu.cup_estado_registro = 0',
'   and not exists (SELECT null',
'  FROM asdm_e.car_cuentas_por_cobrar a,',
'       asdm_e.car_cuentas_seguros b',
' WHERE a.cxc_id = b.cxc_id',
'   AND a.ede_id = 1186',
'   AND b.pse_id <> 11',
'   AND a.cxc_saldo > 0',
'   AND a.cli_id = :p7_cli_Id)) >= 8.9'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(165053579847482814745)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(165053579589418814743)
,p_button_name=>'btnConsultarPaybahn'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Consultar'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(116718043832076450127)
,p_button_sequence=>190
,p_button_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_button_name=>'bn_dscto'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'OBTENER DESCUENTO'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:144:&SESSION.::&DEBUG.:RP:P144_CLI_ID,P144_PAG:&P7_CLI_ID.,7'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select x.cxc_id',
'from(',
'SELECT a.cxc_id, b.div_fecha_vencimiento,',
'       dense_rank()over(partition by a.cxc_id order by b.div_fecha_vencimiento) top',
'  FROM car_cuentas_por_cobrar a,',
'       car_dividendos         b',
' WHERE b.cxc_id = a.cxc_id',
'   and sign(a.cxc_saldo)=1',
'   and sign(b.div_saldo_cuota)=1',
'   AND a.cxc_estado = ''GEN''',
'   AND a.cxc_estado_registro = 0',
'   AND a.ede_id != 1186',
'   AND a.emp_id = :f_emp_id',
'   and a.cli_id = :p7_cli_Id',
'   --RULLOA 06/05/2020: Se habilita para que se pueda aplicar el descuento varias veces por pedido de Byron Andrade.',
'   AND NOT EXISTS',
' (SELECT NULL FROM car_promo_descuentos d WHERE d.div_id = b.div_id)) x',
'where x.top = 1',
'and trunc(div_fecha_vencimiento,''mm'') >= trunc((sysdate),''mm'')',
'and :f_uge_id = 274'))
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117884238446167672075)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(117884452101880893728)
,p_button_name=>'BTN_UPD_MAIL_CEL'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'ACTUALIZAR'
,p_button_alignment=>'LEFT-CENTER'
,p_button_condition=>'P7_NRO_IDENTIFICACION'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38525612051305238540)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_button_name=>'LIMPIAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'LIMPIAR TODO'
,p_button_alignment=>'LEFT-CENTER'
,p_warn_on_unsaved_changes=>null
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38525613641210238555)
,p_branch_name=>'VALIDA_TELEFONOS'
,p_branch_action=>'f?p=128:11:&SESSION.:cargar_datos:&DEBUG.:RP:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_EMP_LOGO,F_EMP_FONDO,F_VERSION,F_UGESTION,P11_APP_ID,P11_PAG_ID,F_POPUP,P11_NRO_IDENTIFICACION:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_VERSION.,&F_UGESTION.,211,7,S,&P7_NRO_IDENTIFICACION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_clientes_contac.fn_dev_validacion_datos(pv_per_nro_identificacion => :P7_NRO_IDENTIFICACION,',
'                                                             pn_emp_id => :F_EMP_ID)=1 ',
'',
'AND :REQUEST=''DATOS_CLIENTE'''))
,p_branch_condition_text=>'PLSQL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(104665092905013448)
,p_branch_name=>'BR_SEGURO'
,p_branch_action=>'f?p=&APP_ID.:7:&SESSION.:IR_SEGURO:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(38511504796460976602)
,p_branch_sequence=>20
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41471242075678528)
,p_name=>'P7_MENSAJE_ANTICIPO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_use_cache_before_default=>'NO'
,p_source=>'APLICAR EL PAGO CON ANTICIPO DE CLIENTES'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'NVL(:P7_TOTAL_ANTICIPOS,0) > 0  and :P7_CLI_ID is not null and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(83079749465154067)
,p_name=>'P7_CXC_ID_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'Cxc id ref'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(93324351909488358)
,p_name=>'P7_NRO_IDE_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'CEDULA REFINANCIAMIENTO'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cattributes_element=>'style="color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'CLIENTE_REFIN'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(93324472727488359)
,p_name=>'P7_MENSAJE_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(44817529029273385035)
,p_source=>'VALOR A PAGAR PARA REALIZAR EL REFINANCIAMIENTO'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cattributes_element=>'style="color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'      from CAR_ABONOS_REFINANCIAMIENTO A, CAR_CUENTAS_POR_COBRAR CXC',
'     WHERE A.CXC_ID = CXC.CXC_ID',
'       AND CXC.CLI_ID = :P7_cli_id',
'       AND A.CAR_ESTADO_REGISTRO = 0',
'       AND A.CAR_ABONO = 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1460252844355868487)
,p_name=>'P7_PIT_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(165053579918475814746)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1460252914980868488)
,p_name=>'P7_PIT_IDE1'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(165053579918475814746)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1460253044147868489)
,p_name=>'P7_PROVEEDOR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(165053579918475814746)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1460253154873868490)
,p_name=>'P7_ERROR'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(165053579918475814746)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15362541933567963364)
,p_name=>'P7_COBRADOR'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'Cobrador'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15362541989021963365)
,p_name=>'P7_TOTAL_ANTICIPOS'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Anticipos'
,p_source=>'pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P7_CLI_ID,:f_uge_id);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15362542523413963370)
,p_name=>'P7_SALDO_TOTAL'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15362542621524963371)
,p_name=>'P7_MORA_TOTAL'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15362542692018963372)
,p_name=>'P7_GCOB_TOTAL'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498910314367052249)
,p_name=>'P7_NRO_IDENTIFICACION'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'Nro identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.per_nro_identificacion || '' | '' ||TRIM(b.per_primer_apellido || '' '' || b.per_segundo_apellido || '' '' ||',
'            b.per_primer_nombre || '' '' || b.per_segundo_nombre || '' '' ||',
'            b.per_razon_social) || ',
'       '' | CLI_ID: '' || a.cli_id d,',
'       b.per_nro_identificacion r',
'  FROM asdm_clientes a, asdm_personas b',
' WHERE b.per_id = a.per_id',
'   AND b.per_estado_registro = 0',
'   AND a.cli_estado_registro = 0',
'   AND a.emp_id = :f_emp_id',
'   AND b.emp_id = :f_emp_id',
''))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498910396894052250)
,p_name=>'P7_PER_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498910469135052251)
,p_name=>'P7_CLI_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'Cli id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_tag_attributes=>'readonly'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498910621689052252)
,p_name=>'P7_NOMBRE_CLIENTE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_tag_attributes=>'readonly'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498910723358052253)
,p_name=>'P7_INS_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498911079049052257)
,p_name=>'P7_NOMBRE_INSTITUCION'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_prompt=>'Nombre institucion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'readonly'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498912672638052273)
,p_name=>'P7_CSC_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38511504327546968653)
,p_name=>'P7_ALERTA_SEGURO'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) =',
'              extract(MONTH FROM SYSDATE) THEN',
'          ''CLIENTE CON SEGURO POR RENOVAR''',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) <',
'              extract(MONTH FROM SYSDATE) AND a.dse_saldo_cuota > 0 THEN',
'          ''CLENTE CON SEGURO CADUCADO PENDIENTE DE PAGO''',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) <',
'              extract(MONTH FROM SYSDATE) AND a.dse_saldo_cuota = 0 THEN',
'          ''CLENTE CON SEGURO CADUCADO''',
'       END texto',
'  FROM car_dividendos_seguros a, car_cuentas_seguros b',
' WHERE b.cse_id = a.cse_id',
'   AND b.cse_plazo = a.dse_nro_vencimiento',
'   AND a.dse_fecha_vencimiento <= LAST_DAY(SYSDATE)',
'   AND b.ese_id = ''ENV''',
'   and b.cli_id = :p7_cli_Id',
'   and rownum = 1'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:16;color:GREEN;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38525613262512238552)
,p_name=>'P7_CALCULADORA'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(38525615329531238572)
,p_prompt=>'Calculadora'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>5
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'RIGHT-BOTTOM'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44829941739545659768)
,p_name=>'P7_ACUERDO_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MAX(c.acp_id) ',
'FROM car_acuerdos_pago_cab c ',
'WHERE c.cli_id = :P7_CLI_ID',
'AND   c.acp_estado_registro = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':P7_CLI_ID IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44835569001613714125)
,p_name=>'P7_CONTACTOS_NEG'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(38525614339038238562)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:15;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840790704704029528)
,p_name=>'P7_CXC_SEL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38451556976302642955)
,p_prompt=>'Creditos Seleccionados'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct cxc_id l,cxc_id r',
'From car_cuotas_cliente ',
'where cli_id = :p7_cli_id',
'and usu_id = :f_user_id',
'AND cxc_excluida is null'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'POPUP'
,p_attribute_08=>'CIC'
,p_attribute_10=>'300'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840791210129029533)
,p_name=>'P7_INT_MORA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(44840791065579029532)
,p_item_default=>'0'
,p_prompt=>'Interes Mora %'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>4
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840791319222029534)
,p_name=>'P7_GTO_COB'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(44840791065579029532)
,p_item_default=>'0'
,p_prompt=>'Gasto Cobranza %'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>4
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840791406348029535)
,p_name=>'P7_MORA_COND'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(44840791065579029532)
,p_prompt=>'Valor a Cobrar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840791475977029536)
,p_name=>'P7_GTO_COND'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(44840791065579029532)
,p_prompt=>'Valor a Cobrar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840792723216029548)
,p_name=>'P7_INT_MORA_1'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(44840792633139029547)
,p_prompt=>'Interes Mora %'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>2
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840792788517029549)
,p_name=>'P7_MORA_COND_1'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(44840792633139029547)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Mora a Cobrar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(div_mora_pagada)',
'from car_cuotas_pagar',
'where cli_id = :p7_cli_id',
'and usu_id = :f_user_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840792933196029550)
,p_name=>'P7_GTO_COB_1'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(44840792633139029547)
,p_prompt=>'Gasto Cobranza %'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>2
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44840792959817029551)
,p_name=>'P7_GTO_COND_1'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(44840792633139029547)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cobranza a Cobrar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(div_gasto_pagado)',
'from car_cuotas_pagar',
'where cli_id = :p7_cli_id',
'and usu_id = :f_user_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850371141368833773)
,p_name=>'P7_ALERTA_SEGMENTO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(34498912649847052272)
,p_source=>'Por favor revisar si los siguientes datos del Cliente son correctos, Resultado Equifax, Clasificacion Cliente. Verificar si se registraron los Ingresos del Cliente'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P7_CSC_ID'
,p_display_when2=>'6'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850371206849833774)
,p_name=>'P7_COB_RESTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(44840792633139029547)
,p_prompt=>'%Condonacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850371327867833775)
,p_name=>'P7_MORA_RESTA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(44840792633139029547)
,p_prompt=>'%Condonacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_tag_attributes=>'readonly'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(116753265054232287561)
,p_name=>'P7_CORREO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(117884452101880893728)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P7_NRO_IDENTIFICACION'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(116753265214711287562)
,p_name=>'P7_CELULAR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(117884452101880893728)
,p_prompt=>'Celular'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P7_NRO_IDENTIFICACION'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(116753265957036287570)
,p_name=>'P7_TEL_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(34498910242852052248)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(165053579683564814744)
,p_name=>'P7_EJECUTAR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(165053579589418814743)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(194579527260424327530)
,p_name=>'P7_CALIFICACION'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(38525615329531238572)
,p_prompt=>'Calf. Cr&eacute;dito'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>8
,p_tag_attributes=>'style="font-size:30;color:BLUE"'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'RIGHT-BOTTOM'
,p_read_only_when=>'P7_CALIFICACION'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(196774903433851416735)
,p_name=>'P7_TIENE_CORREO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(117884452101880893728)
,p_prompt=>'NO Tiene correo'
,p_display_as=>'NATIVE_CHECKBOX'
,p_lov=>'STATIC:;S'
,p_lov_display_null=>'YES'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P7_NRO_IDENTIFICACION'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(206176474166145967445)
,p_name=>'P7_VALIDA_ACT_CLI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(15362541506248963360)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  LN_DIAS_PARAMETRO NUMBER;',
'  LN_VALOR          NUMBER;',
'',
'  LN_VARIABLE NUMBER := pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                           ''cn_par_id_actu_info_cli'');',
'',
'BEGIN',
'',
'',
'if :P7_CLI_ID is not null then ',
'  SELECT PA.PEN_VALOR',
'    INTO LN_DIAS_PARAMETRO',
'    FROM ASDM_E.CAR_PAR_ENTIDADES PA',
'   WHERE PA.PAR_ID = LN_VARIABLE;',
'',
'  select TRUNC(SYSDATE - C.CLI_FECHA_ACTUALIZACION)',
'    INTO LN_VALOR',
'    from asdm_clientes c',
'   where cli_id = :P7_CLI_ID;',
'   ',
'   IF LN_VALOR >= LN_DIAS_PARAMETRO  THEN',
'     ',
'     RETURN 1;',
'     ',
'   ELSE ',
'     RETURN 0;',
'   END IF;',
'         ',
'end if;',
'',
'exception ',
'when others then ',
'  return 0;',
'',
'END;'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(34498910826122052254)
,p_name=>'ad_nro_identificacion'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_NRO_IDENTIFICACION'
,p_condition_element=>'P7_NRO_IDENTIFICACION'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(288746948228824634)
,p_event_id=>wwv_flow_imp.id(34498910826122052254)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT TRIM(b.per_primer_apellido || '' '' || b.per_segundo_apellido || '' '' ||',
'              b.per_primer_nombre || '' '' || b.per_segundo_nombre || '' '' ||',
'              b.per_razon_social),',
'         a.cli_id,',
'         b.per_id,',
'         a.csc_id',
'    INTO :p7_nombre_cliente, :p7_cli_id, :p7_per_id, :p7_csc_id',
'    FROM asdm_clientes a, asdm_personas b',
'   WHERE b.per_id = a.per_id',
'     AND b.per_estado_registro = 0',
'     AND a.cli_estado_registro = 0',
'     AND a.emp_id = :f_emp_id',
'     AND b.emp_id = :f_emp_id',
'     AND b.per_nro_identificacion = :p7_nro_identificacion;',
'END;',
''))
,p_attribute_02=>'P7_NRO_IDENTIFICACION'
,p_attribute_03=>'P7_CSC_ID,P7_PER_ID,P7_CLI_ID,P7_NOMBRE_CLIENTE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(34498910941436052255)
,p_event_id=>wwv_flow_imp.id(34498910826122052254)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'DATOS_CLIENTE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44817530622320385051)
,p_name=>'ad_actualiza'
,p_event_sequence=>30
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.lv_total'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44817530653502385052)
,p_event_id=>wwv_flow_imp.id(44817530622320385051)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'ACTUALIZA'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38525612245174238541)
,p_name=>'ad_limpia'
,p_event_sequence=>40
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(38525612051305238540)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38525612982611238549)
,p_event_id=>wwv_flow_imp.id(38525612245174238541)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota_dml.pr_del_cuotas_pagar(pn_cli_id => :p7_cli_id,',
'                    pn_usu_id => :f_user_id,',
'                    pv_error  => :p0_error);',
':P7_VALIDA_ACT_CLI :=0;'))
,p_attribute_03=>'P7_VALIDA_ACT_CLI'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38525612305432238542)
,p_event_id=>wwv_flow_imp.id(38525612245174238541)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'LIMPIAR'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38525613416344238553)
,p_name=>'ad_calculadora'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_CALCULADORA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'focusin'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38525613490247238554)
,p_event_id=>wwv_flow_imp.id(38525613416344238553)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'PLUGIN_COM.PLANETAPEX.ATOM_CALCULATOR'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_CALCULADORA'
,p_attribute_01=>'extended'
,p_attribute_02=>'N'
,p_attribute_03=>'fa-calculator'
,p_attribute_04=>'bottom left'
,p_attribute_06=>'material'
,p_attribute_08=>'focus'
,p_attribute_09=>'12'
,p_attribute_10=>'Y'
,p_attribute_14=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38525615425318238573)
,p_name=>'ad_total'
,p_event_sequence=>60
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.lv_total_general'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38525615549684238574)
,p_event_id=>wwv_flow_imp.id(38525615425318238573)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'ACTUALIZA_TOTAL'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44840790822378029529)
,p_name=>'ad_cxc_sel'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_CXC_SEL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44840790865614029530)
,p_event_id=>wwv_flow_imp.id(44840790822378029529)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'pq_car_pago_cuota.pr_cxc_seleccionados(:p7_cxc_sel)'
,p_attribute_02=>'P7_CXC_SEL'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44840791621431029537)
,p_name=>'ad_mora_cond'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_INT_MORA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44840791717904029538)
,p_event_id=>wwv_flow_imp.id(44840791621431029537)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_MORA_COND'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round((SUM(int_mora) - (SUM(int_mora) * (:p7_int_mora / 100))), 2) mora',
'  FROM car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_INT_MORA'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44840791825943029539)
,p_name=>'ad_gto_cond'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_GTO_COB'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44840791901383029540)
,p_event_id=>wwv_flow_imp.id(44840791825943029539)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_GTO_COND'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round((SUM(gto_cob) - (SUM(gto_cob) * (:p7_gto_cob / 100))), 2) gto',
'  FROM car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_GTO_COB'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44840792127274029542)
,p_name=>'ad_aplicar'
,p_event_sequence=>100
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(44840792043182029541)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44840792198704029543)
,p_event_id=>wwv_flow_imp.id(44840792127274029542)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'APLICAR'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44840793068608029552)
,p_name=>'ad_valor_cond'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_GTO_COND_1'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44840793228581029553)
,p_event_id=>wwv_flow_imp.id(44840793068608029552)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_GTO_COB_1'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'CASE WHEN SUM(GTO_COB) > 0 THEN',
'round(((:p7_gto_cond_1 * 100)/SUM(gto_cob)),2)',
'ELSE 0 END gto',
'  FROM asdm_e.car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_GTO_COND_1'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44850371352896833776)
,p_event_id=>wwv_flow_imp.id(44840793068608029552)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_COB_RESTA'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'CASE WHEN SUM(GTO_COB) > 0 THEN',
'100 - (round(((:p7_gto_cond_1 * 100)/SUM(gto_cob)),2))',
'ELSE 0 END gto',
'  FROM asdm_e.car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_GTO_COND_1'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(41474261667678559)
,p_name=>'ad_valor_cond_1'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_GTO_COND_1'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(41474501514678561)
,p_event_id=>wwv_flow_imp.id(41474261667678559)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_GTO_COB'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'CASE WHEN SUM(GTO_COB) > 0 THEN',
'100 - (round(((:p7_gto_cond_1 * 100)/SUM(gto_cob)),2))',
'ELSE 0 END gto',
'  FROM asdm_e.car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_GTO_COND_1'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(44840793332579029554)
,p_name=>'ad_valor_int'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_MORA_COND_1'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(44840793395785029555)
,p_event_id=>wwv_flow_imp.id(44840793332579029554)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_INT_MORA_1'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'CASE WHEN SUM(int_mora) > 0 THEN',
'round(((:p7_mora_cond_1 * 100)/SUM(int_mora)),2) ',
'ELSE 0 END MORA',
'  FROM asdm_e.car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_MORA_COND_1'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(41471116210678527)
,p_event_id=>wwv_flow_imp.id(44840793332579029554)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_MORA_RESTA'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'CASE WHEN SUM(int_mora) > 0 THEN',
'100 - (round(((:p7_mora_cond_1 * 100)/SUM(int_mora)),2) )',
'ELSE 0 END MORA',
'  FROM asdm_e.car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_MORA_COND_1'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(41474600161678562)
,p_name=>'ad_valor_int_1'
,p_event_sequence=>140
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_MORA_COND_1'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(41474786228678564)
,p_event_id=>wwv_flow_imp.id(41474600161678562)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_INT_MORA'
,p_attribute_01=>'SQL_STATEMENT'
,p_attribute_03=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'CASE WHEN SUM(int_mora) > 0 THEN',
'100 - (round(((:p7_mora_cond_1 * 100)/SUM(int_mora)),2) )',
'ELSE 0 END MORA',
'  FROM asdm_e.car_cuotas_pagar',
' WHERE usu_id = :f_user_id',
'   AND cli_id = :p7_cli_id',
''))
,p_attribute_07=>'P7_MORA_COND_1'
,p_attribute_08=>'Y'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(116753266458076287575)
,p_name=>'actualiza_celular'
,p_event_sequence=>170
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_CELULAR'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'focusout'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117884451996852893727)
,p_event_id=>wwv_flow_imp.id(116753266458076287575)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P7_CELULAR'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117884238031936672071)
,p_name=>'actualizar_correo'
,p_event_sequence=>180
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_CORREO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117884238462187672076)
,p_event_id=>wwv_flow_imp.id(117884238031936672071)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P7_CORREO'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(196774903486896416736)
,p_name=>'da_sin_correo'
,p_event_sequence=>190
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_TIENE_CORREO'
,p_condition_element=>'P7_TIENE_CORREO'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'S'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(196774903593374416737)
,p_event_id=>wwv_flow_imp.id(196774903486896416736)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_DISABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_CORREO'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(196774903832573416739)
,p_event_id=>wwv_flow_imp.id(196774903486896416736)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_ENABLE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_CORREO'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(196774903693761416738)
,p_event_id=>wwv_flow_imp.id(196774903486896416736)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P7_CORREO := NULL;'
,p_attribute_03=>'P7_CORREO'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(196774903900866416740)
,p_event_id=>wwv_flow_imp.id(196774903486896416736)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CLEAR'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_CORREO'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(196774904050422416741)
,p_name=>'da_sin_correo_1'
,p_event_sequence=>200
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_TIENE_CORREO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(196774904078957416742)
,p_event_id=>wwv_flow_imp.id(196774904050422416741)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P7_CORREO'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(196774904243275416743)
,p_event_id=>wwv_flow_imp.id(196774904050422416741)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_CORREO'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(1460253795053868497)
,p_name=>'da_close_dialog'
,p_event_sequence=>210
,p_triggering_element_type=>'REGION'
,p_triggering_region_id=>wwv_flow_imp.id(165053579589418814743)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460254187109868501)
,p_event_id=>wwv_flow_imp.id(1460253795053868497)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460254073576868499)
,p_event_id=>wwv_flow_imp.id(1460253795053868497)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(165053579589418814743)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(1430837365710812880)
,p_name=>'da_set_tipo_per'
,p_event_sequence=>220
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_TIPO_PER'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1430837384739812881)
,p_event_id=>wwv_flow_imp.id(1430837365710812880)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P7_TIPO_PER'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1430837729363812884)
,p_event_id=>wwv_flow_imp.id(1430837365710812880)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_NRO_IDENTIFICACION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(1430837518563812882)
,p_name=>'da_set_tipo_id'
,p_event_sequence=>230
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P7_TIPO_ID'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1430837666476812883)
,p_event_id=>wwv_flow_imp.id(1430837518563812882)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P7_TIPO_ID'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1430837835210812885)
,p_event_id=>wwv_flow_imp.id(1430837518563812882)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P7_NRO_IDENTIFICACION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(15362721899173428835)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_vencidos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_contac_center.pr_impresion_cartera(pn_emp_id => :f_emp_id,',
'                                          pn_cli_id => :p7_cli_id,',
'                                          pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'IMP_VENCIDOS'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>15330468747903663909
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(48423597365805614237)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_control'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'if apex_collection.collection_exists(''CO_MOV_CAJA'') then',
'apex_collection.delete_collection(''CO_MOV_CAJA'');',
'end if;',
'delete from asdm_e.CAR_DIV_CONTROL WHERE USU_ID = :f_user_id;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>48391344214535849311
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38511505153056983988)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_emision_seguro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  pr_url(--pn_app_origen => null,',
'         pn_app_destino => 242,',
'         --pn_pag_origen => null,',
'         pn_pag_destino => 54,',
'         --pv_sesion => :SESSION,',
'         pv_request => ''CLIORDEN'',',
'         pv_parametros => ''P54_CLI_ID,P54_CLASIFICACION,P54_ORIGEN'',',
'         pv_valores => :P7_CLI_ID||'',''||:P7_CSE_ID||'',''||''P'');',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'IR_SEGURO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38479252001787219062
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63195253837984826639)
,p_process_sequence=>50
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_sol_cupo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_car_imprimir.pr_imprimir_doc_pago_cuota (pn_cxc_id => :p7_cxc_id_ref,',
'                                            pn_cli_id => :p7_cli_id,',
'                                            pn_emp_id => :f_emp_id,',
'                                            PV_ERROR => :p0_error);',
'',
'end;  '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'solicitud_cupo'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>63163000686715061713
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117884238350962672074)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_mail_correo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--raise_application_error(-20000,''prueba vp1- CORREO: ''||:p7_correo || ''-CELULAR ''||:P7_CELULAR || ''-cli ''||:P7_cli_id || ''-pn_tel_id ''||:p7_tel_id);',
'pq_asdm_personas_dml.pr_upd_personas_mail_cel(pv_mail => :p7_correo,',
'                                                pv_celular => :p7_celular,',
'                                                pn_cli_id => :p7_cli_id,',
'                                                pn_tel_id => :p7_tel_id,',
'                                                pn_usu_id => :f_user_id,',
'                                                pn_emp_id => :f_emp_id,',
'                                                pv_error => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117884238446167672075)
,p_internal_uid=>117851985199692907148
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(34498911033239052256)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_car_pago_cuota.pr_carga_info_cliente(pn_emp_id      => :f_emp_id,',
'                                          pn_cli_id      => :p7_cli_id,',
'                                          pn_uge_id      => :f_uge_id,',
'                                          pn_tse_id      => :f_seg_id,',
'                                          pn_user_id     => :f_user_id,',
'                                          pn_ins_id      => :p7_ins_id,',
'                                          pv_nombre_inst => :p7_nombre_institucion,',
'                                          pv_cobrador    => :p7_cobrador,',
'                                          pn_cxc_id_ref  => :p7_cxc_id_ref,',
'                                          pv_error       => :p0_error);',
'',
'  :p7_int_mora := 0;',
'  :p7_gto_cob  := 0;',
'  :p7_cxc_sel  := NULL;',
'  BEGIN',
'    SELECT c.calificacion',
'      INTO :p7_calificacion',
'      FROM asdm_e.car_calif_ini_clientes c',
'     WHERE c.cli_id = :p7_cli_id;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'  END;',
'',
'  pq_car_pago_cuota.pr_get_mail_cel(pn_cli_id   => :p7_cli_id,',
'                                    pv_mail     => :p7_correo,',
'                                    pv_telefono => :p7_celular,',
'                                    pn_tel_id   => :p7_tel_id,',
'                                    pv_error    => :p0_error);',
'',
'  IF apex_collection.collection_exists(''COL_CXC_SELECCIONADOS'') THEN',
'    apex_collection.delete_collection(''COL_CXC_SELECCIONADOS'');',
'  END IF;',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_mov_caja''));',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_pago_cuota''));',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'DATOS_CLIENTE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>34466657881969287330
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44817530804335385053)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_act_datos_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota.pr_act_pagos(pn_cli_id => :p7_cli_Id,',
'                               pn_usu_id => :f_user_id,',
'                               pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ACTUALIZA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>44785277653065620127
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38525611843258238537)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_promociones'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' BEGIN',
'  ',
'  pq_asdm_promociones.pr_valida_cxc_promocion(pn_emp_id => :f_emp_id,',
'                                              pn_cli_id => :p7_cli_id,',
'                                              pn_seg_id => :F_SEG_ID,',
'                                              pn_pro_id => 1,',
'                                              pv_error => :p0_error);',
'                                              ',
'   END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'DATOS_CLIENTE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38493358691988473611
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38525612369713238543)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'pr_limpia'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(38525612051305238540)
,p_internal_uid=>38493359218443473617
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38525612691225238546)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pr'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota_dml.pr_del_cuotas_pagar(pn_cli_id => :p7_cli_id,',
'                    pn_usu_id => :f_user_id,',
'                    pv_error  => :p0_error);',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(38525612051305238540)
,p_process_when_type=>'NEVER'
,p_internal_uid=>38493359539955473620
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38525615572234238575)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_act_datos_total'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota.pr_act_pago_total(pn_cli_id => :p7_cli_Id,',
'                               pn_usu_id => :f_user_id,',
'                               pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ACTUALIZA_TOTAL'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38493362420964473649
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44840792338913029544)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_aplica_condonacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_pago_cuota.pr_act_condonacion(pn_cli_id => :p7_cli_Id,',
'                               pn_usu_id => :f_user_id,',
'                               pn_int_mora => :p7_mora_cond,',
'                               pn_gto_cob  => :p7_gto_cond,',
'                               pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'APLICAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>44808539187643264618
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(165053580471998814752)
,p_process_sequence=>100
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_consultar_paybahn'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--mfidrovo 10/02/2022',
'Begin',
'pr_et(4230,''Ingreso'', null);',
'if :P7_CLI_ID is not null then',
'  pq_inv_paybahn.pr_actualizar_clave(pn_cli_id => :P7_CLI_ID,',
'                                     pv_error => :pv_error);',
'',
':P7_EJECUTAR := ''S'';',
'end if;',
'End;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(165053579847482814745)
,p_internal_uid=>165021327320729049826
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44835574445325730562)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_alertas_neg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_ede_id_ars NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                             ''cn_ede_id_ars'');',
'',
'  ln_ede_id_recsa NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                               ''cn_ede_id_recsa'');',
'',
'  ln_ede_id_masoluc2 NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                  ''cn_ede_id_masoluc2'');',
'',
'  ln_ede_id_projuridico NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_ede_id_projuridico'');',
'',
'  ln_ede_id_ahrtgroup NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                   ''cn_ede_id_ahrtgroup'');',
'',
'  ln_ede_id_finsolred NUMBER := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                          ''cn_ede_id_finsolred'');',
'',
'  ln_ede_id_solinemp NUMBER := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                         ''cn_ede_id_solinemp'');',
'',
'  ln_num_ars         NUMBER;',
'  ln_num_recsa       NUMBER;',
'  ln_num_masoluc2    NUMBER;',
'  ln_num_projuridico NUMBER;',
'  ln_num_finsolred   NUMBER;',
'  ln_num_ahrtgroup   NUMBER;',
'  ln_num_solinemp    NUMBER;',
'',
'BEGIN',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_finsolred',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_finsolred',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  IF ln_num_finsolred > 0 THEN',
'    :p7_contactos_neg := ''Oficina                               072833300 / 072833100 '' ||',
'                         chr(13) || ''',
'                          Direccion:           Padre Aguirre 9 66 y Gran Colombia  '' ||',
'                         chr(13) || ''',
'                          Paulina Villavicencio         0983365438'' ||',
'                         chr(13) || ''',
'                          Magaly Baque                  0998968536'' ||',
'                         chr(13);',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_masoluc2',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_masoluc2',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  IF ln_num_masoluc2 > 0 THEN',
'  ',
'    :p7_contactos_neg := ''En los creditos negociados con RECUMPS se aceptara como abono a la cuenta cualquier valor que el cliente entregue,'' ||',
'                         chr(13) || ''',
'                        sin considerar si este se aplica a la cuota o como interes de mora.'';',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_ars',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_ars',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  -- raise_application_Error(-20000, ''aquiiii'' || ln_num_ars);',
'',
'  IF ln_num_ars > 0 THEN',
'  ',
unistr('    :p7_contactos_neg := ''Cr\00E9dito  vendido a ARS del Ecuador, por favor contactar al  personal de servicio al cliente en las  oficinas de Quito (Av. Amazonas 2248 y Ram\00EDrez D\00E1valos) '' ||'),
'                         chr(13) || ''',
unistr('                        y Guayaquil (Urdesa Central, Calle diagonal No.417C y V\00EDctor Emilio Estrada Of. 204). Tel\00E9fonos: 02 3815160, 1800 \2013 CARTERA. Contacto: Patricia Rodr\00EDguez'';'),
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_recsa',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_recsa',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  IF ln_num_recsa > 0 AND ln_num_ars > 0 THEN',
'  ',
unistr('    :p7_contactos_neg := ''Cr\00E9dito  vendido a ARS del Ecuador, por favor contactar al  personal de servicio al cliente en las  oficinas de Quito (Av. Amazonas 2248 y Ram\00EDrez D\00E1valos) '' ||'),
'                         chr(13) || ''',
unistr('                        y Guayaquil (Urdesa Central, Calle diagonal No.417C y V\00EDctor Emilio Estrada Of. 204). Tel\00E9fonos: 02 3815160, 1800 \2013 CARTERA. Contacto: Patricia Rodr\00EDguez'';'),
'  ',
'    :p7_contactos_neg := :p7_contactos_neg || chr(13) || chr(13) ||',
'                        ',
unistr('                         ''Cr\00E9dito vendido a COBRANZAS DEL ECUADOR (RECSA), por favor contactarse con el personal de servicio al cliente'' ||'),
'                         chr(13) || chr(13) ||',
'                         ''QUITO Katy Egas kegas@recsa.com.ec (02) 2999-800 / 2983-900  EXT. 9822'' ||',
'                         chr(13) ||',
'                         ''CUENCA  Thelmo Guzman tguzman@recsa.com.ec (07)2820858  '' ||',
'                         chr(13) ||',
unistr('                         ''AMBATO  Martha Proa\00F1o mproano@recsa.com.ec (03) 2822-883 / 2822-366  EXT. 6081 '' ||'),
'                         chr(13) ||',
'                         ''GUAYAQUIL Michel Onofre donofre@recsa.com.ec (04) 6011-380 / 6011-350  7004'' ||',
'                         chr(13) ||',
'                         ''MANTA Yandry Arteaga  yarteaga@recsa.com.ec (05) 2629-844 EXT. 6161'' ||',
'                         chr(13) ||',
'                         ''SANTO DOMINGO Susana Borja  sborja@recsa.com.ec (02) 2767-942 / 2767-943  EXT. 6111'';',
'  ',
'  ELSIF ln_num_recsa > 0 THEN',
'  ',
unistr('    :p7_contactos_neg := ''Cr\00E9dito vendido a COBRANZAS DEL ECUADOR (RECSA), por favor contactarse con el personal de servicio al cliente'' ||'),
'                         chr(13) || chr(13) ||',
'                         ''QUITO Katy Egas kegas@recsa.com.ec (02) 2999-800 / 2983-900  EXT. 9822'' ||',
'                         chr(13) ||',
'                         ''CUENCA  Thelmo Guzman tguzman@recsa.com.ec (07)2820858  '' ||',
'                         chr(13) ||',
unistr('                         ''AMBATO  Martha Proa\00F1o mproano@recsa.com.ec (03) 2822-883 / 2822-366  EXT. 6081 '' ||'),
'                         chr(13) ||',
'                         ''GUAYAQUIL Michel Onofre donofre@recsa.com.ec (04) 6011-380 / 6011-350  7004'' ||',
'                         chr(13) ||',
'                         ''MANTA Yandry Arteaga  yarteaga@recsa.com.ec (05) 2629-844 EXT. 6161'' ||',
'                         chr(13) ||',
'                         ''SANTO DOMINGO Susana Borja  sborja@recsa.com.ec (02) 2767-942 / 2767-943  EXT. 6111'';',
'  ',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_projuridico',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_projuridico',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  IF ln_num_projuridico > 0 THEN',
'  ',
unistr('    :p7_contactos_neg := ''Cr\00E9dito  vendido a Projuridico, por favor contactar al  personal de servicio al cliente en las  oficinas de Guayaquil (Lorenzo de Garaicoa Nro. 732 y Victor Manuel Rendon, Ed. Plaza Centenario) '' ||'),
'                         chr(13) || ''',
unistr('                        Piso 3, Oficina 32.  Tel\00E9fonos: 04 2598300, Ext. 6000 Contacto: Jessica Granizo'';'),
'  ',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_ahrtgroup',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_ahrtgroup',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  IF ln_num_ahrtgroup > 0 THEN',
'  ',
'    :p7_contactos_neg := ''Direccion Oficina:           FEDERICO MALO 1-90 (CUENCA)  '' ||',
'                         chr(13) || ''',
unistr('                          VICENTE RODR\00CDGUEZ AGUIRRE             0985190065'' ||'),
'                         chr(13);',
'  ',
'  END IF;',
'',
'  SELECT COUNT(*)',
'    INTO ln_num_solinemp',
'    FROM car_cuentas_por_cobrar cx',
'   WHERE cx.ede_id = ln_ede_id_solinemp',
'     AND cx.cli_id = :p7_cli_id;',
'',
'  IF ln_num_solinemp > 0 THEN',
'    :p7_contactos_neg := ''Oficina                               SOLINEMP S.A. '' ||',
'                         chr(13) || ''',
unistr('                          Direccion:           Av. Remigio Crespo y Calle Guayas (esq) en el 3er piso del Edificio San Jos\00E9  '' ||'),
'                         chr(13) || ''',
'                          Katalina Villa (Operadora)         0968313752'' ||',
'                         chr(13) || ''',
'                          Byron Andrade (Gerente)            0992514319'' ||',
'                         chr(13) || ''',
'                          Sebastian Ochoa (Jefe operativo)   0998412444'' ||',
'                         chr(13) || ''',
'                          bandrade@solinemp.com -- informacionlegal06@solinemp.com -- gerencia@solinemp.com'' ||',
'                         chr(13);',
'  END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P7_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_process_when_type=>'EXISTS'
,p_internal_uid=>44803321294055965636
);
wwv_flow_imp.component_end;
end;
/
