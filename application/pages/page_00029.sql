prompt --application/pages/page_00029
begin
--   Manifest
--     PAGE: 00029
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>29
,p_name=>unistr('Facturaci\00BF\00BFn ServiciosRespadoViernes')
,p_step_title=>unistr('Facturaci\00BF\00BFn ServiciosRespadoViernes')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(260853577746360838)
,p_plug_name=>'Tipo_Despacho'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>110
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_04'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_ord_ID is null and :P29_CLI_ID is not null',
'and (select count(*) ',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_DETALLE'') > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(260853762224360851)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(260853974500360852)
,p_plug_name=>'ADICIONALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>65
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(260854179108360853)
,p_plug_name=>'Facturacion'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_2'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(260854357306360854)
,p_plug_name=>'INSERTA ARTICULOS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_when_condition=>':P29_ORD_ID is null and :P29_POL_ID is not null and :P29_TVE_ID is not null'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<!--',
'',
'function pull_multi_value2(pValue){',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ''APPLICATION_PROCESS=P_BUSCA_ITEM'',0);',
'if(pValue){',
'get.add(''F_ITE_SKU_ID'',pValue)',
'}else{',
'get.add(''F_ITE_SKU_ID'',''null'')',
'}    ',
'',
'    gReturn = get.get(''XML'');',
'',
'    if(gReturn){',
'        var l_Count = gReturn.getElementsByTagName("item").length;',
'        for(var i = 0;i<l_Count;i++){',
'            var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'            var l_ID = l_Opt_Xml.getAttribute(''id'');',
'            var l_El = html_GetElement(l_ID);    ',
'            if(l_Opt_Xml.firstChild){',
'                var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'            }else{',
'                var l_Value = '''';',
'            }',
'',
'            if(l_El){',
'                if(l_El.tagName == ''INPUT''){',
'                    l_El.value = l_Value;',
'                }else if(l_El.tagName == ''SPAN'' && ',
'                l_El.className == ''grabber''){',
'                    l_El.parentNode.innerHTML = l_Value;',
'                    l_El.parentNode.id = l_ID;',
'                }else{',
'                    l_El.innerHTML = l_Value;',
'                }',
'            }',
'        }',
'    }',
'    get = null;',
'',
'}',
'',
'//-->',
'</script>'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(260854560959360854)
,p_name=>'CO_VARIABLES_ENTRADA'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_ENTRADA'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859255885360882)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859381215360882)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>5
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859472484360882)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859558139360882)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859675667360882)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859765514360882)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859866351360882)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859972060360882)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860060764360882)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260854756276360861)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260854881604360871)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260854963710360871)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855064227360871)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855152744360872)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855276848360872)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855374610360872)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855461687360872)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855572512360872)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855682260360874)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855772357360874)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855857031360874)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260855970990360874)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856055894360874)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856178561360874)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856282873360874)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856378161360874)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856451270360875)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856579910360875)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856655959360875)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856755318360876)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856864549360876)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260856965410360876)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857065600360876)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857169552360876)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857276856360879)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857383925360879)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857466192360879)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857580469360879)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857657230360879)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857783395360879)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857858252360879)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260857959004360879)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858068559360879)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858163226360879)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858256530360879)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858360469360879)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858478152360879)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858571846360880)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858677632360880)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858754216360880)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858874036360880)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260858956625360880)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859068324360880)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260859182321360882)
,p_query_column_id=>54
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(260860154843360882)
,p_name=>'TOTALES'
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_totales;'
,p_display_when_condition=>'P29_POL_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div style="width:210px">',
''))
,p_footer=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(260860154843360882)
,p_plug_column_width=>' valign="top"'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860354947360883)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860456560360884)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860553405360884)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860675241360884)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860755935360884)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860871097360884)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260860974871360884)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861057173360884)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861172268360884)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861258934360884)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861351717360884)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861464197360884)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861573096360884)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861674306360884)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861761029360884)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861863712360884)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260861951539360884)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862061392360884)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862154021360885)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862266155360885)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862380477360885)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862452420360885)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862568655360885)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862666370360885)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862774511360885)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862862996360885)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260862957816360885)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863056474360885)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863163992360885)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863260447360885)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863381573360885)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863468214360885)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863566683360885)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863669710360885)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863755615360886)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863855398360886)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260863968023360886)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864071657360886)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864166520360886)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864265927360886)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864380311360886)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864453307360886)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864552734360886)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864655224360886)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864770875360892)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864861418360892)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260864958527360892)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865052762360892)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865157222360892)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865261840360892)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865356414360893)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865466251360893)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865560937360893)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865678750360893)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865753781360894)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865874722360894)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260865954480360894)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260866055667360894)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260866169034360894)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260866252142360894)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(260866351403360894)
,p_plug_name=>'ERROR'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P29_ERROR'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(260866576216360897)
,p_name=>'DetalleOrdeen'
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_detalle;'
,p_display_when_condition=>'P29_POL_ID'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>31
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>50
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(260866576216360897)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260866877867360898)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260866980928360898)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.::P20_SEQ_ID,P20_AGE_ID_AGENTE,P20_IEB_ID,P20_ITE_SKU_ID,P20_DESCRIPCION_LARGA,P20_ESTADO,P20_ESTADO_DES,P20_BODEGA,P20_CANTIDAD:#COL03#,#COL04#,#COL05#,#COL06#,#COL07#,#COL08#,#COL09#,#COL10#,#COL11#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867058879360898)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867152661360898)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867282374360898)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867366319360898)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867455196360898)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867582377360899)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867670995360899)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867768901360899)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867869058360899)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260867961521360899)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868079179360899)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869756705360900)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868165188360899)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868267810360899)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868355568360899)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868479434360899)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868579411360899)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868653057360899)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868783671360899)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868876488360899)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260868968290360899)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869075480360899)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869153782360899)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869253783360899)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869352672360900)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869461552360900)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869583364360900)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260869673374360900)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260866770423360898)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(260869857342360900)
,p_name=>'CO_VARIABLES_DETALLE'
,p_template=>wwv_flow_imp.id(270520665472046666)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_DETALLE'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(260869857342360900)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870053707360900)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870161268360900)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870254250360900)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870369181360900)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870480280360900)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870571807360900)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870673055360900)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870773610360900)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870858977360901)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260870971599360901)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871066542360902)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871164516360902)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871269015360903)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871369365360903)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871464188360903)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871554500360903)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871654477360903)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871758217360903)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871879280360903)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260871962233360903)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872073737360903)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872172411360903)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872255647360903)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872368393360903)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872463051360903)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872554489360903)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872653048360903)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872770212360903)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872875868360903)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260872964225360903)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873068081360904)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873153418360904)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873275225360904)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873366535360904)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873483505360904)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873577119360904)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873676927360904)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873759147360904)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873870526360904)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260873961035360904)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874081336360904)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874172084360904)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874278186360904)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874360472360904)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874467175360904)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874578896360904)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874668858360904)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874761351360905)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874874993360905)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260874972559360905)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875059991360905)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875180723360905)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875263884360905)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875383837360905)
,p_query_column_id=>54
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(260875467256360905)
,p_name=>'COL_AUT'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select seq_id, c001, c002, c003, c004, c005 from apex_collections',
'where collection_name = ''CO_AUTORIZACIONES'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875663817360905)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875760620360905)
,p_query_column_id=>2
,p_column_alias=>'C001'
,p_column_display_sequence=>2
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875867359360905)
,p_query_column_id=>3
,p_column_alias=>'C002'
,p_column_display_sequence=>3
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260875981551360905)
,p_query_column_id=>4
,p_column_alias=>'C003'
,p_column_display_sequence=>4
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260876068134360905)
,p_query_column_id=>5
,p_column_alias=>'C004'
,p_column_display_sequence=>5
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(260876155403360905)
,p_query_column_id=>6
,p_column_alias=>'C005'
,p_column_display_sequence=>6
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260885773991360938)
,p_button_sequence=>13
,p_button_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_button_name=>'P29_AGREGAR_ARTICULOS'
,p_button_static_id=>'P29_AGREGAR_ARTICULOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>unistr('Agregar Art\00BF\00BFculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>'P29_TVE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_request_source=>'pr_cargar_detalle'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260878455272360922)
,p_button_sequence=>430
,p_button_plug_id=>wwv_flow_imp.id(260866576216360897)
,p_button_name=>'P29_ELIMINAR_LINEA'
,p_button_static_id=>'P29_ELIMINAR_LINEA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>unistr('Eliminar_Art\00BF\00BFculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>'P29_ord_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_request_source=>'Eliminar'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260878263224360922)
,p_button_sequence=>636
,p_button_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_button_name=>'P29_RECALCULAR'
,p_button_static_id=>'P29_RECALCULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Recalcular'
,p_button_alignment=>'LEFT'
,p_button_condition=>'P29_ord_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_request_source=>'Recalcular'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260876661810360917)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(260853762224360851)
,p_button_name=>'BTN_CLIENTENUEVO'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Modificar_Cliente'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:popUp2(''http://oas.pdm:7777/pls/apex/f?p=195:3:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,,1,1,S'', 8000, 850);'||wwv_flow.LF||
''
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260876255793360906)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(260853762224360851)
,p_button_name=>'NUEVO_BASADO_EN_ESTA_ORDEN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Nuevo_Basado_En_Esta_Orden_Venta'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>'P29_COM_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260876466546360910)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(260853762224360851)
,p_button_name=>'LIMPIAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Limpiar_/_Nuevo'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260876865112360917)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(260853762224360851)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'http://192.168.100.237:7778/reports/rwservlet?module=ven_ordenes.rdf&userid=asdm_p/asdm_p@mxpdm&destype=cache&desformat=pdf&pn_ord_id=&P29_COM_ID.&pn_emp_id=&F_EMP_ID.&pn_uge_id=&F_UGE_ID.&pn_usuario_id=&F_USER_ID.'
,p_button_condition=>'P29_COM_ID'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260877081374360917)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_imp.id(260853577746360838)
,p_button_name=>'CRONOGRAMA_PAGOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Cronograma_Pagos'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_VALOR_FINANCIAR_CRE is not null and :P29_TVE_ID = 2',
'AND :P29_ORD_TIPO = PQ_CONSTANTES.fn_cv_ord_tipo_orden_venta'))
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(260877278157360917)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(260853762224360851)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(260896072108360978)
,p_branch_action=>'f?p=147:201:&SESSION.:::201:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,P201_PER_NRO_IDENTIFICACION,P201_APP_REGRESA,P201_PAG_REGRESA:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,1,S,&P29_IDENTIFICACION.,&APP_ID.,&APP_PAGE_ID.'||wwv_flow.LF||
''
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_branch_condition=>'P29_CLIENTE_EXISTE'
,p_branch_condition_text=>'N'
,p_branch_comment=>'Created 07-ABR-2010 17:22 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(260896670415360992)
,p_branch_action=>'f?p=&FLOW_ID.:29:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(260877278157360917)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(260896262830360991)
,p_branch_action=>'f?p=&APP_ID.:29:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(260876466546360910)
,p_branch_sequence=>15
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(260896471617360991)
,p_branch_action=>'f?p=&APP_ID.:29:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>38
,p_branch_comment=>'Created 19-MAR-2010 15:12 by MURGILES'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260877456125360918)
,p_name=>'P29_TSE_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_item_default=>'pq_ven_listas.fn_retorna_segento_age(:f_uge_id);'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tse Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260877680387360921)
,p_name=>'P29_STOCK'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Stock'
,p_source=>'return pq_ven_comunes.fn_stock_sku_estado_bod_ventas(:P29_ITE_SKU_ID,:P29_ESTADO,:P29_BODEGA);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>3
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260877868576360922)
,p_name=>'P29_ESTADO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_item_default=>'pq_constantes.fn_cn_cei_id_estado_a;'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>2
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260878066630360922)
,p_name=>'P29_SEQ_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260878681913360922)
,p_name=>'P29_CORREO'
,p_item_sequence=>19
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260878852465360922)
,p_name=>'P29_NOMBRE'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>120
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-BOTTOM'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260879074646360930)
,p_name=>'P29_TELEFONO'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260879281754360930)
,p_name=>'P29_AGE_ID_AGENTE'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_prompt=>'Vendedor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>2000
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260879451314360930)
,p_name=>'P29_ORD_ID_VAR'
,p_item_sequence=>37
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Ord Id Var'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260879673013360931)
,p_name=>'P29_TTR_ID'
,p_item_sequence=>36
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Ttr Id'
,p_source=>'select pq_constantes.fn_cn_ttr_id_comprobante from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260879876003360931)
,p_name=>'P29_TOTAL_FACTURA_ING'
,p_item_sequence=>638
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Ajustar a Total Factura:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260880055953360931)
,p_name=>'P29_DESCTO_PUNTOS'
,p_item_sequence=>631
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Descuento Pekas'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260880279955360931)
,p_name=>'P29_TIPO_TELEFONO'
,p_item_sequence=>17
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>unistr('T\00BF\00BFlefono')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260880466686360931)
,p_name=>'P29_TIPO_DIRECCION'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>unistr('Direcci\00BF\00BFn')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260880664036360932)
,p_name=>'P29_TVE_ID'
,p_item_sequence=>24
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Tve'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TERMINOS_VENTA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_terminos_venta(:f_emp_id,null,:p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P29_POL_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P29_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Terminos de venta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260881163779360932)
,p_name=>'P29_CLI_ID'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Cliente Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260881374661360933)
,p_name=>'P29_EOR_OBSERVACIONES'
,p_item_sequence=>22
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Observaciones'
,p_source=>'EOR_OBSERVACIONES'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>87
,p_cMaxlength=>255
,p_cHeight=>3
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when=>'P29_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260881581392360933)
,p_name=>'P29_DIRECCION'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260881775696360933)
,p_name=>'P29_IDENTIFICACION'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>unistr('Identificaci\00BF\00BFn')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260881955266360933)
,p_name=>'P29_IEB_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260882175036360933)
,p_name=>'P29_ITE_SKU_ID'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_prompt=>'Item id'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ITEMS_SERV_REC_CEL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas_caja.fn_lov_items_serv_recarga_cel(:f_emp_id,   :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cSize=>5
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''item'')"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when_type=>'ALWAYS'
,p_read_only_when=>'P29_ORD_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260882371502360934)
,p_name=>'P29_LINES1'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'__________'
,p_source=>'__________________________________________________________________________________'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_ORD_TIPO is not null and ',
':P29_ORD_TIPO = pq_constantes.fn_cv_ord_tipo_traspaso_cli'))
,p_display_when2=>'SQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260882564616360934)
,p_name=>'P29_IDENTIFICACION_ORIG'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Identificacion Cliente Origen'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange=''pull_multi_value3(this.value)'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_ORD_TIPO is not null and ',
':P29_ORD_TIPO = pq_constantes.fn_cv_ord_tipo_traspaso_cli'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260882767908360935)
,p_name=>'P29_NOMBRE_CLIENTE_ORIGEN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Nombre Cliente Origen'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_ORD_TIPO is not null and ',
':P29_ORD_TIPO = pq_constantes.fn_cv_ord_tipo_traspaso_cli'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260882958718360935)
,p_name=>'P29_LINEA2'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'__________'
,p_source=>'__________________________________________________________________________________'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_ORD_TIPO is not null and ',
':P29_ORD_TIPO = pq_constantes.fn_cv_ord_tipo_traspaso_cli'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260883160432360935)
,p_name=>'P29_TOTAL_FACTURA'
,p_item_sequence=>637
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Total Factura'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260883369216360935)
,p_name=>'P29_TVE_ID_ORIGINAL'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Tve Id Original'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>unistr('Utilizado para mantener el termino de venta que vino como par\00BF\00BFmetro y que permita cambiar a contado, tarj credito y credito propio el t\00BF\00BFrmino de venta')
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260883565839360936)
,p_name=>'P29_POL_ID'
,p_item_sequence=>23
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>unistr('Pol\00BF\00BFtica')
,p_source=>'pol_id'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LV_POLITICAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_politicas(:f_emp_id,:f_uge_id,:P25_CLI_ID,:P20_TVE_ID, :F_AGE_ID_AGENCIA, :F_SEG_ID, :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'p29_cli_id'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P29_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260883755190360936)
,p_name=>'P29_DIR_ID'
,p_item_sequence=>21
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Dir Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260883974863360936)
,p_name=>'P29_CAT_ID'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cat Id'
,p_source=>'return pq_ven_listas.fn_retorna_cat_id_tipo_stock(:P29_ITE_SKU_ID);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260884163932360937)
,p_name=>'P29_BPR_ID'
,p_item_sequence=>802
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Bpr Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260884380247360937)
,p_name=>'P29_X'
,p_item_sequence=>38
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'X'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260884562094360937)
,p_name=>'P29_BODEGA'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_item_default=>'pq_ven_listas.fn_retorna_bodega_xdefecto(:P25_AGE_ID_AGENCIA,:P20_ERROR);'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_BODEGAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov VARCHAR2(8000);',
'BEGIN',
'    lv_lov := pq_ven_listas.fn_lov_bodegas(:P25_AGE_ID_AGENCIA,',
'                                           :f_emp_id,',
'                                           :P25_TSE_ID,',
'                                           :P0_ERROR',
');',
'    RETURN(lv_lov);',
'END;',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_POL_ID IS NOT NULL',
'AND :P29_TVE_ID IS NOT NULL'))
,p_read_only_when=>'P29_ORD_ID'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260884773704360937)
,p_name=>'P29_DESCRIPCION_LARGA'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_prompt=>'Item'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P29_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260884967846360937)
,p_name=>'P29_ESTADO_DES'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260885182904360938)
,p_name=>'P29_ORD_FECHA'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Fecha'
,p_source=>'SYSDATE'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260885378299360938)
,p_name=>'P29_ENTRADA_MINIMA'
,p_item_sequence=>634
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>unistr('Entrada M\00BF\00BFnima')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260885556150360938)
,p_name=>'P29_CANTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P29_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P29_ORD_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260885982740360938)
,p_name=>'P29_COT_ID'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>unistr('Cotizaci\00BF\00BFn #')
,p_pre_element_text=>'<b><font COLOR="#990000">'
,p_post_element_text=>'</font>'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260886163303360939)
,p_name=>'P29_AGE_ID_AGENCIA'
,p_item_sequence=>27
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_item_default=>'pq_ven_listas.fn_retorna_agencia(:f_uge_id);'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Age Id Agencia'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260886366641360939)
,p_name=>'P29_ERROR'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_imp.id(260866351403360894)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260886551912360939)
,p_name=>'P29_CLIENTE_EXISTE'
,p_item_sequence=>28
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Cliente Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260886775791360939)
,p_name=>'P29_TIPO_DESCTO'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_item_default=>'0'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P29_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260886955423360939)
,p_name=>'P29_COM_ID'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Orden #'
,p_pre_element_text=>'<b><font COLOR="#990000">'
,p_post_element_text=>'</font>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P29_COM_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P29_ORD_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260887162918360939)
,p_name=>'P29_ENTRADA_CRE'
,p_item_sequence=>702
,p_item_plug_id=>wwv_flow_imp.id(260860154843360882)
,p_prompt=>'Entrada Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260887367555360940)
,p_name=>'P29_PLAZO_CRE'
,p_item_sequence=>742
,p_item_plug_id=>wwv_flow_imp.id(260860154843360882)
,p_prompt=>'Plazo Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260887552296360940)
,p_name=>'P29_CLI_ID_ORIGEN'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>unistr('C\00BF\00BFdigo')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_ORD_TIPO is not null and ',
':P29_ORD_TIPO = pq_constantes.fn_cv_ord_tipo_traspaso_cli'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260887775006360940)
,p_name=>'P29_FECHA_CORTE_CRE'
,p_item_sequence=>32
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Fecha Corte Cre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P29_TVE_ID = 2'
,p_display_when2=>'SQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260887955767360940)
,p_name=>'P29_VALOR_FINANCIAR_CRE'
,p_item_sequence=>712
,p_item_plug_id=>wwv_flow_imp.id(260860154843360882)
,p_prompt=>'Valor Financiar Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260888156591360940)
,p_name=>'P29_CUOTA_CRE'
,p_item_sequence=>732
,p_item_plug_id=>wwv_flow_imp.id(260860154843360882)
,p_prompt=>'Cuota Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260888351738360941)
,p_name=>'P29_TASA_CRE'
,p_item_sequence=>722
,p_item_plug_id=>wwv_flow_imp.id(260860154843360882)
,p_prompt=>'Tasa Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260888577881360941)
,p_name=>'P29_COT_ID_VAR'
,p_item_sequence=>33
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260888760661360941)
,p_name=>'P29_PV_ERROR'
,p_item_sequence=>34
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260888981643360941)
,p_name=>'P29_TRX_ID_ESTADO'
,p_item_sequence=>35
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Trx Id Estado'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260889176371360941)
,p_name=>'P29_TRX_ID'
,p_item_sequence=>29
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260889376812360942)
,p_name=>'P29_DESCTO_LINEA'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(260854357306360854)
,p_item_default=>'0'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P29_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260889573276360942)
,p_name=>'P29_ENTRADA'
,p_item_sequence=>633
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Entrada'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260889752491360942)
,p_name=>'P29_PLAN_TARJETA'
,p_item_sequence=>25
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Plan Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_TVE_ID = pq_constantes.fn_cn_tve_id_tarjeta_credito',
'and :P29_POL_ID is not null'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260889983411360942)
,p_name=>'P29_ORD_TIPO'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Ord Tipo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260890173572360942)
,p_name=>'P29_PEKAS'
,p_item_sequence=>632
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Dolares por Pekas '
,p_format_mask=>'999G999G999G999G990D00'
,p_source=>'return to_char(pq_ven_comunes.fn_obtener_descuento_pekas(:P29_CLI_ID,:P29_PLAZO_FACTURA,:P29_POL_ID,:P29_POCE_NACIONAL),''999,999,999,999.99'');'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260890367854360943)
,p_name=>'P29_POCE_NACIONAL'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Poce Nacional'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260890567393360943)
,p_name=>'P29_PLAZO_FACTURA'
,p_item_sequence=>26
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_prompt=>'Plazo Factura'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P29_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P29_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260890763047360943)
,p_name=>'P29_DESCTO_FACTURA'
,p_item_sequence=>635
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Descuento Factura'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260890983246360943)
,p_name=>'P29_CONT'
,p_item_sequence=>639
,p_item_plug_id=>wwv_flow_imp.id(260853974500360852)
,p_prompt=>'Cont'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260891166548360943)
,p_name=>'P29_PLAZO_ANT'
,p_item_sequence=>31
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Plazo Ant'
,p_source=>':P29_PLAZO_FACTURA'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(260891359693360944)
,p_name=>'P29_COND_CAB'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(260854179108360853)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cond Cab'
,p_source=>':P29_POL_ID||''-''||:P29_TVE_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(260891661991360951)
,p_computation_sequence=>10
,p_computation_item=>'P29_PLAZO_FACTURA'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  Ln_valor number(3) := 0;',
'begin',
'  if :P29_TVE_ID = 2 then',
'       Ln_Valor := 12;',
'  else',
'      Ln_Valor := 0;',
'  end if;',
'   return  Ln_Valor;',
'end;'))
,p_compute_when=>':P29_TVE_ID is not null and :P29_COND_CAB != :P29_POL_ID||''-''||:P29_TVE_ID'
,p_compute_when_text=>'PLSQL'
,p_compute_when_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(260891857216360954)
,p_computation_sequence=>20
,p_computation_item=>'P0_DESPLIEGA_AUT'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    if :P29_ENTRADA is null then',
'        return ''N'';',
'    end if;',
'    if :P29_ENTRADA_MINIMA > nvl(:P29_ENTRADA,0) then',
'        return ''S'';',
'    end if;',
'end;'))
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(260892082097360954)
,p_computation_sequence=>30
,p_computation_item=>'P0_TRX_AUT'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_computation=>'86'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(260892271687360955)
,p_validation_name=>'VALIDA_PEKAS'
,p_validation_sequence=>10
,p_validation=>'nvl(:P29_DESCTO_PUNTOS,0) <= to_number(nvl(:P29_PEKAS,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'No puede exceder del limite de pekas'
,p_validation_condition=>'P29_DESCTO_PUNTOS'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(260892461422360961)
,p_validation_name=>'VALIDA ENTRADA'
,p_validation_sequence=>20
,p_validation=>'nvl(:P29_ENTRADA,0) > to_number(nvl(:P29_ENTRADA_MINIMA,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Entrada no puede ser menor a la minima, Solicite Autorizacion'
,p_validation_condition=>':P29_ENTRADA is not null and :P0_DESPLIEGA_AUT = ''S'' and :P0_AUTORIZACION is null'
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(260878263224360922)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(260892651975360961)
,p_validation_name=>'VALIDA_AUTORIZACION'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   Lv_Autorizacion  varchar2(100);',
'begin',
'  FOR reg_usu in 1..APEX_APPLICATION.G_F30.COUNT LOOP',
'    return pq_asdm_seguridad.fn_verifica_autorizacion(',
'                          to_number(APEX_APPLICATION.G_F30(reg_usu)),',
'                          :P0_TRX_AUT,',
'                          :P29_IDENTIFICACION,',
'                          :P29_ENTRADA,:P0_AUTORIZACION,',
'                          Lv_Autorizacion,:P29_ERROR);',
'  END LOOP;',
'end;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_BOOLEAN'
,p_error_message=>'Autorizacion no valida, solicitela nuevamente '
,p_validation_condition=>'P0_AUTORIZACION'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(260892880296360961)
,p_validation_name=>'P29_AGE_ID_AGENTE'
,p_validation_sequence=>40
,p_validation=>'P29_AGE_ID_AGENTE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('El \00BF\00BFtem tiene que tener un vendor asignado')
,p_validation_condition=>'pr_cargar_detalle'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(260879281754360930)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(260893069292360961)
,p_validation_name=>'Cantidad'
,p_validation_sequence=>50
,p_validation=>':p29_cantidad > 0 and :p29_cantidad is not null;'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese una cantidad correcta'
,p_validation_condition=>'pr_cargar_detalle'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(260885556150360938)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(260893260569360961)
,p_validation_name=>'Cantidad stock'
,p_validation_sequence=>60
,p_validation=>'to_number(:p29_cantidad) <= to_number(:P29_STOCK);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'La cantidad debe ser menor al Stock'
,p_validation_condition=>':p29_cat_id = pq_constantes.fn_cn_cat_stock'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(260885773991360938)
,p_associated_item=>wwv_flow_imp.id(260885556150360938)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260895578485360977)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_DATOS_CLIENTES'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_listas.pr_datos_clientes(',
':f_uge_id,                 ',
':f_emp_id,                 ',
':P29_IDENTIFICACION, ',
':P29_NOMBRE,                 ',
':P29_CLI_ID,',
':P29_CORREO,',
':P29_DIRECCION,',
':p29_TIPO_DIRECCION,',
':P29_TELEFONO,',
':P29_TIPO_TELEFONO,',
':P29_CLIENTE_EXISTE,',
':P29_DIR_ID,',
':P29_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>228642427215596051
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260893978179360969)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ING_CABECERA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'    pq_ven_comunes.pr_ing_fac_cab(:f_emp_id,',
'                                  :p29_pol_id,',
'                                  ''0'', --llave',
'                                    pq_constantes.fn_cn_ttr_id_comprobante,',
'                                  :p29_tve_id,',
'                                  :p29_plazo_factura,',
'                                  :p29_plan_tarjeta);',
'    pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                               :P29_POL_ID,',
'                                                pq_constantes.fn_cn_ttr_id_comprobante,',
'                                               :P29_DESCTO_PUNTOS,',
'                                               :P29_DESCTO_FACTURA,',
'                                               :P29_TVE_ID,',
'                                               :P29_PLAN_TARJETA,',
'                                               :P29_ENTRADA,',
'                                               :P29_PLAZO_FACTURA,',
'                                               :P29_TOTAL_FACTURA,',
'                                               :P29_POCE_NACIONAL,',
'                                               :P29_ENTRADA_MINIMA,',
'                                               :P29_PLAZO_CRE,',
'                                               :P29_ENTRADA_CRE,',
'                                               :P29_VALOR_FINANCIAR_CRE,',
'                                               :P29_TASA_CRE,	',
'                                               :P29_CUOTA_CRE);',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P29_POL_ID is not null and :P29_TVE_ID is not null and (',
':P29_COND_CAB != :P29_POL_ID||''-''||:P29_TVE_ID or :P29_PLAZO_ANT != :P29_PLAZO_FACTURA)',
''))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>228640826909596043
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260894158267360969)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare ',
'    Ln_total_factura number(15,2);',
'begin',
' :p29_error := null;',
'  pq_ven_comunes.pr_carga_coleccion_detalle(:F_EMP_ID, ',
'                                                 pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                 :P29_ITE_SKU_ID,',
'                                                  null, --ITE_SKU_REL,',
'                                                  null, --TIPO_REL_ID,',
'                                                  null, --TIPO_ITE_COMBO,',
'                                                  null, --RIT_PORCENTAJE,',
'                                                 :P29_IEB_ID,',
'                                                 :P29_ESTADO,',
'                                                 :P29_POL_ID,',
'                                                 :P29_ORD_FECHA,',
'                                                 :P29_CANTIDAD,',
'                                                 :P29_TIPO_DESCTO,',
'                                                 :P29_DESCTO_LINEA,',
'                                                 :P29_DESCTO_PUNTOS,',
'                                                 :P29_DESCTO_FACTURA,',
'                                                 :P29_TVE_ID,',
'                                                 :P29_PLAN_TARJETA,',
'                                                 :P29_ENTRADA,',
'                                                 :P29_PLAZO_FACTURA,',
'                                                 :P29_AGE_ID_AGENTE,',
'                                                 :P29_AGE_ID_AGENCIA,',
'                                                 Ln_total_Factura,',
'                                                 :P29_POCE_NACIONAL,',
'                                                 :P29_ENTRADA_MINIMA,',
'                                                 :P29_BODEGA);',
'',
' pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                               :P29_POL_ID,',
'                                                pq_constantes.fn_cn_ttr_id_comprobante,',
'                                               :P29_DESCTO_PUNTOS,',
'                                               :P29_DESCTO_FACTURA,',
'                                               :P29_TVE_ID,',
'                                               :P29_PLAN_TARJETA,',
'                                               :P29_ENTRADA,',
'                                               :P29_PLAZO_FACTURA,',
'                                               :P29_TOTAL_FACTURA,',
'                                               :P29_POCE_NACIONAL,',
'                                               :P29_ENTRADA_MINIMA,',
'                                               :P29_PLAZO_CRE,',
'                                               :P29_ENTRADA_CRE,',
'                                               :P29_VALOR_FINANCIAR_CRE,',
'                                               :P29_TASA_CRE,	',
'                                               :P29_CUOTA_CRE);',
'',
'  :P29_ite_sku_id := null;',
'  :P29_DESCRIPCION_LARGA := null;',
'  :P29_age_id_agente := null;',
'  :P29_cantidad  := null;',
'  :P29_stock  := null;',
'  :P29_estado  := null;',
'  :P29_ESTADO_DES := null;',
'  :P29_SEQ_ID := null;',
'  :P29_TOTAL_FACTURA := Ln_total_factura;',
'',
'',
'',
'exception when OTHERS then',
'  :P29_ERROR := sqlerrm;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260885773991360938)
,p_process_when=>'p29_seq_id'
,p_process_when_type=>'ITEM_IS_NULL'
,p_internal_uid=>228641006997596043
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260894365973360970)
,p_process_sequence=>15
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_RECALCULAR_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    ln_desc_factura_ant NUMBER(11, 4) := :p29_descto_factura;',
'    lv_autorizacion     VARCHAR2(100);',
'    lb_resultado        BOOLEAN := TRUE;',
'BEGIN',
'    :p29_error := NULL;',
'    IF :p29_total_factura_ing IS NOT NULL THEN',
'        :p29_descto_factura := NULL;',
'        pq_ven_comunes.pr_ajusta_precio(:f_emp_id,',
'                                        :p29_pol_id,',
'                                        :p29_tve_id,',
'                                        :p29_plan_tarjeta,',
'                                        :p29_entrada,',
'                                        :p29_plazo_factura,',
'                                        :p29_total_factura,',
'                                        :p29_total_factura_ing,',
'                                        :p29_descto_puntos,',
'                                        :p29_descto_factura,',
'                                        :p29_poce_nacional,',
'                                        :p29_entrada_minima,',
'                                               :P29_PLAZO_CRE,',
'                                               :P29_ENTRADA_CRE,',
'                                               :P29_VALOR_FINANCIAR_CRE,',
'                                               :P29_TASA_CRE,	',
'                                               :P29_CUOTA_CRE,',
'                                        :p29_cont,',
'                                        :p29_error);',
'    END IF;',
'    IF :p29_error IS NOT NULL',
'       OR :p29_total_factura_ing IS NULL THEN',
'        :p29_descto_factura := ln_desc_factura_ant;',
'        pq_ven_comunes.pr_recalcula_coleccion_detalle(:f_emp_id,',
'                                                      :p29_pol_id,',
'                                                      pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                      :p29_descto_puntos,',
'                                                      :p29_descto_factura,',
'                                                      :p29_tve_id,',
'                                                      :p29_plan_tarjeta,',
'                                                      :p29_entrada,',
'                                                      :p29_plazo_factura,',
'                                                      :p29_total_factura,',
'                                                      :p29_poce_nacional,',
'                                                      :p29_entrada_minima,',
'                                               :P29_PLAZO_CRE,',
'                                               :P29_ENTRADA_CRE,',
'                                               :P29_VALOR_FINANCIAR_CRE,',
'                                               :P29_TASA_CRE,	',
'                                               :P29_CUOTA_CRE);',
'        IF :p0_despliega_aut = ''S'' THEN',
'            :p0_despliega_aut := ''N'';',
'        END IF;',
'    END IF;',
'END;',
'',
':p29_ieb_id            := NULL;',
':p29_ite_sku_id        := NULL;',
':p29_age_id_agente     := NULL;',
':p29_cot_id            := NULL;',
':p29_cantidad          := NULL;',
':p29_stock             := NULL;',
':p29_estado            := NULL;',
':p29_seq_id            := NULL;',
':p29_total_factura_ing := NULL;',
':p0_autorizacion       := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260878263224360922)
,p_internal_uid=>228641214703596044
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260894567330360973)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACTUALIZA_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    Ln_total_factura number(15,2);',
'BEGIN',
'/*',
'',
' pq_ven_comunes.pr_elimina_coleccion_detalle(:p29_seq_id || '':'' ||',
'                                                :p29_ite_sku_id || '':'' ||',
'                                                :p29_ieb_id || '':'' ||',
'                                                :p29_estado);',
'',
'  pq_ven_comunes.pr_carga_coleccion_detalle(:F_EMP_ID, ',
'                                                 pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                 :P29_ITE_SKU_ID,',
'                                                  null, --ITE_SKU_REL,',
'                                                  null, --TIPO_REL_ID,',
'                                                  null, --TIPO_ITE_COMBO,',
'                                                  null, --RIT_PORCENTAJE,',
'                                                 :P29_IEB_ID,',
'                                                 :P29_ESTADO,',
'                                                 :P29_POL_ID,',
'                                                 :P29_ORD_FECHA,',
'                                                 :P29_CANTIDAD,',
'                                                 :P29_TIPO_DESCTO,',
'                                                 :P29_DESCTO_LINEA,',
'                                                 :P29_DESCTO_PUNTOS,',
'                                                 :P29_DESCTO_FACTURA,',
'                                                 :P29_TVE_ID,',
'                                                 :P29_PLAN_TARJETA,',
'                                                 :P29_ENTRADA,',
'                                                 :P29_PLAZO_FACTURA,',
'                                                 :P29_AGE_ID_AGENTE,',
'                                                 :P29_AGE_ID_AGENCIA,',
'                                                 Ln_total_factura,',
'                                                 :P29_POCE_NACIONAL,',
'                                                 :P29_ENTRADA_MINIMA,',
'                                                 :P29_BODEGA);',
'',
'',
'pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                               :P29_POL_ID,',
'                                                pq_constantes.fn_cn_ttr_id_comprobante,',
'                                               :P29_DESCTO_PUNTOS,',
'                                               :P29_DESCTO_FACTURA,',
'                                               :P29_TVE_ID,',
'                                               :P29_PLAN_TARJETA,',
'                                               :P29_ENTRADA,',
'                                               :P29_PLAZO_FACTURA,',
'                                               :P29_TOTAL_FACTURA,',
'                                               :P29_POCE_NACIONAL,',
'                                               :P29_ENTRADA_MINIMA,',
'                                               :P29_PLAZO_CRE,',
'                                               :P29_ENTRADA_CRE,',
'                                               :P29_VALOR_FINANCIAR_CRE,',
'                                               :P29_TASA_CRE,	',
'                                               :P29_CUOTA_CRE);',
'*/',
':p29_ieb_id        := NULL;',
':p29_ite_sku_id    := NULL;',
':p29_age_id_agente := NULL;',
':p29_cot_id        := NULL;',
':p29_cantidad      := NULL;',
':p29_stock         := NULL;',
':p29_estado        := NULL;',
':p29_seq_id        := NULL;',
':P29_TOTAL_FACTURA := Ln_total_factura;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260885773991360938)
,p_process_when=>'p29_seq_id'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>228641416060596047
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260894762655360973)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'PR_LIMPIAR'
,p_attribute_01=>'CLEAR_CACHE_FOR_PAGES'
,p_attribute_04=>'25'
,p_process_when_button_id=>wwv_flow_imp.id(260876466546360910)
,p_internal_uid=>228641611385596047
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260893559504360968)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR_COLECCIONES'
,p_process_sql_clob=>'pq_ven_comunes.pr_elimina_coleccion;'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260876466546360910)
,p_internal_uid=>228640408234596042
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260893762439360969)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINA_LINEA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'    FOR i IN 1 .. apex_application.g_f50.COUNT LOOP',
'        pq_ven_comunes.pr_elimina_coleccion_detalle(apex_application.g_f50(i));',
'    END LOOP;',
'',
'    pq_ven_comunes.pr_recalcula_coleccion_detalle(:f_emp_id,',
'                                                  :p29_pol_id,',
'                                                  pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                  :p29_descto_puntos,',
'                                                  :p29_descto_factura,',
'                                                  :p29_tve_id,',
'                                                  :p29_plan_tarjeta,',
'                                                  :p29_entrada,',
'                                                  :p29_plazo_factura,',
'                                                  :p29_total_factura,',
'                                                  :p29_poce_nacional,',
'                                                  :p29_entrada_minima,',
'                                               :P29_PLAZO_CRE,',
'                                               :P29_ENTRADA_CRE,',
'                                               :P29_VALOR_FINANCIAR_CRE,',
'                                               :P29_TASA_CRE,	',
'                                               :P29_CUOTA_CRE);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260878455272360922)
,p_internal_uid=>228640611169596043
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260895178418360976)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR_COM_ID'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P29_COM_ID := NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260876255793360906)
,p_internal_uid=>228642027148596050
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260894978141360973)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABAR_FACTURA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'declare',
'   Ln_eje_eve_id number;',
'   Lv_Error varchar2(250);',
'   cursor cu_autorizaciones is',
'       select c001 usu_aut, c002 ttr_id, c003 valor, c004 Est_aut',
'       from apex_collections ',
'       where collection_name =  pq_asdm_seguridad.cv_coleccionAut;',
'begin',
'   Lv_Error := null;',
'   pq_swi_gen.pr_iniciar_ejecucion_evento(19, Ln_eje_eve_id, Lv_Error);',
'   if Lv_Error is null then ',
'   --Graba transaccion comprobantes',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_trx_id'',:P0_TRX_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_emp_id'',:F_EMP_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_uge_id'',:F_UGE_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_usu_id'',:F_USER_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_ttr_id'',pq_constantes.fn_cn_ttr_id_comprobante, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pd_trx_fecha_ejecucion'',TO_DATE(sysdate,''DD/MM/YYYY HH24:MI:SS'') ,Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_trx_estado_registro'',pq_constantes.fn_cv_emp_estado_reg_activo, Lv_Error);',
'   --Graba comprobantes',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_id'',:P29_COM_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_tve_id'',:P29_TVE_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_caj_id'',:P0_CAJ_ID,  Lv_Error);       ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_pol_id'',:P29_POL_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_cli_id'',:P29_CLI_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_dir_id_enviar_factura'',:P29_DIR_ID_ENVIAR_FACTURA, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_com_tipo'', :P29_COM_TIPO,  Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pd_com_fecha'',:P29_COM_FECHA, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_numero'',:P29_COM_NUMERO,Lv_Error);     ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_age_num_sri'',:P0_AGE_NUM_SRI,Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_caj_num_sri'',:P0_CAJ_NUM_SRI,Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_age_id_agente'',:P29_AGE_ID_AGENTE, Lv_Error); ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_age_id_agencia'',:P29_AGE_ID_AGENCIA, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_ord_id'',:P29_ORDEN_VENTA, Lv_Error); ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_com_observacion'',:P29_COM_OBSERVACIONES, Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_saldo'',:P29_SALDO, Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_plazo'',:P29_PLAZO_FACTURA, Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_ede_id'',:P29_PLAN_TARJETA, Lv_Error);       ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''PN_COM_NRO_IMPRESION'',NULL, Lv_Error);       ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_error'',null, Lv_Error);',
'         ',
'      if Lv_Error is null then',
'           pq_swi_gen.pr_ejecutar_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'',
'      if Lv_Error is null then',
'          pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id, ''pn_com_id'', :p29_COM_ID, Lv_Error);',
'          pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id, ''pn_trx_id'', :P29_TRX_ID, Lv_Error);',
'          pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id, ''pv_error'',  :P29_ERROR, Lv_Error);',
'      end if;',
'',
'      if Lv_Error is null then',
'          pq_swi_gen.pr_cerrar_ejecucion_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'   end if;',
'   for reg_aud in cu_autorizaciones loop',
'         insert into kseg_e.kseg_autorizaciones values (null,sysdate, :P29_TRX_ID, reg_aud.valor, :F_USER_ID, ',
'                      reg_aud.usu_aut, reg_aud.ttr_id, reg_aud.est_aut);',
'   end loop;',
'   :P29_ERROR := Lv_Error;',
'   if :P29_ERROR is not null then',
'      raise_application_error(-20010,Lv_error);',
'   end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(260877278157360917)
,p_internal_uid=>228641826871596047
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260893357009360962)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_items'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_listas_caja.pr_datos_items_fac_servicios(',
':F_EMP_ID,',
':F_USER_ID,',
':F_UGE_ID,               ',
':P29_ITE_SKU_ID,          ',
':P29_CLI_ID,            ',
':P29_DESCRIPCION_LARGA,',
':P29_AGE_ID_AGENTE,              ',
':P29_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'item'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>228640205739596036
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260895780340360977)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':p0_ttr_id :=  pq_ven_listas_caja.fn_tip_comprobante_por_seg_id(:F_UGE_ID);',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
':F_EMP_ID,',
':F_PCA_ID,',
':P0_TTR_ID,',
':P0_TTR_DESCRIPCION, ',
':P0_PERIODO,',
':P0_DATFOLIO,',
':P0_FOL_SEC_ACTUAL,',
':P0_CAJ_NUM_SRI,',
':P0_AGE_NUM_SRI,',
':P0_CAJ_ID,',
':P0_PV_ERROR);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>228642629070596051
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(260895352878360977)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_llama_eliminar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comunes.pr_elimina_coleccion_detalle(:p29_seq_id);',
':P29_EMP_ID := null;',
':P29_ieb_id := null;',
':P29_ite_sku_id := null;',
'--:P29_age_id_agente := null;',
'--:P29_pol_id_det := null;',
'--:P29_cot_id   := null;',
':P29_cantidad  := null;',
':P29_stock  := null;',
'--:P29_estado  := null;',
':P29_costo  := null;',
':P29_precio  := null;',
':P29_iva  := null;',
':P29_valor  := null;',
':P29_SEQ_ID := null;',
':P29_ERROR := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'Eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>228642201608596051
);
wwv_flow_imp.component_end;
end;
/
