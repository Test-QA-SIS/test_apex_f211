prompt --application/pages/page_00131
begin
--   Manifest
--     PAGE: 00131
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>131
,p_name=>'APLICACION CUOTA GRATIS'
,p_alias=>'APLICACION_CUOTA_GRATIS'
,p_step_title=>'APLICACION CUOTA GRATIS'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(320558375686316213)
,p_plug_name=>'CUOTAS GRATIS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(320621164883233627)
,p_name=>'DATOS CLIENTE'
,p_parent_plug_id=>wwv_flow_imp.id(320558375686316213)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.per_nro_identificacion,',
'       b.nombre_completo,',
'       a.cli_id,',
'       a.cxc_fecha_adicion fecha_venta,',
'       (SELECT c.uge_num_sri || ''-'' || c.pue_num_sri || ''-'' || c.com_numero',
'          FROM ven_comprobantes c',
'         WHERE c.com_id = a.com_id) folio,',
'       ''PROMOCION '' || d.cpr_cuota_promo || '' CUOTAS GRATIS'' promocion,',
'       d.cpr_cuota_fin || '' CUOTAS DISPONIBLES'' disponible',
'  FROM car_cuentas_por_cobrar a,',
'       v_asdm_datos_clientes  b,',
'       car_cuotas_promo       d',
' WHERE b.cli_id = a.cli_id',
'   AND d.cxc_id = a.cxc_id',
'   AND a.cxc_id = :P131_CXC_ID',
'   and d.cpr_estado_registro = 0'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320621452574233701)
,p_query_column_id=>1
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>1
,p_column_heading=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320621583508233703)
,p_query_column_id=>2
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>2
,p_column_heading=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320621680954233703)
,p_query_column_id=>3
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>3
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320621756397233703)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VENTA'
,p_column_display_sequence=>4
,p_column_heading=>'FECHA_VENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320621883804233703)
,p_query_column_id=>5
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>5
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320621964685233703)
,p_query_column_id=>6
,p_column_alias=>'PROMOCION'
,p_column_display_sequence=>6
,p_column_heading=>'PROMOCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320622057847233703)
,p_query_column_id=>7
,p_column_alias=>'DISPONIBLE'
,p_column_display_sequence=>7
,p_column_heading=>'DISPONIBLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(320750063160726423)
,p_name=>'CARTERA CLIENTE'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C003 credito,',
'C010 Nro_Vencimiento,',
'C011 Fecha_Vencimiento,',
'C012 Valor_cuota,',
'C002 Valor_aplica,',
'c013 nota',
'FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''COL_CUOTA_GRATIS'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320750373976726462)
,p_query_column_id=>1
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>1
,p_column_heading=>'CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320750475464726465)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320750575264726465)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320750662055726465)
,p_query_column_id=>4
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(320750775034726465)
,p_query_column_id=>5
,p_column_alias=>'VALOR_APLICA'
,p_column_display_sequence=>5
,p_column_heading=>'VALOR_APLICA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span style="color:red;font-weight:bold;font-size:15;">#VALOR_APLICA#</span>',
''))
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15322393817503058541)
,p_query_column_id=>6
,p_column_alias=>'NOTA'
,p_column_display_sequence=>6
,p_column_heading=>'NOTA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span style="color:green;font-weight:bold;font-size:15;">#NOTA#</span>',
''))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(320759880792854509)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(320750063160726423)
,p_button_name=>'APLICAR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'APLICAR'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>unistr('javascript:apex.confirm(''UNA VEZ APLICADO EL PAGO NO SE PODR\00C1 REVERSAR. DESEA CONTINUAR?'',''APLICA'');')
,p_button_condition=>'SELECT * FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''COL_CUOTA_GRATIS'''
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(322776055510457246)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(320558375686316213)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:&P131_PAG.:&SESSION.:CUOTA_GRATIS:&DEBUG.:131::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(322044475911990943)
,p_branch_name=>'br_grabar'
,p_branch_action=>'f?p=&APP_ID.:131:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'APLICA'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(320558778631318748)
,p_name=>'P131_CXC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(320558375686316213)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(320558957806318756)
,p_name=>'P131_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(320558375686316213)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(320736753604527144)
,p_name=>'P131_OBSERVACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(320621164883233627)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38525614243693238561)
,p_name=>'P131_PAG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(320558375686316213)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(322047369454056039)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
' ln_age_id_agente number := pq_car_credito_interfaz.fn_retorna_agente_conectado(:f_user_id,',
'                                                                            :f_emp_id);',
'begin',
'pq_asdM_promociones.pr_aplica_cuota_gratis(pn_emp_id         => :f_emp_id,',
'                                           pn_uge_id         => :f_uge_id,',
'                                           pn_usu_id         => :f_user_id,',
'                                           pn_cxc_id         => :p131_cxc_id,',
'                                           pn_age_id_agente  => ln_age_id_agente,',
'                                           pn_age_id_agencia => :f_age_id_agencia,',
'                                           pn_seg_id         => :f_seg_id,',
'                                           pv_error          => :p0_error);',
'',
' pq_ven_comprobantes.pr_redirect(pv_desde        => ''P'',',
'                                        pn_app_id       => 211,',
'                                        pn_pag_id       => NULL,',
'                                        pn_emp_id       => :f_emp_id,',
'                                        pv_session      => :session,',
'                                        pv_token        => :f_token,',
'                                        pn_user_id      => :f_user_id,',
'                                        pn_rol          => :p0_rol,',
'                                        pv_rol_desc     => :p0_rol_desc,',
'                                        pn_tree_rot     => :p0_tree_root,',
'                                        pn_opcion       => :f_opcion_id,',
'                                        pv_parametro    => :f_parametro,',
'                                        pv_empresa      => :f_empresa,',
'                                        pn_uge_id       => :f_uge_id,',
'                                        pn_uge_id_gasto => :f_uge_id_gasto,',
'                                        pv_ugestion     => :f_ugestion,',
'                                        pv_error        => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'CUOTA CANCELADA CORRECTAMENTE'
,p_internal_uid=>289794218184291113
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(320736374476521705)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cuota_gratis'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_promociones.pr_carga_cuota_gratis(pn_cxc_id      => :p131_cxc_id,',
'                                          pv_observacion => :p131_observacion,',
'                                          pv_error       => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'NUEVO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>288483223206756779
);
wwv_flow_imp.component_end;
end;
/
