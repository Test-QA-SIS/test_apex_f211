prompt --application/pages/page_00158
begin
--   Manifest
--     PAGE: 00158
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>158
,p_name=>unistr('\00D3rdenes Pendientes Despacho')
,p_step_title=>unistr('\00D3rdenes Pendientes Despacho')
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(349280989612710642779)
,p_plug_name=>'Facturas pendientes despacho'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(349280989685223642780)
,p_plug_name=>'Listado Materiales'
,p_parent_plug_id=>wwv_flow_imp.id(349280989612710642779)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524253738046668)
,p_plug_display_sequence=>40
,p_plug_new_grid=>true
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN p.per_tipo_identificacion IN (''CED'', ''PAS'') THEN',
'          (p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'          p.per_primer_apellido || '' '' || p.per_segundo_apellido)',
'         ELSE',
'          nvl(p.per_razon_social,',
'              p.per_primer_nombre || '' '' || p.per_segundo_nombre)',
'       END nombre,',
'							op.cli_id,',
'       op.com_id,',
'       op.com_fecha,',
'							op.pol_id,',
'							op.num_dias',
'  FROM asdm_e.inv_pob_despachos_pendientes op,',
'       asdm_agencias                       a,',
'       asdm_clientes                       c,',
'       asdm_personas                       p',
' WHERE op.uge_id = a.uge_id',
'   AND op.uge_id = :F_UGE_ID',
'   AND op.cli_id = c.cli_id',
'   AND c.per_id = p.per_id;'))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(349280989771537642781)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'ILIMA'
,p_internal_uid=>349248736620267877855
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(117893429161479496290)
,p_db_column_name=>'NOMBRE'
,p_display_order=>10
,p_column_identifier=>'AW'
,p_column_label=>'Nombres'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(117893427323560496282)
,p_db_column_name=>'COM_ID'
,p_display_order=>20
,p_column_identifier=>'AQ'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(117893427621422496289)
,p_db_column_name=>'CLI_ID'
,p_display_order=>30
,p_column_identifier=>'AR'
,p_column_label=>'Cli id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(117893428017180496289)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>40
,p_column_identifier=>'AS'
,p_column_label=>'Fecha Comprobante'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(117893428367054496289)
,p_db_column_name=>'POL_ID'
,p_display_order=>50
,p_column_identifier=>'AT'
,p_column_label=>'Politica'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(117893428779409496290)
,p_db_column_name=>'NUM_DIAS'
,p_display_order=>60
,p_column_identifier=>'AV'
,p_column_label=>unistr('N\00FAmero D\00EDas')
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(349540806657698221889)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1178611764'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'NOMBRE:CLI_ID:COM_ID:POL_ID:COM_FECHA:NUM_DIAS:'
,p_sort_column_1=>'MMI_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(349280993790861642821)
,p_plug_name=>'Detalle Material'
,p_parent_plug_id=>wwv_flow_imp.id(349280989612710642779)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526077972046669)
,p_plug_display_sequence=>20
,p_plug_new_grid=>true
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P05_MMI_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(349540824256774588898)
,p_plug_name=>'Procesa Materiales'
,p_parent_plug_id=>wwv_flow_imp.id(349280989612710642779)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>30
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117893433416524496322)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(349540824256774588898)
,p_button_name=>'btn_actualizar_facturas'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Actualizar Facturas Pendientes'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117893431075504496308)
,p_name=>'158_73_P05_MMI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(349280993790861642821)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117893431469012496319)
,p_name=>'158_73_P05_IDOC_NUMBER'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(349280993790861642821)
,p_prompt=>'Idoc number'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117893431861789496320)
,p_name=>'158_73_P05_MATERIAL'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(349280993790861642821)
,p_prompt=>'Material'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117893432304899496321)
,p_name=>'158_73_P05_ESTADO_PROCESAMIENTO'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(349280993790861642821)
,p_prompt=>'Estado procesamiento'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:ERROR_SAP-SIC;ERROR_SAP-SIC,PENDIENTE_SAP-SIC;PENDIENTE_SAP-SIC'
,p_cHeight=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117893432730093496321)
,p_name=>'158_73_P05_MENSAJE_ERROR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(349280993790861642821)
,p_prompt=>'Mensaje Error'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cHeight=>5
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117893433759496496358)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_update_facturas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_age_id NUMBER;',
'BEGIN',
'  -- Call the procedure',
'  BEGIN',
'    SELECT a.age_id',
'      INTO ln_age_id',
'      FROM asdm_agencias a',
'     WHERE a.uge_id = :f_uge_id',
'       AND a.emp_id = :f_emp_id',
'       AND a.age_estado_registro = 0;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      ln_age_id := NULL;',
'  END;',
'',
'  IF ln_age_id IS NOT NULL THEN',
'  ',
'    pq_asdm_comunes.pr_pob_despachos_pendientes(pn_emp_id => :f_emp_id,',
'                                                pn_age_id => ln_age_id,',
'                                                pn_tse_id => :f_seg_id,',
'                                                pc_error  => :p0_error);',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117893433416524496322)
,p_process_when=>'F_UGE_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>117861180608226731432
);
wwv_flow_imp.component_end;
end;
/
