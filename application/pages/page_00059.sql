prompt --application/pages/page_00059
begin
--   Manifest
--     PAGE: 00059
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>59
,p_name=>'Abrir Caja'
,p_step_title=>'Abrir Caja'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240125090221'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(87474253985603000)
,p_name=>'Cajas Abiertas'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.pue_id,',
'       b.pue_nombre,',
'       a.uge_num_est_sri,',
'       b.pue_num_sri,',
'       c.usu_id,',
'       decode(d.pca_estado_pc, ''A'', ''ABIERTA'', ''CERRADA'') estado,',
'       d.pca_fechahora_inicio,',
'       decode(d.pca_token, ''A'', ''ACTIVA'', ''INACTIVA'') actual',
'  from asdm_unidades_gestion       a,',
'       asdm_puntos_emision         b,',
'       asdm_usuario_puntos_emision c,',
'       ven_periodos_caja           d',
' where c.pue_id = b.pue_id',
'   and b.uge_id = a.uge_id',
'   and b.emp_id = a.emp_id',
'   and d.emp_id = b.emp_id',
'   and d.pue_id = b.pue_id',
'   and d.pca_estado_registro = 0',
'   and c.upe_estado_registro = 0',
'   and b.pue_estado_registro = 0',
'   and a.uge_estado_registro = 0',
'   and d.pca_estado_pc = ''A''',
'   and a.uge_id = :F_UGE_ID',
'   and c.usu_id = :f_user_id',
'   and a.emp_id = :f_emp_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149417270119859376)
,p_query_column_id=>1
,p_column_alias=>'PUE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Pue Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(87474573934603087)
,p_query_column_id=>2
,p_column_alias=>'PUE_NOMBRE'
,p_column_display_sequence=>1
,p_column_heading=>'Caja Abierta'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149417366039859376)
,p_query_column_id=>3
,p_column_alias=>'UGE_NUM_EST_SRI'
,p_column_display_sequence=>6
,p_column_heading=>'Uge Num Est Sri'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149417462860859376)
,p_query_column_id=>4
,p_column_alias=>'PUE_NUM_SRI'
,p_column_display_sequence=>7
,p_column_heading=>'Pue Num Sri'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149417580974859376)
,p_query_column_id=>5
,p_column_alias=>'USU_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Usu Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(87478176804656880)
,p_query_column_id=>6
,p_column_alias=>'ESTADO'
,p_column_display_sequence=>2
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(88414859154431214)
,p_query_column_id=>7
,p_column_alias=>'PCA_FECHAHORA_INICIO'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Apertura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(88609858609342117)
,p_query_column_id=>8
,p_column_alias=>'ACTUAL'
,p_column_display_sequence=>4
,p_column_heading=>'Actual'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(87488758674717883)
,p_name=>'Cajas Disponibles'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.pue_id,',
'       b.pue_nombre,',
'       a.uge_num_est_sri,',
'       b.pue_num_sri,',
'       CASE',
'         WHEN (SELECT COUNT(*)',
'                 FROM asdm_movimientos_caja         ca,',
'                      asdm_movimientos_caja_detalle d,',
'                      asdm_puntos_emision           p,',
'                      ven_periodos_caja             pca',
'                WHERE ca.mca_id = d.mca_id',
'                  AND ca.ttr_id IN (9, 23)',
'                  AND p.pue_id = b.pue_id',
'                  AND ca.pca_id = pca.pca_id',
'                  AND EXISTS (SELECT NULL',
'                         FROM asdm_periodos_sin_recap r',
'                        WHERE r.pca_id = pca.pca_id',
'                         AND r.emp_id=:f_emp_id',
'                         AND r.psr_estado_registro = ''0'')',
'                  AND pca.pue_id = p.pue_id',
'                  AND d.tfp_id = :p59_tfp_id -- asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito'')---:P59_TFP_ID--:P59_PAGO_TARJETAS',
'                  AND p.uge_id = ca.uge_id',
'                  AND ca.mca_estado_registro = 0',
'                  AND d.mcd_estado_registro = 0',
'',
'                  AND NOT EXISTS (SELECT NULL',
'                         FROM ven_depositos_recap_det de',
'                        WHERE de.mcd_id = d.mcd_id)) > 0 THEN',
'          ''deposito recap pendiente''',
'         ELSE',
'          ''Verificar depositos recap''',
'       END dep_recap',
'  FROM asdm_unidades_gestion       a,',
'       asdm_puntos_emision         b,',
'       asdm_usuario_puntos_emision c',
' WHERE c.pue_id = b.pue_id',
'   AND b.uge_id = a.uge_id',
'   AND b.emp_id = a.emp_id',
'   AND c.upe_estado_registro = 0',
'   AND b.pue_estado_registro = 0',
'   AND a.uge_estado_registro = 0',
'   AND a.uge_id = :f_uge_id',
'   AND c.usu_id = :f_user_id',
'   AND a.emp_id = :f_emp_id',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
,p_comment=>'Ya no se realiza bloqueo de caja porque se bloquea el rol, 201:0 se ajusta para que bloquee rol (:P59_MOSTRAR IS NULL) ILIMA'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149404367593726278)
,p_query_column_id=>1
,p_column_alias=>'PUE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Pue Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149406882054749315)
,p_query_column_id=>2
,p_column_alias=>'PUE_NOMBRE'
,p_column_display_sequence=>2
,p_column_heading=>'Pue Nombre'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149406974106749325)
,p_query_column_id=>3
,p_column_alias=>'UGE_NUM_EST_SRI'
,p_column_display_sequence=>3
,p_column_heading=>'Uge Num Est Sri'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149407075269749325)
,p_query_column_id=>4
,p_column_alias=>'PUE_NUM_SRI'
,p_column_display_sequence=>4
,p_column_heading=>'Pue Num Sri'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7207838579172354256)
,p_query_column_id=>5
,p_column_alias=>'DEP_RECAP'
,p_column_display_sequence=>5
,p_column_heading=>'Dep recap'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:137:&SESSION.:recap:&DEBUG.:RP:P137_PUE_ID:#PUE_ID#'
,p_column_linktext=>'#DEP_RECAP#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(138533475701011697)
,p_plug_name=>'Abrir Caja con una Fecha Anterior'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(138545168353104205)
,p_plug_name=>'Apertura de Caja'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_plug_display_when_condition=>'ABRIR_CAJA'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(138626069766681663)
,p_plug_name=>'CIERRA_CAJA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P59_FECHA_CARGADA'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(8753991711517870937)
,p_plug_name=>'Ordenes Pendientes de Facturar o Despachar'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>100
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P59_ORDENES_PENDIENTES IS NOT NULL'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(652148868404771138)
,p_name=>'.'
,p_parent_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'',
'c001 Orden,',
'c002 Fecha,',
'c003 vendedor,',
'c004 tipo,',
'c005 estado,',
'--c006 VIGENCIA',
'c007 OBSERVACION',
'       ',
'       ',
'FROM APEX_COLLECTIONS',
'WHERE COLLECTION_NAME=''COLL_ORDENES_VEN_AUX'''))
,p_display_when_condition=>' :P59_MOSTRAR IS NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select o.ord_sec_id',
'  from ven_ordenes o,',
'      /* (SELECT MAX(eor_id) eor_id, ord_id----PBERNAL09/09/2016 SE QUITA ESTE SELECT Y SE COLOCA EN EL WHERE',
'          FROM ven_estados_ordenes eor',
'         WHERE eor.ord_id = O.ord_id',
'         GROUP BY ord_id) e,*/',
'       ven_estados_ordenes t,',
'       ven_tipos_est_ordenes r',
' where-- o.ord_id = e.ord_id----PBERNAL09/09/2016',
'   --and e.eor_id = t.eor_id----PBERNAL09/09/2016',
'',
'   ',
'   ',
'       t.teo_id = r.teo_id',
'     AND o.teo_id = r.teo_id',
'    AND O.ORD_ID=T.ORD_ID',
'',
'',
'   and o.emp_id = :f_emp_id',
'   and o.ord_estado_registro = 0',
'   and o.age_id_agencia = :f_age_id_agencia',
'   and t.teo_id in',
'       (',
'      pq_constantes.fn_retorna_constante(null, ''cn_teo_id_ingresada''),----PBERNAL09/09/2016',
'        pq_constantes.fn_retorna_constante(null, ''cn_teo_id_aprob_comercial''),----PBERNAL09/09/2016',
'        pq_constantes.fn_retorna_constante(null,''cn_teo_id_aprob_financiera'')----PBERNAL09/09/2016',
'       ',
'     /*   :P59_CN_TEO_ID_INGRESADA,',
'        :P59_CN_TEO_ID_APROB_COMERCIAL,',
'        :P59_CN_TEO_ID_APROB_FINANCIERA*/',
'        ',
'        ',
'        )',
'   and to_date(o.ord_fecha, ''dd/mm/yyyy'') < to_date(sysdate, ''dd/mm/yyyy'')'))
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(652151267567828129)
,p_query_column_id=>1
,p_column_alias=>'ORDEN'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(652151377615828129)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(652151472010828129)
,p_query_column_id=>3
,p_column_alias=>'VENDEDOR'
,p_column_display_sequence=>3
,p_column_heading=>'Vendedor'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(652151561593828129)
,p_query_column_id=>4
,p_column_alias=>'TIPO'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(652151658937828129)
,p_query_column_id=>5
,p_column_alias=>'ESTADO'
,p_column_display_sequence=>5
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(3039549731454594767)
,p_query_column_id=>6
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>6
,p_column_heading=>'Observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(8753991023891870930)
,p_name=>'URGENTE '
,p_parent_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'c001 Orden,',
'c002 Fecha,',
'c003 vendedor,',
'c004 tipo,',
'c005 estado,',
'--c006 VIGENCIA',
'c007 OBSERVACION',
'       ',
'       ',
'FROM APEX_COLLECTIONS',
'WHERE COLLECTION_NAME=''COLL_ORDENES_VEN_AUX''',
'AND C006 IS NOT NULL'))
,p_display_when_condition=>' :P59_MOSTRAR=''N'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(8753991126336870931)
,p_query_column_id=>1
,p_column_alias=>'ORDEN'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(8753991209427870932)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(8753991305032870933)
,p_query_column_id=>3
,p_column_alias=>'VENDEDOR'
,p_column_display_sequence=>3
,p_column_heading=>'Vendedor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(8753991431877870934)
,p_query_column_id=>4
,p_column_alias=>'TIPO'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(8753991548299870935)
,p_query_column_id=>5
,p_column_alias=>'ESTADO'
,p_column_display_sequence=>5
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(8753991573015870936)
,p_query_column_id=>6
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>6
,p_column_heading=>'Observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(15333913344794418428)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_button_name=>'Cancelar_ordenes'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar ordenes Pendientes'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=242:11:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(86839067999309677)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(87474253985603000)
,p_button_name=>'CAMBIAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cambiar / Activar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:60:&SESSION.:CAMBIAR:&DEBUG.:60:P60_PUE_ID:&P59_PUE_ID.'
,p_button_condition=>'P59_FECHA_CARGADA'
,p_button_condition_type=>'ITEM_IS_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(87078870601065055)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(87488758674717883)
,p_button_name=>'ABRIR_PERIODO'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Abrir Periodo'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:60:&SESSION.:ABRIR_CAJA:&DEBUG.:60::'
,p_button_condition=>'asdm_p.pq_constantes.fn_retorna_constante(0, ''cn_abrir_caja'') = ''S'' or :P59_MOV_SIN_RECAP = 0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'asdm_p.pq_constantes.fn_retorna_constante(0, ''cn_abrir_caja'') = ''S'' or :P59_MOV_SIN_RECAP = 0',
'',
'asdm_p.pq_constantes.fn_retorna_constante(0, ''cn_abrir_caja'') = ''S'' or '))
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(138533967174018717)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(138533475701011697)
,p_button_name=>'ABRIR_CAJA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'ABRIR PERIODO'
,p_button_position=>'BOTTOM'
,p_button_condition=>'P59_FECHA_CARGADA'
,p_button_condition_type=>'ITEM_IS_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(138562753602185116)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(138545168353104205)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'P59_FECHA'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(138627856045687223)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(138626069766681663)
,p_button_name=>'CIERRE_CAJA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cierre Caja'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(89134863986391387)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(87488758674717883)
,p_button_name=>'PERIODOS_CAJA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Ver Periodos de Caja'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(138538769907066846)
,p_branch_action=>'f?p=&APP_ID.:59:&SESSION.:ABRIR_CAJA:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(138533967174018717)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 09-FEB-2012 16:31 by ACALLE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(138563880745202344)
,p_branch_action=>'f?p=&APP_ID.:59:&SESSION.:GRABAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(138562753602185116)
,p_branch_sequence=>20
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 09-FEB-2012 16:53 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(86836760324203294)
,p_name=>'P59_DATOS'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(87488758674717883)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*select B.PUE_NOMBRE || '' Abierta el '' || a.pca_fechahora_inicio',
'  from ven_periodos_caja a, ASDM_PUNTOS_EMISION B',
' WHERE A.PUE_ID = B.PUE_ID',
'   and a.pue_id=:p59_pue_id',
'  AND a.pca_estado_pc =',
'             pq_constantes.fn_retorna_constante(NULL,',
'                                                ''cv_pca_estado_pc_abierto'')*/',
'select ''Periodo de Caja: ''||pue.pue_nombre||'' Abierta el ''|| pca.pca_fechahora_inicio',
'  from asdm_puntos_emision pue, ven_periodos_caja pca',
' where pca.pue_id = pue.pue_id',
' and pue.pue_id = :p59_pue_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(87183878817410813)
,p_name=>'P59_PCA_ID'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(87488758674717883)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(87184053668413087)
,p_name=>'P59_PUE_ID'
,p_item_sequence=>25
,p_item_plug_id=>wwv_flow_imp.id(87474253985603000)
,p_use_cache_before_default=>'NO'
,p_source=>':p0_pue_id'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(138549351300118233)
,p_name=>'P59_CAJA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(138545168353104205)
,p_prompt=>'Seleccione la Caja'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select pue.pue_nombre, pue.pue_id',
'  from asdm_usuario_puntos_emision upe, asdm_puntos_emision pue',
' where pue.pue_id = upe.pue_id',
'   AND pue.pue_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND upe.upe_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and pue.emp_id = :f_emp_id',
'   and pue.uge_id = :F_UGE_ID',
'   and upe.usu_id = :f_user_id',
'minus',
'select pue.pue_nombre, pue.pue_id',
'  from asdm_usuario_puntos_emision upe,',
'       asdm_puntos_emision         pue,',
'       ven_periodos_caja           pca',
' where pue.pue_id = upe.pue_id',
'   AND pue.pue_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND upe.upe_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   AND pca.pca_estado_pc =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_pca_estado_pc_abierto'')',
'   and upe.usu_id = pca.usu_id',
'   and pca.pue_id = pue.pue_id',
'   and pue.emp_id = :f_emp_id',
'   and pue.uge_id = :F_UGE_ID',
'   and upe.usu_id = :f_user_id'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--Seleccione--'
,p_lov_null_value=>'0'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''ABRIR_CAJA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(138551153855128441)
,p_name=>'P59_FECHA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(138545168353104205)
,p_prompt=>'Fecha Caja'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''ABRIR_CAJA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P59_CAJA_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(138693554602282854)
,p_name=>'P59_FECHA_CARGADA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(138533475701011697)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Fecha Cargada'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PCA_FECHA_SISTEMA FROM VEN_PERIODOS_CAJA',
'WHERE EMP_ID = :f_emp_id',
'AND USU_ID = :f_user_id',
'AND PCA_ID = :p0_periodo_caja'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(139366551935708445)
,p_name=>'P59_LABEL'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(138533475701011697)
,p_item_default=>unistr('Opci\00BF\00BFn para ingresar comprobantes de d\00BF\00BFas anteriores:  Advertencia: No podr\00BF\00BF realizar ninguna transacci\00BF\00BFn realizada el d\00BF\00BFa de hoy hasta que realice el cierre de esta caja')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(652114755316192858)
,p_name=>'P59_CN_TEO_ID_APROB_FINANCIERA'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(87474253985603000)
,p_item_default=>'pq_constantes.fn_retorna_constante(null,''cn_teo_id_aprob_financiera'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Cn Teo Id Aprob Financiera'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(652149680531791784)
,p_name=>'P59_ORDENES_PENDIENTES'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:24;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(652161078067172730)
,p_name=>'P59_MOSTRAR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7207838363749354254)
,p_name=>'P59_TFP_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(87474253985603000)
,p_prompt=>'TFP'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7207838470782354255)
,p_name=>'P59_ALERTA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(87488758674717883)
,p_prompt=>'Alerta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-size:20;color:red;"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P59_MOV_SIN_RECAP > 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when=>'P59_ALERTA'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(8753991842697870938)
,p_name=>'P59_DIAS_ORDENES'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_prompt=>unistr('D\00EDas en Ordenes')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(8753991858550870939)
,p_name=>'P59_DIAS_DESPACHOS'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(8753991711517870937)
,p_prompt=>unistr('D\00EDas en Despachos')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(150123303183532256327)
,p_name=>'P59_MOV_SIN_RECAP'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(87488758674717883)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(150123303424249256329)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_load_constant'
,p_process_sql_clob=>':P59_TFP_ID:=pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito'');'
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'P59_TFP_ID'
,p_process_when_type=>'ITEM_IS_NULL'
,p_internal_uid=>150091050272979491403
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(150123303265659256328)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_recap'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_valor VARCHAR2(4000);',
'',
'BEGIN',
'',
'  SELECT listagg(p.pue_nombre, ''; '') within GROUP(ORDER BY p.pue_id)',
'    INTO lv_valor',
'    FROM asdm_movimientos_caja         a,',
'         asdm_movimientos_caja_detalle d,',
'         asdm_puntos_emision           p,',
'         ven_periodos_caja             pca',
'   WHERE a.mca_id = d.mca_id',
'     AND a.ttr_id IN (9, 23)',
'     AND a.uge_id = :f_uge_id',
'     AND d.tfp_id = :p59_tfp_id -- asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito'')---:P59_TFP_ID--:P59_PAGO_TARJETAS',
'     AND p.uge_id = a.uge_id',
'     AND a.pca_id = pca.pca_id',
'     AND EXISTS (SELECT NULL',
'            FROM asdm_periodos_sin_recap r',
'           WHERE r.pca_id = pca.pca_id',
'             AND r.emp_id = :f_emp_id',
'              AND r.psr_estado_registro =0 ',
'             )',
'     AND pca.pue_id = p.pue_id',
'     AND p.pue_estado_registro = 0',
'     AND a.mca_estado_registro = 0',
'     AND d.mcd_estado_registro = 0',
'     AND NOT EXISTS (SELECT NULL',
'            FROM asdm_e.ven_depositos_recap_det de',
'           WHERE de.mcd_id = d.mcd_id',
'             AND de.drd_estado_registro = 0)',
'     AND rownum = 1;',
'',
'  IF lv_valor IS NULL THEN',
'    :p59_mov_sin_recap := 0;',
'    :p59_alerta        := NULL;',
'  ELSE   ',
'    :p59_mov_sin_recap := 1;',
'    :p59_alerta        := ''LAS CAJAS: '' || lv_valor ||',
'                         '' TIENEN DEPOSITOS RECAP PENDIENTES'';',
'  END IF;',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    :p59_mov_sin_recap := 0;',
'  WHEN OTHERS THEN',
'    :p59_mov_sin_recap := 5;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>150091050114389491402
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(87211976196570924)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_abrir_periodo_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'  pq_ven_movimientos_caja.pr_nuevo_periodo_caja(pn_emp_id => :F_EMP_ID,',
'                                                pn_uge_id => :F_UGE_ID,',
'                                                pn_pue_id => :P59_PUE_ID,',
'                                                pn_usu_id => :F_USER_ID,',
'                                                pn_pca_id => :P59_PCA_ID,',
'                                                pd_fecha_apertura => SYSDATE,',
'                                                pd_fecha_sistema => null,',
'                                                pv_error  => :p0_error);',
'',
' pq_ven_movimientos_caja.pr_cambiar_caja(pn_emp_id => :f_emp_id,',
'                pn_pca_id   => :P59_PCA_ID,',
'                pn_user_id  => :F_USER_ID,',
'                pn_uge_id   => :F_UGE_ID,',
'                pv_error    => :P0_ERROR);',
'',
'',
'/*',
'---agregado por Patricio Bernal para reeviar los documentos electronicos rechazados de la unidad de gestion',
'pq_asdm_docs_xml.pr_reenvia_doc_xml_masivo_uge ( PN_UGE_ID=>:F_UGE_ID,',
'                                               pn_emp_id =>:f_emp_id);',
'*/',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error al Abrir la Caja'
,p_process_when=>'ABRIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Caja Abierta Correctamente'
,p_internal_uid=>54958824926805998
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(87507658552784098)
,p_process_sequence=>40
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cambiar_periodo_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_movimientos_caja.pr_cambiar_caja(pn_emp_id => :f_emp_id,',
'                pn_pca_id   => :P59_PCA_ID,',
'                pn_user_id  => :F_USER_ID,',
'                pn_uge_id   => :F_UGE_ID,',
'                pv_error    => :P0_ERROR);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'No se pudo cambiar de caja'
,p_process_when=>'CAMBIAR_CAJA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Caja Cambiada Correctamente'
,p_internal_uid=>55254507283019172
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(87956968284150341)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'datos caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cv_pca_estado_pc_abierto varchar(10):=pq_constantes.fn_retorna_constante(0,''cv_pca_estado_pc_abierto'');',
'cv_estado_reg_activo varchar(1):=pq_constantes.fn_retorna_constante(0,''cv_estado_reg_activo'');',
'CURSOR cu_periodo_abierto IS',
'      SELECT pca.pca_id, pue.pue_id',
'        FROM asdm_puntos_emision         pue,',
'             asdm_usuario_puntos_emision upe,',
'             ven_periodos_caja           pca',
'       WHERE pue.uge_id = :f_uge_id --cajas de la unidad de gestion',
'         AND pue.pue_id = upe.pue_id',
'         AND upe.usu_id = :f_user_id --todas las cajas asignadas al usuario',
'         AND pca.pue_id = pue.pue_id',
'         AND pca.usu_id = :f_user_id',
'         AND pca.pca_estado_pc = cv_pca_estado_pc_abierto             ',
'         AND pue.pue_estado_registro =cv_estado_reg_activo',
'         AND upe.upe_estado_registro =cv_estado_reg_activo',
'         AND pca.pca_estado_registro =cv_estado_reg_activo',
'         AND pue.emp_id = :f_emp_id',
'         AND pca.emp_id = :f_emp_id           ',
'         AND pca.pca_token = cv_pca_estado_pc_abierto;',
'begin',
' OPEN cu_periodo_abierto;',
'    FETCH cu_periodo_abierto',
'      INTO :p0_periodo_caja, :p0_pue_id; --Si existe un periodo abierto selecciona el id del periodo',
'    IF cu_periodo_abierto%NOTFOUND THEN',
'      null;',
'    end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>55703817014385415
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(652147973900744318)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_COLECCION'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_contador       NUMBER;',
'  cn_tse_id_minoreo NUMBER(2) := pq_constantes.fn_retorna_constante(0,',
'                                                                    ''cn_tse_id_minoreo'');',
'BEGIN',
'',
'  SELECT a.age_vigencia_dias, a.age_vigencia_dias_inv',
'    INTO :p59_dias_ordenes, :p59_dias_despachos',
'    FROM asdm_agencias a',
'   WHERE a.age_id = :f_age_id_agencia;',
'/*',
'  pq_asdm_comunes.pr_crea_ordenes_pendientes(pn_age_id_agencia => :f_age_id_agencia,',
'                                             pn_emp_id         => :f_emp_id);',
'*/',
'  SELECT nvl(COUNT(seq_id), 0)',
'    INTO ln_contador',
'    FROM apex_collections a',
'   WHERE a.collection_name = ''COLL_ORDENES_VEN_AUX''',
'     AND a.c006 IS NOT NULL;',
'',
'  /*raise_application_error(-20001,',
'  ''sale con ''||ln_contador);*/',
'',
'  IF ln_contador > 0 AND :f_seg_id = cn_tse_id_minoreo THEN',
'    :p59_ordenes_pendientes := ''Existen Ordenes Pendientes de Cerrar O Despachar'';',
'    :p59_mostrar            := ''N'';',
'  ELSE',
'  ',
'    SELECT nvl(COUNT(seq_id), 0)',
'      INTO ln_contador',
'      FROM apex_collections a',
'     WHERE a.collection_name = ''COLL_ORDENES_VEN_AUX'';',
'  ',
'    IF ln_contador > 0 AND :f_seg_id = cn_tse_id_minoreo THEN',
'      :p59_ordenes_pendientes := ''Existen Ordenes Pendientes de Facturar O Despachar, Notifique a su Administrador'';',
'      :p59_mostrar            := NULL;',
'    ELSE',
'      :p59_ordenes_pendientes := NULL;',
'      :p59_mostrar            := NULL;',
'    END IF;',
'  ',
'  END IF;',
'',
'EXCEPTION',
'',
'  WHEN OTHERS THEN',
'    :p59_ordenes_pendientes := NULL;',
'    :p59_mostrar            := NULL;',
'    RAISE;',
'  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>619894822630979392
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(138537069430057179)
,p_process_sequence=>10
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_abrir_periodo_fecha'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
' pq_ven_movimientos_caja.pr_nuevo_periodo_caja(pn_emp_id => :F_EMP_ID,',
'                                                pn_uge_id => :F_UGE_ID,',
'                                                pn_pue_id => :P59_CAJA_ID,',
'                                                pn_usu_id => :F_USER_ID,',
'                                                pn_pca_id => :P59_PCA_ID,',
'                                                pd_fecha_apertura => :P59_FECHA,',
'                                                pd_fecha_sistema => :P59_FECHA,',
'                                                pv_error  => :f0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>106283918160292253
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(138649275848872668)
,p_process_sequence=>10
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cierre_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_mca_id number;',
'ln_trx_id number;',
'begin',
'pq_ven_movimientos_caja.pr_cierre_caja(pn_emp_id          =>:f_emp_id,',
'                                                  pn_uge_id          =>:f_uge_id,',
'                                                  pn_user_id         =>:f_user_id,',
'                                                  pn_trx_id          =>ln_trx_id,',
'                                                  pn_uge_id_gasto    =>:f_uge_id_gasto,',
'                                                  pn_mca_id          =>ln_mca_id,',
'                                                  pn_pca_id          =>:f_pca_id,',
'                                                  pv_error           =>:p0_error);',
':p0_periodo_caja := null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(138627856045687223)
,p_process_when=>'CIERRE_CAJA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>106396124579107742
);
wwv_flow_imp.component_end;
end;
/
