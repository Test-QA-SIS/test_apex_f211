prompt --application/pages/page_00201
begin
--   Manifest
--     PAGE: 00201
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>201
,p_name=>'Categorias - Punto de Venta'
,p_step_title=>'Categorias - Punto de Venta'
,p_reload_on_submit=>'A'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(115265002519902999827)
,p_plug_name=>'Categorias'
,p_region_name=>'catego'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apex_item.text(p_idx        => 1,',
'                      p_value      => s.cat_id,',
'                      p_attributes => ''class="cat_id" id="'' || s.cpo_id || ''"'') cat_id,',
'                      ',
'       APEX_ITEM.SELECT_LIST_FROM_QUERY(p_idx        => 1,',
'                      p_value      => s.cat_id,p_query =>''SELECT CASE',
'         WHEN lev = 1 THEN',
'          ''''Linea -> '''' || cat_descripcion_corta',
'       END linea,',
'       cat_id',
'  FROM (SELECT c.*, LEVEL lev',
'          FROM inv_categorias c',
'         WHERE c.emp_id = 1',
'           AND c.cat_estado_registro = 0',
'           AND c.tca_id = 12',
'        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'         START WITH c.cat_id_padre IS NULL)',
' WHERE lev = 1',
' ORDER BY lev ASC'',',
' p_attributes => ''class="cat_id" id="'' || s.cpo_id || ''"'') categoria,',
'       ',
'       ',
'       APEX_ITEM.DATE_POPUP(p_idx => 2,',
'                            p_value => s.cpo_fecha_ins,',
'                            p_date_format     =>''dd/mm/yyyy hh24:mi'',',
'                            p_attributes => ''class="fecha_ins" id="'' ||',
'                                                              s.cpo_id || ''"'',',
'                            p_item_id => s.cpo_fecha_ins) fecha_ins,',
'                                                              ',
'                                                              ',
'       APEX_ITEM.DATE_POPUP(p_idx => 3,',
'                            p_value => s.cpo_fecha_upd,',
'                            p_date_format     =>''DD/MM/YYYY hh24:mi'',',
'                            p_attributes => ''class="fecha_upd" id="'' ||',
'                                                              s.cpo_id || ''"'') fecha_upd,',
'       ',
'                                                           ',
'       apex_item.hidden(p_idx        => 4,',
'                        p_value      => s.cpo_id,',
'                        p_attributes => ''class="cpo_id"'',',
'                        p_item_id    => ''cpo_id_'' || s.cpo_id) cpo_dato,',
'       ',
'       CASE',
'         WHEN s.cat_id IS NULL OR s.cpo_fecha_ins IS NULL  OR s.cpo_fecha_upd IS NULL THEN',
'          ''<p style="color: #ffffff; background-color: #A81010; font-weight:bold">&#171; PENDIENTE</p>''',
'         ELSE',
'          NULL',
'       END pendiente,',
'       ',
'       s.cpo_id cpo_id,      ',
'       s.cpo_id eliminar',
'',
'  FROM asdm_e.ven_categorias_pos s',
' ORDER BY s.cpo_id ASC;',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(115265002624657999828)
,p_max_row_count=>'1000000'
,p_allow_report_saving=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'FPALOMEQUE'
,p_internal_uid=>115232749473388234902
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265002700342999829)
,p_db_column_name=>'CAT_ID'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Cat id'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265002766787999830)
,p_db_column_name=>'FECHA_INS'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Fecha ins'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265002894840999831)
,p_db_column_name=>'FECHA_UPD'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Fecha upd'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265002968538999832)
,p_db_column_name=>'CPO_DATO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Cpo dato'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265003297921999835)
,p_db_column_name=>'ELIMINAR'
,p_display_order=>50
,p_column_identifier=>'G'
,p_column_label=>'Eliminar'
,p_column_link=>'f?p=&APP_ID.:201:&SESSION.:eliminar:&DEBUG.:RP:P201_ELIMINAR:#CPO_ID#'
,p_column_linktext=>'Eliminar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265003094732999833)
,p_db_column_name=>'PENDIENTE'
,p_display_order=>60
,p_column_identifier=>'E'
,p_column_label=>'Pendiente'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(115265003172549999834)
,p_db_column_name=>'CPO_ID'
,p_display_order=>70
,p_column_identifier=>'F'
,p_column_label=>'Cpo id'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(120489771605239161632)
,p_db_column_name=>'CATEGORIA'
,p_display_order=>80
,p_column_identifier=>'H'
,p_column_label=>'Categoria'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(115265021722870053262)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1152327686'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CAT_ID:FECHA_INS:FECHA_UPD:ELIMINAR:PENDIENTE::CATEGORIA'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(120918514177715296397)
,p_application_user=>'ETENESACA'
,p_name=>'Principal'
,p_description=>'Principal'
,p_report_seq=>10
,p_report_alias=>'1208862611'
,p_status=>'PUBLIC'
,p_report_columns=>'CATEGORIA:FECHA_INS:FECHA_UPD:ELIMINAR:PENDIENTE:'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(115265003414435999836)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(115265002519902999827)
,p_button_name=>'BT_ADD_REG'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nueva Categoria'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115265003541081999837)
,p_name=>'P201_LISTA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(115265002519902999827)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115265003639604999838)
,p_name=>'P201_ELIMINAR'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(115265002519902999827)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(115265003679168999839)
,p_name=>'da_change_cat'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.cat_id'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265003800991999840)
,p_event_id=>wwv_flow_imp.id(115265003679168999839)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P201_LISTA'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var z = $(".cpo_id");',
'',
'var',
'  //Checkbox that was changed',
'  $lista = $(this.triggeringElement),',
'  $str_id = $lista.attr(''id''),',
'  //DOM object for APEX Item that holds list.',
'  apexItemIDList = apex.item(this.affectedElements.get(0)),',
'  //Convert comma list into an array or blank array',
'  //Note: Not sure about the "?" syntax see: http://www.talkapex.com/2009/07/javascript-if-else.html',
'  $ids = apexItemIDList.getValue().length === 0 ? [] : apexItemIDList.getValue().split('':''),',
'  //Index of current ID. If it''s not in array, value will be -1',
'  idIndex = $ids.indexOf($lista.val())',
';',
'',
'var flControl = -1;',
'',
'for(var i=0;i<$ids.length;i++){',
'    var tex = $ids[i].split("-");',
'    if(tex[0] == $str_id){',
'        ',
'        if($lista.val() == ''''){',
'            $ids.splice(i, 1);',
'        }else{',
'            $ids[i] = $str_id + ''-'' + $lista.val();   ',
'        }',
'        flControl = 0;',
'    }',
'}',
'',
'if(flControl != 0){',
'    if($lista.val() != ''''){',
'        $ids.push($str_id + "-" + $lista.val());',
'    }    ',
'}',
'',
'console.log(''"'' + $lista.val() +''"'');',
'',
'//Convert array back to comma delimited list',
'apexItemIDList.setValue($ids.join('':''));'))
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265003937687999841)
,p_event_id=>wwv_flow_imp.id(115265003679168999839)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_error          VARCHAR2(2000);',
'  lv_estado_activo  VARCHAR2(1) := ''0'';',
'  ln_datos_split    apex_t_varchar2;',
'  ln_datos_split_id apex_t_varchar2;',
'  ln_cpo_id         number;',
'  ln_cat_id         varchar2(50);',
'  ',
'  CURSOR datos IS',
'    select * from  table(ln_datos_split);',
'    ',
'BEGIN  ',
'',
'      ln_datos_split := apex_string.split(p_str => :P201_LISTA, p_sep => '':'');',
'      ',
'      FOR reg IN datos LOOP      ',
'          ln_datos_split_id := apex_string.split(p_str => reg.COLUMN_VALUE, p_sep => ''-'');',
'          ',
'          SELECT val',
'              into ln_cpo_id',
'          FROM (Select rownum AS rn, column_value AS val',
'                FROM TABLE(ln_datos_split_id))',
'          WHERE rn = 1;',
'          ',
'          SELECT val',
'              into ln_cat_id',
'          FROM (Select rownum AS rn, column_value AS val',
'                FROM TABLE(ln_datos_split_id))',
'          WHERE rn = 2;',
'          ',
'          UPDATE asdm_e.ven_categorias_pos',
'           SET cat_id = ln_cat_id',
'          WHERE cpo_id = ln_cpo_id;',
'         COMMIT;',
'',
'      END LOOP;',
'      ',
'      :P201_LISTA := '''';',
'  ',
'COMMIT;',
'  ',
'EXCEPTION',
'WHEN OTHERS THEN',
'      :P201_LISTA := '''';',
'    raise_application_error(-20000,''Error al actualizar Categoria: '' || SQLERRM);',
'    ROLLBACK;',
'END;',
''))
,p_attribute_02=>'P201_LISTA'
,p_attribute_03=>'P201_LISTA'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004048938999842)
,p_event_id=>wwv_flow_imp.id(115265003679168999839)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'$(''#catego'').trigger(''apexrefresh'');'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(115265004540628999847)
,p_name=>'da_change_fecha_ins'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.fecha_ins'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004594689999848)
,p_event_id=>wwv_flow_imp.id(115265004540628999847)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P201_LISTA'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var z = $(".cpo_id");',
'',
'var',
'  //Checkbox that was changed',
'  $lista = $(this.triggeringElement),',
'  $str_id = $lista.attr(''id''),',
'  //DOM object for APEX Item that holds list.',
'  apexItemIDList = apex.item(this.affectedElements.get(0)),',
'  //Convert comma list into an array or blank array',
'  //Note: Not sure about the "?" syntax see: http://www.talkapex.com/2009/07/javascript-if-else.html',
'  $ids = apexItemIDList.getValue().length === 0 ? [] : apexItemIDList.getValue().split('':''),',
'  //Index of current ID. If it''s not in array, value will be -1',
'  idIndex = $ids.indexOf($lista.val())',
';',
'',
'var flControl = -1;',
'',
'for(var i=0;i<$ids.length;i++){',
'    var tex = $ids[i].split("-");',
'    if(tex[0] == $str_id){',
'        ',
'        if($lista.val() == ''''){',
'            $ids.splice(i, 1);',
'        }else{',
'            $ids[i] = $str_id + ''-'' + $lista.val();   ',
'        }',
'        flControl = 0;',
'    }',
'}',
'',
'if(flControl != 0){',
'    if($lista.val() != ''''){',
'        $ids.push($str_id + "-" + $lista.val());',
'    }    ',
'}',
'',
'console.log(''"'' + $lista.val() +''"'');',
'',
'//Convert array back to comma delimited list',
'apexItemIDList.setValue($ids.join('';''));'))
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004736761999849)
,p_event_id=>wwv_flow_imp.id(115265004540628999847)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_error          VARCHAR2(2000);',
'  lv_estado_activo  VARCHAR2(1) := ''0'';',
'  ln_datos_split    apex_t_varchar2;',
'  ln_datos_split_id apex_t_varchar2;',
'  ln_cpo_id         number;',
'  ln_fecha_ins         varchar2(100);',
'  ',
'  CURSOR datos IS',
'    select * from  table(ln_datos_split);',
'    ',
'BEGIN  ',
'',
'      ln_datos_split := apex_string.split(p_str => :P201_LISTA, p_sep => '';'');',
'      ',
'      FOR reg IN datos LOOP      ',
'          ln_datos_split_id := apex_string.split(p_str => reg.COLUMN_VALUE, p_sep => ''-'');',
'          ',
'          SELECT val',
'              into ln_cpo_id',
'          FROM (Select rownum AS rn, column_value AS val',
'                FROM TABLE(ln_datos_split_id))',
'          WHERE rn = 1;',
'          ',
'          SELECT val',
'              into ln_fecha_ins',
'          FROM (Select rownum AS rn, column_value AS val',
'                FROM TABLE(ln_datos_split_id))',
'          WHERE rn = 2;',
'          ',
'          --raise_application_error(-20000,''ln_cpo_id: ''||ln_cpo_id||'', sysdate: ''||to_date(sysdate,''dd/mm/yyyy hh24:mi:ss''));',
'          ',
'          UPDATE asdm_e.ven_categorias_pos',
'           SET cpo_fecha_ins = to_date(ln_fecha_ins,''dd/mm/yyyy hh24:mi'')',
'          WHERE cpo_id = ln_cpo_id;',
'         COMMIT;',
'',
'      END LOOP;',
'      ',
'      :P201_LISTA := '''';',
'  ',
'COMMIT;',
'  ',
'EXCEPTION',
'WHEN OTHERS THEN',
'      :P201_LISTA := '''';',
'    raise_application_error(-20000,''Error al actualizar Fecha Ins: '' || SQLERRM);',
'    ROLLBACK;',
'END;',
''))
,p_attribute_02=>'P201_LISTA'
,p_attribute_03=>'P201_LISTA'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004770016999850)
,p_event_id=>wwv_flow_imp.id(115265004540628999847)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'$(''#catego'').trigger(''apexrefresh'');'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(115265004078284999843)
,p_name=>'da_change_fecha_upd'
,p_event_sequence=>30
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.fecha_upd'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004171433999844)
,p_event_id=>wwv_flow_imp.id(115265004078284999843)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P201_LISTA'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var z = $(".cpo_id");',
'',
'var',
'  //Checkbox that was changed',
'  $lista = $(this.triggeringElement),',
'  $str_id = $lista.attr(''id''),',
'  //DOM object for APEX Item that holds list.',
'  apexItemIDList = apex.item(this.affectedElements.get(0)),',
'  //Convert comma list into an array or blank array',
'  //Note: Not sure about the "?" syntax see: http://www.talkapex.com/2009/07/javascript-if-else.html',
'  $ids = apexItemIDList.getValue().length === 0 ? [] : apexItemIDList.getValue().split('':''),',
'  //Index of current ID. If it''s not in array, value will be -1',
'  idIndex = $ids.indexOf($lista.val())',
';',
'',
'var flControl = -1;',
'',
'for(var i=0;i<$ids.length;i++){',
'    var tex = $ids[i].split("-");',
'    if(tex[0] == $str_id){',
'        ',
'        if($lista.val() == ''''){',
'            $ids.splice(i, 1);',
'        }else{',
'            $ids[i] = $str_id + ''-'' + $lista.val();   ',
'        }',
'        flControl = 0;',
'    }',
'}',
'',
'if(flControl != 0){',
'    if($lista.val() != ''''){',
'        $ids.push($str_id + "-" + $lista.val());',
'    }    ',
'}',
'',
'console.log(''"'' + $lista.val() +''"'');',
'',
'//Convert array back to comma delimited list',
'apexItemIDList.setValue($ids.join('';''));'))
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004285657999845)
,p_event_id=>wwv_flow_imp.id(115265004078284999843)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_error          VARCHAR2(2000);',
'  lv_estado_activo  VARCHAR2(1) := ''0'';',
'  ln_datos_split    apex_t_varchar2;',
'  ln_datos_split_id apex_t_varchar2;',
'  ln_cpo_id         number;',
'  ln_fecha_upd         varchar2(100);',
'  ',
'  CURSOR datos IS',
'    select * from  table(ln_datos_split);',
'    ',
'BEGIN  ',
'',
'      ln_datos_split := apex_string.split(p_str => :P201_LISTA, p_sep => '';'');',
'      ',
'      FOR reg IN datos LOOP      ',
'          ln_datos_split_id := apex_string.split(p_str => reg.COLUMN_VALUE, p_sep => ''-'');',
'          ',
'          SELECT val',
'              into ln_cpo_id',
'          FROM (Select rownum AS rn, column_value AS val',
'                FROM TABLE(ln_datos_split_id))',
'          WHERE rn = 1;',
'          ',
'          SELECT val',
'              into ln_fecha_upd',
'          FROM (Select rownum AS rn, column_value AS val',
'                FROM TABLE(ln_datos_split_id))',
'          WHERE rn = 2;',
'          ',
'          --raise_application_error(-20000,''ln_cpo_id: ''||ln_cpo_id||'', sysdate: ''||to_date(sysdate,''dd/mm/yyyy hh24:mi:ss''));',
'          ',
'          UPDATE asdm_e.ven_categorias_pos',
'           SET cpo_fecha_upd = to_date(ln_fecha_upd,''dd/mm/yyyy hh24:mi'')',
'          WHERE cpo_id = ln_cpo_id;',
'         COMMIT;',
'',
'      END LOOP;',
'      ',
'      :P201_LISTA := '''';',
'  ',
'COMMIT;',
'  ',
'EXCEPTION',
'WHEN OTHERS THEN',
'      :P201_LISTA := '''';',
'    raise_application_error(-20000,''Error al actualizar Fecha Upd: '' || SQLERRM);',
'    ROLLBACK;',
'END;',
''))
,p_attribute_02=>'P201_LISTA'
,p_attribute_03=>'P201_LISTA'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(115265004385347999846)
,p_event_id=>wwv_flow_imp.id(115265004078284999843)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'$(''#catego'').trigger(''apexrefresh'');'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(115265004858720999851)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_nuedo_dato'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  counter NUMBER;',
'BEGIN',
'  BEGIN',
'    SELECT count(s.cpo_id)',
'      INTO counter',
'      FROM asdm_e.ven_categorias_pos s',
'     WHERE s.cat_id IS NULL',
'       OR s.cpo_fecha_ins IS NULL',
'       or s.cpo_fecha_upd IS NULL;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      counter := 0;',
'  END;',
'',
'  IF counter > 0 THEN',
'    raise_application_error(-20000,',
'                            ''No se puede agregar otra fila, datos pendientes de completar!'');',
'  ELSE',
'    INSERT INTO asdm_e.ven_categorias_pos',
'      (EMP_ID, USU_ID, CAT_ID, CPO_FECHA_INS, CPO_FECHA_UPD, CPO_ESTADO_REGISTRO)',
'    VALUES',
'      (:f_emp_id,:f_user_id,null,null,null,0);',
'    COMMIT;',
'  END IF;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    raise_application_error(-20000,',
'                            ''Error al agregar nueva fila: '' || SQLERRM);',
'    ROLLBACK;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(115265003414435999836)
,p_internal_uid=>115232751707451234925
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(115265004970567999852)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_eliminar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  DELETE FROM asdm_e.ven_categorias_pos WHERE cpo_id = :P201_ELIMINAR;',
'  COMMIT;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    raise_application_error(-20000,',
'                            ''Error al borrar registro: '' || SQLERRM);',
'    ROLLBACK;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>115232751819298234926
);
wwv_flow_imp.component_end;
end;
/
