prompt --application/pages/page_00126
begin
--   Manifest
--     PAGE: 00126
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>126
,p_name=>'Reimpresion Documentos SRI  Negociacion'
,p_step_title=>'Reimpresion Documentos SRI  Negociacion'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00FAmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
' ',
'',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(284769678975077585)
,p_plug_name=>unistr('REIMPRESI\00D3N NEGOCIACIONES')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(284773079485077624)
,p_name=>'Factura'
,p_parent_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.COM_ID,',
'       a.UGE_NUM_SRI Establecimiento,',
'       a.PUE_NUM_SRI Pto_Emision,',
'       a.COM_NUMERO Numero,',
'       a.COM_FECHA,',
'       a.CLI_ID,',
'       a.nombre_completo,',
'       a.age_id_agencia,',
'       a.age_nombre_comercial,',
'       a.com_tipo,',
'       (SELECT b.vco_valor_variable',
'          FROM ven_var_comprobantes b',
'         WHERE b.com_id = a.com_id',
'           AND b.emp_id = a.emp_id',
'           AND b.var_id =',
'               pq_constantes.fn_retorna_constante(a.emp_id,',
'                                                  ''cn_var_id_total'')) total_factura,',
'       CASE WHEN to_date(a.COM_FECHA, ''DD-MM-YYYY'') >=   to_date((SYSDATE-530), ''DD-MM-YYYY'') THEN',
'          ''IMPRIMIR''',
'         ELSE',
'             NULL',
'          ',
'       END IMPRIMIR',
'',
'  FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO = to_number(:P126_COM_NUMERO)',
'   AND a.ttr_id = to_number(:P126_ttr)',
'   AND a.pue_num_sri = :P126_PUE_NUM_SRI',
'   AND a.uge_num_sri = :P126_UGE_NUM_EST_SRI',
'   AND a.emp_id = :f_emp_id',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773262398077628)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773381166077630)
,p_query_column_id=>2
,p_column_alias=>'ESTABLECIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'EST.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773459276077630)
,p_query_column_id=>3
,p_column_alias=>'PTO_EMISION'
,p_column_display_sequence=>4
,p_column_heading=>'PTO.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773557795077630)
,p_query_column_id=>4
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>5
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773665260077630)
,p_query_column_id=>5
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>1
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
,p_column_css_style=>'font-size:16px; color: green; font-weight: bold'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773751893077631)
,p_query_column_id=>6
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>6
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773864660077631)
,p_query_column_id=>7
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>7
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284773976554077631)
,p_query_column_id=>8
,p_column_alias=>'AGE_ID_AGENCIA'
,p_column_display_sequence=>8
,p_column_heading=>'AGE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284774082482077631)
,p_query_column_id=>9
,p_column_alias=>'AGE_NOMBRE_COMERCIAL'
,p_column_display_sequence=>9
,p_column_heading=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284774166899077631)
,p_query_column_id=>10
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>10
,p_column_heading=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284774260676077631)
,p_query_column_id=>11
,p_column_alias=>'TOTAL_FACTURA'
,p_column_display_sequence=>11
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(284774353929077631)
,p_query_column_id=>12
,p_column_alias=>'IMPRIMIR'
,p_column_display_sequence=>12
,p_column_heading=>'Imprimir'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:126:&SESSION.:REIMPRIMIR:&DEBUG.::P126_AGE_ID,P126_COM_NUMERO_ANT,P126_COM_ID:#AGE_ID_AGENCIA#,#NUMERO#,#COM_ID#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(284777279158077638)
,p_plug_name=>'NUEVA SECUENCIA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT  count(*)',
' FROM v_ven_comprobantes a',
' WHERE a.COM_NUMERO=:P126_COM_NUMERO',
' AND a.AGE_ID_AGENCIA=:F_AGE_ID_AGENCIA',
'AND a.com_tipo=:P126_COM_TIPO)> 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(284777478055077641)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_button_name=>'REIMPRIMIR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Reimprimir'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(284769866832077593)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(284779777991077666)
,p_branch_action=>'f?p=&APP_ID.:126:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 27-SEP-2011 08:44 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284770053875077603)
,p_name=>'P126_FOL_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'Folio'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  a.uge_num_est_sri|| ''-'' ||a.pue_num_sri ,',
'       fol_nro_folio',
'FROM   v_asdm_folios a',
'WHERE  a.emp_id = :f_emp_id',
'       AND a.usuario_id = :f_user_id',
'       AND a.uge_id = :f_uge_id',
'       AND',
'       a.tgr_id =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_guia_remision'')',
'       and a.fol_fecha_caducidad>=sysdate',
'       and a.fol_estado_registro=pq_constantes.fn_retorna_constante(null,''cv_estado_reg_activo'')',
'       order by a.fol_nro_folio'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P126_COM_TIPO=''GR'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284770267731077609)
,p_name=>'P126_TTR'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_use_cache_before_default=>'NO'
,p_prompt=>'TTR'
,p_source=>'return pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may'')'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284770465519077611)
,p_name=>'P126_TGR'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_use_cache_before_default=>'NO'
,p_source=>'return  pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion'')'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284770660199077611)
,p_name=>'P126_COM_NUM'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>' -  '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p126_com_tipo IS NOT NULL AND (:P126_COM_TIPO!=''GR'' OR :P126_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284770880569077613)
,p_name=>'P126_COM_NUMERO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'DIGITE LA SECUENCIA QUE DESEA REIMPRIMIR'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit()";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P126_COM_NUM = :P126_NUEVA_SECUENCIA'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284771069762077614)
,p_name=>'P126_NUEVA_SECUENCIA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>' - '
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p126_com_tipo IS NOT NULL AND (:P126_COM_TIPO!=''GR'' OR :P126_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284771279832077616)
,p_name=>'P126_PUE_NUM_SRI'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p126_com_tipo IS NOT NULL AND (:P126_COM_TIPO!=''GR'' OR :P126_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284771455392077617)
,p_name=>'P126_UGE_NUM_EST_SRI'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'DIGITE LA NUEVA SECUENCIA A IMPRIMIR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p126_com_tipo IS NOT NULL AND (:P126_COM_TIPO!=''GR'' OR :P126_FOL_ID IS NOT NULL)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284771669868077617)
,p_name=>'P126_COM_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284771875007077617)
,p_name=>'P126_COM_NUMERO_ANT'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'com_numero'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284772080590077618)
,p_name=>'P126_TRX_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_use_cache_before_default=>'NO'
,p_prompt=>'trx_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284772273944077619)
,p_name=>'P126_COM_TIPO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'Tipo Documento'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Factura Negociacion;F'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--Seleccione--'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''LIMPIA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284772465896077621)
,p_name=>'P126_AGE_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'AGE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284772673631077621)
,p_name=>'P126_NRO_FOLIO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_prompt=>'nro folio'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284772866774077622)
,p_name=>'P126_PCA_ID'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(284769678975077585)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pca Id'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284774473343077631)
,p_name=>'P126_MSG'
,p_item_sequence=>71
,p_item_plug_id=>wwv_flow_imp.id(284773079485077624)
,p_prompt=>'IMPORTANTE:'
,p_source=>unistr('Se permite reimprimir facturas \00FAnicamente del d\00EDa, para facturas anteriores generar Nota de Cr\00E9dito')
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" style="color: red" '
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P126_COM_TIPO = ''F'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284777671729077641)
,p_name=>'P126_DATFOLIO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_prompt=>'DATFOLIO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284777863983077642)
,p_name=>'P126_PUE_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_prompt=>'PUE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284778054762077642)
,p_name=>'P126_TTR_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_prompt=>'TTR_ID'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284778257460077642)
,p_name=>'P126_PERIODO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_prompt=>'PERIODO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284778466959077644)
,p_name=>'P126_ERROR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_prompt=>'ERROR'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(284778676450077644)
,p_name=>'P126_TTR_DESCRIPCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(284777279158077638)
,p_prompt=>'TTR_DESCRIPCION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(284778974711077651)
,p_validation_name=>'P126_COM_NUM'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p126_com_num = :P126_nueva_secuencia then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>'P126_COM_NUM'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_associated_item=>wwv_flow_imp.id(284770660199077611)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(284779070051077657)
,p_process_sequence=>100
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'carga_datos_folio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'',
'pq_ven_Comprobantes.pr_datos_folio(pn_emp_id          => :F_EMP_ID,',
'               pn_usu_id          => :F_USER_ID,',
'               pn_uge_id          => :F_uge_id,',
'               pn_ttr_id          => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'               pn_tgr_id          => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'               pn_pue_id          => :P126_PUE_ID,',
'               pv_pue_num_sri     => :P126_pue_num_sri,',
'               pv_uge_num_est_sri => :P126_uge_num_est_sri,',
'               pn_fol_sec_actual  => :P126_nueva_secuencia,',
'               pn_nro_folio       => :p126_nro_folio,',
'               pv_error           => :P0_error,',
'               pv_negociacion => ''S'');',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p126_com_tipo IS NOT NULL AND (:P126_COM_TIPO!=''GR'' OR :P126_FOL_ID IS NOT NULL)'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>252525918781312731
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(284779274375077661)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REIMPRIMIR_FAC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'   ',
'if :P126_COM_NUMERO_ANT is not null AND :P126_COM_ID IS NOT NULL then',
'  pq_ven_comprobantes.pr_reimprimir_mayoreo(pn_emp_id         => :F_EMP_ID,',
'                                            pn_com_id         => :P126_COM_ID,',
'                                            pn_com_numero     => :P126_NUEVA_SECUENCIA,',
'                                            pn_com_numero_ant => :P126_COM_NUMERO_ANT,',
'                                            pn_age_id         => :P126_AGE_ID,',
'                                            pn_tse_id         => :f_seg_id,',
'                                            pn_uge_id         => :f_uge_id,',
'                                            pn_uge_id_gasto   => :f_uge_id_gasto,',
'                                            pn_usu_id         => :f_user_id,',
'                                            pn_pue_id         => :P126_PUE_ID,',
'                                            pn_trx_id         => :P126_TRX_ID,',
'                                            pv_pue_num_sri     => :P126_PUE_NUM_SRI, ',
'                                            pv_uge_num_est_sri => :P126_UGE_NUM_EST_SRI,',
'                                            pv_com_tipo       => :p126_com_tipo,',
'                                            pn_tgr_id         => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'                                            pn_ttr_id         => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'                                            pv_error          => :P0_ERROR);',
'end if;',
'',
'',
'',
'IF :P0_ERROR IS NULL THEN',
':P126_COM_NUMERO_ANT := null;',
':P126_NUEVA_SECENCIA := null;',
':p126_COM_NUMERO := NULL;',
':P126_COM_NUM := NULL;',
':P126_COM_TIPO := NULL;',
':P126_COM_ID := NULL;',
':P126_NRO_FOLIO := NULL;',
':P126_AGE_ID := NULL;',
':P126_TTR := NULL;',
':P126_TGR := NULL;',
':P126_TRX_ID := NULL;',
'END IF;',
'',
' EXCEPTION',
'    WHEN OTHERS THEN',
'      raise_application_error(-20000,',
'                              ''Apex PR_REIMPRIMIR_FAC ''||:P0_ERROR ||SQLERRM||dbms_utility.format_error_backtrace);',
'      ROLLBACK;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(284777478055077641)
,p_process_when=>'REIMPRIMIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>252526123105312735
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(284779477266077663)
,p_process_sequence=>110
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P126_COM_NUMERO_ANT := null;',
':P126_NUEVA_SECENCIA := null;',
':p126_COM_NUMERO := NULL;',
':P126_COM_NUM := NULL;',
':P126_NRO_FOLIO := NULL;',
':P126_AGE_ID := NULL;',
':P126_TTR := NULL;',
':P126_TGR := NULL;',
':P126_TRX_ID := NULL;',
':P126_COM_ID := NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'LIMPIA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>252526325996312737
);
wwv_flow_imp.component_end;
end;
/
