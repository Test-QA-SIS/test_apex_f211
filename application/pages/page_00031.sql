prompt --application/pages/page_00031
begin
--   Manifest
--     PAGE: 00031
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>31
,p_name=>'Report Cheques Anulados'
,p_step_title=>'Report Cheques Anulados'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(35909156901861387)
,p_plug_name=>'Realizar Consulta'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>5
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(267944979117623100)
,p_plug_name=>'Listado de Cheques'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select to_number(c001) cre_id,',
'       c002 estado_cheque,',
'       to_number(c003) cli_id,',
'       c004 cliente,',
'       to_number(c005) ede_id,',
'       c006 ede_descripcion,',
'       to_number(c007) cre_id_reemplazado,',
'       to_number(c008) rca_id,',
'       c009 cre_nro_cheque,',
'       to_number(c010) cre_valor,',
'       c011 cre_tipo,',
'       c012 tipocheque,',
'       c013 cre_nro_cuenta,',
'       c014 cre_nro_pin,',
'       c015 cre_titular_cuenta,',
'       c016 cre_observaciones,',
'       to_date(c017, ''dd/mm/yyyy'') rca_fecha_ingreso,',
'       to_date(c018, ''dd/mm/yyyy'') cre_fecha_deposito,',
'       c019 cre_estado_registro,',
'       to_date(c020, ''dd/mm/yyyy'') fecha_movimiento,',
'       c021 usuario,',
'       c022 uge_nombre',
'',
'  from apex_collections',
' where collection_name = ''COL_BIND'''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(267945070844623100)
,p_name=>'Reporte Cheques Anulados'
,p_max_row_count=>'100000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No existen datos'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>235691919574858174
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945251858623122)
,p_db_column_name=>'CRE_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Id.Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945362963623130)
,p_db_column_name=>'CLI_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Id.Cliente '
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945454014623130)
,p_db_column_name=>'CLIENTE'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945577080623130)
,p_db_column_name=>'EDE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Id.Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945665236623131)
,p_db_column_name=>'EDE_DESCRIPCION'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'EDE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945767062623131)
,p_db_column_name=>'CRE_ID_REEMPLAZADO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Reemplaza cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID_REEMPLAZADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945879317623131)
,p_db_column_name=>'RCA_ID'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Rca Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'RCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267945970114623131)
,p_db_column_name=>'CRE_NRO_CHEQUE'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Nro.<br>Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946059367623131)
,p_db_column_name=>'CRE_VALOR'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946157266623131)
,p_db_column_name=>'CRE_TIPO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Tipo Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_static_id=>'CRE_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946254345623132)
,p_db_column_name=>'TIPOCHEQUE'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
,p_static_id=>'TIPOCHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946379364623132)
,p_db_column_name=>'CRE_NRO_CUENTA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Nro.<br>Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946467550623132)
,p_db_column_name=>'CRE_NRO_PIN'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Nro. Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946568739623132)
,p_db_column_name=>'CRE_TITULAR_CUENTA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Titular_Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_TITULAR_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946679026623132)
,p_db_column_name=>'CRE_OBSERVACIONES'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(267946774530623132)
,p_db_column_name=>'CRE_FECHA_DEPOSITO'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>unistr('Fecha_Dep\00BF\00BFsito')
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
,p_static_id=>'CRE_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77630660553850245)
,p_db_column_name=>'CRE_ESTADO_REGISTRO'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Cre Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77680472659274374)
,p_db_column_name=>'USUARIO'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'USUARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77680566821274375)
,p_db_column_name=>'UGE_NOMBRE'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>unistr('Unidad<br>de Gesti\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(88989470491787724)
,p_db_column_name=>'RCA_FECHA_INGRESO'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Rca Fecha Ingreso'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'RCA_FECHA_INGRESO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(88989554891787869)
,p_db_column_name=>'FECHA_MOVIMIENTO'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Fecha Movimiento'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(88994553831830200)
,p_db_column_name=>'ESTADO_CHEQUE'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Estado Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ESTADO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(88995366298833891)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheques Anulados'
,p_report_seq=>10
,p_report_alias=>'567423'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:RCA_FECHA_INGRESO:CRE_FECHA_DEPOSITO:FECHA_MOVIMIENTO:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(88995553775833925)
,p_report_id=>wwv_flow_imp.id(88995366298833891)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Anulado'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Anulado''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(88996477379836998)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheques Aplica Cuota'
,p_report_seq=>10
,p_report_alias=>'567434'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:RCA_FECHA_INGRESO:CRE_FECHA_DEPOSITO:FECHA_MOVIMIENTO:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(88996679642836998)
,p_report_id=>wwv_flow_imp.id(88996477379836998)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Aplica Cuota'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Aplica Cuota''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(88997753269839539)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheques Deposito Real'
,p_report_seq=>10
,p_report_alias=>'567447'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:RCA_FECHA_INGRESO:CRE_FECHA_DEPOSITO:FECHA_MOVIMIENTO:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(88997974250839539)
,p_report_id=>wwv_flow_imp.id(88997753269839539)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Deposito Real'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Deposito Real''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(88999569199844119)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheque Ingresado a la Fecha'
,p_report_seq=>10
,p_report_alias=>'567465'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:CRE_FECHA_DEPOSITO:FECHA_MOVIMIENTO:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(89008373486854802)
,p_report_id=>wwv_flow_imp.id(88999569199844119)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Ingresado'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Ingresado''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(89008474022854802)
,p_report_id=>wwv_flow_imp.id(88999569199844119)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'TIPOCHEQUE'
,p_operator=>'='
,p_expr=>'A la Fecha'
,p_condition_sql=>'"TIPOCHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''A la Fecha''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(89002780281847360)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheques Ingresado Posfechado'
,p_report_seq=>10
,p_report_alias=>'567497'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:RCA_FECHA_INGRESO:CRE_FECHA_DEPOSITO:FECHA_MOVIMIENTO:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(89002957429847361)
,p_report_id=>wwv_flow_imp.id(89002780281847360)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Ingresado'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Ingresado''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(89003073442847361)
,p_report_id=>wwv_flow_imp.id(89002780281847360)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'TIPOCHEQUE'
,p_operator=>'='
,p_expr=>'Posfechado'
,p_condition_sql=>'"TIPOCHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Posfechado''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(89626752133632929)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheques Devueltos/Protestados'
,p_report_seq=>10
,p_report_alias=>'573737'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:CRE_FECHA_DEPOSITO:FECHA_EJECUCION:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(89626951333632935)
,p_report_id=>wwv_flow_imp.id(89626752133632929)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Devuelto/Protestado'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Devuelto/Protestado''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(89888357391881114)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cheques Deposito Transitorio'
,p_report_seq=>10
,p_report_alias=>'576353'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:CRE_FECHA_DEPOSITO:FECHA_EJECUCION:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(89888558295881119)
,p_report_id=>wwv_flow_imp.id(89888357391881114)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Deposito Transitorio'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Deposito Transitorio''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(267958460274719208)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292300032919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>25
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:FECHA_MOVIMIENTO:CRE_FECHA_DEPOSITO:RCA_FECHA_INGRESO:ESTADO_CHEQUE:USUARIO:UGE_NOMBRE:'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(35909373177866138)
,p_button_sequence=>100
,p_button_plug_id=>wwv_flow_imp.id(35909156901861387)
,p_button_name=>'CONSULTAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Consultar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36422968245974858)
,p_name=>'P31_DESDE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(35909156901861387)
,p_prompt=>'Desde'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36423163285977084)
,p_name=>'P31_HASTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(35909156901861387)
,p_prompt=>'Hasta'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(77529682429860344)
,p_name=>'P31_ESTADOS_CHEQUES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(267944979117623100)
,p_prompt=>'Estados Cheques'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(12935601979632395181)
,p_name=>'P31_UGE_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(35909156901861387)
,p_prompt=>'Uge Id'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_UNIDADES_GESTION_CHE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_UNIDADES_GESTION_CHE'');',
'return (lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Todos'
,p_cSize=>70
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(12937139181021459102)
,p_name=>'ad_refresh'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P31_UGE_ID'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(12937140081624459122)
,p_event_id=>wwv_flow_imp.id(12937139181021459102)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P31_UGE_ID'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(35909561880872304)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  lv_query   VARCHAR2(3000);',
'  LN_ORDEN   NUMBER := 222;',
'  lb_exists  BOOLEAN;',
'  lv_nombres apex_application_global.vc_arr2;',
'  lv_valores apex_application_global.vc_arr2;',
'',
'  F_EMP_ID NUMBER := :f_emp_id;',
'',
'  ',
'BEGIN',
'/*',
'  pq_car_consultas.pr_create_apex_session(p_app_id      => 147,',
'                                          p_app_user    => ''jordonez'',',
'                                          p_app_page_id => 44);*/',
'',
'  lv_nombres(1) := ''F_EMP_ID'';',
'  lv_nombres(2) := ''P31_DESDE'';',
'  lv_nombres(3) := ''P31_HASTA'';    ',
'  lv_nombres(4) := ''P31_UGE_ID'';   ',
'  lv_valores(1) := F_EMP_ID;',
'  lv_valores(2) :=  v(''P31_DESDE'');',
'  lv_valores(3) :=  v(''P31_HASTA'');',
'  lv_valores(4) :=  v(''P31_UGE_ID'');  ',
'',
'  lv_query := ''',
'  ',
'  SELECT cre.cre_id,',
'       ech.ech_descripcion estado_cheque,',
'       cre.cli_id,',
'       (PE.PER_PRIMER_APELLIDO || '''' '''' || PE.PER_SEGUNDO_APELLIDO || '''' '''' ||',
'       PE.PER_PRIMER_NOMBRE || '''' '''' || PE.PER_SEGUNDO_NOMBRE || '''' '''' ||',
'       PE.PER_RAZON_SOCIAL) cliente,',
'       cre.ede_id,',
'       ede.ede_descripcion,',
'       cre.cre_id_reemplazado,',
'       (SELECT RCA.RCA_ID',
'          FROM car_respaldos_cartera RCA',
'         WHERE RCA.RCA_ID = CRE.RCA_ID) rca_id,',
'       cre.cre_nro_cheque,',
'       cre.cre_valor,',
'       cre.cre_tipo,',
'       pq_lv_kdda.fn_obtiene_valor_lov(183, cre.cre_tipo) tipocheque,',
'       cre.cre_nro_cuenta,',
'       cre.cre_nro_pin,',
'       cre.cre_titular_cuenta,',
'       cre.cre_observaciones,',
'       (SELECT RCA.rca_fecha_ingreso',
'          FROM car_respaldos_cartera RCA',
'         WHERE RCA.RCA_ID = CRE.RCA_ID) rca_fecha_ingreso,',
'       cre.cre_fecha_deposito,',
'       cre.cre_estado_registro,',
'       cre.fecha_movimiento,',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(cre.usu_id) usuario,',
'    ',
'          (select u.uge_nombre',
'           from asdm_unidades_gestion u',
'           where u.uge_id = cre.uge_id) uge_nombre',
'',
'  FROM asdm_empresas           emp, ',
'       asdm_cheques_recibidos  cre, ',
'       asdm_entidades_destinos ede, ',
'       ASDM_CLIENTES           cli, ',
'       ASDM_PERSONAS           PE, ',
'       asdm_estados_cheques ech',
'',
' WHERE emp.emp_id = cre.emp_id',
'   AND emp.emp_id = :F_EMP_ID',
'   AND ede.ede_id = cre.ede_id',
'   AND cre.cre_estado_registro = 0',
'   and ech.ech_id = cre.ech_id',
'   AND cli.cli_id = cre.cli_id',
'   AND CLI.PER_ID = PE.PER_ID',
'   AND PE.EMP_ID = :F_EMP_ID',
'   AND PE.PER_ESTADO_REGISTRO = 0',
'   AND PE.PER_TIPO_PERSONA IN (''''N'''', ''''J'''')',
'   AND ECH.ECH_ESTADO_REGISTRO = 0',
'   and cre.fecha_movimiento between :P31_DESDE and :P31_HASTA',
'   and cre.uge_id = NVL(:P31_UGE_ID, cre.uge_id)'';',
'',
'  lb_exists := apex_collection.collection_exists(p_collection_name => ''COL_BIND'');',
'  IF lb_exists THEN',
'    apex_collection.delete_collection(p_collection_name => ''COL_BIND'');',
'  END IF;',
'  apex_collection.create_collection_from_query_b(p_collection_name => ''COL_BIND'',',
'                                                 p_query           => lv_query,',
'                                                 p_names           => lv_nombres,',
'                                                 p_values          => lv_valores);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(35909373177866138)
,p_internal_uid=>3656410611107378
);
wwv_flow_imp.component_end;
end;
/
