prompt --application/pages/page_00081
begin
--   Manifest
--     PAGE: 00081
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>81
,p_name=>unistr('Deposito Transitorio Tarjetas de Cr\00BF\00BFdito')
,p_step_title=>unistr('Deposito Transitorio Tarjetas de Cr\00BF\00BFdito')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240129094718'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(86825469053403494)
,p_plug_name=>'Detalle Movimientos Tarjetas'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select TRIM(substr(entidad2,',
'                   4,',
'                   INSTR(SUBSTR(entidad2, 4, LENGTH(ENTIDAD2)), '' ->'', 1, 1))) BANCO,',
'       substr(entidad2,',
'              1,',
'              INSTR(SUBSTR(entidad2, 1, LENGTH(ENTIDAD2)), '' ->'', 4, 1)) BANCO2,',
'       ',
'       substr(entidad2,',
'              LENGTH(substr(entidad2,',
'                            1,',
'                            INSTR(SUBSTR(entidad2, 1, LENGTH(ENTIDAD2)),',
'                                  '' ->'',',
'                                  4,',
'                                  1))) + 4,',
'              LENGTH(ENTIDAD2)) CADENA2,',
'       ',
'       substr(substr(entidad2,',
'                     LENGTH(substr(entidad2,',
'                                   1,',
'                                   INSTR(SUBSTR(entidad2, 1, LENGTH(ENTIDAD2)),',
'                                         '' ->'',',
'                                         4,',
'                                         1))) + 4,',
'                     LENGTH(ENTIDAD2)),',
'              1,',
'              INSTR(SUBSTR(substr(entidad2,',
'                                  LENGTH(substr(entidad2,',
'                                                1,',
'                                                INSTR(SUBSTR(entidad2,',
'                                                             1,',
'                                                             LENGTH(ENTIDAD2)),',
'                                                      '' ->'',',
'                                                      4,',
'                                                      1))) + 4,',
'                                  LENGTH(ENTIDAD2)),',
'                           1,',
'                           LENGTH(substr(entidad2,',
'                                         LENGTH(substr(entidad2,',
'                                                       1,',
'                                                       INSTR(SUBSTR(entidad2,',
'                                                                    1,',
'                                                                    LENGTH(ENTIDAD2)),',
'                                                             '' ->'',',
'                                                             4,',
'                                                             1))) + 4,',
'                                         LENGTH(ENTIDAD2)))),',
'                    '' ->'',',
'                    4,',
'                    1)) TARJETA,',
'       TFP_ID,',
'       FORMA_PAGO,',
'       EDE_ID,',
'       EDE_DESCRIPCION,',
'       EDE_ID_PADRE,',
'       ENTIDAD,',
'       ENTIDAD2,',
'       MCD_NRO_TARJETA,',
'       MCD_VALOR_MOVIMIENTO,',
'       MCD_NRO_AUT_REF,',
'       MCD_NRO_LOTE,',
'       MCD_ID_REF,',
'       MCD_TITULAR_TARJETA,',
'       MCD_VOUCHER,',
'       MCD_NRO_RECAP',
'  from (SELECT a.tfp_id,',
'               (SELECT tfp_descripcion',
'                  FROM asdm_tipos_formas_pago',
'                 WHERE tfp_id = a.tfp_id) forma_pago,',
'               a.ede_id,',
'               ede.ede_descripcion,',
'               ede.ede_id_padre,',
'               (SELECT ede_descripcion',
'                  FROM asdm_entidades_destinos',
'                 WHERE ede_id = a.ede_id) entidad,',
'               (SELECT d',
'                  FROM (SELECT sys_connect_by_path(ede.ede_descripcion, '' -> '') AS d,',
'                               ede.ede_id r',
'                          FROM asdm_entidades_destinos ede',
'                         WHERE ede.emp_id = :f_emp_id',
'                           AND ede.ede_estado_registro =',
'                               pq_constantes.fn_retorna_constante(NULL,',
'                                                                  ''cv_estado_reg_activo'')',
'                         START WITH ede.ede_id_padre IS NULL',
'                        CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre) CUE',
'                 WHERE CUE.R IN',
'                       (SELECT E.EDE_ID',
'                          FROM ASDM_ENTIDADES_DESTINOS E',
'                         WHERE E.EDE_ESTADO_REGISTRO =',
'                               pq_constantes.fn_retorna_constante(NULL,',
'                                                                  ''cv_estado_reg_activo''))',
'                   and cue.r = a.ede_id) entidad2,',
'               a.mcd_nro_tarjeta,',
'               a.mcd_valor_movimiento,',
'               a.mcd_nro_aut_ref,',
'               a.mcd_nro_lote,',
'               a.mcd_id_ref,',
'               a.mcd_titular_tarjeta,',
'               A.MCD_VOUCHER,',
'               A.MCD_NRO_RECAP',
'          FROM asdm_movimientos_caja_detalle a,',
'               asdm_movimientos_caja         b,',
'               asdm_tipos_formas_pago        c,',
'               ven_periodos_caja             d,',
'               asdm_transacciones            e,',
'               asdm_entidades_destinos       ede',
'         WHERE a.mca_id = b.mca_id',
'           and a.ede_id = ede.ede_id',
'           AND b.emp_id = a.emp_id',
'           AND c.emp_id = a.emp_id',
'           AND d.emp_id = a.emp_id',
'           AND e.emp_id = a.emp_id',
'           AND a.mcd_estado_registro =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_estado_reg_activo'')',
'           AND b.pca_id = :f_pca_id',
'           AND a.tfp_id = c.tfp_id',
'           AND c.tfp_va_al_deposito = ''S''',
'           AND d.pca_id = b.pca_id',
'           AND d.pca_estado_pc =',
'               pq_constantes.fn_retorna_constante(null,',
'                                                  ''cv_pca_estado_pc_abierto'')',
'           AND d.pca_token =',
'               pq_constantes.fn_retorna_constante(null,',
'                                                  ''cv_pca_estado_pc_abierto'')',
'           AND a.tfp_id in',
'               (pq_constantes.fn_retorna_constante(b.emp_id,',
'                                                   ''cn_tfp_id_tarjeta_credito''))',
'           AND e.trx_id = b.trx_id',
'           and e.ttr_id !=',
'               pq_constantes.fn_retorna_constante(null,',
'                                                  ''cn_ttr_id_egr_caj_dep_tran'')',
'           AND a.emp_id = :f_emp_id',
'           AND nvl(A.MCD_DEP_TRANSITORIO, ''N'') !=',
'               pq_constantes.fn_retorna_constante(null, ''cv_si''));'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(86825579519403494)
,p_name=>'Detalle Movimientos Tarjetas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_owner=>'ADMIN'
,p_internal_uid=>54572428249638568
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86825769639403689)
,p_db_column_name=>'BANCO'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Banco'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'BANCO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86825878462403821)
,p_db_column_name=>'BANCO2'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Banco2'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'BANCO2'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86825966460403821)
,p_db_column_name=>'CADENA2'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cadena2'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CADENA2'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826055963403821)
,p_db_column_name=>'TARJETA'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Tarjeta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826181912403821)
,p_db_column_name=>'TFP_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Tfp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TFP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826274481403822)
,p_db_column_name=>'FORMA_PAGO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Forma Pago'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FORMA_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826379510403822)
,p_db_column_name=>'EDE_ID'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826565822403822)
,p_db_column_name=>'EDE_ID_PADRE'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Ede Id Padre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID_PADRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826666589403822)
,p_db_column_name=>'ENTIDAD'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ENTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826774134403823)
,p_db_column_name=>'ENTIDAD2'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Entidad2'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ENTIDAD2'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826865397403823)
,p_db_column_name=>'MCD_NRO_TARJETA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Nro Tarjeta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_NRO_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86826975731403823)
,p_db_column_name=>'MCD_VALOR_MOVIMIENTO'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Valor Movimiento'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_VALOR_MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86827078595403823)
,p_db_column_name=>'MCD_NRO_AUT_REF'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Nro Aut Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_NRO_AUT_REF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86827181113403823)
,p_db_column_name=>'MCD_NRO_LOTE'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Nro Lote'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_NRO_LOTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86827259123403823)
,p_db_column_name=>'MCD_ID_REF'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Mcd Id Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_ID_REF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86827379861403824)
,p_db_column_name=>'MCD_TITULAR_TARJETA'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Titular Tarjeta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_TITULAR_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86827471970403824)
,p_db_column_name=>'MCD_VOUCHER'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Voucher'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_VOUCHER'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(86827576456403824)
,p_db_column_name=>'MCD_NRO_RECAP'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Nro Recap'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_NRO_RECAP'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(857575766055138405)
,p_db_column_name=>'EDE_DESCRIPCION'
,p_display_order=>29
,p_column_identifier=>'T'
,p_column_label=>'Ede Descripcion'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(86827767328404261)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'545747'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'BANCO:TARJETA:FORMA_PAGO:MCD_NRO_TARJETA:MCD_VALOR_MOVIMIENTO:MCD_NRO_AUT_REF:MCD_NRO_LOTE:MCD_VOUCHER:MCD_NRO_RECAP:MCD_TITULAR_TARJETA'
,p_break_on=>'BANCO:TARJETA:0:0:0:0'
,p_break_enabled_on=>'BANCO:TARJETA:0:0:0:0'
,p_sum_columns_on_break=>'MCD_VALOR_MOVIMIENTO'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(86829668186458588)
,p_plug_name=>unistr('Detalle Dep\00BF\00BFsito')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>90
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(123702277465216982)
,p_name=>'col_deposito_transitorio'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name like ''COL_DEP_VOUCHER'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123702766718217042)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123702871025217042)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123702957539217042)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703083187217042)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703165657217042)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703252844217042)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703382509217042)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703478381217042)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703560173217042)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703660779217042)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703778851217042)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703868337217042)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123703980142217042)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704054337217042)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704172316217042)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704252950217042)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704361373217043)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704456189217043)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704559189217043)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704658880217043)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704762693217043)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704876186217043)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123704968773217043)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705078571217043)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705158902217043)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705268200217043)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705365442217043)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123702478971217025)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123702570870217041)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123702660588217042)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708562068217046)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705457975217043)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705554048217043)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705653946217043)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705782424217043)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705875317217043)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123705963443217043)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706072785217044)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706178407217044)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706283185217044)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706353494217044)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706458889217044)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706572989217044)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706677184217044)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706775910217044)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706858434217044)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123706952053217044)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707063435217044)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707162769217045)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707269009217046)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707368015217046)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707465564217046)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707576050217046)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707677736217046)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707758801217046)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707867752217046)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123707952141217046)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708071811217046)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708172637217046)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708268958217046)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708364302217046)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708473685217046)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708965354217046)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708679343217046)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708778046217046)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123708872882217046)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(123709070174217057)
,p_name=>'PAGOS TARJETA DE CREDITO'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001       mcd_id,',
'       c002       tfp_id,',
'       c003       forma_pago,',
'       c004       ede_id,',
'       c005       entidad,',
'       C007       valor,',
'       c006       tarjeta,',
'       c008       autorizacion,',
'       c009       Lote,',
'       c011       titular_tarjeta',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_DEP_VOUCHER'''))
,p_display_when_condition=>'p81_tipo'
,p_display_when_cond2=>'2'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(123709070174217057)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709370526217062)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709469738217063)
,p_query_column_id=>2
,p_column_alias=>'MCD_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Mcd Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709568682217063)
,p_query_column_id=>3
,p_column_alias=>'TFP_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Tfp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709660745217065)
,p_query_column_id=>4
,p_column_alias=>'FORMA_PAGO'
,p_column_display_sequence=>4
,p_column_heading=>'Forma Pago'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709775797217065)
,p_query_column_id=>5
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Ede Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709258598217062)
,p_query_column_id=>6
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>6
,p_column_heading=>'Entidad'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709879820217065)
,p_query_column_id=>7
,p_column_alias=>'VALOR'
,p_column_display_sequence=>8
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123710278740217066)
,p_query_column_id=>8
,p_column_alias=>'TARJETA'
,p_column_display_sequence=>7
,p_column_heading=>'Tarjeta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123710078875217065)
,p_query_column_id=>9
,p_column_alias=>'AUTORIZACION'
,p_column_display_sequence=>9
,p_column_heading=>'Autorizacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123710173787217066)
,p_query_column_id=>10
,p_column_alias=>'LOTE'
,p_column_display_sequence=>10
,p_column_heading=>'Lote'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123709981465217065)
,p_query_column_id=>11
,p_column_alias=>'TITULAR_TARJETA'
,p_column_display_sequence=>11
,p_column_heading=>'Titular Tarjeta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(123711775422217091)
,p_plug_name=>'DEPOSITO TRANSITORIO DE CREDITO'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(123711979568217101)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'graba'
,p_button_condition_type=>'REQUEST_NOT_EQUAL_CONDITION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(123712173871217109)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_button_name=>'CANCELAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(123717060115217207)
,p_branch_action=>'f?p=&APP_ID.:81:&SESSION.:graba:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(123711979568217101)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(123717275207217218)
,p_branch_action=>'f?p=&FLOW_ID.:39:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(123712173871217109)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(123717465705217228)
,p_branch_action=>'f?p=&APP_ID.:81:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 13-NOV-2009 01:40 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123712383444217112)
,p_name=>'P81_TARJETA'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Voucher'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123712555755217135)
,p_name=>'P81_TRX_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123712755995217135)
,p_name=>'P81_TTR_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Ttr Id'
,p_source=>'pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_egr_caj_dep_tran'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123712971428217139)
,p_name=>'P81_MCA_ID'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Mca Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123713173216217139)
,p_name=>'P81_MCA_OBSERVACION'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Observacion'
,p_source=>'select ''DEPOSITO TRANSITORIO TC ''||to_char(sysdate,''dd/mm/yyyy hh24:mm:ss'') FROM DUAL'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>2
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123713352303217141)
,p_name=>'P81_EFECTIVO'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Efectivo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p81_tipo = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123713574702217143)
,p_name=>'P81_CHEQUES'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Cheques'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p81_tipo = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123713781892217143)
,p_name=>'P81_MCA_TOTAL'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total'
,p_source=>'P81_tarjeta'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123713958646217144)
,p_name=>'P81_MCA_OBSERVACION1'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Observacion'
,p_source=>'select mca_observacion from asdm_movimientos_caja where mca_id=:p81_mca_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123714164616217144)
,p_name=>'P81_EFECTIVO1'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Efectivo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca_total - nvl(total.suma,0)',
'FROM   asdm_movimientos_caja,',
'       (SELECT SUM(c009) suma',
'        FROM   apex_collections',
'        WHERE  collection_name = ''COL_DEP_TRANSITORIOS''',
'               AND c012 = ''SI'') total',
'WHERE  mca_id =:p81_mca_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123714379336217145)
,p_name=>'P81_OTROS1'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Otros'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(c009),0)',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_DEP_TRANSITORIOS''',
'AND C012=''SI'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123714551601217145)
,p_name=>'P81_MCA_TOTAL1'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total'
,p_source=>'select to_char(mca_total,99999999999.99) from asdm_movimientos_caja where mca_id=:p81_mca_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123714758096217146)
,p_name=>'P81_ENTIDAD_DESTINO'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>unistr('Banco Dep\00BF\00BFsito')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123714970468217147)
,p_name=>'P81_EDE_ID'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>unistr('Cuenta Dep\00BF\00BFsito')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'p81_entidad_destino'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123715154062217147)
,p_name=>'P81_TIPO'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Tipo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123715369241217147)
,p_name=>'P81_SEQ_ID'
,p_item_sequence=>100
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123715560187217147)
,p_name=>'P81_ESTADO'
,p_item_sequence=>110
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Estado'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(123943359763643887)
,p_name=>'P81_BANCO_TARJETA'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(123711775422217091)
,p_prompt=>'Banco Tarjeta'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(123715855432217176)
,p_validation_name=>'P81_ENTIDAD_DESTINO'
,p_validation_sequence=>10
,p_validation=>'P81_ENTIDAD_DESTINO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Value must be specified.'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(123711979568217101)
,p_associated_item=>wwv_flow_imp.id(123714758096217146)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(123716068793217185)
,p_validation_name=>'P81_MCA_OBSERVACION'
,p_validation_sequence=>20
,p_validation=>'P81_MCA_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe ingresar la Observacion'
,p_when_button_pressed=>wwv_flow_imp.id(123711979568217101)
,p_associated_item=>wwv_flow_imp.id(123713173216217139)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(123716169810217185)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_deposito_transitorio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':p81_seq_id:=null;',
':p81_estado:=null;',
'',
'',
'if :p81_mca_total > 0 then',
'  pq_ven_movimientos_caja.pr_dep_transitorio_tarjetas(pn_emp_id          =>:f_emp_id,',
'                                                  pn_uge_id          =>:f_uge_id,',
'                                                  pn_user_id         =>:f_user_id,',
'                                                  pn_ttr_id          =>:p81_ttr_id,',
'                                                  pn_trx_id          =>:p81_trx_id,',
'                                                  pn_uge_id_gasto    =>:f_uge_id_gasto,',
'                                                  pn_mca_id          =>:p81_mca_id,',
'                                                  pn_pca_id          =>:f_pca_id,',
'                                                  pv_mca_observacion =>:p81_mca_observacion,',
'                                                  pn_mca_total       =>:p81_mca_total,',
'                                                  pn_EDE_id          =>:P81_EDE_ID,',
'                                                  pn_tipo            =>2,',
'                                                  pn_ede_id_financiera => :p81_banco_tarjeta,',
'                                                  pv_error           =>:p0_error);',
'',
'',
'',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(123711979568217101)
,p_internal_uid=>91463018540452259
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(123716554529217193)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_valor number;',
'ln_cheque number;',
'ln_tarjeta number;',
'begin',
'/*',
'pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id => :f_emp_id,',
'                        pn_pca_id => :f_pca_id,',
'                        pn_valor_efectivo => :p81_efectivo,',
'                        pn_valor_cheque => :p81_cheques,',
'                        pn_valor_tarjeta => :p81_tarjeta,',
'                        pv_error  => :p0_error);*/',
'',
'',
'pq_ven_movimientos_caja.pr_carga_coll_depo_tarjetas(pn_emp_id => :f_emp_id,',
'                                        pn_pca_id => :f_pca_id,',
'                                      --  pn_ede_id_financiera => :p81_banco_tarjeta,',
'                                        pn_ede_id_financiera => null,',
'                                        pn_valor_tarjeta => :p81_tarjeta,',
'                                        pv_error  => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request not in (''cambia'',''graba'') or :p81_seq_id is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>91463403259452267
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(123716780767217193)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_modifica_reg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'if :p81_estado=''SI'' then',
' apex_collection.update_member_attribute(p_collection_name =>''COL_DEP_TRANSITORIOS'',',
'                                          p_seq             =>:P81_SEQ_ID,',
'                                          p_attr_number     =>12,',
'                                          p_attr_value      =>''NO'');',
'else',
'apex_collection.update_member_attribute(p_collection_name =>''COL_DEP_TRANSITORIOS'',',
'                                          p_seq             =>:P81_SEQ_ID,',
'                                          p_attr_number     =>12,',
'                                          p_attr_value      =>''SI'');',
'end if;',
':p81_seq_id:=null;',
':p81_estado:=null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cambia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>91463629497452267
);
wwv_flow_imp.component_end;
end;
/
