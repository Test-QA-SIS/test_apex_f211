prompt --application/pages/page_00074
begin
--   Manifest
--     PAGE: 00074
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>74
,p_name=>'Report 1'
,p_step_title=>'Report 1'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111202979603424714)
,p_plug_name=>'Detalle Provisiones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'select a.CLI_ID,',
'       (select b.NOMBRE_COMPLETO',
'          from v_asdm_datos_clientes b',
'         where b.cli_id = a.cli_id',
'           and b.emp_id = c.emp_id) nombre,',
'       a.CXC_ID,',
'       a.DIV_ID,',
'       c.div_nro_vencimiento,',
'       c.div_fecha_vencimiento,',
'       a.PNC_VALOR',
'  from car_provisiones_nc a, car_dividendos c',
' where c.div_id = a.div_id',
'   and c.emp_id = :f_emp_id',
'   and a.pge_id = :P74_TGE_ID'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(111203081593424714)
,p_name=>'Detalle Provisiones'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_owner=>'ACALLE'
,p_internal_uid=>78949930323659788
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111203461140424758)
,p_db_column_name=>'CLI_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111203559123424758)
,p_db_column_name=>'CXC_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111203672139424758)
,p_db_column_name=>'DIV_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111203766190424759)
,p_db_column_name=>'PNC_VALOR'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Pnc Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PNC_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(70857879609496016)
,p_db_column_name=>'NOMBRE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(70857979613496017)
,p_db_column_name=>'DIV_NRO_VENCIMIENTO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Div Nro Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(70858071536496018)
,p_db_column_name=>'DIV_FECHA_VENCIMIENTO'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Div Fecha Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(111204151769424965)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'789511'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:NOMBRE:CXC_ID:DIV_ID:DIV_NRO_VENCIMIENTO:DIV_FECHA_VENCIMIENTO:PNC_VALOR'
,p_sum_columns_on_break=>'PNC_VALOR'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(111207264653458369)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(111202979603424714)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:72:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111206772487451191)
,p_name=>'P74_TGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(111202979603424714)
,p_prompt=>'Tge Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
