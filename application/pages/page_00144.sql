prompt --application/pages/page_00144
begin
--   Manifest
--     PAGE: 00144
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>144
,p_name=>'APLICACION DESCUENTO'
,p_step_title=>'APLICACION DESCUENTO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>'<meta http-equiv="refresh" content="40">'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(756433369236623501)
,p_plug_name=>'APLICACION DESCUENTO'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(756496158433540915)
,p_name=>'DATOS CLIENTE'
,p_parent_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select x.per_nro_identificacion, x.nombre, x.fecha_venta, x.valor_cuota, x.dscto, x.promocion, x.cxc_id, x.cli_Id, x.div_fecha_vencimiento',
'from(',
'SELECT p.per_nro_identificacion,',
'       TRIM(p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'            p.per_primer_apellido || '' '' || p.per_segundo_apellido) nombre,',
'       a.cxc_fecha_adicion fecha_venta,',
'       b.div_valor_cuota valor_cuota,',
'       round((b.div_valor_cuota * :P144_PORCENTAJE_DESCUENTO), 2) dscto,',
'       ''PROMOCION PRONTO PAGO'' promocion,',
'       a.cli_id,',
'       b.div_fecha_vencimiento,',
'       a.cxc_id,',
'       dense_rank()over(partition by a.cxc_id order by b.div_fecha_vencimiento) top',
'  FROM car_cuentas_por_cobrar a,',
'       car_dividendos         b,',
'       asdm_clientes          c,',
'       asdm_personas          p',
' WHERE b.cxc_id = a.cxc_id',
'   AND c.cli_id = a.cli_id',
'   AND p.per_id = c.per_id',
'   and sign(a.cxc_saldo)=1',
'   and sign(b.div_saldo_cuota)=1',
'   AND a.cxc_estado = ''GEN''',
'   AND a.cxc_estado_registro = 0',
'   AND a.ede_id != 1186 --RULLOA 08/04/2020: Se omiten los seguros.',
'   AND a.emp_id = :f_emp_id',
'   and a.cli_id = :p144_cli_id',
'   --RULLOA 06/05/2020: Se habilita para que se pueda aplicar el descuento varias veces por pedido de Byron Andrade.',
'   AND NOT EXISTS',
' (SELECT NULL FROM car_promo_descuentos d WHERE d.div_id = b.div_id)) x',
'where x.top = 1',
'and trunc(div_fecha_vencimiento,''mm'') >= trunc((sysdate),''mm'')'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468130715592072307)
,p_query_column_id=>1
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>1
,p_column_heading=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(467864901768306630)
,p_query_column_id=>2
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>3
,p_column_heading=>'Nombre'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468131913280072313)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENTA'
,p_column_display_sequence=>5
,p_column_heading=>'FECHA_VENTA'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(467864988721306631)
,p_query_column_id=>4
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Valor cuota'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(467865095594306632)
,p_query_column_id=>5
,p_column_alias=>'DSCTO'
,p_column_display_sequence=>8
,p_column_heading=>'Dscto'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468132703085072314)
,p_query_column_id=>6
,p_column_alias=>'PROMOCION'
,p_column_display_sequence=>4
,p_column_heading=>'PROMOCION'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(467865204626306633)
,p_query_column_id=>7
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>9
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:144:&SESSION.:CARGA_COL:&DEBUG.:RP:P144_CXC_ID,P144_VALOR:#CXC_ID#,#DSCTO#'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468131462118072312)
,p_query_column_id=>8
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(467865304219306634)
,p_query_column_id=>9
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Div fecha vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(756625056711033711)
,p_name=>'CARTERA CLIENTE'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C003 credito,',
'C010 Nro_Vencimiento,',
'C011 Fecha_Vencimiento,',
'C012 Valor_cuota,',
'C002 Valor_aplica,',
'c013 nota',
'FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''COL_DSCTO_PROMO'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'no data found'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468134235302072316)
,p_query_column_id=>1
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>1
,p_column_heading=>'CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468134587875072316)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468135017474072317)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468135445408072318)
,p_query_column_id=>4
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468135754722072318)
,p_query_column_id=>5
,p_column_alias=>'VALOR_APLICA'
,p_column_display_sequence=>5
,p_column_heading=>'VALOR_APLICA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span style="color:red;font-weight:bold;font-size:15;">#VALOR_APLICA#</span>',
''))
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(468136222785072319)
,p_query_column_id=>6
,p_column_alias=>'NOTA'
,p_column_display_sequence=>6
,p_column_heading=>'NOTA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span style="color:green;font-weight:bold;font-size:15;">#NOTA#</span>',
''))
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(468136648474072319)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(756625056711033711)
,p_button_name=>'APLICAR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'APLICAR'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>unistr('javascript:apex.confirm(''UNA VEZ APLICADO EL PAGO NO SE PODR\00C1 REVERSAR. DESEA CONTINUAR?'',''APLICA'');')
,p_button_condition=>'SELECT * FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''COL_DSCTO_PROMO'''
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(468128786963072274)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:&P144_PAG.:&SESSION.:CUOTA_GRATIS:&DEBUG.:RP,144,7::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(468137836498072343)
,p_branch_name=>'br_grabar'
,p_branch_action=>'f?p=&APP_ID.:144:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'APLICA'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(444257276346407167)
,p_name=>'P144_PORCENTAJE_DESCUENTO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select to_number (pe.pen_valor/100)',
'from asdm_e.car_par_entidades pe',
'where pe.pen_id = pq_constantes.fn_retorna_constante(:f_emp_id,''pen_id_par_ent_dscto_cobranza'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(467864842916306629)
,p_name=>'P144_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(467865438313306635)
,p_name=>'P144_VALOR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(468129182165072286)
,p_name=>'P144_CXC_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(468129565205072295)
,p_name=>'P144_PAG'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(468129990100072297)
,p_name=>'P144_COM_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(756433369236623501)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(468133517135072315)
,p_name=>'P144_OBSERVACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(756496158433540915)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(468137327424072341)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
' ln_age_id_agente number := pq_car_credito_interfaz.fn_retorna_agente_conectado(:f_user_id,',
'                                                                            :f_emp_id);',
' ln_valida_col_carga number := 0;',
'begin',
'',
'SELECT count (*)',
'into ln_valida_col_carga',
'FROM APEX_COLLECTIONS WHERE COLLECTION_NAME = ''COL_DSCTO_PROMO'';',
'',
'if ln_valida_col_carga > 0 then',
'pq_car_promociones.pr_aplica_promo_dscto(pn_emp_id         => :f_emp_id,',
'                                           pn_uge_id         => :f_uge_id,',
'                                           pn_usu_id         => :f_user_id,',
'                                           pn_cxc_id         => :p144_cxc_id,',
'                                           pn_age_id_agente  => ln_age_id_agente,',
'                                           pn_age_id_agencia => :f_age_id_agencia,',
'                                           pn_seg_id         => :f_seg_id,',
'                                           pv_error          => :p0_error);',
'end if;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'DESCUENTO APLICADO CORRECTAMENTE'
,p_internal_uid=>435884176154307415
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(468136969811072338)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cuota_gratis'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'                                         ',
'pq_car_promociones.pr_carga_descuento(pn_cxc_id      => :p144_cxc_id,',
'                                      pn_dscto       => :p144_valor,',
'                                      pv_observacion => :p144_observacion,',
'                                      pv_error       => :p0_error) ;                                        '))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGA_COL'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>435883818541307412
);
wwv_flow_imp.component_end;
end;
/
