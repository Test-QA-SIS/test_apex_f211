prompt --application/pages/page_00165
begin
--   Manifest
--     PAGE: 00165
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>165
,p_name=>'Desbloquear Dispositivo Samsung'
,p_page_mode=>'MODAL'
,p_step_title=>'Desbloquear Dispositivo Samsung'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(28584123219306198548)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'16'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20240129123151'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(358104771932071371)
,p_plug_name=>'Desbloquea Dispositivo'
,p_region_name=>'DIALOGO_DESBLOQUEA'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(358104895981071372)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_button_name=>'BT_REGRESAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BELOW_BOX'
,p_button_execute_validations=>'N'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(392809020071891232)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_button_name=>'BT_ACTUALZAR'
,p_button_static_id=>'BT_ACTUALZAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Actualizar Clave'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(358105165965071375)
,p_name=>'P165_PIT_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(358105298503071376)
,p_name=>'P165_PIT_IDE1'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(392808484042891227)
,p_name=>'P165_PIT_CLAVE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(392808925406891231)
,p_name=>'P165_CLAVE'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_prompt=>'Ingrese Clave Temporal del Usuario'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-weight:bold;"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(392809570674891238)
,p_name=>'P165_VALIDA_ERROR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(392810957315891252)
,p_name=>'P165_ERRORES'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(358104771932071371)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(393963515699061129)
,p_name=>'BT_REGRESAR'
,p_event_sequence=>20
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(358104895981071372)
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(524392031804961567)
,p_event_id=>wwv_flow_imp.id(393963515699061129)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP:= ''N'';'
,p_attribute_02=>'F_POPUP'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
,p_da_action_comment=>' apex.navigation.dialog.close(true);'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(395766504353039329)
,p_event_id=>wwv_flow_imp.id(393963515699061129)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>' apex.navigation.dialog.close(true);'
,p_server_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460253774765868496)
,p_event_id=>wwv_flow_imp.id(393963515699061129)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DIALOG_CLOSE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(1460254528438868504)
,p_name=>'da_actualizar'
,p_event_sequence=>30
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(392809020071891232)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460254651056868505)
,p_event_id=>wwv_flow_imp.id(1460254528438868504)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'spin();'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460254771589868506)
,p_event_id=>wwv_flow_imp.id(1460254528438868504)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_error varchar2(500);',
'',
'begin',
'    if :P165_CLAVE is not null then',
'          asdm_p.pq_inv_paybahn.pr_actualizar_clave_knox(pn_pit_id => :P165_PIT_ID,',
'                                                         pn_pit_ide1 => :P165_PIT_IDE1,',
'                                                         pv_pit_clave => :P165_CLAVE,',
'                                                         pv_error => lv_error);',
'',
'          --raise_application_error(-20001,''P165_PIT_ID'' || :P165_PIT_ID || ''P165_PIT_IDE1'' || :P165_PIT_IDE1 || ''P165_CLAVE'' || :P165_CLAVE);',
'          if lv_error is not null then',
'              :P165_VALIDA_ERROR := 1;             ',
'              apex_util.set_session_state(''P165_ERRORES'', lv_error);             ',
'',
'          else',
'              :P165_VALIDA_ERROR := 0;',
'              apex_util.set_session_state(''P165_ERRORES'', '''');',
'              :F_POPUP := ''N'';',
'',
'          end if;',
'    else',
'        apex_util.set_session_state(''P165_ERRORES'', ''Se debe ingresar una Clave'');',
'    end if;',
'end;',
''))
,p_attribute_02=>'P165_PIT_ID,P165_PIT_IDE1,P165_CLAVE'
,p_attribute_03=>'P165_VALIDA_ERROR,P165_ERRORES'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460254856011868507)
,p_event_id=>wwv_flow_imp.id(1460254528438868504)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'removeSpinWait();'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(1460254895680868508)
,p_event_id=>wwv_flow_imp.id(1460254528438868504)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DIALOG_CLOSE'
,p_server_condition_type=>'ITEM_IS_NULL'
,p_server_condition_expr1=>'P165_ERRORES'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(392809175473891234)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACTUALIZA_CLAVE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_error varchar2(500);',
'begin',
'  -- Call the procedure',
'  asdm_p.pq_inv_paybahn.pr_actualizar_clave_knox(pn_pit_id => :P165_PIT_ID,',
'                                                 pn_pit_ide1 => :P165_PIT_IDE1,',
'                                                 pv_pit_clave => :P165_CLAVE,',
'                                                 pv_error => lv_error);',
'  if lv_error is not null then',
'      :P165_VALIDA_ERROR := 1;',
'      raise_application_error(-20000, lv_error);',
'  else',
'      :P165_VALIDA_ERROR := 0;',
'  end if;',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(392809020071891232)
,p_process_when_type=>'NEVER'
,p_internal_uid=>376682599839008771
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(524392088463961568)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA'
,p_process_sql_clob=>':F_POPUP := ''S'';'
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>508265512829079105
);
wwv_flow_imp.component_end;
end;
/
