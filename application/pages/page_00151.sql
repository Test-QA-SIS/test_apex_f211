prompt --application/pages/page_00151
begin
--   Manifest
--     PAGE: 00151
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>151
,p_name=>unistr('Plazo Cr\00E9ditos Refinanciamiento')
,p_step_title=>unistr('Plazo Cr\00E9ditos Refinanciamiento')
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'02'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44850612515131853600)
,p_plug_name=>'CAR_PLAZOS_CREDITOS'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>30
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44850618610751853707)
,p_plug_name=>'Report 1'
,p_region_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select "PCR_ID", ',
'"CXC_ID",',
'"PCR_PLAZO",',
'"USU_ID",',
'"PCR_ESTADO_REGISTRO"',
'from "ASDM_E"."CAR_PLAZOS_CREDITOS" ',
'  ',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(44850618966820853708)
,p_name=>'Report 1'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:151:&APP_SESSION.::::P151_PCR_ID:#PCR_ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_owner=>'JALVAREZ'
,p_internal_uid=>44818365815551088782
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44850619094047853714)
,p_db_column_name=>'PCR_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Pcr Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44850619434622853718)
,p_db_column_name=>'CXC_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Cxc Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44850619850873853718)
,p_db_column_name=>'PCR_PLAZO'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Pcr Plazo'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44850620231776853718)
,p_db_column_name=>'USU_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Usu Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44850620585354853719)
,p_db_column_name=>'PCR_ESTADO_REGISTRO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Pcr Estado Registro'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(44850634236528029785)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'448183811'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'PCR_ID:CXC_ID:PCR_PLAZO:USU_ID:PCR_ESTADO_REGISTRO'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44850613038080853604)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Guardar'
,p_button_position=>'CHANGE'
,p_button_condition=>'P151_PCR_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44850613157394853604)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:151:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44850612927730853604)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Guardar'
,p_button_position=>'CREATE'
,p_button_condition=>'P151_PCR_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44850613102684853604)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P151_PCR_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44850621012504853719)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(44850618610751853707)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Nuevo'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:151:&SESSION.::&DEBUG.:151'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(44850614759185853609)
,p_branch_action=>'f?p=&APP_ID.:151:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850615224319853610)
,p_name=>'P151_PCR_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_use_cache_before_default=>'NO'
,p_source=>'PCR_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_protection_level=>'S'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850615568632853655)
,p_name=>'P151_CXC_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Cr\00E9dito')
,p_source=>'CXC_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''CREDITO: ''||CXC.CXC_ID || '' - CI: '' || PE.PER_NRO_IDENTIFICACION || '' - '' ||',
'       NVL(PE.PER_RAZON_SOCIAL,',
'           PE.PER_PRIMER_APELLIDO || '' '' || PE.PER_SEGUNDO_APELLIDO || '' '' ||',
'           PE.PER_PRIMER_NOMBRE || '' '' || PE.PER_SEGUNDO_NOMBRE) D,',
'       CXC.CXC_ID R',
'  FROM CAR_CUENTAS_POR_COBRAR CXC, ASDM_CLIENTES CL, ASDM_PERSONAS PE',
' WHERE CXC.CLI_ID = CL.CLI_ID',
'   AND CL.PER_ID = PE.PER_ID',
'   AND CL.EMP_ID = :F_EMP_ID',
'   AND PE.EMP_ID = :F_EMP_ID',
'   AND CXC.CXC_SALDO>0',
'   AND CXC.CXC_ESTADO_REGISTRO = 0'))
,p_lov_display_null=>'YES'
,p_cSize=>32
,p_cMaxlength=>255
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850615935043853659)
,p_name=>'P151_PCR_PLAZO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Plazo'
,p_source=>'PCR_PLAZO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850616264934853659)
,p_name=>'P151_USU_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_use_cache_before_default=>'NO'
,p_item_default=>':F_USER_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_source=>'USU_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44850616660439853660)
,p_name=>'P151_PCR_ESTADO_REGISTRO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(44850612515131853600)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Estado Registro'
,p_source=>'PCR_ESTADO_REGISTRO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTADO_REGISTRO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(300);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ESTADO_REG'');',
'return (lv_lov);',
'END;'))
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44850617537991853668)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from CAR_PLAZOS_CREDITOS'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'CAR_PLAZOS_CREDITOS'
,p_attribute_03=>'P151_PCR_ID'
,p_attribute_04=>'PCR_ID'
,p_internal_uid=>44818364386722088742
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44850617860430853669)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of CAR_PLAZOS_CREDITOS'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'CAR_PLAZOS_CREDITOS'
,p_attribute_03=>'P151_PCR_ID'
,p_attribute_04=>'PCR_ID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
,p_internal_uid=>44818364709161088743
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44850618311482853670)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(44850613102684853604)
,p_internal_uid=>44818365160213088744
);
wwv_flow_imp.component_end;
end;
/
