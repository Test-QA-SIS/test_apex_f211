prompt --application/pages/page_00026
begin
--   Manifest
--     PAGE: 00026
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>26
,p_name=>'FacturaOrdenesVentaMayoreo'
,p_step_title=>'FacturaOrdenesVentaMayoreo'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(248950655749727225)
,p_plug_name=>'Pendientes Facturar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ode.ord_id,',
'       ode.ord_fecha,',
'       ode.cli_id,',
'       ode.nombre_completo,',
'       ode.lcm_nombre,',
'       ode.ode_fecha_entrega,',
'       ode.ode_observacion,',
'       ode.age_id_agencia',
'FROM   v_inv_ordenes_despacho ode',
'WHERE     ode.tipo_ord = pq_constantes.fn_retorna_constante(NULL,''cv_ord_tipo_orden_venta'')',
'       AND ode.com_id IS NULL',
'       AND ode.age_id_agencia = :F_AGE_ID_AGENCIA',
'       AND ode.emp_id = :f_emp_id',
'       AND ode.ode_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_num_rows=>15
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(248950757086727225)
,p_name=>'Pendientes Facturar'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_owner=>'ADMIN'
,p_internal_uid=>216697605816962299
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248950962275727226)
,p_db_column_name=>'ORD_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951081633727226)
,p_db_column_name=>'ORD_FECHA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Ord Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'ORD_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951172717727226)
,p_db_column_name=>'CLI_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951259522727226)
,p_db_column_name=>'NOMBRE_COMPLETO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Nombre Completo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951354311727227)
,p_db_column_name=>'LCM_NOMBRE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Lcm Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'LCM_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951483479727227)
,p_db_column_name=>'ODE_FECHA_ENTREGA'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Ode Fecha Entrega'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'ODE_FECHA_ENTREGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951567356727227)
,p_db_column_name=>'ODE_OBSERVACION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Ode Observacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'ODE_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248951666229727227)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Age Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(248951855317727381)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292129484919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'ORD_ID:ORD_FECHA:CLI_ID:NOMBRE_COMPLETO:LCM_NOMBRE:ODE_FECHA_ENTREGA:ODE_OBSERVACION:AGE_ID_AGENCIA'
);
wwv_flow_imp.component_end;
end;
/
