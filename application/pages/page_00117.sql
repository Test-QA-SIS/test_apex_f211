prompt --application/pages/page_00117
begin
--   Manifest
--     PAGE: 00117
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>117
,p_name=>'INGRESO DE ANTICIPO CON RETENCION'
,p_step_title=>'Ingreso de Anticipo con Retencion'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>',
'',
'<script type="text/javascript">',
'var numero_filas=1000;',
'',
'function refresh_ir_report() {',
'          var lThis = $x(''loading-image'');',
'          var lHtml = ''<div class="loading-indicator" align="center"><img src="#IMAGE_PREFIX#ws/ajax-loader.gif" style="margin-right:8px;" align="absmiddle"/>Loading...</div>'';',
'          try {',
'                  lThis =  replaceHtml(lThis,lHtml);',
'          } catch(err) {',
'                  lThis.innerHTML = lHtml;',
'          }',
'          setTimeout(function () { gReport.pull(); lThis.innerHTML = ''''; }, 1000);',
'  }',
'',
'function actualiza_region()',
'{ ',
'refresh_ir_report();',
'$(''#ingreso'').trigger(''apexrefresh'');',
'//$a_report(''6450221250867283'',''1'',numero_filas,''1000'');',
'}',
'function actualiza_collection(fila,seq_id)',
'{',
'',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_ACT_RETENCION'',0);',
'  get.addParam(''x10'',seq_id);',
'  get.addParam(''x07'',$x(''f47_'' + fila).value);',
'  gReturn = get.get();',
'  get = null;',
'  actualiza_region();',
'',
'}',
'',
'function enter2tab(evt,itemid)',
'{',
'    if (evt.keyCode == 13)',
'   {',
'     $x(itemid).focus();',
'     return false;',
'   }',
' return true;',
'}',
'',
'function valida_numEnterTab(evt,itemid,ln_valor){',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'  if (evt.keyCode == 13) {',
'     ',
'        $x(itemid).focus();',
'        return false;',
'           ',
'   }else if(evt.keyCode == 8){',
'       return true;',
'   }else if(evt.keyCode == 9){',
'       return true;',
'   }else if(evt.keyCode == null){',
'       return true;  ',
'   }else{',
'       if (!patron_numero.test((ln_valor).value)) {',
unistr('        alert(''Ingrese solo n\00FAmeros'');'),
'        ln_valor.value = '''';',
'        html_GetElement((ln_valor).id).focus();',
'        return true;',
'        } ',
'    }',
' return false;',
'}',
'',
'function actualiza(fila, valor_calc, seq_id, valor_ret)',
'{',
'var ln_suma = valor_calc + 0.5;',
'var ln_resta = valor_calc - 0.5;',
'',
'if (valor_ret < 0){alert(''Debe ingresar valores mayores a cero'');}',
'if (valor_ret > ln_suma){alert(''El valor supera al valor calculado mas la tolerancia :''+ln_suma);}',
'if (valor_ret < ln_resta){alert(''El valor es muy inferior al valor calculado menos la tolerancia :''+ln_resta);}',
'',
'actualiza_collection(fila,seq_id);',
'}',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00C2\00BF\00C2\00BF\00C2\00BF\00C2\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'</script>'))
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'02'
,p_last_updated_by=>'LBERREZUETA'
,p_last_upd_yyyymmddhh24miss=>'20240205123113'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(52181670047577938)
,p_plug_name=>'parametros'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_2'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':f_emp_id is null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(52196672142578016)
,p_plug_name=>'<SPAN STYLE="font-size: 12pt">INGRESO DE ANTICIPO CON RETENCION</span>'
,p_region_name=>'ingreso'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(52202568463578051)
,p_plug_name=>'Datos de Retencion'
,p_parent_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523966992046668)
,p_plug_display_sequence=>110
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_when_condition=>':P117_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>':P64_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(52208265616578059)
,p_name=>'retenciones'
,p_parent_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>13
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'seq_id id,',
'to_number(c001) com_id,',
'c002 fecha_ret,',
'c003 fecha_validez,',
'c004 nro_retencion,',
'c005 nro_autorizacion,',
'c006 nro_establecimiento,',
'c007 nro_pto_emision,',
'c008 porcentaje,',
'to_number(c009) base,',
'to_number(c013) valor_calculado,',
'apex_item.text(p_idx        => 25,',
'                      p_value      => to_number(c010),',
'                      p_attributes => ''unreadonly id="f47_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c013||'',''||seq_id||'',this.value)"'',',
'                      p_size       => ''10'') valor_retencion,',
'''X'' eliminar,',
'c026 tipo',
'',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION''',
'UNION',
'select ',
'NULL id,',
'NULL com_id,',
'NULL fecha_ret,',
'NULL fecha_validez,',
'NULL nro_retencion,',
'NULL nro_autorizacion,',
'NULL nro_establecimiento,',
'NULL nro_pto_emision,',
'NULL porcentaje,',
'SUM(to_number(c009)) base,',
'sum(to_number(c013)) valor_calculado,',
'TO_CHAR(SUM(to_number(c010))) valor_retencion,',
'NULL eliminar,',
'null tipo',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52208478138578059)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>13
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52208578322578059)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Com Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52208651951578059)
,p_query_column_id=>3
,p_column_alias=>'FECHA_RET'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Ret'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52208772077578060)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VALIDEZ'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Validez'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52208882607578060)
,p_query_column_id=>5
,p_column_alias=>'NRO_RETENCION'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Retencion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52208959461578060)
,p_query_column_id=>6
,p_column_alias=>'NRO_AUTORIZACION'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Autorizacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209066927578060)
,p_query_column_id=>7
,p_column_alias=>'NRO_ESTABLECIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Nro Establecimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209157378578060)
,p_query_column_id=>8
,p_column_alias=>'NRO_PTO_EMISION'
,p_column_display_sequence=>7
,p_column_heading=>'Nro Pto Emision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209275822578060)
,p_query_column_id=>9
,p_column_alias=>'PORCENTAJE'
,p_column_display_sequence=>8
,p_column_heading=>'Porcentaje'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209365700578060)
,p_query_column_id=>10
,p_column_alias=>'BASE'
,p_column_display_sequence=>10
,p_column_heading=>'Base'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209451631578060)
,p_query_column_id=>11
,p_column_alias=>'VALOR_CALCULADO'
,p_column_display_sequence=>11
,p_column_heading=>'Valor Calculado'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209578548578060)
,p_query_column_id=>12
,p_column_alias=>'VALOR_RETENCION'
,p_column_display_sequence=>12
,p_column_heading=>'Valor Retencion'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52209655729578062)
,p_query_column_id=>13
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>14
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:117:&SESSION.:ELIMINA_RET:&DEBUG.::P117_SEQ_ID_RET:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7207478742657180963)
,p_query_column_id=>14
,p_column_alias=>'TIPO'
,p_column_display_sequence=>9
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(52223975342578086)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_parent_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>14
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);',
''))
,p_display_when_condition=>':P117_CLI_ID is not null'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224172301578087)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_SEQ_ID,P30_TFP_ID,P30_EDE_ID,P30_PLAN,P30_MCD_VALOR_MOVIMIENTO,P30_CRE_TITULAR_CUENTA,P30_CRE_NRO_CUENTA,P30_CRE_NRO_CHEQUE,P30_CRE_NRO_PIN,P30_TJ_NUMERO,P30_MCD_NRO_AUT_REF,P30_TJ_NUMERO_VOUCHER,P30_LOTE,P30_M'
||'CD_NRO_COMPROBANTE,P30_RAP_NRO_RETENCION,P30_SALDO_RETENCION,P30_RAP_COM_ID,P30_RAP_FECHA,P30_RAP_FECHA_VALIDEZ,P30_RAP_NRO_AUTORIZACION,P30_MODIFICACION,P30_RAP_NRO_ESTABL_RET,P30_RAP_NRO_PEMISION_RET,P30_PRE_ID:#COL03#,#COL04#,#COL05#,#COL05#,#COL3'
||'0#,#COL11#,#COL12#,#COL13#,#COL14#,#COL15#,#COL16#,#COL17#,#COL18#,#COL19#,#COL20#,#COL21#,#COL22#,#COL23#,#COL24#,#COL25#,S,#COL26#,#COL27#,#COL28#'
,p_column_linktext=>'Editar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224259348578087)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:64:&SESSION.:eliminar:&DEBUG.::P64_SEQ_ID,P64_COM_ID_COL_MOVCAJA:#COL03#,#COL22#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224373442578087)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224476018578088)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224553048578088)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224668196578088)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224773083578088)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224881128578088)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52224969914578088)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225083658578088)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225176620578088)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225279701578088)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225381927578088)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225458126578088)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225582398578088)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225673997578088)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225762575578088)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225883924578088)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52225966598578088)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226064622578088)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>22
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226162328578088)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>23
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_cattributes_element=>'style="color:RED;"'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226255494578088)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>24
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226362696578089)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>25
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226469225578090)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>26
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226563095578090)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>27
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226667902578090)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>20
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226775339578090)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>21
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226882384578090)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52226957775578090)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52227063800578090)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(52265479637669973)
,p_plug_name=>'PAGO'
,p_parent_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>140
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(52210377961578063)
,p_name=>'detalle_retencion'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>130
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name = ''COLL_DET_RETENCION'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52210575836578064)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52210674580578064)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52210762516578064)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52210873855578064)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52210958326578065)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211079274578065)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211158817578065)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211276341578065)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211378957578065)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211457396578066)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211573321578066)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211680992578066)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211759472578066)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211867204578066)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52211964963578066)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212073120578066)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212158386578066)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212267884578066)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212362554578066)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212460741578066)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212572768578066)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212682349578066)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212770542578066)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212862429578066)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52212960001578066)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213058018578066)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213157005578066)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213262971578066)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213355604578066)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213467629578067)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213572017578067)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213662794578067)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213782981578067)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213866722578067)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52213969373578067)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214068563578067)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214158897578067)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214279219578067)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214369619578067)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214452619578068)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214578651578068)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214660508578070)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214773560578070)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214856352578070)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52214972334578070)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215080079578070)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215169259578070)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215271941578070)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215368117578070)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215473122578070)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215565986578070)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215674172578071)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215753945578071)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215865271578071)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52215972167578071)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216066147578071)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216183770578074)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216254380578074)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216381842578074)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216451842578074)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216578447578074)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216660292578074)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216752231578074)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216861460578074)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52216965729578074)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217083542578074)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(52217167986578076)
,p_name=>'mov_caja'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                                                      ''cv_coleccion_mov_caja'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217380102578076)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217457972578077)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217552129578077)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217682377578077)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217773691578077)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217862494578077)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52217971233578077)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218052500578077)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218174789578077)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218257768578077)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218370556578077)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218473208578077)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218560488578077)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218668393578077)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218753906578077)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218878636578077)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52218974887578077)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219076389578077)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219168247578077)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219253086578077)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219359359578077)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219455037578079)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219557549578079)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219653256578079)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219757406578079)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219864213578079)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52219960002578080)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220078455578082)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220164008578082)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220282630578082)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220359435578082)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220462232578082)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220583661578082)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220655173578083)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220763362578083)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220878598578083)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52220960227578083)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221060116578083)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221178940578083)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221253201578083)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221378987578083)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221458599578085)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221567759578085)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221655697578085)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221777985578085)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221881433578085)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52221975011578085)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222070444578085)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222158916578085)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222253456578085)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222356919578085)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222459620578085)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222567400578085)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222677741578085)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222760726578085)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222867876578085)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52222980921578085)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223052216578085)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223151738578085)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223268491578085)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223357330578086)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223463564578086)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223574497578086)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223653818578086)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223752797578086)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(52223870527578086)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(52209776382578062)
,p_button_sequence=>830
,p_button_plug_id=>wwv_flow_imp.id(52208265616578059)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR_RETENCION'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>'(select count(*) from apex_collections where collection_name = ''COLL_VALOR_RETENCION'') > 0'
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(52202781508578051)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Create'
,p_button_position=>'CREATE'
,p_button_condition_type=>'NEVER'
,p_database_action=>'INSERT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(52189081718577997)
,p_button_sequence=>.5
,p_button_name=>'PAGAR_SELECCIONADOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Pagar Seleccionados'
,p_button_position=>'LEGACY_ORPHAN_COMPONENTS'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(53928476173019065)
,p_button_sequence=>840
,p_button_plug_id=>wwv_flow_imp.id(52265479637669973)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Generar Movimiento'
,p_button_position=>'TOP'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(52234074898578140)
,p_branch_name=>'br_grabar'
,p_branch_action=>'f?p=&APP_ID.:117:&SESSION.:GRABAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(53928476173019065)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(52234262144578146)
,p_branch_action=>'f?p=&APP_ID.:117:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>50
,p_branch_comment=>'Created 07-NOV-2011 10:30 by YGUAMAN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52181860918577939)
,p_name=>'P117_COM_ID'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52182083827577944)
,p_name=>'P117_COM_NUMERO'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Com Numero'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52182264529577945)
,p_name=>'P117_DIV_NRO_VENCIMIENTO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Div Nro Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52182459652577945)
,p_name=>'P117_DIV_ID'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Div Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52182658861577945)
,p_name=>'P117_DIV_SALDO_INTERES_MORA'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Div Saldo Interes Mora'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52182853815577945)
,p_name=>'P117_DIV_SALDO_CUOTA'
,p_item_sequence=>410
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Div Saldo Cuota'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52183075271577947)
,p_name=>'P117_REQUEST'
,p_item_sequence=>420
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Request'
,p_source=>':REQUEST'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52183254611577947)
,p_name=>'P117_COM_NUMERO_COMPLETO'
,p_item_sequence=>430
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Com Numero Completo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52183467312577947)
,p_name=>'P117_DIV_SALDO_GASTO_COBRANZA'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Div Saldo Gasto Cobranza'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52183664188577947)
,p_name=>'P117_EMP_ID'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Emp Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52183851502577947)
,p_name=>'P117_DIV_FECHA_VENCIMIENTO'
,p_item_sequence=>370
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Div Fecha Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52184080880577948)
,p_name=>'P117_CXC_SALDO'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_imp.id(52181670047577938)
,p_prompt=>'Cxc Saldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52196865208578016)
,p_name=>'P117_FECHA_HASTA_CORTE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Fecha Hasta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_FECHAS_CORTE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c001 DESCRIPCION,',
'       a.c002 VALOR',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_FECHAS_CORTE'''))
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
,p_item_comment=>'onChange="pr_ejecuta_proc_solo_campos(''PR_FILTRA_DIV_PENDIENTES'',''P6_CLI_ID'',''P6_F_EMP_ID'',''P6_F_SEG_ID'',''P6_FECHA_DESDE'',''P6_FECHA_HASTA_CORTE'',''R70263701157111210'',''R70272713295111230'')"'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52197066130578018)
,p_name=>'P117_AGENTE_COBRADOR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Cobrador:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52197260283578018)
,p_name=>'P117_CLI_ID_NOMBRE_REFERIDO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Cli Id Nombre Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52197467225578018)
,p_name=>'P117_PER_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')  THEN',
'return pq_constantes.fn_retorna_constante(NULL,''cv_per_tipo_iden_ced'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(NULL,''cv_per_tipo_iden_ruc''); ',
'END IF'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tipo Identificacion:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52197667858578019)
,p_name=>'P117_FECHA_DESDE'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Fecha Desde:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52197881709578019)
,p_name=>'P117_FECHA_HASTA'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Fecha Hasta:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="pr_ejecuta_proc_solo_campos(''PR_CARGA_DIV_PENDIENTES'',''P6_CLI_ID'',''P6_F_EMP_ID'',''P6_F_SEG_ID'',''P6_FECHA_DESDE'',''P6_FECHA_HASTA'',''R70263701157111210'',''R70272713295111230'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_item_comment=>'No tenia condicion.'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52198057358578020)
,p_name=>'P117_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Cliente: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52198269077578020)
,p_name=>'P117_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>' - '
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>20
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'');";onKeyDown="if(event.keyCode==13) doSubmit(''cliente'');"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52198473736578020)
,p_name=>'P117_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52198660613578025)
,p_name=>'P117_CORREO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52198859767578026)
,p_name=>'P117_DIRECCION'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52199081413578026)
,p_name=>'P117_TIPO_DIRECCION'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>unistr('Direcci\00BF\00BFn')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52199254705578026)
,p_name=>'P117_TIPO_TELEFONO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>unistr('Tel\00BF\00BFfono')
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52199475551578026)
,p_name=>'P117_TELEFONO'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52199659710578026)
,p_name=>'P117_CLIENTE_EXISTE'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52199883212578027)
,p_name=>'P117_CXC_ID'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52200056421578031)
,p_name=>'P117_VALOR_PAGO'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_item_default=>'0'
,p_prompt=>'Pagar lo que alcance con este valor '
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''valor_cuota'')";onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Con este valor se cancelaran las CUOTAS VENCIDAS que alcancen.'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52200582093578037)
,p_name=>'P117_TOTAL_CHEQUES'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>12
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P117_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52200760560578045)
,p_name=>'P117_SALDO_ANTICIPOS'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Anticipos'
,p_source=>'pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P117_CLI_ID,:F_UGE_ID);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>12
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P117_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52200977057578045)
,p_name=>'P117_DIR_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Dir Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52201166675578046)
,p_name=>'P117_CLI_ID_REFERIDO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Cli Id Referido'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52201366575578049)
,p_name=>'P117_F_EMP_ID'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Emp Id'
,p_source=>':F_EMP_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52201575997578049)
,p_name=>'P117_F_SEG_ID'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Seg Id'
,p_source=>':F_SEG_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52201768917578049)
,p_name=>'P117_TFP_ID'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_item_default=>'pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tfp Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52201981739578050)
,p_name=>'P117_NOMBRE_INSTITUCION'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'  - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52202161172578050)
,p_name=>'P117_INS_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_prompt=>'Institucion:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52202380883578051)
,p_name=>'P117_RET_IVA'
,p_item_sequence=>45
,p_item_plug_id=>wwv_flow_imp.id(52196672142578016)
,p_item_default=>'S'
,p_prompt=>'Retiene Iva'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Si;S,No;N'
,p_cHeight=>1
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52202980935578051)
,p_name=>'P117_RAP_NRO_RETENCION'
,p_item_sequence=>560
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P117_RAP_NRO_AUTORIZACION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52203154111578052)
,p_name=>'P117_RAP_NRO_AUTORIZACION'
,p_item_sequence=>570
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Nro Autorizacion:'
,p_post_element_text=>unistr(' 10 \00F3 37 \00F3 49')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>55
,p_cMaxlength=>49
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P117_RAP_FECHA'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52203351446578052)
,p_name=>'P117_RAP_FECHA'
,p_item_sequence=>580
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Ret.:'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return enter2tab(event,''P117_RAP_FECHA_VALIDEZ'')"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52203555284578052)
,p_name=>'P117_RAP_FECHA_VALIDEZ'
,p_item_sequence=>590
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return enter2tab(event,''P117_RAP_COM_ID'')"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52203779433578052)
,p_name=>'P117_RAP_COM_ID'
,p_item_sequence=>600
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Factura:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LV_VEN_COMP_RET_ANTICIPO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'',
'IF :P117_F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'') THEN',
'',
'    lv_lov := ',
'      pq_ven_pagos_cuota_retencion.fn_lov_facturas_ret_anticipo(pn_emp_id => :f_emp_id,',
'                                                                       pn_cli_id => :P117_CLI_ID);',
'ELSE ',
'',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_VEN_COMPROBANTES_F_ND_SIN_RET'');',
'lv_lov := lv_lov || '' AND vco.cli_id = :P117_CLI_ID ORDER BY vco.com_id'';',
'',
'END IF;',
'',
'',
'    RETURN lv_lov;',
'',
'end;',
'',
''))
,p_cSize=>20
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''carga_subtotal'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52203980353578053)
,p_name=>'P117_RAP_NRO_ESTABL_RET'
,p_item_sequence=>540
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Nro Retencion:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P117_RAP_NRO_PEMISION_RET'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52204183618578053)
,p_name=>'P117_RAP_NRO_PEMISION_RET'
,p_item_sequence=>550
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>' -'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P117_RAP_NRO_RETENCION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52204369927578054)
,p_name=>'P117_PRE_ID'
,p_item_sequence=>650
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Pre Id:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52204552045578054)
,p_name=>'P117_PRECIONEENTER'
,p_item_sequence=>630
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Presione Enter para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52204752974578054)
,p_name=>'P117_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>660
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Valor Recibido:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''cargar_tfp'');" '
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P117_VALOR_TOTAL_PAGOS > 0'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Presione Enter para cargar el valor'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52205266070578055)
,p_name=>'P117_SALDO_RETENCION'
,p_item_sequence=>670
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52205459960578055)
,p_name=>'P117_COM_ID_COL_MOVCAJA'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Com Id Col Movcaja'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52205658141578055)
,p_name=>'P117_FLETE'
,p_item_sequence=>720
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Flete'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52205860947578055)
,p_name=>'P117_SUBTOTAL'
,p_item_sequence=>710
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Subtotal'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52206059617578056)
,p_name=>'P117_IVA_BIENES'
,p_item_sequence=>810
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Iva Bienes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P117_RET_IVA = ''S'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>':P117_IVA - :P117_IVA_FLETE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52206253796578056)
,p_name=>'P117_DEA_ID'
,p_item_sequence=>725
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dea Id'
,p_source=>'DEA_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52206477258578056)
,p_name=>'P117_DEA_SECCION'
,p_item_sequence=>726
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_use_cache_before_default=>'NO'
,p_item_default=>'&SESSION.'
,p_prompt=>'Dea Seccion'
,p_source=>'DEA_SECCION'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>60
,p_cMaxlength=>1000
,p_cHeight=>4
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52206653251578056)
,p_name=>'P117_DEA_DOCUMENTO1'
,p_item_sequence=>575
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_use_cache_before_default=>'NO'
,p_prompt=>'XML'
,p_source=>'DEA_DOCUMENTO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'onchange="doSubmit(''CREATE'')"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DB_COLUMN'
,p_attribute_06=>'Y'
,p_attribute_08=>'attachment'
,p_attribute_12=>'NATIVE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52206859785578057)
,p_name=>'P117_SUBTOTAL_IVA'
,p_item_sequence=>690
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Subtotal Con Iva'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vco_valor_variable',
'        from ven_var_Comprobantes a',
'       where a.com_id = :P117_RAP_COM_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_con_iva'');'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52207057869578057)
,p_name=>'P117_SUBTOTAL_SIN_IVA'
,p_item_sequence=>700
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Subtotal Sin Iva'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vco_valor_variable',
'        from ven_var_Comprobantes a',
'       where a.com_id = :P117_RAP_COM_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_sin_iva'');'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52207260964578057)
,p_name=>'P117_TRE_ID'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'%'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  if :P117_RET_IVA = ''S'' then',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion , RPO_id',
'FROM ASDM_E.CAR_RET_PORCENTAJES',
'where rpo_emp_id = ''||:f_emp_id || '' and RPO_ESTADO_REGISTRO = 0'';',
'  else',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion, RPO_id',
'FROM ASDM_E.CAR_RET_PORCENTAJES',
'WHERE RPO_DESCRIPCION = ''''RET FUENTE''''',
'and rpo_emp_id = ''|| :f_emp_id || '' and RPO_ESTADO_REGISTRO = 0'';',
'  end if;',
'  RETURN(lv_lov);',
'END;',
''))
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52207460434578057)
,p_name=>'P117_BASE'
,p_item_sequence=>620
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Base'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGA_RET'');" '
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52207665344578058)
,p_name=>'P117_SEQ_ID_RET'
,p_item_sequence=>680
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Seq Id Ret'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52207876864578058)
,p_name=>'P117_IVA'
,p_item_sequence=>800
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Iva'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P117_RET_IVA = ''S'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vco_valor_variable',
'        from ven_var_Comprobantes a',
'       where a.com_id = :P117_RAP_COM_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_iva_calculo'');'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52208061359578059)
,p_name=>'P117_IVA_FLETE'
,p_item_sequence=>805
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Iva Flete'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P117_RET_IVA = ''S'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select vd.vcd_valor_variable',
'  from ven_comprobantes_det de, ven_var_comprobantes_det vd',
' where de.com_id = :p117_RAP_COM_ID',
'   and de.ite_sku_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_item_serv_logisticos'')',
'   and de.cde_id = vd.cde_id',
'   and vd.var_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_iva'');'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52209967206578062)
,p_name=>'P117_SUBTOTAL_FACT'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(52208265616578059)
,p_prompt=>'Subtotal Fact'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52210153012578062)
,p_name=>'P117_OBSERVACION'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(52208265616578059)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52267055232700085)
,p_name=>'P117_TOTAL'
,p_item_sequence=>830
,p_item_plug_id=>wwv_flow_imp.id(52265479637669973)
,p_use_cache_before_default=>'NO'
,p_prompt=>'TOTAL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(c006)',
'from apex_collections ',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                              ''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(53920559889804260)
,p_name=>'P117_OBSERVACIONES'
,p_item_sequence=>840
,p_item_plug_id=>wwv_flow_imp.id(52265479637669973)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>4000
,p_cHeight=>5
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(53921080148810140)
,p_name=>'P117_SEQ_ID'
,p_item_sequence=>850
,p_item_plug_id=>wwv_flow_imp.id(52265479637669973)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(76463062374708489)
,p_name=>'P117_XXX'
,p_item_sequence=>860
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_item_default=>':p0_ttr_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Xxx'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(5581427962642086106)
,p_name=>'P117_INT_DIFERIDO'
,p_item_sequence=>870
,p_item_plug_id=>wwv_flow_imp.id(52202568463578051)
,p_prompt=>'Int Diferido'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round(a.vco_valor_variable,2)',
'        from ven_var_Comprobantes a',
'       where a.com_id = :P117_RAP_COM_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_int_diferido'');'))
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52227269855578092)
,p_validation_name=>'P117_RAP_NRO_AUTORIZACION'
,p_validation_sequence=>30
,p_validation=>'to_number(:P117_RAP_NRO_AUTORIZACION) > 0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de autorizaci\00BF\00BFn de la retenci\00BF\00BFn')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(52203154111578052)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52227475384578098)
,p_validation_name=>'P117_RAP_FECHA'
,p_validation_sequence=>40
,p_validation=>':P117_RAP_FECHA IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la fecha de la retenci\00BF\00BFn')
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(52203351446578052)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52227673997578099)
,p_validation_name=>'P117_RAP_FECHA_VALIDEZ'
,p_validation_sequence=>50
,p_validation=>':P117_RAP_FECHA_VALIDEZ IS NOT NULL'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese la fecha de validez de la retenci\00BF\00BFn')
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(52203555284578052)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52227865016578099)
,p_validation_name=>'P117_RAP_COM_ID'
,p_validation_sequence=>60
,p_validation=>'P117_RAP_COM_ID'
,p_validation_type=>'ITEM_NOT_NULL_OR_ZERO'
,p_error_message=>unistr('Ingrese la factura a la que pertenece la retenci\00BF\00BFn')
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(52203779433578052)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52228064788578099)
,p_validation_name=>'P117_RAP_NRO_RETENCION'
,p_validation_sequence=>70
,p_validation=>'to_number(:P117_RAP_NRO_RETENCION) > 0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de retenci\00BF\00BFn')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52228255700578099)
,p_validation_name=>'P117_RAP_NRO_ESTABL_RET'
,p_validation_sequence=>70
,p_validation=>'to_number(:P117_RAP_NRO_ESTABL_RET) > 0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero de establecimiento de la retenci\00BF\00BFn')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(52203980353578053)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52228471998578099)
,p_validation_name=>'P117_RAP_NRO_PEMISION_RET'
,p_validation_sequence=>80
,p_validation=>'to_number(:P117_RAP_NRO_PEMISION_RET) >0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('Ingrese el n\00BF\00BFmero del punto de emisi\00BF\00BFn de la retenci\00BF\00BFn')
,p_always_execute=>'Y'
,p_validation_condition=>':request= ''cargar_tfp'''
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(52204183618578053)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52228679353578099)
,p_validation_name=>'P117_TRE_ID'
,p_validation_sequence=>90
,p_validation=>'P117_TRE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Porcentaje'
,p_validation_condition=>'CARGA_RET'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(52207260964578057)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(52232370265578119)
,p_name=>'pr_actualiza_pagina'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(52232665726578134)
,p_event_id=>wwv_flow_imp.id(52232370265578119)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52229575906578105)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'',
'if :p117_cli_id is null then',
'if apex_collection.collection_exists(''COLL_VALOR_RETENCION'') then',
'      apex_collection.delete_collection(''COLL_VALOR_RETENCION'');',
'end if;',
'end if;',
'',
'',
'',
'ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja_otros'');',
':P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_ing_anticipo_cli'');',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:F_EMP_ID,',
'                                                        :F_PCA_ID,',
'                                                        ln_tgr_id ,',
'                                                        :P0_TTR_DESCRIPCION, ',
'                                                        :P0_PERIODO,',
'                                                        :P0_DATFOLIO,',
'                                                        :P0_FOL_SEC_ACTUAL,',
'                                                        :P0_pue_num_sri,',
'                                                        :P0_uge_num_est_sri,',
'                                                        :P0_PUE_ID,',
'                                                        :P0_TTR_ID,',
'                                                        :P0_NRO_FOLIO, ',
'                                                        :P0_ERROR); --:P117_COM_NUMERO_COMPLETO);',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>19976424636813179
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52229180084578105)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from ASDM_DOC_ELEC_AUX'
,p_attribute_02=>'ASDM_DOC_ELEC_AUX'
,p_attribute_03=>'P117_DEA_ID'
,p_attribute_04=>'DEA_ID'
,p_process_when_type=>'NEVER'
,p_internal_uid=>19976028814813179
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52231557415578111)
,p_process_sequence=>100
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_linea_coleccion_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota_retencion.pr_elimina_linea_col_movcaja(pn_com_id => :P117_COM_ID_COL_MOVCAJA,',
'                                                          pn_seq_id => :p117_seq_id,',
'                                                          pv_error  => :p0_error);',
':P117_SEQ_ID := NULL; ',
':P117_MCD_VALOR_MOVIMIENTO := NULL;',
':P117_COM_ID_COL_MOVCAJA := NULL;',
'',
'--:P30_TFP_ID_ELIMINAR := NULL;',
'--:P30_MODIFICACION := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>19978406145813185
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52231766173578111)
,p_process_sequence=>100
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_rap_id asdm_retenciones_aplicadas.rap_id%TYPE; -- Yguaman 2011/10/22 10:53am ',
'',
'  Ln_eje_eve_id  number;',
'  Lv_Error       varchar2(3500);',
'  ln_mca_id      con_movimientos_cabecera.mca_id%type;',
'  ln_mov_caja    asdm_movimientos_caja.mca_id%TYPE;',
'  lv_observacion varchar2(3000);',
'  ln_trx_id number;',
'',
'BEGIN',
'',
'',
'',
'  lv_observacion := replace(:P117_OBSERVACIONES, '','', ''.'');',
'',
unistr('  --- Yessica Guam\00BF\00BFn 2011/10/22. Cuando es ingreso de anticipo por retenci\00BF\00BFn debe grabarse los datos de la retenci\00BF\00BFn primero ya que se reemplaza el # de retenci\00BF\00BFn por el id de retenci\00BF\00BFn generado'),
'  pq_ven_pagos_cuota.pr_graba_pago_retencion(pn_emp_id   => :f_emp_id,',
'                                             pv_facturar => ''M'',pn_usu_id => :f_user_id,pn_uge_id => :f_uge_id,PV_SECCION => :session,',
'                                             pv_error    => :p0_error);',
'',
'  pq_swi_gen.pr_iniciar_ejecucion_evento(''ANT_CLIENTES'',',
'                                         Ln_eje_eve_id,',
'                                         Lv_Error);',
'  if Lv_Error is null then',
'  ',
'    --transaccion',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pn_emp_id'',',
'                                       :F_EMP_ID,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pn_uge_id'',',
'                                       :F_UGE_ID,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pn_uge_id_gasto'',',
'                                       :F_UGE_ID_GASTO,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pn_usu_id'',',
'                                       :F_USER_ID,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pn_ttr_id'',',
'                                       :P0_TTR_ID,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pn_trx_id'',',
'                                       ln_trx_id,',
'                                       Lv_Error);',
'  ',
'    --Movimiento caja',
'    ',
'    ',
'  ',
'    ',
'    ',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_MCA_ID'',',
'                                       ln_mov_caja,',
'                                       Lv_Error);',
'                                       ',
'                                       ',
'                                       ',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_PCA_ID'',',
'                                       :f_pca_id,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_MCA_ID_REFERENCIA'',',
'                                       NULL,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_AGE_ID_GESTIONADO_POR'',',
'                                       NULL,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_CLI_ID'',',
'                                       :P117_CLI_ID,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PD_MCA_FECHA'',',
'                                       TO_DATE(sysdate,',
'                                               ''DD/MM/YYYY HH24:MI:SS''),',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_MCA_OBSERVACION'',',
'                                       lv_observacion,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_MCA_ESTADO_MC'',',
'                                       :NULL,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_MCA_TOTAL'',',
'                                       :P117_TOTAL,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_COM_ID'',',
'                                       :NULL,',
'                                       Lv_Error);',
'  ',
'  ',
'  ',
'    -- Anticipo de Clientes',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_ACL_OBSERVACIONES'',',
'                                       lv_observacion,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pv_error'',',
'                                       :P0_error,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PD_FECHA'',',
'                                       SYSDATE,',
'                                       Lv_Error);',
'  ',
'  ',
'',
'  ',
'    -- Redirect despues grabar ant clientes',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_DESDE'',',
'                                       ''MC'',',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_APP_ID'',',
'                                       NULL,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_PAG_ID'',',
'                                       NULL,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_SESSION'',',
'                                       :session,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_TOKEN'',',
'                                       :f_token,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_USER_ID'',',
'                                       :f_user_id,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_ROL'',',
'                                       :p0_rol,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_ROL_DESC'',',
'                                       :p0_rol_desc,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_TREE_ROT'',',
'                                       :p0_tree_root,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_OPCION'',',
'                                       :f_opcion_id,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_PARAMETRO'',',
'                                       :f_parametro,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''pv_empresa'',',
'                                       :f_empresa,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PV_UGESTION'',',
'                                       :f_ugestion,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_PRO_ID'',',
'                                       null,--:P8_PRO_ID,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_VALOR'',',
'                                       null,--:P8_VALOR_ORDEN_PAGO,',
'                                       Lv_Error);',
'    pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,',
'                                       ''PN_UUG_ID'',',
'                                       :F_UUG_ID,',
'                                       Lv_Error);',
'  ',
'',
'  ',
'    if Lv_Error is null then',
'      pq_swi_gen.pr_ejecutar_evento(Ln_eje_eve_id, Lv_Error);',
'    end if;',
'  ',
'       ',
'  ',
'    if Lv_Error is null then',
'      pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                         ''PN_MCA_ID'',',
'                                         ln_mov_caja,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                         ''pn_trx_id'',',
'                                         ln_trx_id,',
'                                         Lv_Error);',
'      pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id,',
'                                         ''pv_error'',',
'                                         :lv_error,',
'                                         Lv_Error);',
'    ',
'    end if;',
'  ',
'    if Lv_Error is null then',
'      pq_swi_gen.pr_cerrar_ejecucion_evento(Ln_eje_eve_id, Lv_Error);',
'    end if;',
'  end if;',
'',
'  :P0_ERROR := Lv_Error;',
'',
'  pq_con_funciones_rubros.pr_cargar_transacciones(pn_trx_id    => ln_trx_id,',
'                                                  pn_mca_id    => ln_mca_id,',
'                                                  pn_mon_id    => asdm_p.pq_tes_reposicion_gastos.fn_dev_moneda_principal(pn_emp_id => :f_emp_id),',
'                                                  pd_rep_fecha => sysdate,',
'                                                  pv_error     => :p0_error);',
'',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_mov_caja''));',
'',
'end;',
'if :P0_ERROR is null then',
'  :P117_CLI_ID               := null;',
'  :P117_IDENTIFICACION       := null;',
' ',
'end if;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'pr_graba_factura_pago_cuota'
,p_process_when_button_id=>wwv_flow_imp.id(118594174254586217)
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>19978614903813185
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52231362022578111)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  pq_ven_pagos_cuota.pr_valida_pago_retencion(pn_nro_retencion        => :p117_rap_nro_retencion,',
'                                              pn_emp_id               => :f_emp_id,',
'                                              pn_com_id               => :p117_rap_com_id,',
'                                              pv_facturar             => ''R'', --- Pago con Retencion             ',
'                                              pv_rap_nro_establ_ret   => :p117_rap_nro_establ_ret,',
'                                              pv_rap_nro_pemision_ret => :p117_rap_nro_pemision_ret,',
'                                              pn_saldo_retencion      => :p117_saldo_retencion,',
'                                              pn_mcd_valor_movimiento => :p117_mcd_valor_movimiento,',
'                                              pn_valor_total_pagos    => :p117_mcd_valor_movimiento, --          :P117_VALOR_TOTAL_PAGOS, -- Usado cuando viene de facturacion',
'                                              pv_modificacion         => ''N'', --:P117_MODIFICACION,',
'                                              pn_pre_id               => :p117_pre_id,',
'                                              pv_dejar_saldo          => ''S'',',
'                                              pv_rap_nro_autorizacion => :P117_RAP_NRO_AUTORIZACION,',
'                                              pv_error                => :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request = ''cargar_tfp'' and :p117_tfp_id =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_tfp_id_retencion'')'))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>19978210752813185
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(20126611061186635791)
,p_process_sequence=>3
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'  pq_ven_pagos_cuota_retencion.pr_carga_variables(pn_com_id           => :P117_RAP_COM_ID,',
'                                                  pn_emp_id           => :f_emp_id,',
'                                                  pn_subtotal_con_iva => :P117_SUBTOTAL_IVA,',
'                                                  pn_subtotal_sin_iva => :P117_SUBTOTAL_SIN_IVA,',
'                                                  pn_subtotal         => :P117_SUBTOTAL,',
'                                                  pn_flete            => :P117_FLETE,',
'                                                  pn_iva              => :P117_IVA,',
'                                                  pn_int_diferido     => :P117_INT_DIFERIDO,',
'                                                  pn_iva_flete        => :P117_IVA_FLETE,',
'                                                  pn_iva_bienes       => :P117_IVA_BIENES,',
'                                                  pv_error            => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'carga_subtotal'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>20094357909916870865
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52229771054578106)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_age_id number;',
'  ln_zon_id number;',
'  lv_zona   varchar2(100);',
'',
'BEGIN',
'',
'  :P117_CLI_ID               := NULL;',
'  :P117_AGENTE_COBRADOR      := NULL;',
'  :p117_mcd_valor_movimiento := 0;',
'  --- Retencion',
'  :P117_RAP_NRO_RETENCION    := NULL;',
'  :P117_RAP_NRO_AUTORIZACION := NULL;',
'  :P117_RAP_FECHA            := NULL;',
'  :P117_RAP_FECHA_VALIDEZ    := NULL;',
'  :P117_RAP_NRO_ESTABL_RET   := NULL;',
'  :P117_RAP_NRO_PEMISION_RET := NULL;',
'  :P117_PRE_ID               := NULL;',
'  :P117_RAP_COM_ID           := NULL;',
'  :P117_INS_ID               := NULL;',
'  :P117_NOMBRE_INSTITUCION   := ''NO ASIGNANDO'';',
'',
'  pq_ven_listas2.pr_datos_clientes(:F_UGE_ID,',
'                                  :F_EMP_ID,',
'                                  :P117_IDENTIFICACION,',
'                                  :P117_NOMBRE,',
'                                  :P117_CLI_ID,',
'                                  :P117_CORREO,',
'                                  :P117_DIRECCION,',
'                                  :P117_TIPO_DIRECCION,',
'                                  :P117_TELEFONO,',
'                                  :P117_TIPO_TELEFONO,',
'                                  :P117_CLIENTE_EXISTE,',
'                                  :P117_DIR_ID,',
'                                  :P117_CLI_ID_REFERIDO,',
'                                  :P117_CLI_ID_NOMBRE_REFERIDO,',
'                                  :P117_PER_TIPO_IDENTIFICACION,',
'                                  :P0_ERROR);',
'',
'  -- Yguaman 2011/12/15. Institucion de Cliente',
'  BEGIN',
'    SELECT ains.ins_id, ape.per_razon_social',
'      INTO :P117_INS_ID, :P117_NOMBRE_INSTITUCION',
'      FROM asdm_clientes_instituciones acin,',
'           asdm_instituciones          ains,',
'           asdm_personas               ape',
'     WHERE acin.cli_id = :P117_CLI_ID',
'       AND acin.cin_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'       AND acin.ins_id = ains.ins_id',
'       AND ains.per_id = ape.per_id',
'       AND ains.ins_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'       AND ape.per_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'');',
'  ',
'  EXCEPTION',
'    WHEN NO_DATA_FOUND THEN',
'      :P117_INS_ID             := NULL;',
'      :P117_NOMBRE_INSTITUCION := ''NO ASIGNANDO'';',
'    ',
'  END;',
'',
'  pq_ven_pagos_cuota.pr_carga_saldos_cliente(:P117_CLI_ID,',
'                                             :F_EMP_ID,',
'                                             :P117_TOTAL_CHEQUES,',
'                                             :P0_ERROR);',
'',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_pago_cuota''));',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_mov_caja''));',
'  pq_inv_movimientos.pr_elimina_colecciones(''COLL_VALOR_RETENCION'');',
'',
'  pq_inv_movimientos.pr_elimina_colecciones(''COLL_DET_RETENCION'');',
'',
'  if :P6_CLI_ID is not null and',
'     :f_SEG_ID =',
'     pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_minoreo'') and',
'     :f_emp_id =',
'     pq_constantes.fn_retorna_constante(0, ''cn_emp_id_marcimex'') then',
'  ',
'    -- Yguaman 2011/11/23 18:16pm, agregado para grabar el cobrador cuando se va a generar la transaccion de pago de cuota',
'    -- Solo para minoreo y cuando es el pago de cuota -- Jandres.',
'  ',
'    /* -- Antes estaba esto, se reemplazo por otra funcion',
'    pq_car_cobranza.pr_cliente_cobrador(pn_cli_id =>:P117_CLI_ID,',
'                                        pn_emp_id => :F_EMP_ID,',
'                                        pn_age_id => ln_age_id ,',
'                                        pv_agente => :P117_AGENTE_COBRADOR,',
'                                        pn_zon_id => ln_zon_id ,',
'                                        pv_zona   => lv_zona ,',
'                                        pv_error  => :p0_error);*/',
'  ',
unistr('    ln_age_id := pq_car_cobranza.fn_retorna_ata_id_cobrador(pn_emp_id => :F_EMP_ID, -- c\00BF\00BFdigo empresa'),
'                                                            pn_cli_id => :P117_CLI_ID, -- codigo cliente',
'                                                            pn_uge_id => :F_UGE_ID); -- codigo unidad de gestion',
'  ',
'    BEGIN',
'      SELECT pe.nombre_completo',
'        INTO :P117_AGENTE_COBRADOR',
'        FROM asdm_agentes_tagentes ata,',
'             asdm_agentes          age,',
'             v_asdm_datos_clientes pe',
'       WHERE ata.ata_id = ln_age_id',
'         AND ata.age_id = age.age_id',
'         AND age.per_id = pe.per_id',
'         AND age.age_estado_registro =',
'             pq_constantes.fn_retorna_constante(NULL,',
'                                                ''cv_estado_reg_activo'');',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        NULL;',
'        /*raise_application_error(-20000,',
'                                ''Debe asignar Cobrador al Cliente para continuar.'' || SQLERRM);',
'    */',
'  END;',
'  end if;',
'',
'  :P117_COM_ID     := NULL;',
'  :P117_VALOR_PAGO := 0;',
'  :P117_CXC_ID     := null;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>19976619784813180
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52229953826578107)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_div_pendientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id  => :P117_CLI_ID,',
'                                     pn_emp_id  => :f_emp_id,',
'                                     pn_seg_id => :F_SEG_ID,',
'                                     pd_fecha_desde  => NULL,',
'                                     pd_fecha_hasta => NULL,  ',
'                                     pn_tfp_id      => :P117_TFP_ID,                                 ',
'                                     pv_error  => :P0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>19976802556813181
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52230954603578109)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_int_condonado'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_VEN_PAGOS_CUOTA.pr_actualiza_int_condonado(pn_emp_id => :F_EMP_ID,',
'                                       pv_selecc =>   :P117_CONDONAR_TODO,                                 ',
'                                       pv_error  =>   :P0_error,',
'                                       pn_tse_id =>   :F_SEG_ID,',
'                                       pn_tfp_id =>   :P117_TFP_ID);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'bien'
,p_internal_uid=>19977803333813183
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52231159678578109)
,p_process_sequence=>100
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota_retencion.pr_carga_retencion(:f_emp_id,''A'',:p117_dea_seccion, :p0_error); ',
'    :p117_mcd_valor_movimiento := 0;',
'    --- Retencion',
'    /*:P117_RAP_NRO_RETENCION   := NULL;',
'    :P117_RAP_NRO_AUTORIZACION := NULL;',
'    :P117_RAP_FECHA := NULL;',
'    :P117_RAP_FECHA_VALIDEZ := NULL;',
'    :P117_RAP_NRO_ESTABL_RET   := NULL; ',
'    :P117_RAP_NRO_PEMISION_RET := NULL;*/',
'',
'    :P117_PRE_ID               := NULL; ',
'    :P117_RAP_COM_ID := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''CARGAR'' '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>19978008408813183
,p_process_comment=>unistr('Se ejecuta con el submit creado en el screep del head de la p\00BF\00BFgina, este screep se ejecuta al dar click en el boton grabar, envia el url para imprimir la factura')
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52231961217578114)
,p_process_sequence=>110
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_retenciones_pagar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_pagos_cuota_retencion.pr_valida_retenciones_pagar(pn_cli_id => :P117_CLI_ID,',
'                                                           pn_emp_id => :F_EMP_ID,',
'                                                           pn_total_pagos  => :P117_VALOR_PAGO_COLECCION,',
'                                                           pv_error => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(52184667942577959)
,p_internal_uid=>19978809947813188
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52228771140578100)
,p_process_sequence=>120
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'-- LBERREZUETA 28-ENE-2024 Permite valor 0 si el %ret es 0',
'    LV_RPO_VALOR NUMBER;',
'BEGIN',
'',
'BEGIN',
'SELECT P.RPO_VALOR INTO LV_RPO_VALOR',
'FROM ASDM_E.CAR_RET_PORCENTAJES P',
'WHERE P.RPO_ID = :P117_TRE_ID;',
'EXCEPTION',
'WHEN NO_DATA_FOUND THEN',
'      raise_application_error(-20000, ''Error CAR_RET_PORCENTAJES no encuentra datos tre_id ''||:P117_TRE_ID);',
'',
'WHEN TOO_MANY_ROWS THEN',
unistr('     raise_application_error(-20000, ''Error ,CAR_RET_PORCENTAJES retorna m\00E1s de un dato tre_id ''||:P117_TRE_ID);'),
'',
'WHEN OTHERS THEN',
'',
'   raise_application_error(-20000, ''Error CAR_RET_PORCENTAJEStre_id ''||:P117_TRE_ID);',
'',
'END;',
'',
'IF :P117_BASE > 0 OR (:P117_BASE = 0 AND LV_RPO_VALOR = 0) THEN',
'pq_ven_pagos_cuota_retencion.pr_carga_val_retenciones(PN_EMP_ID               => :f_emp_id,',
'                         PN_UGE_ID               => :f_uge_id,',
'                         PN_TSE_ID               => :f_seg_id,',
'                         pn_com_id               => :P117_RAP_COM_ID,',
'                         pd_rap_fecha            => :P117_RAP_FECHA,',
'                         pd_rap_fecha_validez    => :P117_RAP_FECHA_VALIDEZ,',
'                         pn_rap_nro_retencion    => :P117_RAP_NRO_RETENCION,',
'                         pn_rap_nro_autorizacion => :P117_RAP_NRO_AUTORIZACION,',
'                         pn_RAP_NRO_ESTABL_RET   => :P117_RAP_NRO_ESTABL_RET, ',
'                         pn_RAP_NRO_PEMISION_RET => :P117_RAP_NRO_PEMISION_RET, ',
'                         pn_porcentaje           => :P117_tRE_ID, ',
'                         pn_base_ret             => :p117_base,',
'                         pv_ret_iva              => :p117_ret_iva,',
'                         pv_observacion          => :p117_observacion,',
'                         pv_sesion               => :p117_dea_seccion,',
'                         pn_rpo_id  =>    :P117_tRE_ID,                          ',
'                         PV_ERROR                => :p0_error);',
'END IF;',
'',
':P117_TRE_ID := NULL;',
':P117_BASE := 0;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>19975619870813174
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52229367538578105)
,p_process_sequence=>230
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of ASDM_DOC_ELEC_AUX'
,p_attribute_02=>'ASDM_DOC_ELEC_AUX'
,p_attribute_03=>'P117_DEA_ID'
,p_attribute_04=>'DEA_ID'
,p_attribute_11=>'I'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(40279383686123880)
,p_process_success_message=>'ARCHIVO CARGADO'
,p_internal_uid=>19976216268813179
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52230356200578108)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_coleccion_cuotas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--DELETE FROM asdm_doc_elec_aux where dea_seccion =V(''SESSION'');',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_VALOR_RETENCION'');',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_DET_RETENCION'');'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P117_CLI_ID is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>19977204930813182
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52230572673578108)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_col_div_pendientes'
,p_process_sql_clob=>'pq_inv_movimientos.pr_elimina_colecciones(''CO_DIV_PENDIENTES'');'
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P117_CLI_ID is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>19977421403813182
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52230778348578108)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_FECHAS_CORTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  pq_car_credito.pr_fechas_corte_pago_cuota(pn_emp_id => :f_emp_id,',
'                              pn_tse_id => :F_SEG_ID,',
'                              pd_fecha  => SYSDATE, ',
'                              pv_error  => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>19977627078813182
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52228970218578105)
,p_process_sequence=>130
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_ret'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_cuenta number;',
'  ln_com_id ven_comprobantes.com_id%TYPE;',
'begin',
'  select count(*)',
'    into ln_cuenta',
'    from apex_collections a',
'   where a.collection_name = ''COLL_VALOR_RETENCION''',
'     and a.seq_id = :P117_SEQ_ID_RET;',
'  if ln_cuenta > 0 then',
'    apex_collection.delete_member(p_collection_name => ''COLL_VALOR_RETENCION'',',
'                                  p_seq             => :P117_SEQ_ID_RET);',
'  end if;',
'',
'  begin',
'    select distinct (to_number(b.c001))',
'      into ln_Com_id',
'      from apex_collections b',
'     where b.collection_name = ''COLL_VALOR_RETENCION'';',
'  ',
'    :p117_observacion := pq_ven_pagos_cuota_retencion.fn_valida_ret(:f_emp_id,',
'                                                                    ln_com_id);',
'  ',
'  exception',
'    when no_data_found then',
'      null;',
'    when too_many_rows then',
'      raise_application_error(-20000,',
'                              ''NO PUEDE CARGAR VARIAS FACTURAS CON UNA SOLA RETENCION'');',
'  END;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>19975818948813179
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(53929771765074592)
,p_process_sequence=>140
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_VALOR_RETENCION'');',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_DET_RETENCION'');'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'limpia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>21676620495309666
);
wwv_flow_imp.component_end;
end;
/
