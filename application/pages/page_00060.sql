prompt --application/pages/page_00060
begin
--   Manifest
--     PAGE: 00060
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>60
,p_name=>'Cajas Disponibles'
,p_step_title=>'Cajas Disponibles'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(86840183136421334)
,p_plug_name=>'Periodo de Caja'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(87277880042874805)
,p_name=>'Cajas Disponibles a Cambiar'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.pue_id, d.pca_id, b.pue_nombre, a.uge_num_est_sri, b.pue_num_sri',
'  from asdm_unidades_gestion       a,',
'       asdm_puntos_emision         b,',
'       asdm_usuario_puntos_emision c,',
'       ven_periodos_caja           d',
' where c.pue_id = b.pue_id',
'   and b.uge_id = a.uge_id',
'   and b.emp_id = a.emp_id',
'   and d.emp_id = b.emp_id',
'   and d.pue_id = b.pue_id',
'   and d.pca_estado_registro = 0--pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and c.upe_estado_registro = 0--pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and b.pue_estado_registro = 0--pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and a.uge_estado_registro = 0--pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and d.pca_estado_pc = ''A''',
'--       pq_constantes.fn_retorna_constante(NULL, ''cv_pca_estado_pc_abierto'')',
'   and a.uge_id = :F_UGE_ID',
'   and c.usu_id = :f_user_id',
'   and a.emp_id = :f_emp_id',
'   and d.pca_token is null',
'   '))
,p_display_when_condition=>'CAMBIAR'
,p_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(87278268898874815)
,p_query_column_id=>1
,p_column_alias=>'PUE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Codigo Punto Emision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149443672149881776)
,p_query_column_id=>2
,p_column_alias=>'PCA_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Pca Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(87278159649874807)
,p_query_column_id=>3
,p_column_alias=>'PUE_NOMBRE'
,p_column_display_sequence=>1
,p_column_heading=>'Punto Emision'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:59:&SESSION.:CAMBIAR_CAJA:&DEBUG.:59:P59_PCA_ID,P59_PUE_ID,P0_PUE_ID:#PCA_ID#,#PUE_ID#,#PUE_ID#'
,p_column_linktext=>'#PUE_NOMBRE#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149431156296082455)
,p_query_column_id=>4
,p_column_alias=>'UGE_NUM_EST_SRI'
,p_column_display_sequence=>3
,p_column_heading=>'Uge Num Est Sri'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149431276422082455)
,p_query_column_id=>5
,p_column_alias=>'PUE_NUM_SRI'
,p_column_display_sequence=>4
,p_column_heading=>'Pue Num Sri'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(89742265565398179)
,p_name=>'Cajas Disponibles a Abrir'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.pue_id, b.pue_nombre, a.uge_num_est_sri, b.pue_num_sri',
'  from asdm_unidades_gestion       a,',
'       asdm_puntos_emision         b,',
'       asdm_usuario_puntos_emision c',
' where c.pue_id = b.pue_id',
'   and b.uge_id = a.uge_id',
'   and b.emp_id = a.emp_id',
'   and c.upe_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and b.pue_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and a.uge_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and a.uge_id = :F_UGE_ID',
'   and c.usu_id = :f_user_id',
'   and a.emp_id = :f_emp_id',
'minus',
'select b.pue_id, b.pue_nombre, a.uge_num_est_sri, b.pue_num_sri',
'  from asdm_unidades_gestion       a,',
'       asdm_puntos_emision         b,',
'       asdm_usuario_puntos_emision c,',
'       ven_periodos_caja           d',
' where c.pue_id = b.pue_id',
'   and b.uge_id = a.uge_id',
'   and b.emp_id = a.emp_id',
'   and d.emp_id = b.emp_id',
'   and d.pue_id = b.pue_id',
'   and d.pca_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and c.upe_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and b.pue_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and a.uge_estado_registro =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'   and d.pca_estado_pc =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_pca_estado_pc_abierto'')',
'   and a.uge_id = :F_UGE_ID',
'   and d.usu_id = :f_user_id',
'   and a.emp_id = :f_emp_id'))
,p_display_when_condition=>'ABRIR_CAJA'
,p_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(89742679603398287)
,p_query_column_id=>1
,p_column_alias=>'PUE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Codigo Punto Emision'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(89742578473398257)
,p_query_column_id=>2
,p_column_alias=>'PUE_NOMBRE'
,p_column_display_sequence=>1
,p_column_heading=>'Punto Emision'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:59:&SESSION.:ABRIR:&DEBUG.::P0_PUE_ID,P59_PUE_ID:#PUE_ID#,#PUE_ID#'
,p_column_linktext=>'#PUE_NOMBRE#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149428567892047965)
,p_query_column_id=>3
,p_column_alias=>'UGE_NUM_EST_SRI'
,p_column_display_sequence=>3
,p_column_heading=>'Uge Num Est Sri'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(149428652521047965)
,p_query_column_id=>4
,p_column_alias=>'PUE_NUM_SRI'
,p_column_display_sequence=>4
,p_column_heading=>'Pue Num Sri'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(87214078536590506)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(89742265565398179)
,p_button_name=>'CANCELAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:59:&SESSION.::&DEBUG.:59::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(87470566059578103)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(87277880042874805)
,p_button_name=>'CANCELAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:59:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(86840573532421350)
,p_name=>'P60_DATOS'
,p_item_sequence=>5
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Datos'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(86840779214421351)
,p_name=>'P60_PUE_ID'
,p_item_sequence=>10
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Pue Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(86840959798421524)
,p_name=>'P60_PCA_ID'
,p_item_sequence=>20
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Pca Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(89725053912328551)
,p_name=>'P60_UGE_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(89742265565398179)
,p_prompt=>'Uge Id'
,p_source=>':f_uge_id'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
