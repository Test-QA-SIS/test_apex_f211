prompt --application/pages/page_00123
begin
--   Manifest
--     PAGE: 00123
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>123
,p_tab_set=>'Menu'
,p_name=>'FR_CONSULTA_INDUCCION'
,p_step_title=>unistr('Consulta de Lecturas de N\00FAmeros de Serie')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(65463872236769860)
,p_plug_name=>unistr('Consulta de Series de Inducci\00F3n')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="font-size:14;font-family:Tahoma"'
,p_plug_template=>wwv_flow_imp.id(270526077972046669)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT    a.scl_id, a.sdl_nro_serie, a.sdl_cod_articulo, a.ite_sku_id, a.sdl_estado_registro, b.scl_fecha_registro, b.scl_cliente_destino, b.scl_bodega_despacho',
'  FROM      slm_detalle_lista a,',
'            slm_cabecera_lista b',
'  WHERE     a.sdl_nro_serie IS NOT NULL --= ''E000397653030550175''',
'  AND       b.scl_id = a.scl_id'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(65463955858769860)
,p_name=>unistr('Consulta de Series de Inducci\00F3n')
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:127:&SESSION.::&DEBUG.::P127_NRO_SERIE:#SDL_NRO_SERIE#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'IVEGA'
,p_internal_uid=>33210804589004934
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65464163195769872)
,p_db_column_name=>'SCL_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Lista'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SCL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65464270209769873)
,p_db_column_name=>'SDL_NRO_SERIE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Nro.Serie'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SDL_NRO_SERIE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65464360259769873)
,p_db_column_name=>'SDL_COD_ARTICULO'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cod.Barras'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SDL_COD_ARTICULO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65464478335769873)
,p_db_column_name=>'ITE_SKU_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Ite Sku Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ITE_SKU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65464556722769874)
,p_db_column_name=>'SDL_ESTADO_REGISTRO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Estado Lectura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SDL_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65563468109170172)
,p_db_column_name=>'SCL_FECHA_REGISTRO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Fecha Lista'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'SCL_FECHA_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65563562835170181)
,p_db_column_name=>'SCL_CLIENTE_DESTINO'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SCL_CLIENTE_DESTINO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(65563668299170181)
,p_db_column_name=>'SCL_BODEGA_DESPACHO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Bodega'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SCL_BODEGA_DESPACHO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(65467972695770179)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'332149'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'SCL_ID:SDL_NRO_SERIE:SDL_COD_ARTICULO:SDL_ESTADO_REGISTRO:SCL_FECHA_REGISTRO:SCL_CLIENTE_DESTINO:SCL_BODEGA_DESPACHO:'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(10716128667055631977)
,p_name=>'Base de Series'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    *',
'FROM      slm_series_jde',
'WHERE     (ssj_nro_serie = :P123_SERIE OR :P123_SERIE IS NULL)',
'or        (ssj_cod_barras = :P123_SERIE OR :P123_SERIE IS NULL)'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10716129152164632006)
,p_query_column_id=>1
,p_column_alias=>'SSJ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SSJ_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10716129256692632008)
,p_query_column_id=>2
,p_column_alias=>'SSJ_NRO_SERIE'
,p_column_display_sequence=>2
,p_column_heading=>'SERIE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10716129368430632008)
,p_query_column_id=>3
,p_column_alias=>'SSJ_COD_BARRAS'
,p_column_display_sequence=>3
,p_column_heading=>'CODIGO BARRAS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10716129465664632008)
,p_query_column_id=>4
,p_column_alias=>'SSJ_ESTADO_REGISTRO'
,p_column_display_sequence=>4
,p_column_heading=>'ESTADO'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10716129574165632008)
,p_query_column_id=>5
,p_column_alias=>'SSJ_FECHA'
,p_column_display_sequence=>5
,p_column_heading=>'FECHA CARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(10717029864725688061)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(10716128667055631977)
,p_button_name=>'BT_BUSCAR_SERIE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Buscar Serie'
,p_button_position=>'TOP'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(10716932269973682407)
,p_name=>'P123_SERIE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(10716128667055631977)
,p_prompt=>'Serie'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
