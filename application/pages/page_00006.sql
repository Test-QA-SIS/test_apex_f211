prompt --application/pages/page_00006
begin
--   Manifest
--     PAGE: 00006
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>6
,p_tab_set=>'Menu'
,p_name=>'Pagos Cuota'
,p_step_title=>'Pagos Cuota'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'',
'var numero_filas=500;',
'',
'//FUNCIONES PARA ACTUALIZAR REGIONES',
'',
'',
'function act_region()',
'{',
'$a_report(''R70272713295111230'',''1'',numero_filas,''15''); // REGION DIRECCIONES',
'} ',
'',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(102498882076875955)
,p_name=>'col'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT c001,',
'       c002,',
'       c003,',
'       c004,',
'       c005,',
'       c006,',
'       c007,',
'       c008,',
'       c009,',
'       c010,',
'       c011,',
'       c012,',
'       c013,',
'       c014,',
'       c015,',
'       c016,',
'       c017,',
'       c018,',
'       c019,',
'       c020,',
'       c021,',
'       c022,',
'       c023,',
'       pq_car_manejo_mora.fn_calcula_int_mora(pn_ede_id          => NULL,',
'                                              pn_emp_id          => c006,',
'                                              pn_div_id          => c005,',
'                                              pn_valor_condonado => nvl(c022,',
'                                                                        0)) intmora,',
'       pq_car_manejo_mora.fn_calcula_gasto_cobranza(NULL,',
'                                                    c006,',
'                                                    c005,',
'                                                    nvl(c023,0),',
'                                                    :p6_f_seg_id) gtocob',
'',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'      --''CO_DIV_PENDIENTES''',
'   AND c001 = :p6_cli_id}'';'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>25
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(102498882076875955)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499073513876026)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>2
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499169659876053)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>1
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499258875876053)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499381147876053)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499479554876053)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499553515876053)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499672661876053)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499756867876053)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499852826876053)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102499953136876053)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500071444876053)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500162964876053)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500253956876053)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500376945876053)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500468804876053)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500559393876054)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500670876876054)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500775200876054)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500871047876054)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102500957618876054)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102501069163876054)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102501179158876054)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102501282143876054)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102501364986876054)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102501477277876054)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(102501558959876055)
,p_name=>'cant'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT count(c004)',
'         ',
'          FROM apex_collections',
'         WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_pago_cuota'')',
'           AND c002 = :p6_com_id'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(102501558959876055)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102501777496876062)
,p_query_column_id=>1
,p_column_alias=>'COUNT(C004)'
,p_column_display_sequence=>1
,p_column_heading=>'Count(C004)'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(102501881141876062)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_pagos_cuota_retencion.fn_mostrar_div_pendientes(:P0_error);',
'',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(102501881141876062)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502080357876072)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502156264876072)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502256121876072)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502355333876073)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502469742876073)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502582712876073)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502667788876073)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502757550876073)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502854661876073)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102502978091876073)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505764125876078)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505852171876078)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505977807876078)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506069370876078)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506176437876078)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506281563876078)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506380305876078)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506474102876078)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506573267876078)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506655901876078)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506780062876078)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506881195876078)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102506973746876078)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507070912876078)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507163457876078)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507259575876078)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507383833876078)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507476491876079)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507564723876079)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507657386876079)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507768566876079)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507879268876079)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102507966299876079)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503062572876073)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503180815876073)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503255558876073)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503374661876073)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503481966876073)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503555267876073)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503682116876073)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503775062876073)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503881939876073)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102503981175876074)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504082684876074)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504152410876074)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504270925876074)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504352493876077)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504464764876077)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504571539876077)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504682511876077)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504754354876077)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504878352876077)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102504955242876077)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505075994876077)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505183515876077)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505265197876077)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505379202876077)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505470980876077)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505581958876077)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102505652193876078)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(102508083443876079)
,p_name=>'borrar despues de probar'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>18
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_pagos_cuota.fn_select_pagos_div_cuota(:P6_CXC_ID,:p0_error);'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT count(c.cxc_id)',
'  FROM v_car_cartera_clientes c',
' WHERE c.emp_id = :f_emp_id',
'   AND c.cli_id = :p6_cli_id )> 0'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>6
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(102508083443876079)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508252631876079)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508368250876079)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508465898876079)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511960722876123)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512054127876123)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512154449876123)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512255653876123)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512360082876123)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512480894876123)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512559223876123)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512671456876123)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512778372876123)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512864757876123)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102512982690876127)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513056723876128)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513173299876128)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513279644876128)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513355950876128)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513464845876128)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513561974876128)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513671792876128)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513771259876128)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513866345876128)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102513955638876128)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102514080799876128)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102514157598876128)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509958294876102)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510066431876102)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510164849876122)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510263580876122)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510365060876122)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510474199876122)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510568580876122)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510672630876122)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510771132876122)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510853339876122)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102510975278876122)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511080532876122)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511165141876122)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511263566876122)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511355390876122)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511468208876122)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511578867876122)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511674618876122)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511781231876122)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102511865988876123)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508575784876080)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508653326876080)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508768026876080)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508855319876080)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102508975098876080)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509057861876080)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509152240876080)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509265623876080)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509381784876080)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509476766876089)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509556315876089)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509656480876089)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509774882876102)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102509859375876102)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102514281895876128)
,p_plug_name=>'parametros'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_2'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':f_emp_id is null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102524263207876145)
,p_plug_name=>'_'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>65
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P6_CLI_ID is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(6594670274451384240)
,p_name=>'CREDITOS QUE APLICAN PROMOCIONES'
,p_parent_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>160
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c001 credito,',
'       c002 factura,',
'       c003 valor_puntualito,',
'       c004 promocion,',
'       CASE c005',
'         WHEN ''X'' THEN',
'          ''APLICAR''',
'         ELSE',
'          NULL',
'       END aplicar',
'  FROM apex_collections',
' WHERE collection_name = ''COL_CXC_APLICAN''',
''))
,p_display_when_condition=>':F_SEG_ID = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'')'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_footer=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<font color="RED">',
'CUOTAS GRATIS<br/>',
'* Promocion disponible al cancelar la cuota indicada<br/>',
'* Promocion aplica para cuotas completas</font>',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'no data found'
,p_query_row_count_max=>500
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(6594670573520384241)
,p_query_column_id=>1
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>1
,p_column_heading=>'<SPAN STYLE="font-size: 13pt">Credito</span>'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(6594670674228384241)
,p_query_column_id=>2
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>2
,p_column_heading=>'<SPAN STYLE="font-size: 13pt">Factura</span>'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(6594670760634384241)
,p_query_column_id=>3
,p_column_alias=>'VALOR_PUNTUALITO'
,p_column_display_sequence=>3
,p_column_heading=>'<SPAN STYLE="font-size: 13pt">Valor Puntualito</span>'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7637138801290837271)
,p_query_column_id=>4
,p_column_alias=>'PROMOCION'
,p_column_display_sequence=>4
,p_column_heading=>'<SPAN STYLE="font-size: 13pt">Promocion</span>'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7637138941812837272)
,p_query_column_id=>5
,p_column_alias=>'APLICAR'
,p_column_display_sequence=>5
,p_column_heading=>'<SPAN STYLE="font-size: 13pt">Aplicar</span>'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:131:&SESSION.:NUEVO:&DEBUG.:RP:P131_CXC_ID,P131_COM_ID,P131_PAG:#CREDITO#,#FACTURA#,6'
,p_column_linktext=>'#APLICAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102525864564876156)
,p_plug_name=>'CARTERA DE CLIENTE - &P6_NOMBRE.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Select',
'      seq_id,',
'      to_number(c001) cli_id,',
'      c002 per_id,',
'      to_number(c003) cxc_id,',
'      c004 div_id,',
'      to_number(c005) nro_vencimiento,',
'      to_date(c006) fecha_vencimiento,',
'(case when c007 < ''0'' then',
'             ''0'' else c007 end ) as dias_mora,      ',
'      c008 valor_cuota,                                     ',
'      c009 respaldo_cheques,',
'      to_number(c010) saldo, ',
'      c011 estado,',
'apex_item.checkbox(10,seq_id,p_attributes => ',
'',
'''onclick="pr_carga_cuotas_con_valor(''''PR_CUOTAS_VALOR'''',''''P6_F_EMP_ID'''',''''P6_TFP_ID'''',this.value,''''P6_F_SEG_ID'''',''''P6_VALOR_PAGO'''',''''R70272713295111230'''',''''R70263701157111210'''');',
'pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''TP'''',''''P6_VALOR_PAGO_COLECCION'''');pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''IC'''',''''P6_TOTAL_INT_COND'''');',
'pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''GC'''',''''P6_TOTAL_GST_COND'''');pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''TC'''',''''P6_TOTAL_CONDONAR'''')"'') pagar,',
'      --c012 pagar,',
'      c013 fac_mxpro,',
'      c014 com_id_factura,',
'      c015 comp,',
'      c016 factura_migrada,',
'      c017 ttr_id,',
'      c018 transaccion,',
'      c019 comprobante,',
'      c020 ede_dividendo,                                     ',
'      c021 vendedor,',
'      c022 segmento,',
'      c023 selecc,',
'      c024 Rol, -- yguaman 2011/12/15',
'      c025 Agencia_Creacion,',
'       trunc(SYSDATE) - to_date(c006) Dias_Vencidos , -- yguaman 2012/02/28 ',
'       ',
'       (SELECT co.pue_num_sri || ''-''||co.uge_num_sri||''-''||co.com_numero',
'       FROM ven_comprobantes co',
'       WHERE co.com_id = to_number(c014)) folio',
'      from apex_collections       ',
'      WHERE collection_name = ''CO_DIV_PENDIENTES''',
'',
'       --- Rango de Fechas a consultar',
'        AND (trunc(c006) >= trunc(:P6_FECHA_DESDE) OR',
'             :P6_FECHA_DESDE IS NULL)',
'         AND (to_date(c006,''dd-mm-yyyy'') <= to_date(:P6_FECHA_HASTA_CORTE,''dd-mm-yyyy'') OR',
'             :P6_FECHA_HASTA_CORTE IS NULL)',
'',
'      --AND c023 = ''N''',
'--AND C020 != 1056     ',
' AND c004 not in',
'     ',
'       (SELECT col.c005 div_id',
'          FROM apex_collections col',
'         WHERE col.collection_name =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_coleccion_pago_cuota''))',
'      ORDER BY to_number(c001), to_number(c003), to_number(c005);',
'',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P6_CLI_ID is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(102526060597876162)
,p_name=>'Listado Facturas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y_OF_Z'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>70272909328111236
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102528063170876185)
,p_db_column_name=>'ESTADO'
,p_display_order=>7
,p_column_identifier=>'L'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ESTADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102528156942876185)
,p_db_column_name=>'PAGAR'
,p_display_order=>8
,p_column_identifier=>'M'
,p_column_label=>'Pagar'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_tz_dependent=>'N'
,p_static_id=>'PAGAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102528361498876186)
,p_db_column_name=>'COMPROBANTE'
,p_display_order=>17
,p_column_identifier=>'Y'
,p_column_label=>'Comp.'
,p_column_link=>'f?p=211:52:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_POPUP,P52_COM_ID,P52_VALIDA_CONS_CARTERA:f?p=201,&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOG'
||'O.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,N,#COM_ID_FACTURA#,2'
,p_column_linktext=>'#COMPROBANTE#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'COMPROBANTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102528481531876187)
,p_db_column_name=>'TRANSACCION'
,p_display_order=>18
,p_column_identifier=>'Z'
,p_column_label=>'Trans.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TRANSACCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102526156003876177)
,p_db_column_name=>'SEGMENTO'
,p_display_order=>19
,p_column_identifier=>'AA'
,p_column_label=>'Segmento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SEGMENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102526273708876182)
,p_db_column_name=>'EDE_DIVIDENDO'
,p_display_order=>21
,p_column_identifier=>'AC'
,p_column_label=>'Tipo Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_DIVIDENDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102526363983876182)
,p_db_column_name=>'VENDEDOR'
,p_display_order=>22
,p_column_identifier=>'AD'
,p_column_label=>'Vendedor'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VENDEDOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102526581785876182)
,p_db_column_name=>'PER_ID'
,p_display_order=>24
,p_column_identifier=>'AF'
,p_column_label=>'Per Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102526781290876183)
,p_db_column_name=>'DIV_ID'
,p_display_order=>26
,p_column_identifier=>'AH'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527059451876183)
,p_db_column_name=>'DIAS_MORA'
,p_display_order=>29
,p_column_identifier=>'AK'
,p_column_label=>'Dias Mora'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DIAS_MORA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527171334876183)
,p_db_column_name=>'VALOR_CUOTA'
,p_display_order=>30
,p_column_identifier=>'AL'
,p_column_label=>'Valor Cuota'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527262720876183)
,p_db_column_name=>'RESPALDO_CHEQUES'
,p_display_order=>31
,p_column_identifier=>'AM'
,p_column_label=>'Respaldo Cheq.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RESPALDO_CHEQUES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527451319876184)
,p_db_column_name=>'FAC_MXPRO'
,p_display_order=>33
,p_column_identifier=>'AO'
,p_column_label=>'Fac Mxpro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FAC_MXPRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527577157876184)
,p_db_column_name=>'COM_ID_FACTURA'
,p_display_order=>34
,p_column_identifier=>'AP'
,p_column_label=>'Com Id Factura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527663756876184)
,p_db_column_name=>'COMP'
,p_display_order=>35
,p_column_identifier=>'AQ'
,p_column_label=>'Comp'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COMP'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527774815876184)
,p_db_column_name=>'TTR_ID'
,p_display_order=>36
,p_column_identifier=>'AR'
,p_column_label=>'Ttr Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527862873876184)
,p_db_column_name=>'SELECC'
,p_display_order=>37
,p_column_identifier=>'AS'
,p_column_label=>'Selecc'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
,p_static_id=>'SELECC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(102527960357876185)
,p_db_column_name=>'SEQ_ID'
,p_display_order=>38
,p_column_identifier=>'AT'
,p_column_label=>'Seq Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(121336681214870797)
,p_db_column_name=>'ROL'
,p_display_order=>39
,p_column_identifier=>'AU'
,p_column_label=>'Rol'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ROL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(132944172164655243)
,p_db_column_name=>'SALDO'
,p_display_order=>40
,p_column_identifier=>'AV'
,p_column_label=>'Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(143207763415878604)
,p_db_column_name=>'AGENCIA_CREACION'
,p_display_order=>41
,p_column_identifier=>'AW'
,p_column_label=>'Agencia Creacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA_CREACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(78104063848309645)
,p_db_column_name=>'FACTURA_MIGRADA'
,p_display_order=>42
,p_column_identifier=>'AX'
,p_column_label=>'Factura Migrada'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA_MIGRADA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112863875614732293)
,p_db_column_name=>'FECHA_VENCIMIENTO'
,p_display_order=>43
,p_column_identifier=>'AY'
,p_column_label=>'Fecha Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(172414654527987056)
,p_db_column_name=>'CLI_ID'
,p_display_order=>44
,p_column_identifier=>'AZ'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(172414782564987065)
,p_db_column_name=>'CXC_ID'
,p_display_order=>45
,p_column_identifier=>'BA'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(172414861119987065)
,p_db_column_name=>'NRO_VENCIMIENTO'
,p_display_order=>46
,p_column_identifier=>'BB'
,p_column_label=>'Nro Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(2200153256836970803)
,p_db_column_name=>'DIAS_VENCIDOS'
,p_display_order=>47
,p_column_identifier=>'BC'
,p_column_label=>'Dias Vencidos'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIAS_VENCIDOS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(3033223375894631968)
,p_db_column_name=>'FOLIO'
,p_display_order=>48
,p_column_identifier=>'BD'
,p_column_label=>'Folio'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(102528551620876187)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'702755'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>5
,p_report_columns=>'AGENCIA_CREACION:COMPROBANTE:FACTURA_MIGRADA:CXC_ID:DIV_ID:NRO_VENCIMIENTO:FECHA_VENCIMIENTO:VALOR_CUOTA:RESPALDO_CHEQUES:SALDO:PAGAR:TRANSACCION:EDE_DIVIDENDO:VENDEDOR:SEGMENTO:ROL:FOLIO:'
,p_sort_column_1=>'NRO_VENCIMIENTO'
,p_sort_direction_1=>'ASC'
,p_sort_column_2=>'DIV_ID'
,p_sort_direction_2=>'DESC'
,p_sort_column_3=>'CLI_ID'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'CXC_ID'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
,p_break_on=>'CXC_ID:COMPROBANTE:TRANSACCION:AGENCIA_CREACION:VENDEDOR:FOLIO'
,p_break_enabled_on=>'CXC_ID:COMPROBANTE:TRANSACCION:AGENCIA_CREACION:VENDEDOR:FOLIO'
,p_sum_columns_on_break=>'SALDO'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(3033286366253635954)
,p_report_id=>wwv_flow_imp.id(102528551620876187)
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'DIAS_VENCIDOS'
,p_operator=>'>'
,p_expr=>'0'
,p_condition_sql=>' (case when ("DIAS_VENCIDOS" > to_number(#APXWS_EXPR#)) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# > #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_font_color=>'#FF7755'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(102516852426876136)
,p_name=>'CUOTAS SELECCIONADAS PARA PAGAR'
,p_parent_plug_id=>wwv_flow_imp.id(102525864564876156)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_pagos_cuota.fn_apex_item_col_pago_cuota(pv_error => :p0_error,',
'                                                      pn_emp_id => :f_emp_id,',
'                                                      pn_ttr_id_origen => pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota''));',
'',
'',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(102516852426876136)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520882800876141)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>11
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520966489876141)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>20
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521080077876141)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>1
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521166989876141)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>2
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521269121876141)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>3
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521359591876141)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>4
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521466926876141)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521564647876141)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>6
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521663117876141)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521751686876141)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>5
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521868918876141)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>9
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102521978694876141)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>10
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522066346876141)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522964410876142)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>15
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522163940876142)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>16
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522281139876142)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>17
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522379680876142)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>18
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522470255876142)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>14
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522572634876142)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522677983876142)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>21
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522759924876142)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>12
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102522881301876142)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517176527876139)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517282578876139)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517376461876139)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517483752876139)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517582382876139)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517669590876139)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517769708876139)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517859022876139)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517953817876139)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518061980876139)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518160271876139)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518268901876139)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518354772876139)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518460296876139)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518580977876139)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518683605876140)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518783796876140)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518876806876140)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102518978573876140)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519070866876140)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519175820876140)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519259428876140)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519378475876140)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519454301876140)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519564765876140)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519682224876140)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519759274876140)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102517057564876139)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519858936876140)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102519967673876140)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520064147876140)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520156175876140)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520265768876140)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520353912876141)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520463596876141)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520578638876141)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520680817876141)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(102520751647876141)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102524051882876144)
,p_plug_name=>'Condonar'
,p_parent_plug_id=>wwv_flow_imp.id(102516852426876136)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(102528983338876197)
,p_plug_name=>'<SPAN STYLE="font-size: 12pt">PAGO DE CUOTA GENERAL</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT count(*)',
'  FROM asdm_unidades_gestion a, asdm_agencias b',
' WHERE b.uge_id = a.uge_id',
'   AND b.tse_id = 2',
'   and a.uge_estado_registro = 0',
'   and b.age_estado_registro = 0',
'   and a.emp_id = 1',
'   and a.uge_id = :f_uge_id) = 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(273809882941600303)
,p_name=>'Creditos Negociados'
,p_parent_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (CASE',
'         WHEN COUNT(*) > 0 AND ne.ede_id = 1056 THEN',
'           ',
'          ''El cliente tiene el credito: '' || cx.cxc_id || '' Negociado con '' ||',
'          ed.ede_descripcion ||',
'          '', los cobros en caja se podran realizar hasta '' ||',
'          (ne.neg_fecha_recepcion +',
'          pq_car_negociaciones.fn_ret_valor_parametro_ede(pn_ede_id => ne.ede_id,',
'                                                           pn_par_id => pq_constantes.fn_retorna_constante(0,',
'                                                                                                           ''cn_par_id_num_dias_cobro_neg_mor'')))||',
unistr('        '', los tel\00E9fonos de contacto en MASOLUC son:07 2822369, 0994190443, 0994190678.'''),
'        ',
'        WHEN COUNT(*) > 0 AND ne.ede_id <> 1056 THEN',
'        ',
'            ''El cliente tiene el credito: '' || cx.cxc_id || '' Negociado con '' ||',
'          ed.ede_descripcion ||',
'          '', los cobros en caja se podran realizar hasta '' ||',
'          (ne.neg_fecha_recepcion +',
'          pq_car_negociaciones.fn_ret_valor_parametro_ede(pn_ede_id => ne.ede_id,',
'                                                           pn_par_id => pq_constantes.fn_retorna_constante(0,',
'                                                                                                           ''cn_par_id_num_dias_cobro_neg_mor'')))',
'         ELSE',
'          NULL',
'       ',
'       END) ALERTA',
'  FROM car_cuentas_por_cobrar  cx,',
'       CAR_CXC_NEGOCIACIONES   CN,',
'       CAR_NEGOCIACIONES       NE,',
'       asdm_entidades_destinos ed',
' WHERE EXISTS',
'       (SELECT ''x''',
'          FROM asdm_E.Car_Tipo_Negociacion_Entidad e',
'         WHERE ctn_id =',
'               pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'           AND e.cte_estado_registro = 0',
'           AND e.ede_id = cx.ede_id)',
'   AND cx.cxc_saldo > 0',
'   AND cx.cxc_estado = ''GEN''',
'   AND CX.CXC_ESTADO_REGISTRO = 0',
'   AND cne_estado_cxc = ''NEGOCIADO''',
'   AND CLI_ID = :P6_CLI_ID',
'   AND CX.CXC_ID = CN.CNE_CXC_ID',
'   AND CN.NEG_ID = NE.NEG_ID',
'   AND cx.ede_id = ed.ede_id',
' GROUP BY cx.cxc_id, ne.neg_fecha_recepcion, ed.ede_descripcion, ne.ede_id'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P6_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(273810251500600377)
,p_query_column_id=>1
,p_column_alias=>'ALERTA'
,p_column_display_sequence=>1
,p_column_heading=>'ALERTA'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<font color=RED>#ALERTA#</font>'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(6594658059513351516)
,p_name=>'<SPAN STYLE="font-size: 14pt">Cupo</span>'
,p_parent_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>150
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT-- cu.cup_fecha_inicio fecha_asignacion,',
'       -- cu.cup_cupo_inicial,',
'     --  cu.cup_cupo_asignado,',
'       pq_car_manejo_cupos.fn_retorna_cupo_utilizado(pn_cli_id => cu.cli_id,',
'                                                     pn_tse_id => 2) cupo_utilizado,',
'       cu.cup_cupo_disponible/*,',
'       ',
'       (CASE',
'         WHEN cu.cup_estado_registro = ''0'' THEN',
'          ''Activo''',
'         WHEN cu.cup_estado_registro = ''9'' THEN',
'          ''Inactivo''',
'       END',
'       ',
'       ) estado,',
'       ',
'       (SELECT ag.age_nombre_comercial',
'          FROM asdm_agencias ag',
'         WHERE ag.uge_id = cu.uge_id) agencia_origen*/',
'',
'  FROM car_cupos_clientes cu',
' WHERE cu.cli_id = :p6_cli_id',
'   AND cu.cup_tipo_cupo = ''M''',
'   AND cu.cup_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')'))
,p_display_when_condition=>':F_SEG_ID = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'')'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(6594658364092351540)
,p_query_column_id=>1
,p_column_alias=>'CUPO_UTILIZADO'
,p_column_display_sequence=>1
,p_column_heading=>'Utilizado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(6594658459850351542)
,p_query_column_id=>2
,p_column_alias=>'CUP_CUPO_DISPONIBLE'
,p_column_display_sequence=>2
,p_column_heading=>'Disponible'
,p_use_as_row_header=>'N'
,p_column_html_expression=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<font style="background:#5FA827;font-family:Times New Roman;font-size:300%; color:#FFF6CF"> #CUP_CUPO_DISPONIBLE# </font>',
'',
''))
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7470260152518956456)
,p_name=>'<SPAN STYLE="font-size: 17pt;   ">PERFIL DE CREDITO - &P6_NOMBRE_SEGMENTO. </span>'
,p_parent_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select csc_id,',
'       csc_descripcion,',
'       csc_color,',
'       csc_nombre_comercial,',
'       emp_id,',
'       csc_estado_registro,',
'       dbms_lob.getlength("CSC_IMAGEN") csc_imagen',
'',
'  FROM ASDM_e.Car_Segmentacion_Credito',
' WHERE CSC_ID = :p6_csc_id'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260303864956457)
,p_query_column_id=>1
,p_column_alias=>'CSC_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260414387956458)
,p_query_column_id=>2
,p_column_alias=>'CSC_DESCRIPCION'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260493819956459)
,p_query_column_id=>3
,p_column_alias=>'CSC_COLOR'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260622809956460)
,p_query_column_id=>4
,p_column_alias=>'CSC_NOMBRE_COMERCIAL'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260724122956461)
,p_query_column_id=>5
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260760689956462)
,p_query_column_id=>6
,p_column_alias=>'CSC_ESTADO_REGISTRO'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7470260911563956463)
,p_query_column_id=>7
,p_column_alias=>'CSC_IMAGEN'
,p_column_display_sequence=>7
,p_column_heading=>'    '
,p_use_as_row_header=>'N'
,p_column_format=>'IMAGE:CAR_SEGMENTACION_CREDITO:CSC_IMAGEN:CSC_ID::::::::'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(15337641240621025353)
,p_name=>'<SPAN STYLE="font-size: 16pt; color:RED;">ACUERDO DE CONDONACION DE CAPITAL</span>'
,p_parent_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' ',
'select ccc_id,',
'       ccc_saldo,',
'       ccc_fecha,',
'       ccc_observacion,',
'       uge_id,',
'       (select uge.uge_nombre',
'       from asdm_unidades_gestion uge',
'       where uge.uge_id = cc.uge_id) uge_nombre,',
'       ccc_modo,',
'       ccc_dias,',
'       ccc_creditos',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p6_cli_id',
'   and cc.ccc_estado_registro  = 0;'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p6_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641284516025354)
,p_query_column_id=>1
,p_column_alias=>'CCC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ccc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641416526025355)
,p_query_column_id=>2
,p_column_alias=>'CCC_SALDO'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641494194025356)
,p_query_column_id=>3
,p_column_alias=>'CCC_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha_Condonacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641593053025357)
,p_query_column_id=>4
,p_column_alias=>'CCC_OBSERVACION'
,p_column_display_sequence=>4
,p_column_heading=>'Observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641715660025358)
,p_query_column_id=>5
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Uge id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641818440025359)
,p_query_column_id=>6
,p_column_alias=>'UGE_NOMBRE'
,p_column_display_sequence=>6
,p_column_heading=>'Agencia'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337641914842025360)
,p_query_column_id=>7
,p_column_alias=>'CCC_MODO'
,p_column_display_sequence=>7
,p_column_heading=>'Modo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337642036947025361)
,p_query_column_id=>8
,p_column_alias=>'CCC_DIAS'
,p_column_display_sequence=>8
,p_column_heading=>'Cada/Dias'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(15337642113551025362)
,p_query_column_id=>9
,p_column_alias=>'CCC_CREDITOS'
,p_column_display_sequence=>9
,p_column_heading=>'Nro_creditos'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(27316767679994484545)
,p_name=>'<SPAN STYLE="font-size: 13pt">Detalle Creditos Condonacion</span>'
,p_parent_plug_id=>wwv_flow_imp.id(15337641240621025353)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select de.cxc_id',
'  from asdm_e.car_condonacion_detalle de, asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p6_cli_id',
'   and cc.ccc_id = de.ccc_id',
'   and cc.ccc_estado_registro = 0',
'   and cc.ccc_estado_registro = de.ccd_estado_registro',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316767807030484546)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(27316767936386484547)
,p_name=>'<SPAN STYLE="font-size: 13pt">Detalle Acuerdo de Condonacion</span>'
,p_parent_plug_id=>wwv_flow_imp.id(15337641240621025353)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' ',
' select cc.ccc_id,',
'        cu.ccu_nro_cuota,',
'        cu.ccu_valor_cuota,',
'        cu.ccu_saldo_cuota,',
'        cu.ccu_fecha_vencimiento,',
'        cu.ccu_fecha_ult_mov',
'   from asdm_E.Car_Condonacion_Cab cc, asdm_E.Car_Condonacion_Cuotas cu',
'  where cc.cli_id = :p6_cli_id',
'    and cc.ccc_id = cu.ccc_id',
'    and cc.ccc_estado_registro = 0',
'  order by ccu_nro_cuota;'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316767956571484548)
,p_query_column_id=>1
,p_column_alias=>'CCC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ccc id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316768097567484549)
,p_query_column_id=>2
,p_column_alias=>'CCU_NRO_CUOTA'
,p_column_display_sequence=>2
,p_column_heading=>'Nro cuota'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316768204022484550)
,p_query_column_id=>3
,p_column_alias=>'CCU_VALOR_CUOTA'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316768308892484551)
,p_query_column_id=>4
,p_column_alias=>'CCU_SALDO_CUOTA'
,p_column_display_sequence=>4
,p_column_heading=>'Saldo cuota'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316768355411484552)
,p_query_column_id=>5
,p_column_alias=>'CCU_FECHA_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(27316768468902484553)
,p_query_column_id=>6
,p_column_alias=>'CCU_FECHA_ULT_MOV'
,p_column_display_sequence=>6
,p_column_heading=>'Fecha Ult Mov'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(10165452173699463573)
,p_name=>'<SPAN STYLE="font-size: 16pt; color:RED;">ACUERDOS DE PAGO</span>'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>11
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apc.acp_id, apc.cli_id, apc.age_id, apc.acp_fecha_acuerdo,',
'       apc.acp_observacion, apc.uge_id, apc.acp_modo, apc.acp_total, ',
'       apc.acp_creditos, apc.acp_valor cuotas, apc.acp_valor_1 couta_inicial,',
'       apc.acp_dias cada,',
'       (CASE ',
'         WHEN apc.acp_tipo = 1 THEN',
'           ''DIAS''',
'         ELSE ',
'           ''DEL MES''',
'        END) Tipo,',
'        rownum nro_cuota,          ',
'        apd.acd_fecha fechas_pago     ',
'FROM car_acuerdos_pago_cab apc,',
'     car_acuerdos_pago_det apd',
'WHERE apc.acp_id = apd.acp_id',
'AND   apc.acp_estado_registro =0',
'AND   apd.acd_estado_registro = 0 ',
'AND   apc.acp_id = :P6_ACUERDO_ID',
'ORDER BY apd.acd_fecha ASC'))
,p_display_when_condition=>':P6_ACUERDO_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453082925463751)
,p_query_column_id=>1
,p_column_alias=>'ACP_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453180023463755)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453276851463755)
,p_query_column_id=>3
,p_column_alias=>'AGE_ID'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453369612463755)
,p_query_column_id=>4
,p_column_alias=>'ACP_FECHA_ACUERDO'
,p_column_display_sequence=>4
,p_column_heading=>'ACP_FECHA_ACUERDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453469560463755)
,p_query_column_id=>5
,p_column_alias=>'ACP_OBSERVACION'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453565212463755)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453651321463756)
,p_query_column_id=>7
,p_column_alias=>'ACP_MODO'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453781362463756)
,p_query_column_id=>8
,p_column_alias=>'ACP_TOTAL'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453875647463756)
,p_query_column_id=>9
,p_column_alias=>'ACP_CREDITOS'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165453979467463756)
,p_query_column_id=>10
,p_column_alias=>'CUOTAS'
,p_column_display_sequence=>10
,p_column_heading=>'CUOTAS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165454082029463756)
,p_query_column_id=>11
,p_column_alias=>'COUTA_INICIAL'
,p_column_display_sequence=>11
,p_column_heading=>'COUTA_INICIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165454172142463756)
,p_query_column_id=>12
,p_column_alias=>'CADA'
,p_column_display_sequence=>12
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165454256541463756)
,p_query_column_id=>13
,p_column_alias=>'TIPO'
,p_column_display_sequence=>13
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165454354990463756)
,p_query_column_id=>14
,p_column_alias=>'NRO_CUOTA'
,p_column_display_sequence=>14
,p_column_heading=>'NRO_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165454471640463756)
,p_query_column_id=>15
,p_column_alias=>'FECHAS_PAGO'
,p_column_display_sequence=>15
,p_column_heading=>'FECHAS_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(10165575480410474993)
,p_name=>'DETALLE ACUERDO - ITEMS PARA LA DEVOLUCION'
,p_parent_plug_id=>wwv_flow_imp.id(10165452173699463573)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>110
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT apa.apa_id, apa.com_id factura, apa.cxc_id, aid.cantidad,',
'       aid.ite_sku_id item_id, aid.item  ',
'FROM car_acuerdo_pago_cxc apa,',
'     car_acuerdo_item_dev aid',
'WHERE apa.apa_id = aid.apa_id',
'AND   apa.apa_estado_registro = 0',
'AND   aid.aid_estado_registro = 0',
'AND   apa.acp_id = :P6_ACUERDO_ID'))
,p_display_when_condition=>':P6_ACUERDO_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165575869377475000)
,p_query_column_id=>1
,p_column_alias=>'APA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'APA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165575972109475002)
,p_query_column_id=>2
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>2
,p_column_heading=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165576083688475002)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>3
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165576151681475003)
,p_query_column_id=>4
,p_column_alias=>'CANTIDAD'
,p_column_display_sequence=>4
,p_column_heading=>'CANTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165576272966475003)
,p_query_column_id=>5
,p_column_alias=>'ITEM_ID'
,p_column_display_sequence=>5
,p_column_heading=>'ITEM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10165576382246475003)
,p_query_column_id=>6
,p_column_alias=>'ITEM'
,p_column_display_sequence=>6
,p_column_heading=>'ITEM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(34498909345574052239)
,p_name=>'ORDENES CUOTA COMODIN'
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.occ_id,',
'       p.per_tipo_identificacion,',
'       p.per_nro_identificacion,',
'       trim(p.per_primer_apellido||'' ''||p.per_segundo_apellido||'' ''||p.per_primer_nombre||'' ''||p.per_segundo_nombre||'' ''||p.per_razon_social) nombres,',
'       a.cxc_id,',
'       a.div_nro_vencimiento,',
'       kseg_p.pq_kseg_devuelve_datos.fn_nombre_usuario(a.occ_usu_id_genera) usuario_contac,',
'       a.occ_fecha_genera fecha',
'  from car_orden_cuota_comodin a, asdm_clientes b, asdm_personas p',
'  where p.per_id = b.per_id',
'  and b.cli_id = a.cli_id',
'  AND a.uge_id = :f_uge_Id',
'  and occ_estado is null',
'  and com_id_nc is null',
''))
,p_display_when_condition=>'F_UGE_ID'
,p_display_when_cond2=>'1601'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909403092052240)
,p_query_column_id=>1
,p_column_alias=>'OCC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Occ id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909518923052241)
,p_query_column_id=>2
,p_column_alias=>'PER_TIPO_IDENTIFICACION'
,p_column_display_sequence=>2
,p_column_heading=>'Per tipo identificacion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909620597052242)
,p_query_column_id=>3
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>3
,p_column_heading=>'Per nro identificacion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909704707052243)
,p_query_column_id=>4
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>4
,p_column_heading=>'Nombres'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909752329052244)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909941200052245)
,p_query_column_id=>6
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Div nro vencimiento'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498909954233052246)
,p_query_column_id=>7
,p_column_alias=>'USUARIO_CONTAC'
,p_column_display_sequence=>7
,p_column_heading=>'Usuario contac'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(34498910101661052247)
,p_query_column_id=>8
,p_column_alias=>'FECHA'
,p_column_display_sequence=>8
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7074774330403131550)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(6594658059513351516)
,p_button_name=>'orden_seguro'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Orden Emision Seguro'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT cu.cup_cupo_disponible',
'  FROM car_cupos_clientes cu',
' WHERE cu.cli_id = :p6_cli_id',
'   AND cu.cup_tipo_cupo = ''M''',
'   AND cu.cup_estado_registro = 0',
'   and not exists (SELECT null',
'  FROM car_cuentas_por_cobrar',
' WHERE ede_id = 1186',
'   AND cxc_saldo > 0',
'   AND cli_id = :p6_cli_Id)) >= 8.9'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(44979415357985165427)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(10165452173699463573)
,p_button_name=>'Imprimir_Detalle_Acuerdo'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Detalle Acuerdo'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=CAR_ACUERDO_DE_PAGO_DET.rdf&userid=asdm_p/asdm_p@sicpro01&destype=cache&desformat=pdf&PN_ACP_ID=&P6_ACUERDO_ID.''));'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(102524481216876145)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_button_name=>'Formas_Pago'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Seguir >>'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:30:&SESSION.:formas_pago:&DEBUG.::P30_CLI_ID,P30_CLI_IDENTIFICACION,P30_MENSAJE,P30_FACTURAR,P30_CLI_NOMBRE,P30_PAGO_REFINANCIAMIENTO,P30_VALOR_REFINANCIAMIENTO,P30_CXC_ID_REF,P30_ES_PRECANCELACION:&P6_CLI_ID.,&P6_IDENTIFICACION.,,N,&P6_NOMBRE.,&P6_PAGO_REFINANCIAMIENTO.,&P6_VALOR_REFINANCIAMIENTO.,&P6_CXC_ID_REF.,N'
,p_button_condition=>':P6_CLI_ID is not null'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(102524677576876154)
,p_button_sequence=>800
,p_button_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_button_name=>'CONDONAR_NO_VALE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CONDONAR_NO_VALE'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(55050476381596206030)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_button_name=>'Regresar_gar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'TOP'
,p_button_condition=>':F_PAG_ORIGEN IS NOT NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(102528768715876196)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(102525864564876156)
,p_button_name=>'PAGAR_SELECCIONADOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Pagar Seleccionados'
,p_button_position=>'TOP_AND_BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(104421769791145443)
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.:formas_pago:&DEBUG.::P30_CLI_ID,P30_CLI_IDENTIFICACION,P30_CLI_NOMBRE,P30_MENSAJE,P30_VALOR_TOTAL_PAGOS,P30_FACTURAR,P30_ES_PRECANCELACION:&P6_CLI_ID.,&P6_IDENTIFICACION.,&P6_NOMBRE.,,6,N,N&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(102524481216876145)
,p_branch_sequence=>1
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 07-NOV-2011 10:30 by YGUAMAN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38347307652529394753)
,p_branch_name=>'Go To Page 11'
,p_branch_action=>'f?p=128:11:&SESSION.:cargar_datos:&DEBUG.:RP:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_EMP_LOGO,F_EMP_FONDO,F_VERSION,F_UGESTION,P11_APP_ID,P11_PAG_ID,F_POPUP,P11_NRO_IDENTIFICACION:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_VERSION.,&F_UGESTION.,211,6,S,&P6_IDENTIFICACION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>2
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_clientes_contac.fn_dev_validacion_datos(pv_per_nro_identificacion => :P6_IDENTIFICACION,',
'                                                             pn_emp_id => :F_EMP_ID)=1 ',
'',
'AND :REQUEST=''cliente'''))
,p_branch_condition_text=>'PLSQL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(102535783875876272)
,p_branch_action=>'f?p=&FLOW_ID.:6:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(102528768715876196)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(102535978343876279)
,p_branch_action=>'f?p=&APP_ID.:44:&SESSION.::&DEBUG.::P44_DESDE_PAGO_CAJA:S'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(102524677576876154)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(102536161472876280)
,p_branch_action=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>30
,p_branch_comment=>'Created 22-ENE-2010 17:22 by ONARANJO'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102514466651876129)
,p_name=>'P6_DIV_SALDO_CUOTA'
,p_item_sequence=>167
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Div Saldo Cuota'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102514663171876133)
,p_name=>'P6_REQUEST'
,p_item_sequence=>171
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Request'
,p_source=>':REQUEST'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102514880937876133)
,p_name=>'P6_COM_NUMERO_COMPLETO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Com Numero Completo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102515066829876133)
,p_name=>'P6_DIV_SALDO_GASTO_COBRANZA'
,p_item_sequence=>166
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Div Saldo Gasto Cobranza'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102515251547876134)
,p_name=>'P6_EMP_ID'
,p_item_sequence=>161
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Emp Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102515462819876134)
,p_name=>'P6_DIV_FECHA_VENCIMIENTO'
,p_item_sequence=>163
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Div Fecha Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102515659864876134)
,p_name=>'P6_CXC_SALDO'
,p_item_sequence=>164
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Cxc Saldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102515870397876135)
,p_name=>'P6_DIV_SALDO_INTERES_MORA'
,p_item_sequence=>165
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Div Saldo Interes Mora'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102516063557876135)
,p_name=>'P6_COM_ID'
,p_item_sequence=>157
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102516280296876135)
,p_name=>'P6_COM_NUMERO'
,p_item_sequence=>158
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Com Numero'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102516462910876135)
,p_name=>'P6_DIV_NRO_VENCIMIENTO'
,p_item_sequence=>159
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Div Nro Vencimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102516662551876135)
,p_name=>'P6_DIV_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(102514281895876128)
,p_prompt=>'Div Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102523067736876142)
,p_name=>'P6_CONDONAR_TODO'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(102516852426876136)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'upper(''n'');',
''))
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'<SPAN STYLE="font-size: 10pt">Condonar Todo el Interes de Mora: </span>'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_named_lov=>'LOV_SI_NO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(300);',
'BEGIN',
'lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_SI_NO'');',
'return (lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''act_int_condonado'')"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'RIGHT-CENTER'
,p_display_when=>':F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*:F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_mayoreo'') and */',
'',
'Esta en HTML Form Element Attributes para que ejecute proceso sin submit:',
'',
'onChange="pr_carga_cuotas_con_valor(''PR_ACTUALIZA_INT_CONDONADO'',''P6_F_EMP_ID'',''P6_F_SEG_ID'',this.value,''P6_F_EMP_ID'',''R70263701157111210'',''R70272713295111230'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P6_F_EMP_ID'',''TP'',''P6_VALOR_PAGO_COLECCION'')'
||';pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P6_F_EMP_ID'',''IC'',''P6_TOTAL_INT_COND'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P6_F_EMP_ID'',''GC'',''P6_TOTAL_GST_COND'');pr_asigna_valores(''PR_CALCULA_TOTALES_PAGO'',''P6_F_EMP_ID'',''TC'',''P6_TOTAL_CONDONAR'
||''')"'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102523261530876143)
,p_name=>'P6_SEQ_ID'
,p_item_sequence=>169
,p_item_plug_id=>wwv_flow_imp.id(102516852426876136)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102523459343876143)
,p_name=>'P6_SALDO_VALOR_PAGO'
,p_item_sequence=>181
,p_item_plug_id=>wwv_flow_imp.id(102516852426876136)
,p_item_default=>'0'
,p_prompt=>'Saldo Valor Pago'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102523682874876143)
,p_name=>'P6_ERROR'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(102516852426876136)
,p_item_default=>'NULL;'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Error'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:16; color:AA0000"'
,p_colspan=>4
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P6_ERROR'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102523882460876144)
,p_name=>'P6_P0_ERROR'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(102516852426876136)
,p_use_cache_before_default=>'NO'
,p_prompt=>'P0 Error'
,p_source=>':P0_ERROR'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102524859307876154)
,p_name=>'P6_VALOR_PAGO_COLECCION'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Pago'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c016),0)',
' FROM apex_collections',
'       WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24" disabled'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102525061721876155)
,p_name=>'P6_LLAVE_AUTORIZACION'
,p_item_sequence=>315
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_use_cache_before_default=>'NO'
,p_prompt=>'LLave autorizacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(c011)',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102525267249876155)
,p_name=>'P6_TOTAL_INT_COND'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Int Condonar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c022),0)',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')',
'and c006 = :F_EMP_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102525459601876155)
,p_name=>'P6_TOTAL_GST_COND'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Gst Condonar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c023),0)',
'  FROM apex_collections',
' WHERE collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')',
'and c006 = :F_EMP_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102525680447876156)
,p_name=>'P6_TOTAL_CONDONAR'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Condonar'
,p_source=>':P6_TOTAL_INT_COND + :P6_TOTAL_GST_COND;'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>7
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102529160236876197)
,p_name=>'P6_PER_TIPO_IDENTIFICACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*IF :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')  THEN',
'return pq_constantes.fn_retorna_constante(NULL,''cv_per_tipo_iden_ced'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(NULL,''cv_per_tipo_iden_ruc''); ',
'END IF*/null'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tipo Identificacion:'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102529383346876205)
,p_name=>'P6_FECHA_DESDE'
,p_item_sequence=>172
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102529555624876205)
,p_name=>'P6_FECHA_HASTA'
,p_item_sequence=>192
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'Fecha Hasta:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="pr_ejecuta_proc_solo_campos(''PR_CARGA_DIV_PENDIENTES'',''P6_CLI_ID'',''P6_F_EMP_ID'',''P6_F_SEG_ID'',''P6_FECHA_DESDE'',''P6_FECHA_HASTA'',''R70263701157111210'',''R70272713295111230'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_item_comment=>'No tenia condicion.'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102529756302876205)
,p_name=>'P6_CLI_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'Cliente: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>5
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102529964978876206)
,p_name=>'P6_IDENTIFICACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'ID. Cliente:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.per_nro_identificacion || '' '' ||',
'       trim(b.per_razon_social || b.per_primer_apellido || '' '' ||',
'       b.per_segundo_apellido || '' '' || b.per_primer_nombre || '' '' ||',
'       b.per_segundo_nombre) || '' '' || a.cli_id NOMBRE,',
'       b.per_nro_identificacion CODIGO',
'  FROM asdm_clientes a, asdm_personas b',
' WHERE b.per_id = a.per_id   ',
'   AND b.per_estado_registro = 0',
'   AND a.cli_estado_registro = 0',
'   and a.emp_id = :f_emp_id',
'   and b.emp_id = :f_emp_id'))
,p_cSize=>20
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')";onKeyDown="if(event.keyCode==13) doSubmit(''cliente'');"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102530180540876206)
,p_name=>'P6_NOMBRE'
,p_item_sequence=>22
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102530380379876206)
,p_name=>'P6_CORREO'
,p_item_sequence=>182
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102530565383876206)
,p_name=>'P6_DIRECCION'
,p_item_sequence=>72
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102530758685876207)
,p_name=>'P6_TIPO_DIRECCION'
,p_item_sequence=>62
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102530974072876215)
,p_name=>'P6_TIPO_TELEFONO'
,p_item_sequence=>52
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102531180418876215)
,p_name=>'P6_TELEFONO'
,p_item_sequence=>202
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102531383806876215)
,p_name=>'P6_CLIENTE_EXISTE'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102531574399876215)
,p_name=>'P6_CXC_ID'
,p_item_sequence=>162
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102531763818876215)
,p_name=>'P6_VALOR_PAGO'
,p_item_sequence=>212
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_item_default=>'0'
,p_display_as=>'NATIVE_HIDDEN'
,p_help_text=>'Con este valor se cancelaran las CUOTAS VENCIDAS que alcancen.'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102532280767876217)
,p_name=>'P6_TOTAL_CHEQUES'
,p_item_sequence=>112
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>'P6_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102532462071876217)
,p_name=>'P6_SALDO_ANTICIPOS'
,p_item_sequence=>42
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Anticipos'
,p_source=>'pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P6_CLI_ID,:f_uge_id);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P6_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P6_CLI_ID,:f_uge_id);'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102532668318876218)
,p_name=>'P6_DIR_ID'
,p_item_sequence=>132
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102532854140876218)
,p_name=>'P6_CLI_ID_REFERIDO'
,p_item_sequence=>142
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102533076247876224)
,p_name=>'P6_CLI_ID_NOMBRE_REFERIDO'
,p_item_sequence=>152
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102533275897876225)
,p_name=>'P6_F_EMP_ID'
,p_item_sequence=>222
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_use_cache_before_default=>'NO'
,p_source=>':F_EMP_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102538481609892235)
,p_name=>'P6_F_SEG_ID'
,p_item_sequence=>232
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_use_cache_before_default=>'NO'
,p_source=>':F_SEG_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(107058876246678506)
,p_name=>'P6_FECHA_HASTA_CORTE'
,p_item_sequence=>102
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'Fecha Hasta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_FECHAS_CORTE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c001 DESCRIPCION,',
'       a.c002 VALOR',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_FECHAS_CORTE'''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
,p_item_comment=>'onChange="pr_ejecuta_proc_solo_campos(''PR_FILTRA_DIV_PENDIENTES'',''P6_CLI_ID'',''P6_F_EMP_ID'',''P6_F_SEG_ID'',''P6_FECHA_DESDE'',''P6_FECHA_HASTA_CORTE'',''R70263701157111210'',''R70272713295111230'')"'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109770665862866112)
,p_name=>'P6_AGENTE_COBRADOR'
,p_item_sequence=>32
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'Cobrador:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(112266381134036225)
,p_name=>'P6_TFP_ID'
,p_item_sequence=>242
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_item_default=>'0'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(121238056235286991)
,p_name=>'P6_NOMBRE_INSTITUCION'
,p_item_sequence=>92
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'  -  '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(121246882442284553)
,p_name=>'P6_INS_ID'
,p_item_sequence=>82
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>unistr('Instituci\00F3n:')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122555972164013495)
,p_name=>'P6_MENSAJE'
,p_item_sequence=>252
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'<SPAN STYLE="font-size: 15pt;color:RED">APLICAR EL PAGO CON ANTICIPO DE CLIENTES</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_new_grid=>true
,p_colspan=>1
,p_rowspan=>1
,p_grid_column=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'NVL(:P6_SALDO_ANTICIPOS,0) > 0  and :P6_CLI_ID is not null and :F_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(213645416819915242)
,p_name=>'P6_ALERTA_SEGURO'
,p_item_sequence=>332
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) =',
'              extract(MONTH FROM SYSDATE) THEN',
'          ''CLIENTE CON SEGURO POR RENOVAR''',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) <',
'              extract(MONTH FROM SYSDATE) AND a.dse_saldo_cuota > 0 THEN',
'          ''CLENTE CON SEGURO CADUCADO PENDIENTE DE PAGO''',
'         WHEN extract(MONTH FROM a.dse_fecha_vencimiento) <',
'              extract(MONTH FROM SYSDATE) AND a.dse_saldo_cuota = 0 THEN',
'          ''CLENTE CON SEGURO CADUCADO''',
'       END texto',
'  FROM car_dividendos_seguros a, car_cuentas_seguros b',
' WHERE b.cse_id = a.cse_id',
'   AND b.cse_plazo = a.dse_nro_vencimiento',
'   AND a.dse_fecha_vencimiento <= LAST_DAY(SYSDATE)',
'   AND b.ese_id = ''ENV''',
'   and b.cli_id = :p6_cli_Id',
'   and rownum = 1'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:16;color:GREEN;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(953689654842332647)
,p_name=>'P6_CONTACTOS_NEG'
,p_item_sequence=>302
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'Contactos'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:15;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P6_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6594643055189257703)
,p_name=>'P6_TIPO_BUSQUEDA'
,p_item_sequence=>312
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_prompt=>'Tipo Busqueda'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Cuotas a Pagar;2,Todo;1,Vencido;3'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7470260968256956464)
,p_name=>'P6_NOMBRE_SEGMENTO'
,p_item_sequence=>352
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7470261109379956465)
,p_name=>'P6_COLOR_SEGMENTO'
,p_item_sequence=>362
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7470261376853956468)
,p_name=>'P6_ALERTA_SEGMENTO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(7470260152518956456)
,p_prompt=>'      '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cattributes_element=>'style="font-size:18;color:RED"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(10165744466351488860)
,p_name=>'P6_ACUERDO_ID'
,p_item_sequence=>292
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MAX(c.acp_id) ',
'FROM car_acuerdos_pago_cab c ',
'WHERE c.cli_id = :P6_CLI_ID',
'AND   c.acp_estado_registro = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':P6_CLI_ID IS NOT NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(12652451796218971533)
,p_name=>'P6_CSC_ID'
,p_item_sequence=>342
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13464805979425211832)
,p_name=>'P6_VALOR_REFINANCIAMIENTO'
,p_item_sequence=>510
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_prompt=>'Valor Refinanciamiento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:20;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P6_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13464832458085215138)
,p_name=>'P6_VALOR_CAPITAL_REFIN'
,p_item_sequence=>520
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_prompt=>'Valor Capital Refin'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P6_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13464861873322219553)
,p_name=>'P6_CXC_ID_REF'
,p_item_sequence=>530
,p_item_plug_id=>wwv_flow_imp.id(102524263207876145)
,p_prompt=>'Cxc Id Ref'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P6_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13464940278994230592)
,p_name=>'P6_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>262
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13464955753845232797)
,p_name=>'P6_SALDO_CXC_REF'
,p_item_sequence=>272
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(13464972661117234926)
,p_name=>'P6_INTERESES_NO_PAG_RE'
,p_item_sequence=>282
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(14218938573838900653)
,p_name=>'P6_VALIDA_ACTUA_CLIENTE'
,p_item_sequence=>322
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  LN_DIAS_PARAMETRO NUMBER;',
'  LN_VALOR          NUMBER;',
'',
'  LN_VARIABLE NUMBER := pq_constantes.fn_retorna_constante(:F_EMP_ID,',
'                                                           ''cn_par_id_actu_info_cli'');',
'',
'BEGIN',
'',
'',
'if :P6_CLI_ID is not null then ',
'  SELECT PA.PEN_VALOR',
'    INTO LN_DIAS_PARAMETRO',
'    FROM ASDM_E.CAR_PAR_ENTIDADES PA',
'   WHERE PA.PAR_ID = LN_VARIABLE;',
'',
'  select TRUNC(SYSDATE - C.CLI_FECHA_ACTUALIZACION)',
'    INTO LN_VALOR',
'    from asdm_clientes c',
'   where cli_id = :P6_CLI_ID;',
'   ',
'   IF LN_VALOR >= LN_DIAS_PARAMETRO  THEN',
'     ',
'     RETURN 1;',
'     ',
'   ELSE ',
'     RETURN 0;',
'   END IF;',
'         ',
'end if;',
'',
'exception',
' when others then',
'return 0;',
'END;'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55050479212070206058)
,p_name=>'P6_CARGADO'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(102528983338876197)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(102533569799876227)
,p_validation_name=>'valor mayor 0'
,p_validation_sequence=>10
,p_validation=>'v(''P6_VALOR_PAGO'') > 0'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Ingrese el valor pago mayor a 0'
,p_validation_condition=>'cuotas'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(102531763818876215)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(103929766061678079)
,p_validation_name=>'P6_VALOR_PAGO_COLECCION_MAYOR_A_CERO'
,p_validation_sequence=>20
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_total number;',
'begin',
'',
'SELECT NVL(SUM(c016),0)',
'into ln_total',
' FROM apex_collections',
'WHERE collection_name =       pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota'')',
'    and c001 = :P6_CLI_ID;',
'',
'if ln_total=0 then',
'raise_application_error(-20000,''Error'');',
'end if;',
'',
'end;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'El valor a cancelar debe ser mayor a cero'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(102524481216876145)
,p_associated_item=>wwv_flow_imp.id(102524859307876154)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(102535268824876250)
,p_name=>'pr_elimina_coleccion_mov'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'unload'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(102535574107876268)
,p_event_id=>wwv_flow_imp.id(102535268824876250)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
''))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(107191180381578439)
,p_name=>'pr_actualiza_pagina'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107191482438578451)
,p_event_id=>wwv_flow_imp.id(107191180381578439)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(102525864564876156)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(107267481008098969)
,p_name=>'pr_actualiza_item_condonar'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_CONDONAR_TODO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(107267773822098969)
,p_event_id=>wwv_flow_imp.id(107267481008098969)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(102516852426876136)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(13465115653761251654)
,p_name=>'ad_carga_Segmento'
,p_event_sequence=>40
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(13465115977509251665)
,p_event_id=>wwv_flow_imp.id(13465115653761251654)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'NULL;'
,p_attribute_02=>'P6_F_SEG_ID'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(6594646967170306367)
,p_name=>'ad_refresh_item_busqueda'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_TIPO_BUSQUEDA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(6594647268827306376)
,p_event_id=>wwv_flow_imp.id(6594646967170306367)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P6_TIPO_BUSQUEDA'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(6594647382407310840)
,p_name=>'ad_carga_div_pendientes'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_TIPO_BUSQUEDA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(6594647659573310846)
,p_event_id=>wwv_flow_imp.id(6594647382407310840)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id  => :P6_CLI_ID,',
'                                     pn_emp_id  => :f_emp_id,',
'                                     pn_seg_id => :F_SEG_ID,',
'                                     pd_fecha_desde  => NULL,',
'                                     pd_fecha_hasta => NULL, ',
'                                     pn_tfp_id      => :P6_TFP_ID, ',
'                                     pn_tipo_busqueda => :P6_TIPO_BUSQUEDA,                                 ',
'                                     pv_error  => :P0_error);'))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(6594648766608315732)
,p_name=>'ad_refresh_region_cartera_cliente'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_TIPO_BUSQUEDA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(6594649066397315734)
,p_event_id=>wwv_flow_imp.id(6594648766608315732)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(102525864564876156)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(55050476068252206027)
,p_name=>'da_carga_cliente_efec_garantias'
,p_event_sequence=>80
,p_condition_element=>'P6_IDENTIFICACION'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'EXPRESSION'
,p_display_when_cond=>':P6_CLI_ID IS NULL AND :P6_CARGADO IS NULL'
,p_display_when_cond2=>'PLSQL'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(55050476241195206028)
,p_event_id=>wwv_flow_imp.id(55050476068252206027)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102533682926876238)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'',
'ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja'');',
':P0_TTR_ID := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_pago_cuota'');',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:F_EMP_ID,',
'                                                        :F_PCA_ID,',
'                                                        ln_tgr_id ,',
'                                                        :P0_TTR_DESCRIPCION, ',
'                                                        :P0_PERIODO,',
'                                                        :P0_DATFOLIO,',
'                                                        :P0_FOL_SEC_ACTUAL,',
'                                                        :P0_pue_num_sri,',
'                                                        :P0_uge_num_est_sri,',
'                                                        :P0_PUE_ID,',
'                                                        :P0_TTR_ID,',
'                                                        :P0_NRO_FOLIO,',
'                                                        :P6_COM_NUMERO_COMPLETO);',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>70280531657111312
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102533859948876248)
,p_process_sequence=>3
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_colec_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':p6_error:=null;',
'pq_ven_pagos_cuota_retencion.pr_elimina_colec_pago_cuota(:P6_SEQ_ID, ',
'                                               :P6_CLI_ID, ',
'                                               :P6_DIV_NRO_VENCIMIENTO, ',
'                                               :f_emp_id, ',
'                                               :P6_CXC_ID, ',
'                                               :P6_VALOR_PAGO_COLECCION, ',
'                                               :P6_SALDO_VALOR_PAGO, ',
'                                               :P6_ERROR);',
':P6_SEQ_ID := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'(:REQUEST = ''eliminar_cuota'') AND (:P6_SEQ_ID IS NOT NULL) '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>70280708679111322
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102534076192876248)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'ln_age_id number;',
'ln_zon_id number;',
'lv_zona varchar2(100);',
'ln_size_id NUMBER(10);',
'lv_pasa varchar2(10);',
'',
'  ln_csc_id_inconsistencia number := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_csc_id_inconsistencia'');',
'',
'BEGIN',
':P6_NOMBRE_INSTITUCION :=  NULL;',
':P6_INS_ID := NULL;',
':P6_CLI_ID := NULL;',
':P6_AGENTE_COBRADOR := NULL;',
':P6_CARGADO := ''S'';',
'---------------------------------------------------',
'IF :P6_IDENTIFICACION IS NOT NULL THEN',
'   BEGIN',
'      SELECT    MIN(a.per_tipo_identificacion)',
'      INTO      :P6_PER_TIPO_IDENTIFICACION',
'      FROM      asdm_personas a',
'      WHERE     a.per_nro_identificacion = :P6_IDENTIFICACION ',
'      AND       a.emp_id = :F_EMP_ID',
'      AND EXISTS (SELECT NULL FROM ASDM_CLIENTES CL WHERE CL.PER_ID=A.PER_ID);---jalvarez 10mar2021 se aumenta para validar que sea cliente',
'   EXCEPTION',
'      WHEN OTHERS THEN',
'         :P6_PER_TIPO_IDENTIFICACION := NULL;',
'   END;',
'   ',
'END IF;',
'----------------------------------------------------',
'',
'pq_ven_listas2.pr_datos_clientes(:F_UGE_ID,                 ',
'                                :F_EMP_ID,                 ',
'                                :P6_IDENTIFICACION, ',
'                                :P6_NOMBRE,                 ',
'                                :P6_CLI_ID,',
'                                :P6_CORREO,',
'                                :P6_DIRECCION,',
'                                :P6_TIPO_DIRECCION,',
'                                :P6_TELEFONO,',
'                                :P6_TIPO_TELEFONO,',
'                                :P6_CLIENTE_EXISTE,',
'                                :P6_DIR_ID,',
'                                :P6_CLI_ID_REFERIDO,',
'                                :P6_CLI_ID_NOMBRE_REFERIDO,',
'                                :P6_PER_TIPO_IDENTIFICACION,',
'                                :P0_ERROR);',
'',
'begin',
' ',
'   SELECT CR.CSC_COLOR, CR.CSC_NOMBRE_COMERCIAL, cr.csc_id',
'  INTO :P6_COLOR_SEGMENTO, :P6_NOMBRE_SEGMENTO, :p6_csc_id',
'  FROM ASDM_CLIENTES CL, ASDM_E.CAR_SEGMENTACION_CREDITO CR',
' WHERE CL.CLI_ID =  :p6_cli_id',
'   AND CL.CSC_ID = CR.CSC_ID;',
'                                ',
'     if ln_csc_id_inconsistencia  = :p6_csc_id then',
'       ',
'       :P6_ALERTA_SEGMENTO := ''Por favor revisar si los siguientes datos del Cliente son correctos, Resultado Equifax, Clasificacion Cliente. Verificar si se registraron los Ingresos del Cliente'';',
'     ',
'     else ',
'     ',
'       :P6_ALERTA_SEGMENTO := null;',
'     ',
'     end if;',
'',
'exception',
'  when no_data_found then null;',
'',
'end;',
'',
':P6_NOMBRE := replace(:P6_NOMBRE,''&'','' '');     ',
'-- Yguaman 2011/12/15. Institucion de Cliente',
'BEGIN',
'SELECT ains.ins_id,ape.per_razon_social',
'INTO :P6_INS_ID, :P6_NOMBRE_INSTITUCION',
'FROM asdm_clientes_instituciones acin, asdm_instituciones ains, asdm_personas ape',
'WHERE acin.cli_id = :P6_CLI_ID',
'AND acin.cin_estado_registro = 0',
'AND acin.ins_id = ains.ins_id',
'AND ains.per_id = ape.per_id',
'AND ains.ins_estado_registro = 0',
'AND ape.per_estado_registro = 0',
'AND APE.EMP_ID = 1',
'AND acin.CIN_ID = (SELECT MAX(CI.CIN_ID)',
'                   FROM ASDM_CLIENTES_INSTITUCIONES CI',
'                   WHERE CI.CLI_ID = :P6_CLI_ID',
'                   AND CI.CIN_ESTADO_REGISTRO = 0',
'                   AND CI.EMP_ID = 1)',
';',
'',
'EXCEPTION',
'WHEN NO_DATA_FOUND THEN',
' :P6_INS_ID := NULL;',
' :P6_NOMBRE_INSTITUCION := ''NO ASIGNANDO'';',
'',
'END;',
'lv_pasa := ''a'';',
'/*pq_ven_pagos_cuota.pr_carga_saldos_cliente(:P6_CLI_ID,',
'                                           :F_EMP_ID,',
'                                           :P6_TOTAL_CHEQUES,',
'                                           :P0_ERROR);*/',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'',
'if :P6_CLI_ID is not  null and :f_SEG_ID = pq_constantes.fn_retorna_constante(NULL,''cn_tse_id_minoreo'') then',
'',
'-- Yguaman 2011/11/23 18:16pm, agregado para grabar el cobrador cuando se va a generar la transaccion de pago de cuota',
'-- Solo para minoreo y cuando es el pago de cuota -- Jandres.',
'lv_pasa := ''b'';',
' :P6_AGENTE_COBRADOR := pq_car_cobranza.fn_retorna_cobrador(pn_emp_id =>:F_EMP_ID ,  -- cdigo empresa',
'                                                          pn_cli_id => :P6_CLI_ID,  -- codigo cliente',
'                                                          pn_uge_id => :F_UGE_ID); -- codigo unidad de gestion',
'end if;',
'lv_pasa := ''c'';',
':P6_COM_ID := NULL;',
':P6_VALOR_PAGO := 0;',
':P6_SALDO_VALOR_PAGO := 0;',
':P6_CXC_ID := null;',
'exception when others then',
'raise_application_error(-20000,''datos_cliente''||lv_pasa||sqlerrm);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>70280924923111322
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102534274711876249)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_div_pendientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id  => :P6_CLI_ID,',
'                                     pn_emp_id  => :f_emp_id,',
'                                     pn_seg_id => :F_SEG_ID,',
'                                     pd_fecha_desde  => NULL,',
'                                     pd_fecha_hasta => NULL, ',
'                                     pn_tfp_id      => :P6_TFP_ID, ',
'                                     pn_tipo_busqueda => :P6_TIPO_BUSQUEDA,                                 ',
'                                     pv_error  => :P0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>70281123442111323
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102534469993876249)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cuotas_vencidas_con_valor_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'--lb_existe BOOLEAN;',
'',
'BEGIN',
'',
'/*:P6_FECHA_DESDE := NULL;',
':P6_FECHA_HASTA := NULL;',
':P6_FECHA_HASTA_CORTE := NULL;*/',
'',
':P6_SALDO_VALOR_PAGO := :P6_VALOR_PAGO;',
'       ',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'--pq_inv_movimientos.pr_crea_borra_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_ven_pagos_cuota.pr_carga_colec_cuotas_vencidas(:P6_CLI_ID, ',
'                                                  :F_EMP_ID, ',
'                                                  :P6_SALDO_VALOR_PAGO, ',
'                                                  :P0_ERROR);',
'',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'valor_cuota'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>70281318724111323
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102534668253876249)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_coleccion_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'/*pq_ven_pagos_cuota.pr_actualiza_colec_div_pagar(:f_emp_id,:P6_ERROR);*/',
'',
'',
'pq_ven_pagos_cuota.pr_actualiza_colec_div_pagar(pn_emp_id => :F_EMP_ID,',
'                             pn_div_id => 0,',
'                             pn_total  => 0,',
'                             pn_im_cond => 0,',
'                             pn_gc_cond => 0,',
'                             pn_tse_id  => 0,',
'                             pv_error  => :P6_ERROR);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'actualiza_cuota'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'Actualiza cuota '
,p_internal_uid=>70281516984111323
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102534877943876249)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_colec_div_pagar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota.pr_carga_colec_div_pagar( pn_seq_id => NULL,',
'                                              pv_error         => :p0_error,',
'                                              pn_emp_id        => :f_emp_id,',
'                                              pn_ttr_id_origen => pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_pago_cuota''),',
'                                              pn_tse_id => :F_SEG_ID);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'paga_selec'
,p_process_when_type=>'NEVER'
,p_internal_uid=>70281726674111323
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(110839151808584284)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_int_condonado'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_VEN_PAGOS_CUOTA.pr_actualiza_int_condonado(pn_emp_id => :F_EMP_ID,',
'                                       pv_selecc =>:P6_CONDONAR_TODO,                                 ',
'                                       pv_error  =>   :P0_error,',
'                                       pn_tse_id =>   :F_SEG_ID,',
'                                       pn_tfp_id =>   :P6_TFP_ID);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''act_int_condonado'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>'bien'
,p_internal_uid=>78586000538819358
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(6594667281246376753)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_cxc_puntualito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' BEGIN',
'  ',
'  pq_asdm_promociones.pr_valida_cxc_promocion(pn_emp_id => :f_emp_id,',
'                                              pn_cli_id => :p6_cli_id,',
'                                              pn_seg_id => :F_SEG_ID,',
'                                              pn_pro_id => 1,',
'                                              pv_error => :p0_error);',
'                                              ',
'   END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>6562414129976611827
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7074774391711131551)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_emision_seguro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  pr_url(--pn_app_origen => null,',
'         pn_app_destino => 242,',
'         --pn_pag_origen => null,',
'         pn_pag_destino => 54,',
'         --pv_sesion => :SESSION,',
'         pv_request => ''CLIORDEN'',',
'         pv_parametros => ''P54_CLI_ID,P54_CLASIFICACION,P54_ORIGEN'',',
'         pv_valores => :P6_CLI_ID||'',''||:P6_CSE_ID||'',''||''P'');',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(7074774330403131550)
,p_internal_uid=>7042521240441366625
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(55050476264838206029)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_redirect_garantias'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pr_url(pn_app_destino => :F_APP_ORIGEN,',
'         pn_pag_destino => :F_PAG_ORIGEN,',
'         pv_request => null,',
'         pv_parametros => ''P60_PER_NRO_IDENTIFICACION'',',
'         pv_valores => :P6_IDENTIFICACION);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(55050476381596206030)
,p_internal_uid=>55018223113568441103
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(102535067582876249)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_coleccion_cuotas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_PREC_SELECCIONADOS'');',
'pq_inv_movimientos.pr_elimina_colecciones(''COLL_DIV_SELECCIONADOS'');'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P6_CLI_ID is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>70281916313111323
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(103923352031636233)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_col_div_pendientes'
,p_process_sql_clob=>'pq_inv_movimientos.pr_elimina_colecciones(''CO_DIV_PENDIENTES'');'
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P6_CLI_ID is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>71670200761871307
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(107060861879712161)
,p_process_sequence=>60
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_FECHAS_CORTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
' /* pq_car_credito.pr_fechas_corte_pago_cuota(pn_emp_id => :f_emp_id,',
'                              pn_tse_id => :F_SEG_ID,',
'                              pd_fecha  => SYSDATE, ',
'                              pv_error  => :p0_error);*/null;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>74807710609947235
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(954282779321363576)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_alertas_neg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE ',
'',
'ln_ede_id_ars NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_ede_id_ars'');',
'',
'ln_ede_id_recsa NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_ede_id_recsa'');',
'',
'ln_ede_id_masoluc2  number := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_ede_id_masoluc2'');',
'',
'ln_ede_id_projuridico  number := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_ede_id_projuridico'');',
'',
'ln_num_ars NUMBER;',
'ln_num_recsa NUMBER;',
'ln_num_masoluc2 NUMBER;',
'ln_num_projuridico number;',
'',
'',
'',
'',
'',
'',
'BEGIN',
'',
'',
'',
'SELECT COUNT(*)',
'INTO ln_num_masoluc2',
'FROM car_cuentas_por_cobrar cx',
'WHERE cx.ede_id =  ln_ede_id_masoluc2',
'and cx.cli_id = :P6_CLI_ID;',
'',
'',
'IF ln_num_masoluc2 > 0 THEN',
'',
'',
'  :P6_CONTACTOS_NEG := ''En los creditos negociados con RECUMPS se aceptara como abono a la cuenta cualquier valor que el cliente entregue,''|| CHR(13) ||''',
'                        sin considerar si este se aplica a la cuota o como interes de mora.'';',
'END IF;',
'',
'',
'',
'SELECT COUNT(*)',
'INTO ln_num_ars',
'FROM car_cuentas_por_cobrar cx',
'WHERE cx.ede_id =  ln_ede_id_ars',
'and cx.cli_id = :P6_CLI_ID;',
'',
'-- raise_application_Error(-20000, ''aquiiii'' || ln_num_ars);',
'',
'',
'IF ln_num_ars > 0 THEN',
'',
'',
unistr('  :P6_CONTACTOS_NEG := ''Cr\00E9dito  vendido a ARS del Ecuador, por favor contactar al  personal de servicio al cliente en las  oficinas de Quito (Av. Amazonas 2248 y Ram\00EDrez D\00E1valos) ''|| CHR(13) ||'''),
unistr('                        y Guayaquil (Urdesa Central, Calle diagonal No.417C y V\00EDctor Emilio Estrada Of. 204). Tel\00E9fonos: 02 3815160, 1800 \2013 CARTERA. Contacto: Patricia Rodr\00EDguez'';'),
'END IF;',
'',
'',
'SELECT COUNT(*)',
'INTO ln_num_recsa',
'FROM car_cuentas_por_cobrar cx',
'WHERE cx.ede_id =  ln_ede_id_recsa',
'and cx.cli_id = :P6_CLI_ID;',
'',
'',
'IF ln_num_recsa > 0 AND ln_num_ars > 0 THEN',
'',
unistr('  :P6_CONTACTOS_NEG := ''Cr\00E9dito  vendido a ARS del Ecuador, por favor contactar al  personal de servicio al cliente en las  oficinas de Quito (Av. Amazonas 2248 y Ram\00EDrez D\00E1valos) ''|| CHR(13) ||'''),
unistr('                        y Guayaquil (Urdesa Central, Calle diagonal No.417C y V\00EDctor Emilio Estrada Of. 204). Tel\00E9fonos: 02 3815160, 1800 \2013 CARTERA. Contacto: Patricia Rodr\00EDguez'';'),
'                        ',
' :P6_CONTACTOS_NEG  := :P6_CONTACTOS_NEG || CHR(13) || CHR(13) ||',
' ',
unistr('                       ''Cr\00E9dito vendido a COBRANZAS DEL ECUADOR (RECSA), por favor contactarse con el personal de servicio al cliente'' || CHR(13) || CHR(13)||'),
'                       ''QUITO Katy Egas kegas@recsa.com.ec (02) 2999-800 / 2983-900  EXT. 9822'' || CHR(13) ||',
'                       ''CUENCA  Thelmo Guzman tguzman@recsa.com.ec (07)2820858  '' || CHR(13) ||',
unistr('                       ''AMBATO  Martha Proa\00F1o mproano@recsa.com.ec (03) 2822-883 / 2822-366  EXT. 6081 '' || CHR(13) ||'),
'                       ''GUAYAQUIL Michel Onofre donofre@recsa.com.ec (04) 6011-380 / 6011-350  7004'' || CHR(13) ||',
'                       ''MANTA Yandry Arteaga  yarteaga@recsa.com.ec (05) 2629-844 EXT. 6161'' || CHR(13) ||',
'                       ''SANTO DOMINGO Susana Borja  sborja@recsa.com.ec (02) 2767-942 / 2767-943  EXT. 6111'';',
'',
'ELSIF ln_num_recsa > 0 THEN',
'  ',
unistr(' :P6_CONTACTOS_NEG  := ''Cr\00E9dito vendido a COBRANZAS DEL ECUADOR (RECSA), por favor contactarse con el personal de servicio al cliente'' || CHR(13) || CHR(13)||'),
'                       ''QUITO Katy Egas kegas@recsa.com.ec (02) 2999-800 / 2983-900  EXT. 9822'' || CHR(13) ||',
'                       ''CUENCA  Thelmo Guzman tguzman@recsa.com.ec (07)2820858  '' || CHR(13) ||',
unistr('                       ''AMBATO  Martha Proa\00F1o mproano@recsa.com.ec (03) 2822-883 / 2822-366  EXT. 6081 '' || CHR(13) ||'),
'                       ''GUAYAQUIL Michel Onofre donofre@recsa.com.ec (04) 6011-380 / 6011-350  7004'' || CHR(13) ||',
'                       ''MANTA Yandry Arteaga  yarteaga@recsa.com.ec (05) 2629-844 EXT. 6161'' || CHR(13) ||',
'                       ''SANTO DOMINGO Susana Borja  sborja@recsa.com.ec (02) 2767-942 / 2767-943  EXT. 6111'';',
'  ',
'END IF;',
'    ',
'',
'',
'SELECT COUNT(*)',
'INTO ln_num_projuridico',
'FROM car_cuentas_por_cobrar cx',
'WHERE cx.ede_id =  ln_ede_id_projuridico',
'and cx.cli_id = :P6_CLI_ID;',
'',
'',
'if  ln_num_projuridico > 0 then',
'  ',
'',
unistr('  :P6_CONTACTOS_NEG := ''Cr\00E9dito  vendido a Projuridico, por favor contactar al  personal de servicio al cliente en las  oficinas de Guayaquil (Lorenzo de Garaicoa Nro. 732 y Victor Manuel Rendon, Ed. Plaza Centenario) ''|| CHR(13) ||'''),
unistr('                        Piso 3, Oficina 32.  Tel\00E9fonos: 04 2598300, Ext. 6000 Contacto: Jessica Granizo'';'),
'',
'end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT ''x''',
'    FROM car_cuentas_por_cobrar  cx,',
'         CAR_CXC_NEGOCIACIONES   CN,',
'         CAR_NEGOCIACIONES       NE',
'   WHERE cx.ede_id IN ( SELECT e.ede_id',
'    FROM asdm_E.Car_Tipo_Negociacion_Entidad  e',
'   WHERE ctn_id = pq_constantes.fn_retorna_constante(0, ''cn_ctn_id_morosa'')',
'   AND e.cte_estado_registro = 0)',
'     AND cx.cxc_saldo > 0',
'     AND cx.cxc_estado = ''GEN''',
'     AND CX.CXC_ESTADO_REGISTRO = 0',
'     AND CLI_ID = :P6_CLI_ID',
'     AND CX.CXC_ID = CN.CNE_CXC_ID',
'     AND CN.CNE_eSTADO_CXC = ''NEGOCIADO''',
'     AND CN.NEG_ID = NE.NEG_ID',
'   GROUP BY cx.cli_id'))
,p_process_when_type=>'EXISTS'
,p_internal_uid=>922029628051598650
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(13465255752291270189)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes_re'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_age_id NUMBER;',
'  ln_zon_id NUMBER;',
'  lv_zona   VARCHAR2(100);',
'',
'  ln_car_id car_abonos_refinanciamiento.car_id%TYPE;',
'',
'BEGIN',
'  :p6_nombre_institucion := NULL;',
'  :p6_ins_id             := NULL;',
'  :p6_cli_id             := NULL;',
'  :p6_agente_cobrador    := NULL;',
'  pq_ven_listas2.pr_datos_clientes(:f_uge_id,',
'                                  :f_emp_id,',
'                                  :p6_identificacion,',
'                                  :p6_nombre,',
'                                  :p6_cli_id,',
'                                  :p6_correo,',
'                                  :p6_direccion,',
'                                  :p6_tipo_direccion,',
'                                  :p6_telefono,',
'                                  :p6_tipo_telefono,',
'                                  :p6_cliente_existe,',
'                                  :p6_dir_id,',
'                                  :p6_cli_id_referido,',
'                                  :p6_cli_id_nombre_referido,',
'                                  :p6_per_tipo_identificacion,',
'                                  :p0_error);',
'',
'  :p6_nombre := REPLACE(:p6_nombre, ''&'', '' '');',
'',
'  -- Yguaman 2011/12/15. Institucion de Cliente',
'  BEGIN',
'    SELECT ains.ins_id, ape.per_razon_social',
'      INTO :p6_ins_id, :p6_nombre_institucion',
'      FROM asdm_clientes_instituciones acin,',
'           asdm_instituciones          ains,',
'           asdm_personas               ape',
'     WHERE acin.cli_id = :p6_cli_id',
'       AND acin.cin_estado_registro = 0',
'       AND acin.ins_id = ains.ins_id',
'       AND ains.per_id = ape.per_id',
'       AND ains.ins_estado_registro = 0',
'       AND ape.per_estado_registro = 0;',
'  ',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      :p6_ins_id             := NULL;',
'      :p6_nombre_institucion := ''NO ASIGNANDO'';',
'    ',
'  END;',
'  /*pq_ven_pagos_cuota.pr_carga_saldos_cliente(:p6_cli_id,',
'                                             :f_emp_id,',
'                                             :p6_total_cheques,',
'                                             :p0_error);*/',
'',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                               ''cv_coleccion_pago_cuota''));',
'',
'  IF :p6_cli_id IS NOT NULL AND',
'     :f_seg_id =',
'     pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_minoreo'') THEN',
'  ',
'    -- Yguaman 2011/11/23 18:16pm, agregado para grabar el cobrador cuando se va a generar la transaccion de pago de cuota',
'    -- Solo para minoreo y cuando es el pago de cuota -- Jandres.',
'  ',
'    /*ln_age_id := pq_car_cobranza.fn_retorna_ata_id_cobrador(pn_emp_id =>:F_EMP_ID ,  -- cdigo empresa',
'    pn_cli_id => :P6_CLI_ID,  -- codigo cliente',
'    pn_uge_id => :F_UGE_ID); -- codigo unidad de gestion*/',
'  ',
'    :p6_agente_cobrador := pq_car_cobranza.fn_retorna_cobrador(pn_emp_id => :f_emp_id, -- cdigo empresa',
'                                                               pn_cli_id => :p6_cli_id, -- codigo cliente',
'                                                               pn_uge_id => :f_uge_id); -- codigo unidad de gestion',
'  ',
'    /*if :P6_AGENTE_COBRADOR is null then',
'      raise_application_error(-20000,''El Cliente: ''||:P6_NOMBRE||'' No tienen asignado un cobrador o una zona.... Debe asignar Cobrador al Cliente para continuar.''); ',
'    end if;*/',
'  ',
'    /*BEGIN',
'    ',
'    ',
'    ',
'    SELECT pe.per_primer_apellido || '' '' || pe.per_segundo_apellido || '' '' ||',
'           pe.per_primer_nombre || '' '' || pe.per_segundo_nombre',
'      INTO :P6_AGENTE_COBRADOR',
'      FROM asdm_agentes_tagentes ata, asdm_agentes age, asdm_personas pe',
'     WHERE ata.ata_id = ln_age_id',
'       AND ata.age_id = age.age_id',
'       AND age.per_id = pe.per_id',
'       AND age.age_estado_registro =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'');',
'    ',
'    ',
'      EXCEPTION',
'    WHEN OTHERS THEN',
'      raise_application_error(-20000,''Debe asignar Cobrador al Cliente para continuar.'');',
'     END;*/',
'  END IF;',
'',
'  :p6_com_id           := NULL;',
'  :p6_valor_pago       := 0;',
'  :p6_saldo_valor_pago := 0;',
'  :p6_cxc_id           := NULL;',
'',
'  BEGIN',
'',
'    pq_ven_pagos_cuota.pr_carga_coll_div_pendientes(pn_cli_id      => :p6_cli_id,',
'                                                    pn_emp_id      => :f_emp_id,',
'                                                    pn_seg_id      => :f_seg_id,',
'                                                    pd_fecha_desde => NULL,',
'                                                    pd_fecha_hasta => NULL,',
'                                                    pn_tfp_id      => :p6_tfp_id,',
'                                                    pv_error       => :p0_error);',
'  ',
'  END;',
'',
'  BEGIN',
'  ',
'    BEGIN',
'    ',
'      SELECT ab.car_id',
'        INTO ln_car_id',
'        FROM car_abonos_refinanciamiento ab',
'       WHERE ab.cxc_id = :p6_cxc_id_ref',
'        and ab.car_estado_registro=0;',
'    ',
'    EXCEPTION',
'      WHEN no_data_found THEN',
'        ln_car_id := NULL;',
'    END;',
'  ',
'    IF ln_car_id IS NULL THEN',
'      pq_car_dml.pr_ins_car_abonos_refin(pn_car_id               => ln_car_id,',
'                                         pn_emp_id               => :f_emp_id,',
'                                         pd_car_fecha            => SYSDATE,',
'                                         pn_car_capital          => :p6_valor_capital_refin,',
'                                         pn_car_intereses        => :p6_valor_refinanciamiento,',
'                                         pn_car_abono            => 0,',
'                                         pn_car_diferencia       => 0,',
'                                         pv_car_estado_registro  => 0,',
'                                         pn_cxc_id               => :p6_cxc_id_ref,',
'                                         pn_car_saldo_cxc_ant    => :p6_saldo_cxc_ref,',
'                                         pn_car_intereses_no_pag => :p6_intereses_no_pag_re,',
'                                         pn_car_abonos_capital   => nvl(pq_car_refin_precancelacion.pr_valida_abono_ant(pn_cxc_id => :p6_cxc_id_ref),',
'                                                                        0),',
'                                         pv_error                => :p0_error);',
'    ',
'    ELSE',
'    ',
'      pq_car_dml.pr_upd_car_abonos_refin(pn_car_id               => ln_car_id,',
'                                         pn_emp_id               => :f_emp_id,',
'                                         pd_car_fecha            => SYSDATE,',
'                                         pn_car_capital          => :p6_valor_capital_refin,',
'                                         pn_car_intereses        => :p6_valor_refinanciamiento,',
'                                         pn_car_abono            => 0,',
'                                         pn_car_diferencia       => 0,',
'                                         pv_car_estado_registro  => 0,',
'                                         pn_cxc_id               => :p6_cxc_id_ref,',
'                                         pn_car_saldo_cxc_ant    => :p6_saldo_cxc_ref,',
'                                         pn_car_intereses_no_pag => :p6_intereses_no_pag_re,',
'                                         pn_car_abonos_capital   => nvl(pq_car_refin_precancelacion.pr_valida_abono_ant(pn_cxc_id => :p6_cxc_id_ref),',
'                                                                        0),',
'                                         pv_error                => :p0_error);',
'    END IF;',
'  ',
'  END;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''cliente'' and :P6_PAGO_REFINANCIAMIENTO = 1'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>13433002601021505263
);
wwv_flow_imp.component_end;
end;
/
