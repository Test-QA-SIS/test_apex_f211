prompt --application/pages/page_00164
begin
--   Manifest
--     PAGE: 00164
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>164
,p_name=>'Simulador del Refinanciamiento'
,p_step_title=>'Simulador del Refinanciamiento'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(384000714435689129)
,p_plug_name=>'Datos Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(384096700053746627)
,p_plug_name=>'Cartera Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli_id,',
'       (SELECT cli.nombre_completo',
'          FROM v_asdm_datos_clientes cli',
'         WHERE cli.cli_id = cxc.cli_id',
'           AND cli.emp_id = cxc.emp_id) nombre,',
'       cxc.cxc_id,',
'       cxc.cxc_saldo,',
'       (SELECT ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cxc.ede_id',
'           AND ede.emp_id = cxc.emp_id) cartera,',
'       (SELECT uge.uge_nombre',
'          FROM asdm_unidades_gestion uge',
'         WHERE uge.uge_id = cxc.uge_id',
'           AND uge.emp_id = cxc.emp_id) unidad_gestion,',
'           cxc.ord_id,',
'           cxc.com_id,',
'      ''CARGAR'' cargar,',
'   CXC.CXC_FECHA_ADICION,',
'   pq_car_refinanciamiento.fn_retorna_credito_seg(pn_cxc_id => cxc.cxc_id,',
'                                                            pn_ord_id => cxc.ord_id) cxc_id_seguro',
'  FROM car_cuentas_por_cobrar cxc',
' WHERE cxc.com_id IS NOT NULL',
'   AND cxc.cli_id IS NOT NULL',
'   AND cxc.cxc_id_refin IS NULL',
'   AND cxc.cxc_saldo > 0',
'   AND cxc.cli_id = :p164_cli_id',
'   AND NOT EXISTS (SELECT col.c002',
'          FROM apex_collections col',
'         WHERE col.collection_name = ''COL_CREDITOS_REFIN''',
'           AND col.c002 = cxc.cxc_id)',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli_id',
'  FROM car_cuentas_por_cobrar cxc',
' WHERE cxc.com_id IS NOT NULL',
'   AND cxc.cli_id IS NOT NULL',
'   AND cxc.cxc_id_refin IS NULL',
'   AND cxc.cxc_saldo > 0',
'   AND cxc.cli_id = :p164_cli_id',
'   AND NOT EXISTS (SELECT col.c002',
'          FROM apex_collections col',
'         WHERE col.collection_name = ''COL_CREDITOS_REFIN''',
'           AND col.c002 = cxc.cxc_id)'))
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(384096798312746627)
,p_name=>'cartera cliente'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>351843647042981701
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379558142037028066)
,p_db_column_name=>'CLI_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379558531864028066)
,p_db_column_name=>'NOMBRE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379561332837028068)
,p_db_column_name=>'ORD_ID'
,p_display_order=>12
,p_column_identifier=>'I'
,p_column_label=>'Orden de venta'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379561732552028068)
,p_db_column_name=>'COM_ID'
,p_display_order=>22
,p_column_identifier=>'J'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379558865977028066)
,p_db_column_name=>'CXC_ID'
,p_display_order=>32
,p_column_identifier=>'C'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379560859329028068)
,p_db_column_name=>'CXC_FECHA_ADICION'
,p_display_order=>42
,p_column_identifier=>'H'
,p_column_label=>unistr('Fecha Adici\00F3n')
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_FECHA_ADICION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379559286347028067)
,p_db_column_name=>'CXC_SALDO'
,p_display_order=>52
,p_column_identifier=>'D'
,p_column_label=>'Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CXC_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379559742916028067)
,p_db_column_name=>'CARTERA'
,p_display_order=>62
,p_column_identifier=>'E'
,p_column_label=>'Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379560084979028067)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>72
,p_column_identifier=>'F'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379560477092028068)
,p_db_column_name=>'CARGAR'
,p_display_order=>82
,p_column_identifier=>'G'
,p_column_label=>'Cargar'
,p_column_link=>'f?p=&APP_ID.:164:&SESSION.:CARGAR:&DEBUG.::P164_CXC_ID,P164_ORD_ID,P164_FECHA_CORTE,P164_COM_ID,P164_CXC_ID_SEGURO:#CXC_ID#,#ORD_ID#,,#COM_ID#,#CXC_ID_SEGURO#'
,p_column_linktext=>'#CARGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CARGAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(379562059770028068)
,p_db_column_name=>'CXC_ID_SEGURO'
,p_display_order=>92
,p_column_identifier=>'K'
,p_column_label=>'Cxc id seguro'
,p_column_link=>'f?p=&APP_ID.:162:&SESSION.::&DEBUG.:RP:P162_CXC_ID_SEGURO,P162_ORD_ID:#CXC_ID_SEGURO#,#ORD_ID#'
,p_column_linktext=>'#CXC_ID_SEGURO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(384097699837746994)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'3473093'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:NOMBRE:ORD_ID:CXC_ID:CXC_ID_SEGURO:CXC_FECHA_ADICION:CXC_SALDO:CARTERA:UNIDAD_GESTION:COM_ID:CARGAR:'
,p_sort_column_1=>'CXC_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(384165402722315796)
,p_name=>'Cartera a Refinanciar'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>25
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001 credito,',
'c002 Nro_Vencimiento,',
'c003 Fecha_Vencimiento,',
'to_number(c004) Valor_Capital,',
'to_number(c005) Valor_Interes,',
'to_number(c006) Valor_Cuota,',
'to_number(c007) Deuda_Presente,',
'to_number(c008) Saldo_Cuota',
'from apex_collections where collection_name = ''COL_CARTERA_REFIN''',
'ORDER BY TO_NUMBER(C002)'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001 credito',
'from apex_collections where collection_name = ''COL_CARTERA_REFIN'''))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379565182979028072)
,p_query_column_id=>1
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>1
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379565608426028072)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379566021004028073)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379566420666028073)
,p_query_column_id=>4
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379566841620028074)
,p_query_column_id=>5
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379567203210028074)
,p_query_column_id=>6
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379567594102028074)
,p_query_column_id=>7
,p_column_alias=>'DEUDA_PRESENTE'
,p_column_display_sequence=>8
,p_column_heading=>'Deuda Presente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379567960387028074)
,p_query_column_id=>8
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(384200620701794017)
,p_name=>'VALORES A REFINANCIAR'
,p_parent_plug_id=>wwv_flow_imp.id(384165402722315796)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT col.seq_id id,',
'       to_number(col.c001) com_id,',
'       to_number(col.c002) cxc_id,',
'       to_number(col.c003) cli_id,',
'       to_number(col.c004) total,',
'       col.c006 uge_id,',
'       col.c006||'' - ''||col.c007 uge_nombre,',
'       ''ELIMINAR'' eliminar',
'  FROM apex_collections col',
' WHERE col.collection_name = ''COL_CREDITOS_REFIN'''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379568688628028076)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379569054649028076)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379569470451028076)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>4
,p_column_heading=>unistr('Cr\00E9dito')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379569886707028076)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379570284193028077)
,p_query_column_id=>5
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>7
,p_column_heading=>'Valor Presente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379570677697028077)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379571099813028077)
,p_query_column_id=>7
,p_column_alias=>'UGE_NOMBRE'
,p_column_display_sequence=>6
,p_column_heading=>unistr('Centro de gesti\00F3n')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379571461783028077)
,p_query_column_id=>8
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>8
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:164:&SESSION.:ELIMINA:&DEBUG.::P164_SEQ_ID:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(384244024033135595)
,p_plug_name=>'Refinanciar'
,p_parent_plug_id=>wwv_flow_imp.id(384200620701794017)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>51
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(587186583485623067)
,p_name=>'DETALLE DEL SEGURO'
,p_parent_plug_id=>wwv_flow_imp.id(384165402722315796)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT s.ose_id,',
'       s.cxc_id_seguro,',
'       s.ose_plazo_cobertura,',
'       s.pse_valor_mensual,',
'       s.cse_id,',
'       s.pse_poliza_seguro,',
'       s.pse_codigo_producto,',
'       s.pse_campania,',
'       s.pse_codigo_paquete,',
'       s.ose_fecha,',
'       cse.cse_fecha_ini,',
'       cse.cse_fecha_fin,',
'       cse.cse_plazo,',
'       cse.cse_fecha_envio,',
'       cse.pse_num_poliza,',
'       cxc.cxc_id,',
'       cxc.cxc_saldo,',
'       cxc.cxc_fecha_adicion',
'  FROM ven_ordenes_seguros    s,',
'       car_cuentas_seguros    cse,',
'       car_cuentas_por_cobrar cxc',
' WHERE s.ord_origen = :p164_ord_id',
'   AND s.cse_id = cse.cse_id',
'   AND cse.cxc_id = cxc.cxc_id',
'   --AND s.cxc_id_venta = :p164_cxc_id',
'   AND s.cxc_id_seguro = :P164_CXC_ID_SEGURO',
'   AND cxc.cxc_saldo > 0;',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT s.ose_id',
'  FROM ven_ordenes_seguros    s,',
'       car_cuentas_seguros    cse,',
'       car_cuentas_por_cobrar cxc',
' WHERE s.ord_origen = :p164_ord_id',
'   AND s.cse_id = cse.cse_id',
'   AND cse.cxc_id = cxc.cxc_id',
'   --AND s.cxc_id_venta = :p164_cxc_id',
'   AND s.cxc_id_seguro = :P164_CXC_ID_SEGURO',
'   AND cxc.cxc_saldo > 0;'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379581235630028088)
,p_query_column_id=>1
,p_column_alias=>'OSE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ose id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379581607711028088)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID_SEGURO'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id seguro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379582030935028088)
,p_query_column_id=>3
,p_column_alias=>'OSE_PLAZO_COBERTURA'
,p_column_display_sequence=>3
,p_column_heading=>'Plazo cobertura'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379582362048028088)
,p_query_column_id=>4
,p_column_alias=>'PSE_VALOR_MENSUAL'
,p_column_display_sequence=>4
,p_column_heading=>'Valor mensual'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379582845641028089)
,p_query_column_id=>5
,p_column_alias=>'CSE_ID'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379583215132028089)
,p_query_column_id=>6
,p_column_alias=>'PSE_POLIZA_SEGURO'
,p_column_display_sequence=>7
,p_column_heading=>unistr('P\00F3liza seguro')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379583570596028089)
,p_query_column_id=>7
,p_column_alias=>'PSE_CODIGO_PRODUCTO'
,p_column_display_sequence=>8
,p_column_heading=>'Producto'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379584023339028089)
,p_query_column_id=>8
,p_column_alias=>'PSE_CAMPANIA'
,p_column_display_sequence=>9
,p_column_heading=>unistr('Campa\00F1a')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379584356700028089)
,p_query_column_id=>9
,p_column_alias=>'PSE_CODIGO_PAQUETE'
,p_column_display_sequence=>10
,p_column_heading=>'Paquete'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379584845722028090)
,p_query_column_id=>10
,p_column_alias=>'OSE_FECHA'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379585227782028090)
,p_query_column_id=>11
,p_column_alias=>'CSE_FECHA_INI'
,p_column_display_sequence=>12
,p_column_heading=>'Fecha inicial'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379585592849028090)
,p_query_column_id=>12
,p_column_alias=>'CSE_FECHA_FIN'
,p_column_display_sequence=>13
,p_column_heading=>'Fecha final'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379586045061028090)
,p_query_column_id=>13
,p_column_alias=>'CSE_PLAZO'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379586445523028090)
,p_query_column_id=>14
,p_column_alias=>'CSE_FECHA_ENVIO'
,p_column_display_sequence=>15
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379586817752028091)
,p_query_column_id=>15
,p_column_alias=>'PSE_NUM_POLIZA'
,p_column_display_sequence=>16
,p_column_heading=>unistr('Num p\00F3liza')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379587206686028091)
,p_query_column_id=>16
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379587566198028091)
,p_query_column_id=>17
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>5
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379588041257028091)
,p_query_column_id=>18
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(389750405187699350)
,p_name=>'prueba ddd'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CXC_ID,',
'               CRE_NRO_VENCIMIENTO,',
'               CRE_FECHA_VENCIMIENTO,',
'               CRE_VALOR_CAPITAL,',
'               CRE_VALOR_INTERES,',
'               CRE_VALOR_COUTA,',
'               CRE_DEUDA,',
'               CRE_SALDO_CUOTA',
'          FROM CAR_CARTERA_REFINANCIAMIENTO',
'         WHERE CXC_ID = :p164_CXC_ID;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379588684757028092)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379589140505028093)
,p_query_column_id=>2
,p_column_alias=>'CRE_NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Cre Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379589542783028093)
,p_query_column_id=>3
,p_column_alias=>'CRE_FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Cre Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379589853582028093)
,p_query_column_id=>4
,p_column_alias=>'CRE_VALOR_CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Cre Valor Capital'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379590280269028093)
,p_query_column_id=>5
,p_column_alias=>'CRE_VALOR_INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Cre Valor Interes'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379590681393028094)
,p_query_column_id=>6
,p_column_alias=>'CRE_VALOR_COUTA'
,p_column_display_sequence=>6
,p_column_heading=>'Cre Valor Couta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379591056236028094)
,p_query_column_id=>7
,p_column_alias=>'CRE_DEUDA'
,p_column_display_sequence=>7
,p_column_heading=>'Cre Deuda'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379591549290028094)
,p_query_column_id=>8
,p_column_alias=>'CRE_SALDO_CUOTA'
,p_column_display_sequence=>8
,p_column_heading=>'Cre Saldo Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(546609053242675670)
,p_name=>'PRUEBA FECHA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    SELECT to_date(c.c002, ''dd/mm/yyyy'')',
'      FROM apex_collections c',
'     WHERE c.collection_name = ''COLL_FECHAS_CORTE''',
'       AND rownum = 1;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379592231660028095)
,p_query_column_id=>1
,p_column_alias=>'TO_DATE(C.C002,''DD/MM/YYYY'')'
,p_column_display_sequence=>1
,p_column_heading=>'To date(c.c002,&#x27;dd&#x2F;mm&#x2F;yyyy&#x27;)'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(587188558485623087)
,p_name=>'CRONOGRAMA DE CUOTAS PARA SEGURO'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.seq_id, c.c001, c.c002, TO_NUMBER(c.c003) c003, c.c004, c.c005, c.c006, c.c007',
'  FROM apex_collections c',
' WHERE c.collection_name = ''COL_COBERTURA'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379592854093028100)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379593341366028100)
,p_query_column_id=>2
,p_column_alias=>'C001'
,p_column_display_sequence=>2
,p_column_heading=>'Plazo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379593673085028100)
,p_query_column_id=>3
,p_column_alias=>'C002'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379594095711028100)
,p_query_column_id=>4
,p_column_alias=>'C003'
,p_column_display_sequence=>4
,p_column_heading=>'Valor mensual'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379594477680028101)
,p_query_column_id=>5
,p_column_alias=>'C004'
,p_column_display_sequence=>5
,p_column_heading=>'pse_id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379594927463028101)
,p_query_column_id=>6
,p_column_alias=>'C005'
,p_column_display_sequence=>6
,p_column_heading=>'Origen'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379595340541028101)
,p_query_column_id=>7
,p_column_alias=>'C006'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379595741945028101)
,p_query_column_id=>8
,p_column_alias=>'C007'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(587189427498623096)
,p_name=>'Cronograma seguro gen PRUEBA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT c.seq_id,',
'        c.c001,',
'        c.c002,',
'        TO_NUMBER(c.c003) c003,',
'        c.c004,',
'        c.c005,',
'        c.c006,',
'        TO_NUMBER(c.c007) c007,',
'        c.c008',
'   FROM apex_collections c',
'  WHERE c.collection_name = ''COL_COBERTURA_SEG'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379596393056028102)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379596840458028102)
,p_query_column_id=>2
,p_column_alias=>'C001'
,p_column_display_sequence=>2
,p_column_heading=>'Plazo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379597228739028102)
,p_query_column_id=>3
,p_column_alias=>'C002'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379597569479028102)
,p_query_column_id=>4
,p_column_alias=>'C003'
,p_column_display_sequence=>4
,p_column_heading=>'Valor mensual'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379597976592028103)
,p_query_column_id=>5
,p_column_alias=>'C004'
,p_column_display_sequence=>5
,p_column_heading=>'PSE_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379598383420028103)
,p_query_column_id=>6
,p_column_alias=>'C005'
,p_column_display_sequence=>6
,p_column_heading=>'oRDEN DE VENTA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379598791752028103)
,p_query_column_id=>7
,p_column_alias=>'C006'
,p_column_display_sequence=>7
,p_column_heading=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379599231827028103)
,p_query_column_id=>8
,p_column_alias=>'C007'
,p_column_display_sequence=>8
,p_column_heading=>'SALDO CAPITAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379599592834028104)
,p_query_column_id=>9
,p_column_alias=>'C008'
,p_column_display_sequence=>9
,p_column_heading=>'PORC CAPITAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(588661842180492198)
,p_plug_name=>'RESULTADOS REFINANCIAMIENTO'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'C001',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(389824002770174471)
,p_name=>unistr('CUOTAS NUEVO CR\00C9DITO')
,p_parent_plug_id=>wwv_flow_imp.id(588661842180492198)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'C001 Nro_Vencimiento,',
'c002 Fecha_Vencimiento,',
'to_number(c003) Valor_Capital,',
'to_number(c004) Valor_Interes,',
'to_number(c005) Valor_Cuota',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_display_when_condition=>':P164_EDE_ID_CARTERA=''MAR'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>30
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379604090302028107)
,p_query_column_id=>1
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379604536506028107)
,p_query_column_id=>2
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379604873128028108)
,p_query_column_id=>3
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379605261501028108)
,p_query_column_id=>4
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379605694841028108)
,p_query_column_id=>5
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(390029424801638526)
,p_plug_name=>'Valores Credito'
,p_parent_plug_id=>wwv_flow_imp.id(389824002770174471)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(390319701573097878)
,p_plug_name=>'Autorizacion'
,p_parent_plug_id=>wwv_flow_imp.id(588661842180492198)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>120
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(587190469231623106)
,p_name=>'NUEVO CRONOGRAMA DE CUOTAS PARA SEGURO'
,p_parent_plug_id=>wwv_flow_imp.id(588661842180492198)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT a.seq_id seq_id,',
'             a.c001   nro_vencimiento,',
'             a.c002   fecha_vencimiento,',
'             to_number(a.c003)   capital,',
'             to_number(a.c004)   interes,',
'             a.c006   saldo_capital,',
'             to_number(a.c007)   valor_cuota,',
'             a.c008   descuento_rol',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''COL_COBERTURA_SEG'';',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT a.seq_id seq_id',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''COL_COBERTURA_SEG'';'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>30
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379600586983028104)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379600955304028105)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Nro vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379601389345028105)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379601834555028105)
,p_query_column_id=>4
,p_column_alias=>'CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Capital'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379602242979028105)
,p_query_column_id=>5
,p_column_alias=>'INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Interes'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379602596069028105)
,p_query_column_id=>6
,p_column_alias=>'SALDO_CAPITAL'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379602968945028106)
,p_query_column_id=>7
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Valor cuota'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379603360890028106)
,p_query_column_id=>8
,p_column_alias=>'DESCUENTO_ROL'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(588662167405492201)
,p_plug_name=>'COMPROBANTES DEL INTERES DIFERIDO'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>35
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(404650414270117214)
,p_name=>unistr('Interes Diferido No Cobrado - Nota de D\00E9bito')
,p_parent_plug_id=>wwv_flow_imp.id(588662167405492201)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''ND'''', this.value ,''''P164_UGE_ID'''',''''P164_USER_ID'''',''''P164_EMP_ID'''',''''P164_CLI_ID'''',''''R25127418309089251'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'');}'';'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379524021855028015)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379524445096028017)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379524808948028017)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379525187142028017)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379525633700028017)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379526006948028017)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Int. Dif'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379526383046028018)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P164_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379526796967028018)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P164_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379527191308028018)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P164_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379527558570028018)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379528005025028018)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379528418706028019)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379528769544028019)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379529152786028019)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379529572516028019)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379529969160028019)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379530406977028019)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379530825474028020)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379531161401028034)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379531604908028035)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379532010389028035)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379532436738028035)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379532781731028035)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379533165200028036)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379533575621028036)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379534016500028036)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379534391484028036)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379534824444028036)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379535209045028037)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379535568583028037)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379536004628028037)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379536387699028037)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379536778889028037)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379537219792028038)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379537606012028038)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379538001670028038)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379538364184028038)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379538831571028038)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379539240989028039)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379539593273028039)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379539973597028039)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379540369349028039)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379540807426028039)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379541229595028040)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379541621562028040)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379542013936028040)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379542400704028040)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379542810621028040)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379543228756028041)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379543562351028041)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379544042149028041)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379544367362028041)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379544775390028042)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379545178980028042)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379545596841028042)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379545969976028042)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379546423793028047)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379546831407028047)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379547171601028047)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379547587571028047)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(510729725879751330)
,p_name=>unistr('Interes Diferido  - Nota de Cr\00E9dito')
,p_parent_plug_id=>wwv_flow_imp.id(588662167405492201)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''NC'''', this.value ,''''P164_UGE_ID'''',''''P164_USER_ID'''',''''P164_EMP_ID'''',''''P164_CLI_ID'''',''''R131206729918723367'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_display_when_condition=>'NVL(:P164_INTERESES_NO_PAG,0) > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379548711893028050)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379549138395028050)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379549545875028051)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379549866393028051)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379550294669028052)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379550698317028052)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INT DIF REFINANCIADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379551141574028052)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>9
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P164_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379551480779028052)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>7
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P164_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(379551944007028052)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>8
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P164_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(379572607374028078)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_button_name=>'pago_cuota'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'>> Nuevo Pago Cuota'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:7:&SESSION.:CLIENTE_REFIN:&DEBUG.:RP,7:P7_NRO_IDE_REF,P7_CXC_ID_REF:&P164_IDENTIFICACION.,&P164_CXC_ID.'
,p_button_condition=>':P164_EDE_ID_CARTERA=''MAR'' AND :P164_CAR_CAPITAL IS NOT NULL AND :P164_VALOR_A_PAGAR_CAJA!=0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(379573021295028078)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_button_name=>'CALCULAR_CRONOGRA'
,p_button_static_id=>'CALCULAR_CRONOGRA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536254089046676)
,p_button_image_alt=>'Calcular'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(379552585513028053)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_button_name=>'Crear_Garante'
,p_button_static_id=>'Crear_Garante'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Crear Garante'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=147:44:&SESSION.::&DEBUG.::F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_POPUP,P44_PAGINA,P44_APLICACION:&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,N,211,102'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(379573355966028078)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_button_name=>'Ir_pago_cuota'
,p_button_static_id=>'Ir_pago_cuota'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'>> Ir Pago Cuota'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.:cliente:&DEBUG.::P6_PER_TIPO_IDENTIFICACION,P6_IDENTIFICACION,P6_PAGO_REFINANCIAMIENTO,P6_VALOR_REFINANCIAMIENTO,P6_VALOR_CAPITAL_REFIN,P6_CXC_ID_REF,P6_SALDO_CXC_REF,P6_INTERESES_NO_PAG_RE:&P164_TIPO_IDENTIFICACION.,&P164_IDENTIFICACION.,1,&P164_VALOR_A_PAGAR_CAJA.,&P164_VALOR_CAPITAL.,&P164_CXC_ID.,&P164_SALDO_CXC_ORIGEN.,&P164_INTERESES_NO_PAG.'
,p_button_condition_type=>'NEVER'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(379615212320028121)
,p_branch_name=>'Go To Page 164'
,p_branch_action=>'f?p=&APP_ID.:164:&SESSION.:graba:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>5
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 20-FEB-2013 15:13 by JORDONEZ'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(379614772489028119)
,p_branch_action=>'f?p=&APP_ID.:164:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 12-DEC-2012 08:54 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(376305799808505241)
,p_name=>'P164_DIAS_MORA'
,p_item_sequence=>276
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_item_default=>'90'
,p_prompt=>unistr('Par\00E1metro de d\00EDas de Mora')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'onchange="doSubmit()"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(376305869046505242)
,p_name=>'P164_PAR_MESES_GRACIA'
,p_item_sequence=>286
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_item_default=>'0'
,p_prompt=>unistr('Par\00E1metro de meses de gracia')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'onchange="doSubmit()"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(376306096856505244)
,p_name=>'P164_ING_ABONO'
,p_item_sequence=>286
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_item_default=>'0'
,p_prompt=>'Ingrese valor abono:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379547960150028048)
,p_name=>'P164_INT_MORA_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(404650414270117214)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379552980378028054)
,p_name=>'P164_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379553353235028054)
,p_name=>'P164_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P164_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P164_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379553764022028054)
,p_name=>'P164_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379554160996028055)
,p_name=>'P164_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379554562278028055)
,p_name=>'P164_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379555024682028055)
,p_name=>'P164_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379555365509028055)
,p_name=>'P164_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Telofono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379555773748028055)
,p_name=>'P164_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379556231688028056)
,p_name=>'P164_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379556563821028056)
,p_name=>'P164_GARANTE'
,p_item_sequence=>186
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>'Garante'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>60
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379557029772028056)
,p_name=>'P164_CALIFICACION_VALIDA'
,p_item_sequence=>256
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_prompt=>unistr('D\00EDas Mora')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round(SYSDATE-MIN(DIV.DIV_FECHA_VENCIMIENTO),0)',
'        FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_DIVIDENDOS DIV',
'       WHERE CXC.CXC_ID = DIV.CXC_ID',
'         AND CXC.CLI_ID =:P164_CLI_ID',
'         AND CXC.CXC_ID=:P164_CXC_ID',
'         AND DIV.DIV_SALDO_CUOTA != 0',
'AND cxc.cxc_estado!=''TMP'';'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379557352479028056)
,p_name=>'P164_CXC_NC'
,p_item_sequence=>266
,p_item_plug_id=>wwv_flow_imp.id(384000714435689129)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379562946893028071)
,p_name=>'P164_COM_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(384096700053746627)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379563269930028071)
,p_name=>'P164_CXC_ID_SEGURO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(384096700053746627)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379563721078028071)
,p_name=>'P164_ORD_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(384096700053746627)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379564071779028071)
,p_name=>'P164_CXC_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(384096700053746627)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379564497807028072)
,p_name=>'P164_EDE_ID_CARTERA'
,p_item_sequence=>216
,p_item_plug_id=>wwv_flow_imp.id(384096700053746627)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379571888780028077)
,p_name=>'P164_SEQ_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(384200620701794017)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379573798908028079)
,p_name=>'P164_ALERTA_ABONO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_prompt=>' '
,p_source=>'''Para realizar el refinanciamiento debe cancelar: '''
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:red;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379574175591028079)
,p_name=>'P164_VALOR_CAPITAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Pendiente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379574589450028079)
,p_name=>'P164_DIFERENCIA'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_prompt=>'Valor Abonado'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT AB.CAR_ABONO',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = :p164_cxc_id',
'   AND ab.car_estado_registro = 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379574991106028079)
,p_name=>'P164_VALOR_ABONADO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Diferencia Abono Intereses'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (AB.CAR_ABONO - AB.CAR_INTERESES) < 0 THEN',
'          0',
'         ELSE',
'         ',
'          (AB.CAR_ABONO - AB.CAR_INTERESES)',
'       END ',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = :p164_cxc_id',
'   AND ab.car_estado_registro = 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ab.car_abono-ab.car_intereses--(ab.car_diferencia  + ab.car_abonos_capital)',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = 1827941',
'   AND ab.car_estado_registro = 0;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379575426178028080)
,p_name=>'P164_VALOR_A_PAGAR_CAJA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_source=>'RETURN pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p164_cxc_id)'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379575838630028081)
,p_name=>'P164_TOTAL_FINANCIAR'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Refinanciar'
,p_source=>':P164_VALOR_CAPITAL - NVL(:P164_VALOR_ABONADO,0)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379576199711028081)
,p_name=>'P164_TASA'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_item_default=>'RETURN ROUND(pq_car_refinanciamiento.fn_retorna_tasa_refin(pn_com_id => :p164_com_id),4);'
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tasa Interes'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-BOTTOM'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN ROUND(pq_car_refin_precancelacion.fn_retorna_tasa_refin(:p0_Error),4);',
''))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379576572750028082)
,p_name=>'P164_PLAZO_APLICA'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379577049930028082)
,p_name=>'P164_PLAZO'
,p_item_sequence=>126
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_prompt=>'Plazo Credito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) PLAZO, TO_NUMBER(PEN_VALOR) ID',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 1 AND :P164_PLAZO_APLICA',
' ORDER BY 1;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379577386488028082)
,p_name=>'P164_FECHA_CORTE'
,p_item_sequence=>136
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_prompt=>'Fecha Corte'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT C002',
'       ',
'        FROM apex_collections c',
'       WHERE c.collection_name = ''COLL_FECHAS_CORTE'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'READONLY style="font-size:14;color:BLACK";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379577799136028086)
,p_name=>'P164_VALOR_CAJA'
,p_item_sequence=>176
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Caja'
,p_source=>'return pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p164_cxc_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379578159279028086)
,p_name=>'P164_SALDO_CXC_ORIGEN'
,p_item_sequence=>196
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_saldo saldo_cxc_origen',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cx.cxc_id = :p164_cxc_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379578551373028086)
,p_name=>'P164_INTERESES_NO_PAG'
,p_item_sequence=>206
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Intereses No Pag'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(INTERESES_NO_PAG) from',
'(SELECT CASE',
'         WHEN ((di.div_valor_cuota) - (di.div_saldo_cuota)) >=',
'              (di.div_valor_interes) THEN',
'          0',
'         ELSE',
'          (di.div_valor_interes) -',
'          ((di.div_valor_cuota) - (di.div_saldo_cuota))',
'       END INTERESES_NO_PAG',
'  FROM car_dividendos di',
' WHERE di.cxc_id = :p164_cxc_id',
'   AND di.div_estado_dividendo = ''P''',
'   AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'   AND di.div_estado_registro =',
'       asdm_p.pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo''));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'''disabled'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379578995975028086)
,p_name=>'P164_PUE_ID'
,p_item_sequence=>226
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379579358274028086)
,p_name=>'P164_VALIDA_PUNTO'
,p_item_sequence=>236
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379579651391028087)
,p_name=>'P164_PUE_NUM_SRI'
,p_item_sequence=>246
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379580132012028087)
,p_name=>'P164_MESES_GRACIA'
,p_item_sequence=>266
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_prompt=>'Meses Gracia'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_MESES_GRACIA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) ||'' MESES'' D, TO_NUMBER(PEN_VALOR) R',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 0 AND',
'       (SELECT PMG.PMG_MESES_GRACIA D',
'          FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_PARAMETROS_MESES_GRACIA PMG',
'         WHERE CXC.CXC_ID = :P102_CXC_ID',
'           AND CXC.UGE_ID = PMG.UGE_ID',
'           AND PMG.PMG_ESTADO_REGISTRO=0)',
' ORDER BY 1'))
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379580545250028087)
,p_name=>'P164_CAR_CAPITAL'
,p_item_sequence=>276
,p_item_plug_id=>wwv_flow_imp.id(384244024033135595)
,p_prompt=>'Capital'
,p_source=>'SELECT A.CAR_CAPITAL FROM CAR_ABONOS_REFINANCIAMIENTO A WHERE A.CXC_ID = :P164_CXC_ID AND A.CAR_ESTADO_REGISTRO=0;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379606388914028108)
,p_name=>'P164_ALERTA_2'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(390029424801638526)
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:20;color:red;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379607520407028109)
,p_name=>'P164_TOTAL_CREDITO_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(390319701573097878)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Credito'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(to_number(c005)) total_credito',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379607947940028109)
,p_name=>'P164_CLAVE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(390319701573097878)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379608333284028111)
,p_name=>'P164_ALERTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(390319701573097878)
,p_item_default=>'Solicitar Clave de Autorizacion al Departamento de Cartera'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379608701952028112)
,p_name=>'P164_EMP_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(390319701573097878)
,p_source=>'f_emp_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379609130970028112)
,p_name=>'P164_UGE_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(390319701573097878)
,p_source=>'f_uge_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(379609505777028112)
,p_name=>'P164_USER_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(390319701573097878)
,p_source=>'f_user_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379614349429028118)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_cliente_existe      VARCHAR2(100);',
'  ln_dir_id              asdm_direcciones.dir_id%TYPE;',
'  lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'  ln_cli_id_referido     asdm_clientes.cli_id%TYPE;',
'',
'BEGIN',
'',
'  :p164_calificacion_valida := NULL;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_CREDITOS_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'  ',
'  END IF;',
'',
'  pq_ven_listas2.pr_datos_clientes(:f_uge_id,',
'                                   :f_emp_id,',
'                                   :p164_identificacion,',
'                                   :p164_nombre,',
'                                   :p164_cli_id,',
'                                   :p164_correo,',
'                                   :p164_direccion,',
'                                   :p164_tipo_direccion,',
'                                   :p164_telefono,',
'                                   :p164_tipo_telefono,',
'                                   lv_cliente_existe,',
'                                   ln_dir_id,',
'                                   ln_cli_id_referido,',
'                                   lv_nombre_cli_referido,',
'                                   :p164_tipo_identificacion,',
'                                   :p0_error);',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(pq_constantes.fn_retorna_constante(NULL,',
'                                                                          ''cv_col_nc_refinanciamiento'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                              ''cv_col_nc_refinanciamiento''));',
'  ',
'  END IF;',
'',
'  :p164_alerta_2 := '' '';',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'REFINNC'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>347361198159263192
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379609851560028115)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_cliente_existe      VARCHAR2(100);',
'  ln_dir_id              asdm_direcciones.dir_id%TYPE;',
'  lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'  ln_cli_id_referido     asdm_clientes.cli_id%TYPE;',
'',
'BEGIN',
'  :p164_calificacion_valida := NULL;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_CREDITOS_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'  ',
'  END IF;',
'',
'  pq_ven_listas2.pr_datos_clientes(:f_uge_id,',
'                                   :f_emp_id,',
'                                   :p164_identificacion,',
'                                   :p164_nombre,',
'                                   :p164_cli_id,',
'                                   :p164_correo,',
'                                   :p164_direccion,',
'                                   :p164_tipo_direccion,',
'                                   :p164_telefono,',
'                                   :p164_tipo_telefono,',
'                                   lv_cliente_existe,',
'                                   ln_dir_id,',
'                                   ln_cli_id_referido,',
'                                   lv_nombre_cli_referido,',
'                                   :p164_tipo_identificacion,',
'                                   :p0_error);',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(pq_constantes.fn_retorna_constante(NULL,',
'                                                                          ''cv_col_nc_refinanciamiento'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                              ''cv_col_nc_refinanciamiento''));',
'  ',
'  END IF;',
'',
'  :p164_alerta_2 := '' '';',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>347356700290263189
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379611149350028116)
,p_process_sequence=>11
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_cro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_age_id_agencia     NUMBER;',
'  ld_fecha_corte        DATE;',
'  a                     NUMBER;',
'  b                     NUMBER;',
'  ln_cuota              NUMBER(15, 2) := 0;',
'  ln_cuota_minima       NUMBER;',
'  ln_plazo_cxc_original NUMBER;',
'  ln_com_id             NUMBER;',
'  lv_pue_sri            VARCHAR2(5);',
'  lv_uge_sri            VARCHAR2(5);',
'  ln_int_nd_refi        NUMBER;',
'  lv_estado_reg_activo  VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                          ''cv_estado_reg_activo'');',
'',
'  lv_div_estado_div_pendi VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                            ''cv_div_estado_div_pendiente'');',
'',
'  ln_cas_id           NUMBER;',
'  ln_intereses_no_pag NUMBER;',
'  ln_intereses        NUMBER;',
'BEGIN',
'',
'  SELECT s.cas_intereses',
'    INTO ln_intereses',
'    FROM car_abonos_simulador s',
'   WHERE s.cxc_id = :P164_cxc_id;',
'',
'  IF :P164_ing_abono >= ln_intereses THEN',
'  ',
'    UPDATE car_abonos_simulador s',
'       SET s.cas_abono      = :P164_ing_abono,',
'           s.cas_diferencia = :P164_ing_abono - s.cas_intereses',
'     WHERE s.cxc_id = :P164_cxc_id;',
'  ',
'    SELECT ab.cas_id, ab.cas_intereses_no_pag',
'      INTO ln_cas_id, ln_intereses_no_pag',
'      FROM car_abonos_simulador ab',
'     WHERE ab.cxc_id = :P164_cxc_id',
'       AND ab.cas_estado_registro = 0;',
'  ',
'    IF ln_intereses_no_pag = 0 THEN',
'      SELECT nvl(SUM(di.div_valor_interes), 0) intereses_no_pag',
'        INTO ln_intereses_no_pag',
'        FROM car_dividendos di',
'       WHERE di.cxc_id = :P164_cxc_id',
'         AND di.div_estado_dividendo = ''P''',
'         AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'         AND di.div_estado_registro =',
'             pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'    ',
'      UPDATE car_abonos_simulador re',
'         SET re.cas_intereses_no_pag = ln_intereses_no_pag',
'       WHERE re.cas_id = ln_cas_id;',
'    ',
'    END IF;',
'  ',
'    --- obtenemos el numero de dividendos pendientes de pago  ',
'  ',
'    SELECT COUNT(di.div_id)',
'      INTO ln_plazo_cxc_original',
'      FROM car_dividendos di',
'     WHERE di.cxc_id = :P164_cxc_id',
'       AND di.div_estado_dividendo = lv_div_estado_div_pendi',
'       AND di.div_estado_registro = lv_estado_reg_activo',
'       AND di.div_saldo_cuota > 0',
'     GROUP BY di.cxc_id;',
'  ',
'    IF ln_plazo_cxc_original < to_number(:P164_plazo) THEN',
'    ',
'      SELECT age.age_id',
'        INTO ln_age_id_agencia',
'        FROM asdm_agencias age',
'       WHERE age.uge_id = 188; --:f_uge_id;',
'    ',
'      ln_cuota := pq_car_refin_precancelacion.fn_valor_cuota(tasa => :P164_tasa,',
'                                                             nper => :P164_plazo,',
'                                                             va   => :P164_total_financiar);',
'    ',
'      IF 2 = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'') THEN',
'      ',
'        ln_cuota_minima := to_number(pq_car_general.fn_obtener_parametro(pn_par_id => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                         ''cn_par_id_cuota_minima_minoreo''),',
'                                                                         pn_ede_id => NULL,',
'                                                                         pn_neg_id => NULL,',
'                                                                         pn_emp_id => :f_emp_id));',
'      ',
'        IF ln_cuota < ln_cuota_minima THEN',
'        ',
'          :P164_alerta_2 := ''La Valor de la Cuota NO debe ser menor a '' ||',
'                           ln_cuota_minima || '' dolares'';',
'        ',
'        ELSE',
'        ',
'          :P164_alerta_2 := '' '';',
'        ',
'        END IF;',
'      ',
'      ELSE',
'      ',
'        :P164_alerta_2 := '' '';',
'      ',
'      END IF;',
'    ',
'      pq_car_refin_precancelacion.pr_cronograma_refin(pn_emp_id       => :f_emp_id,',
'                                                      pn_total_ref    => :P164_total_financiar,',
'                                                      pn_plazo        => :P164_plazo,',
'                                                      pn_tasa         => :P164_tasa,',
'                                                      pn_age_id       => ln_age_id_agencia,',
'                                                      pn_cli_id       => :P164_cli_id,',
'                                                      pn_tse_id       => 2, --:f_seg_id,',
'                                                      pn_meses_gracia => nvl(:P164_par_meses_gracia,',
'                                                                             0), --:P17_MESES_GRACIA,',
'                                                      pv_error        => :p0_error);',
'    ',
'      ----- cargo coleccion de intereses para la nueva nota de CREDITO',
'    ',
'      SELECT com_id',
'        INTO ln_com_id',
'        FROM car_cuentas_por_cobrar x',
'       WHERE x.cxc_id = :P164_cxc_id;',
'    ',
'      SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'        INTO lv_pue_sri, lv_uge_sri',
'        FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'       WHERE pu.uge_id = ug.uge_id',
'         AND pu.pue_id = 689; --:p17_pue_id;',
'    ',
'      SELECT SUM(to_number(c004)) valor_interes',
'        INTO ln_int_nd_refi',
'        FROM apex_collections',
'       WHERE collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'    ',
'      pq_car_refin_precancelacion.pr_carga_coleccion_nd_ref(pn_cli_id        => :P164_cli_id,',
'                                                            pn_com_id        => ln_com_id,',
'                                                            pn_emp_id        => :f_emp_id,',
'                                                            pn_cxc_id        => :P164_cxc_id,',
'                                                            pn_valor_interes => ln_int_nd_refi,',
'                                                            pn_pue_sri       => lv_pue_sri,',
'                                                            pn_uge_sri       => lv_uge_sri,',
'                                                            pv_error         => :p0_error);',
'    ',
'    ELSE',
'    ',
'      :P164_alerta_2 := ''El nuevo plazo de refinanciamiento debe ser MAYOR al # de Dividendos Pendientes de Pago'';',
'    ',
'    END IF;',
'  ',
'  ELSE',
'  ',
'    :p0_error := ''El valor abonado tiene que ser mayor o igual al valor pendiente de pago'';',
'  END IF;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error'
,p_process_when_button_id=>wwv_flow_imp.id(379573021295028078)
,p_internal_uid=>347357998080263190
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379613895709028118)
,p_process_sequence=>21
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_cronograma_seg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ld_fecha_corte DATE;',
'BEGIN',
'',
'  BEGIN',
'    SELECT to_date(c.c002, ''dd/mm/yyyy'')',
'      INTO ld_fecha_corte',
'      FROM apex_collections c',
'     WHERE c.collection_name = ''COLL_FECHAS_CORTE''',
'       AND rownum = 1;',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ld_fecha_corte := NULL;',
'  END;',
'  IF ld_fecha_corte IS NOT NULL THEN',
'    pq_car_refinanciamiento.pr_calcula_nuevo_cronog_seg(pn_ord_id        => :p164_ord_id,',
'                                                        pn_cxc_id_seguro => :p164_cxc_id_seguro,',
'                                                        pn_plazo         => :p164_plazo,',
'                                                        pd_fecha_corte   => ld_fecha_corte,',
'                                                        pv_error         => :p0_error);',
'  ',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(379573021295028078)
,p_internal_uid=>347360744439263192
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379610347548028115)
,p_process_sequence=>11
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'  ln_ede_id       asdm_entidades_destinos.ede_id%TYPE;',
'  lv_abreviatura  asdm_entidades_destinos.ede_abreviatura%TYPE;',
'  ln_int_nd_refi  NUMBER;',
'',
'  ln_intereses_no_pag NUMBER;',
'  ln_cas_id           car_abonos_simulador.cas_id%TYPE;',
'  ln_saldo_ant        car_cuentas_por_cobrar.cxc_saldo%TYPE;',
'  ln_valor_capital    car_abonos_simulador.cas_capital%TYPE;',
'  ln_valor_interes    car_abonos_simulador.cas_intereses%TYPE;',
'  ln_cas_abono        NUMBER;',
'  ln_par_dias_mora    NUMBER;',
'  ln_uge_id           NUMBER;',
'  lv_uge_nombre       asdm_unidades_gestion.uge_nombre%TYPE;',
'  ln_dias_mora        NUMBER;',
'BEGIN',
'',
'  ln_par_dias_mora := :P164_DIAS_MORA;',
'',
'',
'  IF ln_par_dias_mora = 0 THEN',
'    :p0_error := ''DEBE INGRESAR EL NUMERO DE DIAS DE MORA'';',
'    raise_application_error(-20000,',
'                            ''DEBE INGRESAR EL NUMERO DE DIAS DE MORA'');',
'  ELSE',
'  ',
'    SELECT MAX((SELECT round(SYSDATE - MIN(div.div_fecha_vencimiento), 0)',
'                 FROM car_cuentas_por_cobrar cxc, car_dividendos div',
'                WHERE cxc.cxc_id = div.cxc_id',
'                  AND cxc.cli_id = cxc1.cli_id',
'                  AND cxc.cxc_id = cxc1.cxc_id',
'                  AND div.div_saldo_cuota != 0',
'                  AND cxc.cxc_estado != ''TMP''))',
'      INTO ln_dias_mora',
'      FROM car_cuentas_por_cobrar cxc1',
'     WHERE cxc1.cli_id = :p164_cli_id',
'       AND cxc1.cxc_saldo > 0',
'       AND cxc1.cxc_estado = ''GEN'';',
'  ',
'    -----INICIA VALIDACION----------------',
'  ',
'    IF ln_dias_mora < ln_par_dias_mora THEN',
'    ',
'      DELETE FROM car_abonos_simulador ab',
'       WHERE ab.cxc_id = :p164_cxc_id',
'         AND ab.cas_estado_registro = 0;',
'    ',
'      :p164_alerta_abono := NULL;',
'    ',
'      SELECT com_id, ede_id',
'        INTO ln_com_id, ln_ede_id',
'        FROM car_cuentas_por_cobrar x',
'       WHERE x.cxc_id = :p164_cxc_id;',
'    ',
'      SELECT ede.ede_abreviatura',
'        INTO lv_abreviatura',
'        FROM asdm_entidades_destinos ede',
'       WHERE ede.ede_id = ln_ede_id;',
'      :p164_ede_id_cartera := lv_abreviatura;',
'      ----- obtengo los datos del periodo de caja',
'    ',
'      /*   PQ_VEN_MOVIMIENTOS_CAJA.PR_ABRIR_CAJA(PN_USU_ID => :F_USER_ID,',
'      PN_UGE_ID => :F_UGE_ID,',
'      PN_EMP_ID => :F_EMP_ID,',
'      PN_PUE_ID => LN_PUE_ID,',
'      PN_PCA_ID => LN_PCA_ID,',
'      PV_ERROR  => :P0_ERROR);*/',
'    ',
'      SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'        INTO lv_pue_sri, lv_uge_sri',
'        FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'       WHERE pu.uge_id = ug.uge_id',
'         AND pu.pue_id = 689; ---LN_PUE_ID;',
'    ',
'      :p164_pue_id      := 689; ---LN_PUE_ID;',
'      :p164_pue_num_sri := lv_pue_sri;',
'      ---:P17_VALIDA_PUNTO := PQ_ASDM_DOCS_XML.FN_TIPO_PUNTO(PN_PUE_ID => LN_PUE_ID);',
'    ',
'      ---ROUND((add_months(MAX(di.div_fecha_vencimiento),6)-SYSDATE)/30,0) formula',
'      SELECT round((add_months(MAX(di.div_fecha_vencimiento), 11) - SYSDATE) / 30 + 7,',
'                   0) ---jalvarez 05-ABR-20116: se pide cambiar que ya no sean 6 sino 12 meses',
'        INTO :p164_plazo_aplica',
'        FROM car_dividendos di',
'       WHERE di.cxc_id = :p164_cxc_id',
'         AND di.div_estado_dividendo = ''P''',
'         AND di.div_estado_registro = 0',
'         AND di.div_saldo_cuota > 0;',
'    ',
'      IF :p164_plazo_aplica > 36 THEN',
unistr('        ---jalvarez 05-ABR-2021: se pide cambiar aumentar a 7 meses m\00E1s el plazo y validar que no sobre pase los 36 meses'),
'        :p164_plazo_aplica := 36;',
'      END IF;',
'    ',
'      SELECT SUM(nvl(di.div_valor_interes, 0)) intereses_nc',
'        INTO ln_intereses_nc',
'        FROM car_dividendos di',
'       WHERE di.cxc_id = :p164_cxc_id',
'         AND di.div_estado_dividendo = ''P''',
'         AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'         AND di.div_estado_registro =',
'             pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'    ',
'      ----- cargo coleccion de intereses diferidos',
'    ',
'      pq_car_refin_precancelacion.pr_carga_coleccion_nd_ref(pn_cli_id        => :p164_cli_id,',
'                                                            pn_com_id        => ln_com_id,',
'                                                            pn_emp_id        => :f_emp_id,',
'                                                            pn_cxc_id        => :p164_cxc_id,',
'                                                            pn_valor_interes => nvl(ln_intereses_nc,',
'                                                                                    0),',
'                                                            pn_pue_sri       => lv_pue_sri,',
'                                                            pn_uge_sri       => lv_uge_sri,',
'                                                            pv_error         => :p0_error);',
'    ',
'      pq_car_refin_precancelacion.pr_carga_coll_cred_refin(pn_emp_id => :f_emp_id,',
'                                                           pn_cli_id => :p164_cli_id,',
'                                                           pn_cxc_id => :p164_cxc_id,',
'                                                           pn_tse_id => :f_seg_id,',
'                                                           pv_error  => :p0_error);',
'    ',
'      ln_valida_pago_caja := pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p164_cxc_id);',
'    ',
'      IF lv_abreviatura != ''MAR'' THEN',
'      ',
'        :p164_alerta_abono := ''Para realizar el refinanciamiento necesita hacer un cambio de cartera'';',
'      END IF;',
'    ',
'      IF ln_valida_pago_caja > 0 THEN',
'      ',
'        IF :p164_alerta_abono IS NULL THEN',
'          :p164_alerta_abono := :p164_alerta_abono ||',
'                                '' Para realizar el refinanciamiento debe cancelar: '' ||',
'                                ln_valida_pago_caja;',
'        ELSE',
'          :p164_alerta_abono := :p164_alerta_abono || '' y  debe cancelar: '' ||',
'                                ln_valida_pago_caja;',
'        END IF;',
'      ELSE',
'      ',
'        :p164_alerta_abono := '' '';',
'      ',
'      END IF;',
'    ',
'      BEGIN',
'      ',
'        BEGIN',
'        ',
'          SELECT ab.cas_id',
'            INTO ln_cas_id',
'            FROM car_abonos_simulador ab',
'           WHERE ab.cxc_id = :p164_cxc_id',
'             AND ab.cas_estado_registro = 0;',
'        ',
'        EXCEPTION',
'          WHEN no_data_found THEN',
'            ln_cas_id := NULL;',
'        END;',
'      ',
'        IF ln_cas_id IS NULL THEN',
'        ',
'          SELECT SUM(to_number(c004)) capital,',
'                 SUM(to_number(c005)) intereses',
'            INTO ln_valor_capital, ln_valor_interes',
'            FROM apex_collections',
'           WHERE collection_name = ''COL_CREDITOS_REFIN'';',
'        ',
'          ---jalvarez 16/jun/2016 se cambia para obtener los datos de la foto   ',
'        ',
'          SELECT cxc.cxc_saldo',
'            INTO ln_saldo_ant',
'            FROM car_cuentas_por_cobrar cxc',
'           WHERE cxc.cxc_id = :p164_cxc_id;',
'        ',
'          --jalvarez 26-may-2016 se cambia la forma de obtener los interes pendientes de pago',
'          SELECT CASE',
'                   WHEN (SUM(di.div_valor_cuota) - SUM(di.div_saldo_cuota)) >=',
'                        SUM(di.div_valor_interes) THEN',
'                    0',
'                   ELSE',
'                    SUM(di.div_valor_interes) -',
'                    (SUM(di.div_valor_cuota) - SUM(di.div_saldo_cuota))',
'                 END intereses_no_pag',
'            INTO ln_intereses_no_pag',
'            FROM car_dividendos di',
'           WHERE di.cxc_id = :p164_cxc_id',
'             AND di.div_estado_dividendo = ''P''',
'             AND trunc(di.div_fecha_vencimiento, ''mm'') >',
'                 trunc(SYSDATE, ''mm'')',
'             AND di.div_estado_registro =',
'                 pq_constantes.fn_retorna_constante(0,',
'                                                    ''cv_estado_reg_activo'');',
'        ',
'          pq_car_dml.pr_ins_car_abonos_simul(pn_cas_id               => ln_cas_id,',
'                                             pn_emp_id               => :f_emp_id,',
'                                             pd_cas_fecha            => SYSDATE,',
'                                             pn_cas_capital          => ln_valor_capital,',
'                                             pn_cas_intereses        => pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p164_cxc_id),',
'                                             pn_cas_abono            => 0,',
'                                             pn_cas_diferencia       => 0,',
'                                             pv_cas_estado_registro  => pq_constantes.fn_retorna_constante(0,',
'                                                                                                           ''cv_estado_reg_activo''),',
'                                             pn_cxc_id               => :p164_cxc_id,',
'                                             pn_cas_saldo_cxc_ant    => ln_saldo_ant,',
'                                             pn_cas_intereses_no_pag => nvl(ln_intereses_no_pag,',
'                                                                            0),',
'                                             pn_cas_abonos_capital   => nvl(pq_car_refin_precancelacion.pr_valida_abono_ant(pn_cxc_id => :p164_cxc_id),',
'                                                                            0),',
'                                             pv_error                => :p0_error);',
'        ',
'        ELSE',
'        ',
'          UPDATE car_abonos_simulador ar',
'             SET ar.cas_nuevo_capital = ln_valor_capital',
'           WHERE ar.cas_id = ln_cas_id;',
'        ',
'        END IF;',
'      END;',
'    ',
'    ELSE',
'      raise_application_error(-20000,',
unistr('                              ''La calificaci\00F3n del cliente esta en el rango C,D,E,E1,E2 ln_par_dias_mora''||ln_par_dias_mora);'),
unistr('      :p0_error := ''La calificaci\00F3n del cliente esta en el rango C,D,E,E1,E2'';'),
'    END IF;',
'  ',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>347357196278263189
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'',
'BEGIN',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p102_cxc_id;',
'',
'  ----- obtengo los datos del periodo de caja',
'',
'  pq_ven_movimientos_caja.pr_abrir_caja(pn_usu_id => :f_user_id,',
'                                        pn_uge_id => :f_uge_id,',
'                                        pn_emp_id => :f_emp_id,',
'                                        pn_pue_id => ln_pue_id,',
'                                        pn_pca_id => ln_pca_id,',
'                                        pv_error  => :p0_error);',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = ln_pue_id;',
'',
'  SELECT SUM(di.div_valor_interes) intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'',
'  ----- cargo coleccion de intereses diferidos',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nd_ref(pn_cli_id        => :p102_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p102_cxc_id,',
'                                                        pn_valor_interes => ln_intereses_nc,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'  pq_car_refin_precancelacion.pr_carga_coll_cred_refin(pn_emp_id => :f_emp_id,',
'                                                       pn_cli_id => :p102_cli_id,',
'                                                       pn_cxc_id => :p102_cxc_id,',
'                                                       pn_tse_id => :f_seg_id,',
'                                                       pv_error  => :p0_error);',
'',
'  ln_valida_pago_caja := pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p102_cxc_id);',
'',
'  IF ln_valida_pago_caja > 0 THEN',
'  ',
'    :p102_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_valida_pago_caja;',
'  ',
'  ELSE',
'  ',
'    :p102_alerta_abono := '' '';',
'  ',
'  END IF;',
'',
'END;'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379610653997028116)
,p_process_sequence=>11
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_cuenta NUMBER;',
'BEGIN',
'',
'',
'  SELECT COUNT(*)',
'    INTO ln_cuenta',
'    FROM apex_collections',
'   WHERE collection_name = ''COL_CREDITOS_REFIN''',
'     AND seq_id = :p164_seq_id;',
'',
'  IF ln_cuenta > 0 THEN',
'    apex_collection.delete_member(p_collection_name => ''COL_CREDITOS_REFIN'',',
'                                  p_seq             => :p164_seq_id);',
'  END IF;',
'',
':P164_FECHA_CORTE:=NULL;',
'  IF apex_collection.collection_exists(''COLL_FECHAS_CORTE'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_FECHAS_CORTE'');',
'  ',
'  END IF;',
'',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_COBERTURA'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_COBERTURA'');',
'  ',
'  END IF;',
'  ',
'    IF apex_collection.collection_exists(''COL_COBERTURA_SEG'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_COBERTURA_SEG'');',
'  ',
'  END IF;',
'',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'  IF apex_collection.collection_exists(''COL_ND_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_ND_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(pq_constantes.fn_retorna_constante(NULL,',
'                                                                          ''cv_col_gastos_int_mora'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                              ''cv_col_gastos_int_mora''));',
'  ',
'  END IF;',
'  :p164_alerta_2 := '' '';',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>347357502727263190
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379611865676028117)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_pruba_impresion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE ',
'',
'e VARCHAR2(3000);',
'',
'begin',
'  -- Call the procedure',
'  pq_car_refin_precancelacion.pr_imprimir(pn_emp_id => 1,',
'                                          pn_com_id => 881752,',
'                                          pn_ttr_id => 386,',
'                                          pn_age_id => 15,',
'                                          pn_fac_desde => NULL,',
'                                          pn_fac_hasta => NULL,',
'                                          pv_error => e);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>347358714406263191
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379612735614028118)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion_id'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'ln_int_nc_refi number;',
'BEGIN',
'',
'IF :P0_ERROR IS NULL THEN',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p164_cxc_id;',
'',
'  ----- obtengo los datos del periodo de caja',
'',
'',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = 689;',
'',
'',
'SELECT SUM(intereses_nc) INTO ln_intereses_nc FROM ',
'(SELECT  CASE',
'               WHEN ((di.div_valor_cuota) - (di.div_saldo_cuota)) >=',
'                    (di.div_valor_interes) THEN',
'                0',
'               ELSE',
'                (di.div_valor_interes) -',
'                ((di.div_valor_cuota) - (di.div_saldo_cuota))',
'             END intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p164_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo''));',
'         ',
'  ----- cargo coleccion de intereses diferidos',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nc_ref(pn_cli_id        => :p164_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p164_cxc_id,',
'                                                        pn_valor_interes => nvl(ln_intereses_nc,0),',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
unistr(' ----- cargo coleccion de intereses para la nota de d\00E9bito'),
'select ',
'SUM(to_number(c004)) Valor_Interes',
'into ln_int_nc_refi',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'  pq_car_refin_precancelacion.PR_CARGA_COLECCION_ND_REF(pn_cli_id        => :p164_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p164_cxc_id,',
'                                                        pn_valor_interes => ln_int_nc_refi,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'',
'END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>347359584344263192
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379613134518028118)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINA_COLECCION_DIVIDENDOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  ',
'APEX_COLLECTION.create_or_truncate_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'carga'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>347359983248263192
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(379612301173028117)
,p_process_sequence=>31
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_intereses NUMBER;',
'  ln_abono     NUMBER;',
'  ln_diferencia NUMBER;',
'',
'BEGIN',
'',
'  SELECT ab.cas_abono, ab.cas_intereses',
'    INTO ln_abono, ln_intereses',
'    FROM car_abonos_simulador ab',
'   WHERE ab.cxc_id = :p164_cxc_id',
'     AND ab.cas_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'         ',
'        ',
'         ',
'  IF NVL(ln_abono,0) >= ln_intereses THEN',
'    ',
'    :P164_ALERTA_ABONO := NULL;',
'    ',
'  ELSE',
'    ',
'  ln_diferencia := ln_intereses - ln_abono;',
'    if :P164_EDE_ID_CARTERA is null then',
'',
' :p164_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_intereses;    ',
'  ',
'',
'    ELSif :P164_EDE_ID_CARTERA =''MAR'' then',
'',
' :p164_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_intereses;    ',
'  ',
'ELSE',
' :p164_alerta_abono := ''Para realizar el refinanciamiento debe hacer un cambio de cartera y cancelar: '' ||',
'                          ln_intereses;    ',
'end if;',
'  END IF;',
' ',
'   ',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>347359149903263191
);
wwv_flow_imp.component_end;
end;
/
