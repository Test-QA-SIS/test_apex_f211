prompt --application/pages/page_00094
begin
--   Manifest
--     PAGE: 00094
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>94
,p_name=>'RecalculoManualNotaCredito'
,p_alias=>'RECALCULOMANUALNOTACREDITO'
,p_step_title=>'RecalculoManualNotaCredito'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(155163775781077105)
,p_plug_name=>'RecalculoManualNotaCredito'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155223269406340188)
,p_name=>'Facturas Cliente'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''Carcar'' cargar, c.com_id fac_sic,',
'       c.com_id fact_interna_sic,',
'       h.secuencia_factura fact_oracle,',
'       i.cin_id,',
'       c.com_fecha,',
'       i.cli_id,',
'       c.nombre_completo,',
'       c.per_nro_identificacion identificacion,',
'       c.uge_num_sri || '' - '' || c.pue_num_sri || '' - '' || c.com_numero factura,',
'       pq_lv_kdda.fn_obtiene_valor_lov(291,',
'                                       c.com_tipo) tipo,',
'       c.pol_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(55,',
'                                       c.pol_id) politica,',
'       ',
'       c.com_plazo plazo,',
'       pq_ven_retorna_sec_id.fn_cin_sec_id(i.cin_id) cominv',
'',
'  from inv_comprobantes_inventario  i,',
'       asdm_transacciones           t,',
'       v_ven_comprobantes           c,',
'       asdm_p.asdm_homologacion_com h',
'',
' where i.trx_id = t.trx_id',
'   and t.ttr_id in',
'(',
'pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_devolucion''),',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_retiro_def'')',
')',
'   and i.com_id = c.com_id',
'   and i.emp_id = c.emp_id',
'   and i.emp_id = t.emp_id',
'   AND c.com_id = h.com_id(+)',
'   and i.emp_id = :f_emp_id',
'  AND i.com_id NOT IN (select com_id_ref',
'                          from ven_comprobantes   nc,',
'                               asdm_transacciones tr',
'                         WHERE nc.trx_id = tr.trx_id',
'                           AND tr.ttr_id in(',
'pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_devolucion''),',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_retiro_def'')',
')',
'                           and nc.emp_id = nc.emp_id',
'                           AND nc.cin_id = i.cin_id)',
'   and c.cli_id = :P94_CLI_ID',
' ORDER BY c.com_fecha DESC'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155229982482362903)
,p_query_column_id=>1
,p_column_alias=>'CARGAR'
,p_column_display_sequence=>1
,p_column_heading=>'Cargar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:94:&SESSION.::&DEBUG.::P94_COM_ID:#FACT_INTERNA_SIC#'
,p_column_linktext=>'#CARGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155249461844442119)
,p_query_column_id=>2
,p_column_alias=>'FAC_SIC'
,p_column_display_sequence=>15
,p_column_heading=>'Fac Sic'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155223662920340219)
,p_query_column_id=>3
,p_column_alias=>'FACT_INTERNA_SIC'
,p_column_display_sequence=>2
,p_column_heading=>'FACT_INTERNA_SIC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155223779248340219)
,p_query_column_id=>4
,p_column_alias=>'FACT_ORACLE'
,p_column_display_sequence=>3
,p_column_heading=>'FACT_ORACLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155223857351340219)
,p_query_column_id=>5
,p_column_alias=>'CIN_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CIN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155223977661340220)
,p_query_column_id=>6
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>5
,p_column_heading=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224074118340220)
,p_query_column_id=>7
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>6
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224180769340220)
,p_query_column_id=>8
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>7
,p_column_heading=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224274987340220)
,p_query_column_id=>9
,p_column_alias=>'IDENTIFICACION'
,p_column_display_sequence=>8
,p_column_heading=>'IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224374241340220)
,p_query_column_id=>10
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>9
,p_column_heading=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224451434340220)
,p_query_column_id=>11
,p_column_alias=>'TIPO'
,p_column_display_sequence=>10
,p_column_heading=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224555488340220)
,p_query_column_id=>12
,p_column_alias=>'POL_ID'
,p_column_display_sequence=>11
,p_column_heading=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224661887340220)
,p_query_column_id=>13
,p_column_alias=>'POLITICA'
,p_column_display_sequence=>12
,p_column_heading=>'POLITICA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224779298340220)
,p_query_column_id=>14
,p_column_alias=>'PLAZO'
,p_column_display_sequence=>13
,p_column_heading=>'PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155224874500340220)
,p_query_column_id=>15
,p_column_alias=>'COMINV'
,p_column_display_sequence=>14
,p_column_heading=>'COMINV'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155247474526436281)
,p_name=>'Detalle'
,p_parent_plug_id=>wwv_flow_imp.id(155223269406340188)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.cde_id,',
'       i.ite_sec_id,',
'       i.ite_descripcion_larga,',
'       d.cei_id,',
'       c.cei_descripcion',
'  FROM ven_comprobantes_det       d,',
'       inv_items                  i,',
'       inv_categorias_estado_item c',
' WHERE com_id = :p94_com_id',
'   and i.ite_sku_id = d.ite_sku_id',
'   and c.cei_id = d.cei_id'))
,p_display_when_condition=>'P94_COM_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155247781453436287)
,p_query_column_id=>1
,p_column_alias=>'CDE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155247871346436287)
,p_query_column_id=>2
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>2
,p_column_heading=>'ITE_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155247975990436287)
,p_query_column_id=>3
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>3
,p_column_heading=>'ITE_DESCRIPCION_LARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155248053009436287)
,p_query_column_id=>4
,p_column_alias=>'CEI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CEI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155248173780436287)
,p_query_column_id=>5
,p_column_alias=>'CEI_DESCRIPCION'
,p_column_display_sequence=>5
,p_column_heading=>'CEI_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155680677631111960)
,p_name=>'VARAIBLES CABECERA FACTURA'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.var_id,',
'       c.vco_valor_variable,0',
'  from ven_var_comprobantes c',
' where c.com_id = :P94_COM_ID'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Descargar'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_exp_filename=>'Var_Cabecera'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155681172114112055)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155681253786112065)
,p_query_column_id=>2
,p_column_alias=>'VCO_VALOR_VARIABLE'
,p_column_display_sequence=>2
,p_column_heading=>'VCO_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155748176966338895)
,p_query_column_id=>3
,p_column_alias=>'0'
,p_column_display_sequence=>3
,p_column_heading=>'0'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155686263348126836)
,p_name=>'VARAIBLES DETALLE FACTURA'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select d.var_id,',
'       d.vcd_valor_variable,d.cde_id,0',
'  from ven_var_comprobantes_det d',
' where d.cde_id in (',
'SELECT d.cde_id',
'  FROM ven_comprobantes_det       d',
' WHERE com_id = :p94_com_id',
')',
' order by d.cde_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Descargar'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_exp_filename=>'Var_Detalles'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155686654231126855)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155686770940126855)
,p_query_column_id=>2
,p_column_alias=>'VCD_VALOR_VARIABLE'
,p_column_display_sequence=>2
,p_column_heading=>'VCD_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155686572964126848)
,p_query_column_id=>3
,p_column_alias=>'CDE_ID'
,p_column_display_sequence=>3
,p_column_heading=>'CDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155747774196338080)
,p_query_column_id=>4
,p_column_alias=>'0'
,p_column_display_sequence=>4
,p_column_heading=>'0'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155700751883170750)
,p_name=>'VARAIBLES CABECERA_CARGADOS'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.var_id,',
'       c.vcm_valor_variable',
'  from ven_var_comprobantes_man c',
' where c.com_id = :P94_COM_ID'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155701176378170765)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155701277695170772)
,p_query_column_id=>2
,p_column_alias=>'VCM_VALOR_VARIABLE'
,p_column_display_sequence=>2
,p_column_heading=>'VCM_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(155702567813175363)
,p_name=>'VARAIBLES DETALLE_CARGADOS'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select d.cde_id,',
'       d.var_id,',
'       d.vcm_valor_variable',
'  from ven_var_comprobantes_detman d',
' where d.com_id = 	:P94_COM_ID',
' order by d.cde_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155702966457175375)
,p_query_column_id=>1
,p_column_alias=>'CDE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155703052593175381)
,p_query_column_id=>2
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>2
,p_column_heading=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155703180442175381)
,p_query_column_id=>3
,p_column_alias=>'VCM_VALOR_VARIABLE'
,p_column_display_sequence=>3
,p_column_heading=>'VCM_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(155695967897156523)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(155680677631111960)
,p_button_name=>'GRABAR_CABECERA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar Cabecera'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(155697176208158881)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(155686263348126836)
,p_button_name=>'GRABAR_DETALLE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar Detalle'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155167075696095971)
,p_name=>'P94_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(155163775781077105)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_REC_MAN'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.nombre_completo || '' Cod'' || i.cli_id || '' Ced_Ruc'' ||',
'       c.per_nro_identificacion d,',
'       i.cli_id r',
'  from inv_comprobantes_inventario  i,',
'       asdm_transacciones           t,',
'       v_ven_comprobantes           c,',
'       asdm_p.asdm_homologacion_com h',
' where i.trx_id = t.trx_id',
'   and t.ttr_id =',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_ing_retiro_def'')',
'   and i.com_id = c.com_id',
'   and i.emp_id = c.emp_id',
'   and i.emp_id = t.emp_id',
'   AND c.com_id = h.com_id(+)',
'   and i.emp_id = :f_emp_id',
'   AND i.com_id NOT IN (select com_id_ref',
'                          from ven_comprobantes   nc,',
'                               asdm_transacciones tr',
'                         WHERE nc.trx_id = tr.trx_id',
'                           AND tr.ttr_id =',
'                               pq_constantes.fn_retorna_constante(NULL,',
'                                                                  ''cn_ttr_id_nota_credito_retiro'')',
'                           and nc.emp_id = nc.emp_id',
'                           AND nc.cin_id = i.cin_id)',
'',
' ORDER BY c.com_fecha DESC',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit()";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155239458549403314)
,p_name=>'P94_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(155223269406340188)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155692660494144895)
,p_name=>'P94_CABECERA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(155680677631111960)
,p_prompt=>'Cargar_Cabecera'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'WWV_FLOW_FILES'
,p_attribute_12=>'NATIVE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155693672614148425)
,p_name=>'P94_DETALLE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(155686263348126836)
,p_prompt=>'Cargar_Detalle'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'WWV_FLOW_FILES'
,p_attribute_12=>'NATIVE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(165462655043317394)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cabecera'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comunes.pr_lee_archivos_nota_credi_man(',
':P94_CABECERA,',
':f_emp_id,',
':P94_COM_ID,',
'''C'',',
':p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(155695967897156523)
,p_internal_uid=>133209503773552468
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(165466875494474673)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comunes.pr_lee_archivos_nota_credi_man(',
':P94_DETALLE,',
':f_emp_id,',
':P94_COM_ID,',
'''D'',',
':p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(155697176208158881)
,p_internal_uid=>133213724224709747
);
wwv_flow_imp.component_end;
end;
/
