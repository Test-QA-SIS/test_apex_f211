prompt --application/pages/page_00010
begin
--   Manifest
--     PAGE: 00010
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>10
,p_name=>'Historial Postergacion Cheques'
,p_step_title=>'Historial Postergacion Cheques'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(105722157300756532)
,p_plug_name=>'Historial Postergaciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cre.cre_id,',
'       cli.nombre_completo cliente,',
'       cre.cre_nro_cheque nro_cheque,',
'       cre.cre_nro_cuenta nro_cuenta,',
'       cre.cre_nro_pin nro_pin,',
'       cre.cre_valor valor,',
'       ede.ede_descripcion entidad_financiera,',
'       cre.cre_fecha_deposito,',
'       cre.cre_observaciones,',
'       ech.ech_descripcion estado,',
'       ttr.trx_fecha_ejecucion Anulado,',
'        kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(ttr.usu_id)usuario,',
'        uge.uge_nombre',
'  FROM asdm_cheques_recibidos   cre,',
'       asdm_entidades_destinos  ede,',
'       v_asdm_clientes_personas cli,',
'       asdm_estados_cheques     ech,',
'       asdm_transacciones ttr,',
'       asdm_unidades_gestion uge',
' WHERE cre.cre_nro_cheque = :p10_cre_nro_cheque',
'   AND cre.ede_id = ede.ede_id',
'   AND cre.cli_id = cli.cli_id',
'   AND cre.ech_id = ech.ech_id',
'   AND cre.emp_id = :f_emp_id',
'   AND cli.emp_id = :f_emp_id',
'   AND cre.cli_id=:P10_CLI_ID',
'   AND cre.cre_nro_cuenta=:P10_NUM_CUENTA',
'   AND cre.cre_nro_pin=:P10_PIN',
'   AND cre.trx_id=ttr.trx_id',
'   AND ttr.uge_id=uge.uge_id',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_num_rows=>15
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(105722264166756532)
,p_name=>'Historial Postergaciones'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>73469112896991606
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(105722880515756541)
,p_db_column_name=>'CRE_FECHA_DEPOSITO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Fecha<br>Deposito'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(105723055637756542)
,p_db_column_name=>'CRE_OBSERVACIONES'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61221857013313224)
,p_db_column_name=>'CLIENTE'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61221956735313229)
,p_db_column_name=>'NRO_CHEQUE'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Nro.<br>Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61222058528313230)
,p_db_column_name=>'NRO_CUENTA'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Nro.<br>Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61222183830313230)
,p_db_column_name=>'NRO_PIN'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Nro. Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61222278421313230)
,p_db_column_name=>'VALOR'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61222359704313232)
,p_db_column_name=>'ENTIDAD_FINANCIERA'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Entidad<br>Financiera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ENTIDAD_FINANCIERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61222457360313232)
,p_db_column_name=>'ESTADO'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ESTADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61224769396335794)
,p_db_column_name=>'CRE_ID'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Cre Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77668765524135415)
,p_db_column_name=>'ANULADO'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>unistr('Fecha<br>Postergaci\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'ANULADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77668877300135419)
,p_db_column_name=>'USUARIO'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'USUARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77668966849135420)
,p_db_column_name=>'UGE_NOMBRE'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>unistr('Unidad<br>de Gesti\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(105723551693759117)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1276200080919693'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLIENTE:ENTIDAD_FINANCIERA:NRO_CHEQUE:NRO_CUENTA:NRO_PIN:VALOR:CRE_FECHA_DEPOSITO:ESTADO:CRE_OBSERVACIONES:ANULADO:USUARIO:UGE_NOMBRE'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'ASC'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(61226854420350389)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(105722157300756532)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(77649978234006660)
,p_name=>'P10_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(105722157300756532)
,p_prompt=>'cli_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(77650160158006661)
,p_name=>'P10_NUM_CUENTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(105722157300756532)
,p_prompt=>'num_cuenta'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(77650381572006661)
,p_name=>'P10_PIN'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(105722157300756532)
,p_prompt=>'pin'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(105676871270441802)
,p_name=>'P10_CRE_NRO_CHEQUE'
,p_item_sequence=>10
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Cre Nro Cheque'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
