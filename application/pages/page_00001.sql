prompt --application/pages/page_00001
begin
--   Manifest
--     PAGE: 00001
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>1
,p_name=>'Menu Principal Aplicacion Marcimex'
,p_step_title=>'Menu'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'13'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(21025054272399543051)
,p_name=>unistr('FACTURAS PENDIENTES DE REGULARIZACI\00D3N')
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT facturas_faltan@DBL_DBMXSICQ.CONSENSO.COM.EC',  --* from 
'where UGE_ID = :F_UGE_ID'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025054555074543102)
,p_query_column_id=>1
,p_column_alias=>'VCO_VALOR_VARIABLE'
,p_column_display_sequence=>1
,p_column_heading=>'VCO_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025054654936543103)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025054766236543103)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025054879397543103)
,p_query_column_id=>4
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>4
,p_column_heading=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025054973599543103)
,p_query_column_id=>5
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>5
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055065548543103)
,p_query_column_id=>6
,p_column_alias=>'TSE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'TSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055175524543103)
,p_query_column_id=>7
,p_column_alias=>'TERMINO_VENTA'
,p_column_display_sequence=>7
,p_column_heading=>'TERMINO_VENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055268164543103)
,p_query_column_id=>8
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>8
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055352255543103)
,p_query_column_id=>9
,p_column_alias=>'AGE_ID_AGENCIA'
,p_column_display_sequence=>9
,p_column_heading=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055471935543103)
,p_query_column_id=>10
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>10
,p_column_heading=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.::P1_CLI_ID:#CLI_ID#'
,p_column_linktext=>'#NOMBRE_COMPLETO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055567689543104)
,p_query_column_id=>11
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>11
,p_column_heading=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055668317543104)
,p_query_column_id=>12
,p_column_alias=>'PER_TIPO_IDENTIFICACION'
,p_column_display_sequence=>12
,p_column_heading=>'PER_TIPO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055783597543104)
,p_query_column_id=>13
,p_column_alias=>'AGENCIA'
,p_column_display_sequence=>13
,p_column_heading=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025055883740543104)
,p_query_column_id=>14
,p_column_alias=>'AUTORIZADO'
,p_column_display_sequence=>14
,p_column_heading=>'AUTORIZADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(21025060083443593486)
,p_name=>'FACTURAS SIC'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT vco.vco_valor_variable,',
'       cc.cli_id,',
'       cc.com_id,',
'       cc.com_fecha,',
'       cc.emp_id,',
'       cc.tse_id,',
'       (SELECT tt.tve_descripcion',
'          FROM ven_terminos_venta tt',
'         WHERE tt.tve_id = cc.tve_id) termino_venta,',
'       cc.uge_id,',
'       cc.age_id_agencia,',
'       (SELECT p.per_razon_social || p.per_primer_nombre || '' '' ||',
'               p.per_segundo_nombre || '' '' || p.per_primer_apellido || '' '' ||',
'               p.per_segundo_apellido',
'          FROM asdm_personas p, asdm_clientes c',
'         WHERE p.per_id = c.per_id',
'           AND c.cli_id = cc.cli_id) nombre_completo',
'  FROM ven_comprobantes cc, ven_var_comprobantes vco',
'         WHERE cc.cli_id = :P1_CLI_ID--239062',
'         AND cc.uge_id = :F_UGE_ID',
'          -- AND cc.com_tipo = cc.com_tipo',
'         AND vco.com_id = cc.com_id',
'         AND cc.com_tipo = ''F''',
'         AND vco.var_id = 955',
'           AND cc.com_fecha BETWEEN to_date(''31/12/2016'', ''dd/mm/yyyy'') AND',
'               to_date(''31/12/2016'', ''dd/mm/yyyy'') + 0.99999'))
,p_display_when_condition=>':P1_CLI_ID IS NOT NULL'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060373241593488)
,p_query_column_id=>1
,p_column_alias=>'VCO_VALOR_VARIABLE'
,p_column_display_sequence=>1
,p_column_heading=>'VCO_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060482252593488)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060567777593488)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060662339593488)
,p_query_column_id=>4
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>4
,p_column_heading=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060763679593488)
,p_query_column_id=>5
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>5
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060859084593488)
,p_query_column_id=>6
,p_column_alias=>'TSE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'TSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025060976428593488)
,p_query_column_id=>7
,p_column_alias=>'TERMINO_VENTA'
,p_column_display_sequence=>7
,p_column_heading=>'TERMINO_VENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025061070547593488)
,p_query_column_id=>8
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>8
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025061171882593488)
,p_query_column_id=>9
,p_column_alias=>'AGE_ID_AGENCIA'
,p_column_display_sequence=>9
,p_column_heading=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(21025061261080593488)
,p_query_column_id=>10
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>10
,p_column_heading=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(305939048917025076)
,p_branch_action=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 26-JUN-2009 12:55 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(21025056081102544931)
,p_name=>'P1_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(21025054272399543051)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
