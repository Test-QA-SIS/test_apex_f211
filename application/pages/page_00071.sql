prompt --application/pages/page_00071
begin
--   Manifest
--     PAGE: 00071
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>71
,p_name=>'Impresion Notas de Credito'
,p_alias=>'IMPRESION_NC'
,p_step_title=>'Impresion Notas de Credito'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111914660873794459)
,p_plug_name=>'Impresion Notas de Credito'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111947552431132603)
,p_plug_name=>'Notas de Credito  <B>&P71_TTR_DESCRIPCION.   &P71_DATAFOLIO.     -    &P71_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P71_NOTAS_CREDITO'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(119089160822621534)
,p_name=>'Notas de Credito'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select seq_id,',
'c001 Comprobante,',
'c002 Cliente,',
'c003 Nombres,',
'c004 Valor_Rebaja,',
'c005 Numero,',
'c006 Establecimiento,',
'c007 Punto_Emision',
'from apex_collections ',
'where collection_name = ''COL_NOTAS_CREDITO_PG'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'DESCARGAR'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_exp_separator=>';'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119089474448622262)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119089565724622320)
,p_query_column_id=>2
,p_column_alias=>'COMPROBANTE'
,p_column_display_sequence=>2
,p_column_heading=>'COMPROBANTE'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119089661670622320)
,p_query_column_id=>3
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>3
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119089751641622320)
,p_query_column_id=>4
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>4
,p_column_heading=>'NOMBRES'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119089861347622320)
,p_query_column_id=>5
,p_column_alias=>'VALOR_REBAJA'
,p_column_display_sequence=>5
,p_column_heading=>'VALOR_REBAJA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119089974464622320)
,p_query_column_id=>6
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>6
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119090053667622321)
,p_query_column_id=>7
,p_column_alias=>'ESTABLECIMIENTO'
,p_column_display_sequence=>7
,p_column_heading=>'ESTABLECIMIENTO'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(119090158358622321)
,p_query_column_id=>8
,p_column_alias=>'PUNTO_EMISION'
,p_column_display_sequence=>8
,p_column_heading=>'PUNTO_EMISION'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(119094264515641302)
,p_plug_name=>'Notas de Credito '
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from tmp_com_notas_credito',
'where tnp_id = :p71_notas_credito',
'and uge_id = :f_uge_id'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(119094351707641302)
,p_name=>'Notas de Credito '
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>86841200437876376
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119094569182641321)
,p_db_column_name=>'COM_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119094679287641336)
,p_db_column_name=>'CLI_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119094780119641350)
,p_db_column_name=>'TNC_NOMBRES'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Nombres'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TNC_NOMBRES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119094851944641351)
,p_db_column_name=>'TNC_VALOR'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TNC_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119094968205641354)
,p_db_column_name=>'TNC_NUMERO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Numero'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TNC_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119095055175641354)
,p_db_column_name=>'TNC_ESTABLECIMIENTO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Establecimiento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TNC_ESTABLECIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119095173880641354)
,p_db_column_name=>'TNC_PUNTO_EMISION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Punto Emision'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TNC_PUNTO_EMISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119095252291641354)
,p_db_column_name=>'EMP_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119095378753641355)
,p_db_column_name=>'UGE_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119095467198641358)
,p_db_column_name=>'PUE_ID'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(70308651763401442)
,p_db_column_name=>'TNP_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Tnp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TNP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(119095751494641747)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'868427'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'COM_ID:CLI_ID:TNC_NOMBRES:TNC_VALOR:TNC_NUMERO:TNC_ESTABLECIMIENTO:TNC_PUNTO_EMISION:EMP_ID:UGE_ID:PUE_ID'
,p_sort_column_1=>'TNC_NUMERO'
,p_sort_direction_1=>'ASC'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(1790828679547477556)
,p_plug_name=>'Impresion'
,p_parent_plug_id=>wwv_flow_imp.id(119094264515641302)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P71_NOTAS_CREDITO'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(111987063802381846)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(111947552431132603)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_condition=>':P71_SECUENCIA_ACTUAL = :P71_NUEVA_SECUENCIA'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(112058468237903532)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(1790828679547477556)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'IMPRIMIR'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from tmp_com_notas_credito',
'where uge_id = :f_uge_id'))
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1803606877486028758)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(1790828679547477556)
,p_button_name=>'REIMPRESION'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'REIMPRESION NC'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:99:&SESSION.::&DEBUG.::P99_TTR_ID,P99_SECUENCIA,P99_REIMPRIMIR:&P71_TTR_ID.,,'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from tmp_com_notas_credito',
'where uge_id = :f_uge_id'))
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(119141868198756098)
,p_branch_action=>'f?p=&APP_ID.:71:&SESSION.:IMPRIMIR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(112058468237903532)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 08-DEC-2011 09:36 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111915767368815261)
,p_name=>'P71_TTR_DESCRIPCION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'ttr_descripcion'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111915960851815285)
,p_name=>'P71_PERIODO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'periodo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111916162092815285)
,p_name=>'P71_DATAFOLIO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'datafolio'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111916374598815286)
,p_name=>'P71_NUEVA_SECUENCIA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'nueva_secuencia'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111916563997815286)
,p_name=>'P71_PUE_NUM_SRI'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(111947552431132603)
,p_prompt=>'Punto de Emision'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111916765775815286)
,p_name=>'P71_UGE_NUM_EST_SRI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(111947552431132603)
,p_prompt=>'Establecimiento'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111916968504815286)
,p_name=>'P71_PUE_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'pue_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111917161306815286)
,p_name=>'P71_TTR_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'ttr_id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(111948361781135308)
,p_name=>'P71_SECUENCIA_ACTUAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(111947552431132603)
,p_prompt=>'Ingrese la Secuencia actual a Imprimir'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit("CARGA_NC")'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(124535162014249785)
,p_name=>'P71_NOTAS_CREDITO'
,p_is_required=>true
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(111914660873794459)
,p_prompt=>'NOTAS DE CREDITO'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TNP_DESCRIPCION, TNP_ID',
'FROM CAR_TIPOS_NC_PROV',
'WHERE EMP_ID = :F_EMP_ID'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'REDIRECT_SET_VALUE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1790919152105488584)
,p_name=>'P71_IMPRESION'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(1790828679547477556)
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC2:Impresion Notas de Credito Generadas;1,Impresion Detalle de Valores;2'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(111961166377193457)
,p_validation_name=>'P71_SECUENCIA_ACTUAL'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :P71_SECUENCIA_ACTUAL = :P71_NUEVA_SECUENCIA then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'error'
,p_when_button_pressed=>wwv_flow_imp.id(111987063802381846)
,p_associated_item=>wwv_flow_imp.id(111948361781135308)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(124570351335350706)
,p_validation_name=>'P71_NOTAS_CREDITO'
,p_validation_sequence=>20
,p_validation=>'P71_NOTAS_CREDITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione el tipo de nota de credito que va a imprimir'
,p_when_button_pressed=>wwv_flow_imp.id(111987063802381846)
,p_associated_item=>wwv_flow_imp.id(124535162014249785)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(112067172786933174)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_impresion_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_car_provisiones.pr_impresion_nc(pn_emp_id => :f_emp_id,',
'                pn_uge_id => :f_uge_id,',
'                pn_pue_id => :p71_pue_id,',
'                pn_tnp_iD => :P71_NOTAS_CREDITO,',
'                pn_tipo_imp => :P71_IMPRESION,',
'                pv_error  => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>79814021517168248
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(112004878590603741)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_notas_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'pq_car_provisiones.pr_actualizar_nc(pn_emp_id => :f_emp_id,',
'                 pn_ttr_id => :p71_ttr_id,',
'                 pn_pca_id => :f_pca_id,',
'                 pn_tnp_iD => :P71_NOTAS_CREDITO,',
'                 pv_error  => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(111987063802381846)
,p_internal_uid=>79751727320838815
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(111866278376862966)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_folio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%type;',
'',
'BEGIN',
'',
'if :p71_notas_credito = 1 then',
'',
':p71_ttr_id := pq_constantes.fn_retorna_constante(null,''cn_ttr_id_nc_pagpunt_gareal'');',
'ln_tgr_id := pq_car_credito.fn_obtiene_tipo_grupo_trx(pn_emp_id => :f_emp_id,',
'                                                        pn_ttr_id => :p71_ttr_id,--tipo_transaccion nota de credito garantia real pago puntual',
'                                                        pv_error  => :p0_error);',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       ln_tgr_id,',
'      :P71_TTR_DESCRIPCION, ',
'      :P71_PERIODO,',
'      :P71_DATAFOLIO,',
'      :P71_NUEVA_SECUENCIA,',
'      :P71_PUE_NUM_SRI  ,',
'      :P71_UGE_NUM_EST_SRI ,',
'      :P71_PUE_ID,',
'      :p71_ttr_id,',
'      :p0_nro_folio,',
'      :P0_ERROR);',
'',
'if :p0_error is null then',
'pq_car_provisiones.pr_coleccion_nc(pn_emp_id => :f_emp_id,',
'                                                pn_uge_id => :f_uge_id,',
'                                                pn_pue_id =>:p71_pue_id,',
'                                                pn_tnp_id => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_ppgr''),',
'                                                pv_error  => :P0_ERROR);',
'end if;',
'elsif :p71_notas_credito = 2 then',
':p71_ttr_id := pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_nc_montos'');',
'ln_tgr_id := pq_car_credito.fn_obtiene_tipo_grupo_trx(pn_emp_id => :f_emp_id,',
'                                                        pn_ttr_id => :p71_ttr_id,--tipo_transaccion nota de credito garantia real pago puntual',
'                                                        pv_error  => :p0_error);',
'',
':P0_TTR_ID := ln_tgr_id;',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       ln_tgr_id,',
'      :P71_TTR_DESCRIPCION, ',
'      :P71_PERIODO,',
'      :P71_DATAFOLIO,',
'      :P71_NUEVA_SECUENCIA,',
'      :P71_PUE_NUM_SRI  ,',
'      :P71_UGE_NUM_EST_SRI ,',
'      :P71_PUE_ID,',
'      :p71_ttr_id,',
'      :p0_nro_folio,',
'      :P0_ERROR);',
'',
'if :p0_error is null then',
'pq_car_provisiones.pr_coleccion_nc(pn_emp_id => :f_emp_id,',
'                                                pn_uge_id => :f_uge_id,',
'                                                pn_pue_id =>:p71_pue_id,',
'                                                pn_tnp_id => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_monto''),',
'                                                pv_error  => :P0_ERROR);',
'end if;',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>79613127107098040
);
wwv_flow_imp.component_end;
end;
/
