prompt --application/pages/page_00122
begin
--   Manifest
--     PAGE: 00122
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>122
,p_name=>'Impresion Masiva Facturas Agencia'
,p_step_title=>'Impresion Masiva de Facturas Agencia'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'',
'</script>'))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(256869561946343282)
,p_plug_name=>'Clientes Autorizados Negociacion'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.cli_id,',
'  --     f.cns_fecha fecha,',
'  --     f.cns_estado_registro Estado_Registro,',
'     --  f.ede_id ,',
'       nvl(p.per_razon_social,',
'           p.per_primer_apellido || '' '' || p.per_segundo_apellido',
'        || '' '' ||p.per_primer_nombre || '' '' || p.per_segundo_nombre )',
'            Cliente',
'   --    d.ede_descripcion Entidad',
'  --     u.nombre Usuario_Asigna',
'  from car_clientes_neg_segmento f,',
'       asdm_clientes             c,',
'       asdm_personas             p',
'   --    asdm_entidades_destinos   d',
'     --  kseg_usuarios             u',
' where f.cli_id = c.cli_id',
'   and c.per_id = p.per_id',
'--   and d.ede_id = f.ede_id',
'--   and u.usuario_id = f.usu_id',
'   and f.emp_id = :f_emp_id'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(256869655666343284)
,p_name=>'Clientes Negociacion'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No existen clientes parametrizados'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'DLOPEZ'
,p_internal_uid=>224616504396578358
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(256869874112343295)
,p_db_column_name=>'CLI_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(256870264874343297)
,p_db_column_name=>'CLIENTE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(256870768927344083)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'2246177'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:CLIENTE'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(289954471806715629)
,p_name=>'IMPRESION DE FACTURAS'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select seq_id Numero,',
'c001 Com_id,',
'c002 Fecha,',
'c003 Cli_id,',
'c004 Cliente,',
'c005 Uge_id,',
'c006 Unidad_Gestion,',
'c007 TOTAL ,',
'c008 ||'' - ''||C009 Agencia',
'from apex_collections where collection_name=''COLL_FAC_MAYOREO'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No hay Facturas pendientes de imprimir.'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289954651802715647)
,p_query_column_id=>1
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>1
,p_column_heading=>'RANGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289954778896715650)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289954852352715650)
,p_query_column_id=>3
,p_column_alias=>'FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289954969772715651)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289955082533715651)
,p_query_column_id=>5
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>5
,p_column_heading=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289955165163715651)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>8
,p_column_heading=>'UGE_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289955253031715651)
,p_query_column_id=>7
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>9
,p_column_heading=>'UNIDAD GESTION GENERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289955372137715651)
,p_query_column_id=>8
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>6
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289977854287237366)
,p_query_column_id=>9
,p_column_alias=>'AGENCIA'
,p_column_display_sequence=>7
,p_column_heading=>'Agencia'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(289955457715715661)
,p_plug_name=>' <B>  NUMERO DE FOLIO <B>&P122_NRO_FOLIO. '
,p_parent_plug_id=>wwv_flow_imp.id(289954471806715629)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select count(*)',
'from apex_collections',
'where collection_name=''COLL_FAC_MAYOREO'')>0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>'<B>SECUENCIA ACTUAL<B>  &P51_COM_NUMERO.'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(289957074405715669)
,p_plug_name=>'RANGOS DE IMPRESION'
,p_parent_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P122_COM_NUM'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(289958058314715675)
,p_name=>'FACTURAS A IMPRIMIR'
,p_parent_plug_id=>wwv_flow_imp.id(289954471806715629)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.seq_id Numero,',
'c.c001 Com_id,',
'c.c002 Fecha,',
'c.c003 Cli_id,',
'c.c004 Cliente,',
'c.c005 Uge_id,',
'c.c006 Unidad_Gestion,',
'c.c007 TOTAL,',
'case when c.seq_id = ',
'  (',
'select max(c.seq_id)',
'from apex_collections c, tmp_com_fac_imp d',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and d.com_id = to_number(c.c001)',
'and d.emp_id = :f_emp_id',
'and d.uge_id = to_number(c.c005))',
'then ''X'' ',
'  else null',
'    end Eliminar ',
'',
'from apex_collections c, tmp_com_fac_imp d',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and d.com_id = to_number(c.c001)',
'and d.emp_id = :f_emp_id',
'and d.uge_id = to_number(c.c005)',
'--and c.seq_id BETWEEN :P122_FAC_DESDE AND :P122_FAC_HASTA',
'ORDER BY C.SEQ_ID',
'',
''))
,p_display_when_condition=>':p122_fac_desde is not null and :p122_fac_hasta is not null'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No hay facturas para imprimir'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958262592715676)
,p_query_column_id=>1
,p_column_alias=>'NUMERO'
,p_column_display_sequence=>1
,p_column_heading=>'NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958377249715676)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958478568715676)
,p_query_column_id=>3
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958564937715676)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958655592715676)
,p_query_column_id=>5
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>5
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958782650715676)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958873443715676)
,p_query_column_id=>7
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>7
,p_column_heading=>'Unidad Gestion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289958964177715676)
,p_query_column_id=>8
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>8
,p_column_heading=>'Total'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(289959058298715676)
,p_query_column_id=>9
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>9
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:122:&SESSION.:ELIMINAR:&DEBUG.::P122_COM_ELIMINA:#COM_ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(289957274755715669)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(289957074405715669)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>'p122_com_num'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(289959154678715676)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(289958058314715675)
,p_button_name=>'impresion'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select count(*)',
'from apex_collections c, tmp_com_fac_imp d',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and d.com_id = to_number(c.c001)',
'and d.emp_id = :f_emp_id',
'and d.uge_id = to_number(c.c005)'))
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(289960857724715700)
,p_branch_action=>'f?p=&APP_ID.:122:&SESSION.:impresion:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(289959154678715676)
,p_branch_sequence=>20
,p_branch_comment=>'Created 21-JUL-2011 18:40 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(289961060060715707)
,p_branch_action=>'f?p=&APP_ID.:122:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 19-JUL-2011 12:28 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289955680495715662)
,p_name=>'P122_UGE_NUM_EST_SRI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_prompt=>'DIGITE LA NUEVA SECUENCIA A IMPRIMIR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289955858986715666)
,p_name=>'P122_PUE_NUM_SRI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289956077885715666)
,p_name=>'P122_COM_NUMERO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289956271846715667)
,p_name=>'P122_COM_NUM'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P122_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289956471942715667)
,p_name=>'P122_PUE_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_prompt=>'PUE_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289956666882715668)
,p_name=>'P122_NRO_FOLIO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_prompt=>'NRO_FOLIO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289956851901715668)
,p_name=>'P122_PUE_ELECTRONICO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(289955457715715661)
,p_prompt=>'Pue Electronico	'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289957467153715672)
,p_name=>'P122_FAC_DESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(289957074405715669)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Desde'
,p_source=>'1'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="valida_numero(this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'p122_com_num'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289957659274715674)
,p_name=>'P122_FAC_HASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(289957074405715669)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Hasta'
,p_source=>'1'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="valida_numero(this)"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'p122_com_num'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(289957871912715674)
,p_name=>'P122_COM_ELIMINA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(289957074405715669)
,p_prompt=>'com_borrar'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(289959462180715679)
,p_validation_name=>'P122_FAC_HASTA'
,p_validation_sequence=>10
,p_validation=>'P122_FAC_HASTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Tiene que ingresar un valor'
,p_when_button_pressed=>wwv_flow_imp.id(72750572459250708)
,p_associated_item=>wwv_flow_imp.id(289957659274715674)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(289959656185715685)
,p_validation_name=>'P122_FAC_DESDE'
,p_validation_sequence=>20
,p_validation=>'P122_FAC_DESDE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Tiene que ingresar un valor'
,p_when_button_pressed=>wwv_flow_imp.id(72750572459250708)
,p_associated_item=>wwv_flow_imp.id(289957467153715672)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(289959858994715686)
,p_validation_name=>'P122_COM_NUM'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p122_com_num = :P122_com_numero then',
'return null;',
'else',
':P122_com_num := null;',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_imp.id(289956271846715667)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(289959955338715686)
,p_process_sequence=>9
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_FAC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cursor cur_fac_temp is',
'      select *',
'        from tmp_com_fac_imp a',
'       where a.emp_id = :f_emp_id',
'         and a.UGE_ID = :F_UGE_ID;',
'',
'',
'',
'BEGIN',
'',
'',
'',
'pq_ven_comprobantes.pr_impresion_mayoreo(',
'pn_emp_id =>:F_EMP_ID,',
'pn_pca_id =>:F_PCA_ID,',
'pn_tse_id =>:f_seg_id,',
'pn_uge_id =>:F_UGE_ID,',
'pn_usu_id => :F_USER_ID,',
'pn_com_numero => :p122_com_numero,',
'pn_fac_desde =>:P122_FAC_DESDE,',
'pn_fac_hasta =>:P122_FAC_HASTA,',
'pv_com_aplica_negociacion => ''S'',',
'pv_error =>:P0_ERROR);',
':P122_FAC_DESDE:='''';',
':P122_FAC_HASTA:='''';',
':p122_com_num := null;',
':p122_com_numero := null;',
'',
'for c in cur_fac_temp loop    ',
'      --vacio la tabla temporal con las facturas a imprimir',
'      pq_ven_comprobantes_dml.pr_upd_tmp_fac_impresion(PN_COM_ID => c.com_id,',
'                                                       PV_ESTADO_REGISTRO => pq_constantes.fn_retorna_constante(null,''cv_estado_reg_inactivo''),',
'                                                       pv_error  => :p0_error);    ',
'end loop;',
'pq_ven_Comprobantes.pr_datos_folio(pn_emp_id          => :F_EMP_ID,',
'               pn_usu_id          => :F_USER_ID,',
'               pn_uge_id          => :F_UGE_ID,',
'               pn_ttr_id          => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'               pn_tgr_id          => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'               pn_pue_id          => :P122_PUE_ID,',
'               pv_pue_num_sri     => :P122_pue_num_sri,',
'               pv_uge_num_est_sri => :P122_uge_num_est_sri,',
'               pn_fol_sec_actual  => :P122_COM_NUMERO,',
'               pn_nro_folio       => :P122_NRO_FOLIO,',
'               pv_error           => :P0_error,',
'               pv_negociacion => ''S'');',
'pq_ven_comprobantes.pr_col_impresion_may_age(pn_emp_id => :f_emp_id,',
'                     pn_age_id_agencia=> :F_AGE_ID_AGENCIA,',
'                     pv_error  => :p0_error);',
'',
':P122_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P122_PUE_ID);',
'iF :P122_PUE_ELECTRONICO= ''E'' THEN ',
':P122_COM_NUM := :P122_COM_NUMERO;',
'end if;',
'',
'',
' EXCEPTION',
'   ',
'    WHEN OTHERS THEN',
'      ROLLBACK;',
'',
'            raise_application_error(-20000,''Error al genera factura''|| :P0_error||SQLERRM);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(289959154678715676)
,p_process_when=>'impresion'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>257706804068950760
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(289960182458715691)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_eliminar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'      pq_ven_comprobantes_dml.pr_del_tmp_fac_impresion(PN_COM_ID => :p122_com_elimina,',
'                                                       pv_error  => :p0_error);    ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>257707031188950765
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(289960360938715692)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'cargar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Begin',
'pq_ven_Comprobantes.pr_datos_folio(pn_emp_id          => :F_EMP_ID,',
'               pn_usu_id          => :F_USER_ID,',
'               pn_uge_id          => :F_uge_id,',
'               pn_ttr_id          => pq_constantes.fn_retorna_constante(null, ''cn_ttr_id_comprobante_may''),',
'               pn_tgr_id          => pq_constantes.fn_retorna_constante(null, ''cn_tgr_id_facturacion''),',
'               pn_pue_id          => :P122_PUE_ID,',
'               pv_pue_num_sri     => :P122_pue_num_sri,',
'               pv_uge_num_est_sri => :P122_uge_num_est_sri,',
'               pn_fol_sec_actual  => :P122_COM_NUMERO,',
'               pn_nro_folio       => :P122_NRO_FOLIO,',
'               pv_error           => :P0_error,',
'               pv_negociacion => ''S'');',
'',
'pq_ven_comprobantes.pr_col_impresion_may_age(pn_emp_id => :f_emp_id,',
'                     pn_age_id_agencia => :F_AGE_ID_AGENCIA,',
'                     pv_error  => :p0_error);',
'',
'/*:P122_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P122_PUE_ID);',
'iF :P122_PUE_ELECTRONICO= ''E'' THEN ',
':P122_COM_NUM := :P122_COM_NUMERO;',
'',
'',
'end if;',
'*/',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>257707209668950766
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(289960557392715692)
,p_process_sequence=>10
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cursor cur_col_facturas is',
'select to_number(c.seq_id) Numero,',
'to_number(c.c001) Com_id,',
'c.c002 Fecha,',
'to_number(c.c003) Cli_id,',
'c.c004 Cliente,',
'to_number(c.c005) Uge_id,',
'c.c006 Unidad_Gestion,',
'to_number(c.c007) TOTAL ',
'from apex_collections c',
'where c.collection_name=''COLL_FAC_MAYOREO''',
'and to_number(c.c005) = :F_UGE_ID',
'and to_number(c.seq_id) BETWEEN :P122_FAC_DESDE AND :P122_FAC_HASTA;',
'',
'ln_cuantos number;',
'begin',
'for x in cur_col_facturas loop',
'  select count(*)',
'  into ln_cuantos',
'  from tmp_com_fac_imp a',
'  where a.com_id = x.com_id',
'  and a.emp_id = :f_emp_id',
'  and a.uge_Id = x.uge_id;',
'  ',
'  if ln_cuantos > 0 then',
'    null;',
'  else',
'pq_ven_comprobantes_dml.pr_ins_tmp_fac_impresion(PN_COM_ID =>x.com_id,',
'                         PN_EMP_ID => :f_emp_id,',
'                         PN_UGE_ID => x.Uge_id,',
'                         PV_ESTADO_REGISTRO => pq_constantes.fn_retorna_constante(null,''cv_estado_reg_activo''),',
'--                         PN_AGE_ID_AGENCIA => :F_AGE_ID_AGENCIA,',
'                         pv_error  => :p0_error);',
'  end if;',
'end loop;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(72750572459250708)
,p_internal_uid=>257707406122950766
);
wwv_flow_imp.component_end;
end;
/
