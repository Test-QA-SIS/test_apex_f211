prompt --application/pages/page_00086
begin
--   Manifest
--     PAGE: 00086
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>86
,p_name=>'TRASPASO ANTICIPOS CLIENTE'
,p_alias=>'TRASPASO_ANTICIPOS_CLIENTE'
,p_step_title=>'TRASPASO ANTICIPOS CLIENTE'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'16'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240105112429'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(132988969429007352)
,p_plug_name=>'Traspaso de Anticipos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(132985071375998446)
,p_plug_name=>'Datos Clientes'
,p_parent_plug_id=>wwv_flow_imp.id(132988969429007352)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133053677619142736)
,p_plug_name=>'Valor'
,p_parent_plug_id=>wwv_flow_imp.id(132988969429007352)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P86_AUTORIZACION'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133060669925235152)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(133053677619142736)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition=>':p86_autorizacion = pq_ven_movimientos_caja.fn_claves_caja(:f_emp_id,:P86_TTR_ID,:P86_IDENTIFICACION,1,:P86_VALOR)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(133064579369294695)
,p_branch_action=>'f?p=&APP_ID.:86:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(133060669925235152)
,p_branch_sequence=>10
,p_branch_condition=>'GRABAR'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 21-JAN-2012 09:11 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132985259629998553)
,p_name=>'P86_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P86_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P86_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132985458069998598)
,p_name=>'P86_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132985680213998598)
,p_name=>'P86_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>80
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132985883340998599)
,p_name=>'P86_SALDO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Anticipos'
,p_pre_element_text=>'<b>'
,p_post_element_text=>'</b>'
,p_source=>'return pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P86_CLI_ID,:f_uge_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'SPAN STYLE="font-size: 16pt"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132986062574998607)
,p_name=>'P86_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Direcci&oacute;n'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132986274496998607)
,p_name=>'P86_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132986458200998607)
,p_name=>'P86_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Tel&eacute;fono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132986681966998607)
,p_name=>'P86_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132986872210998637)
,p_name=>'P86_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>80
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(132987077969998637)
,p_name=>'P86_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Tipo Identificaci&oacute;n'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133055071863150608)
,p_name=>'P86_VALOR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'Valor a Traspasar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>unistr('onchange="javascript:patronRe = /^\005Cd+\005C.?\005Cd*$/;if (!patronRe.test(this.value)) {alert(''Debe Ingresar un valor v\00BF\00BFlido'');this.value = '''';{html_GetElement(''P86_VALOR'').focus();}}" ')
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133055566106158372)
,p_name=>'P86_UGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133053677619142736)
,p_prompt=>'Unidad de Gestion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT u.uge_nombre, u.uge_id',
'  FROM asdm_unidades_gestion u',
' WHERE u.emp_id = :f_emp_id',
'   AND u.tug_id  in (pq_constantes.fn_retorna_constante(null,''cn_tug_id_agencia''))',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133055760198158384)
,p_name=>'P86_OBSERVACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(133053677619142736)
,p_prompt=>unistr('Observaci\00BF\00BFn')
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>65
,p_cMaxlength=>200
,p_cHeight=>2
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(140839153094619992)
,p_name=>'P86_AUTORIZACION'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_prompt=>'CLAVE DE AUTORIZACION'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''AUTORIZA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(140855667863690409)
,p_name=>'P86_TTR_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(132985071375998446)
,p_use_cache_before_default=>'NO'
,p_source=>'select pq_constantes.fn_retorna_constante(null,''cn_ttr_id_traspaso_egreso'') from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133062673650255115)
,p_validation_name=>'P86_VALOR'
,p_validation_sequence=>10
,p_validation=>'P86_VALOR'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Valor'
,p_when_button_pressed=>wwv_flow_imp.id(133060669925235152)
,p_associated_item=>wwv_flow_imp.id(133055071863150608)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133062953003258629)
,p_validation_name=>'P86_UGE_ID'
,p_validation_sequence=>20
,p_validation=>'P86_UGE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione la Unidad de Gestion'
,p_when_button_pressed=>wwv_flow_imp.id(133060669925235152)
,p_associated_item=>wwv_flow_imp.id(133055566106158372)
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133063166855262679)
,p_validation_name=>'P86_OBSERVACION'
,p_validation_sequence=>30
,p_validation=>'P86_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese las Observaciones'
,p_when_button_pressed=>wwv_flow_imp.id(133060669925235152)
,p_associated_item=>wwv_flow_imp.id(133055760198158384)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133065783786314847)
,p_validation_name=>'P86_IDENTIFICACION'
,p_validation_sequence=>40
,p_validation=>'P86_IDENTIFICACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Cliente'
,p_when_button_pressed=>wwv_flow_imp.id(133060669925235152)
,p_associated_item=>wwv_flow_imp.id(132985259629998553)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(140848655087639474)
,p_validation_name=>'P86_AUTORIZACION'
,p_validation_sequence=>50
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p86_autorizacion = pq_ven_movimientos_caja.fn_claves_caja(:f_emp_id,:P86_TTR_ID,:P86_IDENTIFICACION,1,:P86_VALOR)',
' then',
'return null;',
'else ',
':p86_AUTORIZACION := null;',
'return ''CLAVE INCORRECTA'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'CLAVE INCORRECTA'
,p_validation_condition=>'AUTORIZA'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(140839153094619992)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133028279742804946)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_dir_id number;',
'ln_CLI_ID_REFERIDO number;',
'ln_cliente_existe varchar2(5);',
'begin',
'pq_ven_listas2.pr_datos_clientes(',
':f_uge_id,',
':f_emp_id,',
':P86_IDENTIFICACION,',
':P86_NOMBRE,',
':P86_CLI_ID,',
':P86_CORREO,',
':P86_DIRECCION,',
':P86_TIPO_DIRECCION,        ',
':P86_TELEFONO,              ',
':P86_TIPO_TELEFONO,        ',
'ln_cliente_existe,',
'ln_dir_id,',
'ln_CLI_ID_REFERIDO ,',
':P86_NOMBRE_CLI_REFERIDO,',
':P86_TIPO_IDENTIFICACION,',
':P0_ERROR);',
':P86_VALOR := NULL;',
':P86_AUTORIZACION := NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>100775128473040020
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133063961361289494)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_traspaso'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'asdm_p.pq_ven_movimientos_caja.pr_traspaso_anticipos(pn_emp_id         => :f_emp_id,',
'                      pn_cli_id         => :p86_cli_id,',
'                      pn_valor          => :p86_valor,',
'                      pn_uge_id_origen  => :f_uge_id,',
'                      pn_uge_id_destino => :p86_uge_id,',
'                      pn_usu_id         => :f_user_id,',
'                      pn_pca_id         => :f_pca_id,',
'                      pv_observacion    => :p86_observacion,',
'                      pv_error          => :p0_error);',
'',
'if :p0_error is null then',
':p86_identificacion := null;',
':P86_NOMBRE := null;',
':P86_CLI_ID := null;',
':P86_CORREO := null;',
':P86_DIRECCION := null;',
':P86_TIPO_DIRECCION := null;     ',
':P86_TELEFONO := null;',
':P86_TIPO_TELEFONO := null;     ',
':P86_NOMBRE_CLI_REFERIDO := null;',
':P86_TIPO_IDENTIFICACION := null;',
':p86_saldo  := null;',
':p86_valor  := null;',
':p86_uge_id  := null;',
':p86_observacion := null;',
':P86_AUTORIZACION := NULL;',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'ERROR'
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Traspaso Realizado Correctamente'
,p_internal_uid=>100810810091524568
);
wwv_flow_imp.component_end;
end;
/
