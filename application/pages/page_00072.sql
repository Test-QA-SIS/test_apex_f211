prompt --application/pages/page_00072
begin
--   Manifest
--     PAGE: 00072
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>72
,p_name=>'GENERACION DE PROVISIONES'
,p_step_title=>'GENERACION DE PROVISIONES'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>'&G_META_TAG.'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(41040953413620037)
,p_plug_name=>'GENERAR NOTAS DE CREDITO'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523966992046668)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select nvl(max(pge_num_provision), 0)',
'  from car_provisiones_generadas',
' where uge_Id = :f_uge_id',
'   and pge_nota_credito is null) = 3'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(41306177208187548)
,p_name=>'Estado de la Tarea'
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *  FROM APEX_PLSQL_JOBS',
'where SYSTEM_STATUS = ''IN PROGRESS''',
'and JOB = :P72_PX_JOB_ID'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select nvl(max(pge_num_provision), 0)',
'  from car_provisiones_generadas',
' where uge_Id = :f_uge_id',
'   and pge_nota_credito is null) = 3'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41306477527187703)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41306569913187705)
,p_query_column_id=>2
,p_column_alias=>'JOB'
,p_column_display_sequence=>2
,p_column_heading=>'JOB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41306679449187705)
,p_query_column_id=>3
,p_column_alias=>'FLOW_ID'
,p_column_display_sequence=>3
,p_column_heading=>'FLOW_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41306783872187705)
,p_query_column_id=>4
,p_column_alias=>'OWNER'
,p_column_display_sequence=>4
,p_column_heading=>'OWNER'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41306854172187705)
,p_query_column_id=>5
,p_column_alias=>'ENDUSER'
,p_column_display_sequence=>5
,p_column_heading=>'ENDUSER'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41306977349187705)
,p_query_column_id=>6
,p_column_alias=>'CREATED'
,p_column_display_sequence=>6
,p_column_heading=>'CREATED'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41307070000187705)
,p_query_column_id=>7
,p_column_alias=>'MODIFIED'
,p_column_display_sequence=>7
,p_column_heading=>'MODIFIED'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41307168982187705)
,p_query_column_id=>8
,p_column_alias=>'STATUS'
,p_column_display_sequence=>8
,p_column_heading=>'STATUS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41307270780187705)
,p_query_column_id=>9
,p_column_alias=>'SYSTEM_STATUS'
,p_column_display_sequence=>9
,p_column_heading=>'SYSTEM_STATUS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41307359500187705)
,p_query_column_id=>10
,p_column_alias=>'SYSTEM_MODIFIED'
,p_column_display_sequence=>10
,p_column_heading=>'SYSTEM_MODIFIED'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41307465881187705)
,p_query_column_id=>11
,p_column_alias=>'SECURITY_GROUP_ID'
,p_column_display_sequence=>11
,p_column_heading=>'SECURITY_GROUP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(41566271552736354)
,p_name=>'ERRORES'
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'SELECT * fROM car_errores_rebajas WHERE UGE_iD = :F_UGE_ID'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select nvl(max(pge_num_provision), 0)',
'  from car_provisiones_generadas',
' where uge_Id = :f_uge_id',
'   and pge_nota_credito is null) = 3'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41566858070736865)
,p_query_column_id=>1
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'UGE_ID'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41566963608737073)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41567058007737073)
,p_query_column_id=>3
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>3
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(110488372380448314)
,p_plug_name=>'Generar Provisiones x Rebajas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(109513373859343677)
,p_plug_name=>'Provisiones Generadas'
,p_parent_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.PGE_ID,',
'       a.TPR_ID,',
'       a.UGE_ID,',
'       a.TRX_ID,',
'       decode(a.pge_mes_provision,',
'              1,',
'              ''ENERO'',',
'              2,',
'              ''FEBRERO'',',
'              3,',
'              ''MARZO'',',
'              4,',
'              ''ABRIL'',',
'              5,',
'              ''MAYO'',',
'              6,',
'              ''JUNIO'',',
'              7,',
'              ''JULIO'',',
'              8,',
'              ''AGOSTO'',',
'              9,',
'              ''SEPTIEMBRE'',',
'              10,',
'              ''OCTUBRE'',',
'              11,',
'              ''NOVIEMBRE'',',
'              12,',
'              ''DICIEMBRE'') PGE_MES_PROVISION,',
'       a.PGE_ANIO_PROVISION,',
'       a.PGE_NUM_PROVISION,',
'       a.PGE_VALOR_PROVISION,',
'       a.PGE_ESTADO_REGISTRO,',
'       a.PGE_NOTA_CREDITO,',
'       b.tpr_descripcion provision',
'  from car_provisiones_generadas a, car_tipos_provisiones b',
' where b.tpr_id = a.tpr_id',
'   and tnp_id = :F_EMP_ID',
'   and a.uge_id = :F_UGE_ID',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(109513556474343813)
,p_name=>'Provisiones Generadas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:74:&SESSION.::&DEBUG.::P74_TGE_ID:#PGE_ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#magnifying_glass_white_bg.gif" alt="">'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>77260405204578887
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109513663020343967)
,p_db_column_name=>'TPR_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Tipo Provision'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TPR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109513772004343993)
,p_db_column_name=>'PGE_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Pge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109513872076343993)
,p_db_column_name=>'UGE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109513953828343993)
,p_db_column_name=>'TRX_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'trx_id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109514176261343994)
,p_db_column_name=>'PGE_ANIO_PROVISION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Anio'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_ANIO_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109514256748343994)
,p_db_column_name=>'PGE_NUM_PROVISION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Numero Provision'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_NUM_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109514376094343994)
,p_db_column_name=>'PGE_VALOR_PROVISION'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Valor Provision'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_VALOR_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109514462399343994)
,p_db_column_name=>'PGE_ESTADO_REGISTRO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(109514575828343995)
,p_db_column_name=>'PGE_NOTA_CREDITO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Nota Credito'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_NOTA_CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63177651305663918)
,p_db_column_name=>'PROVISION'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Provision'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(40908754351370843)
,p_db_column_name=>'PGE_MES_PROVISION'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Mes Provisionado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_MES_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(109514677614343996)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'772616'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PGE_ID:PROVISION:TPR_ID:UGE_ID:TRX_ID:PGE_MES_PROVISION:PGE_ANIO_PROVISION:PGE_NUM_PROVISION:PGE_VALOR_PROVISION:PGE_ESTADO_REGISTRO:PGE_NOTA_CREDITO'
,p_sort_column_1=>'PGE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(41014955370636637)
,p_report_id=>wwv_flow_imp.id(109514677614343996)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PGE_NOTA_CREDITO'
,p_operator=>'is null'
,p_condition_sql=>'"PGE_NOTA_CREDITO" is null'
,p_condition_display=>'#APXWS_COL_NAME# #APXWS_OP_NAME#'
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(41015072290636696)
,p_report_id=>wwv_flow_imp.id(109514677614343996)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'PGE_VALOR_PROVISION'
,p_operator=>'>'
,p_expr=>'0'
,p_condition_sql=>'"PGE_VALOR_PROVISION" > to_number(#APXWS_EXPR#)'
,p_condition_display=>'#APXWS_COL_NAME# > #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(1005190383358015544)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT count(*) FROM APEX_PLSQL_JOBS',
'where SYSTEM_STATUS = ''IN PROGRESS''',
'and JOB = :P72_PX_JOB_ID'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(1005197757688016247)
,p_query_column_id=>1
,p_column_alias=>'COUNT(*)'
,p_column_display_sequence=>1
,p_column_heading=>'COUNT(*)'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(109515073504344184)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_button_name=>'GENERA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Generar Provision'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(111676675131452363)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_button_name=>'CONSULTAR_PROVISION'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Consultar Provision a Generar'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:75:&SESSION.::&DEBUG.:75::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(41042866011633124)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(41040953413620037)
,p_button_name=>'GENERA_NC'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'GENERAR NOTAS DE CREDITO'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(109516753260344665)
,p_branch_action=>'f?p=&APP_ID.:72:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 21-NOV-2011 18:51 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41102160402201963)
,p_name=>'P72_PX_JOB_RUNNING'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(41040953413620037)
,p_prompt=>'Tarea Ejecutandose'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41102372869205505)
,p_name=>'P72_PX_JOB_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(41040953413620037)
,p_prompt=>'ID TAREA'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(70229155931028031)
,p_name=>'P72_NUM_PROVISION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Numero de Provisiones:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select tnp_frecuencia_meses',
'  From car_tipos_nc_prov',
' where tnp_id =',
'       pq_constantes.fn_retorna_constante(null, ''cn_tnp_id_reb_ppgr'')',
'and emp_id = :f_emp_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(110491063638464711)
,p_name=>'P72_UGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Uge Id'
,p_source=>':F_UGE_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(110496378323554033)
,p_name=>'P72_UGE_DESCRIPCION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_prompt=>'Unidad de Gestion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select age.age_nombre_comercial',
'  from asdm_unidades_gestion uge, asdm_agencias age',
' where age.uge_id = uge.uge_id',
'   and age.emp_id = uge.emp_id',
'   and uge.emp_id = :f_emp_id',
'   and uge.uge_id = :p72_uge_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(110496756217554034)
,p_name=>'P72_FECHA_PROVISION'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nueva Provision'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select decode(to_number(substr(lpad(:p72_provision, 6, 0) , 0, 2)),',
'              01,',
'              ''ENERO'',',
'              02,',
'              ''FEBRERO'',',
'              03,',
'              ''MARZO'',',
'              04,',
'              ''ABRIL'',',
'              05,',
'              ''MAYO'',',
'              06,',
'              ''JUNIO'',',
'              07,',
'              ''JULIO'',',
'              08,',
'              ''AGOSTO'',',
'              09,',
'              ''SEPTIEMBRE'',',
'              10,',
'              ''OCTUBRE'',',
'              11,',
'              ''NOVIEMBRE'',',
'              12,',
'              ''DICIEMBRE'') || ''/'' || to_number(substr(lpad(:p72_provision, 6, 0) , 3, 6))',
'  from dual;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(110564456093068034)
,p_name=>'P72_PROVISION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(110488372380448314)
,p_prompt=>'Provision'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(41102661572211746)
,p_computation_sequence=>10
,p_computation_item=>'P72_PX_JOB_RUNNING'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'FOR c1 IN(',
'  SELECT 1',
'  FROM apex_plsql_jobs',
'  WHERE (job = :P72_PX_JOB_ID OR :P72_PX_JOB_ID IS NULL)',
'  AND system_status = ''COMPLETE''',
')LOOP',
'  RETURN ''No'';',
'END LOOP;',
'RETURN ''Yes'';'))
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(41107654571304381)
,p_computation_sequence=>10
,p_computation_item=>'G_META_TAG'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'IF :P72_PX_JOB_RUNNING = ''Yes'' THEN',
'  RETURN ''<meta http-equiv="refresh" content="10">'';',
'ELSE',
'  RETURN NULL;',
'END IF;'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(41044952467676448)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_generar_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  l_sql VARCHAR2(30000);',
'  l_job NUMBER;',
' l_session NUMBER := :app_session;',
'  l_user    VARCHAR2(1000) := :app_user;',
'  l_page_id NUMBER := :app_page_id;',
'  l_app_id  NUMBER := :app_id;',
'',
'BEGIN',
'',
'l_sql := ''declare lv_error varchar2(10000);   ',
'begin pq_car_provisiones.pr_genera_nc(''||:f_emp_id || '', ''||:f_uge_id || '', ''||:f_user_id || '', ''||:P72_NUM_PROVISION || '', ''||:p0_pue_id || '', ''||'' to_number('' || l_session || ''), '''''' || l_user || '''''', '' ||',
'                                   l_app_id || '', '' || :app_page_id ||'',lv_error);',
'EXCEPTION',
'    WHEN OTHERS THEN    ',
'      ROLLBACK;  ',
'',
'END;'';',
'',
'',
'  l_job := apex_plsql_job.submit_process(p_sql    => l_sql,',
'                                         p_status => ''Background process submitted'');',
'',
'  --store l_job for later reference',
'',
'  :P72_PX_JOB_ID      := l_job;',
'  :P72_PX_JOB_RUNNING := ''Yes'';',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(41042866011633124)
,p_internal_uid=>8791801197911522
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109516461008344622)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_genera_provision'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_car_provisiones.pr_provisiones(pn_emp_id       => :f_emp_id,',
'                     pn_uge_id       => :f_uge_id,',
'                     pn_uge_id_gasto => :f_uge_id_gasto,',
'                     pn_usu_id       => :f_user_id,',
'                     pn_mes_anio     => :P72_PROVISION,',
'                     pn_pue_id       => :p0_pue_id,',
'                     pn_tnp_id       => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_ppgr''),',
'                     pv_error        => :p0_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error'
,p_process_when_button_id=>wwv_flow_imp.id(109515073504344184)
,p_process_success_message=>'Provisiones Generadas Correctamente'
,p_internal_uid=>77263309738579696
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109697170974186420)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_Clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'LN_MES VARCHAR2(6) := null;',
'begin',
'',
'',
':p72_provision := pq_car_provisiones.fn_retorna_nueva_prov(pn_emp_id => :f_emp_id,',
'                                                      pn_uge_id => :f_uge_id,',
'                                                      pn_tnp_id => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_ppgr''));',
'',
'',
'IF apex_collection.collection_exists(p_collection_name => ''COL_PROVISIONES_PG'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COL_PROVISIONES_PG'');',
'end if;',
'',
'--actualizo los clientes excuidos',
'pq_car_cartera_interfaz.pr_clientes_mayoreo_nc(pn_emp_id => :f_emp_id,',
'                       pv_error  => :p0_error);',
'',
'',
'--cargo coleccion de las provisiones',
'pq_car_provisiones.pr_col_provisiones(pn_emp_id       => :f_emp_id,',
'                                      pn_uge_id       => :f_uge_id,',
'                                      pn_mes_anio     => :p72_provision,',
'                                      pn_tnp_id       => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_ppgr''),',
'                                      pn_age_id       => :F_AGE_ID_AGENCIA,',
'                                      pv_error        => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request != ''GENERA_NC'' AND :request != ''REGRESA'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>77444019704421494
);
wwv_flow_imp.component_end;
end;
/
