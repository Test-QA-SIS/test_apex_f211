prompt --application/pages/page_00052
begin
--   Manifest
--     PAGE: 00052
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>52
,p_name=>'Detalle Factura'
,p_step_title=>'Detalle Factura'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_updated_by=>'JTORRES'
,p_last_upd_yyyymmddhh24miss=>'20240206084107'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(69848268141463762)
,p_plug_name=>'Detalle Factura'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(69858272317463778)
,p_name=>'Detalle Factura'
,p_display_sequence=>2
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_comprobantes.fn_devuelve_var_comprobante(:P52_COM_ID,:f_emp_id,:p0_error);',
'',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(69858272317463778)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860381499463799)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860462383463799)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860576696463799)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860666402463799)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860759305463799)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860858666463799)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860974124463799)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861059492463799)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861161531463799)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861259482463799)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861356159463799)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861460858463799)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861569154463818)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861665903463818)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861754166463818)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861872915463818)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69861982596463818)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862068466463823)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862172653463823)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862263703463827)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862366198463827)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862455575463827)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862552984463827)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862660695463828)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862764023463828)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862858309463828)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69862963196463828)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863071605463828)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863159983463828)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863279076463828)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863365736463828)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863481040463828)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863556392463828)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863662822463828)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863773113463828)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863860531463828)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69863975020463828)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69864053144463828)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69864176013463828)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69864276925463828)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69864363232463828)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859561686463798)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859658982463798)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859760947463799)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859874871463799)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69858467560463778)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69858557774463778)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69858667924463778)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69858777364463798)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69858868538463798)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69858966684463798)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859055239463798)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859175455463798)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859258589463798)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859365114463798)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859461960463798)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69859983156463799)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860059404463799)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860180668463799)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69860269842463799)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(69864469419463829)
,p_name=>'Totales'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>35
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>4
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT vtt.vtt_etiqueta descripcion,',
'              round(cvar.vco_valor_variable,2)',
'FROM   ven_comprobantes          comp,',
'              Ven_Var_Comprobantes           cvar,',
'              ppr_variables_ttransaccion vtt',
'WHERE  comp.com_id = cvar.com_id',
'       AND cvar.var_id = vtt.var_id',
'       AND vtt.ttr_id = pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_comprobante'')',
'       AND vtt.vtt_mostrar_cabecera = pq_constantes.fn_retorna_constante(NULL,''cv_si'')',
'       AND comp.com_estado_registro  = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'       AND comp.emp_id =:f_emp_id',
'       AND comp.com_id = :P52_COM_ID',
' ORDER BY vtt.vtt_orden',
'',
'}'';'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'NO_HEADINGS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120883954881346429)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884065344346444)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884151272346444)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884276168346444)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884381444346444)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884452304346444)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884555389346444)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884658292346444)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884770211346445)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884868372346445)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120884978651346445)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885059734346445)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885175538346445)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885252267346445)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885357210346445)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885453583346448)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885563519346448)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885652470346448)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885762422346448)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885876517346448)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120885956530346448)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886058345346449)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886176520346449)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886256234346449)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886357923346449)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886474512346449)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886578368346449)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886682348346449)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886783548346449)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886856549346449)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120886953254346449)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887058846346449)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887153296346449)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887263313346449)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887381329346449)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887451911346449)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887565946346449)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887680128346450)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887753128346450)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887874601346450)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120887956858346450)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888064745346450)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888151512346450)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888265140346450)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888382668346450)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888463518346450)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888563734346450)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888678252346450)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888768714346450)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888864977346450)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120888962967346450)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889070039346451)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889166724346451)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889260840346451)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889373260346451)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889482284346451)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889555101346451)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889679938346451)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889777634346451)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(120889875577346451)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(70831061644751349)
,p_plug_name=>'observacion'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>45
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(101166583136190253)
,p_plug_name=>'Reimpresion de Documentos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>55
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P52_VALIDA_CONS_CARTERA = 0 or :P52_REGRESAR_PAGINA =0'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(39279473263586166)
,p_plug_name=>'Impresion Documentos desde Sistema'
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>64
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(46369076279929244)
,p_plug_name=>'Impresion Documentos Preimpresos'
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>65
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(46645755908184954)
,p_plug_name=>'Impresion Documentos IMPRENTA'
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>75
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(52931078300947548)
,p_plug_name=>'DOCUMENTO ELECTRONICO'
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.sco_id,a.sco_autorizacion autorizacion, a.sco_id descargar_xml',
'  from asdm_p.V_ASDM_SRI_COMPROBANTES a',
' where a.SCO_COMPROBANTE = :P52_COM_ID',
' and a.tgr_id in (1,5,7)',
' and a.sco_autorizacion is not null'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':F_USER_ID in (86,418,106,14,3267) OR pq_asdm_docs_xml.fn_tipo_punto_com(pn_com_id =>NVL(:P52_COM_ID,0))=''E'''
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(52931166572947549)
,p_name=>'Documento electronico'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_search_bar=>'N'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'PBERNAL'
,p_internal_uid=>20678015303182623
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(52931356666947564)
,p_db_column_name=>'SCO_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Sco Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SCO_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(52934878397987662)
,p_db_column_name=>'DESCARGAR_XML'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Descargar Xml'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_format_mask=>'DOWNLOAD:V_ASDM_SRI_COMPROBANTES:SCO_COMP_AUTORIZADO:SCO_ID::SCO_TIPO_DATO::::Attachment:Download'
,p_tz_dependent=>'N'
,p_static_id=>'DESCARGAR_XML'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(52935767806022953)
,p_db_column_name=>'AUTORIZACION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Autorizacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AUTORIZACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(52931759456950930)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'206787'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'AUTORIZACION:DESCARGAR_XML:'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(63963951626469966)
,p_name=>unistr('Impresi\00F3n de Titulos de Propiedad')
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_TABFORM'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"ROWID",',
'"CDI_ID",',
'"CDE_ID",',
'"ITE_SKU_ID",',
'"EMP_ID",',
'"CDI_IDENTIFICADOR",',
'"CDI_ESTADO_REGISTRO"',
'from "ASDM_E"."VEN_COMPROBANTES_DET_IDEN"',
'WHERE com_id_ = :p52_com_id',
''))
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63967964681469989)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Select Row'
,p_use_as_row_header=>'N'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964273204469970)
,p_query_column_id=>2
,p_column_alias=>'ROWID'
,p_column_display_sequence=>2
,p_column_heading=>'Rowid'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'ROWID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964382773469972)
,p_query_column_id=>3
,p_column_alias=>'CDI_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Cdi Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'CDI_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964474647469972)
,p_query_column_id=>4
,p_column_alias=>'CDE_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Cde Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'CDE_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964562564469972)
,p_query_column_id=>5
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Producto'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'POPUPKEY'
,p_named_lov=>wwv_flow_imp.id(303280661312814696)
,p_lov_show_nulls=>'NO'
,p_column_width=>60
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'ITE_SKU_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964679641469972)
,p_query_column_id=>6
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'EMP_ID'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964776415469972)
,p_query_column_id=>7
,p_column_alias=>'CDI_IDENTIFICADOR'
,p_column_display_sequence=>7
,p_column_heading=>'Nro.Serie'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'CDI_IDENTIFICADOR'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63964881967469972)
,p_query_column_id=>8
,p_column_alias=>'CDI_ESTADO_REGISTRO'
,p_column_display_sequence=>8
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_column_width=>12
,p_ref_schema=>'ASDM_P'
,p_ref_table_name=>'VEN_COMPROBANTES_DET_IDEN'
,p_ref_column_name=>'CDI_ESTADO_REGISTRO'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(26901452059196322998)
,p_plug_name=>'Acta de Entrega Recepcion'
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>90
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(149666279910290186128)
,p_plug_name=>'Impresiones Tricimotos'
,p_parent_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>100
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(149666280000411186129)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(149666279910290186128)
,p_button_name=>'IMPRIMIR_TRICI'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(140178449769885418553)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(149666279910290186128)
,p_button_name=>'IMPRIMIR_TRICI_TODOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Imprimir Todos'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63350396214891991753)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'Btn_regresar'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:7:&SESSION.::&DEBUG.:RP::'
,p_button_condition=>':P52_PAG_ID= 7'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7470887030269185151)
,p_button_sequence=>120
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'ET'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Et'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_NC_1_RE.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.&PN_TIPO_IMPRESION= ''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(80186895875800362660)
,p_button_sequence=>240
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BTN_IMPRIMIR_AUTORIZACION_DEBITO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'IMPRIMIR AUTORIZACION DEBITO'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.CXC_ID',
'FROM ASDM_E.CAR_CUENTAS_POR_COBRAR A,',
'    ASDM_E.CAR_COMPROBANTES_PAGOS_DEBITO B',
'WHERE A.CXC_ID = B.CXC_ID',
'      AND A.COM_ID = :P52_COM_ID;'))
,p_button_condition_type=>'EXISTS'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(180663744576732342)
,p_button_sequence=>250
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'Bt_promociones'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_image_alt=>'Detalle Promociones'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:163:&SESSION.::&DEBUG.:RP:P163_COM_ID,F_POPUP:&P52_COM_ID.,S'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(152177478375449695073)
,p_button_sequence=>260
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'btn_reimpresion_encargo_fiduciario'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Encargo Fiduciario'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select cx.forma_credito',
'from asdm_e.car_cuentas_por_cobrar cx',
'where cx.com_id = :p52_com_id',
'      and cx.cxc_cuota_balon is null',
'      and cx.forma_credito = ''E'';'))
,p_button_condition_type=>'EXISTS'
,p_button_comment=>unistr('RULLOA R3396-04 25/11/2021 Bot\00F3n que servir\00E1 para reimprimir el Encargo Fiduciario.')
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(101181652717285568)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(39279473263586166)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63965269933469977)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(63963951626469966)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Insertar Registro'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:apex.widget.tabular.addRow();'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(41645554683701633)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRESION_FACTURA_MAY'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Factura'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:popupURL(''../../reports/rwservlet?module=VEN_COMPROBANTE_FAC_MASIVO&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&P_FAC_DESDE=&P52_COM_ID.&P_FAC_HASTA=&P52_COM_ID.&P_EMP_ID=&F_EMP_ID.&P_AGE_ID=&P52_UGE_ID.&P_ESTADO_IMPRESION=R'')'||wwv_flow.LF||
''||wwv_flow.LF||
''||wwv_flow.LF||
''||wwv_flow.LF||
''
,p_button_condition=>':P52_TSE_ID = pq_constantes.fn_retorna_constante(0,''cn_tse_id_mayoreo'')'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(95587467685288853)
,p_button_sequence=>82
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRESION_FACTURA_MIN'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Factura Minoreo'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript: fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_FAC&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_EMP_ID=&F_EMP_ID.&PN_COM_ID=&P52_COM_ID.&PN_TIPO_IMPRESION=R''))'
,p_button_condition_type=>'NEVER'
,p_button_comment=>':P52_TSE_ID = pq_constantes.fn_retorna_constante(0,''cn_tse_id_minoreo'')'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(505616854765823580)
,p_button_sequence=>85
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRESION_FAC_MOTO'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Factura Minoreo Moto'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_FAC_MOTO.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_EMP_ID=&F_EMP_ID.&PN_COM_ID=&P52_COM_ID.&PN_TIPO_IMPRESION=R''))'
,p_button_condition_type=>'NEVER'
,p_button_comment=>':F_EMP_ID = 1'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(80599466243371508)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRIME_ND'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'REIMPRIME ND'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_ND.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.&PN_TIPO_IMPRESION=R''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(153163874137325947)
,p_button_sequence=>110
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRESION_NOTA_CREDITO'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Nota Credito'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_NC_&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.&PN_TIPO_IMPRESION= ''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(46373277888995938)
,p_button_sequence=>120
,p_button_plug_id=>wwv_flow_imp.id(46369076279929244)
,p_button_name=>'IMPRIMIR_BLANCO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(46646853531212734)
,p_button_sequence=>130
,p_button_plug_id=>wwv_flow_imp.id(46645755908184954)
,p_button_name=>'IMPRESION_DOCUMENTOS_IMPRENTA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Impresion'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(2184708762193108293)
,p_button_sequence=>140
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'MOTOS'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Motos'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:popupURL(''../../reports/rwservlet?module=VEN_CONTRATO_RESERVA_DOMINIO.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_EMP_ID=&F_EMP_ID.&PN_COM_ID=&P52_COM_ID. '')'||wwv_flow.LF||
''||wwv_flow.LF||
''||wwv_flow.LF||
''||wwv_flow.LF||
''||wwv_flow.LF||
''||wwv_flow.LF||
''
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(3504814161007778009)
,p_button_sequence=>150
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BT_REIMRIME_PUNTUALITO'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimrime Puntualito'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=CAR_PUNTUALITO.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.&PN_TIPO_IMPRESION=R&PN_CLI_ID=&P52_CLI_ID.&PN_TTR_ID=304''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1453464936861107282)
,p_button_sequence=>160
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BT_REIMRIME_PUNTUALITO2'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimprime Puntualito'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:52:&SESSION.:imprime_puntualito:&DEBUG.:::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(4249841977775562470)
,p_button_sequence=>170
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BT_REIMPRIME_RESERVA'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimprimir Reserva de Dominio'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN__RESERVA_DOM.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(4258919773600110115)
,p_button_sequence=>180
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BT_REIMPRIME_CONT_RESERVA'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimprime Contrato de Reserva'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_CONTRATO_RESERVA_DOMINIO.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.''))'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(43411126093630450667)
,p_button_sequence=>190
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BT_IMPRIMIR_ACTA_PLAN_REF'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Impresion Acta Plan Referido'
,p_button_position=>'BOTTOM'
,p_button_condition=>':P52_ACTA_PLAN_REFERIDO >= 1'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42175058560439313)
,p_button_sequence=>200
,p_button_plug_id=>wwv_flow_imp.id(52931078300947548)
,p_button_name=>'IMPRESION_DEL_DOCUMENTO_ELECTRONICO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>unistr('Impresi\00F3n Del Documento ')
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(16314820369966192611)
,p_button_sequence=>200
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRESION_FACTURA_NEGOCIADA'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Factura Negociada'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:popupURL(''../../reports/rwservlet?module=VEN_COMPR_FAC_MASIVO_NEG&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&P_FAC_DESDE=&P52_COM_ID.&P_FAC_HASTA=&P52_COM_ID.&P_EMP_ID=&F_EMP_ID.&P_AGE_ID=&P52_UGE_ID.&P_ESTADO_IMPRESION=R'')'||wwv_flow.LF||
''||wwv_flow.LF||
''
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P52_TSE_ID = pq_constantes.fn_retorna_constante(0,''cn_tse_id_mayoreo'')',
'AND :P52_NEGOCIACION = ''S'''))
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(52940274134271037)
,p_button_sequence=>210
,p_button_plug_id=>wwv_flow_imp.id(52931078300947548)
,p_button_name=>'REENVIO_DE__DOCUMENTO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Enviar Documento'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(3952105751357336570)
,p_button_sequence=>270
,p_button_plug_id=>wwv_flow_imp.id(70831061644751349)
,p_button_name=>'CERTIFICADOGEXINDUCCION'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Certificado Gex Induccion'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:popupURL(''../../reports/rwservlet?module=VEN_GEX_INDUCCION&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.'')'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63965074267469977)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(63963951626469966)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Grabar'
,p_button_position=>'CHANGE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63964961948469977)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(63963951626469966)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63965170285469977)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(63963951626469966)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(84472474486906810)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'REGRESA_CONSULTA_CARTERA'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresa Consulta Cartera'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=241:57:&SESSION.::&DEBUG.:RP::'
,p_button_condition=>':P52_VALIDA_CONS_CARTERA = 1'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(84474460596950107)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'REGRESA_LISTA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar '
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:41:&SESSION.::&DEBUG.:52::'
,p_button_condition=>':P52_VALIDA_CONS_CARTERA =0 and :P52_PAG_ID!= 7'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7637003972123840938)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'P52_REGRESAR_PAGINA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresar '
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:149:&SESSION.::&DEBUG.:52::'
,p_button_condition=>':P52_REGRESAR_PAGINA =0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(106862462059649767)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'REGRESA_PAGO_CUOTA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresa Pago Cuota'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:::'
,p_button_condition=>':P52_VALIDA_CONS_CARTERA = 2'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(106975759372330188)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'REGRESA_LISTA_PAGO_CUOTA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresa Lista Facturas'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:65:&SESSION.::&DEBUG.:::'
,p_button_condition=>':P52_VALIDA_CONS_CARTERA = 3'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(68661271703556404)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'REGRESA_PAGO_CUOTA_RET'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresa Pago Cuota Retencion'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:64:&SESSION.::&DEBUG.:::'
,p_button_condition=>':P52_VALIDA_CONS_CARTERA = 4'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(143355283163010140)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_button_name=>'REGRESA_CONSULTA_MOVIMIENTOS_CARTERA'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< Regresa Consulta Movimientos Cartera'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=241:12:&SESSION.::NO:::'
,p_button_condition=>':P52_VALIDA_CONS_CARTERA = 5'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_comment=>':P52_VALIDA_CONS_CARTERA = 5'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(41323368355088833)
,p_button_sequence=>180
,p_button_plug_id=>wwv_flow_imp.id(63963951626469966)
,p_button_name=>'BT_IMPRIME_TITULO_PROPIEDAD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Titulos de Propiedad'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=REP_VEN_TITULO_PROPIEDAD.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.''))'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(3132568967016307332)
,p_button_sequence=>180
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'BT_IMPRESION__NC_COMBOS'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'Impresion NC Combos'
,p_button_position=>'TOP'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_NC_12.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&PN_EMP_ID=&F_EMP_ID.&PN_TIPO_IMPRESION=I''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63974570522014838)
,p_button_sequence=>190
,p_button_plug_id=>wwv_flow_imp.id(63963951626469966)
,p_button_name=>'BT_CARGAR_IDENTIFICADORES'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar Identificadores'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(20573695283757647900)
,p_button_sequence=>200
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'PROVISIONAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Provisionar'
,p_button_position=>'TOP'
,p_button_condition=>':f_user_id=3267'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(28495254982866971903)
,p_button_sequence=>210
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'REIMPRESION_FACTURA_CUARMUEBLES'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimpresion Factura Cuarmuebles'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript: fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_CUA&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_EMP_ID=&F_EMP_ID.&PN_COM_ID=&P52_COM_ID.&PN_TIPO_IMPRESION=R''))'
,p_button_condition=>':F_EMP_ID = 8'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(15357071215413221073)
,p_button_sequence=>220
,p_button_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_button_name=>'IMPRESION_FACTURA_CUARMUEBLES_1'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Impresion Factura Cuarmuebles'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript: fn_unlock(html_PopUp(''../../reports/rwservlet?module=VEN_COMPROBANTE_CUA&F_EMP_ID..rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_EMP_ID=&F_EMP_ID.&PN_COM_ID=&P52_COM_ID.''))'
,p_button_condition=>':F_EMP_ID = 8'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(26901506474780327474)
,p_button_sequence=>230
,p_button_plug_id=>wwv_flow_imp.id(26901452059196322998)
,p_button_name=>'IMPRIMIR_ACTA_TITULAR_1'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Acta Titular 1'
,p_button_position=>'TOP'
,p_button_redirect_url=>'javascript: fn_unlock(html_PopUp(''../../reports/rwservlet?module=ACTA_ENTREGA_RECEPCION.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&titular=S''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(26901581376691333030)
,p_button_sequence=>240
,p_button_plug_id=>wwv_flow_imp.id(26901452059196322998)
,p_button_name=>'IMPRIMIR_ACTA_ARRENDATARIO_1'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Acta Arrendatario 1'
,p_button_position=>'TOP'
,p_button_redirect_url=>'javascript: fn_unlock(html_PopUp(''../../reports/rwservlet?module=ACTA_ENTREGA_RECEPCION.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PN_COM_ID=&P52_COM_ID.&titular=N''))'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1426347569746917164)
,p_button_sequence=>250
,p_button_plug_id=>wwv_flow_imp.id(26901452059196322998)
,p_button_name=>'IMPRIMIR_ACTA_TITULAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Acta Titular'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:52:&SESSION.:imprime_titular:&DEBUG.:::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1426347670009917165)
,p_button_sequence=>260
,p_button_plug_id=>wwv_flow_imp.id(26901452059196322998)
,p_button_name=>'IMPRIMIR_ACTA_ARRENDATARIO'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir Acta Arrendatario'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:52:&SESSION.:imprime_arrendatario:&DEBUG.:::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(42207077872779022)
,p_branch_name=>'52'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(42175058560439313)
,p_branch_sequence=>1
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(63968065641469989)
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>2
,p_branch_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(102670563122277619)
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:IMPRIMIR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(101181652717285568)
,p_branch_sequence=>5
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'IMPRIMIR'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 26-OCT-2011 15:29 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(69868273266463852)
,p_branch_action=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.:52::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'Grabar_aprobacion_comercial,Grabar_aprobacion'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(69868468230463852)
,p_branch_action=>'f?p=&APP_ID.:11:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(98820363325368282)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(46377165254030059)
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:IMPRIMIR_BLANCO:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(46373277888995938)
,p_branch_sequence=>30
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'IMPRIMIR_BLANCO'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 19-OCT-2012 11:01 by ARIVERA'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(46647980805239485)
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:IMPRESION_DOCUMENTOS_IMPRENTA:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(46646853531212734)
,p_branch_sequence=>40
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'IMPRESION_DOCUMENTOS_IMPRENTA'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 22-OCT-2012 17:23 by ARIVERA'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(149666280426221186133)
,p_branch_name=>'Go To Page 52 Trici'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:IMPRIMIR_TRICI:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(149666280000411186129)
,p_branch_sequence=>50
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'IMPRIMIR_TRICI'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 22-OCT-2012 17:23 by ARIVERA'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(140178450014084418555)
,p_branch_name=>'Go To Page 52 Trici Todos'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:IMPRIMIR_TRICI_TODOS:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(140178449769885418553)
,p_branch_sequence=>60
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'IMPRIMIR_TRICI_TODOS'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 22-OCT-2012 17:23 by ARIVERA'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(69868660145463852)
,p_branch_name=>'Go To Page 52'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>70
,p_branch_condition_type=>'NEVER'
,p_branch_comment=>'Created 21-AGO-2010 11:31 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(43411126220167450668)
,p_branch_name=>'BR_IMPRIMIR_ACTA_PLAN_REFERIDO'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:IMPRIMIR_ACTA_PR:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(43411126093630450667)
,p_branch_sequence=>80
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(80186896003151362661)
,p_branch_name=>'BR_IMPRIME_AUTORIZACION_DEBITO'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:AUTORIZACION:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(80186895875800362660)
,p_branch_sequence=>90
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(152177478537734695074)
,p_branch_name=>'br_imprime_encargo_fiduciario'
,p_branch_action=>'f?p=&APP_ID.:52:&SESSION.:ENCARGO_FIDUCIARIO:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(152177478375449695073)
,p_branch_sequence=>100
,p_branch_comment=>unistr('RULLOA R3396-04 25/11/2021 Bot\00F3n que servir\00E1 para reimprimir el Encargo Fiduciario.')
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41496556219812564)
,p_name=>'P52_TSE_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_use_cache_before_default=>'NO'
,p_prompt=>'tse_id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.tse_id',
'FROM ven_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41672172269801341)
,p_name=>'P52_UGE_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_use_cache_before_default=>'NO'
,p_prompt=>'UGE_ID'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.UGE_ID_FACTURA',
'FROM v_ven_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(46372672993966060)
,p_name=>'P52_DOCUMENTOS_PRE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(46369076279929244)
,p_prompt=>'Documentos'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC : Cronograma de Pagos;1, Pagare a la orden de Marcimex Sociedad Anonima;2, Reserva de dominio;3, Constacia de Dacion en pago;4, Solicitud de Cupo;5 , Reseva de dominio MOTOS; 6'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(46646464004196760)
,p_name=>'P52_DOCUMENTOS_IMP'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(46645755908184954)
,p_prompt=>'Documentos'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'STATIC : ',
'         RESERVA DE DOMINIO;2,',
'         SOLICITUD DE CUPO (PREIMPRESA SIC);4,',
'         TABLA DE AMORTIZACION;5, ',
'         SOLICITUD DE CUPO (ANTERIOR);7,',
'         PAGARE MARCIMEX (SIC);8,',
'         Reseva de dominio MOTOS (PREIMPRESA SIC); 9'))
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>'STATIC : Pagare a la orden de Marcimex Sociedad Anonima;1, Reserva de dominio;2, Constacia de Dacion en pago;3,Solicitud de Cupo;4,Tabla de Amortizacion;5'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(49186371708816118)
,p_name=>'P52_AGE_ID_AGENCIA'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Age Id Agencia'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    age_id',
'FROM      asdm_agencias',
'WHERE     uge_id = :F_UGE_ID',
'AND       ROWNUM = 1'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52940177791264227)
,p_name=>'P52_MAIL'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(52931078300947548)
,p_prompt=>'Mail'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>70
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69849651409463771)
,p_name=>'P52_FECHA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Fecha'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.COM_FECHA',
'FROM ven_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69849868191463772)
,p_name=>'P52_CLIENTE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cliente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.nombre_completo',
'FROM  v_ven_comprobantes c ',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69850053261463772)
,p_name=>'P52_COM_NUM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Factura #'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.UGE_NUM_SRI||''-''||c.PUE_NUM_SRI||''-''||c.COM_NUMERO',
'FROM v_ven_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69850263096463772)
,p_name=>'P52_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'com_id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69850865594463773)
,p_name=>'P52_CLI_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Cli Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.CLI_ID',
'FROM v_ven_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69851076450463773)
,p_name=>'P52_POLITICA'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Politica'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.pol_descripcion_larga',
'FROM v_ven_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69851268308463773)
,p_name=>'P52_AGENTE'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Vendedor'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(age.usu_id) agente',
'FROM v_ven_comprobantes c,',
'     ASDM_AGENTES age',
'WHERE c.COM_ID=:P52_COM_ID',
'AND c.AGE_ID_AGENTE = age.age_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69851469242463773)
,p_name=>'P52_PLAZO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Plazo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  c.vco_valor_variable',
'FROM ven_var_comprobantes c',
'WHERE c.COM_ID=:P52_COM_ID',
'and c.var_Id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_plz_fact'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(70733461120346637)
,p_name=>'P52_NUM_IDENTI'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Num. Identificacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  vcli.per_nro_identificacion',
'FROM v_asdm_clientes_personas vcli , ven_comprobantes c',
'WHERE c.cli_id=vcli.cli_id',
'AND c.COM_ID=:P52_COM_ID	'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(70806065978440452)
,p_name=>'P52_OBSERVACION'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(70831061644751349)
,p_prompt=>unistr('Observaci\00F3n')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.COM_OBSERVACION ||',
'       pq_ven_ordenes_servicios.fn_detalle_aplica_garantia(pn_ord_id => c.ord_id)',
'  FROM ven_comprobantes c',
' WHERE c.COM_ID = :P52_COM_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(84472768037914357)
,p_name=>'P52_VALIDA_CONS_CARTERA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_prompt=>'Valida Cons Cartera'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(101177875049254137)
,p_name=>'P52_DOCUMENTO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(39279473263586166)
,p_prompt=>'Documento'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'STATIC2:PAGARE;2,',
'        SEGURO DE ACCIDENTES;3,',
'        CERTIFICADO DE GARANTIA;4,        ',
'        CERTIFICADO COMPRA PROTEGIDA;35,',
'        RESERVA DE DOMINIO;9,',
'        RESERVA DE DOMINIO MOTORIZADOS;10,',
'        RESERVA DE DOMINIO MOTOS (ANTERIOR);31,        ',
'        CARTA DE VENTA;15,',
'        CONTRATO COMPRA VENTA (VTA CONTADO);20,',
'        TABLA DE AMORTIZACION;33,',
'        SOLICITUD DE CUPO;34,',
'        ENTREGA RECEPCION Y DESLINDE RESPOSABILIDAD (MOTOS TIPO B);35'))
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7637004083953840939)
,p_name=>'P52_REGRESAR_PAGINA'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(16314822172917237249)
,p_name=>'P52_NEGOCIACION'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Negociacion'
,p_source=>'SELECT l.com_aplica_negociacion FROM VEN_COMPROBANTES L WHERE l.com_id = :P52_COM_ID'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43411125958654450666)
,p_name=>'P52_ACTA_PLAN_REFERIDO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(101166583136190253)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT COUNT (A.CPR_ID)',
'FROM ASDM_E.REF_COBRO_PREMIOS A',
'WHERE A.EMP_ID = :f_emp_id',
'AND   A.COM_ID_REFERIDO = :P52_COM_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63350396086080991752)
,p_name=>'P52_PAG_ID'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(69848268141463762)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(149666280495804186134)
,p_name=>'P52_TIPO_TRICI'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(149666279910290186128)
,p_prompt=>'Documentos'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'STATIC:',
'        Carta Venta;CV,',
unistr('        Caracter\00EDsticas;CA,'),
'        Certificado;CE,',
'        Certificado SRI;CS,',
'        Improntas;IM,',
'        Entrega - Recepcion;ER,',
'        Certificado Cilindraje;CC,',
'        Certificado Tonelaje;CT'))
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>'STATIC : Pagare a la orden de Marcimex Sociedad Anonima;1, Reserva de dominio;2, Constacia de Dacion en pago;3,Solicitud de Cupo;4,Tabla de Amortizacion;5'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63966051457469983)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'CDE_ID not null'
,p_validation_sequence=>30
,p_validation=>'CDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'CDE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63966268312469986)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'CDE_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'CDE_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'CDE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63966465593469987)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'ITE_SKU_ID not null'
,p_validation_sequence=>40
,p_validation=>'ITE_SKU_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'ITE_SKU_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63966664045469987)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'ITE_SKU_ID must be numeric'
,p_validation_sequence=>40
,p_validation=>'ITE_SKU_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'ITE_SKU_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63966877044469987)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'EMP_ID not null'
,p_validation_sequence=>50
,p_validation=>'EMP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'EMP_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63967063867469987)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'EMP_ID must be numeric'
,p_validation_sequence=>50
,p_validation=>'EMP_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'EMP_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63967254826469988)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'CDI_IDENTIFICADOR not null'
,p_validation_sequence=>60
,p_validation=>'CDI_IDENTIFICADOR'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'CDI_IDENTIFICADOR'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63967458226469988)
,p_tabular_form_region_id=>wwv_flow_imp.id(63963951626469966)
,p_validation_name=>'CDI_ESTADO_REGISTRO not null'
,p_validation_sequence=>70
,p_validation=>'CDI_ESTADO_REGISTRO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'#COLUMN_HEADER# must have a value.'
,p_when_button_pressed=>wwv_flow_imp.id(63965074267469977)
,p_associated_column=>'CDI_ESTADO_REGISTRO'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42190462098695838)
,p_process_sequence=>5
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRESION_DEL_DOCUMENTO_ELECTRONICO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' BEGIN',
' ',
' pq_asdm_docs_xml.pr_imprimir_sco_com(',
' pn_sco_id =>:f_sco_id,',
'pn_sco_comprobante=>:P52_COM_ID,',
' pn_emp_id =>:f_emp_id);',
' ',
' ',
'   exception',
'  ',
'  ',
'    when others then',
'      ',
'    NULL;',
' ',
' END ;',
'',
'',
':f_sco_id:=null;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':f_sco_id is not null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>9937310828930912
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(43411126313060450669)
,p_process_sequence=>15
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIME_PLAN_REFERIDO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'       pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''VEN_ACTA_PLAN_REFERIDOS'',',
'                                               ---pn_ambiente_id       => 3,',
'                                               pv_nombre_parametros => ''pn_com_id_referido:pn_emp_id'',',
'                                               pv_valor_parametros  => :P52_COM_ID ||'':''|| :f_emp_id,',
'                                               pn_emp_id            => :f_emp_id,',
'                                               pv_formato           => ''pdf'',',
'                                               pv_error             => :p0_error); '))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIR_ACTA_PR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>43378873161790685743
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(80186896066766362662)
,p_process_sequence=>25
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REIMPRIME_AUT_DEBITO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'LN_CXC_ID NUMBER;',
'BEGIN',
'',
'SELECT MAX(CXC_ID)',
'INTO LN_CXC_ID',
'FROM ASDM_E.CAR_CUENTAS_POR_COBRAR',
'WHERE COM_ID = :P52_COM_ID;',
'',
'',
'                                            ',
' pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CAR_AUTORIZACION_DEBITO'',',
'                                            --pn_ambiente_id       => 3,',
'                                            pv_nombre_parametros => ''pn_uge_id:pn_cxc_id'',',
'                                            pv_valor_parametros  => :f_uge_id||'':''||LN_CXC_ID,',
'                                            pn_emp_id            => :f_emp_id,',
'                                            pv_formato           => ''pdf'',',
'                                            pv_error             => :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'AUTORIZACION'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>80154642915496597736
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(152177478563189695075)
,p_process_sequence=>35
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_encargo_fiduciario'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'Modificacion: RULLOA',
'Descripcion:  Procedimiento usado para reimprimir el Encargo Fiduciario.',
'Fecha:        27/11/2021',
'Historia:     R3396-04',
'*/',
'Declare',
'  ln_pol_id       number;',
'  ln_cxc_id       number;',
'  ln_cxc_id_balon number;',
'begin',
'',
'  select c.pol_id, c.cxc_id',
'    into ln_pol_id, ln_cxc_id',
'    from asdm_e.car_cuentas_por_cobrar c',
'   where c.com_id = :p52_com_id',
'     and c.cxc_cuota_balon is null;',
'',
unistr('  if ln_pol_id = 766 /*Pol\00EDtica Cuota Bal\00F3n Marcimex*/'),
'   then',
'    begin',
'      select cb.cxc_id',
'        into ln_cxc_id_balon',
'        from asdm_e.car_cuentas_por_cobrar cb',
'       where cb.cxc_cuota_balon = ln_cxc_id;',
'    exception',
'      when others then',
'        ln_cxc_id_balon := null;',
'    end;',
'    if ln_cxc_id_balon is not null then',
'      pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CAR_CONTRATO_ENCARGO_FIDUCIARIO'',',
'                                              --pn_ambiente_id       => fn_get_ambiente(''REP''),',
'                                              pv_nombre_parametros => ''pn_cxc_id'',',
'                                              pv_valor_parametros  => ln_cxc_id_balon,',
'                                              pn_emp_id            => :f_emp_id,',
'                                              pv_formato           => ''pdf'',',
'                                              pv_error             => :p0_error);',
'    end if;',
'  end if;',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CAR_CONTRATO_ENCARGO_FIDUCIARIO'',',
'                                          --pn_ambiente_id       => fn_get_ambiente(''REP''),',
'                                          pv_nombre_parametros => ''pn_cxc_id'',',
'                                          pv_valor_parametros  => ln_cxc_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => :p0_error);',
'exception',
'  when others then',
'    raise_application_error(-20000,',
'                            ''No se puede reimprimir el Encargo Fiduciario'');',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ENCARGO_FIDUCIARIO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>152145225411919930149
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1426347711560917166)
,p_process_sequence=>50
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_acta_titular'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_error varchar(255);',
'begin',
'    pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre => ''ACTA_ENTREGA_RECEPCION'',',
'                                            pv_nombre_parametros => ''PN_COM_ID:TITULAR'',',
'                                            pv_valor_parametros => :P52_COM_ID||'':''||''S'',',
'                                            pn_emp_id => :f_emp_id,',
'                                            pv_formato => ''pdf'',',
'                                            pv_error => lv_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprime_titular'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1410221135926034703
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1426347843009917167)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_acta_arrendatario'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_error varchar(255);',
'begin',
'    pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre => ''ACTA_ENTREGA_RECEPCION'',',
'                                            pv_nombre_parametros => ''PN_COM_ID:TITULAR'',',
'                                            pv_valor_parametros => :P52_COM_ID||'':''||''N'',',
'                                            pn_emp_id => :f_emp_id,',
'                                            pv_formato => ''pdf'',',
'                                            pv_error => lv_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprime_arrendatario'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1410221267375034704
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1453464992931107283)
,p_process_sequence=>70
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIME_PUNTUALITO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_error varchar2(250);',
'',
'begin',
'    pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CAR_PUNTUALITO'',',
'                                                pv_nombre_parametros => ''PN_CLI_ID:PN_COM_ID:PN_TTR_ID'',',
'                                                pv_valor_parametros  => :P52_CLI_ID||'':''||:P52_COM_ID||'':''||304,',
'                                                pn_emp_id            => :F_EMP_ID,',
'                                                pv_formato           => ''pdf'',',
'                                                pv_error             => lv_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprime_puntualito'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1437338417296224820
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52942374789318504)
,p_process_sequence=>4
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_envio_mail'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_error varchar2(10000);',
'ln_SCO_ID number;',
'LN_SCO_AUTORIZACION NUMBER;',
'begin',
'  ',
'if trim(:P52_MAIL) is not null then',
'  ',
'',
'select a.sco_id , A.SCO_AUTORIZACION into  ln_SCO_ID, LN_SCO_AUTORIZACION from ven_comprobantes a where a.com_id=:P52_COM_ID;',
'',
'IF LN_SCO_AUTORIZACION IS NOT NULL THEN',
'   --dlopez 03/06/2016 para poblar variables nuevas por com solidaria',
'      delete from ven_comprobantes_temp where com_id = :P52_COM_ID;',
'      pq_ven_nota_credito.pr_pobla_var_facturas(pn_emp_id => :f_emp_id,',
'                                                pn_com_id => :P52_COM_ID,',
'                                                pv_error  => :p0_error);',
'      BEGIN',
'        ',
'       pq_ven_comprobantes.pr_poblar_tabla_temp(pn_com_id => :P52_COM_ID,',
'pn_emp_id=>:f_emp_id,',
'                                               pv_error  => :P0_ERROR);--',
'                                               ',
'        :P0_ERROR:=NULL;',
'                                               ',
'commit;',
'       ',
'       EXCEPTION',
'            WHEN OTHERS THEN',
'            null;',
'            end;',
'            ',
'/*',
'',
'kseg_p.pq_kseg_firma_digital.pr_notifica_comprobante(pn_sco_id =>ln_SCO_ID,',
'                                                       pn_emp_id =>:f_emp_id,',
'                                                       PV_DIR_MAIL_ALTE => :P52_MAIL,',
'                                                       pv_error  =>lv_error);',
'                                      */                 ',
'----pbernal 13/02/2017        ',
'kseg_p.pq_kseg_facturacion_elect.pr_solicita_correo(',
'pn_sco_id =>ln_SCO_ID,',
'pn_usu_id=>:f_user_id,',
'pn_emp_id =>:f_emp_id,',
'PV_DIR_MAIL_ALTE => :P52_MAIL,',
'pv_error  =>lv_error);',
'                           ',
'                                                     ',
'                                                       ',
'ELSE',
'  :P0_error:=''No se puede enviar el mail si el documento no esta autorizado'';',
'END IF;',
'                                                       ',
'else',
':P0_error:=''Ingrese el mail'';',
'end if;',
'',
'end ;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(52940274134271037)
,p_internal_uid=>20689223519553578
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42248460224960250)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'IMPRESION_DEL_DOCUMENTO_ELECTRONICO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'            ',
'            ',
'            ',
'            ',
'            declare ',
'ln_sco_id  asdm_sri_comprobantes.sco_id%type;',
'lv_sco_autorizacion  asdm_sri_comprobantes.sco_autorizacion%type;',
'ln_fol_nro_folio asdm_folios.fol_nro_folio%type;',
'lv_fol_documento_electronico ASDM_FOLIOS.FOL_DOCUMENTO_ELECTRONICO%TYPE;',
'begin',
'',
'',
'select a.sco_id ,a.sco_autorizacion,a.fol_nro_folio into ln_sco_id ,lv_sco_autorizacion , ln_fol_nro_folio from ven_comprobantes a where a.com_id=:P52_COM_ID;',
'',
'if ln_sco_id is not null then',
'',
' :f_sco_id :=ln_sco_id;',
'',
'  --dlopez 03/06/2016 para poblar variables nuevas por com solidaria',
'      delete from ven_comprobantes_temp where com_id = :P52_COM_ID;',
'      pq_ven_nota_credito.pr_pobla_var_facturas(pn_emp_id => :f_emp_id,',
'                                                pn_com_id => :P52_COM_ID,',
'                                                pv_error  => :p0_error);',
'',
'',
'',
'',
'      BEGIN',
'        ',
'       pq_ven_comprobantes.pr_poblar_tabla_temp(pn_com_id => :P52_COM_ID,',
'pn_emp_id=>:f_emp_id,',
'                                               pv_error  => :P0_ERROR);--',
'                                               ',
'        :P0_ERROR:=NULL;',
'                                               ',
'',
'       ',
'       EXCEPTION',
'            WHEN OTHERS THEN',
'            null;',
'            end;',
'            ',
'            ',
' ',
' else',
'    :f_sco_id:=null;',
' select f.fol_documento_electronico into lv_fol_documento_electronico from asdm_folios f where f.fol_nro_folio=ln_fol_nro_folio;',
' ',
' if lv_fol_documento_electronico=''S'' then',
unistr('      :P0_error:=''El documento es electr\00F3nico espere unos segundos para que se registre en el SRI'';'),
'   else',
unistr('   :P0_error:=''El documento no es electr\00F3nico'';'),
'   end if;',
'   ',
'   ',
' end if;',
' end ;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(42175058560439313)
,p_internal_uid=>9995308955195324
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63967574511469988)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_imp.id(63963951626469966)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_COMPROBANTES_DET_IDEN'
,p_attribute_03=>'ROWID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(63965074267469977)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
,p_internal_uid=>31714423241705062
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63967753951469988)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_imp.id(63963951626469966)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_COMPROBANTES_DET_IDEN'
,p_attribute_03=>'ROWID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
,p_internal_uid=>31714602681705062
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63973973367920978)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_identificadores'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   CURSOR cu_comprobante_detalle IS',
'      SELECT    *',
'      FROM      ven_comprobantes_det',
'      WHERE     com_id = :p52_com_id',
'      AND       cde_estado_registro = ''0''',
'      AND       cde_id NOT IN(SELECT    cde_id',
'                              FROM      ven_comprobantes_det_iden);',
'BEGIN',
'   FOR d IN cu_comprobante_detalle LOOP',
'      INSERT INTO ven_comprobantes_det_iden(cdi_id, ',
'                                            cde_id, ',
'                                            ite_sku_id, ',
'                                            emp_id, ',
'                                            cdi_identificador, ',
'                                            cdi_estado_registro,',
'                                            com_id_)',
'      VALUES(NULL,',
'             d.cde_id,',
'             d.ite_sku_id,',
'             :F_EMP_ID,',
'             NULL,',
'             0,',
'             :p52_com_id);',
'   END LOOP;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(63974570522014838)
,p_internal_uid=>31720822098156052
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(20593427178794838706)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PROVISIONAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  pn_com_id          ven_comprobantes.com_id%TYPE;',
'  pn_cli_id          ven_comprobantes.cli_id%TYPE;',
'  pn_usu_id          asdm_agentes.usu_id%TYPE;',
'  pn_monto_nc        NUMBER;',
'  pn_monto_total_fac NUMBER;',
'  pn_com_id_fac      ven_comprobantes.com_id%TYPE;',
'  pn_uge_id          ven_comprobantes.uge_id%type;',
'  pn_age_id_agencia  ven_comprobantes.age_id_agencia%type;',
'  pn_pue_id          ven_comprobantes.pue_id%type;',
'  lv_pasa            varchar(50);',
'  CURSOR cu_nc_rebaja is',
'    select *',
'      from ven_comprobantes',
'     where COM_ID=2485925;',
'BEGIN',
'',
'  FOR cu_nc IN cu_nc_rebaja LOOP',
'  ',
'    pn_com_id := cu_nc.com_id;',
'    lv_pasa   := ''a'';',
'    SELECT co.cli_id,',
'           age.usu_id,',
'           vc.vco_valor_variable,',
'           co.pue_id,',
'           co.com_id_ref,',
'           co.uge_id,',
'           co.age_id_agencia',
'      INTO pn_cli_id,',
'           pn_usu_id,',
'           pn_monto_nc,',
'           pn_pue_id,',
'           pn_com_id_fac,',
'           pn_uge_id,',
'           pn_age_id_agencia',
'      FROM ven_comprobantes co, asdm_agentes age, ven_var_comprobantes vc',
'     WHERE co.com_id = pn_com_id',
'       AND co.age_id_agente = age.age_id',
'       AND vc.com_id = co.com_id',
'       AND vc.var_id = 955; --2379900',
'    lv_pasa := ''aa'';',
'  ',
'    SELECT round(SUM(CASE',
'                       WHEN c.com_tipo = ''F'' THEN',
'                        vd.vcd_valor_variable',
'                       WHEN c.com_tipo = ''NC'' THEN',
'                        vd.vcd_valor_variable * -1',
'                     END),',
'                 2)',
'      INTO pn_monto_total_fac',
'      FROM ven_comprobantes         c,',
'           ven_comprobantes_det     d,',
'           ven_var_comprobantes_det vd,',
'           inv_items_categorias     ic,',
'           asdm_clientes            cl,',
'           asdm_personas            pe',
'     WHERE trunc(c.com_fecha) >= ''01/06/2015'' -- 01-02-2015 HASTA:28-02-2015',
'       AND trunc(c.com_fecha) <= ''30/06/2015''',
'       AND C.COM_TIPO = ''F''',
'       AND d.com_id = c.com_id',
'       AND d.ite_sku_id = ic.ite_sku_id',
'       AND d.cde_id = vd.cde_id',
'       AND vd.var_id = 1002',
'          ',
'       AND ic.cat_id IN',
'           (SELECT cat_id FROM asdm_e.ven_parametrizacion_cat_nc)',
'       AND cl.cli_id = c.cli_id',
'       AND c.cli_id = pn_cli_id',
'       AND cl.per_id = pe.per_id',
'       AND c.tse_id =',
'           asdm_p.pq_constantes.fn_retorna_constante(NULL,',
'                                                     ''cn_tse_id_mayoreo'')',
'          ',
'       AND d.ite_sku_id NOT IN',
'           (select ite_sku_id',
'              from asdm_e.ven_items_excl_nc nc',
'             where trunc(nc.ien_fecha_inicio) <= trunc(sysdate)',
'               and trunc(nc.ien_fecha_fin) >= trunc(sysdate)',
'               and nc.ien_estado_registro =',
'                   asdm_p.pq_constantes.fn_retorna_constante(null,',
'                                                             ''cv_estado_reg_activo''));',
'  ',
'    lv_pasa := ''b''; --2379900',
'  ',
'    -- Call the procedure',
'    lv_pasa := ''c'';',
'    pq_ven_ncredito_rebaja.pr_genera_prov_sol(pn_com_id          => pn_com_id,',
'                                              pn_cli_id          => pn_cli_id,',
'                                              pd_fecha_inicio    => ''01/06/2015'',',
'                                              pd_fecha_fin       => ''30/06/2015'',',
'                                              pn_emp_id          => :f_emp_id,',
'                                              pn_uge_id          => pn_uge_id,',
'                                              pn_usu_id          => pn_usu_id,',
'                                              pn_age_id_agencia  => pn_AGE_ID_AGENCIA,',
'                                              pn_pue_id          => pn_pue_id,',
'                                              pn_monto_nc        => pn_monto_nc,',
'                                              pn_monto_total_fac => pn_monto_total_fac,',
'                                              pn_com_id_fac      => pn_com_id_fac,',
'                                              pv_error           => :p0_error);',
'  END LOOP;',
'exception',
'  when others then',
'    raise_application_error(-20000,',
'                            ''pv_error: '' || lv_pasa || ''-com_id'' ||',
'                            pn_com_id || SQLERRM);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(20573695283757647900)
,p_internal_uid=>20561174027525073780
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(46374072611013332)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMP_DOCUMENTOS_PREIMPRESOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  pq_car_imprimir.pr_imprimir_doc_datos(:F_emp_id    ,',
'                                 :P52_COM_NUM  ,',
'                                 :P52_COM_ID     ,',
'                                 :P52_FECHA      ,',
'                                 :P52_NUM_IDENTI,',
'                                 :P52_CLI_ID    ,',
'                                 :P52_DOCUMENTOS_PRE  ,',
'                                 :p0_error    );',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIR_BLANCO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>14120921341248406
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(46647278596229370)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imp_doc_imprenta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_car_imprimir.pr_imprimir_doc_imprenta( :F_emp_id ,',
'                                          :P52_COM_ID ,',
'                                          :P52_FECHA,',
'                                          :P52_CLI_ID ,',
'                                          :P52_DOCUMENTOS_IMP ,',
'                                          :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRESION_DOCUMENTOS_IMPRENTA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>14394127326464444
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(101199461599354343)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_DOCUMENTO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_tipo varchar2(10);',
'begin',
'    begin',
'        select cc.forma_credito',
'        into lv_tipo',
'        from asdm_e.car_cuentas_por_cobrar cc',
'        where cc.cxc_id in',
'          (select max(cxc_id)',
'           from asdm_e.car_cuentas_por_cobrar tt ',
'           where tt.com_id = :P52_COM_ID and tt.cxc_estado_registro = ''0'');',
'    exception',
'        when no_data_found then',
'            lv_tipo := ''M'';',
'    end;',
'    ',
'    if lv_tipo = ''E'' and :P52_DOCUMENTO not in (''33'', ''4'', ''35'', ''15'') then',
unistr('        apex_error.add_error(p_message          => ''Cuando el cr\00E9dito es Electr\00F3nico, solo se puede imprimir la Tabla de amortizaci\00F3n, Certificados y Carta de Venta'','),
'                            p_display_location => apex_error.c_inline_with_field_and_notif,',
'                            p_page_item_name   => ''p0_error'');',
'',
'    else',
'        pq_car_imprimir.pr_imprimir_doc_caja(pn_emp_id =>     :F_emp_id    ,   ',
'                                       pn_mon_id =>     1   ,            ',
'                                       pn_com_num =>    :P52_COM_NUM  ,  ',
'                                       pn_com_id =>     :P52_COM_ID     ,',
'                                       pd_fecha =>      :P52_FECHA      ,',
'                                       pv_num_identi => :P52_NUM_IDENTI, ',
'                                       pn_cli_id =>     :P52_CLI_ID     ,',
'                                       pn_documento =>  :P52_DOCUMENTO  ,',
'                                       pv_error =>      :p0_error);',
'    end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>unistr('Cuando el cr\00E9dito es Electr\00F3nico, solo se puede imprimir el pagare')
,p_process_when=>'IMPRIMIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>68946310329589417
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(149666280206494186131)
,p_process_sequence=>35
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_trici'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'    Autor:       MFIDROVO',
'    Fecha:       29-06-2021',
'    Descripcion: Proceso Tricimotos para ejecutar reporte',
'    Historia:    R3264-07',
'*/',
'DECLARE',
'  lv_error    VARCHAR(1000);',
'  ln_ambiente NUMBER;',
'  lv_reporteo VARCHAR(100);',
'BEGIN',
'',
'  ln_ambiente := fn_get_ambiente(''REP'');',
'   apex_application.g_print_success_message := ''<span style="color:black"> '' ||  :P52_TIPO_TRICI||  '' </span>'';',
'  IF :P52_TIPO_TRICI = ''CV'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Carta_Venta'';',
'  END IF;',
'',
'  IF :P52_TIPO_TRICI = ''CA'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Cert_Caract'';',
'  END IF;',
'',
'  IF :P52_TIPO_TRICI = ''CE'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Certificado'';',
'  END IF;',
'',
'  IF :P52_TIPO_TRICI = ''CS'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Certificado_Sri'';',
'  END IF;',
'',
'  IF :P52_TIPO_TRICI = ''IM'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Improntas'';',
'  END IF;',
'',
'  IF :P52_TIPO_TRICI = ''ER'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Entrega_Recepcion'';',
'  END IF;',
'  ',
'  IF :P52_TIPO_TRICI = ''CC'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Cert_Cilindraje'';',
'  END IF;',
'  ',
'  IF :P52_TIPO_TRICI = ''CT'' THEN',
'    lv_reporteo := ''Ven_Tricimoto_Cert_Tonelaje'';',
'  END IF;',
'',
'  IF lv_reporteo IS NOT NULL THEN',
'  ',
'   ',
'  ',
'    pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => lv_reporteo,',
'                                            --pn_ambiente_id       => ln_ambiente,',
'                                            pv_nombre_parametros => ''P_COM_ID'',',
'                                            pv_valor_parametros  => :P52_COM_ID,',
'                                            pn_emp_id            => :f_emp_id,',
'                                            pv_formato           => ''pdf'',',
'                                            pv_error             => lv_error);',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIR_TRICI'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>149634027055224421205
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(140178449871368418554)
,p_process_sequence=>45
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_trici_todos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'Para que imprimi todos los reportes',
'*/',
'DECLARE',
'  lv_error    VARCHAR(1000);',
'  ln_ambiente NUMBER;',
'  lv_reporteo VARCHAR(100);',
'BEGIN',
'',
'  ln_ambiente                              := fn_get_ambiente(''REP'');',
'  apex_application.g_print_success_message := ''<span style="color:black"> '' ||',
'                                              :p52_tipo_trici || '' </span>'';',
'                                              ',
'  ',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Carta_Venta'',',
'                                          --pn_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Cert_Caract'',',
'                                          --_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Certificado'',',
'                                          --pn_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Certificado_Sri'',',
'                                          --pn_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Improntas'',',
'                                          --n_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Entrega_Recepcion'',',
'                                          --pn_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'  ',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Cert_Cilindraje'',',
'                                          --pn_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'                                          ',
'  pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Ven_Tricimoto_Cert_Tonelaje'',',
'                                          --pn_ambiente_id       => ln_ambiente,',
'                                          pv_nombre_parametros => ''P_COM_ID'',',
'                                          pv_valor_parametros  => :p52_com_id,',
'                                          pn_emp_id            => :f_emp_id,',
'                                          pv_formato           => ''pdf'',',
'                                          pv_error             => lv_error);',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIR_TRICI_TODOS'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>140146196720098653628
);
wwv_flow_imp.component_end;
end;
/
