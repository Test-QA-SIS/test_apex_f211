prompt --application/pages/page_00100
begin
--   Manifest
--     PAGE: 00100
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>100
,p_name=>'Listado Facturas Todas'
,p_step_title=>'Listado Facturas Todas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(39216571685596085)
,p_plug_name=>'Listado Facturas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'col.C001 emp_id,',
'TO_NUMBER(col.C002) com_id,',
'col.C003 Folio,',
'col.C004 Fecha,',
'col.C005 Cli_id,',
'col.C006 Cliente,',
'col.C007 Tipo,',
'col.C008 pol_id,',
'col.C009 Plazo,',
'col.C010 ord_id,',
'col.C011 ord_Sec_id,',
'col.C012 age_id_agente,',
'col.C013 age_id_agencia,',
'col.C014 Agencia,',
'col.C015 com_sec_id,',
'col.C016 Contable,',
'col.C017 ,',
'col.C018 ,',
'col.C019 ,',
'col.C020 ,',
'col.C021 ,',
'col.C022 ,',
'col.C023 ,',
'col.C024 ,',
'col.C025 ,',
'col.C026 ,',
'col.C027 ,',
'col.C028 ,',
'col.C029 ,',
'col.C030 ,',
'',
'    (select tve.tve_descripcion from ven_terminos_venta tve',
'where vco.vco_valor_variable = tve.tve_id) termino_venta,',
' (select hc.secuencia_factura',
'          from asdm_homologacion_com hc',
'         where to_number(col.c001) = hc.emp_id',
'           and to_number(col.c002) = hc.com_id) sec_factura_orac',
'  from apex_collections col, ven_var_comprobantes vco',
' where col.collection_name =',
'       pq_constantes.fn_retorna_constante(null, ''cv_col_comprobantes'')',
' and to_Number(col.c001) = vco.emp_id',
' and to_number(col.c002) = vco.com_id',
' and vco.var_id = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_tve'');',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(39216777191596085)
,p_name=>'Listado Facturas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y_OF_Z'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_csv_output_separator=>';'
,p_detail_link=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.::P52_COM_ID,P52_AGENTE,P52_VALIDA_CONS_CARTERA:#COM_ID#,#AGE_ID_AGENTE#,0'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>6963625921831159
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219281562596109)
,p_db_column_name=>'C017'
,p_display_order=>17
,p_column_identifier=>'S'
,p_column_label=>'&P100_COL_HEAD1.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219370009596110)
,p_db_column_name=>'C018'
,p_display_order=>18
,p_column_identifier=>'T'
,p_column_label=>'&P100_COL_HEAD2.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219475584596110)
,p_db_column_name=>'C019'
,p_display_order=>19
,p_column_identifier=>'U'
,p_column_label=>'&P100_COL_HEAD3.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219565406596110)
,p_db_column_name=>'C020'
,p_display_order=>20
,p_column_identifier=>'V'
,p_column_label=>'&P100_COL_HEAD4.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219655658596110)
,p_db_column_name=>'C021'
,p_display_order=>21
,p_column_identifier=>'W'
,p_column_label=>'&P100_COL_HEAD5.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219756474596110)
,p_db_column_name=>'C022'
,p_display_order=>22
,p_column_identifier=>'X'
,p_column_label=>'&P100_COL_HEAD6.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219870777596110)
,p_db_column_name=>'C023'
,p_display_order=>23
,p_column_identifier=>'Y'
,p_column_label=>'&P100_COL_HEAD7.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219974674596110)
,p_db_column_name=>'C024'
,p_display_order=>24
,p_column_identifier=>'Z'
,p_column_label=>'&P100_COL_HEAD8.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39216874539596088)
,p_db_column_name=>'C025'
,p_display_order=>25
,p_column_identifier=>'AA'
,p_column_label=>'&P100_COL_HEAD9.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39216980726596103)
,p_db_column_name=>'C026'
,p_display_order=>26
,p_column_identifier=>'AB'
,p_column_label=>'&P100_COL_HEAD10.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217079844596103)
,p_db_column_name=>'C027'
,p_display_order=>27
,p_column_identifier=>'AC'
,p_column_label=>'&P100_COL_HEAD11.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217168021596103)
,p_db_column_name=>'C028'
,p_display_order=>28
,p_column_identifier=>'AD'
,p_column_label=>'&P100_COL_HEAD12.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217280919596103)
,p_db_column_name=>'C029'
,p_display_order=>29
,p_column_identifier=>'AE'
,p_column_label=>'&P100_COL_HEAD13.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217362370596104)
,p_db_column_name=>'C030'
,p_display_order=>30
,p_column_identifier=>'AF'
,p_column_label=>'&P100_COL_HEAD14.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217471708596104)
,p_db_column_name=>'TERMINO_VENTA'
,p_display_order=>31
,p_column_identifier=>'BO'
,p_column_label=>'Termino Venta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TERMINO_VENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217554323596104)
,p_db_column_name=>'SEC_FACTURA_ORAC'
,p_display_order=>32
,p_column_identifier=>'BP'
,p_column_label=>'Sec Factura Orac'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SEC_FACTURA_ORAC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217671622596104)
,p_db_column_name=>'EMP_ID'
,p_display_order=>33
,p_column_identifier=>'BQ'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217771003596104)
,p_db_column_name=>'COM_ID'
,p_display_order=>34
,p_column_identifier=>'BR'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217870124596104)
,p_db_column_name=>'FOLIO'
,p_display_order=>35
,p_column_identifier=>'BS'
,p_column_label=>'Folio'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39217953202596104)
,p_db_column_name=>'FECHA'
,p_display_order=>36
,p_column_identifier=>'BT'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218068899596105)
,p_db_column_name=>'CLI_ID'
,p_display_order=>37
,p_column_identifier=>'BU'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218172117596105)
,p_db_column_name=>'CLIENTE'
,p_display_order=>38
,p_column_identifier=>'BV'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218268458596105)
,p_db_column_name=>'TIPO'
,p_display_order=>39
,p_column_identifier=>'BW'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218364885596105)
,p_db_column_name=>'POL_ID'
,p_display_order=>40
,p_column_identifier=>'BX'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218481910596105)
,p_db_column_name=>'PLAZO'
,p_display_order=>41
,p_column_identifier=>'BY'
,p_column_label=>'Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218552531596105)
,p_db_column_name=>'ORD_ID'
,p_display_order=>42
,p_column_identifier=>'BZ'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218665888596105)
,p_db_column_name=>'ORD_SEC_ID'
,p_display_order=>43
,p_column_identifier=>'CA'
,p_column_label=>'Ord Sec Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218777467596109)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>44
,p_column_identifier=>'CB'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218873697596109)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>45
,p_column_identifier=>'CC'
,p_column_label=>'Age Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39218957812596109)
,p_db_column_name=>'AGENCIA'
,p_display_order=>46
,p_column_identifier=>'CD'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219078438596109)
,p_db_column_name=>'COM_SEC_ID'
,p_display_order=>47
,p_column_identifier=>'CE'
,p_column_label=>'Com Sec Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39219164042596109)
,p_db_column_name=>'CONTABLE'
,p_display_order=>48
,p_column_identifier=>'CF'
,p_column_label=>'Contable'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CONTABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(39220078311596111)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'69670'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'SEC_FACTURA_ORAC:COM_SEC_ID:FOLIO:FECHA:CLI_ID:CLIENTE:TIPO:TERMINO_VENTA:PLAZO:ORD_SEC_ID:CONTABLE:C017:C018:C019:C020:C021:C022:C023:COM_ID:AGENCIA'
,p_sort_column_1=>'C002'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'TO_NUMBER(COL.C002)'
,p_sort_direction_2=>'DESC'
,p_sort_column_3=>'C003'
,p_sort_direction_3=>'DESC'
,p_sort_column_4=>'C004'
,p_sort_direction_4=>'DESC'
,p_sort_column_5=>'C011'
,p_sort_direction_5=>'DESC'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(39181476844285349)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(39216571685596085)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39181669399285374)
,p_name=>'P100_FECHADESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(39216571685596085)
,p_item_default=>'trunc(sysdate)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'FechaDesde'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39181878926285384)
,p_name=>'P100_FECHAHASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(39216571685596085)
,p_item_default=>'trunc(sysdate)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'FechaHasta'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39182064674285388)
,p_name=>'P100_COL_HEAD1'
,p_item_sequence=>30
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head1'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39182261011285395)
,p_name=>'P100_COL_HEAD2'
,p_item_sequence=>40
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head2'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39182455690285395)
,p_name=>'P100_COL_HEAD3'
,p_item_sequence=>50
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head3'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39182655029285395)
,p_name=>'P100_COL_HEAD4'
,p_item_sequence=>60
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head14'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39182860795285397)
,p_name=>'P100_COL_HEAD5'
,p_item_sequence=>70
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head5'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39183052290285397)
,p_name=>'P100_COL_HEAD6'
,p_item_sequence=>80
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head6'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39183254556285398)
,p_name=>'P100_COL_HEAD7'
,p_item_sequence=>90
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head7'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39183468930285398)
,p_name=>'P100_COL_HEAD8'
,p_item_sequence=>100
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head8'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39183682094285398)
,p_name=>'P100_COL_HEAD9'
,p_item_sequence=>110
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head9'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39183852848285398)
,p_name=>'P100_COL_HEAD10'
,p_item_sequence=>120
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head10'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39184081478285398)
,p_name=>'P100_COL_HEAD11'
,p_item_sequence=>130
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head11'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39184268527285399)
,p_name=>'P100_COL_HEAD12'
,p_item_sequence=>140
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head12'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39184469064285399)
,p_name=>'P100_COL_HEAD13'
,p_item_sequence=>150
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head13'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39184666224285399)
,p_name=>'P100_COL_HEAD14'
,p_item_sequence=>160
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head14'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39184878814285399)
,p_name=>'P100_COL_HEAD15'
,p_item_sequence=>170
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Col Head15'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(39185951500367290)
,p_name=>'P100_UGE_ID'
,p_item_sequence=>25
,p_item_plug_id=>wwv_flow_imp.id(39216571685596085)
,p_item_default=>'188'
,p_prompt=>'Agencia'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select u.uge_nombre d, u.uge_id r',
'  from asdm_unidades_gestion u',
' where u.emp_id = 1',
'   and u.tug_id = 1',
'union',
'select ''TODAS'' d, NULL r from dual',
'',
''))
,p_cSize=>90
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(39185078109285400)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_COLECCION'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'  CURSOR cu_variables_tran IS',
'     select ',
'             replace(c.vtt_etiqueta,',
'                     '''',',
'                     ''_'') variables',
'        from ppr_variables_ttransaccion c',
'       where c.emp_id = :f_emp_id',
'         and c.ttr_id =',
'             pq_constantes.fn_retorna_constante(NULL,',
'                                                ''cn_ttr_id_comprobante'')',
'         and c.vtt_mostrar_cabecera = ''S''',
'         and c.vtt_estado_registro = 0',
'       order by c.vtt_orden;',
'       ',
'  lv_sentencia VARCHAR2(3000);',
'  i            NUMBER;',
'',
'BEGIN',
'',
'   pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_comprobantes''));',
'',
'   pq_ven_comprobantes.pr_carga_col_comprobantes(:F_EMP_ID,',
'                                                 :P100_UGE_ID,',
'                                                 to_date(:P100_FECHADESDE,''DD-MM-YYYY''),',
'                                                 to_date(:P100_FECHAHASTA,''DD-MM-YYYY''),',
'                                                 :P0_ERROR);',
'',
'',
' i := 0;',
'  lv_sentencia := NULL;',
'  FOR reg IN cu_variables_tran LOOP',
'    i := i + 1;',
'    htmldb_util.set_session_state(''P100_COL_HEAD'' || i, reg.variables);',
'',
'  END LOOP;',
'  i := 10;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>6931926839520474
);
wwv_flow_imp.component_end;
end;
/
