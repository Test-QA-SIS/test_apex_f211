prompt --application/pages/page_00162
begin
--   Manifest
--     PAGE: 00162
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>162
,p_name=>unistr('Detalle cr\00E9dito seguro')
,p_page_mode=>'MODAL'
,p_step_title=>unistr('Detalle cr\00E9dito seguro')
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(28584123219306198548)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'JALVAREZ'
,p_last_upd_yyyymmddhh24miss=>'20230825145401'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(258522534820863920)
,p_name=>unistr('Detalle Cr\00E9dtio Seguro')
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT s.ose_id,',
'       s.cxc_id_seguro,',
'       s.ose_plazo_cobertura,',
'       s.pse_valor_mensual,',
'       s.cse_id,',
'       s.pse_poliza_seguro,',
'       s.pse_codigo_producto,',
'       s.pse_campania,',
'       s.pse_codigo_paquete,',
'       s.ose_fecha,',
'       cse.cse_fecha_ini,',
'       cse.cse_fecha_fin,',
'       cse.cse_plazo,',
'       cse.cse_fecha_envio,',
'       cse.pse_num_poliza,',
'       cxc.cxc_id,',
'       cxc.cxc_saldo,',
'       cxc.cxc_fecha_adicion',
'  FROM ven_ordenes_seguros    s,',
'       car_cuentas_seguros    cse,',
'       car_cuentas_por_cobrar cxc',
' WHERE s.ord_origen = :P162_ORD_ID',
'   AND s.cse_id = cse.cse_id',
'   AND cse.cxc_id = cxc.cxc_id',
'   --AND s.cxc_id_venta = :p102_cxc_id',
'   AND s.cxc_id_seguro = :P162_CXC_ID_SEGURO'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258523060674863980)
,p_query_column_id=>1
,p_column_alias=>'OSE_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258523547511864024)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID_SEGURO'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc Id Seguro'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258523789945864024)
,p_query_column_id=>3
,p_column_alias=>'OSE_PLAZO_COBERTURA'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258524224685864024)
,p_query_column_id=>4
,p_column_alias=>'PSE_VALOR_MENSUAL'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Mensual'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258524553433864024)
,p_query_column_id=>5
,p_column_alias=>'CSE_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258525049909864025)
,p_query_column_id=>6
,p_column_alias=>'PSE_POLIZA_SEGURO'
,p_column_display_sequence=>6
,p_column_heading=>unistr('P\00F3liza Seguro')
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258525407128864025)
,p_query_column_id=>7
,p_column_alias=>'PSE_CODIGO_PRODUCTO'
,p_column_display_sequence=>7
,p_column_heading=>unistr('C\00F3d Producto')
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258525825769864025)
,p_query_column_id=>8
,p_column_alias=>'PSE_CAMPANIA'
,p_column_display_sequence=>8
,p_column_heading=>unistr('Campa\00F1a')
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258526160500864025)
,p_query_column_id=>9
,p_column_alias=>'PSE_CODIGO_PAQUETE'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258526628838864025)
,p_query_column_id=>10
,p_column_alias=>'OSE_FECHA'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258526972284864026)
,p_query_column_id=>11
,p_column_alias=>'CSE_FECHA_INI'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258527406458864026)
,p_query_column_id=>12
,p_column_alias=>'CSE_FECHA_FIN'
,p_column_display_sequence=>12
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258527819301864026)
,p_query_column_id=>13
,p_column_alias=>'CSE_PLAZO'
,p_column_display_sequence=>13
,p_column_heading=>'Plazo'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258528228681864026)
,p_query_column_id=>14
,p_column_alias=>'CSE_FECHA_ENVIO'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258528569157864026)
,p_query_column_id=>15
,p_column_alias=>'PSE_NUM_POLIZA'
,p_column_display_sequence=>15
,p_column_heading=>unistr('Num P\00F3liza')
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258529005475864027)
,p_query_column_id=>16
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258529381168864027)
,p_query_column_id=>17
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>17
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(258529764637864027)
,p_query_column_id=>18
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(256607504563996840)
,p_name=>'P162_ORD_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(258522534820863920)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(256607563766996841)
,p_name=>'P162_CXC_ID_SEGURO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(258522534820863920)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
