prompt --application/pages/page_00097
begin
--   Manifest
--     PAGE: 00097
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>97
,p_name=>'Reimpresion Documentos P. Tarjetas'
,p_step_title=>'Reimpresion Documentos P. Tarjetas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(12886891158284571204)
,p_plug_name=>'Pagos Tarjetas de Credito'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(12899870062230409875)
,p_plug_name=>'Pagos Tarjetas de Credito'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pa.ptc_id,',
'       pa.fecha,',
'       pe.pue_id,',
'       PA.TRX_ID,',
'       pa.ptc_total_bancos,',
'       pa.ptc_total_pago,',
'       pa.ede_id_proveedor,',
'       (SELECT ede.ede_descripcion',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = pa.ede_id_proveedor) entidad,',
'       ''Imprimir'' x',
'  FROM car_pago_tarjetas_credito pa, ven_periodos_caja pe',
' WHERE pa.estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'       AND pa.pca_id = pe.pca_id',
' ORDER BY 1 DESC'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(12899870158705409875)
,p_name=>'Pagos Tarjetas de Credito'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'RPOZO'
,p_internal_uid=>12867617007435644949
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12899871881920409947)
,p_db_column_name=>'PTC_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Ptc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PTC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12899872080739409950)
,p_db_column_name=>'FECHA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12899872175063409950)
,p_db_column_name=>'PTC_TOTAL_BANCOS'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Total Bancos'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PTC_TOTAL_BANCOS'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12899872252777409951)
,p_db_column_name=>'PTC_TOTAL_PAGO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Total Pago'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PTC_TOTAL_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12899872363205409951)
,p_db_column_name=>'EDE_ID_PROVEEDOR'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID_PROVEEDOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12899872455231409951)
,p_db_column_name=>'X'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'X'
,p_column_link=>'f?p=&APP_ID.:97:&SESSION.:imprimir:&DEBUG.::P97_PTC_ID,P97_PUE_ID,P97_TRX_ID:#PTC_ID#,#PUE_ID#,#TRX_ID#'
,p_column_linktext=>'#X#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'X'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12900082455055429940)
,p_db_column_name=>'ENTIDAD'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ENTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12901280560305535503)
,p_db_column_name=>'PUE_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(12902061267802603940)
,p_db_column_name=>'TRX_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Trx Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(12899875561799410145)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'128676225'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PTC_ID:FECHA:PUE_ID:PTC_TOTAL_BANCOS:PTC_TOTAL_PAGO:EDE_ID_PROVEEDOR:ENTIDAD:X'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(12902620456121657263)
,p_report_id=>wwv_flow_imp.id(12899875561799410145)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ENTIDAD'
,p_operator=>'='
,p_expr=>'AMERICAN EXPRESS MIGRACION'
,p_condition_sql=>'"ENTIDAD" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''AMERICAN EXPRESS MIGRACION''  '
,p_enabled=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(12886939169366574357)
,p_name=>'P97_PTC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(12899870062230409875)
,p_prompt=>'Ptc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(12901419770825547966)
,p_name=>'P97_PUE_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(12899870062230409875)
,p_prompt=>'Pue Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(12902082874728605949)
,p_name=>'P97_TRX_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(12899870062230409875)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(12887230480363596487)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reimprimir'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_car_tarjetas_credito.pr_imprimir_tarjetas_cred(pn_pue_id => :P97_PUE_ID,',
'                                                    pn_ptc_id => :P97_PTC_ID,',
'                                                    pn_emp_id => :f_emp_id,',
'                                                    pv_error => :p0_error);',
'',
'',
'                pq_con_imprimir.pr_imprimir_rep(:F_emp_id,',
'                                                :p97_trx_id,',
'                                                1,',
'                                                :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P97_PTC_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>12854977329093831561
);
wwv_flow_imp.component_end;
end;
/
