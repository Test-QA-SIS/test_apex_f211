prompt --application/pages/page_00156
begin
--   Manifest
--     PAGE: 00156
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>156
,p_name=>'Reverso Cuotas Gratis Cuotas Comodin'
,p_step_title=>'Reverso Cuotas Gratis Cuotas Comodin'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38542555894837627666)
,p_plug_name=>'NOTAS DE CREDITO CUOTAS GRATIS'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>22
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select cx.cli_id,',
'       mcb.mca_id movimiento_cartera,',
'       mcb.mca_total,',
'       mcb.com_id nota_credito,',
'       mcb.mca_fecha_adicion',
'from asdm_e.car_cuentas_por_cobrar cx,',
'     asdm_e.car_movimientos_cab mcb',
'where cx.cxc_id = mcb.cxc_id',
'      and mcb.ttr_id = 476',
'      and cx.cli_id = :p156_cli_id',
'      and mcb.uge_id = :p156_f_uge_id',
'      and trunc (mcb.mca_fecha_adicion) = trunc (sysdate)',
'union',
'select cx.cli_id,',
'       mcb.mca_id movimiento_cartera,',
'       mcb.mca_total,',
'       mcb.com_id nota_credito,',
'       mcb.mca_fecha_adicion',
'from asdm_e.car_cuentas_por_cobrar cx,',
'     asdm_e.car_movimientos_cab mcb',
'where cx.cxc_id = mcb.cxc_id',
'      and mcb.ttr_id = 620',
'      and cx.cli_id = :p156_cli_id',
'      and mcb.uge_id = :p156_f_uge_id',
'      and trunc (mcb.mca_fecha_adicion) = trunc (sysdate)',
'',
'---quitar este sql',
'union',
'select cx.cli_id,',
'       mcb.mca_id movimiento_cartera,',
'       mcb.mca_total,',
'       mcb.com_id nota_credito,',
'       mcb.mca_fecha_adicion',
'from asdm_e.car_cuentas_por_cobrar cx,',
'     asdm_e.car_movimientos_cab mcb',
'where cx.cxc_id = mcb.cxc_id',
'      and mcb.ttr_id = 476',
'      and cx.cli_id = :p156_cli_id',
'      and mcb.uge_id = :p156_f_uge_id',
'      and mcb.mca_id = 66858809',
'order by 2 desc'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_display_condition_type=>'NOT_EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca.mca_id,',
'       mca.mca_fecha,',
'       mca.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' ||',
'       cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       mca.mca_total valor,',
'       mca.mca_observacion observacion,',
'       mca.age_id_gestionado_por,',
'       mca.age_id_gestionado_por || '' - '' ||',
'       (SELECT per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'               per.per_primer_nombre || '' '' || per.per_segundo_nombre',
'          FROM asdm_agentes age, asdm_personas per',
'         WHERE age.age_id = mca.age_id_gestionado_por',
'           AND age.per_id = per.per_id) gestionado_por_agente,',
'       mca.uge_id',
'  FROM asdm_movimientos_caja mca, asdm_clientes cl, asdm_personas cli,',
'       car_movimientos_cab mcb',
' WHERE cli.per_id = cl.per_id',
'   AND cl.cli_id = mca.cli_id',
'   and mca.mca_id = mcb.mca_id_movcaja',
'   AND mca.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_pago_cuota'')',
'   AND mcb.ttr_id = 476',
'   AND mca.cli_id = :p156_cli_id',
'   AND mca.uge_id = :p156_f_uge_id',
' union',
'   SELECT mca.mca_id,',
'       mca.mca_fecha,',
'       mca.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' ||',
'       cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       mca.mca_total valor,',
'       mca.mca_observacion observacion,',
'       mca.age_id_gestionado_por,',
'       mca.age_id_gestionado_por || '' - '' ||',
'       (SELECT per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'               per.per_primer_nombre || '' '' || per.per_segundo_nombre',
'          FROM asdm_agentes age, asdm_personas per',
'         WHERE age.age_id = mca.age_id_gestionado_por',
'           AND age.per_id = per.per_id) gestionado_por_agente,',
'       mca.uge_id',
'  FROM asdm_movimientos_caja mca, asdm_clientes cl, asdm_personas cli,',
'       car_movimientos_cab mcb',
' WHERE cli.per_id = cl.per_id',
'   AND cl.cli_id = mca.cli_id',
'   and mca.mca_id = mcb.mca_id_movcaja',
'   AND mca.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_pago_cuota'')',
'   AND mcb.ttr_id = 620',
'   AND mca.cli_id = :p156_cli_id',
'   AND mca.uge_id = :p156_f_uge_id'))
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(38542555986786627667)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:XLSX:PDF'
,p_enable_mail_download=>'Y'
,p_owner=>'RULLOA'
,p_internal_uid=>38510302835516862741
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542556221386627669)
,p_db_column_name=>'MOVIMIENTO_CARTERA'
,p_display_order=>10
,p_column_identifier=>'B'
,p_column_label=>'Movimiento cartera'
,p_column_link=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.:RP:P156_MOVIMIENTO_CARTERA,P156_VALOR_NC:#MOVIMIENTO_CARTERA#,#MCA_TOTAL#'
,p_column_linktext=>'#MOVIMIENTO_CARTERA#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542556134520627668)
,p_db_column_name=>'CLI_ID'
,p_display_order=>20
,p_column_identifier=>'A'
,p_column_label=>'Cli id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542556310475627670)
,p_db_column_name=>'MCA_TOTAL'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Mca total'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542556379638627671)
,p_db_column_name=>'NOTA_CREDITO'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Nota credito'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542556536660627672)
,p_db_column_name=>'MCA_FECHA_ADICION'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Mca fecha adicion'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(44843395159215780953)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'448111421'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CLI_ID:MOVIMIENTO_CARTERA:MCA_TOTAL:NOTA_CREDITO:MCA_FECHA_ADICION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44843538176213375327)
,p_name=>'DETALLE MOVIMIENTO CARTERA'
,p_parent_plug_id=>wwv_flow_imp.id(38542555894837627666)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*select mde.mca_id movimiento_cartera,',
'       mcbb.cxc_id,',
'       mde.div_id,',
'       di.div_nro_vencimiento,',
'       di.div_fecha_vencimiento,',
'       mde.mde_valor',
'from asdm_e.car_movimientos_cab mcbb,',
'     asdm_e.car_movimientos_det mde,',
'     asdm_e.car_dividendos di,',
'     asdm_e.car_cuotas_promo cp,',
'     asdm_e.car_cuotas_promo_det cpd',
'where mcbb.mca_id = mde.mca_id',
'      and di.div_id = mde.div_id',
'      and di.cxc_id = cp.cxc_id',
'      and cp.cpr_id = cpd.cpr_id',
'      and mcbb.mca_id = :P156_MOVIMIENTO_CARTERA',
'      and di.div_estado_dividendo = ''C''',
'      and di.div_nro_vencimiento = cpd.cpd_nro_vencim',
'      and mcbb.ttr_id = 476',
'order by mde.div_id*/',
'',
'select mde.mca_id movimiento_cartera,',
'       mcbb.cxc_id,',
'       mde.div_id,',
'       di.div_nro_vencimiento,',
'       di.div_fecha_vencimiento,',
'       mde.mde_valor',
'from asdm_e.car_movimientos_cab mcbb,',
'     asdm_e.car_movimientos_det mde,',
'     asdm_e.car_dividendos di,',
'     asdm_e.car_cuotas_promo cp--,',
'     --asdm_e.car_cuotas_promo_det cpd',
'where mcbb.mca_id = mde.mca_id',
'      and di.div_id = mde.div_id',
'      and di.cxc_id = cp.cxc_id',
'      --and cp.cpr_id = cpd.cpr_id',
'      and mcbb.mca_id = :P156_MOVIMIENTO_CARTERA',
'      --and di.div_estado_dividendo = ''C''',
'      --and di.div_nro_vencimiento = cpd.cpd_nro_vencim',
'      and mcbb.ttr_id = 476',
'--order by mde.div_id',
'union',
'select mde.mca_id movimiento_cartera,',
'       mcbb.cxc_id,',
'       mde.div_id,',
'       di.div_nro_vencimiento,',
'       di.div_fecha_vencimiento,',
'       mde.mde_valor',
'from asdm_e.car_movimientos_cab mcbb,',
'     asdm_e.car_movimientos_det mde,',
'     asdm_e.car_dividendos di',
'where mcbb.mca_id = mde.mca_id',
'      and di.div_id = mde.div_id',
'      and mcbb.mca_id = :P156_MOVIMIENTO_CARTERA',
'      and mcbb.ttr_id = 620',
'--order by mde.div_id'))
,p_display_when_condition=>'P156_VALIDA_ESTADO_REVERSO'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44843538251784375328)
,p_query_column_id=>1
,p_column_alias=>'MOVIMIENTO_CARTERA'
,p_column_display_sequence=>1
,p_column_heading=>'Movimiento cartera'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44843538402718375329)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44843538490186375330)
,p_query_column_id=>3
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Div id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44843538697912375332)
,p_query_column_id=>4
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'Div nro vencimiento'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44843538775108375333)
,p_query_column_id=>5
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Div fecha vencimiento'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44843538604144375331)
,p_query_column_id=>6
,p_column_alias=>'MDE_VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'Mde valor'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38571286885977214625)
,p_plug_name=>'Clave de Autorizacion'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>81
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38571289584984242692)
,p_plug_name=>'orden'
,p_parent_plug_id=>wwv_flow_imp.id(38571286885977214625)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>630
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38635480965335877127)
,p_plug_name=>'REVERSO DE CUOTAS GRATIS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38635709386082778119)
,p_plug_name=>'LISTADO DE PAGOS DE CUOTA - CLIENTE: <b>&P156_NOMBRE. '
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>21
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca.mca_id,',
'       mca.mca_fecha,',
'       mca.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' ||',
'       cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       mca.mca_total valor,',
'       mca.mca_observacion observacion,',
'       mca.age_id_gestionado_por,',
'       mca.age_id_gestionado_por || '' - '' ||',
'       (SELECT per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'               per.per_primer_nombre || '' '' || per.per_segundo_nombre',
'          FROM asdm_agentes age, asdm_personas per',
'         WHERE age.age_id = mca.age_id_gestionado_por',
'           AND age.per_id = per.per_id) gestionado_por_agente,',
'       mca.uge_id',
'  FROM asdm_movimientos_caja mca, asdm_clientes cl, asdm_personas cli,',
'       car_movimientos_cab mcb',
' WHERE cli.per_id = cl.per_id',
'   AND cl.cli_id = mca.cli_id',
'   and mca.mca_id = mcb.mca_id_movcaja(+)',
'   AND mca.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_pago_cuota'')',
'   AND mcb.ttr_id = 476',
'   AND mca.cli_id = :p156_cli_id',
'   AND mca.uge_id = :p156_f_uge_id',
'   AND trunc(mca.mca_fecha) = trunc(SYSDATE)',
'   AND mca.mca_estado_registro = 0',
'   AND mca.emp_id = :f_emp_id',
'   AND mca.mca_estado_mc IS NULL',
' union',
'SELECT mca.mca_id,',
'       mca.mca_fecha,',
'       mca.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' ||',
'       cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       mca.mca_total valor,',
'       mca.mca_observacion observacion,',
'       mca.age_id_gestionado_por,',
'       mca.age_id_gestionado_por || '' - '' ||',
'       (SELECT per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'               per.per_primer_nombre || '' '' || per.per_segundo_nombre',
'          FROM asdm_agentes age, asdm_personas per',
'         WHERE age.age_id = mca.age_id_gestionado_por',
'           AND age.per_id = per.per_id) gestionado_por_agente,',
'       mca.uge_id',
'  FROM asdm_movimientos_caja mca, asdm_clientes cl, asdm_personas cli,',
'       car_movimientos_cab mcb',
' WHERE cli.per_id = cl.per_id',
'   AND cl.cli_id = mca.cli_id',
'   and mca.mca_id = mcb.mca_id_movcaja(+)',
'   AND mca.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_pago_cuota'')',
'   AND mcb.ttr_id = 620',
'   AND mca.cli_id = :p156_cli_id',
'   AND mca.uge_id = :p156_f_uge_id',
'   AND trunc(mca.mca_fecha) = trunc(SYSDATE)',
'   AND mca.mca_estado_registro = 0',
'   AND mca.emp_id = :f_emp_id',
'   AND mca.mca_estado_mc IS NULL',
' ORDER BY 1 DESC',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca.mca_id,',
'       mca.mca_fecha,',
'       mca.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' ||',
'       cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       mca.mca_total valor,',
'       mca.mca_observacion observacion,',
'       mca.age_id_gestionado_por,',
'       mca.age_id_gestionado_por || '' - '' ||',
'       (SELECT per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'               per.per_primer_nombre || '' '' || per.per_segundo_nombre',
'          FROM asdm_agentes age, asdm_personas per',
'         WHERE age.age_id = mca.age_id_gestionado_por',
'           AND age.per_id = per.per_id) gestionado_por_agente,',
'       mca.uge_id',
'  FROM asdm_movimientos_caja mca, asdm_clientes cl, asdm_personas cli,',
'       car_movimientos_cab mcb',
' WHERE cli.per_id = cl.per_id',
'   AND cl.cli_id = mca.cli_id',
'   and mca.mca_id = mcb.mca_id_movcaja',
'   AND mca.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_pago_cuota'')',
'   AND mcb.ttr_id = 476',
'   AND mca.cli_id = :p156_cli_id',
'   AND mca.uge_id = :p156_f_uge_id',
' union',
'   SELECT mca.mca_id,',
'       mca.mca_fecha,',
'       mca.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' ||',
'       cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       mca.mca_total valor,',
'       mca.mca_observacion observacion,',
'       mca.age_id_gestionado_por,',
'       mca.age_id_gestionado_por || '' - '' ||',
'       (SELECT per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'               per.per_primer_nombre || '' '' || per.per_segundo_nombre',
'          FROM asdm_agentes age, asdm_personas per',
'         WHERE age.age_id = mca.age_id_gestionado_por',
'           AND age.per_id = per.per_id) gestionado_por_agente,',
'       mca.uge_id',
'  FROM asdm_movimientos_caja mca, asdm_clientes cl, asdm_personas cli,',
'       car_movimientos_cab mcb',
' WHERE cli.per_id = cl.per_id',
'   AND cl.cli_id = mca.cli_id',
'   and mca.mca_id = mcb.mca_id_movcaja',
'   AND mca.ttr_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_pago_cuota'')',
'   AND mcb.ttr_id = 620',
'   AND mca.cli_id = :p156_cli_id',
'   AND mca.uge_id = :p156_f_uge_id'))
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(38635709493878778119)
,p_name=>'listado_pagos_cuota'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'YGUAMAN'
,p_internal_uid=>38603456342609013193
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542373827623544151)
,p_db_column_name=>'MCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Seleccionar'
,p_column_link=>'f?p=&APP_ID.:156:&SESSION.:CARGAR:&DEBUG.::P156_MCA_ID_PAGO_CUOTA,P156_MCA_TOTAL,P156_MCA_FECHA,P156_MCA_OBSERVACION,P156_AGE_ID_GESTIONADO_POR,P156_CLI_ID_NOMBRE_REFERIDO:#MCA_ID#,#VALOR#,#MCA_FECHA#,#OBSERVACION#,#GESTIONADO_POR_AGENTE#,#CLIENTE#'
,p_column_linktext=>'Mov Caja # #MCA_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542374219388544152)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Mca Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542374593779544153)
,p_db_column_name=>'CLI_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542374983964544154)
,p_db_column_name=>'CLIENTE'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542375446341544155)
,p_db_column_name=>'VALOR'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542375786005544157)
,p_db_column_name=>'OBSERVACION'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Observacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542376183894544158)
,p_db_column_name=>'AGE_ID_GESTIONADO_POR'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Age Id Gestionado Por'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_GESTIONADO_POR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542376614485544159)
,p_db_column_name=>'GESTIONADO_POR_AGENTE'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Gestionado Por Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'GESTIONADO_POR_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(38542373381269544149)
,p_db_column_name=>'UGE_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(38635711076808782436)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'385101238'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>10
,p_report_columns=>'MCA_ID:MCA_FECHA:CLIENTE:VALOR:OBSERVACION:GESTIONADO_POR_AGENTE:UGE_ID'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38635972376736090848)
,p_plug_name=>'pr_reversar_pago'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>71
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38757173274990950423)
,p_plug_name=>'DatosReversoPago'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>41
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select count(*) ',
'FROM apex_collections',
'WHERE collection_name = pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_mov_caja''))>0'))
,p_plug_display_when_cond2=>'SQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(38757391470410643626)
,p_name=>'<B> REVERSO DE PAGO DE CUOTA   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>61
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_pagos_cuota.fn_apex_col_mov_caja_rev_pago(:p0_error);'
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS_INITCAP'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
,p_comment=>'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:p0_error);'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(38757391470410643626)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542379904676544175)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542380347243544177)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542380656952544178)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>7
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542381107115544179)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>8
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542381527936544179)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>13
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542381947684544180)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>14
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542382301390544181)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>6
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542382665525544183)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>3
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542383079670544184)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>4
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542383464670544185)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>5
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542383931848544186)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>15
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542384294766544187)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>16
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542384731016544188)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>17
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542385109207544189)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>18
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542385481698544190)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>19
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542385923990544191)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>9
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542386308672544192)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>10
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542386658513544194)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>11
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542387112638544195)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>12
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542387476963544196)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542387861074544197)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542388272013544197)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542388693086544198)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542389117526544198)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542389460048544199)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542389890101544200)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542390297128544201)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542390712315544201)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542391118090544202)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542391497214544202)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542391887531544203)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542392292747544204)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542392662891544205)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542393141079544205)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542393474938544206)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542393879912544207)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542394332301544207)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542394664328544208)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542395071676544208)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542395522981544211)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542395902731544212)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542396252977544213)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542396730464544214)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542397087368544215)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542397460911544216)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542397946683544217)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542398325220544218)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542398724192544219)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542399100566544220)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542399441461544221)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542399802796544221)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542400171617544222)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542400598930544223)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542400973751544223)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542401360747544224)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542401825254544225)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542402204801544225)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542402620008544226)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542402959486544227)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542403351878544228)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44830942382503107776)
,p_name=>'MOVIMIENTOS DE CAJA Y CARTERA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>51
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT m.mca_id_movcaja movimiento_caja,',
'       m.mca_id movimiento_cartera,',
'       m.cxc_id,',
'       m.mca_total total,',
'       m.ttr_id,',
'       (select tt.ttr_descripcion',
'       from asdm_e.asdm_tipos_transacciones tt',
'       where tt.ttr_id = m.ttr_id) tipo_transaccion,',
'       m.com_id',
'  FROM asdm_e.car_movimientos_cab m',
' WHERE m.mca_id_movcaja = :P156_MCA_ID_PAGO_CUOTA',
'   AND m.mca_estado_registro = 0',
'order by movimiento_cartera'))
,p_display_when_condition=>':P156_CLI_ID is not null and :P156_MCA_ID_PAGO_CUOTA is not null'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542551997730627627)
,p_query_column_id=>1
,p_column_alias=>'MOVIMIENTO_CAJA'
,p_column_display_sequence=>1
,p_column_heading=>'Movimiento caja'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542552067591627628)
,p_query_column_id=>2
,p_column_alias=>'MOVIMIENTO_CARTERA'
,p_column_display_sequence=>2
,p_column_heading=>'Movimiento cartera'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542552197213627629)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542552288703627630)
,p_query_column_id=>4
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>4
,p_column_heading=>'Total'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542552402799627631)
,p_query_column_id=>5
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Ttr id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542552461920627632)
,p_query_column_id=>6
,p_column_alias=>'TIPO_TRANSACCION'
,p_column_display_sequence=>6
,p_column_heading=>'Tipo transaccion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542552631301627633)
,p_query_column_id=>7
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Com id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38635636094190510477)
,p_plug_name=>unistr('Asignaci\00BF\00BFn de Folios para :  ')
,p_parent_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'((NVL(:P156_GC_TOTAL,0)>0 or NVL(:P156_IM_TOTAL,0)>0) and :P156_MCA_TOTAL >0 and :P156_MCA_ID_PAGO_CUOTA is not null and :P156_ERROR is null) or :P156_TFP_ID = 35'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(38635636275260510526)
,p_name=>'Gastos Cobrados en Pago de Cuota - Nota de Credito '
,p_parent_plug_id=>wwv_flow_imp.id(38635636094190510477)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>500
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,       ',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       to_number(c010) valor, ',
'       apex_item.text(25,',
'                      p_value =>NVL(c020,0),',
'                      p_size => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' || seq_id ||',
'       '',''''TO'''', this.value ,''''P156_F_UGE_ID'''',''''P156_F_USER_ID'''',''''P156_F_EMP_ID'''',''''P156_CLI_ID'''',''''R93263611187966384'''')"'') folio,',
'c011 Establec,',
'c012 PtoEmis,',
'c013 || '' - '' || c014 Gasto,',
'c002 Tipo',
'--c020 folio_col',
'  FROM apex_collections',
' WHERE collection_name =  pq_constantes.fn_retorna_constante(NULL,''cv_col_folios_nc_gastos'')',
' ORDER BY c001,c003,c007,c013 ;',
'',
'',
'',
'}'';'))
,p_display_when_condition=>'(:P156_IM_TOTAL>0 or :P156_GC_TOTAL >0) AND :P156_VALIDA_PUNTO = ''P'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen gastos cobrados'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542434589126544325)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542435027780544325)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542435367946544326)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>4
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542435761073544327)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>5
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542436192913544328)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>6
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542436566564544328)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>8
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542437036512544330)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>11
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542437393419544330)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>9
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542437766217544332)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>10
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542438172129544332)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>7
,p_column_heading=>unistr('Transacci\00BF\00BFn')
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542438640614544333)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>3
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542438974189544334)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542439436497544335)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542439777959544335)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542440191756544336)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542440591438544337)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542441022679544338)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542441403536544338)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542441824664544339)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542442240912544340)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542442639941544340)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542442974629544341)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542443448336544342)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542443797822544343)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542444159059544344)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542444560025544345)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542445033780544346)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542445425521544346)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542445805267544347)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542446221381544347)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542446578168544348)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542446988044544348)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542447427748544349)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542447775780544350)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542448186614544350)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542448605012544352)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542448962658544353)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542449415592544354)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542449803283544355)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542450227385544357)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542450561596544358)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542451025374544359)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542451397663544361)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542451764390544362)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542452157148544364)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542452613431544365)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542453043650544367)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542453413870544368)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542453756555544369)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542454184147544370)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542454557427544372)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542455028844544373)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542455387126544374)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542455810141544375)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542456221628544376)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542456596066544377)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542456961292544379)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542457432300544380)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542457793645544381)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542458239688544383)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(38636840782852818631)
,p_name=>'DIVIDENDOS PAGADOS EN MOV. CAJA #  &P156_MCA_ID_PAGO_CUOTA.'
,p_parent_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_pagos_cuota.fn_carga_cuota_gratis(pn_tipo_reverso => :P156_TIPO_REVERSO,',
'                                      pv_error  => :P0_ERROR);'))
,p_display_when_condition=>':P156_MCA_ID_PAGO_CUOTA is not null'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen dividendos a devolver'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
,p_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*',
'Select',
'      seq_id,',
'      c001 cli_id,',
'      c002 per_id,',
'      c003 cxc_id,',
'      c004 div_id,',
'      c005 nro_vencimiento,',
'      c006 fecha_vencimiento,',
'      c008 valor_cuota,                                     ',
'      c009 respaldo_cheques,',
'      c010 saldo,',
'      c011 estado,',
'      --c012 pagar,',
'      c013 fac_mxpro,',
'      c014 com_id_factura,',
'      c015 comp,',
'      c016 com_tipo,',
'      c017 ttr_id,',
'      c018 transaccion,',
'      c019 comprobante,',
'--      c020 ede_dividendo,                                     ',
'      c021 vendedor,',
'      c022 segmento,',
'apex_item.checkbox(10,seq_id,p_attributes => ',
'''onclick="pr_carga_cuotas_con_valor(''''PR_CUOTAS_VALOR'''',''''P6_F_EMP_ID'''',''''P6_TFP_ID'''',this.value,''''P6_F_SEG_ID'''',''''P6_VALOR_PAGO'''',''''R70272713295111230'''',''''R70263701157111210'''');',
'pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''TP'''',''''P6_VALOR_PAGO_COLECCION'''');pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''IC'''',''''P6_TOTAL_INT_COND'''');',
'pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''GC'''',''''P6_TOTAL_GST_COND'''');pr_asigna_valores(''''PR_CALCULA_TOTALES_PAGO'''',''''P6_F_EMP_ID'''',''''TC'''',''''P6_TOTAL_CONDONAR'''')"'') Devolver,',
'      c023 selecc',
'--      c024  Rol',
'      from apex_collections       ',
'      WHERE collection_name = ''CO_DIV_PENDIENTES''',
'       --AND c023 = ''N''',
'     AND c004 not in',
'       (SELECT col.c005 div_id',
'          FROM apex_collections col',
'         WHERE col.collection_name =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_coleccion_pago_cuota''))',
'      ORDER BY c001,c003,c004',
'',
'',
'*/',
'',
'',
'',
'',
'',
'',
'/*SELECT dd.mde_id,',
'       d.com_id_factura         comprob, -- Yguaman 2011/11/13',
'       d.cxc_id, -- Yguaman 2011/11/13',
'       dv.div_id                dividendo,',
'       dv.div_nro_vencimiento nro_venc,',
'       dv.div_fecha_vencimiento fecha_vencimiento,',
'       dd.mde_valor             valor,',
'       d.ttr_id                 Trans, -- Yguaman 2011/11/13',
'       d.ttr_descripcion, -- Yguaman 2011/11/13',
'       dv.div_id_ref Div_Ref  -- Yguaman 2011/11/13',
'  FROM v_car_movimientos d, car_movimientos_det dd, car_dividendos dv',
' WHERE d.mca_id_movcaja = :P156_MCA_ID_PAGO_CUOTA',
'   AND d.mca_id = dd.mca_id',
'   AND dd.div_id = dv.div_id',
'   ORDER BY dv.div_nro_vencimiento, dv.div_id asc -- Yguaman 2011/11/13*/'))
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542411677600544264)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>2
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542412059414544265)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>3
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542412498159544267)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542412893309544268)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>5
,p_column_heading=>'Cxc id '
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542413288747544270)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>6
,p_column_heading=>'Div id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542413745746544274)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>7
,p_column_heading=>'# Venc'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542414137259544275)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>8
,p_column_heading=>'Fecha Venc'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542414476617544276)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>10
,p_column_heading=>'Com_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542414877087544277)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>11
,p_column_heading=>'Ttr_id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542415264956544278)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>12
,p_column_heading=>'Transacc'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542415669834544280)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>19
,p_column_heading=>'Vendedor'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542416070033544281)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>20
,p_column_heading=>'Segmento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542416515244544282)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>21
,p_column_heading=>'Rol'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542416924986544284)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>23
,p_column_heading=>'Selecc'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542417311266544285)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>1
,p_column_heading=>'Mov. Detalle'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542417671738544286)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>13
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542418140609544288)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>14
,p_column_heading=>'Im Cob'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_when_condition=>'#COL17#'
,p_display_when_condition2=>'0'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542418538954544289)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>15
,p_column_heading=>'Im Con'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_when_condition=>'#COL18#'
,p_display_when_condition2=>'0'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542418947590544290)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>16
,p_column_heading=>'Gc Cob'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542419293955544291)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>17
,p_column_heading=>'Gc Con'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542419750698544293)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>22
,p_column_heading=>'Devolver'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P156_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_devolucion_pago_cuota'')'
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542420126261544294)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>18
,p_column_heading=>'Total'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542420542432544295)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>9
,p_column_heading=>'# Comp'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542420943752544296)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542421344129544297)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542421659568544298)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542422112913544300)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542422479173544301)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542422894608544302)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542423343817544304)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542423679890544304)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542424069919544305)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542410139934544258)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542410545098544260)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542410869625544261)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542411339757544263)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542424459730544305)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542424927388544306)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542425283689544306)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542425746369544307)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542426056784544308)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542426509822544309)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542426918318544309)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542427274684544310)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542427702188544311)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542428130793544312)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542428517562544313)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542428922611544313)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542429301078544314)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542429680760544315)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542430130853544315)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542430476697544316)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542430885001544316)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542431335650544317)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542431715029544318)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542432130911544318)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542432478034544319)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542432942078544319)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542433277869544320)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542433728513544321)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(38638541174965342098)
,p_name=>'CUOTAS A REVERSAR:'
,p_parent_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_ven_pagos_cuota.fn_apex_item_rev_pago_dev(pn_emp_id => :F_EMP_ID,',
'                                 pv_error => :P0_Error);'))
,p_display_when_condition=>':P156_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_devolucion_pago_cuota'')'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'Seleccione los dividendos a devolver'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
,p_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*RETURN pq_ven_pagos_cuota.fn_apex_item_rev_pago_dev(pn_emp_id => :F_EMP_ID,',
'                                 pv_error => :P0_Error)*/',
'',
'',
'SELECT',
'                        seq_id,      ',
'                        c006 emp_id,                   ',
'                        c001 cli_id,',
'                        c002 Tipo, ',
'                        c003 "Comprob.", ',
'                        c007 "Cxc_id",',
'                        c005 "Div_id",',
'                        c004 "Venc",',
'                        --c008 ttr_id,                        ',
'                        to_date(c009,''DD-MM-YYYY'') "Fecha Venc",',
'                        c010 "V.Cuota",',
'                        c011 "Int.Mora Cobr",',
'                        c012 "Int.Mora Cond",',
'                        c022 "Int.Mora Cond 1",',
'                        c013 "Gto.Cobr Cobr",',
'                        c014 "Gto.Cobr Cond",',
'                        c023 "Gto.Cobr Cond 1",                   ',
'                        c019 "Total Deuda",',
'                        c016 "Total Pagado",',
'''<img src="#IMAGE_PREFIX#del.gif" style="cursor:pointer" onClick="pr_ejecuta_proc_con_valores(''''PR_ELIMINA_COL_PAGO_CUOTA'''',''''P156_F_EMP_ID'''','''' || c007 || '''','''' || c004',
'                     || '''','''' || c001 || '''','''' || seq_id || '''','''' || c005 || '''',''''P156_F_SEG_ID'''',''''P156_TFP_ID'''',''''P156_F_EMP_ID'''',''''R94468118780274489'''',''''R96168510892797956'''');',
'                     ">'' "Eliminar"          ',
'                   FROM apex_collections',
'                  WHERE collection_name = ',
'                    pq_constantes.fn_retorna_constante(NULL,',
'                                                       ''cv_coleccion_pago_cuota'')  order by c005 ;'))
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542458914951544387)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542459272524544388)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542459676178544389)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542460074464544391)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542460454186544392)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542460919089544393)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542461323800544394)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Div id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542461670465544396)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'# Venc.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542462127033544397)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Fecha Venc.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542462549507544399)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542462926757544400)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Int.Mora <b> Cobr'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542463315146544402)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Int.Mora <b> Cond'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542463738838544403)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Int.Mora <b> Cond 1'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542464055417544404)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Gto.Cobr <b> Cobr'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542464496331544405)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Gto.Cobr <b> Cond'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542464902703544406)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Gto.Cobr <b> Cond 1'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542465297259544407)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Tota Pagado'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542465719682544408)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Tota Pagado 1'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542466128017544409)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542466546981544411)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542466945246544412)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542467256261544413)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542467719528544414)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542468106979544415)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542468528828544416)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542468882995544417)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542469280636544418)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542469660674544420)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542470053720544421)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542470403960544422)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542470806896544423)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542471178101544424)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542471581548544425)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542472037232544426)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542472443528544428)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542472775211544429)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542473193795544430)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542473562791544431)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542473991290544432)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542474433277544433)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542474829934544435)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542475154056544436)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542475561914544437)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542476005371544438)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542476389415544439)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542476769838544441)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542477195897544442)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542477609191544443)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542478017870544444)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542478409827544444)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542478830487544445)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542479187924544446)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542479563667544447)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542480011985544448)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542480439520544449)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542480751633544450)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542481213266544450)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542481613230544451)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542481970051544452)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542482383249544453)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(52201486375376485241)
,p_name=>'prueba ja'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>91
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'from apex_collections',
'where collection_name = ''CO_DIV_PENDIENTES'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542492916354544479)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542493300426544480)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542493736701544480)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542494134899544481)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542494527805544481)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542494924947544482)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542495285794544482)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542495727123544483)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542496063849544483)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542496515759544484)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542496882656544484)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542497253622544485)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542497730371544486)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542498083092544487)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542498498195544488)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542498899284544489)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542499277122544489)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542499694699544490)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542500087803544491)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542500478683544491)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542500886313544492)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542501265657544492)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542501731692544494)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542502067760544495)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542502457869544495)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542502885813544497)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542503319933544498)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542503690303544499)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542504123431544500)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542504546656544500)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542504902104544501)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542505277051544502)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542505708929544503)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542506116074544504)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542506531968544505)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542506916295544507)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542507298558544508)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542507735130544509)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542508116567544510)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542508498063544512)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542508897722544513)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542509260902544514)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542509669587544515)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542510108914544516)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542510538547544517)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542510943195544518)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542511285453544519)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542511729146544521)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542512041830544522)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542512397537544523)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542512816429544525)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542513230290544526)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542513570756544527)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542514008017544528)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542514430583544529)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542514840068544530)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542515194997544532)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542515631701544533)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542515990501544534)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542516396625544535)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542516794458544536)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542517225647544537)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542517624353544538)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542518041704544539)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542518397606544540)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38542518779560544541)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38542403830978544229)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(38757391470410643626)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:popUp2(''../../reports/rwservlet?module=PAGO_CUOTA.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&pagesize=10 x 10&pn_emp_id=&F_EMP_ID.&pn_mca_id=&P156_MCA_ID_PAGO_CUOTA.'', 850, 500,85);'
,p_button_condition=>':P156_MCA_ID_PAGO_CUOTA is not null and :P156_MCA_TOTAL>0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38542378058925544167)
,p_button_sequence=>200
,p_button_plug_id=>wwv_flow_imp.id(38635972376736090848)
,p_button_name=>'GENERAR_REVERSO_PAGO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Generar Reverso Pago'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>':P156_MCA_ID_PAGO_CUOTA is not null and :P156_MCA_TOTAL>0 and :P156_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:F_EMP_Id,''cn_tfp_id_devolucion_pago_cuota'') and :P156_ERROR is null'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38542485042883544459)
,p_button_sequence=>210
,p_button_plug_id=>wwv_flow_imp.id(38571289584984242692)
,p_button_name=>'GENERAR_REVERSO_PAGO_CUOTA'
,p_button_static_id=>'GENERAR_REVERSO_PAGO_CUOTA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Generar Reverso Pago Cuota'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:156:&SESSION.:GENERAR_REVERSO_PAGO_CUOTA:&DEBUG.:::'
,p_button_condition=>'(:P156_MCA_ID_PAGO_CUOTA is not null and :P156_MCA_TOTAL>0 and :P156_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:F_EMP_Id,''cn_tfp_id_reverso_pago_cuota'') and :P156_ERROR is null) OR (:P156_MOVIMIENTO_CARTERA IS NOT NULL and :P156_TIPO_REVERSO '
||'= pq_constantes.fn_retorna_constante(:F_EMP_Id,''cn_tfp_id_reverso_pago_cuota'') and :P156_ERROR is null and :P156_VALIDA_ESTADO_REVERSO is not null)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_cattributes=>'class="redirect" '
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38542485705566544461)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_button_name=>'CANCELARNUEVO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar / Nuevo'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38542527994562544566)
,p_branch_action=>'f?p=&FLOW_ID.:156:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(38542485705566544461)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38542528383341544566)
,p_branch_action=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(281582355281559671)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38542526794580544563)
,p_branch_action=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.:79::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>40
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'cliente'
,p_branch_comment=>'Created 05-JAN-2012 16:06 by YGUAMAN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38542527171053544564)
,p_branch_name=>'Go To Page 144'
,p_branch_action=>'f?p=&APP_ID.:144:&SESSION.::&DEBUG.::P144_NRO_IDENTIFICACION,P144_TTR_ID,P144_TFP_ID,P144_TOTAL_MORA:&P156_IDENTIFICACION.,&P156_TTR_ID.,&P156_TIPO_REVERSO.,&P156_IM_TOTAL.'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(247060380420242665)
,p_branch_sequence=>60
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>':P156_ERROR is null'
,p_branch_condition_text=>'PLSQL'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 10-JAN-2012 19:54 by YGUAMAN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38542527641625544565)
,p_branch_action=>'f?p=&APP_ID.:156:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_branch_condition_type=>'NEVER'
,p_branch_comment=>'Created 21-ENE-2010 15:27 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542377419846544165)
,p_name=>'P156_ERROR'
,p_item_sequence=>700
,p_item_plug_id=>wwv_flow_imp.id(38635709386082778119)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'CENTER-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542378810056544171)
,p_name=>'P156_NDE_OBSERVACION'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(38757173274990950423)
,p_item_default=>'REVERSO DE PAGO DE CUOTAS GRATIS'
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>50
,p_cMaxlength=>100
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542379163786544172)
,p_name=>'P156_PCA_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(38757173274990950423)
,p_prompt=>'Pca Id'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542404225863544230)
,p_name=>'P156_MCA_ID_PAGO_CUOTA'
,p_item_sequence=>76
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_prompt=>'Mov.Caja Pago Cuota:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542404638252544231)
,p_name=>'P156_CLI_ID_NOMBRE_REFERIDO'
,p_item_sequence=>77
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_prompt=>'Cliente:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542405042894544232)
,p_name=>'P156_MCA_FECHA'
,p_item_sequence=>89
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_prompt=>'Fecha:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542405357722544233)
,p_name=>'P156_MCA_OBSERVACION'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542405774527544235)
,p_name=>'P156_AGE_ID_GESTIONADO_POR'
,p_item_sequence=>102
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_use_cache_before_default=>'NO'
,p_source=>'F_AGE_ID_AGENCIA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542406158949544235)
,p_name=>'P156_MCA_TOTAL'
,p_item_sequence=>104
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_item_default=>'0'
,p_prompt=>'Total Pago:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542406643323544236)
,p_name=>'P156_TRX_ID'
,p_item_sequence=>178
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542407017083544237)
,p_name=>'P156_GC_TOTAL'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_use_cache_before_default=>'NO'
,p_prompt=>' Gastos Cobranza:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(nvl(to_number(c010), 0)), 0)',
'  FROM apex_collections col',
' WHERE col.collection_name = ''CO_DIV_PENDIENTES'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-BOTTOM'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(nvl(c010, 0)), 0)',
'  FROM apex_collections col',
' WHERE col.collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_folios_nc_gastos'')',
' AND col.c013 =',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_credito_car_pago_gc'')'))
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542407449733544238)
,p_name=>'P156_IM_TOTAL'
,p_item_sequence=>600
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Interes de Mora: '
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(nvl(to_number(c008), 0)), 0)',
'  FROM apex_collections col',
' WHERE col.collection_name = ''CO_DIV_PENDIENTES'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(nvl(c010, 0)), 0)',
'  FROM apex_collections col',
' WHERE col.collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_folios_nc_gastos'')',
' AND col.c013 =',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_credito_car_pago_im'')'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542407829366544238)
,p_name=>'P156_DIR_ID'
,p_item_sequence=>700
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542408220560544239)
,p_name=>'P156_CLI_ID_REFERIDO'
,p_item_sequence=>800
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542408554139544240)
,p_name=>'P156_TOTAL_A_REVERSAR'
,p_item_sequence=>830
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select NVL(SUM(NVL(to_number(c019),0)),0)',
'from apex_collections a',
'where a.collection_name = pq_constantes.fn_retorna_constante(0,''cv_coleccion_pago_cuota'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542408958539544240)
,p_name=>'P156_TTR_ID'
,p_item_sequence=>840
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_item_default=>'pq_constantes.fn_retorna_constante(0,''cn_ttr_id_reverso_pago_cuota'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542409353121544241)
,p_name=>'P156_VALIDA_PUNTO'
,p_item_sequence=>850
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542483061790544455)
,p_name=>'P156_CLAVE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38571286885977214625)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Clave Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542483544929544455)
,p_name=>'P156_2_0'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(38571286885977214625)
,p_display_as=>'NATIVE_STOP_AND_START_HTML_TABLE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542483904879544456)
,p_name=>'P156_3_0'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(38571286885977214625)
,p_display_as=>'NATIVE_STOP_AND_START_HTML_TABLE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542484263034544457)
,p_name=>'P156_5_0'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(38571286885977214625)
,p_display_as=>'NATIVE_STOP_AND_START_HTML_TABLE'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542486108118544462)
,p_name=>'P156_F_USER_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F User Id'
,p_source=>'F_USER_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542486489851544463)
,p_name=>'P156_TIPO_IDENTIFICACION'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542486891788544464)
,p_name=>'P156_IDENTIFICACION'
,p_item_sequence=>73
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>'Identificacion Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (per.per_razon_social || per.per_primer_nombre || '' '' ||',
'       per.per_segundo_nombre || '' '' || per.per_primer_apellido || '' '' ||',
'       per.per_segundo_apellido) || '' - Cod: '' || cli.cli_id || '' - Id: '' ||',
'       per.per_nro_identificacion nombre,',
'       per.per_nro_identificacion identificacion',
'  FROM asdm_personas per, asdm_clientes cli',
' WHERE cli.per_id = per.per_id',
'   AND cli.emp_id = per.emp_id',
'   AND cli.cli_estado_registro = 0',
'   AND per.per_estado_registro = 0',
'   '))
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET_FILTER'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542487344293544464)
,p_name=>'P156_CLI_ID'
,p_item_sequence=>75
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>'Cliente:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542487655311544465)
,p_name=>'P156_NOMBRE'
,p_item_sequence=>92
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>70
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap" '
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542488147531544466)
,p_name=>'P156_CORREO'
,p_item_sequence=>93
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>70
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542488522575544466)
,p_name=>'P156_TIPO_DIRECCION'
,p_item_sequence=>94
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542488913681544467)
,p_name=>'P156_DIRECCION'
,p_item_sequence=>95
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>' - '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>55
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542489348398544468)
,p_name=>'P156_TIPO_TELEFONO'
,p_item_sequence=>96
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542489712146544468)
,p_name=>'P156_TELEFONO'
,p_item_sequence=>98
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_prompt=>' -  '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>55
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542490151161544469)
,p_name=>'P156_F_UGE_ID'
,p_item_sequence=>610
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Unidad de Gesti\00F3n Conectada:')
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542490467433544470)
,p_name=>'P156_F_EMP_ID'
,p_item_sequence=>630
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Emp Id'
,p_source=>'F_EMP_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542490864415544471)
,p_name=>'P156_TIPO_REVERSO'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_source=>'17'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<b>TIPOS DE REVERSO DE PAGO</b> <br><br>',
'',
'<b>TOTAL</b><br><br>',
'',
unistr('Lista todos los pagos de cuota que se han realizado en la fecha consultada (del d\00BF\00BFa)<br>'),
'Consiste en reversar los movimientos generados de caja y cartera. <br>',
'En caso de existir gastos cobrados, se generan las respectivas N/C<br>',
'No hace devoluciones de dinero, solo registros los asientos de reversa de los respectivos pagos. <br><br>',
'',
'<b>TRANSFERENCIA DE PAGO</b><br><br>',
'',
unistr('Lista todos los pagos sin importar su forma de pago, exceptuando los pagos del d\00BF\00BFa que deben ser escogidos por el tipo de reverso total.<br>'),
unistr('Consiste en transferir el pago de un cliente "A" (err\00BF\00BFnea) a un cliente "B" (correcto)<br><br>'),
'',
unistr('<b>Cliente "A" - Err\00BF\00BFneo </b><br><br>'),
'En caso de haberse cobrado gastos: Se generaran las N/C correspondientes, tomando los folios ingresados desde pantalla para imprimir<br><br>',
'Reversa cartera erronea de:<br>',
'No reversa movimiento de caja del pago de cuota hecho al cliente "A"<br>',
unistr('Los datos ingresado en cheques o retenciones se quedaran con el cliente "A", ya que no se pueden reemplazar con datos del nuevo cliente, pero debe existir la relaci\00BF\00BFn de que fue reemplazado por el cliente "B"<br><br>	'),
'',
'<b>Cliente "B" - Correcto </b><br><br>',
'En caso de generarse Int. de Mora o Gastos de Cobranza esto se condonaran<br>',
'Realiza el Pago cartera correcto<br>',
'Debe quedar un registro de que proviene del pago del cliente "A"<br><br>',
'',
unistr('<b> DEVOLUCI\00BF\00BFN DE PAGO</b><br><br>'),
unistr('Lista todos los pagos que no hayan sido pagados ni con tarjeta de cr\00BF\00BFdito ni retenciones, exceptuando los pagos del d\00BF\00BFa que deben ser escogidos por el tipo de reverso total.<br>'),
unistr('Solo puede reversar la ultima cuota del cr\00BF\00BFdito a devolver.<br>'),
'En caso de haberse cobrado gastos: Se generaran las N/C correspondientes, tomando los folios ingresados desde pantalla para imprimir<br>',
'No reversa movimiento de caja generado por el pago de cuota<br>',
'Genera movimiento de caja para el total devuelto a anticipo de clientes<br>',
'Mediante proceso se debe generar el egreso de anticipo de clientes con clave<br>',
''))
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542491793369544474)
,p_name=>'P156_TFP_ID'
,p_item_sequence=>810
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tfp Id'
,p_source=>'P156_TIPO_REVERSO'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542492200170544475)
,p_name=>'P156_F_SEG_ID'
,p_item_sequence=>820
,p_item_plug_id=>wwv_flow_imp.id(38635480965335877127)
,p_use_cache_before_default=>'NO'
,p_prompt=>'F Seg Id'
,p_source=>'F_SEG_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542519219848544542)
,p_name=>'P156_CLIENTE_EXISTE'
,p_item_sequence=>82
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542519625821544543)
,p_name=>'P156_MCA_ID_ND'
,p_item_sequence=>138
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Mca Id Nd'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542519994858544544)
,p_name=>'P156_MCA_ID_ANT_CLI'
,p_item_sequence=>168
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Mca Id Ant Cli'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542552714640627634)
,p_name=>'P156_TOTAL'
,p_item_sequence=>860
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sum(m.mca_total) total',
'  FROM asdm_e.car_movimientos_cab m',
' WHERE m.mca_id_movcaja = :P156_MCA_ID_PAGO_CUOTA',
'   AND m.mca_estado_registro = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542553377265627641)
,p_name=>'P156_CUOTA'
,p_item_sequence=>870
,p_item_plug_id=>wwv_flow_imp.id(44830942382503107776)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sum(m.mca_total) total',
'  FROM asdm_e.asdm_movimientos_caja m',
' WHERE m.mca_id = :P156_MCA_ID_PAGO_CUOTA',
'   AND m.mca_estado_registro = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542556687891627674)
,p_name=>'P156_VALOR_NC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38542555894837627666)
,p_prompt=>'Valor Cuotas:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38542556780035627675)
,p_name=>'P156_MOVIMIENTO_CARTERA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38542555894837627666)
,p_prompt=>'Movimiento Cartera:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44843539076675375336)
,p_name=>'P156_VALIDA_ESTADO_REVERSO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38542555894837627666)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select mde.div_id',
'from asdm_e.car_movimientos_cab mcbb,',
'     asdm_e.car_movimientos_det mde,',
'     asdm_e.car_dividendos di',
'where mcbb.mca_id = mde.mca_id',
'      and di.div_id = mde.div_id',
'      and mcbb.mca_id = :P156_MOVIMIENTO_CARTERA',
'      and di.div_estado_dividendo = ''C''',
'      and rownum = 1',
'order by mde.div_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(38542520517031544545)
,p_validation_name=>'P156_NDE_OBSERVACION'
,p_validation_sequence=>10
,p_validation=>'P156_NDE_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese la Observaci\00BF\00BFn')
,p_when_button_pressed=>wwv_flow_imp.id(247060380420242665)
,p_associated_item=>wwv_flow_imp.id(38542378810056544171)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38542525270104544560)
,p_name=>'refrescar'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38542525780590544561)
,p_event_id=>wwv_flow_imp.id(38542525270104544560)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(38635636094190510477)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38542526346695544562)
,p_event_id=>wwv_flow_imp.id(38542525270104544560)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(38635636275260510526)
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38542553522146627642)
,p_event_id=>wwv_flow_imp.id(38542525270104544560)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(44830942382503107776)
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38542524422748544557)
,p_name=>'PR_SET_CLAVE'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P156_CLAVE'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38542524910943544559)
,p_event_id=>wwv_flow_imp.id(38542524422748544557)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'P156_CLAVE'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542522423160544550)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'  :p0_ttr_id := pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_reverso_pago_cuota'');',
'   ln_tgr_id := pq_constantes.fn_retorna_constante(NULL,''cn_tgr_id_mov_caja'');',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'  ',
'  pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                  :f_pca_id,',
'                                                   ln_tgr_id,',
'                                                  :p0_ttr_descripcion,',
'                                                  :p0_periodo,',
'                                                  :p0_datfolio,',
'                                                  :p0_fol_sec_actual,',
'                                                  :p0_pue_num_sri,',
'                                                  :p0_uge_num_est_sri,',
'                                                  :p0_pue_id,',
'                                                  :p0_ttr_id,',
'                                                  :P0_NRO_FOLIO,',
'                                                  :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>38510269271890779624
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542523247491544553)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_col_detalle_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'     ln_tfp_tc number;',
'BEGIN',
'',
':P156_ERROR := NULL;',
'',
'IF :P156_ERROR is null then',
'',
'pq_ven_nota_debito.pr_carga_col_detalle_pago(:P156_MCA_ID_PAGO_CUOTA,:P156_ERROR);',
'',
'',
'IF  :P156_ERROR is null then',
'pq_ven_pagos_cuota.pr_carga_div_mov_pago(pn_mca_id_reversado => :P156_MCA_ID_PAGO_CUOTA, ',
'                                  pn_emp_id => :F_EMP_ID,',
'                                  pn_cli_id => :P156_CLI_ID,',
'                                  pn_tipo_reverso => :P156_TIPO_REVERSO, ',
'                                  pv_error  => :P156_ERROR);',
'END IF;',
'',
'IF  :P156_ERROR is null then',
'pq_ven_pagos_cuota.pr_valido_reverso_pago(pn_mca_id_reversar => :P156_MCA_ID_PAGO_CUOTA,',
'                                   pn_emp_id => :F_EMP_ID,',
'                                   pn_tipo_reverso => :P156_TIPO_REVERSO,',
'                                   pv_error  => :P156_ERROR);',
'',
'END IF;',
'END IF;',
'',
'-----------------*******************--------------',
'/*-- Filtra por el momento, porque no esta el reverso de pago de cuota con tarjeta de credito',
'IF :P156_TIPO_REVERSO = pq_constantes.fn_retorna_constante(:F_EMP_Id,''cn_tfp_id_reverso_pago_cuota'') AND :P156_ERROR is null  THEN',
'      SELECT COUNT(*)',
'      INTO ln_tfp_tc',
'      FROM asdm_movimientos_caja_detalle mcd',
'     WHERE mcd.mca_id = :P156_MCA_ID_PAGO_CUOTA',
'       AND mcd.tfp_id IN',
'           (pq_constantes.fn_retorna_constante(pn_emp_id    => :F_emp_id,',
'                                               pv_constante => ''cn_tfp_id_tarjeta_credito''));',
'   IF ln_tfp_tc > 0 THEN',
unistr('      :P156_ERROR := ''El reverso de pago de cuota con tarjeta de cr\00BF\00BFdito, NO ESTA DISPONIBLE POR EL MOMENTO.'';'),
'   END IF;',
'END IF;*/',
'-----------------*******************--------------',
'if :p32_error is not null then',
'raise_application_error(-20000,''error'');',
'end if;',
'exception',
'  when others then',
'  raise_application_error(-20000, sqlerrm || '' '' || :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>38510270096221779627
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542521580545544549)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_div_mov_pago'
,p_process_sql_clob=>'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));'
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38510268429275779623
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542523573531544554)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_reverso_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_efectivo    NUMBER := 0;',
'  ln_cheque      NUMBER := 0;',
'  ln_tarjeta     NUMBER := 0;',
'  ln_valor_cuota NUMBER := 0;',
'  ln_clave       NUMBER := 0;',
'  valida_clave EXCEPTION;',
'  ln_valor_pago_seleccionado number := 0;',
'BEGIN',
'',
'IF :p156_mca_id_pago_cuota IS NOT NULL THEN',
'execute immediate ''',
'select sum(to_number(c016))',
'from apex_collections',
'where collection_name = ''''CO_PAGO_CUOTA''''''',
'into ln_valor_pago_seleccionado;',
'',
'execute immediate ''SELECT sum(ca.mca_total)',
'FROM car_movimientos_cab ca',
'WHERE ca.mca_id_movcaja = :1''',
'INTO ln_valor_cuota',
'using :p156_mca_id_pago_cuota;',
'   ',
'   if ln_valor_pago_seleccionado != ln_valor_cuota then',
'   ',
'      raise_application_error(-20000, ''Existen Diferencias entre el Valor Total del Pago y el valor Reversar'');',
'   ',
'   end if;',
'',
'',
'   ',
'  --------------------   agregado por jandres valida clave reverso 18/05/2013',
'IF :f_seg_id = 2 THEN',
'  BEGIN',
'  ',
'    /*SELECT MAX(di.div_valor_cuota)',
'      INTO ln_valor_cuota',
'      FROM car_dividendos di',
'     WHERE cxc_id =',
'           (SELECT MAX(ca.cxc_id)',
'              FROM car_movimientos_cab ca',
'             WHERE ca.mca_id_movcaja = :p156_mca_id_pago_cuota)',
'       AND di.div_nro_vencimiento > 0;*/',
'',
'SELECT ca.mca_total',
'INTO ln_valor_cuota',
'FROM asdm_movimientos_caja ca',
'WHERE ca.mca_id = :p156_mca_id_pago_cuota;',
'  ',
'    pq_car_general.pr_clave_reverso_pago_cuota(pn_cli_id     => :p156_cli_id,',
'                                               pn_per_id     => NULL,',
'                                               pn_emp_id     => :f_emp_id,',
'                                               pn_valor      => ln_valor_cuota,',
'                                               pn_resultado  => ln_clave,',
'                                               pn_uge_id     => :f_uge_id,',
'                                               pn_usu_id     => :f_user_id,',
'                                               pn_tipo_clave => 8,',
'                                               pn_tipo_uso   => 0,',
'                                               pv_error      => :p0_error);',
' -- raise_application_error(-20000,ln_valor_cuota);',
'--RAISE_APPLICATION_ERROR(-20000,'':p156_clave: '' || :p156_clave || '' LN_CLAVE '' || LN_CLAVE);',
'  ',
'    IF ln_clave != nvl(:p156_clave, -1) THEN',
'    ',
'     raise_application_error(-20000,',
'                              ''Clave de Autorizacion Incorrecta, comunicarse con el Dep. Cartera'');',
'    ',
'    END IF;',
'  ',
'  END;',
'END IF;',
'  --------------------',
'',
'  pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id         => :f_emp_id,',
'                                                  pn_pca_id         => :f_pca_id,',
'                                                  pn_valor_efectivo => ln_efectivo,',
'                                                  pn_valor_cheque   => ln_cheque,',
'                                                  pn_valor_tarjeta  => ln_tarjeta,',
'                                                  pv_error          => :p0_error);',
'',
'',
'  /*IF ln_efectivo < nvl(to_number(:p156_mca_total), 0) AND :f_seg_id = 2 THEN',
'    :p0_error := ''NO TIENE DINERO EN EFECTIVO DISPONIBLE EN CAJA PARA PODER REALIZAR EL REVERSO... caja:'' ||',
'                 ln_efectivo;',
'    raise_application_error(-20000, :p0_error);',
'  END IF;*/',
'  ',
'  ',
'  IF :p156_im_total > 0 THEN',
'    pq_ven_pagos_cuota.pr_valida_existe_folios(pn_emp_id => :f_emp_id,',
'                                               pn_cli_id => :p156_cli_id,',
'                                               pn_gasto  => ''TO'',',
'                                               pv_error  => :p0_error);',
'  END IF;',
'',
'  IF :p0_error IS NULL THEN ',
'  ',
'    pq_car_credito.pr_reverso_cuotas_gratis(pn_mca_id_movcaja_reversar => :p156_mca_id_pago_cuota,',
'                                   pn_uge_id                  => :f_uge_id,',
'                                   pn_uge_id_gasto            => :f_uge_id_gasto,',
'                                   pn_usu_id                  => :f_user_id,',
'                                   pn_emp_id                  => :f_emp_id,',
'                                   pn_cli_id                  => :p156_cli_id,',
'                                   pn_tipo_reverso            => :p156_tipo_reverso,',
'                                   pv_observacion             => :p156_nde_observacion,',
'                                   pn_mca_total               => :p156_mca_total,',
'                                   pn_pca_id                  => :f_pca_id,',
'                                   pn_tse_id                  => :f_seg_id,',
'                                   pn_age_id_agencia          => :f_age_id_agencia,',
'                                   pv_error                   => :p156_error);',
'  END IF;',
'  pq_ven_comprobantes.pr_redirect(pv_desde        => ''P'',',
'                                  pn_app_id       => NULL,',
'                                  pn_pag_id       => NULL,',
'                                  pn_emp_id       => :f_emp_id,',
'                                  pv_session      => :session,',
'                                  pv_token        => :f_token,',
'                                  pn_user_id      => :f_user_id,',
'                                  pn_rol          => :p0_rol,',
'                                  pv_rol_desc     => :p0_rol_desc,',
'                                  pn_tree_rot     => :p0_tree_root,',
'                                  pn_opcion       => :f_opcion_id,',
'                                  pv_parametro    => :f_parametro,',
'                                  pv_empresa      => :f_empresa,',
'                                  pn_uge_id       => :f_uge_id,',
'                                  pn_uge_id_gasto => :f_uge_id_gasto,',
'                                  pv_ugestion     => :f_ugestion,',
'                                  pv_error        => :p0_error);',
'',
':p156_mca_id_nd := NULL; -- yguaman 2011/12/27',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                             ''cv_coleccion_mov_caja''));',
':P156_MOVIMIENTO_CARTERA := NULL;',
':p156_mca_id_pago_cuota := NULL;',
':P156_VALOR_NC := NULL;',
'END IF; ',
'                                                                             ',
'IF :P156_MOVIMIENTO_CARTERA  IS NOT NULL THEN',
'',
'   pq_car_general.pr_clave_reverso_pago_cuota(pn_cli_id     => :p156_cli_id,',
'                                               pn_per_id     => NULL,',
'                                               pn_emp_id     => :f_emp_id,',
'                                               pn_valor      => :P156_VALOR_NC,',
'                                               pn_resultado  => ln_clave,',
'                                               pn_uge_id     => :f_uge_id,',
'                                               pn_usu_id     => :f_user_id,',
'                                               pn_tipo_clave => 8,',
'                                               pn_tipo_uso   => 0,',
'                                               pv_error      => :p0_error);',
'                                               ',
'    IF ln_clave != nvl(:p156_clave, -1) THEN',
'    ',
'     raise_application_error(-20000,',
'                              ''Clave de Autorizacion Incorrecta, comunicarse con el Dep. Cartera'');',
'    ',
'    END IF;',
'                                               ',
'--raise_application_error(-20000,''PRUEBA ROBERTH ULLOA'');',
'                                               ',
'   pq_car_credito.pr_reverso_cuotas_gratis(pn_mca_id_movcaja_reversar => 0,',
'                                   pn_uge_id                  => :f_uge_id,',
'                                   pn_uge_id_gasto            => :f_uge_id_gasto,',
'                                   pn_usu_id                  => :f_user_id,',
'                                   pn_emp_id                  => :f_emp_id,',
'                                   pn_cli_id                  => :p156_cli_id,',
'                                   pn_tipo_reverso            => :p156_tipo_reverso,',
'                                   pv_observacion             => :p156_nde_observacion,',
'                                   pn_mca_total               => :P156_VALOR_NC,',
'                                   pn_pca_id                  => :f_pca_id,',
'                                   pn_tse_id                  => :f_seg_id,',
'                                   pn_age_id_agencia          => :f_age_id_agencia,',
'                                   pn_id_mov_cartera          => :P156_MOVIMIENTO_CARTERA,',
'                                   pv_error                   => :p156_error);',
'                                   ',
' pq_ven_comprobantes.pr_redirect(pv_desde        => ''P'',',
'                                  pn_app_id       => NULL,',
'                                  pn_pag_id       => NULL,',
'                                  pn_emp_id       => :f_emp_id,',
'                                  pv_session      => :session,',
'                                  pv_token        => :f_token,',
'                                  pn_user_id      => :f_user_id,',
'                                  pn_rol          => :p0_rol,',
'                                  pv_rol_desc     => :p0_rol_desc,',
'                                  pn_tree_rot     => :p0_tree_root,',
'                                  pn_opcion       => :f_opcion_id,',
'                                  pv_parametro    => :f_parametro,',
'                                  pv_empresa      => :f_empresa,',
'                                  pn_uge_id       => :f_uge_id,',
'                                  pn_uge_id_gasto => :f_uge_id_gasto,',
'                                  pv_ugestion     => :f_ugestion,',
'                                  pv_error        => :p0_error);',
'',
':p156_mca_id_nd := NULL; -- yguaman 2011/12/27',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,',
'                                                                             ''cv_coleccion_mov_caja''));',
'                                   ',
':P156_MOVIMIENTO_CARTERA := NULL;',
':p156_mca_id_pago_cuota := NULL;',
':P156_VALOR_NC := NULL;',
'END IF;',
'END;',
'-------------------------------------------------------------------------------------------------------------------------------'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error al reversar'
,p_process_when_button_id=>wwv_flow_imp.id(71801254654235609)
,p_process_when=>'GENERAR_REVERSO_PAGO_CUOTA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Reverso realizado'
,p_internal_uid=>38510270422261779628
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542520801563544546)
,p_process_sequence=>70
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_gastos_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'pq_ven_pagos_cuota.pr_carga_gastos_folios_rev(pn_cli_id => :P156_CLI_ID,',
'                           pn_emp_id => :F_EMP_ID,',
'                           pn_mca_id_movcaja_reversar =>:P156_MCA_ID_PAGO_CUOTA,',
'                           pn_tipo_reverso => :P156_TIPO_REVERSO,',
'                           pv_error  => :P0_ERROR);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38510267650293779620
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542522799549544552)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P156_MCA_OBSERVACION := null;',
':P156_MCA_TOTAL := null;',
':P156_MCA_ID_PAGO_CUOTA := NULL;',
':P156_MCA_FECHA := NULL;',
':P79_IDENTIFICACION := NULL;',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'pq_ven_listas2.pr_datos_clientes(:f_uge_id,                 ',
'                                :f_emp_id,                 ',
'                                :P156_IDENTIFICACION, ',
'                                :P156_NOMBRE,                 ',
'                                :P156_CLI_ID,',
'                                :P156_CORREO,',
'                                :P156_DIRECCION,',
'                                :P156_TIPO_DIRECCION,',
'                                :P156_TELEFONO,',
'                                :P156_TIPO_TELEFONO,',
'                                :P156_CLIENTE_EXISTE,',
'                                :P156_DIR_ID,',
'                                :P156_CLI_ID_REFERIDO,',
'                                :P156_CLI_ID_NOMBRE_REFERIDO,',
'                                :p156_tipo_identificacion,',
'                                :P0_ERROR);',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38510269648279779626
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542521234753544548)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_existe_folios'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_pagos_cuota.pr_valida_existe_folios(pn_emp_id => :F_EMP_ID,',
'                        pn_cli_id => :P156_CLI_ID,',
'                        pn_gasto  => ''TO'',',
'                        pv_error  => :P0_ERROR);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(247060380420242665)
,p_process_when=>':P156_VALIDA_PUNTO = ''P'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>38510268083483779622
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542523987006544556)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Limpia'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
':P156_MCA_ID_PAGO_CUOTA := NULL;',
':P156_MCA_FECHA := NULL;',
':P156_AGE_ID_GESTIONADO_POR := NULL;',
':P156_MCA_OBSERVACION := NULL;',
':P156_MCA_TOTAL := NULL;',
':P156_TRX_ID := NULL;',
':P156_CLI_ID_REFERIDO := NULL;',
':P156_CLI_ID_NOMBRE_REFERIDO := NULL;',
':P156_DIR_ID := NULL;',
':P156_NDE_OBSERVACION := NULL;',
':P156_PCA_ID := NULL;',
':P156_ERROR := NULL;',
':P156_CLI_ID := NULL;',
':P156_CLIENTE_EXISTE := NULL;',
':P156_MCA_ID_ND := NULL;',
':P156_MCA_ID_ANT_CLI := NULL;',
':P156_TIPO_IDENTIFICACION := NULL;',
':P156_IDENTIFICACION := NULL;',
':P156_NOMBRE := NULL;',
':P156_CORREO := NULL;',
':P156_TIPO_DIRECCION := NULL;',
':P156_DIRECCION := NULL;',
':P156_TIPO_TELEFONO := NULL;',
':P156_TELEFONO := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(38542485705566544461)
,p_internal_uid=>38510270835736779630
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38542521978463544549)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_vALIDA_PUNTO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'  ln_pue_id             number;',
'  lv_tipo_punto_emision varchar2(1);',
'',
'begin',
'',
'  select ca.pue_id',
'    into ln_pue_id',
'    from ven_periodos_caja ca',
'   where ca.pca_id = :f_pca_id;',
'',
'  :P156_VALIDA_punto := pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => ln_pue_id);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>38510268827193779623
);
wwv_flow_imp.component_end;
end;
/
