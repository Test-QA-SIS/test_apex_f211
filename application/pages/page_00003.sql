prompt --application/pages/page_00003
begin
--   Manifest
--     PAGE: 00003
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>3
,p_name=>'PeriodosCaja'
,p_step_title=>'PeriodosCaja'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112520'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(97259675834766988)
,p_plug_name=>'Periodos Caja'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pca.pca_id,',
'       pca.emp_id,',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(usu_id) username,',
'       pca.pue_id,',
'       p.pue_nombre,',
'       pca.pca_fechahora_inicio,',
'       pca.pca_fechahora_final,',
'       pca.pca_valor_cierre_sistema,',
'       pca.pca_valor_cierre_fisico,',
'       pq_lv_kdda.fn_obtiene_valor_lov(176,pca.pca_estado_pc) estado_pc,',
'       pca.pca_fecha_sistema,',
'       pq_lv_kdda.fn_obtiene_valor_lov(78,pca.pca_estado_registro) estado,',
'       ''imprimir'' imprimir,',
'       :P0_BD conn',
'FROM  ',
'       ven_periodos_caja pca,',
'       asdm_puntos_emision p',
'       ',
'WHERE  pca.emp_id = :f_emp_id',
'       AND pca.usu_id = :f_user_id',
'       and p.pue_id = pca.pue_id',
'       and p.uge_id = :f_uge_id',
'ORDER  BY 1 DESC'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(97259861581766992)
,p_name=>'Periodos Caja'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#ws/small_page.gif" alt="" />'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>65006710312002066
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97259955414766996)
,p_db_column_name=>'PCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Periodo Caja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97260057237766997)
,p_db_column_name=>'EMP_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Emp_Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(266941952873216850)
,p_db_column_name=>'PUE_ID'
,p_display_order=>3
,p_column_identifier=>'O'
,p_column_label=>'# Caja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97260369812766998)
,p_db_column_name=>'PCA_FECHAHORA_INICIO'
,p_display_order=>4
,p_column_identifier=>'E'
,p_column_label=>'Fecha Inicio'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'PCA_FECHAHORA_INICIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97260452450766998)
,p_db_column_name=>'PCA_FECHAHORA_FINAL'
,p_display_order=>5
,p_column_identifier=>'F'
,p_column_label=>'Fecha Final'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'PCA_FECHAHORA_FINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97260578433766998)
,p_db_column_name=>'PCA_VALOR_CIERRE_SISTEMA'
,p_display_order=>6
,p_column_identifier=>'G'
,p_column_label=>'Valor Cierre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_VALOR_CIERRE_SISTEMA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97260661701766999)
,p_db_column_name=>'PCA_VALOR_CIERRE_FISICO'
,p_display_order=>7
,p_column_identifier=>'H'
,p_column_label=>'Valor Cierre Caja<br> Sistema'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_VALOR_CIERRE_FISICO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(97260878383766999)
,p_db_column_name=>'PCA_FECHA_SISTEMA'
,p_display_order=>8
,p_column_identifier=>'J'
,p_column_label=>'Fecha Sistema'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'PCA_FECHA_SISTEMA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(99538477601403962)
,p_db_column_name=>'USERNAME'
,p_display_order=>9
,p_column_identifier=>'L'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
,p_static_id=>'USERNAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(99593974549932930)
,p_db_column_name=>'ESTADO_PC'
,p_display_order=>10
,p_column_identifier=>'M'
,p_column_label=>'Estado Caja'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
,p_static_id=>'ESTADO_PC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(99594068642932938)
,p_db_column_name=>'ESTADO'
,p_display_order=>11
,p_column_identifier=>'N'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
,p_static_id=>'ESTADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(89121563808353475)
,p_db_column_name=>'PUE_NOMBRE'
,p_display_order=>12
,p_column_identifier=>'P'
,p_column_label=>'Pue Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(146274871489088170)
,p_db_column_name=>'IMPRIMIR'
,p_display_order=>13
,p_column_identifier=>'Q'
,p_column_label=>'Imprimir'
,p_column_link=>'javascript:popUp2(''../../reports/rwservlet?module=cierre_caja.rdf&userid=asdm_p/asdm_p@#CONN#&destype=cache&desformat=pdf&pn_pca_id=#PCA_ID#&pn_emp_id=&F_EMP_ID.'', 850, 500,85);'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'IMPRIMIR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(146308867471872273)
,p_db_column_name=>'CONN'
,p_display_order=>14
,p_column_identifier=>'R'
,p_column_label=>'Conn'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_tz_dependent=>'N'
,p_static_id=>'CONN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(97261067829767005)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1275008750919693'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PUE_NOMBRE:PCA_ID:PCA_FECHAHORA_INICIO:PCA_FECHAHORA_FINAL:PCA_VALOR_CIERRE_SISTEMA:PCA_VALOR_CIERRE_FISICO:ESTADO:ESTADO_PC:USERNAME:IMPRIMIR'
,p_sort_column_1=>'PCA_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp.component_end;
end;
/
