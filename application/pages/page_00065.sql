prompt --application/pages/page_00065
begin
--   Manifest
--     PAGE: 00065
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>65
,p_tab_set=>'Menu'
,p_name=>'Reporte Listado de Facturas Pago Cuota'
,p_step_title=>'Reporte Listado de Facturas Pago Cuota'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(106839483874603415)
,p_plug_name=>'<SPAN STYLE="font-size: 13pt">Listado Facturas de Cliente &P6_CLI_ID. - &P6_NOMBRE.</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'  FROM (SELECT com.emp_id,',
'               com.com_tipo,',
'               com.com_id,',
'               com.com_numero,',
'               com.com_fecha,',
'               com.cli_id,',
'               vcli.per_segundo_apellido || vcli.per_primer_apellido ||',
'               vcli.per_primer_nombre || vcli.per_segundo_nombre cliente,               ',
'               com.pol_id,',
'               com.com_plazo,',
'               com.ord_id,',
'               pq_ven_retorna_sec_id.fn_ord_sec_id(com.ord_id) ord_sec_id,',
'               com.age_id_agente,',
'               kseg_p.pq_kseg_devuelve_datos.fn_nombre_usuario(agt.usu_id) vendedor,',
'               com.age_id_agencia,',
'               age.age_nombre_comercial agencia,',
'               vco.var_id,',
'               vco.vco_valor_variable,',
'               vcm.cxc_id,',
'               (SELECT cxc.cxc_saldo',
'                  FROM car_cuentas_por_cobrar cxc',
'                 WHERE cxc.cxc_id = vcm.cxc_id) saldo_cartera,',
'               (CASE WHEN (SELECT COUNT(*)',
'               FROM car_dividendos div',
'               WHERE div.div_descuento_x_rol = ''S''',
'               AND div.cxc_id = vcm.cxc_id) >0 THEN',
'               ''S''',
'              ELSE ''-'' END) AS Dscto_x_Rol',
'          FROM ven_comprobantes         com,',
'               ven_var_comprobantes     vco,',
'               asdm_agencias            age,',
'               asdm_agentes             agt,',
'               v_asdm_clientes_personas vcli,',
'               v_car_movimientos        vcm',
'         WHERE age.age_id = com.age_id_agencia',
'           AND vcli.cli_id = com.cli_id',
'           AND agt.age_id = com.age_id_agente',
'           AND vco.com_id = com.com_id',
'              /*AND com.com_fecha >=                        ',
'               nvl(trunc(to_date(:P65_FECHADESDE)),SYSDATE - 30) and com.com_fecha <= nvl(trunc(to_date(:P65_FECHAHASTA)),',
'              sysdate) */',
'',
'             AND (com.com_fecha >=                        ',
'              trunc(to_date(:P65_FECHADESDE)) or :P65_FECHADESDE is null) and (com.com_fecha <= trunc(to_date(:P65_FECHAHASTA)+1) or  :P65_FECHAHASTA is null)',
'',
'           AND com.emp_id = :F_EMP_ID',
'           AND age.uge_id = :F_UGE_ID',
'              ---- Para sacar el total del credito que falta por pagar',
'           AND vcm.com_id = com.com_id',
'           AND com.cli_id = :P6_CLI_ID',
'        ) var_com pivot(SUM(var_com.vco_valor_variable) FOR var_id IN(3 "TOTAL FAC",',
'                                                                      170',
unistr('                                                                      "CUOTA PAGAR\00BF\00BF"))'),
' ORDER BY com_id DESC',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(106839778670603551)
,p_name=>'Listado Facturas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_sort_asc_image=>'blue_arrow_up.gif'
,p_sort_desc_image=>'blue_arrow_down.gif'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y_OF_Z'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.::P52_COM_ID,P52_AGENTE,P52_VALIDA_CONS_CARTERA:#COM_ID#,#AGE_ID_AGENTE#,3'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>74586627400838625
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106927468695105808)
,p_db_column_name=>'EMP_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106927574532105860)
,p_db_column_name=>'COM_TIPO'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106927666669105861)
,p_db_column_name=>'COM_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'# Comp. Interno'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106927771755105861)
,p_db_column_name=>'COM_NUMERO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'# Comp. Impreso'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106927863373105861)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106927955538105861)
,p_db_column_name=>'CLI_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928060443105862)
,p_db_column_name=>'CLIENTE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928170651106022)
,p_db_column_name=>'POL_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928268832106022)
,p_db_column_name=>'COM_PLAZO'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928365184106022)
,p_db_column_name=>'ORD_ID'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Orden'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928453560106022)
,p_db_column_name=>'ORD_SEC_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Orden Sec '
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928582886106026)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928652478106026)
,p_db_column_name=>'VENDEDOR'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Vendedor'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VENDEDOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928764587106026)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928865840106027)
,p_db_column_name=>'AGENCIA'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106928976697106027)
,p_db_column_name=>'CXC_ID'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106929067210106027)
,p_db_column_name=>'SALDO_CARTERA'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Saldo Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SALDO_CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106929151493106027)
,p_db_column_name=>'TOTAL FAC'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Total Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TOTAL FAC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(106929276161106027)
,p_db_column_name=>unistr('CUOTA PAGAR\00BF\00BF')
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Cuota '
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>unistr('CUOTA PAGAR\00BF\00BF')
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(107054655487123740)
,p_db_column_name=>'DSCTO_X_ROL'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Dscto X Rol'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DSCTO_X_ROL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(106846664433603922)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'745936'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>unistr('COM_TIPO:COM_ID:COM_NUMERO:COM_FECHA:CXC_ID:COM_PLAZO:ORD_ID:VENDEDOR:AGENCIA:CUOTA PAGAR\00BF\00BF:TOTAL FAC:SALDO_CARTERA')
,p_sort_column_1=>'C002'
,p_sort_direction_1=>'DESC'
,p_sum_columns_on_break=>'TOTAL FAC:SALDO_CARTERA'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(106959363818227443)
,p_application_user=>'APXWS_ALTERNATIVE'
,p_name=>'Cartera'
,p_report_seq=>10
,p_report_alias=>'747063'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'COM_TIPO:COM_ID:COM_NUMERO:COM_FECHA:CXC_ID:COM_PLAZO:ORD_ID:VENDEDOR:AGENCIA:APXWS_CC_001:TOTAL FAC:SALDO_CARTERA'
,p_sort_column_1=>'C002'
,p_sort_direction_1=>'DESC'
,p_sum_columns_on_break=>'SALDO_CARTERA:TOTAL FAC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(106994054234432793)
,p_report_id=>wwv_flow_imp.id(106959363818227443)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'SALDO_CARTERA'
,p_operator=>'>'
,p_expr=>'0'
,p_condition_sql=>'"SALDO_CARTERA" > to_number(#APXWS_EXPR#)'
,p_condition_display=>'#APXWS_COL_NAME# > #APXWS_EXPR_NUMBER#  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_worksheet_computation(
 p_id=>wwv_flow_imp.id(106994157451432793)
,p_report_id=>wwv_flow_imp.id(106959363818227443)
,p_db_column_name=>'APXWS_CC_001'
,p_column_identifier=>'C01'
,p_computation_expr=>'ROUND( S,2)'
,p_format_mask=>'999G999G999G999G990D00'
,p_column_type=>'NUMBER'
,p_column_label=>'Cuota'
,p_report_label=>'Cuota'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106846852796603952)
,p_name=>'P65_FECHAHASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'FechaHasta'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit()"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106847075255604060)
,p_name=>'P65_COL_HEAD8'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head8'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106847258614604060)
,p_name=>'P65_COL_HEAD7'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head7'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106847474579604061)
,p_name=>'P65_COL_HEAD9'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head9'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106847676511604061)
,p_name=>'P65_COL_HEAD5'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head5'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106847853502604061)
,p_name=>'P65_COL_HEAD13'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head13'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106848051395604061)
,p_name=>'P65_COL_HEAD14'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head14'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106848274779604062)
,p_name=>'P65_COL_HEAD15'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head15'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106848471458604062)
,p_name=>'P65_COL_HEAD1'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head1'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106848683109604062)
,p_name=>'P65_COL_HEAD2'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head2'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106848856731604062)
,p_name=>'P65_COL_HEAD6'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head6'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106849082506604062)
,p_name=>'P65_COL_HEAD10'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head10'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106849282578604062)
,p_name=>'P65_FECHADESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'FechaDesde'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit()"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106849459986604063)
,p_name=>'P65_COL_HEAD3'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head3'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106849660511604063)
,p_name=>'P65_COL_HEAD4'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head14'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106849854694604095)
,p_name=>'P65_COL_HEAD11'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head11'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(106850078495604095)
,p_name=>'P65_COL_HEAD12'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(106839483874603415)
,p_prompt=>'Col Head12'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(106850270798604096)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_COLECCION'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'  CURSOR tip_cat IS',
'     select ',
'             replace(c.vtt_etiqueta,',
'                     '''',',
'                     ''_'') variables',
'        from ppr_variables_ttransaccion c',
'       where c.emp_id = 1',
'         and c.ttr_id =',
'             pq_constantes.fn_retorna_constante(NULL,',
'                                                ''cn_ttr_id_comprobante'')',
'         and c.vtt_mostrar_cabecera = ''S''',
'         and c.vtt_estado_registro = 0',
'       order by c.vtt_orden;',
'       ',
'  lv_sentencia VARCHAR2(3000);',
'  i            NUMBER;',
'',
'BEGIN',
'',
'   pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_col_comprobantes''));',
'',
'   pq_ven_comprobantes.pr_carga_col_comprobantes(:F_EMP_ID,',
'                                                 :F_UGE_ID,',
'                                                 :P65_FECHADESDE,',
'                                                 :P65_FECHAHASTA,',
'                                                 :P0_ERROR);',
'',
' i            := 0;',
'  lv_sentencia := NULL;',
'  FOR reg IN tip_cat LOOP',
'    i := i + 1;',
'    htmldb_util.set_session_state(''P65_COL_HEAD'' || i, reg.variables);',
'  END LOOP;',
'  i := 10;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>74597119528839170
);
wwv_flow_imp.component_end;
end;
/
