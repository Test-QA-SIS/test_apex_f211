prompt --application/pages/page_00105
begin
--   Manifest
--     PAGE: 00105
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>105
,p_name=>'Creditos Refinanciados'
,p_step_title=>'Creditos Refinanciados'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(51240860280590205)
,p_plug_name=>'Creditos Refinanciados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cxc.cxc_fecha_adicion fecha_reverso,',
'       cxc.cxc_id,',
'       cxc.ede_id,',
'       (SELECT ede.ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cxc.ede_id) tipo_cartera,',
'       cxc.cxc_saldo,',
'       cxc.com_id,',
'       (SELECT co.com_fecha',
'          FROM ven_comprobantes co',
'         WHERE co.com_id = cxc.com_id) fecha_com,',
'       cxc.uge_id,',
'       (SELECT uge.uge_nombre',
'          FROM asdm_unidades_gestion uge',
'         WHERE cxc.uge_id = uge.uge_id) ugestion,',
'       ',
'       (SELECT u.username',
'          FROM kseg_e.kseg_usuarios u',
'         WHERE u.usuario_id = cxc.usu_id) usuario',
'',
'  FROM car_cuentas_por_cobrar cxc',
' WHERE cxc.cxc_id_refin IS NOT NULL',
'   AND cxc.cxc_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(51240970117590205)
,p_name=>'Creditos Refinanciados'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_detail_link_condition_type=>'NEVER'
,p_icon_view_columns_per_row=>1
,p_owner=>'JORDONEZ'
,p_internal_uid=>18987818847825279
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241172462590207)
,p_db_column_name=>'FECHA_REVERSO'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Fecha Reverso'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_REVERSO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241263279590209)
,p_db_column_name=>'CXC_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Cxc Id'
,p_column_link=>'f?p=&APP_ID.:105:&SESSION.::&DEBUG.::P105_CXC_ID:#CXC_ID#'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241353370590209)
,p_db_column_name=>'EDE_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241455373590209)
,p_db_column_name=>'TIPO_CARTERA'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Tipo Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO_CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241575019590209)
,p_db_column_name=>'CXC_SALDO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Cxc Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241652572590210)
,p_db_column_name=>'COM_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241777950590210)
,p_db_column_name=>'FECHA_COM'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Fecha Com'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_COM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241876482590210)
,p_db_column_name=>'UGE_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51241979764590210)
,p_db_column_name=>'UGESTION'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Ugestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51242068810590210)
,p_db_column_name=>'USUARIO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'USUARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(51242260338590393)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'189892'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'FECHA_REVERSO:CXC_ID:EDE_ID:TIPO_CARTERA:CXC_SALDO:COM_ID:FECHA_COM:UGE_ID:UGESTION:USUARIO'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(51244176496617891)
,p_name=>'Detalle Credito'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT di.div_nro_vencimiento,',
'       di.div_fecha_vencimiento,',
'       di.div_valor_capital,',
'       di.div_valor_interes,',
'       di.div_valor_cuota,',
'       di.div_saldo_cuota',
'  FROM car_dividendos di',
' WHERE cxc_id = :P105_CXC_ID',
'and di.div_nro_vencimiento > 0',
'order by di.div_nro_vencimiento'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(51244466568617894)
,p_query_column_id=>1
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'# Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(51244551593617898)
,p_query_column_id=>2
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(51244682612617899)
,p_query_column_id=>3
,p_column_alias=>'DIV_VALOR_CAPITAL'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(51244782838617899)
,p_query_column_id=>4
,p_column_alias=>'DIV_VALOR_INTERES'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Intereses'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(51244883915617899)
,p_query_column_id=>5
,p_column_alias=>'DIV_VALOR_CUOTA'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(51244972174617901)
,p_query_column_id=>6
,p_column_alias=>'DIV_SALDO_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51243378311599452)
,p_name=>'P105_CXC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(51240860280590205)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
