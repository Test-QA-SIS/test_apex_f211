prompt --application/pages/page_00102
begin
--   Manifest
--     PAGE: 00102
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>102
,p_name=>'REFINANCIMIENTO'
,p_alias=>'REFINANCIAMIENTO'
,p_step_title=>'REFINANCIMIENTO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(36730869744426092)
,p_plug_name=>'Datos Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(36826855362483590)
,p_plug_name=>'Cartera Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli_id,',
'       (SELECT cli.nombre_completo',
'          FROM v_asdm_datos_clientes cli',
'         WHERE cli.cli_id = cxc.cli_id',
'           AND cli.emp_id = cxc.emp_id) nombre,',
'       cxc.cxc_id,',
'       cxc.cxc_saldo,',
'       (SELECT ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cxc.ede_id',
'           AND ede.emp_id = cxc.emp_id) cartera,',
'       (SELECT uge.uge_nombre',
'          FROM asdm_unidades_gestion uge',
'         WHERE uge.uge_id = cxc.uge_id',
'           AND uge.emp_id = cxc.emp_id) unidad_gestion,',
'           cxc.ord_id,',
'           cxc.com_id,',
'      ''CARGAR'' cargar,',
'   CXC.CXC_FECHA_ADICION,',
'   pq_car_refinanciamiento.fn_retorna_credito_seg(pn_cxc_id => cxc.cxc_id,',
'                                                            pn_ord_id => cxc.ord_id) cxc_id_seguro',
'  FROM car_cuentas_por_cobrar cxc',
' WHERE cxc.com_id IS NOT NULL',
'   AND cxc.cli_id IS NOT NULL',
'   AND cxc.cxc_id_refin IS NULL',
'   AND cxc.cxc_saldo > 0',
'   AND cxc.cli_id = :p102_cli_id',
'   AND NOT EXISTS (SELECT col.c002',
'          FROM apex_collections col',
'         WHERE col.collection_name = ''COL_CREDITOS_REFIN''',
'           AND col.c002 = cxc.cxc_id)',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cli_id',
'  FROM car_cuentas_por_cobrar cxc',
' WHERE cxc.com_id IS NOT NULL',
'   AND cxc.cli_id IS NOT NULL',
'   AND cxc.cxc_id_refin IS NULL',
'   AND cxc.cxc_saldo > 0',
'   AND cxc.cli_id = :p102_cli_id',
'   AND NOT EXISTS (SELECT col.c002',
'          FROM apex_collections col',
'         WHERE col.collection_name = ''COL_CREDITOS_REFIN''',
'           AND col.c002 = cxc.cxc_id)'))
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(36826953621483590)
,p_name=>'cartera cliente'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>4573802351718664
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36827175145483617)
,p_db_column_name=>'CLI_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36827271864483619)
,p_db_column_name=>'NOMBRE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(239916504304360028)
,p_db_column_name=>'ORD_ID'
,p_display_order=>12
,p_column_identifier=>'I'
,p_column_label=>'Orden de venta'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(241392466738229166)
,p_db_column_name=>'COM_ID'
,p_display_order=>22
,p_column_identifier=>'J'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36827371691483619)
,p_db_column_name=>'CXC_ID'
,p_display_order=>32
,p_column_identifier=>'C'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51294980472931225)
,p_db_column_name=>'CXC_FECHA_ADICION'
,p_display_order=>42
,p_column_identifier=>'H'
,p_column_label=>unistr('Fecha Adici\00F3n')
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_FECHA_ADICION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36827459794483619)
,p_db_column_name=>'CXC_SALDO'
,p_display_order=>52
,p_column_identifier=>'D'
,p_column_label=>'Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CXC_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36827553854483620)
,p_db_column_name=>'CARTERA'
,p_display_order=>62
,p_column_identifier=>'E'
,p_column_label=>'Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36827675139483620)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>72
,p_column_identifier=>'F'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36860060353570954)
,p_db_column_name=>'CARGAR'
,p_display_order=>82
,p_column_identifier=>'G'
,p_column_label=>'Cargar'
,p_column_link=>'f?p=&APP_ID.:102:&SESSION.:CARGAR:&DEBUG.::P102_CXC_ID,P102_ORD_ID,P102_FECHA_CORTE,P102_COM_ID,P102_CXC_ID_SEGURO:#CXC_ID#,#ORD_ID#,,#COM_ID#,#CXC_ID_SEGURO#'
,p_column_linktext=>'#CARGAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CARGAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(256607303821996838)
,p_db_column_name=>'CXC_ID_SEGURO'
,p_display_order=>92
,p_column_identifier=>'K'
,p_column_label=>'Cxc id seguro'
,p_column_link=>'f?p=&APP_ID.:162:&SESSION.::&DEBUG.:RP:P162_CXC_ID_SEGURO,P162_ORD_ID:#CXC_ID_SEGURO#,#ORD_ID#'
,p_column_linktext=>'#CXC_ID_SEGURO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(36827855146483957)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'45748'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:NOMBRE:ORD_ID:CXC_ID:CXC_ID_SEGURO:CXC_FECHA_ADICION:CXC_SALDO:CARTERA:UNIDAD_GESTION:COM_ID:CARGAR:'
,p_sort_column_1=>'CXC_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(36895558031052759)
,p_name=>'Cartera a Refinanciar'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>25
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001 credito,',
'c002 Nro_Vencimiento,',
'c003 Fecha_Vencimiento,',
'to_number(c004) Valor_Capital,',
'to_number(c005) Valor_Interes,',
'to_number(c006) Valor_Cuota,',
'to_number(c007) Deuda_Presente,',
'to_number(c008) Saldo_Cuota',
'from apex_collections where collection_name = ''COL_CARTERA_REFIN''',
'ORDER BY TO_NUMBER(C002)'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001 credito',
'from apex_collections where collection_name = ''COL_CARTERA_REFIN'''))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36971671413823013)
,p_query_column_id=>1
,p_column_alias=>'CREDITO'
,p_column_display_sequence=>1
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36971755830823014)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36972755614827920)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36971868435823014)
,p_query_column_id=>4
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36971978088823014)
,p_query_column_id=>5
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36972078216823014)
,p_query_column_id=>6
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36972172668823014)
,p_query_column_id=>7
,p_column_alias=>'DEUDA_PRESENTE'
,p_column_display_sequence=>8
,p_column_heading=>'Deuda Presente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57237373533723129)
,p_query_column_id=>8
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(36930776010530980)
,p_name=>'VALORES A REFINANCIAR'
,p_parent_plug_id=>wwv_flow_imp.id(36895558031052759)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT col.seq_id id,',
'       to_number(col.c001) com_id,',
'       to_number(col.c002) cxc_id,',
'       to_number(col.c003) cli_id,',
'       to_number(col.c004) total,',
'       col.c006 uge_id,',
'       col.c006||'' - ''||col.c007 uge_nombre,',
'       ''ELIMINAR'' eliminar',
'  FROM apex_collections col',
' WHERE col.collection_name = ''COL_CREDITOS_REFIN'''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36964674868748283)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36959874775691539)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36959977280691542)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>4
,p_column_heading=>unistr('Cr\00E9dito')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36960052302691542)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36960176853691542)
,p_query_column_id=>5
,p_column_alias=>'TOTAL'
,p_column_display_sequence=>7
,p_column_heading=>'Valor Presente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57238954363783818)
,p_query_column_id=>6
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57239073684783823)
,p_query_column_id=>7
,p_column_alias=>'UGE_NOMBRE'
,p_column_display_sequence=>6
,p_column_heading=>unistr('Centro de gesti\00F3n')
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(36961368672699190)
,p_query_column_id=>8
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>8
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:102:&SESSION.:ELIMINA:&DEBUG.::P102_SEQ_ID:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(36974179341872558)
,p_plug_name=>'Refinanciar'
,p_parent_plug_id=>wwv_flow_imp.id(36930776010530980)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>51
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(239916738794360030)
,p_name=>'DETALLE DEL SEGURO'
,p_parent_plug_id=>wwv_flow_imp.id(36895558031052759)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT s.ose_id,',
'       s.cxc_id_seguro,',
'       s.ose_plazo_cobertura,',
'       s.pse_valor_mensual,',
'       s.cse_id,',
'       s.pse_poliza_seguro,',
'       s.pse_codigo_producto,',
'       s.pse_campania,',
'       s.pse_codigo_paquete,',
'       s.ose_fecha,',
'       cse.cse_fecha_ini,',
'       cse.cse_fecha_fin,',
'       cse.cse_plazo,',
'       cse.cse_fecha_envio,',
'       cse.pse_num_poliza,',
'       cxc.cxc_id,',
'       cxc.cxc_saldo,',
'       cxc.cxc_fecha_adicion',
'  FROM ven_ordenes_seguros    s,',
'       car_cuentas_seguros    cse,',
'       car_cuentas_por_cobrar cxc',
' WHERE s.ord_origen = :p102_ord_id',
'   AND s.cse_id = cse.cse_id',
'   AND cse.cxc_id = cxc.cxc_id',
'   --AND s.cxc_id_venta = :p102_cxc_id',
'   AND s.cxc_id_seguro = :P102_CXC_ID_SEGURO',
'   AND cxc.cxc_saldo > 0;',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT s.ose_id',
'  FROM ven_ordenes_seguros    s,',
'       car_cuentas_seguros    cse,',
'       car_cuentas_por_cobrar cxc',
' WHERE s.ord_origen = :p102_ord_id',
'   AND s.cse_id = cse.cse_id',
'   AND cse.cxc_id = cxc.cxc_id',
'   --AND s.cxc_id_venta = :p102_cxc_id',
'   AND s.cxc_id_seguro = :P102_CXC_ID_SEGURO',
'   AND cxc.cxc_saldo > 0;'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239916761095360031)
,p_query_column_id=>1
,p_column_alias=>'OSE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ose id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239916870597360032)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID_SEGURO'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id seguro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239916983292360033)
,p_query_column_id=>3
,p_column_alias=>'OSE_PLAZO_COBERTURA'
,p_column_display_sequence=>3
,p_column_heading=>'Plazo cobertura'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917067761360034)
,p_query_column_id=>4
,p_column_alias=>'PSE_VALOR_MENSUAL'
,p_column_display_sequence=>4
,p_column_heading=>'Valor mensual'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917233581360035)
,p_query_column_id=>5
,p_column_alias=>'CSE_ID'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917280483360036)
,p_query_column_id=>6
,p_column_alias=>'PSE_POLIZA_SEGURO'
,p_column_display_sequence=>7
,p_column_heading=>unistr('P\00F3liza seguro')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917434384360037)
,p_query_column_id=>7
,p_column_alias=>'PSE_CODIGO_PRODUCTO'
,p_column_display_sequence=>8
,p_column_heading=>'Producto'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917458386360038)
,p_query_column_id=>8
,p_column_alias=>'PSE_CAMPANIA'
,p_column_display_sequence=>9
,p_column_heading=>unistr('Campa\00F1a')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917556912360039)
,p_query_column_id=>9
,p_column_alias=>'PSE_CODIGO_PAQUETE'
,p_column_display_sequence=>10
,p_column_heading=>'Paquete'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917699784360040)
,p_query_column_id=>10
,p_column_alias=>'OSE_FECHA'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917791915360041)
,p_query_column_id=>11
,p_column_alias=>'CSE_FECHA_INI'
,p_column_display_sequence=>12
,p_column_heading=>'Fecha inicial'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917896428360042)
,p_query_column_id=>12
,p_column_alias=>'CSE_FECHA_FIN'
,p_column_display_sequence=>13
,p_column_heading=>'Fecha final'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239917976025360043)
,p_query_column_id=>13
,p_column_alias=>'CSE_PLAZO'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918120692360044)
,p_query_column_id=>14
,p_column_alias=>'CSE_FECHA_ENVIO'
,p_column_display_sequence=>15
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918186475360045)
,p_query_column_id=>15
,p_column_alias=>'PSE_NUM_POLIZA'
,p_column_display_sequence=>16
,p_column_heading=>unistr('Num p\00F3liza')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918342954360046)
,p_query_column_id=>16
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918413767360047)
,p_query_column_id=>17
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>5
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918546161360048)
,p_query_column_id=>18
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(42480560496436313)
,p_name=>'prueba ddd'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CXC_ID,',
'               CRE_NRO_VENCIMIENTO,',
'               CRE_FECHA_VENCIMIENTO,',
'               CRE_VALOR_CAPITAL,',
'               CRE_VALOR_INTERES,',
'               CRE_VALOR_COUTA,',
'               CRE_DEUDA,',
'               CRE_SALDO_CUOTA',
'          FROM CAR_CARTERA_REFINANCIAMIENTO',
'         WHERE CXC_ID = :p102_CXC_ID;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651170112693660)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651356662693662)
,p_query_column_id=>2
,p_column_alias=>'CRE_NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Cre Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651452247693662)
,p_query_column_id=>3
,p_column_alias=>'CRE_FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Cre Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651552780693662)
,p_query_column_id=>4
,p_column_alias=>'CRE_VALOR_CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Cre Valor Capital'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651676211693662)
,p_query_column_id=>5
,p_column_alias=>'CRE_VALOR_INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Cre Valor Interes'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651771032693662)
,p_query_column_id=>6
,p_column_alias=>'CRE_VALOR_COUTA'
,p_column_display_sequence=>6
,p_column_heading=>'Cre Valor Couta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651875792693662)
,p_query_column_id=>7
,p_column_alias=>'CRE_DEUDA'
,p_column_display_sequence=>7
,p_column_heading=>'Cre Deuda'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(17967651975263693662)
,p_query_column_id=>8
,p_column_alias=>'CRE_SALDO_CUOTA'
,p_column_display_sequence=>8
,p_column_heading=>'Cre Saldo Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(199339208551412633)
,p_name=>'PRUEBA FECHA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    SELECT to_date(c.c002, ''dd/mm/yyyy'')',
'      FROM apex_collections c',
'     WHERE c.collection_name = ''COLL_FECHAS_CORTE''',
'       AND rownum = 1;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(199339299627412634)
,p_query_column_id=>1
,p_column_alias=>'TO_DATE(C.C002,''DD/MM/YYYY'')'
,p_column_display_sequence=>1
,p_column_heading=>'To date(c.c002,&#x27;dd&#x2F;mm&#x2F;yyyy&#x27;)'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(239918713794360050)
,p_name=>'CRONOGRAMA DE CUOTAS PARA SEGURO'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.seq_id, c.c001, c.c002, TO_NUMBER(c.c003) c003, c.c004, c.c005, c.c006, c.c007',
'  FROM apex_collections c',
' WHERE c.collection_name = ''COL_COBERTURA'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918758213360051)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239918936503360052)
,p_query_column_id=>2
,p_column_alias=>'C001'
,p_column_display_sequence=>2
,p_column_heading=>'Plazo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919004580360053)
,p_query_column_id=>3
,p_column_alias=>'C002'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919136369360054)
,p_query_column_id=>4
,p_column_alias=>'C003'
,p_column_display_sequence=>4
,p_column_heading=>'Valor mensual'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919243110360055)
,p_query_column_id=>5
,p_column_alias=>'C004'
,p_column_display_sequence=>5
,p_column_heading=>'pse_id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919336133360056)
,p_query_column_id=>6
,p_column_alias=>'C005'
,p_column_display_sequence=>6
,p_column_heading=>'Origen'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919381966360057)
,p_query_column_id=>7
,p_column_alias=>'C006'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919548298360058)
,p_query_column_id=>8
,p_column_alias=>'C007'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(239919582807360059)
,p_name=>'Cronograma seguro gen PRUEBA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT c.seq_id,',
'        c.c001,',
'        c.c002,',
'        TO_NUMBER(c.c003) c003,',
'        c.c004,',
'        c.c005,',
'        c.c006,',
'        TO_NUMBER(c.c007) c007,',
'        c.c008',
'   FROM apex_collections c',
'  WHERE c.collection_name = ''COL_COBERTURA_SEG'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919686550360060)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919819623360061)
,p_query_column_id=>2
,p_column_alias=>'C001'
,p_column_display_sequence=>2
,p_column_heading=>'Plazo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239919877015360062)
,p_query_column_id=>3
,p_column_alias=>'C002'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920003776360063)
,p_query_column_id=>4
,p_column_alias=>'C003'
,p_column_display_sequence=>4
,p_column_heading=>'Valor mensual'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920138395360064)
,p_query_column_id=>5
,p_column_alias=>'C004'
,p_column_display_sequence=>5
,p_column_heading=>'PSE_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920169505360065)
,p_query_column_id=>6
,p_column_alias=>'C005'
,p_column_display_sequence=>6
,p_column_heading=>'oRDEN DE VENTA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920311664360066)
,p_query_column_id=>7
,p_column_alias=>'C006'
,p_column_display_sequence=>7
,p_column_heading=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920421204360067)
,p_query_column_id=>8
,p_column_alias=>'C007'
,p_column_display_sequence=>8
,p_column_heading=>'SALDO CAPITAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920481758360068)
,p_query_column_id=>9
,p_column_alias=>'C008'
,p_column_display_sequence=>9
,p_column_heading=>'PORC CAPITAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(241391997489229161)
,p_plug_name=>'RESULTADOS REFINANCIAMIENTO'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'C001',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(42554158078911434)
,p_name=>unistr('CUOTAS NUEVO CR\00C9DITO')
,p_parent_plug_id=>wwv_flow_imp.id(241391997489229161)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'C001 Nro_Vencimiento,',
'c002 Fecha_Vencimiento,',
'to_number(c003) Valor_Capital,',
'to_number(c004) Valor_Interes,',
'to_number(c005) Valor_Cuota',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_display_when_condition=>':P102_EDE_ID_CARTERA=''MAR'''
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>30
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42737073521297878)
,p_query_column_id=>1
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42737160050297883)
,p_query_column_id=>2
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42737283561297883)
,p_query_column_id=>3
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Capital'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42737364623297884)
,p_query_column_id=>4
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Interes'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42737480127297885)
,p_query_column_id=>5
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(42759580110375489)
,p_plug_name=>'Valores Credito'
,p_parent_plug_id=>wwv_flow_imp.id(42554158078911434)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(43049856881834841)
,p_plug_name=>'Autorizacion'
,p_parent_plug_id=>wwv_flow_imp.id(241391997489229161)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>120
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(239920624540360069)
,p_name=>'NUEVO CRONOGRAMA DE CUOTAS PARA SEGURO'
,p_parent_plug_id=>wwv_flow_imp.id(241391997489229161)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT a.seq_id seq_id,',
'             a.c001   nro_vencimiento,',
'             a.c002   fecha_vencimiento,',
'             to_number(a.c003)   capital,',
'             to_number(a.c004)   interes,',
'             a.c006   saldo_capital,',
'             to_number(a.c007)   valor_cuota,',
'             a.c008   descuento_rol',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''COL_COBERTURA_SEG'';',
''))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT a.seq_id seq_id',
'        FROM apex_collections a',
'       WHERE a.collection_name = ''COL_COBERTURA_SEG'';'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>30
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(239920680310360070)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241388835997229129)
,p_query_column_id=>2
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Nro vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241388889557229130)
,p_query_column_id=>3
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241389030126229131)
,p_query_column_id=>4
,p_column_alias=>'CAPITAL'
,p_column_display_sequence=>4
,p_column_heading=>'Capital'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241389094595229132)
,p_query_column_id=>5
,p_column_alias=>'INTERES'
,p_column_display_sequence=>5
,p_column_heading=>'Interes'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241389235170229133)
,p_query_column_id=>6
,p_column_alias=>'SALDO_CAPITAL'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241389324879229134)
,p_query_column_id=>7
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Valor cuota'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(241389397385229135)
,p_query_column_id=>8
,p_column_alias=>'DESCUENTO_ROL'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(241392322714229164)
,p_plug_name=>'COMPROBANTES DEL INTERES DIFERIDO'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>35
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'') > 0'))
,p_plug_display_when_cond2=>'SQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(57380569578854177)
,p_name=>unistr('Interes Diferido No Cobrado - Nota de D\00E9bito')
,p_parent_plug_id=>wwv_flow_imp.id(241392322714229164)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''ND'''', this.value ,''''P102_UGE_ID'''',''''P102_USER_ID'''',''''P102_EMP_ID'''',''''P102_CLI_ID'''',''''R25127418309089251'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'');}'';'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57380755500854194)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57380868423854205)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57380960103854205)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381078704854205)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381162945854205)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381281438854205)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Int. Dif'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381377081854205)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio '
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381476493854205)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381576018854205)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381670046854205)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381770935854205)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381873067854205)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57381963110854205)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382073570854205)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382163761854206)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382265249854206)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382374844854206)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382480525854206)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382565441854206)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382653207854206)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382755520854206)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382881008854206)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57382959207854206)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383063831854206)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383176271854206)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383255680854207)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383366376854207)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383477398854207)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383563180854207)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383668216854207)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383759849854207)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383865410854207)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57383976131854207)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384054730854207)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384169415854208)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384268942854208)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384380573854208)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384466892854208)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384551865854208)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384666620854208)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384771363854208)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384870688854208)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57384965544854208)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385053098854208)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385180695854208)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385257572854208)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385360206854208)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385477152854208)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385577295854208)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385661468854208)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385783951854208)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385870424854208)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57385975348854208)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386051932854209)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386154260854209)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386267919854209)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386367100854209)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386483385854209)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386558766854209)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(57386661451854209)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(163459881188488293)
,p_name=>unistr('Interes Diferido  - Nota de Cr\00E9dito')
,p_parent_plug_id=>wwv_flow_imp.id(241392322714229164)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''NC'''', this.value ,''''P102_UGE_ID'''',''''P102_USER_ID'''',''''P102_EMP_ID'''',''''P102_CLI_ID'''',''''R131206729918723367'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_display_when_condition=>'NVL(:P102_INTERESES_NO_PAG,0) > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460164933488374)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460278935488379)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460365487488379)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460480144488379)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460569861488380)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460657678488380)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INT DIF REFINANCIADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460783280488381)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>9
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460851866488381)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>7
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(163460981193488381)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>8
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P102_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(30144869357550544236)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_button_name=>'pago_cuota'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'>> Nuevo Pago Cuota'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:7:&SESSION.:CLIENTE_REFIN:&DEBUG.:RP,7:P7_NRO_IDE_REF,P7_CXC_ID_REF:&P102_IDENTIFICACION.,&P102_CXC_ID.'
,p_button_condition=>':P102_EDE_ID_CARTERA=''MAR'' AND :P102_CAR_CAPITAL IS NOT NULL AND :P102_VALOR_A_PAGAR_CAJA!=0'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42563059819987679)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_button_name=>'CALCULAR_CRONOGRA'
,p_button_static_id=>'CALCULAR_CRONOGRA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536254089046676)
,p_button_image_alt=>'Calcular'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42874577430898530)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_button_name=>'GENERA'
,p_button_static_id=>'GENERA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535660381046676)
,p_button_image_alt=>'Generar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(48472264625685006)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_button_name=>'Crear_Garante'
,p_button_static_id=>'Crear_Garante'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Crear Garante'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=147:44:&SESSION.::&DEBUG.::F_EMP_ID,F_EMPRESA,F_EMP_LOGO,F_EMP_FONDO,F_UGE_ID,F_UGE_ID_GASTO,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_POPUP,P44_PAGINA,P44_APLICACION:&F_EMP_ID.,&F_EMPRESA.,&F_EMP_LOGO.,&F_EMP_FONDO.,&F_UGE_ID.,&F_UGE_ID_GASTO.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,N,211,102'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(56239161264406489)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_button_name=>'Ir_pago_cuota'
,p_button_static_id=>'Ir_pago_cuota'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'>> Ir Pago Cuota'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.:cliente:&DEBUG.::P6_PER_TIPO_IDENTIFICACION,P6_IDENTIFICACION,P6_PAGO_REFINANCIAMIENTO,P6_VALOR_REFINANCIAMIENTO,P6_VALOR_CAPITAL_REFIN,P6_CXC_ID_REF,P6_SALDO_CXC_REF,P6_INTERESES_NO_PAG_RE:&P102_TIPO_IDENTIFICACION.,&P102_IDENTIFICACION.,1,&P102_VALOR_A_PAGAR_CAJA.,&P102_VALOR_CAPITAL.,&P102_CXC_ID.,&P102_SALDO_CXC_ORIGEN.,&P102_INTERESES_NO_PAG.'
,p_button_condition_type=>'NEVER'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(46908251603781301)
,p_branch_action=>'f?p=&APP_ID.:102:&SESSION.:graba:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(42874577430898530)
,p_branch_sequence=>5
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 20-FEB-2013 15:13 by JORDONEZ'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(36737979103504441)
,p_branch_action=>'f?p=&APP_ID.:102:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 12-DEC-2012 08:54 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36731075409426105)
,p_name=>'P102_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36731259662426116)
,p_name=>'P102_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36731472035426117)
,p_name=>'P102_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Telofono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36731656222426117)
,p_name=>'P102_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36731876084426117)
,p_name=>'P102_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36732056025426117)
,p_name=>'P102_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P102_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P102_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36732280192426120)
,p_name=>'P102_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36732483990426120)
,p_name=>'P102_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36732871276426121)
,p_name=>'P102_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36867465211648017)
,p_name=>'P102_CXC_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(36826855362483590)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36963775991729675)
,p_name=>'P102_SEQ_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(36930776010530980)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36974765620878061)
,p_name=>'P102_TOTAL_FINANCIAR'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Refinanciar'
,p_source=>':P102_VALOR_CAPITAL - NVL(:P102_VALOR_ABONADO,0)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42561562112978869)
,p_name=>'P102_TASA'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_item_default=>'RETURN ROUND(pq_car_refinanciamiento.fn_retorna_tasa_refin(pn_com_id => :p102_com_id),4);'
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tasa Interes'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-BOTTOM'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN ROUND(pq_car_refin_precancelacion.fn_retorna_tasa_refin(:p0_Error),4);',
''))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42561769048978873)
,p_name=>'P102_PLAZO'
,p_item_sequence=>126
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_prompt=>'Plazo Credito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) PLAZO, TO_NUMBER(PEN_VALOR) ID',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 1 AND :P102_PLAZO_APLICA',
' ORDER BY 1;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLUE"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42617156860923393)
,p_name=>'P102_FECHA_CORTE'
,p_item_sequence=>136
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_prompt=>'Fecha Corte'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT C002',
'       ',
'        FROM apex_collections c',
'       WHERE c.collection_name = ''COLL_FECHAS_CORTE'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'READONLY style="font-size:14;color:BLACK";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42757079979365983)
,p_name=>'P102_TOTAL_CREDITO_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Credito'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(to_number(c005)) total_credito',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43050475928840405)
,p_name=>'P102_CLAVE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_prompt=>'Clave Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43053565502884676)
,p_name=>'P102_ALERTA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_item_default=>'Solicitar Clave de Autorizacion al Departamento de Cartera'
,p_prompt=>'Alerta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:12;color:RED;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43061176902010959)
,p_name=>'P102_ALERTA_2'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(42759580110375489)
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:20;color:red;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43582677878291566)
,p_name=>'P102_VALOR_CAJA'
,p_item_sequence=>176
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Caja'
,p_source=>'return pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p102_cxc_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43593277168966991)
,p_name=>'P102_ALERTA_ABONO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_prompt=>' '
,p_source=>'''Para realizar el refinanciamiento debe cancelar: '''
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:red;font-family:Arial Narrow"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(43975578914336183)
,p_name=>'P102_GARANTE'
,p_item_sequence=>186
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>'Garante'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>60
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(49206368758629268)
,p_name=>'P102_VALOR_CAPITAL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Pendiente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(TO_NUMBER(c004))',
'from apex_collections where collection_name = ''COL_CREDITOS_REFIN'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(49206580023629271)
,p_name=>'P102_VALOR_ABONADO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Diferencia Abono Intereses'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (AB.CAR_ABONO - AB.CAR_INTERESES) < 0 THEN',
'          0',
'         ELSE',
'         ',
'          (AB.CAR_ABONO - AB.CAR_INTERESES)',
'       END ',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = :p102_cxc_id',
'   AND ab.car_estado_registro = 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ab.car_abono-ab.car_intereses--(ab.car_diferencia  + ab.car_abonos_capital)',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = 1827941',
'   AND ab.car_estado_registro = 0;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(56246069893532011)
,p_name=>'P102_VALOR_A_PAGAR_CAJA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_source=>'RETURN pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p102_cxc_id)'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(56850372803332764)
,p_name=>'P102_SALDO_CXC_ORIGEN'
,p_item_sequence=>196
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_saldo saldo_cxc_origen',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cx.cxc_id = :p102_cxc_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(56850881460335199)
,p_name=>'P102_INTERESES_NO_PAG'
,p_item_sequence=>206
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Intereses No Pag'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(INTERESES_NO_PAG) from',
'(SELECT CASE',
'         WHEN ((di.div_valor_cuota) - (di.div_saldo_cuota)) >=',
'              (di.div_valor_interes) THEN',
'          0',
'         ELSE',
'          (di.div_valor_interes) -',
'          ((di.div_valor_cuota) - (di.div_saldo_cuota))',
'       END INTERESES_NO_PAG',
'  FROM car_dividendos di',
' WHERE di.cxc_id = :p102_cxc_id',
'   AND di.div_estado_dividendo = ''P''',
'   AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'   AND di.div_estado_registro =',
'       asdm_p.pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo''));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'''disabled'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(57386764836854210)
,p_name=>'P102_INT_MORA_COB'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(57380569578854177)
,p_prompt=>'Total a Pagar:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(57757569667865594)
,p_name=>'P102_EMP_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_source=>'f_emp_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(57757766496865597)
,p_name=>'P102_UGE_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_source=>'f_uge_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(57757963767865597)
,p_name=>'P102_USER_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(43049856881834841)
,p_source=>'f_user_id'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135373155804633338)
,p_name=>'P102_EDE_ID_CARTERA'
,p_item_sequence=>216
,p_item_plug_id=>wwv_flow_imp.id(36826855362483590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155443772943517427)
,p_name=>'P102_PUE_NUM_SRI'
,p_item_sequence=>246
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(163474873552732560)
,p_name=>'P102_PUE_ID'
,p_item_sequence=>226
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(208841077249732818)
,p_name=>'P102_VALIDA_PUNTO'
,p_item_sequence=>236
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(239916632787360029)
,p_name=>'P102_ORD_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(36826855362483590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(241392427347229165)
,p_name=>'P102_COM_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(36826855362483590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(256607415575996839)
,p_name=>'P102_CXC_ID_SEGURO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(36826855362483590)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(257147358612146940)
,p_name=>'P102_CALIFICACION_VALIDA'
,p_item_sequence=>256
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_prompt=>unistr('D\00EDas Mora')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT round(SYSDATE-MIN(DIV.DIV_FECHA_VENCIMIENTO),0)',
'        FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_DIVIDENDOS DIV',
'       WHERE CXC.CXC_ID = DIV.CXC_ID',
'         AND CXC.CLI_ID =:P102_CLI_ID',
'         AND CXC.CXC_ID=:P102_CXC_ID',
'         AND DIV.DIV_SALDO_CUOTA != 0',
'AND cxc.cxc_estado!=''TMP'';'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(363684366644038537)
,p_name=>'P102_MESES_GRACIA'
,p_item_sequence=>266
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_prompt=>'Meses Gracia'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_MESES_GRACIA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) ||'' MESES'' D, TO_NUMBER(PEN_VALOR) R',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 0 AND',
'       (SELECT PMG.PMG_MESES_GRACIA D',
'          FROM CAR_CUENTAS_POR_COBRAR CXC, CAR_PARAMETROS_MESES_GRACIA PMG',
'         WHERE CXC.CXC_ID = :P102_CXC_ID',
'           AND CXC.UGE_ID = PMG.UGE_ID',
'           AND PMG.PMG_ESTADO_REGISTRO=0)',
' ORDER BY 1'))
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(435369471971009797)
,p_name=>'P102_PLAZO_APLICA'
,p_item_sequence=>125
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(474511154420589085)
,p_name=>'P102_DIFERENCIA'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_prompt=>'Valor Abonado'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT AB.CAR_ABONO',
'  FROM car_abonos_refinanciamiento ab',
' WHERE ab.cxc_id = :p102_cxc_id',
'   AND ab.car_estado_registro = 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7637138538005837268)
,p_name=>'P102_CXC_NC'
,p_item_sequence=>266
,p_item_plug_id=>wwv_flow_imp.id(36730869744426092)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(18568685069079272278)
,p_name=>'P102_CAR_CAPITAL'
,p_item_sequence=>276
,p_item_plug_id=>wwv_flow_imp.id(36974179341872558)
,p_prompt=>'Capital'
,p_source=>'SELECT A.CAR_CAPITAL FROM CAR_ABONOS_REFINANCIAMIENTO A WHERE A.CXC_ID = :P102_CXC_ID AND A.CAR_ESTADO_REGISTRO=0;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7637138585799837269)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_cliente_existe varchar2(100);',
'ln_dir_id  asdm_direcciones.dir_id%TYPE;',
'lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'ln_cli_id_referido asdm_clientes.cli_id%TYPE;',
'',
'',
'begin',
'    ASDM_P.PRUEBAS_PAOB(pn_numero  => 88888,',
'                    pc_texto1  => ''ln_cxc_id'',',
'                    pv_texto2  => ''JOHN 333'' ||:F_SEG_ID,',
'                    pn_numero1 => 444);',
'                    ',
'',
':P102_CALIFICACION_VALIDA:=null;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'',
'',
'  IF apex_collection.collection_exists(''COL_CREDITOS_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'  ',
'  END IF;',
'',
'',
'pq_ven_listas2.pr_datos_clientes(',
':f_uge_id,',
':f_emp_id,',
':P102_IDENTIFICACION,',
':P102_NOMBRE,',
':P102_CLI_ID,',
':P102_CORREO,',
':P102_DIRECCION,',
':P102_TIPO_DIRECCION,        ',
':P102_TELEFONO,              ',
':P102_TIPO_TELEFONO,        ',
'lv_cliente_existe,',
'ln_dir_id ,',
'ln_cli_id_referido ,',
'lv_nombre_cli_referido ,',
':P102_TIPO_IDENTIFICACION,',
':P0_ERROR);',
'',
'',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists( pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'  ',
'  END IF;',
'',
'',
':P102_ALERTA_2 := '' '';',
'',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'REFINNC'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>7604885434530072343
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(36733455676431463)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_cliente_existe varchar2(100);',
'ln_dir_id  asdm_direcciones.dir_id%TYPE;',
'lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'ln_cli_id_referido asdm_clientes.cli_id%TYPE;',
'',
'',
'begin',
':P102_CALIFICACION_VALIDA:=null;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'',
'',
'  IF apex_collection.collection_exists(''COL_CREDITOS_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'  ',
'  END IF;',
'',
'',
'pq_ven_listas2.pr_datos_clientes(',
':f_uge_id,',
':f_emp_id,',
':P102_IDENTIFICACION,',
':P102_NOMBRE,',
':P102_CLI_ID,',
':P102_CORREO,',
':P102_DIRECCION,',
':P102_TIPO_DIRECCION,        ',
':P102_TELEFONO,              ',
':P102_TIPO_TELEFONO,        ',
'lv_cliente_existe,',
'ln_dir_id ,',
'ln_cli_id_referido ,',
'lv_nombre_cli_referido ,',
':P102_TIPO_IDENTIFICACION,',
':P0_ERROR);',
'',
'',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists( pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'  ',
'  END IF;',
'',
'',
':P102_ALERTA_2 := '' '';',
'',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>4480304406666537
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42568476527323608)
,p_process_sequence=>11
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_cro'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_age_id_agencia     NUMBER;',
'  ld_fecha_corte        DATE;',
'  a                     NUMBER;',
'  b                     NUMBER;',
'  ln_cuota              NUMBER(15, 2) := 0;',
'  ln_cuota_minima       NUMBER;',
'  ln_plazo_cxc_original NUMBER;',
'  ln_com_id             NUMBER;',
'  lv_pue_sri            VARCHAR2(5);',
'  lv_uge_sri            VARCHAR2(5);',
'  ln_int_nd_refi        NUMBER;',
'  lv_estado_reg_activo  VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                          ''cv_estado_reg_activo'');',
'',
'  lv_div_estado_div_pendi VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                            ''cv_div_estado_div_pendiente'');',
'',
'  ln_car_id           NUMBER;',
'  ln_intereses_no_pag NUMBER;',
'  ln_crc_id           NUMBER := NULL;',
'',
'BEGIN',
'',
'  SELECT ab.car_id, ab.car_intereses_no_pag',
'    INTO ln_car_id, ln_intereses_no_pag',
'    FROM car_abonos_refinanciamiento ab',
'   WHERE ab.cxc_id = :p102_cxc_id',
'     AND ab.car_estado_registro = 0;',
'',
'  IF ln_intereses_no_pag = 0 THEN',
'    SELECT nvl(SUM(di.div_valor_interes), 0) intereses_no_pag',
'      INTO ln_intereses_no_pag',
'      FROM car_dividendos di',
'     WHERE di.cxc_id = :p102_cxc_id',
'       AND di.div_estado_dividendo = ''P''',
'       AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'       AND di.div_estado_registro =',
'           pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'  ',
'    UPDATE car_abonos_refinanciamiento re',
'       SET re.car_intereses_no_pag = ln_intereses_no_pag',
'     WHERE re.car_id = ln_car_id;',
'  ',
'  END IF;',
'',
'  --- obtenemos el numero de dividendos pendientes de pago  ',
'',
'  SELECT COUNT(di.div_id)',
'    INTO ln_plazo_cxc_original',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = lv_div_estado_div_pendi',
'     AND di.div_estado_registro = lv_estado_reg_activo',
'     AND di.div_saldo_cuota > 0',
'   GROUP BY di.cxc_id;',
'',
'  BEGIN',
'    SELECT a.crc_id',
'      INTO ln_crc_id',
'      FROM asdm_e.car_campanias_refinancia_cab a,',
'           asdm_e.car_campanias_refinancia_det b',
'     WHERE a.crc_id = b.crc_id',
'       AND a.crc_estado_campania =',
'           pq_constantes.fn_retorna_constante(0,',
'                                              ''cn_crc_estado_campania_abierto'')',
'       AND b.cxc_id = :p102_cxc_id;',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ln_crc_id := NULL;',
'  END;',
'',
'  IF ln_plazo_cxc_original < to_number(:p102_plazo) OR',
'     ln_crc_id IS NOT NULL THEN',
'  ',
'    SELECT age.age_id',
'      INTO ln_age_id_agencia',
'      FROM asdm_agencias age',
'     WHERE age.uge_id = :f_uge_id;',
'  ',
'    ln_cuota := pq_car_refin_precancelacion.fn_valor_cuota(tasa => :p102_tasa,',
'                                                           nper => :p102_plazo,',
'                                                           va   => :p102_total_financiar);',
'  ',
'    IF :f_seg_id =',
'       pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'') THEN',
'    ',
'      ln_cuota_minima := to_number(pq_car_general.fn_obtener_parametro(pn_par_id => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                       ''cn_par_id_cuota_minima_minoreo''),',
'                                                                       pn_ede_id => NULL,',
'                                                                       pn_neg_id => NULL,',
'                                                                       pn_emp_id => :f_emp_id));',
'    ',
'      IF ln_cuota < ln_cuota_minima THEN',
'      ',
'        :p102_alerta_2 := ''La Valor de la Cuota NO debe ser menor a '' ||',
'                          ln_cuota_minima || '' dolares'';',
'      ',
'      ELSE',
'      ',
'        :p102_alerta_2 := '' '';',
'      ',
'      END IF;',
'    ',
'    ELSE',
'    ',
'      :p102_alerta_2 := '' '';',
'    ',
'    END IF;',
'',
'    pq_car_refin_precancelacion.pr_cronograma_refin(pn_emp_id       => :f_emp_id,',
'                                                    pn_total_ref    => :p102_total_financiar,',
'                                                    pn_plazo        => :p102_plazo,',
'                                                    pn_tasa         => :p102_tasa,',
'                                                    pn_age_id       => ln_age_id_agencia,',
'                                                    pn_cli_id       => :p102_cli_id,',
'                                                    pn_tse_id       => :f_seg_id,',
'                                                    pn_meses_gracia => :p102_meses_gracia,',
'                                                    pv_error        => :p0_error);',
'  ',
'    ----- cargo coleccion de intereses para la nueva nota de CREDITO',
'  ',
'    SELECT com_id',
'      INTO ln_com_id',
'      FROM car_cuentas_por_cobrar x',
'     WHERE x.cxc_id = :p102_cxc_id;',
'  ',
'    SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'      INTO lv_pue_sri, lv_uge_sri',
'      FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'     WHERE pu.uge_id = ug.uge_id',
'       AND pu.pue_id = :p102_pue_id;',
'  ',
'    SELECT SUM(to_number(c004)) valor_interes',
'      INTO ln_int_nd_refi',
'      FROM apex_collections',
'     WHERE collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'  ',
'    pq_car_refin_precancelacion.pr_carga_coleccion_nd_ref(pn_cli_id        => :p102_cli_id,',
'                                                          pn_com_id        => ln_com_id,',
'                                                          pn_emp_id        => :f_emp_id,',
'                                                          pn_cxc_id        => :p102_cxc_id,',
'                                                          pn_valor_interes => ln_int_nd_refi,',
'                                                          pn_pue_sri       => lv_pue_sri,',
'                                                          pn_uge_sri       => lv_uge_sri,',
'                                                          pv_error         => :p0_error);',
'  ',
'  ELSE',
'  ',
'    :p102_alerta_2 := ''El nuevo plazo de refinanciamiento debe ser MAYOR al # de Dividendos Pendientes de Pago'';',
'      RAISE_APPLICATION_ERROR(-20000,''El nuevo plazo de refinanciamiento: ''||:p102_plazo||'' debe ser MAYOR al # de Dividendos Pendientes de Pago:''||ln_plazo_cxc_original);',
'  END IF;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error'
,p_process_when_button_id=>wwv_flow_imp.id(42563059819987679)
,p_internal_uid=>10315325257558682
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(239918641176360049)
,p_process_sequence=>21
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_calcula_cronograma_seg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ld_fecha_corte DATE;',
'BEGIN',
'',
'  BEGIN',
'    SELECT to_date(c.c002, ''dd/mm/yyyy'')',
'      INTO ld_fecha_corte',
'      FROM apex_collections c',
'     WHERE c.collection_name = ''COLL_FECHAS_CORTE''',
'       AND rownum = 1;',
'  EXCEPTION',
'    WHEN no_data_found THEN',
'      ld_fecha_corte := NULL;',
'  END;',
'  IF ld_fecha_corte IS NOT NULL THEN',
'    pq_car_refinanciamiento.pr_calcula_nuevo_cronog_seg(pn_ord_id        => :p102_ord_id,',
'                                                        pn_cxc_id_seguro => :p102_cxc_id_seguro,',
'                                                        pn_plazo         => :p102_plazo,',
'                                                        pd_fecha_corte   => ld_fecha_corte,',
'                                                        pv_error         => :p0_error);',
'  ',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(42563059819987679)
,p_internal_uid=>207665489906595123
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42873965309895028)
,p_process_sequence=>10
,p_process_point=>'BEFORE_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_genera_refinan'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_cuota             NUMBER;',
'  ln_valor_nota_credito NUMBER;',
'  ln_age_id_agente     NUMBER;',
'  ln_valor_debito number;',
'ln_valor_credito number;',
'LN_VALOR_CAPITAL_CRO NUMBER;',
'LN_CAPITAL_ABONO NUMBER;',
'BEGIN',
'',
'',
'select ',
'SUM(to_number(c003)) Valor_Capital',
'INTO LN_VALOR_CAPITAL_CRO',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'SELECT (R.CAR_CAPITAL - R.CAR_DIFERENCIA) ',
'INTO LN_CAPITAL_ABONO ',
'FROM car_abonos_refinanciamiento r WHERE r.cxc_id=:p102_cxc_id AND R.CAR_ESTADO_REGISTRO=0;',
'',
'IF LN_VALOR_CAPITAL_CRO !=LN_CAPITAL_ABONO THEN',
':p0_error:=''HAY UNA DIFERENCIA ENTRE EL VALOR CALCULADO DEL CRONOGRAMA: ''|| LN_VALOR_CAPITAL_CRO ||'' Y EL VALOR A REFINANCIAR :''|| LN_CAPITAL_ABONO;',
'RAISE_APPLICATION_ERROR(-20000,:p0_error);',
'END IF;',
'',
'---debito',
'SELECT (a.car_capital-a.car_diferencia )+nvl(car_intereses_no_pag,0)',
'into ln_valor_debito ',
'FROM asdm_e.car_abonos_refinanciamiento a ',
'WHERE a.cxc_id =:p102_cxc_id',
'AND a.car_estado_registro=0;',
'',
'---credito',
'SELECT  (a.car_saldo_cxc_ant - a.car_abono) ',
'into ln_valor_credito ',
'FROM asdm_e.car_abonos_refinanciamiento a ',
'WHERE a.cxc_id =:p102_cxc_id',
'AND a.car_estado_registro=0;',
'	',
'  ',
'if ln_valor_debito=ln_valor_credito  then',
'',
'  ln_cuota := round(pq_car_refin_precancelacion.fn_valor_cuota(tasa => :p102_tasa,',
'                                                         nper => :p102_plazo,',
'                                                         va   => :p102_total_financiar), 4);',
'',
' --- ln_valor_nota_debito := :p102_total_credito_ref - :p102_total_financiar;',
'SELECT c010 ',
'    INTO   ln_valor_nota_credito',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'');',
'',
'  ln_age_id_agente := pq_car_credito_interfaz.fn_retorna_age_conectado(:f_user_id,',
'                                                                       :f_emp_id,',
'                                                                       ''cn_tag_id_vendedor'');',
'',
'',
' --  RAISE_APPLICATION_ERROR(-20000,  ln_valor_nota_debito );',
'',
'  pq_car_refin_precancelacion.pr_genera_refianciamien(pn_emp_id            => :f_emp_id,',
'                                                      pn_uge_id            => :f_uge_id,',
'                                                      pn_usu_id            => :f_user_id,',
'                                                      pn_cli_id            => :p102_cli_id,',
'                                                      pn_tse_id            => :f_seg_id,',
'                                                      pn_uge_id_gasto      => :f_uge_id_gasto,',
'                                                      pn_clave             => :p102_clave,',
'                                                      pn_valor_cuota       => ln_cuota,',
'                                                      pn_cli_id_ga         => :p102_garante,',
'                                                      pn_age_id_agencia    => :f_age_id_agencia,',
'                                                      pn_age_id_agentes    => ln_age_id_agente,',
'                                                      PN_VALOR_NOTA_DEBITO => ln_valor_nota_credito,',
'                                                      pv_pue_num_sri => :P102_PUE_NUM_SRI,',
'                                                      pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'                                                      pn_pue_id => :P102_PUE_ID,',
'                                                      pv_error             => :p0_error);',
'',
'',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COL_REFIN'');',
'apex_collection.create_or_truncate_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nd_refinanciamiento''));',
'',
':P102_TOTAL_CREDITO_REF := null;',
'',
'',
'else ',
'',
':p0_error:=''Los valores no coinciden debito:''|| ln_valor_debito ||'' credito: ''|| ln_valor_credito;',
'RAISE_APPLICATION_ERROR(-20000,:p0_error);',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'Datos Procesados'
,p_internal_uid=>10620814040130102
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(199858200807464075)
,p_process_sequence=>10
,p_process_point=>'BEFORE_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_REFINANCIAMIENTO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_car_refinanciamiento.pr_refinanciar_seguro(pn_emp_id           => :f_emp_id,',
'                                                pn_ord_id           => :p102_ord_id,',
'                                                pn_cxc_id_seguro      => :P102_CXC_ID_SEGURO,',
'                                                pn_usu_id           => :f_user_id,',
'                                                pn_uge_id           => :f_uge_id,',
'                                                pd_fecha_corte      => :p102_fecha_corte,',
'                                                pn_cxc_id_principal => :p102_cxc_id,',
'                                                pv_error            => :p0_error);',
'  IF apex_collection.collection_exists(''COL_COBERTURA'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_COBERTURA'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_COBERTURA_SEG'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_COBERTURA_SEG'');',
'  ',
'  END IF;',
'',
'  IF :p0_error IS NULL THEN',
'  ',
'    pq_car_refin_precancelacion.pr_graba_refinanciamiento(pn_cxc_id            => :p102_cxc_id,',
'                                                          pn_tasa              => :p102_tasa,',
'                                                          pn_plazo             => :p102_plazo,',
'                                                          pn_total_financiar   => :p102_total_financiar,',
'                                                          pn_user_id           => :f_user_id,',
'                                                          pn_emp_id            => :f_emp_id,',
'                                                          pn_uge_id            => :f_uge_id,',
'                                                          pn_cli_id            => :p102_cli_id,',
'                                                          pn_seg_id            => :f_seg_id,',
'                                                          pn_uge_id_gasto      => :f_uge_id_gasto,',
'                                                          pn_clave             => :p102_clave,',
'                                                          pn_garante           => :p102_garante,',
'                                                          pn_age_id_agencia    => :f_age_id_agencia,',
'                                                          pv_pue_num_sri       => :p102_pue_num_sri,',
'                                                          pv_uge_num_est_sri   => :p0_uge_num_est_sri,',
'                                                          pn_pue_id            => :p102_pue_id,',
'                                                          pn_total_credito_ref => :p102_total_credito_ref,',
'                                                          pv_error             => :p0_error);',
'  ',
'  END IF;',
'',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'graba'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>167605049537699149
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'                                                                                      ',
' /*',
' pq_car_refin_precancelacion.pr_graba_refinanciamiento(pn_cxc_id => :p102_cxc_id,',
'                                                        pn_tasa => :p102_tasa,',
'                                                        pn_plazo => :p102_plazo,',
'                                                        pn_total_financiar => :p102_total_financiar,',
'                                                        pn_user_id => :f_user_id,',
'                                                        pn_emp_id => :f_emp_id,',
'                                                        pn_uge_id => :f_uge_id,',
'                                                        pn_cli_id => :p102_cli_id,',
'                                                        pn_seg_id => :f_seg_id,',
'                                                        pn_uge_id_gasto => :f_uge_id_gasto,',
'                                                        pn_clave => :p102_clave,',
'                                                        pn_garante => :p102_garante,',
'                                                        pn_age_id_agencia => :f_age_id_agencia,',
'                                                        pv_pue_num_sri => :P102_PUE_NUM_SRI,',
'                                                        pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'                                                        pn_pue_id => :P102_PUE_ID,',
'                                                        pn_total_credito_ref => :P102_TOTAL_CREDITO_REF,',
'                                                        pv_error => :p0_error);*/'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(36869359585665379)
,p_process_sequence=>11
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
' ',
'BEGIN',
'pq_car_refin_precancelacion.pr_cargar_colecciones(pn_cxc_id => :P102_CXC_ID,',
'                                                    pn_user_id => :F_USER_ID,',
'                                                    pn_emp_id => :F_emp_id,',
'                                                    pn_uge_id => :F_uge_id,',
'                                                    pn_cli_id => :P102_CLI_ID,',
'                                                    pn_seg_id => :F_SEG_ID,',
'                                                    pv_pue_num_sri => :P102_PUE_NUM_SRI,',
'                                                    pn_pue_id => :P102_PUE_ID,',
'                                                    pv_alerta_abono => :P102_ALERTA_ABONO,',
'                                                    pv_ede_id_cartera => :P102_EDE_ID_CARTERA,',
'                                                    pv_valida_punto => :P102_VALIDA_PUNTO,',
'                                                    pn_plazo_aplica => :P102_PLAZO_APLICA,',
'                                                    pv_error => :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>4616208315900453
,p_process_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'',
'BEGIN',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p102_cxc_id;',
'',
'  ----- obtengo los datos del periodo de caja',
'',
'  pq_ven_movimientos_caja.pr_abrir_caja(pn_usu_id => :f_user_id,',
'                                        pn_uge_id => :f_uge_id,',
'                                        pn_emp_id => :f_emp_id,',
'                                        pn_pue_id => ln_pue_id,',
'                                        pn_pca_id => ln_pca_id,',
'                                        pv_error  => :p0_error);',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = ln_pue_id;',
'',
'  SELECT SUM(di.div_valor_interes) intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'',
'  ----- cargo coleccion de intereses diferidos',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nd_ref(pn_cli_id        => :p102_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p102_cxc_id,',
'                                                        pn_valor_interes => ln_intereses_nc,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'  pq_car_refin_precancelacion.pr_carga_coll_cred_refin(pn_emp_id => :f_emp_id,',
'                                                       pn_cli_id => :p102_cli_id,',
'                                                       pn_cxc_id => :p102_cxc_id,',
'                                                       pn_tse_id => :f_seg_id,',
'                                                       pv_error  => :p0_error);',
'',
'  ln_valida_pago_caja := pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p102_cxc_id);',
'',
'  IF ln_valida_pago_caja > 0 THEN',
'  ',
'    :p102_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_valida_pago_caja;',
'  ',
'  ELSE',
'  ',
'    :p102_alerta_abono := '' '';',
'  ',
'  END IF;',
'',
'END;'))
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(36961771920709624)
,p_process_sequence=>11
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_credito'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_cuenta NUMBER;',
'BEGIN',
'',
'  BEGIN',
'    DELETE FROM asdm_e.car_cartera_refinanciamiento',
'     WHERE cxc_id = :p102_cxc_id;',
'    DELETE FROM asdm_e.car_abonos_refinanciamiento',
'     WHERE cxc_id = :p102_cxc_id;',
'    :P102_ORD_ID:= null;',
'  END;',
'  --- raise_application_error(-20000,'':P102_CXC_ID ''||:P102_CXC_ID );',
'  SELECT COUNT(*)',
'    INTO ln_cuenta',
'    FROM apex_collections',
'   WHERE collection_name = ''COL_CREDITOS_REFIN''',
'     AND seq_id = :p102_seq_id;',
'',
'  IF ln_cuenta > 0 THEN',
'    apex_collection.delete_member(p_collection_name => ''COL_CREDITOS_REFIN'',',
'                                  p_seq             => :p102_seq_id);',
'  END IF;',
'',
':P102_FECHA_CORTE:=NULL;',
'  IF apex_collection.collection_exists(''COLL_FECHAS_CORTE'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_FECHAS_CORTE'');',
'  ',
'  END IF;',
'',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(''COL_COBERTURA'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_COBERTURA'');',
'  ',
'  END IF;',
'  ',
'    IF apex_collection.collection_exists(''COL_COBERTURA_SEG'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_COBERTURA_SEG'');',
'  ',
'  END IF;',
'',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'  IF apex_collection.collection_exists(''COL_ND_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_ND_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists(pq_constantes.fn_retorna_constante(NULL,',
'                                                                          ''cv_col_gastos_int_mora'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                              ''cv_col_gastos_int_mora''));',
'  ',
'  END IF;',
'  :p102_alerta_2 := '' '';',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>4708620650944698
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(48622962958281237)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_pruba_impresion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE ',
'',
'e VARCHAR2(3000);',
'',
'begin',
'  -- Call the procedure',
'  pq_car_refin_precancelacion.pr_imprimir(pn_emp_id => 1,',
'                                          pn_com_id => 881752,',
'                                          pn_ttr_id => 386,',
'                                          pn_age_id => 15,',
'                                          pn_fac_desde => NULL,',
'                                          pn_fac_hasta => NULL,',
'                                          pv_error => e);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>16369811688516311
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(57800372933077553)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion_id'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_adeuda NUMBER;',
'',
'  ln_valida_pago_caja NUMBER;',
'  ln_com_id           NUMBER;',
'  ln_pue_id           NUMBER;',
'  ln_pca_id           NUMBER;',
'',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'  ln_intereses_nc NUMBER;',
'ln_int_nc_refi number;',
'BEGIN',
'',
'IF :P0_ERROR IS NULL THEN',
'',
'  SELECT com_id',
'    INTO ln_com_id',
'    FROM car_cuentas_por_cobrar x',
'   WHERE x.cxc_id = :p102_cxc_id;',
'',
'  ----- obtengo los datos del periodo de caja',
'',
'  pq_ven_movimientos_caja.pr_abrir_caja(pn_usu_id => :f_user_id,',
'                                        pn_uge_id => :f_uge_id,',
'                                        pn_emp_id => :f_emp_id,',
'                                        pn_pue_id => ln_pue_id,',
'                                        pn_pca_id => ln_pca_id,',
'                                        pv_error  => :p0_error);',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = ln_pue_id;',
'',
'  /*SELECT SUM(di.div_valor_interes) intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');*/',
'---jalvarez 26-may-2016 se cambia la consulta de los intereses cuando hay saldos adicionales',
'',
'/*SELECT  CASE',
'               WHEN (SUM(di.div_valor_cuota) - sum(di.div_saldo_cuota)) >=',
'                    sum(di.div_valor_interes) THEN',
'                0',
'               ELSE',
'                sum(di.div_valor_interes) -',
'                (sum(di.div_valor_cuota) - sum(di.div_saldo_cuota))',
'             END intereses_nc',
'    INTO ln_intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');*/',
'----JALVAREZ 11-DIC-2017 SE MODIFICA PARA QUE EL CALCULO SEA EXACTO',
'SELECT SUM(intereses_nc) INTO ln_intereses_nc FROM ',
'(SELECT  CASE',
'               WHEN ((di.div_valor_cuota) - (di.div_saldo_cuota)) >=',
'                    (di.div_valor_interes) THEN',
'                0',
'               ELSE',
'                (di.div_valor_interes) -',
'                ((di.div_valor_cuota) - (di.div_saldo_cuota))',
'             END intereses_nc',
'    FROM car_dividendos di',
'   WHERE di.cxc_id = :p102_cxc_id',
'     AND di.div_estado_dividendo = ''P''',
'     AND trunc(di.div_fecha_vencimiento, ''mm'') > trunc(SYSDATE, ''mm'')',
'     AND di.div_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo''));',
'         ',
'  ----- cargo coleccion de intereses diferidos',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nc_ref(pn_cli_id        => :p102_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p102_cxc_id,',
'                                                        pn_valor_interes => nvl(ln_intereses_nc,0),',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
unistr(' ----- cargo coleccion de intereses para la nota de d\00E9bito'),
'select ',
'SUM(to_number(c004)) Valor_Interes',
'into ln_int_nc_refi',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'  pq_car_refin_precancelacion.PR_CARGA_COLECCION_ND_REF(pn_cli_id        => :p102_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :p102_cxc_id,',
'                                                        pn_valor_interes => ln_int_nc_refi,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'',
'END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>25547221663312627
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63126863897632292)
,p_process_sequence=>21
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINA_COLECCION_DIVIDENDOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  ',
'APEX_COLLECTION.create_or_truncate_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'carga'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>30873712627867366
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(56698166076935941)
,p_process_sequence=>31
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_intereses NUMBER;',
'  ln_abono     NUMBER;',
'  ln_diferencia NUMBER;',
'',
'BEGIN',
'',
'  SELECT ab.car_abono, ab.car_intereses',
'    INTO ln_abono, ln_intereses',
'    FROM car_abonos_refinanciamiento ab',
'   WHERE ab.cxc_id = :p102_cxc_id',
'     AND ab.car_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'         ',
'  IF NVL(ln_abono,0) >= ln_intereses THEN',
'    ',
'    :P102_ALERTA_ABONO := NULL;',
'    ',
'  ELSE',
'    ',
'  ln_diferencia := ln_intereses - ln_abono;',
'    if :P102_EDE_ID_CARTERA is null then',
'',
' :p102_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_intereses;    ',
'  ',
'',
'    ELSif :P102_EDE_ID_CARTERA =''MAR'' then',
'',
' :p102_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_intereses;    ',
'  ',
'ELSE',
' :p102_alerta_abono := ''Para realizar el refinanciamiento debe hacer un cambio de cartera y cancelar: '' ||',
'                          ln_intereses;    ',
'end if;',
'  END IF;',
' ',
'   ',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>24445014807171015
);
wwv_flow_imp.component_end;
end;
/
