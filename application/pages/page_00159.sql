prompt --application/pages/page_00159
begin
--   Manifest
--     PAGE: 00159
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>159
,p_name=>unistr('Pagos de Tarjeta Procesados-Facturaci\00F3n')
,p_page_mode=>'MODAL'
,p_step_title=>unistr('Pagos de Tarjeta Procesados-Facturaci\00F3n')
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(28584123219306198548)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(241882206599031903)
,p_name=>'Pago Procesado'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ca.rpc_id,',
'       rs.rpr_id,',
'       pe.per_nro_identificacion || '' - '' || pe.per_primer_nombre || '' '' ||',
'       pe.per_segundo_nombre || '' '' || pe.per_primer_apellido || '' '' ||',
'       pe.per_segundo_apellido cliente,',
'       ca.rpc_fecha,',
'       rs.rpr_num_tarj_trun,',
'       r.rpr_id_valor_total total_pago,',
'       rs.rpr_respuesta,',
'       ''PAGAR'' utilizar',
'  FROM car_redes_pago_cab  ca,',
'       asdm_clientes       cl,',
'       asdm_personas       pe,',
'       car_redes_pago_req  r,',
'       car_redes_pago_resp rs',
' WHERE ca.rpc_id = rs.rpc_id',
'   AND ca.cli_id = cl.cli_id',
'   AND cl.per_id = pe.per_id',
'   AND ca.rpc_id = r.rpc_id',
'   AND rs.rpr_id_req = r.rpr_id',
'   AND r.rpr_id_anulacion IS NULL',
'   AND ca.rpc_estado NOT IN  (''CERRADO'',''ANULADO'')',
'   AND r.rpr_estado_pago IN (''PROCESADO'')',
'   AND rs.rpr_estado_resp IN (''PROCESADO'')',
'   AND ca.cli_id = :p159_cli_id',
'   AND ca.uge_id = :p159_uge_id',
'   AND rs.rpr_tipo_respuesta = ''PP''',
'   AND r.rpr_tipo_requerimiento = ''PP'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138505253086796121)
,p_query_column_id=>1
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138505740629796121)
,p_query_column_id=>2
,p_column_alias=>'RPR_ID'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138502888605796113)
,p_query_column_id=>3
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>1
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138503279430796115)
,p_query_column_id=>4
,p_column_alias=>'RPC_FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138503678103796117)
,p_query_column_id=>5
,p_column_alias=>'RPR_NUM_TARJ_TRUN'
,p_column_display_sequence=>3
,p_column_heading=>'Num Tarjeta'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138504079949796117)
,p_query_column_id=>6
,p_column_alias=>'TOTAL_PAGO'
,p_column_display_sequence=>4
,p_column_heading=>'Total Pago'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138504518910796118)
,p_query_column_id=>7
,p_column_alias=>'RPR_RESPUESTA'
,p_column_display_sequence=>5
,p_column_heading=>'Autorizado'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(138504930905796121)
,p_query_column_id=>8
,p_column_alias=>'UTILIZAR'
,p_column_display_sequence=>6
,p_column_heading=>'Utilizar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:UTILIZAR:&DEBUG.:RP:P30_RPC_ID,P30_RPR_ID:#RPC_ID#,#RPR_ID#'
,p_column_linktext=>'#UTILIZAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(138506063403796124)
,p_name=>'P159_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(241882206599031903)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(138506489452796132)
,p_name=>'P159_UGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(241882206599031903)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
