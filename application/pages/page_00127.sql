prompt --application/pages/page_00127
begin
--   Manifest
--     PAGE: 00127
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>127
,p_tab_set=>'Menu'
,p_name=>'FR_DETALLE_LECTURAS_SERIES'
,p_step_title=>'Detalle de Lecturas de Series'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(65586072434286085)
,p_name=>'Detalle de Lecturas'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'style="font-family:Tahoma"'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    a.scl_id, a.sdl_nro_serie, a.sdl_cod_articulo, a.ite_sku_id, a.sdl_estado_registro, m.ite_descripcion_larga, m.ite_sec_id,',
'            b.scl_nro_dcto, b.scl_tipo_doc, b.scl_bodega_despacho, b.scl_estado_lista, b.scl_cant_solicitada, b.scl_fecha_registro, b.scl_usuario, b.scl_origen, b.scl_estado_registro, b.scl_cliente_destino,',
'            c.ode_id, c.ode_sec_id, c.ode_fecha, ',
'            d.com_id, d.com_fecha, d.uge_num_sri||''-''||d.pue_num_sri||''-''||d.com_numero factura, d.com_tipo,',
'            f.age_nombre_comercial, f.tse_id,',
'            h.per_primer_nombre||'' ''||h.per_primer_apellido||'' ''||h.per_segundo_apellido vendedor,',
'            e.com_id com_id_nc, e.com_fecha com_fecha_nc, e.com_tipo com_tipo_nc, e.uge_num_sri||''-''||e.pue_num_sri||''-''||e.com_numero nota_credito,',
'            i.ord_id, i.ord_sec_id, i.ord_fecha,',
'            j.teo_id, j.teo_descripcion',
'',
'  FROM      slm_detalle_lista a,',
'            slm_cabecera_lista b,',
'            inv_ordenes_despacho c,',
'            ven_comprobantes d,',
'            ven_comprobantes e,',
'            asdm_agencias f,',
'            asdm_agentes g,',
'            asdm_personas h,',
'            ven_ordenes i,',
'            ven_tipos_est_ordenes j,',
'            asdm_clientes k,',
'            asdm_personas l,',
'            inv_items m',
'  WHERE     sdl_nro_serie = :P127_NRO_SERIE',
'  AND       b.scl_id = a.scl_id',
'  AND       c.ode_id = b.scl_ode_id',
'  AND       d.com_id(+) = c.com_id',
'  AND       e.com_id_ref(+) = d.com_id',
'  AND       f.age_id(+) = d.age_id_agencia',
'  AND       g.age_id(+) = d.age_id_agente',
'  AND       h.per_id(+) = g.per_id',
'  AND       i.ord_id(+) = c.ord_id',
'  AND       j.teo_id(+) = i.teo_id',
'  AND       k.cli_id(+) = i.cli_id_destino',
'  AND       l.per_id(+) = k.per_id',
'  AND       m.ite_sku_id(+) = a.ite_sku_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>2
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586362661286323)
,p_query_column_id=>1
,p_column_alias=>'SCL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Lista'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586462412286327)
,p_query_column_id=>2
,p_column_alias=>'SDL_NRO_SERIE'
,p_column_display_sequence=>2
,p_column_heading=>'Nro.Serie'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586558008286327)
,p_query_column_id=>3
,p_column_alias=>'SDL_COD_ARTICULO'
,p_column_display_sequence=>3
,p_column_heading=>'C.Barras'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586680897286327)
,p_query_column_id=>4
,p_column_alias=>'ITE_SKU_ID'
,p_column_display_sequence=>4
,p_column_heading=>'ITE_SKU_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586753262286328)
,p_query_column_id=>5
,p_column_alias=>'SDL_ESTADO_REGISTRO'
,p_column_display_sequence=>5
,p_column_heading=>'Estado Lectura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586881282286328)
,p_query_column_id=>6
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>6
,p_column_heading=>'Item'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65586980684286328)
,p_query_column_id=>7
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>7
,p_column_heading=>'C.Item'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587070018286328)
,p_query_column_id=>8
,p_column_alias=>'SCL_NRO_DCTO'
,p_column_display_sequence=>8
,p_column_heading=>'SCL_NRO_DCTO'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587174285286328)
,p_query_column_id=>9
,p_column_alias=>'SCL_TIPO_DOC'
,p_column_display_sequence=>9
,p_column_heading=>'SCL_TIPO_DOC'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587260262286328)
,p_query_column_id=>10
,p_column_alias=>'SCL_BODEGA_DESPACHO'
,p_column_display_sequence=>10
,p_column_heading=>'Bodega'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587375316286328)
,p_query_column_id=>11
,p_column_alias=>'SCL_ESTADO_LISTA'
,p_column_display_sequence=>11
,p_column_heading=>'Estado Lista'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587454852286328)
,p_query_column_id=>12
,p_column_alias=>'SCL_CANT_SOLICITADA'
,p_column_display_sequence=>12
,p_column_heading=>'Cantidad Solicitada'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587580259286328)
,p_query_column_id=>13
,p_column_alias=>'SCL_FECHA_REGISTRO'
,p_column_display_sequence=>13
,p_column_heading=>'Fecha Lista'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587674257286328)
,p_query_column_id=>14
,p_column_alias=>'SCL_USUARIO'
,p_column_display_sequence=>14
,p_column_heading=>'Usuario'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587754312286328)
,p_query_column_id=>15
,p_column_alias=>'SCL_ORIGEN'
,p_column_display_sequence=>15
,p_column_heading=>'Origen'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587867044286329)
,p_query_column_id=>16
,p_column_alias=>'SCL_ESTADO_REGISTRO'
,p_column_display_sequence=>16
,p_column_heading=>'Estado R.Lista'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65587972282286329)
,p_query_column_id=>17
,p_column_alias=>'SCL_CLIENTE_DESTINO'
,p_column_display_sequence=>17
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588080347286329)
,p_query_column_id=>18
,p_column_alias=>'ODE_ID'
,p_column_display_sequence=>18
,p_column_heading=>'ODE_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588169124286330)
,p_query_column_id=>19
,p_column_alias=>'ODE_SEC_ID'
,p_column_display_sequence=>19
,p_column_heading=>'O.Despacho'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588256952286330)
,p_query_column_id=>20
,p_column_alias=>'ODE_FECHA'
,p_column_display_sequence=>20
,p_column_heading=>'Fecha Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588357643286330)
,p_query_column_id=>21
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>21
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588482075286330)
,p_query_column_id=>22
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>22
,p_column_heading=>'Fecha Factura'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588555601286330)
,p_query_column_id=>23
,p_column_alias=>'FACTURA'
,p_column_display_sequence=>23
,p_column_heading=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588679973286330)
,p_query_column_id=>24
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>24
,p_column_heading=>'COM_TIPO'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588764297286330)
,p_query_column_id=>25
,p_column_alias=>'AGE_NOMBRE_COMERCIAL'
,p_column_display_sequence=>25
,p_column_heading=>'Agencia'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588854944286330)
,p_query_column_id=>26
,p_column_alias=>'TSE_ID'
,p_column_display_sequence=>26
,p_column_heading=>'Segmento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65588965457286330)
,p_query_column_id=>27
,p_column_alias=>'VENDEDOR'
,p_column_display_sequence=>27
,p_column_heading=>'Vendedor'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589073604286330)
,p_query_column_id=>28
,p_column_alias=>'COM_ID_NC'
,p_column_display_sequence=>28
,p_column_heading=>'COM_ID_NC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589157617286330)
,p_query_column_id=>29
,p_column_alias=>'COM_FECHA_NC'
,p_column_display_sequence=>29
,p_column_heading=>'Fecha NC.'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589266288286334)
,p_query_column_id=>30
,p_column_alias=>'COM_TIPO_NC'
,p_column_display_sequence=>30
,p_column_heading=>'COM_TIPO_NC'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589351343286334)
,p_query_column_id=>31
,p_column_alias=>'NOTA_CREDITO'
,p_column_display_sequence=>31
,p_column_heading=>'NOTA CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589459765286334)
,p_query_column_id=>32
,p_column_alias=>'ORD_ID'
,p_column_display_sequence=>32
,p_column_heading=>'ORD_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589581974286334)
,p_query_column_id=>33
,p_column_alias=>'ORD_SEC_ID'
,p_column_display_sequence=>33
,p_column_heading=>'O.Venta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589656000286334)
,p_query_column_id=>34
,p_column_alias=>'ORD_FECHA'
,p_column_display_sequence=>34
,p_column_heading=>'Fecha O.Venta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589760550286334)
,p_query_column_id=>35
,p_column_alias=>'TEO_ID'
,p_column_display_sequence=>35
,p_column_heading=>'TEO_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65589883266286334)
,p_query_column_id=>36
,p_column_alias=>'TEO_DESCRIPCION'
,p_column_display_sequence=>36
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(65620570684484289)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(65586072434286085)
,p_button_name=>'BT_REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Regresar'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:123:&SESSION.::&DEBUG.:127::'
,p_button_cattributes=>'style="font-size:16;font-family:Tahoma"'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(65613562710446132)
,p_name=>'P127_NRO_SERIE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(65586072434286085)
,p_prompt=>'Nro Serie'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
