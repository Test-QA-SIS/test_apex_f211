prompt --application/pages/page_00113
begin
--   Manifest
--     PAGE: 00113
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>113
,p_tab_set=>'Menu'
,p_name=>'Reporte Nota Credito Montos'
,p_step_title=>'Reporte_Nota_Credito_Montos'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(41920274059583070)
,p_name=>'Reporte NC Montos'
,p_template=>wwv_flow_imp.id(270523966992046668)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.cli_id,',
'       pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'       pe.per_primer_apellido || '' '' || pe.per_segundo_apellido || '' - '' ||',
'       pe.per_razon_social nombre,',
'       round(SUM(vd.vcd_valor_variable),2) valor,',
'       NULL age, apex_item.checkbox(p_idx            => 1,',
'                          p_value          => null',
'                          ) seleccionar',
'',
'  FROM ven_comprobantes         c,',
'       ven_comprobantes_det     d,',
'       ven_var_comprobantes_det vd,',
'       inv_items_categorias     ic,',
'       asdm_clientes            cl,',
'       asdm_personas            pe',
' WHERE c.com_fecha >= :p113_fecha_inicio',
'   AND c.com_fecha <= :p113_fecha_fin',
'   AND com_tipo = ''F''',
'   AND d.com_id = c.com_id',
'   AND d.ite_sku_id = ic.ite_sku_id',
'   AND d.cde_id = vd.cde_id',
'   AND vd.var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_total_cred_entrada'')',
'   AND c.age_id_agencia = :f_age_id_agencia',
'   AND ic.cat_id IN (SELECT cat_id FROM asdm_e.ven_parametrizacion_cat_nc)',
'   AND cl.cli_id = c.cli_id',
'   AND cl.per_id = pe.per_id',
'   AND c.tse_id =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_mayoreo'')',
' HAVING COUNT(UNIQUE(c.age_id_agencia)) = 1',
'',
' GROUP BY c.cli_id,',
'          pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'          pe.per_primer_apellido || '' '' || pe.per_segundo_apellido || '' - '' ||',
'          pe.per_razon_social',
'UNION ALL',
'SELECT caa.cli_id,',
'       pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'       pe.per_primer_apellido || '' '' || pe.per_segundo_apellido || '' - '' ||',
'       pe.per_razon_social nombre,',
'      round(SUM(vd.vcd_valor_variable),2)  valor,',
'       CASE',
'         WHEN (SELECT age_id_agencia',
'                 FROM (SELECT ca.age_id_agencia,',
'                              SUM(vd.vcd_valor_variable) valor,',
'                              ca.cli_id',
'                         FROM ven_comprobantes         ca,',
'                              ven_comprobantes_det     d,',
'                              ven_var_comprobantes_det vd,',
'                              inv_items_categorias     ic',
'                        WHERE ca.com_fecha >= :p113_fecha_inicio',
'                          AND ca.com_fecha <= :p113_fecha_fin',
'                          AND com_tipo = ''F''',
'                          AND d.com_id = ca.com_id',
'                          AND d.ite_sku_id = ic.ite_sku_id',
'                          AND d.cde_id = vd.cde_id',
'                          AND vd.var_id =',
'                              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                 ''cn_var_id_total_cred_entrada'')',
'                          AND ic.cat_id IN',
'                              (SELECT cat_id',
'                                 FROM asdm_e.ven_parametrizacion_cat_nc)',
'                          AND ca.tse_id =',
'                              pq_constantes.fn_retorna_constante(NULL,',
'                                                                 ''cn_tse_id_mayoreo'')',
'                       --  AND ca.cli_id = caa.cli_id',
'                        GROUP BY ca.age_id_agencia, ca.cli_id',
'                        ORDER BY SUM(vd.vcd_valor_variable) DESC) h',
'                WHERE rownum = 1',
'                  AND h.cli_id = caa.cli_id) = :f_age_id_agencia THEN',
'          ''hola''',
'         ELSE',
'          NULL',
'       END, apex_item.checkbox(p_idx            => 1,',
'                          p_value          => null',
'                          )',
'  FROM ven_comprobantes         caa,',
'       ven_comprobantes_det     d,',
'       ven_var_comprobantes_det vd,',
'       inv_items_categorias     ic,',
'       asdm_clientes            cl,',
'       asdm_personas            pe',
' WHERE caa.com_fecha >= :p113_fecha_inicio',
'   AND caa.com_fecha <= :p113_fecha_fin',
'   AND com_tipo = ''F''',
'   AND d.com_id = caa.com_id',
'   AND d.ite_sku_id = ic.ite_sku_id',
'   AND d.cde_id = vd.cde_id',
'   AND vd.var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_total_cred_entrada'')',
'   AND :f_age_id_agencia IN',
'       (SELECT age_id_agencia',
'          FROM ven_comprobantes ce',
'         WHERE ce.cli_id = caa.cli_id',
'           AND ce.com_fecha >= :p113_fecha_inicio',
'           AND ce.com_fecha <= :p113_fecha_fin',
'           AND ce.tse_id =',
'               pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_mayoreo''))',
'   AND ic.cat_id IN (SELECT cat_id FROM asdm_e.ven_parametrizacion_cat_nc)',
'   AND cl.cli_id = caa.cli_id',
'   AND pe.per_id = cl.cli_id',
'   AND caa.tse_id =',
'       pq_constantes.fn_retorna_constante(NULL, ''cn_tse_id_mayoreo'')',
' HAVING COUNT(UNIQUE(caa.age_id_agencia)) > 1',
'-- AND caa.cli_id = 339419',
' GROUP BY caa.cli_id,',
'          pe.per_primer_nombre || '' '' || pe.per_segundo_nombre || '' '' ||',
'          pe.per_primer_apellido || '' '' || pe.per_segundo_apellido || '' - '' ||',
'          pe.per_razon_social'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>60
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Download'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41921060233583086)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Codigo'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'19'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41958764896069937)
,p_query_column_id=>2
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_column_width=>500
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'19'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41935258903221763)
,p_query_column_id=>3
,p_column_alias=>'VALOR'
,p_column_display_sequence=>3
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'19'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41935354026221763)
,p_query_column_id=>4
,p_column_alias=>'AGE'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'19'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42223269333244225)
,p_query_column_id=>5
,p_column_alias=>'SELECCIONAR'
,p_column_display_sequence=>5
,p_column_heading=>'Seleccionar'
,p_use_as_row_header=>'N'
,p_display_as=>'SIMPLE_CHECKBOX'
,p_include_in_export=>'Y'
,p_print_col_width=>'19'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(41962661091137681)
,p_plug_name=>'Seleccionar Fecha'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>5
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(42234277676257605)
,p_plug_name=>'Notas de Credito Pendientes Generar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Select * from (',
'SELECT co.c001 CLI_ID,',
'       co.c002 NOMBRE,',
'       to_number(co.c003, ''999999.9999'', ''nls_numeric_characters=''''.,'''''') VALOR,',
'       co.c004 NRO_DET,',
'       co.c005 FACTURA_MAS_ALTA,',
'       co.c006 AGE,',
'       co.c007 INICIO,',
'       co.c008 FIN,',
'       co.c009 GENERAR_NC,',
'       co.c010 CONSULTA',
'       FROM apex_collections co',
' WHERE co.collection_name = ''COLL_PARA_NC_MONTOS''',
') order by VALOR DESC'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_prn_output_show_link=>'Y'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(42234373203257605)
,p_name=>'Notas de Credito Pendientes Generar'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MCAGUANA'
,p_internal_uid=>9981221933492679
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42234660628257626)
,p_db_column_name=>'NOMBRE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42234856412257627)
,p_db_column_name=>'AGE'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Age'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45575574460649020)
,p_db_column_name=>'GENERAR_NC'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Generar Nc'
,p_column_link=>'f?p=&APP_ID.:108:&SESSION.::&DEBUG.:108:P108_CLIENTE,P108_FECHA_INICIO,P108_FECHA_FIN,P108_ACCION,P108_COM_ID_NOTA:#CLI_ID#,#INICIO#,#FIN#,,'
,p_column_linktext=>'Generar_nc'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'GENERAR_NC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45591782752055429)
,p_db_column_name=>'INICIO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Inicio'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_tz_dependent=>'N'
,p_static_id=>'INICIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45591855665055431)
,p_db_column_name=>'FIN'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Fin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_tz_dependent=>'N'
,p_static_id=>'FIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(499587362796764780)
,p_db_column_name=>'CONSULTA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Consulta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CONSULTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(499654483480043889)
,p_db_column_name=>'CLI_ID'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(499654662213043895)
,p_db_column_name=>'NRO_DET'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Nro Det'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_DET'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(499654763456043895)
,p_db_column_name=>'FACTURA_MAS_ALTA'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Factura Mas Alta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA_MAS_ALTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(19355689459914977429)
,p_db_column_name=>'VALOR'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(42235772771258618)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'99827'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'CLI_ID:NOMBRE:VALOR:GENERAR_NC:CONSULTA:NRO_DET:FACTURA_MAS_ALTA:'
,p_sort_column_1=>'VALOR'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42030061174836494)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(41962661091137681)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42366856223402770)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(42234277676257605)
,p_button_name=>'GENERAR_NC'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Generar Nc'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41961157958118716)
,p_name=>'P113_MES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(41962661091137681)
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:ENE;1,FEB;2,MAR;3,ABR;4,MAY;5,JUN;6,JUL;7,AGO;8,SEP;9,OCT;10,NOV;11,DIC;12'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(41961771101127825)
,p_name=>'P113_ANO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(41962661091137681)
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:2014;2014,2015;2015,2016;2016,2017;2017'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42067379981874612)
,p_name=>'P113_FECHA_INICIO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(41962661091137681)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42068977177875953)
,p_name=>'P113_FECHA_FIN'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(41962661091137681)
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P113_FECHA_FIN IS NOT NULL'
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45589764482987883)
,p_name=>'P113_VALOR_MINIMO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(41962661091137681)
,p_prompt=>'Valor Minimo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MIN(d.mde_monto_inicial)',
'  FROM asdm_e.ven_montos_descuentos d'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42417580960570759)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'GENERAR NC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  pq_ven_ncredito_rebaja.pr_genera_nc_montos(pd_fecha_inicio => :P113_FECHA_INICIO,',
'                                             pd_fecha_fin => :P113_FECHA_FIN,',
'                                             pv_error => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(42366856223402770)
,p_internal_uid=>10164429690805833
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42051572172858606)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_parametros_iniciales'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_dia_inicio   NUMBER;',
'  ln_dia_fin      NUMBER;',
'  ld_fecha_inicio DATE;',
'  ld_fecha_fin    DATE;',
'BEGIN',
'--RAISE_APPLICATION_ERROR(-20000,''AQYUH'');',
'  :p113_mes       := extract(MONTH FROM SYSDATE);',
'  :p113_ano       := extract(YEAR FROM SYSDATE) ;',
'  ln_dia_inicio   := 1;',
'  EXECUTE IMMEDIATE ''alter session set nls_date_format="dd/mm/yyyy"'';',
'  ld_fecha_inicio := (to_date(ln_dia_inicio || ''/'' || :p113_mes || ''/'' ||',
'                              :p113_ano,',
'                              ''DD/MM/YYYY''));',
'                              ',
'  ln_dia_fin      := extract(DAY FROM last_day(ld_fecha_inicio));',
'  ',
'  ld_fecha_fin    := (to_date(ln_dia_fin || ''/'' || :p113_mes || ''/'' ||',
'                              :p113_ano,',
'                              ''DD/MM/YYYY''));',
'  if :f_user_id = 3686 then',
'  :p113_fecha_inicio := trunc(SYSDATE - 160, ''mm'');-- ld_fecha_inicio;',
'  :p113_fecha_fin    := last_day(trunc(SYSDATE - 100));--ld_fecha_fin + 0.99999;',
'  else',
'  :p113_fecha_inicio := trunc(SYSDATE, ''mm'');-- ld_fecha_inicio;',
'  :p113_fecha_fin    := last_day(trunc(SYSDATE));--ld_fecha_fin + 0.99999;',
'  end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>9798420903093680
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(499643957025002194)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_lista_facturas'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    la_nombres              apex_application_global.vc_arr2;',
'    la_valores              apex_application_global.vc_arr2;',
'    lv_nombres              VARCHAR2(2000);',
'    lv_valores              VARCHAR2(2000);',
'    lv_query                VARCHAR2(30000);',
'BEGIN',
'  lv_nombres:=''cv_com_tipo_fa:cv_com_tipo_nc:pd_desde:pd_hasta:cv_formato:f_emp_id:cn_var_id_venta_neta:f_age_id_agencia''; ',
'  lv_nombres:= lv_nombres||'':cn_tse_id_mayoreo:cv_estado_reg_activo:cv_espacio'';  --:cn_var_id_total_cred_entrada',
'  lv_valores:=pq_constantes.fn_retorna_constante(0,''cv_com_tipo_factura'');',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(0,''cv_com_tipo_nc'');',
'  lv_valores:=lv_valores||'':''||:p113_fecha_inicio;',
'  lv_valores:=lv_valores||'':''||:p113_fecha_fin;',
'  lv_valores:=lv_valores||'':''||''dd/mm/yyyy'';',
'  lv_valores:=lv_valores||'':''||:f_emp_id;',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_venta_neta'');',
'  lv_valores:=lv_valores||'':''||:f_age_id_agencia;',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'');',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(0,''cv_estado_reg_activo'');',
'  lv_valores:=lv_valores||'':''||'' '';',
'  --lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total_cred_entrada'');',
'  ',
'lv_query:= ''SELECT c.cli_id,',
'       pe.per_razon_social || pe.per_primer_nombre || :cv_espacio ||',
'       pe.per_segundo_nombre || :cv_espacio || pe.per_primer_apellido || :cv_espacio ||',
'       pe.per_segundo_apellido nombre,',
'       round(SUM(CASE',
'                   WHEN c.com_tipo = :cv_com_tipo_fa THEN',
'                    vd.vcd_valor_variable',
'                   WHEN c.com_tipo = :cv_com_tipo_nc THEN',
'                    vd.vcd_valor_variable * -1',
'                 END),',
'             2) valor,',
'       round(COUNT(vd.vcd_valor_variable), 2) nro_det,',
'       round(MAX(vd.vcd_valor_variable), 2) factura_mas_alta,',
'       NULL age,      ',
'        :pd_desde,',
'        :pd_hasta,',
'       NULL generar_nc,',
'       1 consulta',
'  FROM ven_comprobantes         c,',
'       ven_comprobantes_det     d,',
'       ven_var_comprobantes_det vd,',
'       inv_items_categorias     ic,',
'       asdm_clientes            cl,',
'       asdm_personas            pe',
' WHERE c.com_fecha BETWEEN to_date(:pd_desde,:cv_formato) AND to_date(:pd_hasta,:cv_formato) +  0.99999   ',
'   AND c.emp_id = :f_emp_id',
'   AND d.com_id = c.com_id',
'   AND d.ite_sku_id = ic.ite_sku_id',
'   AND d.cde_id = vd.cde_id',
'   AND vd.var_id = :cn_var_id_venta_neta',
'   AND c.age_id_agencia = :f_age_id_agencia',
'   AND ic.cat_id IN (SELECT cat_id FROM asdm_e.ven_parametrizacion_cat_nc)',
'   AND cl.cli_id = c.cli_id      ',
'   AND cl.per_id = pe.per_id',
'   AND c.tse_id = :cn_tse_id_mayoreo       ',
'   AND d.ite_sku_id NOT IN',
'       (SELECT ite_sku_id',
'          FROM asdm_e.ven_items_excl_nc nc',
'         WHERE c.com_fecha BETWEEN nc.ien_fecha_inicio AND',
'               nc.ien_fecha_fin + 0.99999',
'           AND nc.ien_estado_registro =:cv_estado_reg_activo )',
'   AND not exists (SELECT null ',
'                  FROM asdm_e.ven_comprobantes_nc_montos nn ',
'                  where nn.cde_id = d.cde_id)',
'   HAVING COUNT(UNIQUE(c.age_id_agencia)) = 1',
'',
' GROUP BY c.cli_id,',
'          pe.per_primer_nombre,',
'          pe.per_segundo_nombre,',
'          pe.per_primer_apellido,',
'          pe.per_segundo_apellido,',
'          pe.per_razon_social'';',
'/*UNION ALL',
'',
'SELECT caa.cli_id,',
'       pe.per_razon_social || pe.per_primer_nombre || :cv_espacio ||',
'       pe.per_segundo_nombre || :cv_espacio || pe.per_primer_apellido || :cv_espacio ||',
'       pe.per_segundo_apellido nombre,',
'       round(SUM(CASE',
'                   WHEN caa.com_tipo = :cv_com_tipo_fa THEN',
'                    vd.vcd_valor_variable',
'                   WHEN caa.com_tipo = :cv_com_tipo_nc THEN',
'                    vd.vcd_valor_variable * -1',
'                 END),',
'             2) valor,',
'       ',
'       round(COUNT(vd.vcd_valor_variable), 2) nro_det,',
'       round(MAX(vd.vcd_valor_variable), 2) factura_mas_alta,',
'       CASE',
'       ',
'         WHEN (SELECT age_id_agencia',
'                 FROM (SELECT ca.age_id_agencia,',
'                              SUM(vd.vcd_valor_variable) valor,',
'                              ca.cli_id',
'                         FROM ven_comprobantes         ca,',
'                              ven_comprobantes_det     d,',
'                              ven_var_comprobantes_det vd,',
'                              inv_items_categorias     ic',
'                        WHERE ca.com_fecha BETWEEN to_date(:pd_desde,:cv_formato) AND to_date(:pd_hasta,:cv_formato) +  0.99999 ',
'                          AND com_tipo = :cv_com_tipo_fa',
'                          AND d.com_id = ca.com_id',
'                          AND d.ite_sku_id = ic.ite_sku_id',
'                          AND d.cde_id = vd.cde_id',
'                          AND vd.var_id =:cn_var_id_total_cred_entrada                              ',
'                          AND ic.cat_id IN',
'                              (SELECT cat_id',
'                                 FROM asdm_e.ven_parametrizacion_cat_nc)',
'                          AND ca.tse_id = :cn_tse_id_mayoreo     ',
'                        GROUP BY ca.age_id_agencia, ca.cli_id',
'                        ORDER BY SUM(vd.vcd_valor_variable) DESC) h',
'                WHERE rownum = 1',
'                  AND h.cli_id = caa.cli_id) = :f_age_id_agencia THEN',
'          5',
'         ELSE',
'          NULL',
'       END,       ',
'        :pd_desde,',
'        :pd_hasta,',
'       NULL generar_nc,',
'       2 consulta',
'  FROM ven_comprobantes         caa,',
'       ven_comprobantes_det     d,',
'       ven_var_comprobantes_det vd,',
'       inv_items_categorias     ic,',
'       asdm_clientes            cl,',
'       asdm_personas            pe',
' WHERE caa.com_fecha BETWEEN to_date(:pd_desde,:cv_formato) AND to_date(:pd_hasta,:cv_formato) +  0.99999 ',
'      --AND com_tipo = :cv_com_tipo_fa',
'   AND d.com_id = caa.com_id',
'   AND d.ite_sku_id = ic.ite_sku_id',
'   AND d.cde_id = vd.cde_id',
'   AND vd.var_id = :cn_var_id_venta_neta',
'   AND :f_age_id_agencia IN',
'       (SELECT age_id_agencia',
'          FROM ven_comprobantes ce',
'         WHERE ce.cli_id = caa.cli_id',
'           AND ce.com_fecha BETWEEN to_date(:pd_desde,:cv_formato) AND to_date(:pd_hasta,:cv_formato) +  0.99999 ',
'           AND ce.tse_id = :cn_tse_id_mayoreo)',
'   AND ic.cat_id IN (SELECT cat_id FROM asdm_e.ven_parametrizacion_cat_nc)',
'   AND cl.cli_id = caa.cli_id',
'   AND pe.per_id = cl.per_id',
'   AND caa.tse_id =:cn_tse_id_mayoreo',
'   AND d.ite_sku_id NOT IN',
'       (SELECT ite_sku_id',
'          FROM asdm_e.ven_items_excl_nc nc',
'         WHERE caa.com_fecha BETWEEN nc.ien_fecha_inicio AND',
'               nc.ien_fecha_fin + 0.99999',
'           AND nc.ien_estado_registro =:cv_estado_reg_activo)',
'   AND d.cde_id NOT IN',
'       (SELECT cde_id FROM asdm_e.ven_comprobantes_nc_montos) HAVING',
' COUNT(UNIQUE(caa.age_id_agencia)) > 1',
'-- AND caa.cli_id = 339419',
' GROUP BY caa.cli_id,',
'          pe.per_primer_nombre,',
'          pe.per_segundo_nombre,',
'          pe.per_primer_apellido,',
'          pe.per_segundo_apellido,',
'          pe.per_razon_social*/',
'  --  pr_pruebas_et(556,''AAA'',lv_nombres ||'' VALUES '' || lv_valores);',
'    la_nombres := apex_util.string_to_table(lv_nombres, '':'');',
'    la_valores := apex_util.string_to_table(lv_valores, '':'');',
'',
'    IF apex_collection.collection_exists(p_collection_name => ''COLL_PARA_NC_MONTOS'') THEN',
'      apex_collection.delete_collection(''COLL_PARA_NC_MONTOS'');',
'    END IF;',
'  ',
'    IF length(lv_nombres) > 0 THEN',
'      apex_collection.create_collection_from_query_b(p_collection_name => ''COLL_PARA_NC_MONTOS'',',
'                                                     p_query           => lv_query,',
'                                                     p_names           => la_nombres,',
'                                                     p_values          => la_valores);',
'    END IF;',
'EXCEPTION',
'  WHEN OTHERS THEN ',
'    raise_application_error(-20000,''Error.''||sqlerrm);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>467390805755237268
);
wwv_flow_imp.component_end;
end;
/
