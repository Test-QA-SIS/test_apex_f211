prompt --application/pages/page_00038
begin
--   Manifest
--     PAGE: 00038
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>38
,p_name=>'CAR PAGO CUOTA'
,p_step_title=>'CAR PAGO CUOTA'
,p_reload_on_submit=>'A'
,p_autocomplete_on_off=>'OFF'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>',
'',
'<script type="text/javascript" >',
'',
'function valida_numero(ln_valor)',
'{',
'console.log(ln_valor);',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00FAmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'',
'function valida_numEnterTab(evt,itemid,ln_valor){',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'  if (evt.keyCode == 13) {',
'     ',
'        $x(itemid).focus();',
'        return false;',
'           ',
'   }else if(evt.keyCode == 8){',
'       return true;',
'   }else if(evt.keyCode == 9){',
'       return true;',
'   }else if(evt.keyCode == null){',
'       return true;  ',
'   }else{',
'       if (!patron_numero.test((ln_valor).value)) {',
unistr('        alert(''Ingrese solo n\00FAmeros'');'),
'        ln_valor.value = '''';',
'        html_GetElement((ln_valor).id).focus();',
'        return true;',
'        } ',
'    }',
' return false;',
'}',
'',
'function valida_numero_dec(ln_valor)',
'{',
'var patron_numero =/^^\d{1,10}(\.\d{1,2})?$/;',
'if (!patron_numero.test((ln_valor).value))',
'{',
unistr('alert(''Debe Ingresar solo N\00C3\00BAmeros con dos Decimales'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function sololetras(e) {',
'    key=e.keyCode || e.which;',
'    teclado=String.fromCharCode(key).toLowerCase();',
unistr('    letras="qwertyuiopasdfghjkl\00F1zxcvbnm ";'),
'    especiales="8-37-38-46-164";',
'    teclado_especial=false;',
'    for(var i in especiales){',
'        if(key==especiales[i]){',
'            teclado_especial=true;',
'            break;',
'        }',
'    }',
' ',
'    if(letras.indexOf(teclado)==-1 && !teclado_especial){',
'        return ;        ',
'    }',
'',
'    return 1;',
'}',
'',
'',
'',
'function valida_letras(ln_valor)',
'{',
'let regex = /^[a-zA-Z]+$/; ',
'if (!regex.test((ln_valor).value))',
'{',
'alert(''Debe Ingresar solo Letras'');',
'ln_valor.value = "";',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'function convertir_mayu(ln_valor)',
'{',
'var res_valida_letras;',
'ln_valor.value = ln_valor.value.toUpperCase();',
'',
'',
'}',
'',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'ALEMA'
,p_last_upd_yyyymmddhh24miss=>'20240206111232'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(70323333579643271)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pq_car_redes_pago.fn_retorna_saldo_tarjeta(pn_cli_id => :p38_cli_id,',
'                                                  pn_uge_id => :f_uge_id) SALDO_TARJETA, ',
'       to_number(SUM(nvl(c006, 0))) valor',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_mov_caja'')',
'        ',
'       AND to_number(c005) =',
'           asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                     ''cn_tfp_id_tarjeta_credito'');'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133255618613604464)
,p_query_column_id=>1
,p_column_alias=>'SALDO_TARJETA'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo tarjeta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133255470718604463)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>1
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(4890058987573743402)
,p_plug_name=>'MENSAJE CUOTAS GRATIS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>5
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>'pq_asdm_promociones.fn_promo_cuotas_gratis_new(:f_user_id, :p38_cli_id) is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44840793533474029556)
,p_name=>'col_mov_caja'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select C001, C002, C003, C004, C005, C006, C007 from apex_collections where collection_name = ''COL_MOV_CAJA'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840793805815029559)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840793858373029560)
,p_query_column_id=>2
,p_column_alias=>'C002'
,p_column_display_sequence=>2
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840794048234029561)
,p_query_column_id=>3
,p_column_alias=>'C003'
,p_column_display_sequence=>3
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840794072292029562)
,p_query_column_id=>4
,p_column_alias=>'C004'
,p_column_display_sequence=>4
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840794237754029563)
,p_query_column_id=>5
,p_column_alias=>'C005'
,p_column_display_sequence=>5
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840794339501029564)
,p_query_column_id=>6
,p_column_alias=>'C006'
,p_column_display_sequence=>6
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840794429461029565)
,p_query_column_id=>7
,p_column_alias=>'C007'
,p_column_display_sequence=>7
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(48423596727424614230)
,p_plug_name=>'<SPAN STYLE="font-size: 10pt">PAGO DE CLIENTE - &P38_CLI_ID.  - &P38_NOMBRES.</span>'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38522435678962006177)
,p_plug_name=>'tfp_Cheques'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38522447713490035751)
,p_plug_name=>'IngresoValor'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>80
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38522467515709173110)
,p_plug_name=>'tfp_deposito_transferencia'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38549183528310256356)
,p_plug_name=>'Tarjeta Credito'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P38_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(42753658437951935)
,p_name=>'Lectura tarjeta'
,p_parent_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT res.rpc_id,',
'       res.rpr_fecha_respuesta,',
'       res.rpr_talla,',
'       res.rpr_tipo_respuesta,',
'       (SELECT tra.rpt_nombre',
'          FROM car_redes_pago_tipo_tra ttr, car_redes_pago_tra tra',
'         WHERE ttr.rpa_id = ca.rpa_id',
'           AND ttr.rpt_nombre = ''RESPUESTA''',
'           AND ttr.rpt_id = tra.rpt1_id',
'           AND tra.rpt_codigo = res.rpr_codigo_respuesta) respuesta,',
'       res.rpr_codigo_respuesta,',
'       (SELECT tra.rpt_nombre',
'          FROM car_redes_pago_tipo_tra ttr, car_redes_pago_tra tra',
'         WHERE ttr.rpa_id = ca.rpa_id',
'           AND ttr.rpt_nombre = ''COD RED ADQUIRIENTE''',
'           AND ttr.rpt_id = tra.rpt1_id',
'           AND tra.rpt_codigo = res.rpr_codigo_respuesta) red_adq_corriente,',
'       res.rpr_red_adq_cor,',
'           (SELECT tra.rpt_nombre',
'          FROM car_redes_pago_tipo_tra ttr, car_redes_pago_tra tra',
'         WHERE ttr.rpa_id = ca.rpa_id',
'           AND ttr.rpt_nombre = ''COD DIFERIDO''',
'           AND ttr.rpt_id = tra.rpt1_id',
'           AND tra.rpt_codigo = res.rpr_codigo_respuesta) red_adq_diferido,',
'       res.rpr_red_adq_dif,',
'       res.rpr_num_tarj_trun,',
'       res.rpr_fecha_vencim,',
'       res.rpr_num_tarj_encrip,',
'       res.rpr_men_respuesta',
'  FROM car_redes_pago_resp res, car_redes_pago_cab ca',
' WHERE res.rpc_id = :P38_RPC_ID',
'   AND res.rpc_id = ca.rpc_id',
'   AND res.rpr_tipo_respuesta = ''LT'';'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42753751296951936)
,p_query_column_id=>1
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Rpc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42753916461951937)
,p_query_column_id=>2
,p_column_alias=>'RPR_FECHA_RESPUESTA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754049497951938)
,p_query_column_id=>3
,p_column_alias=>'RPR_TALLA'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754106300951939)
,p_query_column_id=>4
,p_column_alias=>'RPR_TIPO_RESPUESTA'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo _Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211213867150741)
,p_query_column_id=>5
,p_column_alias=>'RESPUESTA'
,p_column_display_sequence=>14
,p_column_heading=>'Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754180568951940)
,p_query_column_id=>6
,p_column_alias=>'RPR_CODIGO_RESPUESTA'
,p_column_display_sequence=>5
,p_column_heading=>unistr('C\00F3digo Respuesta')
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211314909150742)
,p_query_column_id=>7
,p_column_alias=>'RED_ADQ_CORRIENTE'
,p_column_display_sequence=>7
,p_column_heading=>'Red adq corriente'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754307352951941)
,p_query_column_id=>8
,p_column_alias=>'RPR_RED_ADQ_COR'
,p_column_display_sequence=>6
,p_column_heading=>'Red adq corriente'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(146211448111150743)
,p_query_column_id=>9
,p_column_alias=>'RED_ADQ_DIFERIDO'
,p_column_display_sequence=>9
,p_column_heading=>'Red adq diferido'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754366480951942)
,p_query_column_id=>10
,p_column_alias=>'RPR_RED_ADQ_DIF'
,p_column_display_sequence=>8
,p_column_heading=>'Red adq diferido'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754507324951943)
,p_query_column_id=>11
,p_column_alias=>'RPR_NUM_TARJ_TRUN'
,p_column_display_sequence=>10
,p_column_heading=>'Num Tarjeta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754586965951944)
,p_query_column_id=>12
,p_column_alias=>'RPR_FECHA_VENCIM'
,p_column_display_sequence=>11
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754694569951945)
,p_query_column_id=>13
,p_column_alias=>'RPR_NUM_TARJ_ENCRIP'
,p_column_display_sequence=>12
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42754762495951946)
,p_query_column_id=>14
,p_column_alias=>'RPR_MEN_RESPUESTA'
,p_column_display_sequence=>13
,p_column_heading=>'Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(42754952823951948)
,p_name=>'Consulta Tarjeta'
,p_parent_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT rpc_id,',
'       rpr_fecha_respuesta,',
'       RPR_TALLA,',
'       RPR_TIPO_RESPUESTA,',
'       RPR_CODIGO_RESPUESTA,',
'       RPR_NUM_TARJ_ENCRIP,',
'       RPR_BIN_TARJETA,',
'       RPR_FECHA_VENCIM,',
'       RPR_RESPUESTA,',
'       RPR_FILLER',
'  FROM car_redes_pago_resp',
' WHERE rpc_id = :P38_RPC_ID',
'   AND RPR_TIPO_RESPUESTA = ''CT'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755074441951949)
,p_query_column_id=>1
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Rpc id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755221279951950)
,p_query_column_id=>2
,p_column_alias=>'RPR_FECHA_RESPUESTA'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755348938951951)
,p_query_column_id=>3
,p_column_alias=>'RPR_TALLA'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755354280951952)
,p_query_column_id=>4
,p_column_alias=>'RPR_TIPO_RESPUESTA'
,p_column_display_sequence=>4
,p_column_heading=>'Tipo Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755490492951953)
,p_query_column_id=>5
,p_column_alias=>'RPR_CODIGO_RESPUESTA'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755604858951954)
,p_query_column_id=>6
,p_column_alias=>'RPR_NUM_TARJ_ENCRIP'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755748348951955)
,p_query_column_id=>7
,p_column_alias=>'RPR_BIN_TARJETA'
,p_column_display_sequence=>7
,p_column_heading=>'Bin'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755808468951956)
,p_query_column_id=>8
,p_column_alias=>'RPR_FECHA_VENCIM'
,p_column_display_sequence=>8
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755857834951957)
,p_query_column_id=>9
,p_column_alias=>'RPR_RESPUESTA'
,p_column_display_sequence=>9
,p_column_heading=>'Respuesta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(42755959621951958)
,p_query_column_id=>10
,p_column_alias=>'RPR_FILLER'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(43074146670640130)
,p_plug_name=>'Procesar pago'
,p_parent_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133252287146604431)
,p_plug_name=>'Datos de la tarjeta'
,p_parent_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(38571059053985976937)
,p_plug_name=>'tfp_retencion'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(39967119541676674981)
,p_name=>'Retenciones'
,p_parent_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'seq_id id,',
'to_number(c001) com_id,',
'c002 fecha_ret,',
'c003 fecha_validez,',
'c004 nro_retencion,',
'c005 nro_autorizacion,',
'c006 nro_establecimiento,',
'c007 nro_pto_emision,',
'c008 porcentaje,',
'to_number(c009) base,',
'to_number(c013) valor_calculado,',
'apex_item.text(p_idx        => 25,',
'                      p_value      => to_number(c010),',
'                      p_attributes => ''unreadonly id="f47_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c013||'',''||seq_id||'',this.value)"'',',
'                      p_size       => ''10'') valor_retencion,',
'''X'' eliminar,',
'c026 tipo',
'',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION''',
'UNION',
'select ',
'NULL id,',
'NULL com_id,',
'NULL fecha_ret,',
'NULL fecha_validez,',
'NULL nro_retencion,',
'NULL nro_autorizacion,',
'NULL nro_establecimiento,',
'NULL nro_pto_emision,',
'NULL porcentaje,',
'SUM(to_number(c009)) base,',
'sum(to_number(c013)) valor_calculado,',
'TO_CHAR(SUM(to_number(c010))) valor_retencion,',
'NULL eliminar,',
'null tipo',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507824334626417503)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>13
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507824720197417503)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ord Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507825107034417503)
,p_query_column_id=>3
,p_column_alias=>'FECHA_RET'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Ret'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507825501382417503)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VALIDEZ'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Validez'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507825894764417504)
,p_query_column_id=>5
,p_column_alias=>'NRO_RETENCION'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Retencion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507826342900417504)
,p_query_column_id=>6
,p_column_alias=>'NRO_AUTORIZACION'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Autorizacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507826683676417504)
,p_query_column_id=>7
,p_column_alias=>'NRO_ESTABLECIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Nro Establecimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507827057411417504)
,p_query_column_id=>8
,p_column_alias=>'NRO_PTO_EMISION'
,p_column_display_sequence=>7
,p_column_heading=>'Nro Pto Emision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507827476400417504)
,p_query_column_id=>9
,p_column_alias=>'PORCENTAJE'
,p_column_display_sequence=>8
,p_column_heading=>'Porcentaje'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507827933107417505)
,p_query_column_id=>10
,p_column_alias=>'BASE'
,p_column_display_sequence=>10
,p_column_heading=>'Base'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507828327567417505)
,p_query_column_id=>11
,p_column_alias=>'VALOR_CALCULADO'
,p_column_display_sequence=>11
,p_column_heading=>'Valor Calculado'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507828655564417505)
,p_query_column_id=>12
,p_column_alias=>'VALOR_RETENCION'
,p_column_display_sequence=>12
,p_column_heading=>'Valor Retencion'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507829099237417506)
,p_query_column_id=>13
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>14
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:ELIMINA_RET:&DEBUG.:RP:P30_SEQ_ID_RET:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507823876328417501)
,p_query_column_id=>14
,p_column_alias=>'TIPO'
,p_column_display_sequence=>9
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(63177038579111916272)
,p_plug_name=>'Registro de Folio'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>90
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':f_uge_id  in (SELECT A.UGE_ID',
'               FROM asdm_uge_registro_folios A',
'               WHERE A.URF_ESTADO_REGISTRO = ''0'')'))
,p_plug_display_when_cond2=>'SQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(63177038728482916273)
,p_name=>'Registro de Recibos'
,p_parent_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'       SELECT * FROM (',
'      select c.rec_id ,C.BLK_ID CODIGO_BLOQUE,C.FCA_ID CODIGO_PEDIDO,C.FCA_CODIGO CODIGO_RECIBO,C.FCA_ESTATUS ESTADO_RECIBO, c.fca_secuencia  from asdm_e.asdm_folios_caja a , asdm_e.asdm_folios_block b, asdm_e.asdm_folios_block_det c',
'      where ',
'     -- a.uge_id=:F_UGE_ID',
'      a.fca_estado=''R''',
'      AND A.FCA_ESTADO_REGISTRO=''0''',
'      AND B.FCA_ID=A.FCA_ID',
'      AND B.FCA_ESTATUS=''ENT''',
'      AND B.FCA_ESTADO_REGISTRO=''0''',
'--      AND B.COD_AGENTE=231',
'      AND B.FCA_FHASTA=TO_DATE(''31/12/2999'',''DD/MM/YYYY'')',
'      AND C.FCA_ID=B.FCA_ID',
'      AND C.BLK_ID=B.BLK_ID',
'      AND C.FCA_ESTADO_REGISTRO=''0''',
'      AND C.FCA_FHASTA=TO_DATE(''31/12/2999'',''DD/MM/YYYY'')',
'      AND C.COD_AGENTE=:P38_COBRADORES',
'      AND C.FCA_ESTATUS=''PAG'' ',
'      AND A.TTC_ID=:P38_TIPO_FOLIO     ',
'      ORDER BY C.FCA_SECUENCIA ASC)v1',
'      WHERE ROWNUM<2',
'      ;     '))
,p_display_when_condition=>'P38_REGISTRA_RECIBO'
,p_display_when_cond2=>'1'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63184449548594115475)
,p_query_column_id=>1
,p_column_alias=>'REC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Rec id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63180996617613712872)
,p_query_column_id=>2
,p_column_alias=>'CODIGO_BLOQUE'
,p_column_display_sequence=>2
,p_column_heading=>'Codigo bloque'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63180996711688712873)
,p_query_column_id=>3
,p_column_alias=>'CODIGO_PEDIDO'
,p_column_display_sequence=>3
,p_column_heading=>'Codigo pedido'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63180996798075712874)
,p_query_column_id=>4
,p_column_alias=>'CODIGO_RECIBO'
,p_column_display_sequence=>4
,p_column_heading=>'Codigo recibo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63180996906660712875)
,p_query_column_id=>5
,p_column_alias=>'ESTADO_RECIBO'
,p_column_display_sequence=>5
,p_column_heading=>'Estado recibo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63184448886022115469)
,p_query_column_id=>6
,p_column_alias=>'FCA_SECUENCIA'
,p_column_display_sequence=>6
,p_column_heading=>'Fca secuencia'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(63184448821297115468)
,p_query_column_id=>7
,p_column_alias=>'DERIVED$01'
,p_column_display_sequence=>7
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:38:&SESSION.::&DEBUG.:RP:P38_SECUENCIA:#REC_ID#'
,p_column_linktext=>'ANULAR'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(63184449183010115472)
,p_plug_name=>unistr('Motivo Anulaci\00F3n')
,p_parent_plug_id=>wwv_flow_imp.id(63177038728482916273)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P38_SECUENCIA'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(48423597506612614238)
,p_name=>'DETALLE PAGO'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tfp_descripcion,',
'       to_number(c006) valor,',
'       c040 detalle_pago,',
'       co.seq_id,',
'       (SELECT substr(r.rpr_num_tarj_trun, 1, 6)',
'          FROM car_redes_pago_resp r',
'         WHERE r.rpr_id = c030) bin,',
'       (SELECT r.rpr_fecha_respuesta',
'          FROM car_redes_pago_resp r',
'         WHERE r.rpr_id = c030) fecha,',
'       (SELECT r.rpr_secuen_trans',
'          FROM car_redes_pago_resp r',
'         WHERE r.rpr_id = c030) nroregistro,',
'       (SELECT r.rpc_id FROM car_redes_pago_resp r WHERE r.rpr_id = c030) rpc_id,',
'       (select r.rpr_id_req from car_redes_pago_resp r where r.rpr_id=c030) rpr_id,',
'       CASE',
'         WHEN (c030 IS NOT NULL AND',
'              (SELECT trunc(r.rpr_fecha_respuesta)',
'                  FROM car_redes_pago_resp r',
'                 WHERE r.rpr_id = c030) = trunc(SYSDATE)) THEN',
'          ''ANULAR''',
'         ELSE',
'          ''''',
'       END anular',
'  FROM apex_collections co, asdm_tipos_formas_pago p',
' WHERE collection_name = ''CO_MOV_CAJA''',
'   AND p.tfp_id = to_number(co.c005);',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(49644566838711879550)
,p_query_column_id=>1
,p_column_alias=>'TFP_DESCRIPCION'
,p_column_display_sequence=>1
,p_column_heading=>'Forma de Pago'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:20;">#TFP_DESCRIPCION#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38507902516978043648)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:20;">#VALOR#</span>'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(49644566912035879551)
,p_query_column_id=>3
,p_column_alias=>'DETALLE_PAGO'
,p_column_display_sequence=>3
,p_column_heading=>'Detalles Adicionales'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(49644569132130879573)
,p_query_column_id=>4
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:38:&SESSION.:ELIMINA_MOVCAJA:&DEBUG.:RP:P38_SEQ_ID_MOVCAJA:#SEQ_ID#'
,p_column_linktext=>'Eliminar'
,p_column_link_attr=>'class="lock_ui_row" '
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43075974463640149)
,p_query_column_id=>5
,p_column_alias=>'BIN'
,p_column_display_sequence=>4
,p_column_heading=>'Bin'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P38_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')',
''))
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43076055879640150)
,p_query_column_id=>6
,p_column_alias=>'FECHA'
,p_column_display_sequence=>5
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>' :P38_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(43076181893640151)
,p_query_column_id=>7
,p_column_alias=>'NROREGISTRO'
,p_column_display_sequence=>6
,p_column_heading=>'Nroregistro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' :P38_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')',
''))
,p_display_when_condition2=>'PLSQL'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133255105085604459)
,p_query_column_id=>8
,p_column_alias=>'RPC_ID'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133254270239604451)
,p_query_column_id=>9
,p_column_alias=>'RPR_ID'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133255043199604458)
,p_query_column_id=>10
,p_column_alias=>'ANULAR'
,p_column_display_sequence=>9
,p_column_heading=>'Anular'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:38:&SESSION.:reverso_tarjeta:&DEBUG.:RP,:P38_RPR_ID_REQ,P38_RPC_ID,P38_TIPO_IMPRESION,P38_SEQ_ID_MOVCAJA:#RPR_ID#,#RPC_ID#,3,#SEQ_ID#'
,p_column_linktext=>'#ANULAR#'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42753338607951931)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(42753658437951935)
,p_button_name=>'BTN_LECTURA_TARJETA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'CONSULTAR'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38525611224867238531)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(48423597506612614238)
,p_button_name=>'GRABAR'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'GRABAR'
,p_button_position=>'BELOW_BOX'
,p_warn_on_unsaved_changes=>null
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select sum(to_number(c006))',
'from apex_collections',
'where collection_name = ''CO_MOV_CAJA'') = :P38_VALOR_TOTAL_PAGOS'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(63184448552999115466)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(63184449183010115472)
,p_button_name=>'Anular'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Anular'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(43074904762640138)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(42754952823951948)
,p_button_name=>'BTN_CONSULTA_TARJETA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'CONSULTAR'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133252696205604435)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_button_name=>'BTN_VER_SALDO_PTJ'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_image_alt=>'VER'
,p_button_redirect_url=>'f?p=&APP_ID.:157:&SESSION.::&DEBUG.:RP:P157_CLI_ID,P157_UGE_ID:&P38_CLI_ID.,&F_UGE_ID.'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ca.rpa_valor_pag',
'          FROM car_redes_pago_cab  ca,',
'               asdm_clientes       cl,',
'               asdm_personas       pe,',
'               car_redes_pago_req  r,',
'               car_redes_pago_resp rs',
'         WHERE ca.rpc_id = rs.rpc_id',
'           AND ca.cli_id = cl.cli_id',
'           AND cl.per_id = pe.per_id',
'           AND ca.rpc_id = r.rpc_id',
'           AND rs.rpr_id_req = r.rpr_id',
'           AND ca.rpc_estado != ''CERRADO''',
'           AND r.rpr_estado_pago IN (''PROCESADO'')',
'           AND rs.rpr_estado_resp IN (''PROCESADO'')',
'           AND ca.cli_id = :p38_cli_id',
'           AND ca.uge_id = :f_uge_id',
'           AND rs.rpr_tipo_respuesta = ''PP''',
'           AND r.rpr_tipo_requerimiento = ''PP'';'))
,p_button_condition_type=>'EXISTS'
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38507839075231427389)
,p_button_sequence=>500
,p_button_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_button_name=>'CARGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:38:&SESSION.:cargar_tfp:&DEBUG.:::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(48423597317303614236)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'<< REGRESAR PAGO DE CUOTA'
,p_button_position=>'TOP'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:7:&SESSION.:LIMPIAPAGO:&DEBUG.:RP::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38525611532188238534)
,p_branch_name=>'br_grabar'
,p_branch_action=>'f?p=&APP_ID.:38:&SESSION.:GRABAR:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(38525611224867238531)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42478262044527327)
,p_name=>'P38_MENSAJE_PROMO_CUOTAS'
,p_item_sequence=>941
,p_item_plug_id=>wwv_flow_imp.id(4890058987573743402)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_asdm_promociones.fn_promo_cuotas_gratis_new(:f_user_id, :p38_cli_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:18;color:GREEN"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42752874259951927)
,p_name=>'P38_TIPO_PAGO_TJ'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_use_cache_before_default=>'NO'
,p_item_default=>'M'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
unistr('  FROM (SELECT ''Autom\00E1tico'' d, ''A'' r'),
'          FROM dual',
'         WHERE (SELECT COUNT(pue.rpp_id)',
'                  FROM car_redes_pago_pue pue',
'                 WHERE pue.uge_id = :F_uge_id',
'                AND pue.rpp_estado_registro=0) > 0',
'        UNION',
'        SELECT ''Manual'' d, ''M'' r',
'          FROM dual)'))
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42753479372951933)
,p_name=>'P38_TRAMA_RESP'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_prompt=>'Resp:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42753649388951934)
,p_name=>'P38_RPC_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(42757181771951970)
,p_name=>'P38_PROCESO_TJ'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_item_default=>'PP'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'  FROM (SELECT ''LECTURA DE TARJETA'' D, ''LT'' R',
'          FROM DUAL',
'        UNION',
'        SELECT ''CONSULTA DE TARJETA'' D, ''CT'' R',
'          FROM DUAL',
'        UNION',
'        SELECT ''PROCESAR EL PAGO'' D, ''PP'' R',
'          FROM DUAL)'))
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.ede_descripcion,a.ede_id ',
'FROM asdm_entidades_destinos a ',
'WHERE a.ede_tipo=''EFI'' and a.emp_id = :f_emp_id',
'and ede_estado_registro=0;',
'--JALVAREZ 31 JUL 2022'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(55041621687765138)
,p_name=>'P38_FORMA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_prompt=>'Forma:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.ede_descripcion, d.ede_id',
'  FROM asdm_entidades_destinos d',
' WHERE d.ede_id_padre = :P38_TIPO',
'   AND d.ede_tipo = ''PTJ''',
'   AND d.ede_estado_registro = 0;'))
,p_lov_cascade_parent_items=>'P38_TIPO'
,p_ajax_items_to_submit=>'P38_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p38_tarjeta',
'and c.emp_id = :f_emp_id'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133252103501604429)
,p_name=>'P38_PINPAD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pinpad'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''Pinpad: ''||rpa.rpa_nombre ||''/Comercio: '' || rpp.rpp_cid  d,',
'       rpp.rpp_id r',
'  FROM car_redes_pago_pue rpp, asdm_puntos_emision pue, car_redes_pago rpa',
' WHERE rpp.pue_id = pue.pue_id',
'   AND rpp.rpa_id=rpa.rpa_id',
'   AND rpp.rpp_estado_registro = 0',
'   AND rpp.pue_id = :p0_pue_id;'))
,p_cHeight=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_NRO_PINPAD >1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT r.rpa_id || '' - '' || r.rpa_nombre d, r.rpa_id r',
'  FROM car_redes_pago r',
' WHERE r.rpa_estado_registro = 0'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133252183731604430)
,p_name=>'P38_RED'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_item_default=>'2'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133252361447604432)
,p_name=>'P38_SALDO_TARJETA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_car_redes_pago.fn_retorna_saldo_tarjeta(pn_cli_id => 3356423,--:p38_cli_id,',
'                                                        pn_uge_id => 188--:F_UGE_ID',
'                                                 );'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ca.rpa_valor_pag',
'          FROM car_redes_pago_cab  ca,',
'               asdm_clientes       cl,',
'               asdm_personas       pe,',
'               car_redes_pago_req  r,',
'               car_redes_pago_resp rs',
'         WHERE ca.rpc_id = rs.rpc_id',
'           AND ca.cli_id = cl.cli_id',
'           AND cl.per_id = pe.per_id',
'           AND ca.rpc_id = r.rpc_id',
'           AND ca.rpc_estado = ''PROCESADO''',
'           AND ca.cli_id = :p38_cli_id',
'           AND ca.uge_id = :f_uge_id',
'           AND rs.rpr_tipo_respuesta = ''PP''',
'           AND r.rpr_tipo_requerimiento = ''PP'';'))
,p_display_when_type=>'EXISTS'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133252847446604436)
,p_name=>'P38_TARJETA_EMI'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Emisor Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')  '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'LV_TITUTAR VARCHAR2(100);',
'BEGIN',
'SELECT PE.PER_PRIMER_NOMBRE || '' '' || PE.PER_PRIMER_APELLIDO',
'INTO LV_TITUTAR',
'  FROM ASDM_PERSONAS PE, ASDM_CLIENTES CL',
' WHERE PE.PER_ID = CL.PER_ID',
'   AND CL.CLI_ID = :P38_CLI_ID;',
'RETURN LV_TITUTAR;',
'EXCEPTION',
'WHEN OTHERS THEN',
'RETURN NULL;',
'END;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133252867244604437)
,p_name=>'P38_TJ_NUMERO_1'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133253013888604438)
,p_name=>'P38_TJ_NUMERO_VOUCHER_1'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133253133963604439)
,p_name=>'P38_TJ_RECAP_1'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Recap'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133253164164604440)
,p_name=>'P38_MCD_NRO_AUT_REF_1'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Autorizaci&oacute;n:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133254153545604450)
,p_name=>'P38_RPR_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133255302263604461)
,p_name=>'P38_RPR_ID_REQ'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48423597506612614238)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133256524912604473)
,p_name=>'P38_TIPO_IMPRESION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(146213298199150762)
,p_name=>'P38_SALDO_TARJETA_1'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo de pago de Tarjeta'
,p_pre_element_text=>'<b>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_car_redes_pago.fn_retorna_saldo_tarjeta(pn_cli_id => :p38_cli_id,',
'                                                        pn_uge_id => :F_UGE_ID',
'                                                 );'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ca.rpa_valor_pag',
'          FROM car_redes_pago_cab  ca,',
'               asdm_clientes       cl,',
'               asdm_personas       pe,',
'               car_redes_pago_req  r,',
'               car_redes_pago_resp rs',
'         WHERE ca.rpc_id = rs.rpc_id',
'           AND ca.cli_id = cl.cli_id',
'           AND cl.per_id = pe.per_id',
'           AND ca.rpc_id = r.rpc_id',
'           AND rs.rpr_id_req = r.rpr_id',
'           AND ca.rpc_estado != ''CERRADO''',
'           AND r.rpr_estado_pago IN (''PROCESADO'')',
'           AND rs.rpr_estado_resp IN (''PROCESADO'')',
'           AND ca.cli_id = :p38_cli_id',
'           AND ca.uge_id = :f_uge_id',
'           AND rs.rpr_tipo_respuesta = ''PP''',
'           AND r.rpr_tipo_requerimiento = ''PP'';'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(146213889352150768)
,p_name=>'P38_NRO_PINPAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'SELECT count(1)',
'  FROM car_redes_pago_pue rpp',
' WHERE rpp.pue_id=:p0_pue_id',
'   AND rpp.rpp_estado_registro = 0 ;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(854037846583429862)
,p_name=>'P38_MCA_ID_OUT'
,p_item_sequence=>50
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(857574667869138394)
,p_name=>'P38_ERROR_ANTICIPO'
,p_item_sequence=>26
,p_item_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507808265539359176)
,p_name=>'P38_SALDO_ANTICIPOS'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Anticipos'
,p_source=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P38_CLI_ID,:f_uge_id))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P30_CLI_ID,:f_uge_id))'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507808600911368638)
,p_name=>'P38_TFP_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF NVL(:P38_SALDO_ANTICIPOS,0) = 0 THEN',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes''); ',
'END IF;',
'END;'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tfp.tfp_descripcion, tfp.tfp_id',
'  FROM asdm_tipos_formas_pago tfp, asdm_formpag_tipotrans b',
' WHERE b.tfp_id = tfp.tfp_id',
'   AND tfp.tfp_id NOT IN (118,16)',
'   AND b.fpt_estado_registro = 0',
'   AND tfp.tfp_estado_registro = 0',
'   AND tfp.emp_id = :f_emp_id',
'   AND b.ttr_id = 115',
' ORDER BY 1',
''))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P38_VALOR_TOTAL_PAGOS > 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507810346099385571)
,p_name=>'P38_EDE_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_item_default=>'NULL'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Entidad Destino:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
' return pq_ven_listas_caja.fn_lov_entidad_destin_tfp_pag',
'(:F_EMP_ID,',
':P38_TFP_ID,',
':P0_ERROR)'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') or ',
':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or ',
':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'POPUP'
,p_attribute_08=>'CIC'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507810638809388314)
,p_name=>'P38_CRE_TITULAR_CUENTA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_prompt=>'Titular Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>100
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onblur="this.value = this.value.toUpperCase();"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507811668082408282)
,p_name=>'P38_SALDO_PAGO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Pago'
,p_pre_element_text=>'<b>'
,p_source=>'select nvl(:p38_valor_total_pagos,0) - to_number(sum(nvl(c006,0))) VALOR  from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507814855466417487)
,p_name=>'P38_RET_IVA'
,p_item_sequence=>255
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'Retiene Iva'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Si;S,No;N'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507815174076417490)
,p_name=>'P38_RAP_NRO_ESTABL_RET'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'Nro Retencion:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_PEMISION_RET'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507815603975417490)
,p_name=>'P38_RAP_NRO_PEMISION_RET'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_RETENCION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507815969159417491)
,p_name=>'P38_RAP_NRO_RETENCION'
,p_item_sequence=>262
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_AUTORIZACION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507816441853417491)
,p_name=>'P38_RAP_NRO_AUTORIZACION'
,p_item_sequence=>272
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'Nro Autorizacion:'
,p_post_element_text=>' 10 - 37 - 49 digitos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>49
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_FECHA'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507816759133417491)
,p_name=>'P38_RAP_FECHA'
,p_item_sequence=>282
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Retencion:'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507817159607417491)
,p_name=>'P38_RAP_FECHA_VALIDEZ'
,p_item_sequence=>292
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507817554772417491)
,p_name=>'P38_RAP_COM_ID'
,p_item_sequence=>302
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'Factura:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT vco.com_tipo || ''/'' || vco.com_id || '' -'' || vco.com_fecha d,',
'       vco.com_id v',
'  FROM ven_comprobantes vco',
' WHERE com_tipo = ''F''   ',
'   AND EXISTS',
' (SELECT NULL FROM car_cuotas_pagar p ',
'   WHERE p.com_id = vco.com_id',
'    and p.cli_id = :p38_cli_id',
'    and p.usu_id = :f_user_id)',
'   AND NOT EXISTS (SELECT NULL',
'          FROM asdm_retenciones_aplicadas r',
'         WHERE r.com_id = vco.com_id',
'           AND r.rap_estado_registro = 0)',
'   AND NOT EXISTS (SELECT NULL',
'          FROM apex_collections co',
'         WHERE co.collection_name = ''CO_MOV_CAJA''',
'           AND co.c019 = vco.com_id)',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Facturas Disponibles --'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507818005167417492)
,p_name=>'P38_SALDO_RETENCION'
,p_item_sequence=>312
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_post_element_text=>'  para Anticipo de Clientes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507818441460417492)
,p_name=>'P38_PRE_ID'
,p_item_sequence=>372
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507818779269417492)
,p_name=>'P38_TRE_ID'
,p_item_sequence=>1961
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'%'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  if :P38_RET_IVA = ''S'' then',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion , RPO_id',
'FROM CAR_RET_PORCENTAJES',
'where rpo_emp_id = ''||:f_emp_id;',
'  else',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion, RPO_id',
'FROM CAR_RET_PORCENTAJES',
'WHERE RPO_DESCRIPCION = ''''RET FUENTE''''',
'and rpo_emp_id = ''|| :f_emp_id;',
'  end if;',
'  RETURN(lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507819232490417493)
,p_name=>'P38_SEQ_ID_RET'
,p_item_sequence=>1971
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507819612216417494)
,p_name=>'P38_SUBTOTAL_IVA'
,p_item_sequence=>1981
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P38_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_con_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507820025650417494)
,p_name=>'P38_SUBTOTAL_SIN_IVA'
,p_item_sequence=>1991
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Sin Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P38_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_sin_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507820406341417494)
,p_name=>'P38_SUBTOTAL'
,p_item_sequence=>1992
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_item_default=>'(nvl(:P38_SUBTOTAL_IVA,0) + nvl(:P38_SUBTOTAL_SIN_IVA,0)) - nvl(:P38_FLETE, 0)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Subtotal'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507820827242417495)
,p_name=>'P38_FLETE'
,p_item_sequence=>2011
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Flete'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P38_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_servicio_manejo_producto'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507821228865417495)
,p_name=>'P38_IVA'
,p_item_sequence=>2021
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P38_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_iva_calculo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507821610925417496)
,p_name=>'P38_IVA_BIENES'
,p_item_sequence=>2031
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P38_IVA - (:P38_FLETE * 0.12)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Bienes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507822035356417496)
,p_name=>'P38_IVA_FLETE'
,p_item_sequence=>2041
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P38_FLETE * 0.14'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Flete'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507822394266417496)
,p_name=>'P38_INT_DIFERIDO'
,p_item_sequence=>2051
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Int Diferido'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P38_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_int_diferido'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507822822416417496)
,p_name=>'P38_BASE'
,p_item_sequence=>2061
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'Base'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGA_RET'');" '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507823164820417496)
,p_name=>'P38_PRECIONEENTER11'
,p_item_sequence=>2071
,p_item_plug_id=>wwv_flow_imp.id(38571059053985976937)
,p_prompt=>'<SPAN STYLE="font-size: 10pt;color:RED;">Presione Enter para cargar el valor</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507829498250417506)
,p_name=>'P38_OBSERVACIONES_RET'
,p_item_sequence=>2081
,p_item_plug_id=>wwv_flow_imp.id(39967119541676674981)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507830225163420640)
,p_name=>'P38_CRE_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(38522435678962006177)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507830570099420640)
,p_name=>'P38_CRE_FECHA_DEPOSITO'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(38522435678962006177)
,p_prompt=>'Fecha Cheque:'
,p_source=>'to_char(sysdate, ''DD/MM/YYYY'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507831048225420640)
,p_name=>'P38_CRE_NRO_CHEQUE'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(38522435678962006177)
,p_prompt=>'Nro Cheque:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507831423957420640)
,p_name=>'P38_CRE_NRO_CUENTA'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(38522435678962006177)
,p_prompt=>'Nro Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507831842797420641)
,p_name=>'P38_CRE_NRO_PIN'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(38522435678962006177)
,p_prompt=>'Nro Pin:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507832821622422338)
,p_name=>'P38_TJ_ENTIDAD'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_prompt=>'Entidad'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_TARJETA_CREDITO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    ',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_PLAN_TARJETA_CORRIENTE'');',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507833203951422342)
,p_name=>'P38_TITULAR_TC'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Titular Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')  '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'LV_TITUTAR VARCHAR2(100);',
'BEGIN',
'SELECT PE.PER_PRIMER_NOMBRE || '' '' || PE.PER_PRIMER_APELLIDO',
'INTO LV_TITUTAR',
'  FROM ASDM_PERSONAS PE, ASDM_CLIENTES CL',
' WHERE PE.PER_ID = CL.PER_ID',
'   AND CL.CLI_ID = :P38_CLI_ID;',
'RETURN LV_TITUTAR;',
'EXCEPTION',
'WHEN OTHERS THEN',
'RETURN NULL;',
'END;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507833581334422342)
,p_name=>'P38_BANCOS'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_prompt=>'Banco:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.ede_descripcion l, a.ede_id r',
'  FROM asdm_entidades_destinos a',
' WHERE a.ede_tipo = ''EFI''',
'   AND a.ede_estado_registro = 0',
'   AND a.ede_es_induccion IS NULL',
'   AND a.emp_id = :F_EMP_ID',
'   AND EXISTS (SELECT NULL',
'          FROM asdm_entidades_destinos ed',
'         WHERE ed.ede_id_padre = a.ede_id',
'           AND ed.ede_tipo = ''TJ''',
'           AND ed.ede_estado_registro = 0',
'           AND ed.emp_id = :F_EMP_ID)'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.ede_descripcion,a.ede_id ',
'FROM asdm_entidades_destinos a ',
'WHERE a.ede_tipo=''EFI'' and a.emp_id = :f_emp_id',
'and ede_estado_registro=0;',
'--JALVAREZ 31 JUL 2022'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507834039299422342)
,p_name=>'P38_TARJETA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_prompt=>'Tarjeta:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.ede_descripcion d, d.ede_id r',
'  FROM asdm_entidades_destinos d',
' WHERE d.ede_id_padre = :P38_BANCOS',
'   AND d.ede_tipo = ''TJ''',
'   AND d.ede_estado_registro = 0',
'   AND d.emp_id = :f_emp_id;'))
,p_lov_cascade_parent_items=>'P38_BANCOS'
,p_ajax_items_to_submit=>'P38_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id FROM asdm_entidades_destinos b WHERE b.ede_tipo=''TJ''',
'AND b.ede_id_padre=:p38_bancos',
'and ede_estado_registro=0',
'and b.emp_id = :f_emp_id'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507834411952422342)
,p_name=>'P38_TIPO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_prompt=>'Tipo:'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion, c.ede_id',
'  FROM asdm_entidades_destinos c',
' WHERE c.ede_tipo = ''PTJ''',
'   AND c.ede_estado_registro = 0',
'   AND c.ede_id_padre = :p38_tarjeta',
'   AND c.emp_id = :f_emp_id;'))
,p_lov_cascade_parent_items=>'P38_TARJETA'
,p_ajax_items_to_submit=>'P38_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p38_tarjeta',
'and c.emp_id = :f_emp_id'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507834764859422342)
,p_name=>'P38_PLAN'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(43074146670640130)
,p_prompt=>'Plan:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT rpad(d.ede_plazo_plan, 2, '' '') || '' MESES, Gracia: '' ||',
unistr('       d.ede_meses_gracia || '' Comisi\00F3n: '' || d.ede_comision ||'),
unistr('       '' Inter\00E9s: '' || d.ede_interes d,'),
'       d.ede_id',
'  FROM asdm_entidades_destinos d',
' WHERE d.ede_id_padre = :P38_FORMA',
'   AND d.ede_tipo = ''DPT''',
'   AND d.ede_estado_registro = 0',
' ORDER BY d.ede_plazo_plan ASC;'))
,p_lov_cascade_parent_items=>'P38_FORMA'
,p_ajax_items_to_submit=>'P38_FORMA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_descripcion, ''->'') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_tede_detalle_plan_tarjeta'')',
' and ede.emp_id = :f_emp_id',
' and ede_estado_registro=0',
'START WITH ede.ede_id_padre =:P38_TIPO',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507835228538422343)
,p_name=>'P38_TJ_TITULAR_Y'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(38549183528310256356)
,p_prompt=>'Titular:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507835627689422343)
,p_name=>'P38_TJ_NUMERO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507835995861422343)
,p_name=>'P38_TJ_NUMERO_VOUCHER'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507836444227422343)
,p_name=>'P38_TJ_RECAP'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Recap'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507836813823422343)
,p_name=>'P38_LOTE'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507837205405422343)
,p_name=>'P38_MCD_NRO_AUT_REF'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(133252287146604431)
,p_prompt=>'Autorizaci&oacute;n:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507837915117424315)
,p_name=>'P38_MCD_NRO_COMPROBANTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(38522467515709173110)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Comprobante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>13
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507838332770424315)
,p_name=>'P38_MCD_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(38522467515709173110)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Deposito:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507839496062427395)
,p_name=>'P38_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_prompt=>'Valor Recibido:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_help_text=>'Presione Enter para cargar el valor'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
,p_item_comment=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGAR_TFP'');" '
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507840410492427398)
,p_name=>'P38_PRECIONEENTER'
,p_item_sequence=>36
,p_item_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_prompt=>'Presione Enter para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507840805359427399)
,p_name=>'P38_VUELTO'
,p_item_sequence=>46
,p_item_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_prompt=>'Vuelto/Cambio Efectivo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:20"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507842036707427401)
,p_name=>'P38_SALDO_CONDONACION'
,p_item_sequence=>56
,p_item_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_prompt=>'Saldo Condonacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(cc.ccc_saldo)',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p38_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p38_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38507842380099427402)
,p_name=>'P38_TOTAL_PAGO_CONDONACION'
,p_item_sequence=>66
,p_item_plug_id=>wwv_flow_imp.id(38522447713490035751)
,p_prompt=>'Total Pago Condonacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c016), 0)',
'',
'  fROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'   AND EXISTS',
' (select CC.CCC_ID',
'          from asdm_e.car_condonacion_cab cc, CAR_CONDONACION_DETALLE DE',
'         where cc.cli_id = :p38_cli_id',
'           and cc.ccc_estado_registro = 0',
'           and cc.ccc_saldo > 0',
'           AND CC.CCC_ID = DE.CCC_ID',
'           AND DE.CXC_ID = to_number(c007));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p38_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48423596815666614231)
,p_name=>'P38_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48423596908755614232)
,p_name=>'P38_NOMBRES'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(48423596959969614233)
,p_name=>'P38_VALOR_TOTAL_PAGOS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_prompt=>'TOTAL PAGAR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:24"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(49644569029047879572)
,p_name=>'P38_SEQ_ID_MOVCAJA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48423597506612614238)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63177038775524916274)
,p_name=>'P38_REGISTRA_RECIBO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(63177038579111916272)
,p_item_default=>'2'
,p_prompt=>'Registra recibo'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC:SI REGISTRAR;1,NO REGISTAR ;2'
,p_new_grid=>true
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63180815178908265829)
,p_name=>'P38_COBRADORES'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(63177038579111916272)
,p_prompt=>'Cobradores'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct  us.nombre NOMBRE,aa.aga_id',
'from kseg_e.kseg_usuarios us,',
'     asdm_e.asdm_agentes ag,',
'     asdm_e.asdm_agentes_agencias aa,',
'     asdm_e.asdm_unidades_gestion ug,',
'            asdm_e.asdm_agentes_tagentes t   ',
'where  t.uge_id=:f_uge_id',
' and t.ata_estado_registro=0 ',
' and t.tag_id=2 ',
'AND  us.usuario_id = ag.usu_id',
'      and ag.age_id = aa.age_id',
'      and ug.uge_id = aa.uge_id',
'      ',
'      and ag.age_estado_registro = 0',
'      and aa.aga_estado_registro = 0',
'      and ug.uge_estado_registro = 0',
'      --and aa.uge_id =:f_uge_id',
'          and aa.age_id=t.age_id'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P38_REGISTRA_RECIBO'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63180997036812712876)
,p_name=>'P38_TIPO_FOLIO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(63177038579111916272)
,p_prompt=>'Tipo recibo'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'select t.ttc_descripcion, t.ttc_id from asdm_e.asdm_tipos_transacciones_caja t where t.ttc_estado_registro=0 and t.emp_id=1;'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P38_REGISTRA_RECIBO'
,p_display_when2=>'1'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63182461058890523129)
,p_name=>'P38_CXC_ID_REF'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_prompt=>'Cxc id ref'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63184448985570115470)
,p_name=>'P38_SECUENCIA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(63177038579111916272)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Secuencia'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63184449069530115471)
,p_name=>'P38_RAZON'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(63184449183010115472)
,p_prompt=>'Razon'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cHeight=>5
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(63184449563985115476)
,p_name=>'P38_SECUENCIA_1'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(63177038579111916272)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT * FROM (',
'      select c.rec_id from asdm_e.asdm_folios_caja a , asdm_e.asdm_folios_block b, asdm_e.asdm_folios_block_det c',
'      where ',
'      --a.uge_id=:F_UGE_ID',
'      a.fca_estado=''R''',
'      AND A.FCA_ESTADO_REGISTRO=''0''',
'      AND B.FCA_ID=A.FCA_ID',
'      AND B.FCA_ESTATUS=''ENT''',
'      AND B.FCA_ESTADO_REGISTRO=''0''',
'--      AND B.COD_AGENTE=231',
'      AND B.FCA_FHASTA=TO_DATE(''31/12/2999'',''DD/MM/YYYY'')',
'      AND C.FCA_ID=B.FCA_ID',
'      AND C.BLK_ID=B.BLK_ID',
'      AND C.FCA_ESTADO_REGISTRO=''0''',
'      AND C.FCA_FHASTA=TO_DATE(''31/12/2999'',''DD/MM/YYYY'')',
'      AND C.COD_AGENTE=:P38_COBRADORES',
'      AND C.FCA_ESTATUS=''PAG'' ',
'      AND A.TTC_ID=:P38_TIPO_FOLIO     ',
'      ORDER BY C.FCA_SECUENCIA ASC)v1',
'      WHERE ROWNUM<2',
'      ;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(149553471509979157068)
,p_name=>'P38_VALOR_PAGO_DESGRAVAMEN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48423596727424614230)
,p_prompt=>'Valor Pago Desgravamen'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177343053510144707)
,p_validation_name=>'EDE_ID'
,p_validation_sequence=>10
,p_validation=>'P38_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione Una Entidad'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:REQUEST = ''CARGAR_TFP'') ',
'AND ((:P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')) or (:P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito''))',
'    or (:P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))',
'    )'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177343366008146513)
,p_validation_name=>'CRE_TITULAR_CUENTA'
,p_validation_sequence=>20
,p_validation=>'P38_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la cuenta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177343924077152651)
,p_validation_name=>'CRE_NRO_CHEQUE'
,p_validation_sequence=>30
,p_validation=>'P38_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cheque'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177344170598154550)
,p_validation_name=>'CRE_NRO_CUENTA'
,p_validation_sequence=>40
,p_validation=>'P38_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cuenta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177344476962156011)
,p_validation_name=>'CRE_NRO_PIN'
,p_validation_sequence=>50
,p_validation=>'P38_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el PIN'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177344816381158059)
,p_validation_name=>'MCD_NRO_COMPROBANTE'
,p_validation_sequence=>60
,p_validation=>'P38_MCD_NRO_COMPROBANTE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00FAmero de comprobante de dep\00F3sito o transferencia')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''CARGAR_TFP''',
'AND (:P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177345148016160126)
,p_validation_name=>'PLAN'
,p_validation_sequence=>70
,p_validation=>'P38_PLAN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe Seleccionar el Plan'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177345483517161383)
,p_validation_name=>'TITULAR_TC'
,p_validation_sequence=>80
,p_validation=>'P38_TITULAR_TC'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el titular de la Tarjeta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177034184118916228)
,p_validation_name=>'BANCOS'
,p_validation_sequence=>90
,p_validation=>'P38_BANCOS'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el Banco'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177034324794916229)
,p_validation_name=>'TARJETA'
,p_validation_sequence=>100
,p_validation=>'P38_TARJETA_EMI'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar la Tarjeta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P38_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177346810794171688)
,p_validation_name=>'MCD_FECHA_DEPOSITO'
,p_validation_sequence=>150
,p_validation=>'P38_MCD_FECHA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la fecha de deposito'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''CARGAR_TFP''',
'AND (:P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(63177347179505173174)
,p_validation_name=>'p38_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>160
,p_validation=>'trunc(to_date(:P38_MCD_FECHA_DEPOSITO)) <= trunc(sysdate)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Fecha deposito no puede ser mayor a la fecha actual'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P38_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38525610943249238528)
,p_name=>'ad_elimina'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.elimina'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38525610954655238529)
,p_event_id=>wwv_flow_imp.id(38525610943249238528)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'apex_collection.delete_member(''CO_MOV_CAJA'', :P38_SEQ_ID_MOVCAJA);'
,p_attribute_02=>'P38_SEQ_ID_MOVCAJA'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38525611568585238535)
,p_name=>'pr_grabar'
,p_event_sequence=>20
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(38525611224867238531)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38525611734569238536)
,p_event_id=>wwv_flow_imp.id(38525611568585238535)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'GRABAR'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(63180815042020265827)
,p_name=>'Refrescar_Recibos'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_REGISTRA_RECIBO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(63180815096708265828)
,p_event_id=>wwv_flow_imp.id(63180815042020265827)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(63180815295018265830)
,p_name=>'Refresca_tabla'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_COBRADORES'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(63180815417499265831)
,p_event_id=>wwv_flow_imp.id(63180815295018265830)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(63181130515834618627)
,p_name=>'Refresca_Tipo'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_TIPO_FOLIO'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(63181130555246618628)
,p_event_id=>wwv_flow_imp.id(63181130515834618627)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(42757335268951971)
,p_name=>'da_procesar_pago'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_PROCESO_TJ'
,p_condition_element=>'P38_PROCESO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'PP'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43074459798640134)
,p_event_id=>wwv_flow_imp.id(42757335268951971)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TRAMA_RESP'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43075461112640144)
,p_event_id=>wwv_flow_imp.id(42757335268951971)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(42754952823951948)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43074613161640135)
,p_event_id=>wwv_flow_imp.id(42757335268951971)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(42753658437951935)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43073902400640128)
,p_event_id=>wwv_flow_imp.id(42757335268951971)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43074146670640130)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(42757512049951973)
,p_name=>'da_consutla_tarjeta'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_PROCESO_TJ'
,p_condition_element=>'P38_PROCESO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'CT'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43075558537640145)
,p_event_id=>wwv_flow_imp.id(42757512049951973)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TRAMA_RESP'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43073846844640127)
,p_event_id=>wwv_flow_imp.id(42757512049951973)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43074146670640130)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43074263486640132)
,p_event_id=>wwv_flow_imp.id(42757512049951973)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(42753658437951935)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43074161935640131)
,p_event_id=>wwv_flow_imp.id(42757512049951973)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(42754952823951948)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(42757656626951975)
,p_name=>'da_lectura_tarjeta'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_PROCESO_TJ'
,p_condition_element=>'P38_PROCESO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'LT'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43075693068640146)
,p_event_id=>wwv_flow_imp.id(42757656626951975)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TRAMA_RESP'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43074411307640133)
,p_event_id=>wwv_flow_imp.id(42757656626951975)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(42754952823951948)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43073976527640129)
,p_event_id=>wwv_flow_imp.id(42757656626951975)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(43074146670640130)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43074740598640136)
,p_event_id=>wwv_flow_imp.id(42757656626951975)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(42753658437951935)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43075103789640140)
,p_name=>'ad_automatico'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_TIPO_PAGO_TJ'
,p_condition_element=>'P38_TIPO_PAGO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'A'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43075226936640141)
,p_event_id=>wwv_flow_imp.id(43075103789640140)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TJ_NUMERO,P38_TJ_NUMERO_VOUCHER,P38_TJ_RECAP,P38_MCD_NRO_AUT_REF'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(133253321426604441)
,p_event_id=>wwv_flow_imp.id(43075103789640140)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TJ_NUMERO_1,P38_TJ_NUMERO_VOUCHER_1,P38_TJ_RECAP_1,P38_MCD_NRO_AUT_REF_1,P38_PROCESO_TJ,P38_PINPAD'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(146213519453150764)
,p_event_id=>wwv_flow_imp.id(43075103789640140)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         VARCHAR2(250);',
'  ln_rpr_id           NUMBER;',
'  lv_mensaje          VARCHAR2(1000);',
'BEGIN',
'  BEGIN',
'    SELECT rpp.rpp_id',
'      INTO :p38_pinpad',
'      FROM car_redes_pago_pue rpp',
'     WHERE rpp.pue_id = :p0_pue_id',
'       AND rpp.rpp_estado_registro = 0;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      :p38_pinpad := NULL;',
'  END;',
'  IF :p38_pinpad IS NOT NULL THEN',
'    -- Call the procedure                                    ',
'    pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id                 => :p38_pinpad,',
'                                                   pn_cli_id                 => :p38_cli_id,',
'                                                   pn_uge_id                 => :f_uge_id,',
'                                                   pn_emp_id                 => :f_emp_id,',
'                                                   pn_pue_id                 => :p0_pue_id,',
'                                                   pv_rpr_tipo_requerimiento => ''LT'',',
'                                                   pn_rpc_id                 => :p38_rpc_id,',
'                                                   pn_rpr_id_req             => :p38_rpr_id_req,',
'                                                   pv_trama                  => lv_data_set,',
'                                                   pn_ede_id                 => NULL,',
'                                                   pv_error                  => :p0_error);',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'      pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id             => :p38_pinpad,',
'                                              pn_pue_id             => :p0_pue_id,',
'                                              pn_cli_id             => :p38_cli_id,',
'                                              pv_tipo_requerimiento => ''LT'',',
'                                              pv_trama_req          => lv_data_set,',
'                                              pv_trama_res          => :p38_trama_resp,',
'                                              pn_rpc_id             => :p38_rpc_id,',
'                                              pn_rpr_id_req         => :p38_rpr_id_req,',
'                                              pn_rpr_id_res         => :p38_rpr_id,',
'                                              pc_error              => pc_error);',
'',
'      IF pc_error IS NOT NULL THEN',
'        raise_application_error(-20000, pc_error);',
'        :p0_error := pc_error;',
'      END IF;',
'    END IF;',
'    pq_car_redes_pago.pr_retorna_datos_lectura(pn_rpr_id_req => :p38_rpr_id,',
'                                               pn_ede_id     => :p38_bancos);',
'  END IF;',
'END;',
''))
,p_attribute_02=>'P38_RED,P38_CLI_ID,F_UGE_ID,F_EMP_ID,P0_PUE_ID,P38_RPC_ID,P38_RPR_ID_REQ,P0_ERROR,P38_RPR_ID,P38_NRO_PINPAD'
,p_attribute_03=>'P38_RPC_ID,P38_RPR_ID_REQ,P0_ERROR,P38_TRAMA_RESP,P38_RPR_ID,P38_BANCOS,P38_PINPAD,P0_ERROR'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(43075300051640142)
,p_name=>'ad_manual'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_TIPO_PAGO_TJ'
,p_condition_element=>'P38_TIPO_PAGO_TJ'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'M'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(43075400049640143)
,p_event_id=>wwv_flow_imp.id(43075300051640142)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TJ_NUMERO,P38_TJ_NUMERO_VOUCHER,P38_TJ_RECAP,P38_MCD_NRO_AUT_REF'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(133253413497604442)
,p_event_id=>wwv_flow_imp.id(43075300051640142)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P38_TJ_NUMERO_1,P38_TJ_NUMERO_VOUCHER_1,P38_TJ_RECAP_1,P38_MCD_NRO_AUT_REF_1,P38_PROCESO_TJ,P38_PINPAD'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(187974111375354733)
,p_name=>'ad_pinpad'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_PINPAD'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(187974255197354735)
,p_event_id=>wwv_flow_imp.id(187974111375354733)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         VARCHAR2(250);',
'  ln_rpr_id           NUMBER;',
'',
'BEGIN',
'',
'',
'  -- Call the procedure                                    ',
'  pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id                 => :p38_pinpad,',
'                                                 pn_cli_id                 => :p38_cli_id,',
'                                                 pn_uge_id                 => :f_uge_id,',
'                                                 pn_emp_id                 => :f_emp_id,',
'                                                 pn_pue_id                 => :p0_pue_id,',
'                                                 pv_rpr_tipo_requerimiento => ''LT'',',
'                                                 pn_rpc_id                 => :p38_rpc_id,',
'                                                 pn_rpr_id_req             => :p38_rpr_id_req,',
'                                                 pv_trama                  => lv_data_set,',
'                                                 pn_ede_id                 => NULL,',
'                                                 pv_error                  => :p0_error);',
'',
'  IF :p0_error IS NULL THEN',
'  ',
'    pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id             => :p38_pinpad,',
'                                            pn_pue_id             => :p0_pue_id,',
'                                            pn_cli_id             => :p38_cli_id,',
'                                            pv_tipo_requerimiento => ''LT'',',
'                                            pv_trama_req          => lv_data_set,',
'                                            pv_trama_res          => :p38_trama_resp,',
'                                            pn_rpc_id             => :p38_rpc_id,',
'                                            pn_rpr_id_req         => :p38_rpr_id_req,',
'                                            pn_rpr_id_res         => :p38_rpr_id,',
'                                            pc_error              => pc_error);',
'  ',
'    IF pc_error IS NOT NULL THEN',
'      raise_application_error(-20000, pc_error);',
'      :p0_error := pc_error;',
'    END IF;',
'  END IF;',
'  pq_car_redes_pago.pr_retorna_datos_lectura(pn_rpr_id_req => :p38_rpr_id,',
'                                             pn_ede_id     => :p38_bancos);',
'',
'END;',
''))
,p_attribute_02=>'P38_RED,P38_CLI_ID,F_UGE_ID,F_EMP_ID,P0_PUE_ID,P38_RPC_ID,P38_RPR_ID_REQ,P0_ERROR,P38_RPR_ID,P38_NRO_PINPAD,P38_PINPAD'
,p_attribute_03=>'P38_RPC_ID,P38_RPR_ID_REQ,P0_ERROR,P38_TRAMA_RESP,P38_RPR_ID,P38_BANCOS,P38_PINPAD'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(785426095360983427)
,p_name=>'AD_ENTER'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_MCD_VALOR_MOVIMIENTO'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'this.browserEvent.keyCode == 13'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(785426257501983429)
,p_event_id=>wwv_flow_imp.id(785426095360983427)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'lc_error CLOB;',
'BEGIN',
'pq_car_pago_cuota.pr_cargar_forma_pago(pn_tfp_id               => :p38_tfp_id,',
'pn_emp_id               => :f_emp_id,',
'pv_tipo_pago_tj         => :p38_tipo_pago_tj,',
'pv_tj_numero            => :p38_tj_numero,',
'pv_tj_numero_voucher    => :p38_tj_numero_voucher,',
'pn_mcd_nro_aut_ref      => :p38_mcd_nro_aut_ref,',
'pn_ede_id               => :p38_ede_id,',
'pn_plan                 => :p38_plan,',
'pv_cre_titular_cuenta   => :p38_cre_titular_cuenta,',
'pv_titular_tc           => :p38_titular_tc,',
'pn_pinpad               => :p38_pinpad,',
'pn_pue_id               => :p0_pue_id,',
'pn_red                  => :p38_red,',
'pn_cli_id               => :p38_cli_id,',
'pn_uge_id               => :f_uge_id,',
'pn_user_id              => :f_user_id,',
'pn_mcd_valor_movimiento => :p38_mcd_valor_movimiento,',
'pv_tipo_impresion       => :p38_tipo_impresion,',
'pn_rpc_id               => :p38_rpc_id,',
'pv_tarjeta_emi          => :p38_tarjeta_emi,',
'pv_tj_recap             => :p38_tj_recap,',
'pv_tj_numero_1          => :p38_tj_numero_1,',
'pv_tj_numero_voucher_1  => :p38_tj_numero_voucher_1,',
'pv_tj_recap_1           => :p38_tj_recap_1,',
'pn_mcd_nro_aut_ref_1    => :p38_mcd_nro_aut_ref_1,',
'pv_lote                 => :p38_lote,',
'pv_mcd_nro_comprobante  => :p38_mcd_nro_comprobante,',
'pn_comision             => :p38_comision,',
'pn_vuelto               => :p38_vuelto,',
'pn_cre_id               => :p38_cre_id,',
'pn_rap_nro_retencion    => :p38_rap_nro_retencion,',
'pn_cre_nro_cheque       => :p38_cre_nro_cheque,',
'pv_cre_nro_cuenta       => :p38_cre_nro_cuenta,',
'pv_cre_nro_pin          => :p38_cre_nro_pin,',
'pn_saldo_retencion      => :p38_saldo_retencion,',
'pd_rap_fecha            => :p38_rap_fecha,',
'pd_rap_fecha_validez    => :p38_rap_fecha_validez,',
'pn_rap_nro_autorizacion => :p38_rap_nro_autorizacion,',
'pn_rap_nro_establ_ret   => :p38_rap_nro_establ_ret,',
'pn_rap_nro_pemision_ret => :p38_rap_nro_pemision_ret,',
'pn_pre_id               => :p38_pre_id,',
'pd_mcd_fecha_deposito   => :p38_mcd_fecha_deposito,',
'pn_valor_total_pagos    => :p38_valor_total_pagos,',
'pn_seq_id               => :p38_seq_id,',
'pd_cre_fecha_deposito   => :p38_cre_fecha_deposito,',
'pn_tj_entidad           => :p38_tj_entidad,',
'pn_bancos               => :p38_bancos,',
'pn_tarjeta              => :p38_tarjeta,',
'pn_tipo                 => :p38_tipo,',
'pv_modificacion         => :p38_modificacion,',
'pn_rap_com_id           => :p38_rap_com_id,',
'pn_seq_id_movcaja       => :p38_seq_id_movcaja,',
'pv_error                => lc_error);',
'  IF lc_error IS NOT NULL THEN',
'    /*apex_error.add_error(p_message          => lc_error,',
'                         p_display_location => apex_error.c_inline_in_notification);*/',
'    :P38_ERROR_ANTICIPO := lc_error;                     ',
'    --raise_application_error(-20000,lc_error);',
'  ELSE',
'    :P38_ERROR_ANTICIPO := null;',
'  END IF;',
'END;',
''))
,p_attribute_02=>'P38_TFP_ID,F_EMP_ID,P38_TIPO_PAGO_TJ,P38_TJ_NUMERO,P38_TJ_NUMERO_VOUCHER,P38_MCD_NRO_AUT_REF,P38_EDE_ID,P38_PLAN,P38_CRE_TITULAR_CUENTA,P38_TITULAR_TC,P38_PINPAD,P0_PUE_ID,P38_RED,P38_CLI_ID,F_UGE_ID,F_USER_ID,P38_MCD_VALOR_MOVIMIENTO,P38_TIPO_IMPRES'
||'ION,P38_RPC_ID,P38_TARJETA_EMI,P38_TJ_RECAP,P38_TJ_NUMERO_1,P38_TJ_NUMERO_VOUCHER_1,P38_TJ_RECAP_1,P38_MCD_NRO_AUT_REF_1,P38_LOTE,P38_MCD_NRO_COMPROBANTE,P38_COMISION,P38_VUELTO,P38_CRE_ID,P38_RAP_NRO_RETENCION,P38_CRE_NRO_CHEQUE,P38_CRE_NRO_CUENTA,P'
||'38_CRE_NRO_PIN,P38_SALDO_RETENCION,P38_RAP_FECHA,P38_RAP_FECHA_VALIDEZ,P38_RAP_NRO_AUTORIZACION,P38_RAP_NRO_ESTABL_RET,P38_RAP_NRO_PEMISION_RET,P38_PRE_ID,P38_MCD_FECHA_DEPOSITO,P38_VALOR_TOTAL_PAGOS,P38_SEQ_ID,P38_CRE_FECHA_DEPOSITO,P38_TJ_ENTIDAD,P'
||'38_BANCOS,P38_TARJETA,P38_TIPO,P38_MODIFICACION,P38_RAP_COM_ID,P38_SEQ_ID_MOVCAJA,P0_ERROR,P38_ERROR_ANTICIPO'
,p_attribute_03=>'P38_TFP_ID,F_EMP_ID,P38_TIPO_PAGO_TJ,P38_EDE_ID,P38_PLAN,P38_CRE_TITULAR_CUENTA,P38_TITULAR_TC,P38_PINPAD,P0_PUE_ID,P38_RED,P38_CLI_ID,F_UGE_ID,F_USER_ID,P38_MCD_VALOR_MOVIMIENTO,P38_TIPO_IMPRESION,P38_RPC_ID,P38_TARJETA_EMI,P38_MCD_NRO_AUT_REF_1,P38'
||'_LOTE,P38_MCD_NRO_COMPROBANTE,P38_COMISION,P38_VUELTO,P38_CRE_ID,P38_RAP_NRO_RETENCION,P38_CRE_NRO_CHEQUE,P38_CRE_NRO_CUENTA,P38_CRE_NRO_PIN,P38_SALDO_RETENCION,P38_RAP_FECHA,P38_RAP_FECHA_VALIDEZ,P38_RAP_NRO_AUTORIZACION,P38_RAP_NRO_ESTABL_RET,P38_R'
||'AP_NRO_PEMISION_RET,P38_PRE_ID,P38_MCD_FECHA_DEPOSITO,P38_VALOR_TOTAL_PAGOS,P38_SEQ_ID,P38_CRE_FECHA_DEPOSITO,P38_TJ_ENTIDAD,P38_BANCOS,P38_TARJETA,P38_TIPO,P38_MODIFICACION,P38_RAP_COM_ID,P38_SEQ_ID_MOVCAJA,P0_ERROR,P38_ERROR_ANTICIPO'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(785426249191983428)
,p_event_id=>wwv_flow_imp.id(785426095360983427)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'CARGAR_TFP'
,p_attribute_02=>'Y'
,p_server_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(857574683456138395)
,p_name=>'da_error-anticipo'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P38_ERROR_ANTICIPO'
,p_condition_element=>'P38_ERROR_ANTICIPO'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(857574825111138396)
,p_event_id=>wwv_flow_imp.id(857574683456138395)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var error;',
'error = $v(''P38_ERROR_ANTICIPO'')',
'showMessage(error, ''warning'');'))
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(857574908367138397)
,p_event_id=>wwv_flow_imp.id(857574683456138395)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'CARGAR_TFP'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(48423597169148614235)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  CURSOR cur_cuotas IS',
'    SELECT p.div_id, p.div_valor_pago, d.div_saldo_Cuota',
'      FROM car_cuotas_pagar p, car_dividendos d',
'     WHERE p.cli_id = :p38_cli_id',
'       AND p.usu_id = :f_user_id',
'       and d.div_id = p.div_id;',
'',
'BEGIN',
'  for reg in cur_cuotas loop',
'  ',
'    if reg.div_valor_pago <= 0 then',
'      raise_application_error(-20000,''Minimo se debe aplicar 0.01 ctv a la cuota a pagar'');',
'    end if;',
'    pq_car_pago_cuota_dml.pr_ins_div_control(reg.div_id,:f_user_id);',
'    if reg.div_valor_pago > reg.div_saldo_cuota then',
'      raise_application_error(-20000,''Existen diferencias entre los saldos del cliente'');',
'    end if;',
'  end loop;',
'  ',
'  select cpc_total_pago + cpc_total_mora + cpc_total_cobranza+cpc_seg_desgravamen, cpc_seg_desgravamen',
'  into :P38_VALOR_TOTAL_PAGOS, :P38_VALOR_PAGO_DESGRAVAMEN',
'  from car_cuotas_pagar_cab',
'  where cli_id = :p38_cli_id',
'  and usu_id = :f_user_id;',
'  ',
'if :p38_saldo_anticipos > 0 then',
':p38_tfp_id := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes'');',
'else',
':p38_tfp_id := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
'end if;',
'',
':p38_seq_id               := NULL;',
':p38_cre_id               := NULL;',
':p38_cre_fecha_deposito   := SYSDATE;',
':p38_ede_id               := NULL;',
':p38_cre_titular_cuenta   := NULL;',
':p38_cre_nro_cheque       := NULL;',
':p38_cre_nro_cuenta       := NULL;',
':p38_cre_nro_pin          := NULL;',
':p38_mcd_valor_movimiento := NULL;',
'',
':p38_MCD_FECHA_DEPOSITO := NULL;',
':p38_TJ_ENTIDAD         := NULL;',
':p38_BANCOS             := NULL;',
':p38_TARJETA            := NULL;',
':p38_TIPO               := NULL;',
':p38_PLAN               := NULL;        ',
':p38_TJ_NUMERO           := NULL;',
':p38_TJ_NUMERO_VOUCHER   := NULL;',
':p38_TJ_RECAP            := NULL;',
':p38_LOTE                := NULL;',
':p38_MCD_NRO_AUT_REF     := NULL;',
':p38_MCD_NRO_COMPROBANTE := NULL;',
':p38_MODIFICACION        := NULL;',
':p38_MCD_FECHA_DEPOSITO  := NULL;',
'',
':p38_RAP_NRO_RETENCION    := NULL;',
':p38_RAP_NRO_AUTORIZACION := NULL;',
':p38_RAP_FECHA            := NULL;',
':p38_RAP_FECHA_VALIDEZ    := NULL;',
':p38_RAP_COM_ID           := NULL;',
':p38_SALDO_RETENCION      := NULl;',
':p38_RAP_NRO_ESTABL_RET   := NULL;',
':p38_RAP_NRO_PEMISION_RET := NULL;',
':p38_PRE_ID               := NULL;',
':p38_vuelto               := NULL;',
'    ',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'PAGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>48391344017878849309
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38525610817622238527)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_seq_id NUMBER;',
'BEGIN',
'',
'  SELECT seq_id',
'    INTO ln_seq_id',
'    FROM apex_collections',
'   WHERE collection_name = ''CO_MOV_CAJA''',
'     AND seq_id = :p38_seq_id_movcaja;',
'  IF ln_seq_id > 0 THEN',
'    apex_collection.delete_member(''CO_MOV_CAJA'', :p38_seq_id_movcaja);',
'  END IF;',
'  :p38_vuelto               := NULL;',
'  :p38_mcd_valor_movimiento := NULL;',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ELIMINA_MOVCAJA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38493357666352473601
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38525611251726238532)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_mca_id                   asdm_movimientos_caja.mca_id%TYPE;',
'  ln_age_id_agente            asdm_agentes.age_id%TYPE;',
'  ln_ttr_id_pago              asdm_tipos_transacciones.ttr_id%TYPE;',
'  ln_ttr_id_car               asdm_tipos_transacciones.ttr_id%TYPE;',
'  ln_diferencia_saldo_tarjeta NUMBER;',
'',
'  lc_url     CLOB;',
'  lc_err_pag CLOB;',
'  lv_bge_origen VARCHAR2(10) := ''RPX'';',
'  lv_bge_canal  VARCHAR2(20) := ''PAGO_CUOTA'';',
'BEGIN',
'',
'  BEGIN',
'    SELECT pq_car_redes_pago.fn_retorna_saldo_tarjeta(pn_cli_id => :p38_cli_id,',
'                                                      pn_uge_id => :f_uge_id) -',
'           to_number(SUM(nvl(c006, 0))) valor',
'      INTO ln_diferencia_saldo_tarjeta',
'      FROM apex_collections',
'     WHERE collection_name =',
'           pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_mov_caja'')',
'       AND to_number(c005) =',
'           asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                     ''cn_tfp_id_tarjeta_credito'');',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      ln_diferencia_saldo_tarjeta := 0;',
'  END;',
'',
'  ----INICIO: R92: VPROANO 9/JULIO/2019 VALIDACION ',
'',
'  IF nvl(ln_diferencia_saldo_tarjeta, 0) <= 0 THEN',
'  ',
'    IF :p38_registra_recibo = ''1'' THEN',
'      --RAISE_APPLICATION_ERROR(-20000,''ENTRA A GRABAR EL PAGO DE CUOTA:   ''|| :P38_CLI_ID ||''   :::   ''|| :P38_VALOR_TOTAL_PAGOS ||''   :::   '');',
'    ',
'      pq_car_recibo_cobro_manual.pr_graba_pago_recibo(:f_uge_id,',
'                                                      :p38_secuencia_1,',
'                                                      ''PAGO DE CUOTA'',',
'                                                      :p38_cli_id,',
'                                                      :p38_cre_id,',
'                                                      :p38_valor_total_pagos,',
'                                                      :p0_error);',
'    END IF;',
'  ',
'    ln_age_id_agente := pq_car_credito_interfaz.fn_retorna_agente_conectado(:f_user_id,',
'                                                                            :f_emp_id);',
'  ',
'    ln_ttr_id_pago := pq_constantes.fn_retorna_constante(0,',
'                                                         ''cn_ttr_id_pago_cuota'');',
'    ln_ttr_id_car  := pq_constantes.fn_retorna_constante(0,',
'                                                         ''cn_ttr_id_credito_car_pago_cuota'');',
'  ',
'    pq_car_pago_cuota.pr_graba_pago_cuota(pn_mca_id                => :p38_mca_id_out,',
'                                          pn_emp_id                => :f_emp_id,',
'                                          pn_uge_id                => :f_uge_id,',
'                                          pn_uge_id_gasto          => :f_uge_id_gasto,',
'                                          pn_user_id               => :f_user_id,',
'                                          pn_pca_id                => :f_pca_id,',
'                                          pn_age_id_gestionado_por => ln_age_id_agente,',
'                                          pn_cli_id                => :p38_cli_id,',
'                                          pn_mca_total             => :p38_valor_total_pagos,',
'                                          pn_tse_id                => :f_seg_id,',
'                                          pn_age_id_agente         => ln_age_id_agente,',
'                                          pn_age_id_agencia        => :f_age_id_agencia,',
'                                          pn_ttr_id_origen         => ln_ttr_id_pago,',
'                                          pn_ttr_id_cartera        => ln_ttr_id_car,',
'                                          pn_mca_id_reversar       => NULL,',
'                                          pn_cxc_id_ref            => :p38_cxc_id_ref,',
'                                          pn_bge_origen            => lv_bge_origen,',
'                                          pn_bge_canal             => lv_bge_canal,',
'                                          pv_error                 => lc_err_pag);',
'  ',
'    pq_car_pago_cuota.pr_cargar_datos_bitacora(pn_emp_id     => :f_emp_id,',
'                                                     pn_bge_origen => lv_bge_origen,',
'                                                     pn_bge_canal  => lv_bge_canal,',
'                                                     pn_cli_id     => :p38_cli_id,',
'                                                     pn_mca_id     => :p38_mca_id_out,',
'                                                     pv_error      => lc_err_pag);',
'',
'    IF lc_err_pag IS NOT NULL THEN',
'      raise_application_error(-20000,',
'                              ''Error pago de cuota: '' || lc_err_pag);',
'    END IF;',
'  ',
'    --- Null los campos ---',
'  ',
'    :p38_seq_id               := NULL;',
'    :p38_cre_id               := NULL;',
'    :p38_cre_fecha_deposito   := SYSDATE;',
'    :p38_ede_id               := NULL;',
'    :p38_cre_titular_cuenta   := NULL;',
'    :p38_cre_nro_cheque       := NULL;',
'    :p38_cre_nro_cuenta       := NULL;',
'    :p38_cre_nro_pin          := NULL;',
'    :p38_mcd_valor_movimiento := NULL;',
'  ',
'    :p38_mcd_fecha_deposito   := NULL;',
'    :p38_tj_entidad           := NULL;',
'    :p38_bancos               := NULL;',
'    :p38_tarjeta              := NULL;',
'    :p38_tipo                 := NULL;',
'    :p38_tipo_1               := NULL;',
'    :p38_plan                 := NULL;',
'    :p38_tj_numero            := NULL;',
'    :p38_tj_numero_voucher    := NULL;',
'    :p38_tj_recap             := NULL;',
'    :p38_lote                 := NULL;',
'    :p38_mcd_nro_aut_ref      := NULL;',
'    :p38_mcd_nro_comprobante  := NULL;',
'    :p38_modificacion         := NULL;',
'    :p38_mcd_fecha_deposito   := NULL;',
'    :p38_tarjeta_emi          := NULL;',
'    :p38_rap_nro_retencion    := NULL;',
'    :p38_rap_nro_autorizacion := NULL;',
'    :p38_rap_fecha            := NULL;',
'    :p38_rap_fecha_validez    := NULL;',
'    :p38_rap_com_id           := NULL;',
'    :p38_saldo_retencion      := NULL;',
'    :p38_rap_nro_establ_ret   := NULL;',
'    :p38_rap_nro_pemision_ret := NULL;',
'    :p38_pre_id               := NULL;',
'    :p38_seq_id_movcaja       := NULL;',
'    :p38_mcd_valor_movimiento := NULL;',
'    :p38_cxc_id_ref           := NULL;',
'    :p38_tfp_id               := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_tfp_id_efectivo'');',
'  ELSE',
'  ',
'    :p0_error := ''DEBE CONSUMIR TODO EL SALDO DE LOS PAGOS DE TARJETAS '' ||',
'                 ln_diferencia_saldo_tarjeta;',
'    raise_application_error(-20000, :p0_error);',
'  END IF;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(38525611224867238531)
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38493358100456473606
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(854037915785429863)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_go_url'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pr_url(pn_app_destino => 211,',
'           pn_pag_destino => 19,',
'           pv_request     => ''imprimir'',',
'           pv_parametros  => ''F_PARAMETRO,P19_MCA_ID'',',
'           pv_valores     => :f_parametro || '','' || :p38_mca_id_out,',
'           pv_evento      => ''LOAD'');'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':REQUEST = ''GRABAR'' and :p38_mca_id_out is not null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>821784764515664937
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133252461001604433)
,p_process_sequence=>50
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_retorna_datos_rpagos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  /* Creacion: 20-jul-2022',
'  Autor: John Alvarez',
unistr('  Descripcion: Procedimiento para repurar la informaci\00F3n de un proceso de pago procesado'),
'  Historia: */',
'BEGIN',
'',
'  -- Call the procedure',
'  pq_car_redes_pago.pr_carga_datos_tarjeta(pn_emp_id => :f_emp_id,',
'                                           pn_vueltos => :p38_vueltos,',
'                                           pn_cli_id => :p38_cli_id,',
'                                           pn_uge_id => :f_uge_id,',
'                                           pn_rpr_id => :p38_rpr_id,',
'                                           pn_rpc_id => :p38_rpc_id,',
'                                           pv_error => :p0_error);',
'',
'',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    NULL;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'UTILIZAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>100999309731839507
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133255195615604460)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reveso_pago_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pv_error VARCHAR2(1000);',
'BEGIN',
'  pq_car_redes_pago.pr_requerimiento_reverso_pago(pn_pue_id     => :p0_pue_id,',
'                                                  pn_cli_id     => :p38_cli_id,',
'                                                  pn_rpc_id     => :p38_rpc_id,',
'                                                  pn_rpr_id_req => :p38_rpr_id_req,',
'                                                  pv_tipo       => ''A'',',
'                                                  pv_error      => pv_error);',
'  IF pv_error IS NULL THEN',
'    apex_collection.delete_member(''CO_MOV_CAJA'', :p38_seq_id_movcaja);',
'  ELSE',
'    :p0_error := pv_error;',
'  END IF;',
'   :p38_rpc_id:=null;                                                 ',
' :p38_rpr_id_req :=null;  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'reverso_tarjeta'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101002044345839534
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133256601088604474)
,p_process_sequence=>70
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_VOUCHER'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_car_redes_pago.pr_imprimir_voucher(pn_rpc_id => :p38_rpc_id,',
'                                        pn_emp_id => :F_emp_id,',
'                                        pv_tipo => :P38_TIPO_IMPRESION,',
'                                        pv_error => :p0_error);',
' :P38_TIPO_IMPRESION:=null;',
'  :p38_rpc_id:=null;                                                 ',
' :p38_rpr_id_req :=null;  ',
''))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>101003449818839548
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38507885938267905079)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
'  lv_mensaje          VARCHAR2(1000) := NULL;',
'  ln_rpr_id           NUMBER;',
'',
'BEGIN',
'',
'  /*Agregado por Andres Calle',
'  07/septiembre/2011',
'  Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
unistr('  JALVAREZ 05SEP2022  Req: MPO-3 Integraci\00F3n Medianet - AJUSTE EN PANTALLA DE PAGO DE CUOTA. SE AUMENTA PARA GUARDAR EL NRO DE RECAP'),
'  */',
' DELETE FROM JEA;',
' INSERT INTO JEA VALUES (2225,'':p38_tfp_id  ''||:p38_tfp_id ||'' :p38_mcd_valor_movimiento ''||:p38_mcd_valor_movimiento,NULL);',
' COMMIT;',
'  IF :p38_tfp_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tfp_id_tarjeta_credito'') THEN',
'  ',
'    IF :p38_tipo_pago_tj = ''M'' THEN',
'      IF :p38_tj_numero IS NULL THEN',
unistr('        lv_mensaje := ''El n\00FAmero de tarjeta esta en blanco'';'),
'      END IF;',
'    ',
'      IF :p38_tj_numero_voucher IS NULL THEN',
'        IF lv_mensaje IS NULL THEN',
unistr('          lv_mensaje := ''El n\00FAmero de voucher esta en blanco'';'),
'        ELSE',
'          lv_mensaje := lv_mensaje ||',
unistr('                        '', el n\00FAmero de voucher esta en blanco'';'),
'        END IF;',
'      END IF;',
'    ',
'      IF :p38_mcd_nro_aut_ref IS NULL THEN',
'        IF lv_mensaje IS NULL THEN',
unistr('          lv_mensaje := ''El n\00FAmero de autorizaci\00F3n esta en blanco'';'),
'        ELSE',
'          lv_mensaje := lv_mensaje ||',
unistr('                        '', el n\00FAmero de autorizaci\00F3n esta en blanco'';'),
'        END IF;',
'      END IF;',
'    END IF;',
'    IF lv_mensaje IS NULL THEN',
unistr('      ----INICIO JALVAREZ 05SEP2022  Req: MPO-3 Integraci\00F3n Medianet - AJUSTE EN PANTALLA DE PAGO DE CUOTA. SE AUMENTA PARA GUARDAR EL NRO DE RECAP'),
'    ',
'      :p38_ede_id             := :p38_plan;',
'      :p38_cre_titular_cuenta := :p38_titular_tc; -- Para que se muestre en el mismo titular del cheque pero igual se guarde en el titular de TC',
'    ',
'      IF :p38_tipo_pago_tj = ''A'' THEN',
'      ',
'        IF :p38_pinpad IS NULL THEN',
'          BEGIN',
'            SELECT rpp.rpp_id',
'              INTO :p38_pinpad',
'              FROM car_redes_pago_pue rpp',
'             WHERE rpp.pue_id = :p0_pue_id',
'               AND rpp.rpp_estado_registro = 0;',
'          EXCEPTION',
'            WHEN OTHERS THEN',
'              :p38_pinpad := NULL;',
'          END;',
'        END IF;',
'        IF :p38_pinpad IS NOT NULL THEN',
'          pq_car_redes_pago.pr_procesar_pago_caja(pn_rpp_id_pinpad => :p38_pinpad,',
'                                                  pn_rpa_id_red    => :p38_red,',
'                                                  pn_cli_id        => :p38_cli_id,',
'                                                  pn_uge_id        => :f_uge_id,',
'                                                  pn_emp_id        => :f_emp_id,',
'                                                  pn_ede_id        => :p38_ede_id,',
'                                                  pn_usu_id        => :f_user_id,',
'                                                  pn_total         => nvl(to_number(:p38_mcd_valor_movimiento),',
'                                                                          0),',
'                                                  pn_rpr_id        => ln_rpr_id,',
'                                                  pv_sujeto_a_iva  => ''N'',',
'                                                  pv_tipo          => :p38_tipo_impresion,',
'                                                  pn_rpc_id        => :p38_rpc_id,',
'                                                  pv_error         => lv_mensaje);',
'          IF lv_mensaje IS NULL THEN',
'            pq_car_redes_pago.pr_recuperar_datos_tj(pn_rpr_id           => ln_rpr_id,',
'                                                    pv_titular          => :p38_titular_tc,',
'                                                    pv_tarjeta          => :p38_tarjeta_emi,',
'                                                    pv_num_tarjeta      => :p38_tj_numero,',
'                                                    pv_num_voucher      => :p38_tj_numero_voucher,',
'                                                    pv_tj_recap         => :p38_tj_recap,',
'                                                    pv_num_autorizacion => :p38_mcd_nro_aut_ref,',
'                                                    pv_error            => lv_mensaje);',
'          ',
'            :p38_tj_numero_1         := :p38_tj_numero;',
'            :p38_tj_numero_voucher_1 := :p38_tj_numero_voucher;',
'            :p38_tj_recap_1          := :p38_tj_recap;',
'            :p38_mcd_nro_aut_ref_1   := :p38_mcd_nro_aut_ref;',
'            :p38_lote                := :p38_tj_recap;',
'            :p38_mcd_nro_comprobante := :p38_tj_numero_voucher;',
'          END IF;',
'        ELSE',
'          lv_mensaje := ''No tiene asignado un pinpad'';',
'        END IF;',
'      END IF;',
unistr('      ----INICIO JALVAREZ 05SEP2022  Req: MPO-3 Integraci\00F3n Medianet - AJUSTE EN PANTALLA DE PAGO DE CUOTA. SE AUMENTA PARA GUARDAR EL NRO DE RECAP'),
'      /*Agregado por Andres Calle',
'      14/Octubre/2011',
'      valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'      */',
'      IF lv_mensaje IS NULL THEN',
'        IF :p38_comision > 0 THEN',
'          pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                      pn_ede_id   => :p38_ede_id,',
'                                                      pn_comision => :p38_comision,',
'                                                      pv_error    => :p0_error);',
'        ',
'        END IF;',
'      ',
'      END IF;',
'    END IF;',
'  END IF;',
'',
'  IF lv_mensaje IS NULL THEN',
'    :p38_vuelto := NULL;',
'    pq_car_pago_cuota.pr_carga_coleccion_mov_caja(pn_ede_id               => :p38_ede_id,',
'                                                  pn_cre_id               => :p38_cre_id,',
'                                                  pn_tfp_id               => :p38_tfp_id,',
'                                                  pn_mcd_valor_movimiento => nvl(to_number(:p38_mcd_valor_movimiento),',
'                                                                                 0),',
'                                                  pn_mcd_nro_aut_ref      => :p38_mcd_nro_aut_ref,',
'                                                  pn_mcd_nro_lote         => :p38_tj_recap,',
'                                                  pn_mcd_nro_retencion    => :p38_rap_nro_retencion,',
'                                                  pn_mcd_nro_comprobante  => :p38_mcd_nro_comprobante,',
'                                                  pv_titular_cuenta       => :p38_cre_titular_cuenta,',
'                                                  pv_cre_nro_cheque       => :p38_cre_nro_cheque,',
'                                                  pv_cre_nro_cuenta       => :p38_cre_nro_cuenta,',
'                                                  pv_nro_pin              => :p38_cre_nro_pin,',
'                                                  pv_nro_tarjeta          => :p38_tj_numero,',
'                                                  pv_voucher              => :p38_tj_numero_voucher,',
'                                                  pn_saldo_retencion      => nvl(to_number(:p38_saldo_retencion),',
'                                                                                 0),',
'                                                  pn_com_id               => ln_com_id_retencion,',
'                                                  pd_rap_fecha            => :p38_rap_fecha,',
'                                                  pd_rap_fecha_validez    => :p38_rap_fecha_validez,',
'                                                  pn_rap_nro_autorizacion => :p38_rap_nro_autorizacion,',
'                                                  pn_rap_nro_establ_ret   => :p38_rap_nro_establ_ret,',
'                                                  pn_rap_nro_pemision_ret => :p38_rap_nro_pemision_ret,',
'                                                  pn_pre_id               => :p38_pre_id,',
'                                                  pd_fecha_deposito       => :p38_mcd_fecha_deposito,',
'                                                  pv_titular_tarjeta_cre  => :p38_titular_tc,',
'                                                  pn_emp_id               => :f_emp_id,',
'                                                  pn_total_pagos          => :p38_valor_total_pagos,',
'                                                  pn_vueltos              => :p38_vuelto,',
'                                                  pn_cli_id               => :p38_cli_id,',
'                                                  pn_uge_id               => :f_uge_id,',
'                                                  pn_nro_recap            => :p38_tj_recap,',
unistr('                                                  pn_rpr_id               => ln_rpr_id, --JALVAREZ 05SEP2022  Req: MPO-3 Integraci\00F3n Medianet - AJUSTE EN PANTALLA DE PAGO DE CUOTA. PARA GUARDAR EL REGISTRO CON EL QUE SE REGISTRO EL PAGO'),
'                                                  pv_error                => :p0_error);',
'  ',
'    --- Null los campos ---',
'    :p38_seq_id               := NULL;',
'    :p38_cre_id               := NULL;',
'    :p38_cre_fecha_deposito   := SYSDATE;',
'    :p38_ede_id               := NULL;',
'    :p38_cre_titular_cuenta   := NULL;',
'    :p38_cre_nro_cheque       := NULL;',
'    :p38_cre_nro_cuenta       := NULL;',
'    :p38_cre_nro_pin          := NULL;',
'    :p38_mcd_valor_movimiento := NULL;',
'  ',
'    :p38_mcd_fecha_deposito  := NULL;',
'    :p38_tj_entidad          := NULL;',
'    :p38_bancos              := NULL;',
'    :p38_tarjeta             := NULL;',
'    :p38_tipo                := NULL;',
'    :p38_tipo_1              := NULL;',
'    :p38_plan                := NULL;',
'    :p38_tj_numero           := NULL;',
'    :p38_tj_numero_voucher   := NULL;',
'    :p38_tj_recap            := NULL;',
'    :p38_lote                := NULL;',
'    :p38_mcd_nro_aut_ref     := NULL;',
'    :p38_mcd_nro_comprobante := NULL;',
'    :p38_modificacion        := NULL;',
'    :p38_mcd_fecha_deposito  := NULL;',
'  ',
'    :p38_rap_nro_retencion    := NULL;',
'    :p38_rap_nro_autorizacion := NULL;',
'    :p38_rap_fecha            := NULL;',
'    :p38_rap_fecha_validez    := NULL;',
'    :p38_rap_com_id           := NULL;',
'    :p38_saldo_retencion      := NULL;',
'    :p38_rap_nro_establ_ret   := NULL;',
'    :p38_rap_nro_pemision_ret := NULL;',
'    :p38_pre_id               := NULL;',
'    :p38_seq_id_movcaja       := NULL;',
'    :p38_mcd_valor_movimiento := NULL;',
'    :p38_tfp_id               := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                    ''cn_tfp_id_efectivo'');',
'                                                                    ',
'               -- tarjetas de credito',
'          :p38_tipo_pago_tj        := ''M'';',
'          :p38_titular_tc          := NULL;',
'          :p38_tarjeta_emi         := NULL;',
'          :p38_tj_numero_1         := NULL;',
'          :p38_tj_numero_voucher_1 := NULL;',
'          :p38_tj_recap_1          := NULL;',
'          :p38_mcd_nro_aut_ref_1   := NULL;                                                               ',
'  ',
'  ELSE',
'    :p0_error := lv_mensaje;',
'  ',
'  END IF;',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_type=>'NEVER'
,p_internal_uid=>38475632786998140153
,p_process_comment=>'CARGAR_TFP'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38507941213331133744)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_valida_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_Ret NUMBER;',
'',
'BEGIN',
'  IF :P38_BASE > 0 THEN',
'    pq_ven_pagos_cuota_retencion.pr_carga_val_retenciones(PN_EMP_ID               => :f_emp_id,',
'                                                          PN_UGE_ID               => :f_uge_id,',
'                                                          PN_TSE_ID               => :f_seg_id,',
'                                                          pn_com_id               => :P38_ORD_ID,',
'                                                          pd_rap_fecha            => :P38_RAP_FECHA,',
'                                                          pd_rap_fecha_validez    => :P38_RAP_FECHA_VALIDEZ,',
'                                                          pn_rap_nro_retencion    => :P38_RAP_NRO_RETENCION,',
'                                                          pn_rap_nro_autorizacion => :P38_RAP_NRO_AUTORIZACION,',
'                                                          pn_RAP_NRO_ESTABL_RET   => :P38_RAP_NRO_ESTABL_RET,',
'                                                          pn_RAP_NRO_PEMISION_RET => :P38_RAP_NRO_PEMISION_RET,',
'                                                          pn_porcentaje           => :P38_tRE_ID,',
'                                                          pn_base_ret             => :p38_base,',
'                                                          pv_ret_iva              => :p38_ret_iva,',
'                                                          pv_observacion          => :P38_OBSERVACIONES_RET,',
'                                                          pv_sesion               => :P38_DEA_SECCION,',
'                                                         pn_ord_id                => :P38_ORD_ID,',
'                                                          pn_rpo_id  => :P38_tRE_ID,',
'                                                          PV_ERROR                => :p0_error);',
'  END IF;',
'',
'  IF :P30_OBSERVACIONES_RET IS NULL THEN',
'  ',
'    SELECT sum(to_number(c010))',
'      INTO :P38_MCD_VALOR_MOVIMIENTO',
'      FROM apex_collections',
'     WHERE collection_name = ''COLL_VALOR_RETENCION'';',
'  ',
'  ELSE',
'    :P38_MCD_VALOR_MOVIMIENTO := NULL;',
'  ',
'  END IF;',
'',
'  :P38_TRE_ID := NULL;',
'  :P38_BASE   := 0;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CARGA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38475688062061368818
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(63184449392163115474)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Anular'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_recibo_cobro_manual.pr_anula_recibos(:f_uge_id,',
'                                           :P38_SECUENCIA,',
'                                            :P38_RAZON,:P0_ERROR                                           );'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(63184448552999115466)
,p_internal_uid=>63152196240893350548
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(78667384509039561145)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pantalla'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  :P38_EDE_ID               := NULL;',
'  :P38_CRE_TITULAR_CUENTA   := NULL;',
'  :P38_MODIFICACION         := NULL;',
'  :P38_TJ_ENTIDAD           := NULL;',
'  :P38_BANCOS               := NULL;',
'  :P38_TARJETA              := NULL;',
'  :P38_TJ_NUMERO            := NULL;',
'  :P38_TJ_NUMERO_VOUCHER    := NULL;',
'  :P38_TJ_RECAP             := NULL;',
'  :P38_LOTE                 := NULL;',
'  :P38_MCD_NRO_AUT_REF      := NULL;',
'  :P38_CRE_NRO_CHEQUE       := NULL;',
'  :P38_CRE_NRO_CUENTA       := NULL;',
'  :P38_CRE_NRO_PIN          := NULL;',
'  :P38_MCD_NRO_COMPROBANTE  := NULL;',
'  :P38_RAP_NRO_ESTABL_RET   := NULL;',
'  :P38_RAP_NRO_PEMISION_RET := NULL;',
'  :P38_RAP_NRO_RETENCION    := NULL;',
'  :P38_RAP_NRO_AUTORIZACION := NULL;',
'  :P38_RAP_FECHA            := NULL;',
'  :P38_RAP_FECHA_VALIDEZ    := NULL;',
'  :P38_RAP_COM_ID           := NULL;',
'  :P38_SALDO_RETENCION      := NULL;',
'  :P38_PRE_ID               := NULL;',
'  :P38_entrada              := NULL;',
'  :P38_titular_tc           := NULL;',
'  :P38_MCD_VALOR_MOVIMIENTO := 0;  ',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''limpia'' and :P38_ERROR_ANTICIPO is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>unistr('Validaci\00F3n aprobada')
,p_internal_uid=>78635131357769796219
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(42753391349951932)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_consulta_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         VARCHAR2(250);',
'  ln_rpr_id           NUMBER;',
'',
'BEGIN',
'  -- Call the procedure                                    ',
'    pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id => :P38_PINPAD,',
'                                                 pn_cli_id => :p38_cli_id,',
'                                                 pn_uge_id => :f_uge_id,',
'                                                 pn_emp_id => :f_emp_id,',
'                                                 pn_pue_id => :p0_pue_id,',
'                                                 pv_rpr_tipo_requerimiento => ''CT'',',
'                                                 pn_rpc_id => :p38_rpc_id,',
'                                                 pn_rpr_id_req => :p38_rpr_id_req,',
'                                                 pv_trama => lv_data_set,',
'                                                 pn_ede_id => null,',
'                                                 pv_error => :p0_error);',
'',
'    COMMIT;',
'  ',
'    IF :p0_error IS NULL THEN',
'      pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id => :P38_PINPAD,',
'                                          pn_pue_id => :p0_pue_id,',
'                                          pn_cli_id => :p38_cli_id,',
'                                          pv_tipo_requerimiento => ''CT'',',
'                                          pv_trama_req => lv_data_set,',
'                                          pv_trama_res => :p38_trama_resp,',
'                                          pn_rpc_id => null,',
'                                          pn_rpr_id_req => :p38_rpr_id_req,',
'                                          pn_rpr_id_res => :p38_rpr_id,',
'                                          pc_error => :pc_error);',
'                                          ',
'                                          ',
'',
'    ',
'    END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(43074904762640138)
,p_internal_uid=>10500240080187006
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(43074998617640139)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_lectura_tarjeta'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  pn_respuesta_status NUMBER;',
'  pc_respuesta        CLOB;',
'  pc_error            CLOB;',
'  lv_data_set         VARCHAR2(250);',
'  ln_rpr_id           NUMBER;',
'',
'BEGIN',
'  -- Call the procedure                                    ',
'    pq_car_redes_pago.pr_requerimiento_lec_tarjeta(pn_rpp_id => :P38_PINPAD,',
'                                                 pn_cli_id => :p38_cli_id,',
'                                                 pn_uge_id => :f_uge_id,',
'                                                 pn_emp_id => :f_emp_id,',
'                                                 pn_pue_id => :p0_pue_id,',
'                                                 pv_rpr_tipo_requerimiento => ''LT'',',
'                                                 pn_rpc_id => :p38_rpc_id,',
'                                                 pn_rpr_id_req => :p38_rpr_id_req,',
'                                                 pv_trama => lv_data_set,',
'                                                 pn_ede_id => null,',
'                                                 pv_error => :p0_error);',
'',
'    COMMIT;',
'  ',
'    IF :p0_error IS NULL THEN',
'      pq_car_redes_pago.pr_envio_trama_pinpad(pn_rpp_id => :P38_PINPAD,',
'                                          pn_pue_id => :p0_pue_id,',
'                                          pn_cli_id => :p38_cli_id,',
'                                          pv_tipo_requerimiento => ''LT'',',
'                                          pv_trama_req => lv_data_set,',
'                                          pv_trama_res => :p38_trama_resp,',
'                                          pn_rpc_id => :p38_rpc_id,',
'                                          pn_rpr_id_req => :p38_rpr_id_req,',
'                                          pn_rpr_id_res => :p38_rpr_id,',
'                                          pc_error => :pc_error);',
'                                          ',
'                                          ',
'',
'    ',
'    END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(42753338607951931)
,p_internal_uid=>10821847347875213
);
wwv_flow_imp.component_end;
end;
/
