prompt --application/pages/page_00091
begin
--   Manifest
--     PAGE: 00091
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>91
,p_name=>unistr('Impresi\00BF\00BFn Facturas Motos')
,p_step_title=>unistr('Impresi\00BF\00BFn Facturas Motos')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(135532477854042773)
,p_name=>'<B>&P0_TTR_DESCRIPCION.   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.com_id,a.ord_id,g.com_fecha,i.ord_sec_id,i.ord_tipo,g.cli_id,g.age_id_agencia ,h.nombre_completo,''Imprimir'' as Imprimir',
'',
'  FROM ',
'  ',
'       inv_ordenes_despacho           a,',
'       inv_odespachos_detalle         b,',
'       inv_guias_remision             c,',
'       inv_guias_remision_detalle     d,',
'       inv_comprobantes_inventario    e,',
'       inv_comprobantes_detalle       f,',
'       ven_comprobantes g,',
'       v_asdm_clientes_personas h,',
'       ven_ordenes i',
'      ',
'',
' WHERE a.ode_id = b.ode_id',
'   AND c.gre_id = d.gre_id',
'   AND d.odd_id = b.odd_id',
'   AND e.gre_id = c.gre_id',
'   AND f.cin_id = e.cin_id',
'   AND a.com_id=g.com_id',
'   AND g.cli_id=h.cli_id',
'   AND g.ord_id=i.ord_id',
'   AND a.ord_id=i.ord_id',
'   AND i.ord_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_ord_tipo_orden_venta_moto'')',
'   AND g.com_numero IS NULL',
'   AND a.emp_id=:f_emp_id',
'   ORDER BY g.com_fecha DESC '))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen Facturas pendientes de imprimir'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135532763531042784)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135532867613042784)
,p_query_column_id=>2
,p_column_alias=>'ORD_ID'
,p_column_display_sequence=>3
,p_column_heading=>'ORD_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135718258010021598)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>4
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135532971893042784)
,p_query_column_id=>4
,p_column_alias=>'ORD_SEC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Orden #'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(141260674488347996)
,p_query_column_id=>5
,p_column_alias=>'ORD_TIPO'
,p_column_display_sequence=>2
,p_column_heading=>'Tipo<br>Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135533156480042784)
,p_query_column_id=>6
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>6
,p_column_heading=>unistr('C\00BF\00BFdigo <br> Cliente')
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(142072454720252890)
,p_query_column_id=>7
,p_column_alias=>'AGE_ID_AGENCIA'
,p_column_display_sequence=>9
,p_column_heading=>'Age Id Agencia'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135533267049042784)
,p_query_column_id=>8
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>7
,p_column_heading=>'Nombre'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135720368708071953)
,p_query_column_id=>9
,p_column_alias=>'IMPRIMIR'
,p_column_display_sequence=>8
,p_column_heading=>'Imprimir'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:91:&SESSION.:imprimir:&DEBUG.::P91_COM_ID,P91_ORD_ID,P91_AGE_ID_AGENCIA:#COM_ID#,#ORD_ID#,#AGE_ID_AGENCIA#'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(135547782973138804)
,p_plug_name=>'Folios'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_plug_display_when_condition=>'imprimir'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(135732659545182881)
,p_plug_name=>unistr('Informaci\00BF\00BFn')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P91_COM_NUM = :P0_FOL_SEC_ACTUAL'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(135735376083206501)
,p_name=>'Indentificadores'
,p_parent_plug_id=>wwv_flow_imp.id(135732659545182881)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT h.tid_id,',
'       i.tid_descripcion,',
'       h.ebs_valor_identificador valor',
'',
'  FROM inv_ordenes_despacho           a,',
'       inv_odespachos_detalle         b,',
'       inv_guias_remision             c,',
'       inv_guias_remision_detalle     d,',
'       inv_comprobantes_inventario    e,',
'       inv_comprobantes_detalle       f,',
'       inv_comprobantes_detalle_serie g,',
'       inv_items_estado_bodega_serie  h,',
'       inv_tipos_identificador        i',
'',
' WHERE a.ode_id = b.ode_id',
'   AND c.gre_id = d.gre_id',
'   AND d.odd_id = b.odd_id',
'   AND e.gre_id = c.gre_id',
'   AND f.cin_id = e.cin_id',
'   AND g.cde_id = f.cde_id',
'   AND g.ebs_id = h.ebs_id',
'   AND h.ieb_id = f.ieb_id',
'   AND h.tid_id = i.tid_id',
'   AND a.ord_id = :P91_ORD_ID',
'   AND a.com_id= :P91_COM_ID',
'',
'UNION',
'',
'SELECT j.tid_id,',
'       i.tid_descripcion,',
'       j.ise_valor_identificador',
'',
'  FROM inv_ordenes_despacho           a,',
'       inv_odespachos_detalle         b,',
'       inv_guias_remision             c,',
'       inv_guias_remision_detalle     d,',
'       inv_comprobantes_inventario    e,',
'       inv_comprobantes_detalle       f,',
'       inv_comprobantes_detalle_serie g,',
'       inv_items_estado_bodega_serie  h,',
'       inv_tipos_identificador        i,',
'       inv_iden_secundarios_ebs       j',
'',
' WHERE a.ode_id = b.ode_id',
'   AND c.gre_id = d.gre_id',
'   AND d.odd_id = b.odd_id',
'   AND e.gre_id = c.gre_id',
'   AND f.cin_id = e.cin_id',
'   AND g.cde_id = f.cde_id',
'   AND g.ebs_id = h.ebs_id',
'   AND h.ieb_id = f.ieb_id',
'   AND j.tid_id = i.tid_id',
'   AND h.ebs_id = j.ebs_id',
'   AND a.ord_id = :P91_ORD_ID',
'   AND a.com_id= :P91_COM_ID'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>2
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>unistr('No est\00BF\00BFn asignados indentificadores.')
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135735661558206963)
,p_query_column_id=>1
,p_column_alias=>'TID_ID'
,p_column_display_sequence=>1
,p_column_heading=>'TID_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135735772779206963)
,p_query_column_id=>2
,p_column_alias=>'TID_DESCRIPCION'
,p_column_display_sequence=>2
,p_column_heading=>'Identificador'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(135735861207206963)
,p_query_column_id=>3
,p_column_alias=>'VALOR'
,p_column_display_sequence=>3
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(135717766190014472)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(135735376083206501)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_condition=>':P91_COM_NUM = :P0_FOL_SEC_ACTUAL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(135769574697556222)
,p_branch_action=>'f?p=&APP_ID.:91:&SESSION.:IMPRIMIR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(135717766190014472)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 31-JAN-2012 11:37 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(142180176002195606)
,p_branch_action=>'f?p=&APP_ID.:91:&SESSION.::&DEBUG.:91::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>20
,p_branch_condition_type=>'NEVER'
,p_branch_comment=>'Created 24-FEB-2012 12:29 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135551154885178016)
,p_name=>'P91_UGE_NUM_EST_SRI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(135547782973138804)
,p_prompt=>'Establecimiento:'
,p_source=>'P0_UGE_NUM_EST_SRI'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135553562073198978)
,p_name=>'P91_PUE_NUM_SRI'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(135547782973138804)
,p_prompt=>unistr('Punto de Emisi\00BF\00BFn:')
,p_source=>'P0_PUE_NUM_SRI'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135555055278206559)
,p_name=>'P91_COM_NUM'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(135547782973138804)
,p_prompt=>'Secuencia Factura:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135718766106033337)
,p_name=>'P91_COM_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(135547782973138804)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135718971647035002)
,p_name=>'P91_ORD_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(135547782973138804)
,p_prompt=>'Ord Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(135729583393161390)
,p_name=>'P91_ITEM'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(135732659545182881)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Descripci\00BF\00BFn :')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT inv.ite_sec_id||'' - ''||inv.ite_descripcion_larga item',
'FROM ven_comprobantes_det cd,',
'     inv_items inv',
'WHERE cd.ite_sku_id=inv.ite_sku_id',
'AND cd.emp_id=:F_EMP_ID',
'AND inv.emp_id=:F_EMP_ID',
'AND cd.com_id=:P91_COM_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(142073376883259226)
,p_name=>'P91_AGE_ID_AGENCIA'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(135547782973138804)
,p_prompt=>'Age Id Agencia'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(135763658505532617)
,p_validation_name=>'P91_COM_NUM'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p91_com_num = :P0_FOL_SEC_ACTUAL then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'Error'
,p_associated_item=>wwv_flow_imp.id(135555055278206559)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(142040468712992041)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_FACTURA_MOTO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_comprobantes.pr_impresion_factura_motos(pn_emp_id =>:f_emp_id,',
'pn_tse_id =>:f_seg_id,',
'pn_age_id =>null,',
'pn_com_id =>:P91_COM_ID,',
'pn_secuencia_actual =>:p0_fol_sec_actual,',
'pn_pue_id =>:p0_pue_id,',
'pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'pv_pue_num_sri =>:p0_pue_num_sri,',
'pn_nro_folio => :p0_nro_folio,',
'pv_error =>:p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(135717766190014472)
,p_process_when=>'IMPRIMIR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>109787317443227115
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(135541557815065287)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_folio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'  ln_tgr_id  := pq_ven_listas_caja.fn_tipo_grupo_transaccion_seg(:f_emp_id,:f_seg_id);',
'  :P0_TTR_ID := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id); ',
'',
'   pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
'   pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                   :f_pca_id,',
'                                                    ln_tgr_id,',
'                                                  --:p0_ttr_id,',
'                                                   :p0_ttr_descripcion,',
'                                                   :p0_periodo,',
'                                                   :p0_datfolio,',
'                                                   :p0_fol_sec_actual,',
'                                                   :p0_pue_num_sri,',
'                                                   :p0_uge_num_est_sri,',
'                                                   :p0_pue_id,',
'                                                   :p0_ttr_id,',
'                                                   :p0_nro_folio,',
'                                                   :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>103288406545300361
);
wwv_flow_imp.component_end;
end;
/
