prompt --application/pages/page_00135
begin
--   Manifest
--     PAGE: 00135
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>135
,p_name=>'pruebas_aco'
,p_page_mode=>'MODAL'
,p_step_title=>'pruebas_aco'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'ACALLE'
,p_last_upd_yyyymmddhh24miss=>'20170119090546'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37398156663215132)
,p_name=>'Report 1'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select emp_id, emp_ruc, emp_nombre_comercial',
'from asdM_empresas'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Download'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37398773515215140)
,p_query_column_id=>1
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37399161612215141)
,p_query_column_id=>2
,p_column_alias=>'EMP_RUC'
,p_column_display_sequence=>2
,p_column_heading=>'Emp Ruc'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37399607919215142)
,p_query_column_id=>3
,p_column_alias=>'EMP_NOMBRE_COMERCIAL'
,p_column_display_sequence=>3
,p_column_heading=>'Emp Nombre Comercial'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_imp.component_end;
end;
/
