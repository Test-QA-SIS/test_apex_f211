prompt --application/pages/page_00128
begin
--   Manifest
--     PAGE: 00128
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>128
,p_name=>'Cancelacion de Seguros'
,p_alias=>'CANCELACION_SEGUROS'
,p_step_title=>'Cancelacion de Seguros'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(67721066173021977)
,p_plug_name=>'Datos Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(67731662729172428)
,p_name=>'SEGUROS EMITIDOS'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.cse_id,',
'       b.cxc_id,',
'       b.cli_id,',
'       d.nombre_completo,',
'       c.cxc_saldo,',
'       b.cse_fecha_ini,',
'       b.cse_fecha_fin,',
'       ''SELECCIONAR'' seleccionar,',
'       CASE',
'         WHEN nvl((SELECT MAX(s.dse_estado)',
'                    FROM car_dividendos_seguros s',
'                   WHERE s.cse_id = b.cse_id',
'                     AND s.dse_estado = ''REC''),',
'                  ''ENV'') = ''REC'' THEN',
'          ''RECHAZADO''',
'         WHEN b.ese_id = ''EMI'' THEN',
'          ''EMITIDO''',
'         WHEN b.ese_id = ''ENV'' THEN',
'          ''ENVIADO''',
'       END estado,',
'       (select ag.age_id',
'       from asdm_agencias ag',
'       where ag.uge_id = b.uge_id) age_id',
'  FROM car_cuentas_seguros    b,',
'       car_cuentas_por_cobrar c,',
'       v_asdm_datos_clientes  d',
' WHERE c.cxc_id = b.cxc_id',
'   AND d.cli_id = b.cli_id',
'   AND b.cse_fecha_fin > trunc(SYSDATE)',
'  AND B.CLI_ID = :P128_CLI_ID  ',
'  and b.ese_id != ''ANU''',
'  /*and b.pse_id != 11  25/10/2021 acalle. Autorizado por Ana Paola Alvarado*/'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67731969517172565)
,p_query_column_id=>1
,p_column_alias=>'CSE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732082705172573)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732153321172573)
,p_query_column_id=>3
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>3
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732269523172573)
,p_query_column_id=>4
,p_column_alias=>'NOMBRE_COMPLETO'
,p_column_display_sequence=>4
,p_column_heading=>'NOMBRES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732357638172573)
,p_query_column_id=>5
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>5
,p_column_heading=>'SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732480263172573)
,p_query_column_id=>6
,p_column_alias=>'CSE_FECHA_INI'
,p_column_display_sequence=>6
,p_column_heading=>'FECHA EMISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732569312172573)
,p_query_column_id=>7
,p_column_alias=>'CSE_FECHA_FIN'
,p_column_display_sequence=>7
,p_column_heading=>'FECHA CADUCIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67732657521172573)
,p_query_column_id=>8
,p_column_alias=>'SELECCIONAR'
,p_column_display_sequence=>9
,p_column_heading=>'SELECCIONAR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:128:&SESSION.:SELECCIONA:&DEBUG.::P128_CSE_ID,P128_CXC_ID,P128_ESTADO,P128_AGE_ID:#CSE_ID#,#CXC_ID#,#ESTADO#,#AGE_ID#'
,p_column_linktext=>'#SELECCIONAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(303391361578845877)
,p_query_column_id=>9
,p_column_alias=>'ESTADO'
,p_column_display_sequence=>8
,p_column_heading=>'ESTADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(213643881932915227)
,p_query_column_id=>10
,p_column_alias=>'AGE_ID'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(67732853510179222)
,p_plug_name=>'COBERTURA SELECCIONADA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>30
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P128_CLI_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(67759368036382123)
,p_name=>'CRONOGRAMA COBERTURA'
,p_parent_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c001 nro_vencimiento,',
'       c002 fecha_vencimiento,',
'       to_number(c003) valor_cuota,',
'       to_number(c004) valor_abonado,',
'       to_number(c005) valor_devolver,',
'       c006 estado,',
'       to_number(c007) Anticipo_Cliente,',
'c008 estado_cuota',
'  FROM apex_collections',
' WHERE collection_name = ''COL_SEG_EMITIDO''',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67773453760551922)
,p_query_column_id=>1
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67773565683551928)
,p_query_column_id=>2
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67773682427551928)
,p_query_column_id=>3
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67773771904551928)
,p_query_column_id=>4
,p_column_alias=>'VALOR_ABONADO'
,p_column_display_sequence=>4
,p_column_heading=>'Valor Abonado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67773863601551928)
,p_query_column_id=>5
,p_column_alias=>'VALOR_DEVOLVER'
,p_column_display_sequence=>5
,p_column_heading=>'Valor Devolver'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67773952943551928)
,p_query_column_id=>6
,p_column_alias=>'ESTADO'
,p_column_display_sequence=>6
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(101585771687941717)
,p_query_column_id=>7
,p_column_alias=>'ANTICIPO_CLIENTE'
,p_column_display_sequence=>7
,p_column_heading=>'Anticipo Cliente'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(101606280154135298)
,p_query_column_id=>8
,p_column_alias=>'ESTADO_CUOTA'
,p_column_display_sequence=>8
,p_column_heading=>'Estado Cuota'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(67806677066961915)
,p_name=>'pruebas'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * From apex_collections where collection_name = ''COLL_PAGO_DIV_SEGURO'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67806960238961944)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807072408961948)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807165726961948)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807253638961948)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807366721961948)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807463887961953)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807578249961953)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807659418961953)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807760744961953)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807857548961953)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67807951877961953)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808054765961953)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808153794961953)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808264047961953)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808359318961953)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808457793961953)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808557493961956)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808676665961956)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808772915961956)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808852282961956)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67808956322961956)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809060899961956)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809170653961956)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809277921961956)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809357192961956)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809452929961956)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809579233961956)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809666393961956)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809771510961956)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809854616961956)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67809983593961956)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810069472961956)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810165559961956)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810251708961956)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810376265961957)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810467081961957)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810562489961957)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810672302961957)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810777609961957)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810867000961957)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67810971327961958)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811055156961958)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811172654961958)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811277406961958)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811359048961958)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811467514961958)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811556046961958)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811674102961958)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811767103961958)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811852201961958)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67811965267961958)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812067904961958)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812165860961958)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812269637961958)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812365967961958)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812466365961958)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812574330961958)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812683468961958)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812761089961958)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812868076961958)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67812973106961958)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67813058829961958)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67813158246961958)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67813280323961958)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67813374040961958)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(67813477318961959)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(67791252853831956)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(67759368036382123)
,p_button_name=>'CANCELA_SEGURO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancela Seguro'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>'P128_CSE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(67791665170835263)
,p_branch_name=>'br_cancela'
,p_branch_action=>'f?p=&APP_ID.:128:&SESSION.:CANCELAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(67791252853831956)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67721451446021986)
,p_name=>'P128_EDAD'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(67721066173021977)
,p_prompt=>'Edad Cliente'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67721672113021988)
,p_name=>'P128_TIPO_IDENTIFICACION'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(67721066173021977)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67721852146021989)
,p_name=>'P128_NRO_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(67721066173021977)
,p_prompt=>'Nro Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (per.per_razon_social || per.per_primer_nombre || '' '' ||',
'       per.per_segundo_nombre || '' '' || per.per_primer_apellido || '' '' ||',
'       per.per_segundo_apellido) || '' - Cod: '' || cli.cli_id || '' - Id: '' ||',
'       per.per_nro_identificacion nombre,',
'       per.per_nro_identificacion identificacion',
'  FROM asdm_personas per, asdm_clientes cli',
' WHERE cli.per_id = per.per_id',
'   AND cli.emp_id = per.emp_id',
'   AND cli.cli_estado_registro = 0',
'   AND per.per_estado_registro = 0',
'   AND per.emp_id  = :F_EMP_ID',
'   and per.per_tipo_identificacion = nvl(:p128_tipo_identificacion,''CED'')'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onChange=''doSubmit("CLIENTE")'''
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67722069175021995)
,p_name=>'P128_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(67721066173021977)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67722276322021996)
,p_name=>'P128_NOMBRES'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(67721066173021977)
,p_prompt=>'Nombres'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67733165055182009)
,p_name=>'P128_CSE_ID'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_prompt=>'Cse Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67783766410637191)
,p_name=>'P128_CXC_ID'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67783966134637220)
,p_name=>'P128_VALOR_CANCELAR'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_prompt=>'Valor Cancelar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67819374413134928)
,p_name=>'P128_MNC_ID'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_prompt=>'Motivo Cancelacion Seguro'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov       VARCHAR2(500);',
'  ln_mnc_anula asdm_motivos_nc.mnc_id%TYPE := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                 ''cn_mnc_id_anulacion_mismodia'');',
'  ln_mnc_rechazo asdm_motivos_nc.mnc_id%TYPE := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                                 ''cn_mnc_id_rechazo_aseguradora'');                                                                                 ',
'  ',
'BEGIN',
'  IF :p128_estado = ''EMITIDO'' THEN',
'    lv_lov := ''select mnc_descripcion, mnc_id',
'                from asdm_motivos_nc',
'                where mnc_tipo = ''''ANSEG''''',
'                and emp_id = ''|| :f_emp_id ||''',
'                and mnc_id = '' || ln_mnc_anula;',
'  ELSIF :p128_estado = ''ENVIADO'' THEN',
'    lv_lov := ''select mnc_descripcion, mnc_id',
'                from asdm_motivos_nc',
'                where mnc_tipo = ''''ANSEG''''',
'                and emp_id = ''|| :f_emp_id ||''',
'                and mnc_id != '' || ln_mnc_anula;',
'  ELSIF :p128_estado = ''RECHAZADO'' THEN',
'    lv_lov := ''select mnc_descripcion, mnc_id',
'                from asdm_motivos_nc',
'                where mnc_tipo = ''''ANSEG''''',
'                and emp_id = ''|| :f_emp_id ||''',
'                and mnc_id = '' || ln_mnc_rechazo;',
'  ELSE',
'    lv_lov := ''SELECT NULL l, NULL r FROM DUAL'';',
'  END IF;',
'  RETURN(lv_lov);',
'',
'END;',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-SELECCIONE-'
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(101617065378278891)
,p_name=>'P128_FECHA_VIGENCIA'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_prompt=>'Fecha Vigencia Seguro'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:24;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(201472042918444556)
,p_name=>'P128_CLAVE'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_prompt=>'Clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(201472492059444561)
,p_name=>'P128_SOL_CLAVE'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(213644040470915228)
,p_name=>'P128_AGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(303391983563850799)
,p_name=>'P128_ESTADO'
,p_item_sequence=>255
,p_item_plug_id=>wwv_flow_imp.id(67732853510179222)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(67819783674145799)
,p_validation_name=>'P128_MNC_ID'
,p_validation_sequence=>10
,p_validation=>'P128_MNC_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione el Motivo de la Cancelacion'
,p_when_button_pressed=>wwv_flow_imp.id(67791252853831956)
,p_associated_item=>wwv_flow_imp.id(67819374413134928)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(201472811458444564)
,p_validation_name=>'P128_CLAVE'
,p_validation_sequence=>20
,p_validation=>'P128_CLAVE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'DEBE SOLICITAR LA CLAVE PARA ANULAR EL SEGURO POR RECHAZO DE LA ASEGURADORA'
,p_validation_condition=>'P128_SOL_CLAVE'
,p_validation_condition2=>'S'
,p_validation_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_when_button_pressed=>wwv_flow_imp.id(67791252853831956)
,p_error_display_location=>'INLINE_WITH_FIELD'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(201473084335444567)
,p_validation_name=>'P128_CLAVE1'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_clave number;',
'begin',
'ln_clave := pq_asdm_Seguros.fn_clave_anulacion(:p128_cli_id, :P128_age_id);',
'if :p128_clave != ln_clave then',
'RETURN ''clave incorrecta''||ln_clave||'' *'';',
'else',
'return null;',
'end if;',
'',
'end;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_validation_condition=>'P128_SOL_CLAVE'
,p_validation_condition2=>'S'
,p_validation_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_when_button_pressed=>wwv_flow_imp.id(67791252853831956)
,p_associated_item=>wwv_flow_imp.id(201472042918444556)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(201472097425444557)
,p_name=>'ad_rechazo'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P128_MNC_ID'
,p_condition_element=>'P128_MNC_ID'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'83'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(201472218964444558)
,p_event_id=>wwv_flow_imp.id(201472097425444557)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P128_CLAVE'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(201472297979444559)
,p_event_id=>wwv_flow_imp.id(201472097425444557)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P128_CLAVE'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(201472586543444562)
,p_event_id=>wwv_flow_imp.id(201472097425444557)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P128_SOL_CLAVE'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'S'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(201472658026444563)
,p_event_id=>wwv_flow_imp.id(201472097425444557)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P128_SOL_CLAVE'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'N'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(67723359032029433)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_Cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_cuenta NUMBER := 0;',
'BEGIN',
'  IF :p128_nro_identificacion IS NOT NULL THEN',
'    pq_asdm_seguros.pr_datos_cliente(pn_emp_id              => :f_emp_id,',
'                                     pv_tipo_identificacion => :p128_tipo_identificacion,',
'                                     pv_nro_identificacion  => :p128_nro_identificacion,',
'                                     pv_origen              => ''A'',',
'                                     pv_nombres             => :p128_nombres,',
'                                     pn_cli_id              => :p128_cli_id,',
'                                     pn_edad                => :p128_edad,',
'                                     pv_error               => :p0_error);      ',
'  ELSE',
'    :p128_nombres             := NULL;',
'    :p128_cli_id              := NULL;  ',
'  END IF;',
':P128_FECHA_VIGENCIA := null;',
':P128_VALOR_CANCELAR := null;',
':p128_cse_id := null;',
':p128_cxc_id := null;',
':p128_mnc_id := null;',
':P128_ESTADO := NULL;',
'IF apex_collection.collection_exists(''COL_SEG_EMITIDO'') THEN',
'      apex_collection.delete_collection(''COL_SEG_EMITIDO'');',
'    END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CLIENTE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>35470207762264507
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(67736381692196758)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_cobertura'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_ASDM_SEGUROS.pr_carga_col_seg_emitido(pn_cse_id => :p128_cse_id,',
'pn_total_devolver => :P128_VALOR_CANCELAR,',
'pd_fecha_vigencia => :p128_fecha_vigencia,',
'                         pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'SELECCIONA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>35483230422431832
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(67791853676841625)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar_cancelacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_clave number;',
'begin',
'if :p128_sol_clave = ''S'' then',
'ln_clave := pq_asdm_Seguros.fn_clave_anulacion(:p128_cli_id, :P128_age_id);',
'if ln_clave is null then',
'raise_application_error(-20000,''clave no calculada'');',
'end if;',
'if ln_clave != :p128_clave then',
'raise_application_error(-20000,''clave incorrecta'');',
'end if;',
'end if;',
'PQ_ASDM_sEGUROS.pr_cancela_seguro(pn_emp_id => :f_emp_id,',
'                                  pn_cxc_id => :p128_cxc_id,',
'                                  pn_valor  => :P128_VALOR_CANCELAR,',
'                                  pn_mnc_id => :P128_MNC_ID,',
'                                  pn_ttr_id => pq_constantes.fn_retorna_constante(0,''cn_ttr_id_anula_seguro_masivo''),',
'                                  pn_cli_id => :p128_cli_id,',
'                                  pn_uge_id => :f_uge_id,',
'                                  pn_usu_id => :f_user_id,',
'                                  pn_tse_id => :f_seg_id,',
'                                  pd_fecha_validacion => :P128_FECHA_VIGENCIA,',
'                                  pv_error  => :p0_error);',
'',
':P128_CSE_ID := null;',
':P128_CXC_ID := null;',
':P128_VALOR_CANCELAR := null;',
':P128_MNC_ID := null;',
':P128_FECHA_VIGENCIA := null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CANCELAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>35538702407076699
);
wwv_flow_imp.component_end;
end;
/
