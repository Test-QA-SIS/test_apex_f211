prompt --application/pages/page_00032
begin
--   Manifest
--     PAGE: 00032
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>32
,p_name=>'Reporte Comprobantes'
,p_step_title=>'Reporte Comprobantes'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(43608275519239507)
,p_plug_name=>unistr('Notas de Cr\00E9dito pendientes de generar ')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (c.com_id = h.com_id AND h.estado IS NULL) THEN --es migrada',
'          ''Recalcular''',
'         ELSE',
'          NULL',
'       END recalcular,',
'       CASE',
'         WHEN (h.com_id IS NULL) OR (c.com_id = h.com_id AND h.estado = ''R'') THEN',
'          ''Generar NC''',
'         ELSE',
'          NULL',
'       END generar,',
'       ''Generar NC Simple'' generar_nc,',
'       h.com_id hcom,',
'       c.com_id ccom,',
'       i.cin_id,',
'       i.com_id,',
'       c.com_fecha,',
'       ',
'       i.cli_id,',
'       c.nombre_completo,',
'       c.per_nro_identificacion identificacion,',
'       c.uge_num_sri || '' - '' || c.pue_num_sri || '' - '' || c.com_numero factura,',
'       pq_lv_kdda.fn_obtiene_valor_lov(291, c.com_tipo) tipo,',
'       c.pol_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(55, c.pol_id) politica,',
'       ',
'       c.com_plazo plazo,',
'       c.ord_id,',
'       c.age_id_agente,',
'       c.agente vendedor,',
'       pq_ven_retorna_sec_id.fn_ord_sec_id(c.ord_id) orden,',
'       pq_ven_retorna_sec_id.fn_cin_sec_id(i.cin_id) cominv,',
'       h.secuencia_factura',
'  FROM inv_comprobantes_inventario  i,',
'       asdm_transacciones           t,',
'       v_ven_comprobantes           c,',
'       asdm_p.asdm_homologacion_com h,',
'       inv_ordenes_devolucion       o',
'',
' WHERE i.trx_id = t.trx_id',
'   AND t.ttr_id IN (:p32_ttr_dev)',
'   AND i.com_id = c.com_id',
'   AND i.emp_id = c.emp_id',
'   AND i.emp_id = t.emp_id',
'   AND i.cin_estado_registro = 0',
'   AND o.ode_estado_registro = 0',
'   AND c.com_id = h.com_id(+)',
'   AND i.emp_id = :f_emp_id',
'   AND c.tse_id = :f_seg_id',
'   AND i.com_id NOT IN (SELECT com_id_ref',
'                          FROM ven_comprobantes nc, asdm_transacciones tr',
'                         WHERE nc.trx_id = tr.trx_id',
'                           AND tr.ttr_id = :p32_ttr_nc',
'                           AND nc.emp_id = nc.emp_id',
'                           AND nc.cin_id = i.cin_id)',
'   AND o.com_id = c.com_id',
'   AND o.cin_id = i.cin_id',
'   AND o.uge_id = :f_uge_id',
'   AND o.emp_id = c.emp_id',
'   AND o.ode_fecha BETWEEN to_date(:p32_desde, ''dd/mm/yyyy'') AND',
'       to_date(:p32_hasta, ''dd/mm/yyyy'') + 0.99999',
' ORDER BY c.com_fecha DESC',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(43608376913239507)
,p_name=>'rp_comprobantes'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:PDF'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>11355225643474581
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(43608552250239508)
,p_db_column_name=>'CIN_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'cin_id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CIN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(50925372107467508)
,p_db_column_name=>'COM_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fac Ref Sic'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(50925456351467508)
,p_db_column_name=>'CLI_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(50926569122476079)
,p_db_column_name=>'POL_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(50926877779478594)
,p_db_column_name=>'POLITICA'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Politica'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POLITICA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(50927181081478618)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(87257879124808267)
,p_db_column_name=>'NOMBRE_COMPLETO'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(87258061362808269)
,p_db_column_name=>'VENDEDOR'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Vendedor'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VENDEDOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(87265470860834275)
,p_db_column_name=>'TIPO'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(87273451944838325)
,p_db_column_name=>'IDENTIFICACION'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(87274281034846698)
,p_db_column_name=>'PLAZO'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(88723559487872883)
,p_db_column_name=>'ORD_ID'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(88744864663073049)
,p_db_column_name=>'ORDEN'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'# Orden'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(88748082849116141)
,p_db_column_name=>'COMINV'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'# Comp. Inv'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COMINV'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(140978384016161503)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Fecha<br>Factura'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(140978470334161511)
,p_db_column_name=>'FACTURA'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Factura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61431751812150104)
,p_db_column_name=>'RECALCULAR'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Recalcular'
,p_column_link=>'f?p=242:42:&SESSION.::&DEBUG.:42:F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_PARAMETRO,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_POPUP,P42_COM_ID:&F_LOGOUT_URL.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,'
||'&F_PARAMETRO.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_POPUP.,#COM_ID#'
,p_column_linktext=>'#RECALCULAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RECALCULAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(61431867971150107)
,p_db_column_name=>'GENERAR'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Generar'
,p_column_link=>'f?p=&APP_ID.:33:&SESSION.:carga_det:&DEBUG.::P33_CLI_ID,P33_COM_ID_FACTURA,P33_COM_NUMERO,P33_POL_ID,P33_PER_NRO_IDENTIFICACION,P33_CIN_ID,P33_AGE_ID_AGENTE,P33_ORD_ID,P33_PLAZO,P33_FACTURA_ORACLE,P33_PAGINA_ORIGEN:#CLI_ID#,#COM_ID#,#FACTURA#,#POL_ID'
||'#,#IDENTIFICACION#,#CIN_ID#,#AGE_ID_AGENTE#,#ORD_ID#,#PLAZO#,#SECUENCIA_FACTURA#,&P32_PAGINA_ORIGEN.'
,p_column_linktext=>'#GENERAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'GENERAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39186087687388227)
,p_db_column_name=>'GENERAR_NC'
,p_display_order=>37
,p_column_identifier=>'AE'
,p_column_label=>'Generar nc'
,p_column_link=>'f?p=&APP_ID.:153:&SESSION.:carga_det:&DEBUG.:RP:P153_CLI_ID,P153_COM_ID_FACTURA,P153_COM_NUMERO,P153_POL_ID,P153_PER_NRO_IDENTIFICACION,P153_CIN_ID,P153_AGE_ID_AGENTE,P153_ORD_ID,P153_PLAZO,P153_FACTURA_ORACLE,P153_PAGINA_ORIGEN:#CLI_ID#,#COM_ID#,#FA'
||'CTURA#,#POL_ID#,#IDENTIFICACION#,#CIN_ID#,#AGE_ID_AGENTE#,#ORD_ID#,#PLAZO#,#SECUENCIA_FACTURA#,&P32_PAGINA_ORIGEN.'
,p_column_linktext=>'GENERAR NC SIMPLE'
,p_column_link_attr=>'class="lock_ui_row"  style="color: #0269ff;"'
,p_column_type=>'STRING'
,p_display_condition_type=>'EXISTS'
,p_display_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT usu_id',
'            FROM ven_usu_vta_credito',
'           WHERE uvc_estado_registro = 0',
'             AND usu_id = :F_USER_ID;'))
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(67408362644622522)
,p_db_column_name=>'HCOM'
,p_display_order=>47
,p_column_identifier=>'AB'
,p_column_label=>'HCOM'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'HCOM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(67408463574622526)
,p_db_column_name=>'CCOM'
,p_display_order=>57
,p_column_identifier=>'AC'
,p_column_label=>'Ccom'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CCOM'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(154438071330535387)
,p_db_column_name=>'SECUENCIA_FACTURA'
,p_display_order=>67
,p_column_identifier=>'AD'
,p_column_label=>'FacOracle'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'SECUENCIA_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(43609954997239649)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292416391919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'RECALCULAR:GENERAR:FACTURA:COM_ID:SECUENCIA_FACTURA:COM_FECHA:ORDEN:COMINV:CLI_ID:IDENTIFICACION:NOMBRE_COMPLETO:POLITICA:PLAZO:VENDEDOR:TIPO:GENERAR_NC:'
,p_sort_column_1=>'COMINV'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'FACTURA'
,p_sort_direction_2=>'DESC'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(34498858518662863459)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_button_name=>'Consultar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Consultar'
,p_button_alignment=>'LEFT-CENTER'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(34498858612495863460)
,p_branch_action=>'f?p=&APP_ID.:32:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(2228775958476616823)
,p_name=>'P32_X'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_use_cache_before_default=>'NO'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498858130786863455)
,p_name=>'P32_TTR_DEV'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_source=>'select pq_constantes.fn_retorna_constante(0, ''cn_ttr_id_ing_devolucion'') from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498858207732863456)
,p_name=>'P32_TTR_NC'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_source=>'select pq_constantes.fn_retorna_constante(0,''cn_ttr_id_nota_credito'') from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498858278282863457)
,p_name=>'P32_DESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_item_default=>'TRUNC(SYSDATE -30,''mm'')'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Desde'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34498858401975863458)
,p_name=>'P32_HASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Hasta'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(80173533476553447362)
,p_name=>'P32_PAGINA_ORIGEN'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(43608275519239507)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return asdm_p.pq_constantes.fn_retorna_constante(pn_emp_id    => :F_EMP_ID, ',
'                                                 pv_constante => ''cn_pag_efectivizacion'');'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>'EFECTIVIZACION'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp.component_end;
end;
/
