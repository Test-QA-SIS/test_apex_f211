prompt --application/pages/page_00138
begin
--   Manifest
--     PAGE: 00138
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>138
,p_name=>'Reverse Renegociacion'
,p_step_title=>'Reverse Renegociacion'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(238614837384561552)
,p_plug_name=>'Datos Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(238629043804676932)
,p_name=>'Creditos Renegociados'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT cxc.cxc_id, cxc.cxc_saldo, cxc.com_id',
'    FROM car_cuentas_por_cobrar cxc, CAR_RENEGOCIACIONES_MAYOREO REN',
'   WHERE cxc.cli_id = :p138_cli_id',
'     AND cxc.cxc_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'     AND cxc.emp_id = :f_emp_id',
'     AND cxc.cxc_saldo > 0',
'     AND cxc.cxc_estado =',
'         pq_constantes.fn_retorna_constante(0, ''cv_cxc_estado_generada'')',
'     AND cxc.ren_id IS NOT NULL',
'     AND CXC.REN_ID=REN.REN_ID ',
'     AND TRUNC(REN.RMA_FECHA) = TRUNC(SYSDATE)',
'order by 1 desc'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222101656698745682)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:138:&SESSION.:cargar:&DEBUG.::P138_CXC_ID:#CXC_ID#'
,p_column_linktext=>'#CXC_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222102052803745682)
,p_query_column_id=>2
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222102482689745682)
,p_query_column_id=>3
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Comprobante Original'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(238635447968753871)
,p_plug_name=>'Alertas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>2
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(238636834986787937)
,p_name=>'Credito a Reversar'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_id,',
'       (SELECT ede.ede_abreviatura',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.ede_id = cx.ede_id) tipo_cartera,',
'       cx.cxc_saldo,',
'       cx.com_id,',
'       cx.cxc_fecha_adicion',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cxc_id = :p138_cxc_id'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222093092226745649)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222093513632745650)
,p_query_column_id=>2
,p_column_alias=>'TIPO_CARTERA'
,p_column_display_sequence=>5
,p_column_heading=>'Tipo Cartera'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222093856034745651)
,p_query_column_id=>3
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>2
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222094316080745651)
,p_query_column_id=>4
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Comprobante'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222094704847745651)
,p_query_column_id=>5
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>4
,p_column_heading=>'Fecha Adicion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(479635656306477525)
,p_name=>unistr('Nota de D\00E9bito')
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''AND'''', this.value ,''''P138_UGE_ID'''',''''P138_USER_ID'''',''''P138_EMP_ID'''',''''P138_CLI_ID'''',''''R257556031056732004'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis ',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'')'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222089173571745646)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222089573401745647)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222090042593745647)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222090395545745647)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222090815098745647)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222091231256745647)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INTMORA'
,p_use_as_row_header=>'N'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222091585800745648)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>7
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P138_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222091969978745648)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>8
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P138_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222092360337745649)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>9
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P138_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'11'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(479636837044481406)
,p_name=>'Nota de Credito'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''ANC'''', this.value ,''''P138_UGE_ID'''',''''P138_USER_ID'''',''''P138_EMP_ID'''',''''P138_CLI_ID'''',''''R257557211794735885'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis /*,',
'c020 folio_col*/',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222085271260745629)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222085671484745635)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222086140966745635)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222086534430745635)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222086874947745636)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222087285176745636)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INTMORA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222087685864745636)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>7
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P138_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222088053441745636)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>8
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P138_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(222088470812745636)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>9
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P138_VALIDA_PUNTO = ''P'''
,p_display_when_condition2=>'PLSQL'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7207838765223354258)
,p_name=>'PRUEBA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'   SELECT C020 FOLIO, C010 VALOR_NOTA_DEBITO',
'        FROM APEX_COLLECTIONS',
'       WHERE COLLECTION_NAME =',
'             PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,',
'                                                ''cv_col_nd_refinanciamiento'');'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7207838918023354259)
,p_query_column_id=>1
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>1
,p_column_heading=>'Folio'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7207838980017354260)
,p_query_column_id=>2
,p_column_alias=>'VALOR_NOTA_DEBITO'
,p_column_display_sequence=>2
,p_column_heading=>'Valor nota debito'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(222095075648745651)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_button_name=>'REVERSAR'
,p_button_static_id=>'REVERSAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Reversar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:138:&SESSION.:REVERSAR:&DEBUG.:::'
,p_button_condition_type=>'NEVER'
,p_button_cattributes=>'class="redirect"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(222095520189745667)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_button_name=>'PR_REVERSO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'REVERSO'
,p_button_position=>'BOTTOM'
,p_button_condition=>':P138_FECHA_CREDITO=TRUNC(SYSDATE)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(222104600459745707)
,p_branch_name=>'REVERSO'
,p_branch_action=>'f?p=&APP_ID.:138:&SESSION.:REVERSO:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(222095520189745667)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222080303181745575)
,p_name=>'P138_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222080726743745586)
,p_name=>'P138_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_lov_cascade_parent_items=>'P138_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P138_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222081066456745586)
,p_name=>'P138_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222081527130745586)
,p_name=>'P138_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>80
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222081868884745586)
,p_name=>'P138_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222082301565745592)
,p_name=>'P138_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222082687418745592)
,p_name=>'P138_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Telofono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222083122973745593)
,p_name=>'P138_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222083546439745593)
,p_name=>'P138_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>80
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222083861306745593)
,p_name=>'P138_UGE_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Uge Id'
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222084298209745593)
,p_name=>'P138_USER_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'User Id'
,p_source=>'F_USER_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222084682404745593)
,p_name=>'P138_EMP_ID'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(238614837384561552)
,p_prompt=>'Emp Id'
,p_source=>'F_EMP_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222095916716745668)
,p_name=>'P138_CXC_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222096315808745668)
,p_name=>'P138_FECHA_CREDITO'
,p_item_sequence=>115
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Fecha Credito'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.cxc_fecha_adicion',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cxc_id = :p138_cxc_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222096683596745669)
,p_name=>'P138_X'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_use_cache_before_default=>'NO'
,p_item_default=>':F_USER_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'X'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222097073010745673)
,p_name=>'P138_COM_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Com Id'
,p_source=>'SELECT cxc.com_id from car_cuentas_por_cobrar cxc WHERE cxc.cxc_id=:p138_cxc_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222097452818745673)
,p_name=>'P138_VALOR_NC'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Nc'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(C.COM_TOTAL_FACTURA) FROM ven_comprobantes c WHERE c.REN_ID=:P138_REN_ID',
'AND C.COM_TIPO = ''NC''',
'       AND C.TTR_ID=PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_ttr_id_nota_credito_int_pc'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222097874456745674)
,p_name=>'P138_VALOR_ND'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Nd'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(C.COM_TOTAL_FACTURA) FROM ven_comprobantes c WHERE c.REN_ID=:P138_REN_ID AND c.com_tipo=''ND''',
'AND C.TTR_ID=PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0, ''cn_ttr_id_nota_debito_refin'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222098267014745674)
,p_name=>'P138_CXC_ID_REF'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cxc Id Ref'
,p_source=>'SELECT R.CXC_ID_REFIN FROM CAR_CUENTAS_POR_COBRAR R WHERE R.CXC_ID=:P138_CXC_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222098718593745674)
,p_name=>'P138_PUE_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_prompt=>'Pue Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222099144920745674)
,p_name=>'P138_PUE_NUM_SRI'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_prompt=>'Pue Num Sri'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222099480644745675)
,p_name=>'P138_UGE_NUM_EST_SRI'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_prompt=>'Uge Num Est Sri'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222099906216745675)
,p_name=>'P138_VALIDA_PUNTO'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222100299688745675)
,p_name=>'P138_ALERTA'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_item_default=>'''SOLO SE PUEDE REALIZAR EL REVERSO EL MISMO DIA DEL REFINANCIAMIENTO'''
,p_prompt=>'Alerta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P138_FECHA_CREDITO!=TRUNC(SYSDATE)'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(222101037727745681)
,p_name=>'P138_ALERTA_1'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(238635447968753871)
,p_item_default=>'Solo se puede Reversar Refinanciamientos del mismo Dia'
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:10;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(225104460182459139)
,p_name=>'P138_REN_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(238636834986787937)
,p_prompt=>'Ren id'
,p_source=>'SELECT cxc.REN_ID from car_cuentas_por_cobrar cxc WHERE cxc.cxc_id=:p138_cxc_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(222102873255745687)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_cliente_existe      VARCHAR2(100);',
'  ln_dir_id              asdm_direcciones.dir_id%TYPE;',
'  lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'  ln_cli_id_referido     asdm_clientes.cli_id%TYPE;',
'BEGIN',
'  pq_ven_listas.pr_datos_clientes(:f_uge_id,',
'                                  :f_emp_id,',
'                                  :p138_identificacion,',
'                                  :p138_nombre,',
'                                  :p138_cli_id,',
'                                  :p138_correo,',
'                                  :p138_direccion,',
'                                  :p138_tipo_direccion,',
'                                  :p138_telefono,',
'                                  :p138_tipo_telefono,',
'                                  lv_cliente_existe,',
'                                  ln_dir_id,',
'                                  ln_cli_id_referido,',
'                                  lv_nombre_cli_referido,',
'                                  :p138_tipo_identificacion,',
'                                  :p0_error);',
'',
'    APEX_COLLECTION.CREATE_OR_TRUNCATE_COLLECTION(P_COLLECTION_NAME => ''col_nd_refinanciamiento'');',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>189849721985980761
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(222103310087745692)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reversa'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'',
'',
'  pq_car_anulacion_refinan.pr_reverso_refinanciamiento(pn_cxc_id => :P138_CXC_ID,',
'                                                          pn_emp_id => :f_emp_id,',
'                                                          pn_uge_id => :f_uge_id,',
'                                                          pn_usu_id => :F_USER_ID,',
'                                                          pn_uge_id_gasto => :F_UGE_ID_GASTO,',
'                                                          pn_cli_id => :P138_CLI_ID,',
'                                                          pn_tse_id => :f_seg_id,',
'                                                          PN_AGE_ID_AGENCIA  =>  :f_age_id,',
'                                                          PV_PUE_NUM_SRI     => :P0_PUE_NUM_SRI,',
'                                                          PV_UGE_NUM_EST_SRI =>  :P0_UGE_NUM_EST_SRI,',
'                                                          PN_PUE_ID         => :p138_pue_id,',
'                                                          pv_error => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(222095075648745651)
,p_process_when_type=>'NEVER'
,p_internal_uid=>189850158817980766
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(222103697443745694)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  PQ_CAR_ANULACION_RENEGO.pr_cargar_datos(pn_user_id => :f_user_id,',
'                                           pn_uge_id => :f_uge_id,',
'                                           pn_emp_id => :f_emp_id,',
'                                           pn_cxc_id => :p138_cxc_id,',
'                                           pn_cli_id => :p138_cli_id,',
'                                           pn_pue_id => :p138_pue_id,',
'                                           pv_error => :p0_error);',
'',
'',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri into',
':P138_PUE_NUM_SRI,:P138_UGE_NUM_EST_SRI',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = :p138_pue_id;',
'',
':P138_VALIDA_PUNTO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id =>:p138_pue_id);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cargar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>189850546173980768
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(222104068490745699)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_REVERSAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'if :P138_FECHA_CREDITO = TRUNC(sysdate) then',
'',
'  pq_car_anulacion_renego.pr_reverso_refinanciamiento(pn_cxc_id => :P138_CXC_ID,',
'                                                      pn_emp_id => :f_emp_id,',
'                                                      pn_uge_id => :f_uge_id,',
'                                                      pn_usu_id => :F_USER_ID,',
'                                                      pn_uge_id_gasto => :F_UGE_ID_GASTO,',
'                                                      pn_cli_id => :P138_CLI_ID,',
'                                                      pn_tse_id => :f_seg_id,',
'                                                      pn_age_id_agencia => :f_age_id_agencia,',
'                                                      pv_pue_num_sri => :P138_PUE_NUM_SRI,',
'                                                      pv_uge_num_est_sri => :P138_UGE_NUM_EST_SRI,',
'                                                      pn_pue_id => :p138_pue_id,',
'                                                      pv_error => :p0_error);',
'                                                      ',
'                                                      ',
'',
'apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nd_refinanciamiento''));',
'',
'',
'',
':P138_CXC_ID	:=NULL;',
':P138_COM_ID	:=NULL;',
':P138_VALOR_NC	:=NULL;',
':P138_VALOR_ND	:=NULL;',
':P138_CXC_ID_REF	:=NULL;',
':P138_FECHA_CREDITO :=NULL;',
'',
'else',
':p0_error:=''SOLO SE PUEDE REALIZAR EL REVERSO EL MISMO DIA QUE SE HACE EL REFINANCIAMIENTO'';',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'REVERSO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>189850917220980773
);
wwv_flow_imp.component_end;
end;
/
