prompt --application/pages/page_00110
begin
--   Manifest
--     PAGE: 00110
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>110
,p_name=>unistr('Reimpresi\00F3n refinanciamiento.')
,p_step_title=>unistr('Reimpresi\00F3n refinanciamiento.')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(157661281373917137)
,p_plug_name=>unistr('Reimpresi\00F3n de documentos')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(157662069609917140)
,p_plug_name=>'Clientes con refinanciamiento'
,p_parent_plug_id=>wwv_flow_imp.id(157661281373917137)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT col.c001 cxc_id,',
'       col.c002 cli_id,',
'       col.c003 identifiacion,',
'       col.c004 cliente,',
'       col.c005 com_id,',
'       col.c006 cxc_saldo,',
'       col.c007 uge_id,',
'       col.c008 agencia,',
'       col.c009 emp_id,',
'       col.c010 com_numero,',
'       col.c011 com_fecha,',
'       col.c012 com_tipo,',
'       col.c013 pol_id,',
'       col.c014 com_plazo,',
'       col.c015 ord_id,',
'       col.c016 ord_sec_id,',
'       col.c017 age_id_agente,',
'       col.c018 age_id_agencia,',
'       col.c019 com_sec_id ',
'FROM apex_collections col',
'WHERE col.collection_name = ''COLL_CLIENTES_REFIN'''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(157662281682917140)
,p_name=>'Clientes con refinanciamiento'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:110:&SESSION.:ver_opcion:&DEBUG.::P110_COM_ID,P110_COM_NUMERO,P110_CXC_ID,P110_CLIENTE,P110_COM_FECHA:#COM_ID#,#COM_NUMERO#,#CXC_ID#,#CLIENTE#,#COM_FECHA#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ETENESACA'
,p_internal_uid=>125409130413152214
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663269862917144)
,p_db_column_name=>'AGENCIA'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663361269917144)
,p_db_column_name=>'COM_NUMERO'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Com Numero'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663466025917144)
,p_db_column_name=>'COM_TIPO'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Com Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663578138917144)
,p_db_column_name=>'CXC_ID'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663673667917144)
,p_db_column_name=>'CLI_ID'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663773360917145)
,p_db_column_name=>'IDENTIFIACION'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Identifiacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'IDENTIFIACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663853811917145)
,p_db_column_name=>'CLIENTE'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663973819917145)
,p_db_column_name=>'CXC_SALDO'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Cxc Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157664055919917145)
,p_db_column_name=>'UGE_ID'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157664160081917145)
,p_db_column_name=>'EMP_ID'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662358512917142)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Com Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662483580917142)
,p_db_column_name=>'POL_ID'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662557011917142)
,p_db_column_name=>'COM_PLAZO'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Com Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662660775917142)
,p_db_column_name=>'ORD_ID'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662766045917143)
,p_db_column_name=>'ORD_SEC_ID'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Ord Sec Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662858596917143)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157662953063917143)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Age Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663071868917144)
,p_db_column_name=>'COM_SEC_ID'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Com Sec Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(157663157876917144)
,p_db_column_name=>'COM_ID'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(157664274157917145)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1254112'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'AGENCIA:COM_NUMERO:COM_TIPO:CXC_ID:CLI_ID:IDENTIFIACION:CLIENTE:CXC_SALDO:UGE_ID:EMP_ID:COM_FECHA:POL_ID:COM_PLAZO:ORD_ID:ORD_SEC_ID:AGE_ID_AGENTE:AGE_ID_AGENCIA:COM_SEC_ID:COM_ID'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(157664481401917149)
,p_plug_name=>unistr('Reimpresi\00F3n refinanciamiento')
,p_parent_plug_id=>wwv_flow_imp.id(157661281373917137)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>30
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_plug_display_when_condition=>'ver_opcion'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(80178122854626655964)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_button_name=>'BTN_RESERVA_DOMINIO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'Reserva Dominio'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(157664859766917149)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_button_name=>'PAGARE_REF'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'Pagare'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(157664678884917149)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_button_name=>'CRONOGRAMA_DE_PAGOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'Cronograma'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(157661460108917138)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(157661281373917137)
,p_button_name=>'CARGA_CLIENTES'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536673013046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'TOP'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(157666758753917154)
,p_branch_name=>'br_ver_opcion_imprimir'
,p_branch_action=>'f?p=&APP_ID.:110:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(157666972221917154)
,p_branch_name=>'imprime'
,p_branch_action=>'f?p=&APP_ID.:110:&SESSION.:imprime:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(157664678884917149)
,p_branch_sequence=>20
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(157667164748917154)
,p_branch_name=>'imprime_1'
,p_branch_action=>'f?p=&APP_ID.:110:&SESSION.:imprime_1:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(157664859766917149)
,p_branch_sequence=>30
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(80178123069561655966)
,p_branch_name=>'imprime_2'
,p_branch_action=>'f?p=&APP_ID.:110:&SESSION.:imprime_2:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(80178122854626655964)
,p_branch_sequence=>40
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157661664994917139)
,p_name=>'P110_FECHA_INI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(157661281373917137)
,p_prompt=>'Desde'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157661876856917140)
,p_name=>'P110_FECHA_FIN'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(157661281373917137)
,p_prompt=>'Hasta'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157665056051917149)
,p_name=>'P110_CLIENTE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157665252382917150)
,p_name=>'P110_CXC_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_prompt=>'Cxc Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157665465524917151)
,p_name=>'P110_COM_NUMERO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_prompt=>'Com Numero'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157665667448917151)
,p_name=>'P110_COM_FECHA'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_prompt=>'Com Fecha'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(157665875311917151)
,p_name=>'P110_COM_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(157664481401917149)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(157666455802917153)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_pagare_ref'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  pq_car_imprimir.pr_imprimir_doc_caja(pn_emp_id => to_number(:F_EMP_ID),',
'                                       pn_mon_id => NULL,',
'                                       pn_com_num => NULL,',
'                                       pn_com_id => to_number(:p110_com_id),',
'                                       pd_fecha => NULL,',
'                                       pv_num_identi => NULL,',
'                                       pn_cli_id => NULL,',
'                                       pn_documento => 12,',
'                                       pn_cxc_id => :P110_CXC_ID,',
'                                       pv_error => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(157664859766917149)
,p_process_when=>'imprime_1'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>125413304533152227
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(80178122998720655965)
,p_process_sequence=>40
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_reserva'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_cxc_id number;',
'ln_uge_id number;',
'begin',
'  -- Call the procedure',
'  pq_car_imprimir.pr_imprimir_doc_caja(pn_emp_id => to_number(:F_EMP_ID),',
'                                       pn_mon_id => NULL,',
'                                       pn_com_num => NULL,',
'                                       pn_com_id => to_number(:p110_com_id),',
'                                       pd_fecha => NULL,',
'                                       pv_num_identi => NULL,',
'                                       pn_cli_id => NULL,',
'                                       pn_documento => 45,',
'                                       pv_error => :p0_error);',
'                                       ',
'                                    ',
' /* SELECT MAX(c.cxc_id), max(uge_id)',
'        INTO ln_cxc_id, ln_uge_id',
'        FROM car_cuentas_por_cobrar c',
'       WHERE c.com_id = to_number(:p110_com_id);',
'    ',
' --  raise_application_error(-20000,''pruebas vp -''|| ln_cxc_id);   ',
'        pq_asdm_jaspereport.pr_ejecutar_reporte(pv_jre_nombre        =>  ''Car_PagareMarcimex_Completo_2'', ',
'                                                  pn_ambiente_id       => fn_get_ambiente(''REP''), --fn_get_ambiente(''REP''), --2mxsiccer,3prod',
'                                                  pv_nombre_parametros => ''ugeid:pn_cxc_id:val'',',
'                                                  pv_valor_parametros  =>  ln_uge_id || '':'' ||',
'                                                                          ln_cxc_id || '':3'',',
'                                                  pn_emp_id            => to_number(:F_EMP_ID),',
'                                                  pv_formato           => ''pdf'',',
'                                                  pv_error             => :p0_error);*/',
'                                ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(80178122854626655964)
,p_process_when=>'imprime_2'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>80145869847450891039
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(157666282066917153)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_clientes_refin'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  lv_query        CLOB;',
'  lv_nombres      apex_application_global.vc_arr2;',
'  lv_valores      apex_application_global.vc_arr2;',
'  lb_exists       BOOLEAN;',
'BEGIN',
'  lv_nombres(1) := ''cv_estado_reg_activo'';',
'  lv_nombres(2) := ''P110_FECHA_INI'';',
'  lv_nombres(3) := ''P110_FECHA_FIN'';',
'',
'  lv_valores(1) := ''0'';',
'  lv_valores(2) := v(''P110_FECHA_INI'');',
'  lv_valores(3) := v(''P110_FECHA_FIN'');',
'',
'',
'  lv_query := ''SELECT DISTINCT  cxc.cxc_id, cxc.cli_id,v.nro_identificacion, v.nombre_cliente, cxc.com_id, cxc.cxc_saldo, cxc.uge_id ,',
'(SELECT age.age_nombre_comercial FROM asdm_agencias age WHERE age.uge_id = cxc.uge_id) Agencia, co.emp_id,',
'               co.UGE_NUM_SRI || ''''-'''' || co.PUE_NUM_SRI || ''''-'''' ||',
'               LPAD(co.COM_NUMERO, 7, 0) com_numero,',
'               co.com_fecha,              ',
'               co.com_tipo,',
'               co.pol_id,',
'               co.com_plazo,',
'               co.ord_id,',
'               pq_ven_retorna_sec_id.fn_ord_sec_id(co.ord_id) ord_sec_id,',
'               co.age_id_agente,',
'               co.age_id_agencia,',
'               co.com_sec_id  ',
'FROM car_cuentas_por_cobrar cxc, v_car_cartera_clientes v, ven_comprobantes co',
'WHERE cxc.cxc_id_refin IS NOT NULL',
'AND   cxc.cxc_saldo > 0',
'AND   cxc.cxc_estado_registro = :cv_estado_reg_activo',
'AND   v.cli_id =  cxc.cli_id',
'AND   v.cxc_id = cxc.cxc_id',
'AND   co.com_id = cxc.com_id',
'--AND   co.com_fecha BETWEEN TRUNC(to_date(''''26/11/2012'''',''''dd/mm/yyyy'''')) AND TRUNC(to_date(''''30/11/2012'''',''''dd/mm/yyyy'''')+1)',
'AND   co.com_fecha BETWEEN trunc(to_date(:P110_FECHA_INI,''''DD/MM/YYYY'''')) AND  trunc(to_date(:P110_FECHA_FIN ,''''DD/MM/YYYY'''')+1)'';',
'',
'  lb_exists := apex_collection.collection_exists(p_collection_name => ''COLL_CLIENTES_REFIN'');',
'',
'  IF lb_exists THEN',
'    apex_collection.delete_collection(''COLL_CLIENTES_REFIN'');',
'  END IF;',
'',
'  apex_collection.create_collection_from_query_b(p_collection_name => ''COLL_CLIENTES_REFIN'',',
'                                                 p_query           => lv_query,',
'                                                 p_names           => lv_nombres,',
'                                                 p_values          => lv_valores);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>125413130797152227
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(157666064078917151)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_crono_ref'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  pq_car_imprimir.pr_imprimir_doc_caja(pn_emp_id => to_number(:F_EMP_ID),',
'                                       pn_mon_id => NULL,',
'                                       pn_com_num => NULL,',
'                                       pn_com_id => to_number(:p110_com_id),',
'                                       pd_fecha => NULL,',
'                                       pv_num_identi => NULL,',
'                                       pn_cli_id => NULL,',
'                                       pn_documento => 11,',
'                                       pn_cxc_id => :P110_CXC_ID,',
'                                       pv_error => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprime'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>125412912809152225
);
wwv_flow_imp.component_end;
end;
/
