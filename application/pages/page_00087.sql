prompt --application/pages/page_00087
begin
--   Manifest
--     PAGE: 00087
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>87
,p_name=>'DEPOSITOS REALES'
,p_step_title=>'DEPOSITOS REALES'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'function valida_numero(ln_valor)',
'{',
'var patron_numero =/^(?:\+|-)?\d+$/;',
'if (!patron_numero.test((ln_valor).value)) ',
'{',
unistr('alert(''Debe Ingresar solo n\00BF\00BFmeros'');'),
'ln_valor.value = '''';',
'html_GetElement((ln_valor).id).focus();',
'}',
'}',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(133643066571467071)
,p_name=>'Detalle Deposito Diario'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select MCA.MCA_ID,',
'       to_CHAR(MCA.MCA_FECHA, ''DD-MM-YYYY'') FECHA,',
'       mca.mca_observacion OBSERVACION,',
'       sum(MCD.MCD_VALOR_MOVIMIENTO) VALOR_DEPOSITO, mca.pca_id,',
'       APEX_ITEM.hidden(11, MCA.MCA_ID) DEPOSITO',
'  from asdm_movimientos_caja         mca,',
'       asdm_movimientos_caja_detalle mcd,',
'       asdm_transacciones            trx,',
'       asdm_tipos_transacciones      ttr,',
'       ven_periodos_caja             pca,',
'       asdm_puntos_emision           pue',
' where ttr.ttr_id = trx.ttr_id',
'   and trx.trx_id = mca.trx_id',
'   and mcd.mca_id = mca.mca_id',
'   and pca.pca_id = mca.pca_id',
'   and pca.emp_id = mca.emp_id',
'   and pue.pue_id = pca.pue_id',
'   and pue.uge_id = trx.uge_id',
'   and pue.emp_id = mca.emp_id',
'   and trx.uge_id = :f_uge_id',
'   and mca.emp_id = :f_emp_id',
'   and pue.pue_id = :p0_pue_id',
'   and mcd.tfp_id != pq_constantes.fn_retorna_constante(1,''cn_tfp_id_tarjeta_credito'')',
'   and trx.ttr_id IN',
'       (pq_constantes.fn_retorna_constante(NULL,',
'                                           ''cn_ttr_id_egr_caj_dep_tran''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_cierre_caja''))',
'   and not exists',
' (select drd.mca_id',
'          from ven_depositos_reales dre, ven_depositos_reales_det drd',
'         where drd.dre_id = dre.dre_id',
'           and drd.mca_id = mca.mca_id)',
' GROUP BY MCA.MCA_ID, to_CHAR(MCA.MCA_FECHA, ''DD-MM-YYYY''),mca.mca_observacion,',
'       mca.pca_id',
' ORDER BY FECHA           ',
''))
,p_display_when_condition=>'P87_clave'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133643273993467124)
,p_query_column_id=>1
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'MCA_ID'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133643382602467132)
,p_query_column_id=>2
,p_column_alias=>'FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133643771081467132)
,p_query_column_id=>3
,p_column_alias=>'OBSERVACION'
,p_column_display_sequence=>3
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133643468091467132)
,p_query_column_id=>4
,p_column_alias=>'VALOR_DEPOSITO'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133643564765467132)
,p_query_column_id=>5
,p_column_alias=>'PCA_ID'
,p_column_display_sequence=>5
,p_column_heading=>'PERIODO CAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133643663771467132)
,p_query_column_id=>6
,p_column_alias=>'DEPOSITO'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(133645265307467145)
,p_name=>'Depositos Reales'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MCA.MCA_ID, MCA.MCA_FECHA, MCA.MCA_OBSERVACION, ',
'''X'' ELIMINAR, MCA.MCA_TOTAL',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_display_when_condition=>'nvl(:P87_existe,0) > 0'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133645579503467146)
,p_query_column_id=>1
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>1
,p_column_heading=>'MCA ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133645673406467146)
,p_query_column_id=>2
,p_column_alias=>'MCA_FECHA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133645768267467146)
,p_query_column_id=>3
,p_column_alias=>'MCA_OBSERVACION'
,p_column_display_sequence=>3
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133645971224467146)
,p_query_column_id=>4
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>4
,p_column_heading=>'ELIMINAR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:87:&SESSION.:ELIMINA:&DEBUG.::P87_MCA_ID_ELIMINAR:#MCA_ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133645465873467146)
,p_query_column_id=>5
,p_column_alias=>'MCA_TOTAL'
,p_column_display_sequence=>5
,p_column_heading=>'TOTAL'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133646258819467147)
,p_plug_name=>'Ingreso Papeletas'
,p_parent_plug_id=>wwv_flow_imp.id(133645265307467145)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(133647683891467169)
,p_name=>'detalle depositos reales'
,p_parent_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select (SELECT sys_connect_by_path(ede.ede_descripcion, '' -> '') DETALLE',
'  FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_tede_cuenta_bancaria'')',
'   and ede.ede_estado_registro =',
'       pq_constantes.fn_retorna_constante(null, ''cv_estado_reg_activo'')',
'   and ede.emp_id = :F_EMP_ID',
'   and ede.ede_id = C003',
' START WITH ede.ede_id_padre IS NULL',
'CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre)Entidad,',
'C004 FECHA_PAPELETA,',
'C005 NUMERO_PAPELETA,',
'to_number(C006) VALOR',
'from apex_collections where collection_name = ''COLL_DEP_REAL'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133648059924467170)
,p_query_column_id=>1
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>1
,p_column_heading=>'ENTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133648157224467170)
,p_query_column_id=>2
,p_column_alias=>'FECHA_PAPELETA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA PAPELETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133647879461467170)
,p_query_column_id=>3
,p_column_alias=>'NUMERO_PAPELETA'
,p_column_display_sequence=>3
,p_column_heading=>'NUMERO PAPELETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(133647959438467170)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133648268299467190)
,p_plug_name=>'datos deposito real'
,p_parent_plug_id=>wwv_flow_imp.id(133647683891467169)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(133687958054785586)
,p_plug_name=>'CLAVE'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523372472046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133643878262467133)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(133643066571467071)
,p_button_name=>'CARGA_DT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133646462536467167)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_button_name=>'CARGA_DR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133648454187467190)
,p_button_sequence=>35
,p_button_plug_id=>wwv_flow_imp.id(133648268299467190)
,p_button_name=>'CANCELAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(133648664273467211)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(133648268299467190)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(133652869929467292)
,p_branch_action=>'f?p=&APP_ID.:87:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(133648664273467211)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 18-JAN-2012 15:25 by ACALLE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(133652579901467255)
,p_branch_action=>'f?p=&APP_ID.:87:&SESSION.::&DEBUG.:::'
,p_branch_point=>'BEFORE_HEADER'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 17-JAN-2012 10:27 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52966871635540399)
,p_name=>'P87_FECHAMIN'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(133643066571467071)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT MIN(MCA.MCA_FECHA)',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133644066653467134)
,p_name=>'P87_PCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133643066571467071)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pca Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select min(mca.pca_id)',
'  from asdm_movimientos_caja         mca,',
'       asdm_movimientos_caja_detalle mcd,',
'       asdm_transacciones            trx,',
'       asdm_tipos_transacciones      ttr,',
'       ven_periodos_caja             pca,',
'       asdm_puntos_emision           pue',
' where ttr.ttr_id = trx.ttr_id',
'   and trx.trx_id = mca.trx_id',
'   and mcd.mca_id = mca.mca_id',
'   and pca.pca_id = mca.pca_id',
'   and pca.emp_id = mca.emp_id',
'   and pue.pue_id = pca.pue_id',
'   and pue.uge_id = trx.uge_id',
'   and pue.emp_id = mca.emp_id',
'   and trx.uge_id = :f_uge_id',
'   and mca.emp_id = :f_emp_id',
'   and pue.pue_id = :p0_pue_id',
'   and mcd.tfp_id != pq_constantes.fn_retorna_constante(1,''cn_tfp_id_tarjeta_credito'')',
'   and trx.ttr_id IN',
'       (pq_constantes.fn_retorna_constante(NULL,',
'                                           ''cn_ttr_id_egr_caj_dep_tran''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_cierre_caja''))',
'   and not exists',
' (select drd.mca_id',
'          from ven_depositos_reales dre, ven_depositos_reales_det drd',
'         where drd.dre_id = dre.dre_id',
'           and drd.mca_id = mca.mca_id)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133644282775467143)
,p_name=>'P87_FECHA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(133643066571467071)
,p_prompt=>'FECHA'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133644476159467144)
,p_name=>'P87_EXISTE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(133643066571467071)
,p_use_cache_before_default=>'NO'
,p_prompt=>'EXISTE'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT count(*)',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133646083308467146)
,p_name=>'P87_MCA_ID_ELIMINAR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133645265307467145)
,p_prompt=>'Mca Id Eliminar'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133646671307467167)
,p_name=>'P87_NUMERO_PAPELETA'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_prompt=>'Numero Papeleta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange="doSubmit(''PAPELETA'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P87_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133646878807467168)
,p_name=>'P87_BANCO_DEPOSITO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_prompt=>unistr('Banco Dep\00BF\00BFsito')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENT_DESTINO_X_UGE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccion Banco---'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133647076193467168)
,p_name=>'P87_CUENTA_DEPOSITO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_prompt=>'Cuenta Deposito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion d,',
'       c.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  A.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id = :f_uge_id',
'       AND d.ede_id = :p87_banco_deposito   AND A.UCB_ESTADO_REGISTRO =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'ORDER  BY a.ucb_prioridad'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccione Cuenta---'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P87_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133647272549467168)
,p_name=>'P87_VALOR_DEPOSITO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_prompt=>'Valor Deposito'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>unistr('onchange="javascript:patronRe = /^\005Cd+\005C.?\005Cd*$/;if (!patronRe.test(this.value)) {alert(''Debe Ingresar un valor v\00BF\00BFlido'');this.value = '''';{html_GetElement(''P85_VALOR_DEPOSITO'').focus();}}" ')
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P87_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133647460571467169)
,p_name=>'P87_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(133646258819467147)
,p_prompt=>'Fecha Deposito'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P87_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133648861753467211)
,p_name=>'P87_OBSERVACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(133648268299467190)
,p_prompt=>'MOTIVO DESCUADRE'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>45
,p_cMaxlength=>100
,p_cHeight=>2
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P87_VALOR_DESCUADRE != 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133649071464467211)
,p_name=>'P87_VALOR_DESCUADRE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(133648268299467190)
,p_use_cache_before_default=>'NO'
,p_prompt=>'DESCUADRE'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(MCA.MCA_TOTAL),0) - :P87_TOTAL_PAPELETA',
'FROM ASDM_MOVIMIENTOS_CAJA MCA,',
'apex_collections COL',
'where COL.collection_name = ''COLL_DEP_TRA_REAL''',
'AND TO_NUMBER(COL.C001) = MCA.MCA_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'CARGA_DT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133649268892467211)
,p_name=>'P87_TOTAL_PAPELETA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(133648268299467190)
,p_use_cache_before_default=>'NO'
,p_prompt=>'TOTAL DEPOSITO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select nvl(SUM(to_number(c006)),0)',
'from apex_collections ',
'where collection_name = ''COLL_DEP_REAL'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(133692281219839575)
,p_name=>'P87_CLAVE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(133687958054785586)
,p_prompt=>'Clave de Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133650980076467215)
,p_validation_name=>'P87_OBSERVACION'
,p_validation_sequence=>10
,p_validation=>'P87_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el motivo del descuadre'
,p_validation_condition=>':P87_VALOR_DESCUADRE != 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(133648664273467211)
,p_associated_item=>wwv_flow_imp.id(133647460571467169)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133650182025467214)
,p_validation_name=>'P87_BANCO_DEPOSITO'
,p_validation_sequence=>20
,p_validation=>'P87_BANCO_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Banco'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133650578220467215)
,p_validation_name=>'P87_CUENTA_DEPOSITO'
,p_validation_sequence=>40
,p_validation=>'P87_CUENTA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Cuenta'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(133647460571467169)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133650782374467215)
,p_validation_name=>'P87_NUMERO_PAPELETA'
,p_validation_sequence=>50
,p_validation=>'P87_NUMERO_PAPELETA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Papeleta'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(133647460571467169)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133651165310467215)
,p_validation_name=>'P87_VALOR_DEPOSITO'
,p_validation_sequence=>60
,p_validation=>'P87_VALOR_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Valor'
,p_when_button_pressed=>wwv_flow_imp.id(131672868279821498)
,p_associated_item=>wwv_flow_imp.id(133647272549467168)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(133703268845892679)
,p_validation_name=>'P87_CLAVE'
,p_validation_sequence=>70
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p87_clave = pq_ven_depositos_reales.fn_clave_depositos_reales(:f_emp_id) then',
'return null;',
'else ',
':p87_clave := null;',
'return ''CLAVE INCORRECTA'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'CLAVE INCORRECTA'
,p_associated_item=>wwv_flow_imp.id(133692281219839575)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(141065763742092329)
,p_validation_name=>'P87_NUMERO_PAPELETA'
,p_validation_sequence=>80
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if pq_ven_movimientos_caja.fn_papeletas_ingresadas(:f_emp_id,:P87_NUMERO_PAPELETA) IS NULL',
' then',
'return null;',
'else ',
':P87_NUMERO_PAPELETA := null;',
'return ''YA EXISTE UNA PAPELETA INGRESADA CON ESE NUMERO'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'YA EXISTE UNA PAPELETA INGRESADA CON ESE NUMERO'
,p_validation_condition=>'PAPELETA'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(133646671307467167)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(52970279731552210)
,p_validation_name=>'P87_FECHA_DEPOSITO'
,p_validation_sequence=>90
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :P85_FECHA_DEPOSITO > trunc(SYSDATE) OR  :P85_FECHA_DEPOSITO IS NULL then',
'   return ''LA FECHA DE LA PAPELETA NO PUEDE SER MENOR A LA DEL CIERRE O MAYOR A LA FECHA ACTUAL'';',
'else',
'   return null;',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_error_message=>'ERROR'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(133646462536467167)
,p_associated_item=>wwv_flow_imp.id(133647460571467169)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133652281677467253)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_dep_tran'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_depositos_reales.pr_col_dep_tra_real(pv_error  => :p0_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(131205182939566836)
,p_internal_uid=>101399130407702327
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133651878701467252)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_dep_real'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_depositos_reales.pr_col_dep_real(pn_emp_id         => :f_emp_id,',
'                pn_uge_id         => :f_uge_id,',
'                pn_ede_id         => :p87_cuenta_deposito,',
'                pn_fecha_papeleta => :p87_fecha_deposito,',
'                pn_num_papeleta   => :p87_numero_papeleta,',
'                pn_valor_papeleta => :p87_valor_deposito,',
'                pv_error          => :p0_error);',
':P87_BANCO_DEPOSITO := NULL;',
':P87_FECHA_DEPOSITO := NULL;',
':P87_CUENTA_DEPOSITO := NULL;',
':P87_NUMERO_PAPELETA := NULL;',
':P87_VALOR_DEPOSITO := NULL;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(131672868279821498)
,p_process_when=>'CARGA_DR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101398727431702326
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133651278637467216)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'    END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'    END IF;',
'',
'',
':P87_BANCO_DEPOSITO := NULL;',
':P87_FECHA_DEPOSITO := NULL;',
':P87_CUENTA_DEPOSITO := NULL;',
':P87_NUMERO_PAPELETA := NULL;',
':P87_VALOR_DEPOSITO := NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'INGRESAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101398127367702290
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133651469900467252)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_quita_seleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_seq_id number;',
'begin',
'',
'select to_number(seq_id)',
'into ln_seq_id',
'from apex_collections WHERE COLLECTION_NAME = ''COLL_DEP_TRA_REAL''',
'AND c001 = :P87_MCA_ID_ELIMINAR;',
'',
'apex_collection.delete_member(p_collection_name => ''COLL_DEP_TRA_REAL'',p_seq => ln_seq_id);',
'',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'    END IF;',
':P87_BANCO_DEPOSITO := NULL;',
':P87_FECHA_DEPOSITO := NULL;',
':P87_CUENTA_DEPOSITO := NULL;',
':P87_NUMERO_PAPELETA := NULL;',
':P87_VALOR_DEPOSITO := NULL;',
'',
'EXCEPTION WHEN NO_DATA_FOUND THEN',
'NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101398318630702326
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133651676732467252)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_deposito_real'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_depositos_reales.pr_deposito_real_efe_che(pn_emp_id              => :f_emp_id,',
'                         pn_uge_id              => :f_uge_id,',
'                         pn_user_id             => :f_user_id,',
'                         pn_uge_id_gasto        => :f_uge_id_gasto,',
'                         pv_dre_razon_descuadre => :p87_observacion,',
'                         pn_dre_valor_descuadre => :p87_valor_descuadre,',
'                         pn_pca_id              => :p87_pca_id,--:f_pca_id,',
'                         pv_error               => :p0_error);',
'if :p0_error is null then',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'END IF;',
'',
'',
':P87_BANCO_DEPOSITO := NULL;',
':P87_FECHA_DEPOSITO := NULL;',
':P87_CUENTA_DEPOSITO := NULL;',
':P87_NUMERO_PAPELETA := NULL;',
':P87_VALOR_DEPOSITO := NULL;',
':P87_PCA_ID := NULL;',
':P87_existe := NULL;',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'DEPOSITO GRABADO CORRECTAMENTE'
,p_internal_uid=>101398525462702326
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(133652064894467252)
,p_process_sequence=>30
,p_process_point=>'ON_SUBMIT_BEFORE_COMPUTATION'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cancelar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_TRA_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_TRA_REAL'');',
'END IF;',
'IF apex_collection.collection_exists(p_collection_name => ''COLL_DEP_REAL'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COLL_DEP_REAL'');',
'END IF;',
'',
'',
':P87_BANCO_DEPOSITO := NULL;',
':P87_FECHA_DEPOSITO := NULL;',
':P87_CUENTA_DEPOSITO := NULL;',
':P87_NUMERO_PAPELETA := NULL;',
':P87_VALOR_DEPOSITO := NULL;',
':P87_PCA_ID := NULL;',
':P87_existe := NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CANCELAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101398913624702326
);
wwv_flow_imp.component_end;
end;
/
