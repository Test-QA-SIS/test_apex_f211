prompt --application/pages/page_00075
begin
--   Manifest
--     PAGE: 00075
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>75
,p_name=>'Provision Pago Puntual /  Garantia Real'
,p_step_title=>'Provision Pago Puntual /  Garantia Real'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(111683575051560105)
,p_plug_name=>'Report 1'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'c001,',
'c002 Unidad_gestion,',
'decode(c003,1,''PAGO_PUNTUAL'',2,''GARANTIA_REAL'') TIPO_PROVISION,',
'to_number(c004) CUENTA_X_COBRAR,',
'to_number(c005) CLIENTE,',
'(select nombre_completo',
'from v_asdm_datos_clientes cli where cli.cli_id = to_number(c005)',
'and cli.emp_id = :f_emp_id) NOMBRES,',
'c007 DIV_ID,',
'c008 NRO_VENCIMIENTO,',
'c009 FECHA_VENCIMIENTO,',
'to_number(c010) VALOR_CUOTA,',
'c011 VAR_ID,',
'to_number(c012) REABAJA',
'from apex_collections where collection_name = ''COL_PROVISIONES_PG'''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(111683665868560105)
,p_name=>'Report 1'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_csv_output_separator=>';'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>79430514598795179
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111684061441560428)
,p_db_column_name=>'C001'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'C001'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111696157535627043)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111696262200627090)
,p_db_column_name=>'TIPO_PROVISION'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Tipo Provision'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111696564560627094)
,p_db_column_name=>'NOMBRES'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Nombres'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111696654809627095)
,p_db_column_name=>'DIV_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111696758829627095)
,p_db_column_name=>'NRO_VENCIMIENTO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Nro Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111696873142627095)
,p_db_column_name=>'FECHA_VENCIMIENTO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Fecha Vencimiento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(111697051557627126)
,p_db_column_name=>'VAR_ID'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Var Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63162854159645787)
,p_db_column_name=>'CUENTA_X_COBRAR'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Cuenta X Cobrar'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CUENTA_X_COBRAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63163153604645791)
,p_db_column_name=>'CLIENTE'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63163263331645791)
,p_db_column_name=>'VALOR_CUOTA'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Valor Cuota'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(63163374809645791)
,p_db_column_name=>'REABAJA'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Rebaja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'REABAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(111690574111560693)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'794375'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'UNIDAD_GESTION:TIPO_PROVISION:CUENTA_X_COBRAR:CLIENTE:NOMBRES:DIV_ID:NRO_VENCIMIENTO:FECHA_VENCIMIENTO:VALOR_CUOTA:REABAJA'
,p_sum_columns_on_break=>'REABAJA'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(111693659997580406)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(111683575051560105)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:72:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp.component_end;
end;
/
