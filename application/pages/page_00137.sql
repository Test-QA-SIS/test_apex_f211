prompt --application/pages/page_00137
begin
--   Manifest
--     PAGE: 00137
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>137
,p_name=>unistr('Dep\00F3sitos Recap')
,p_step_title=>unistr('Dep\00F3sitos Recap')
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'var numero_filas= 500;',
'function refresh_ir_report() {',
'          var lThis = $x(''loading-image'');',
'          var lHtml = ''<div class="loading-indicator" align="center"><img src="#IMAGE_PREFIX#ws/ajax-loader.gif" style="margin-right:8px;" align="absmiddle"/>Loading...</div>'';',
'          try {',
'                  lThis =  replaceHtml(lThis,lHtml);',
'          } catch(err) {',
'                  lThis.innerHTML = lHtml;',
'          }',
'          setTimeout(function () { gReport.pull(); lThis.innerHTML = ''''; }, 1);',
'  }',
'',
'',
'function actualiza_region()',
'{ ',
'     refresh_ir_report();',
'     $(''#depositorecap'').trigger(''apexrefresh'');',
'     //$a_report(''93455026648160010'',''1'',numero_filas,''15'');',
'}',
'',
'',
'function actualiza_recap(fila, seq_id,  valor_presente)',
'{',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_ACTUALIZA_RECAP'',0);',
'  ',
'  get.addParam(''x01'',seq_id);',
'  get.addParam(''x02'',valor_presente);',
'  gReturn = get.get();',
'  get = null;',
'  actualiza_region();',
'}',
'',
'',
'function actualiza_voucher(fila, seq_id,  valor_presente)',
'{',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_ACTUALIZA_VOUCHER'',0);',
'  ',
'  get.addParam(''x05'',seq_id);',
'  get.addParam(''x07'',valor_presente);',
'  gReturn = get.get();',
'  get = null;',
'  actualiza_region();',
'}',
'',
'function actualiza_autorizacion(fila, seq_id,  valor_presente)',
'{',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_ACTUALIZA_AUTORIZACION'',0);',
'  ',
'  get.addParam(''x05'',seq_id);',
'  get.addParam(''x07'',valor_presente);',
'  gReturn = get.get();',
'  get = null;',
'  actualiza_region();',
'}',
'',
'function actualiza_fecha(fila, seq_id,  valor_presente)',
'{',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_ACTUALIZA_FECHA'',0);',
'  ',
'  get.addParam(''x03'',seq_id);',
'  get.addParam(''x04'',valor_presente);',
'  gReturn = get.get();',
'  get = null;',
'  actualiza_region();',
'}',
'',
'',
'',
'function actualiza_cuenta_dep(fila, seq_id,  valor_presente)',
'{',
'var get = new htmldb_Get(null,$v(''pFlowId''),''APPLICATION_PROCESS=PR_ACTUALIZA_CUENTA'',0);',
'  ',
'  get.addParam(''x06'',seq_id);',
'  get.addParam(''x08'',valor_presente);',
'  gReturn = get.get();',
'  get = null;',
'  actualiza_region();',
'  doSubmit(''SUBMIT'');',
'}',
'',
'',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'ENARANJO'
,p_last_upd_yyyymmddhh24miss=>'20240110084458'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7418772461614459448)
,p_plug_name=>'Marcar Todos'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7511613001313251230)
,p_plug_name=>unistr('Dep\00BF\00BFsito')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(7511706892598544749)
,p_plug_name=>unistr('Dep\00BF\00BFsitos  Transitorios')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P137_CUENTA_DEPOSITO'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7512283104072080866)
,p_name=>'DEPOSITOS RECAP TARJETAS DE CREDITO'
,p_region_name=>'depositorecap'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.seq_id,',
'       c.c001 mca_id,',
'       c.c002 trx_id,',
'       c.c003 cli_id,',
'       c.c004 mca_fecha,',
'       c.c005 mca_observacion,',
'       c.c006 mca_total,',
'       c.c007 mcd_id,',
'       c.c008 ede_id,',
'       c.c009 tfp_id,',
'       f.tfp_descripcion tipo_pago,',
'       to_number(c.c010) mcd_valor_movimiento,',
'       ',
'              CASE',
'         WHEN c.c009 =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tfp_id_tarjeta_credito'') THEN',
'          apex_item.text(p_idx        => 15,',
'                         p_value      => c011,',
'                         p_attributes => ''unreadonly id="f12_'' || TRIM(rownum) || ''"'' ||',
'                                         ''" onchange="actualiza_autorizacion('' ||',
'                                         TRIM(rownum) || '','' || seq_id ||',
'                                         '',this.value)"'',',
'                         p_size       => 19)',
'         ELSE',
'          c.c017',
'       END mcd_nro_aut_ref,',
'       ',
'       ----c.c011 mcd_nro_aut_ref,',
'       c.c012 mcd_nro_lote,',
'       c.c013 mcd_nro_retencion,',
'       c.c014 mcd_nro_comprobante,',
'       c.c015 mcd_nro_tarjeta,',
'       CASE',
'         WHEN c.c009 =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tfp_id_tarjeta_credito'') THEN',
'          apex_item.text(p_idx        => 11,',
'                         p_value      => c016,',
'                         p_attributes => ''unreadonly id="f11_'' || TRIM(rownum) || ''"'' ||',
'                                         ''" onchange="actualiza_recap('' ||',
'                                         TRIM(rownum) || '','' || seq_id ||',
'                                         '',this.value)"'',',
'                         p_size       => 19)',
'         ELSE',
'          c.c016',
'       END mcd_nro_recap,',
'       CASE',
'         WHEN c.c009 =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tfp_id_tarjeta_credito'') THEN',
'          apex_item.text(p_idx        => 12,',
'                         p_value      => c017,',
'                         p_attributes => ''unreadonly id="f12_'' || TRIM(rownum) || ''"'' ||',
'                                         ''" onchange="actualiza_voucher('' ||',
'                                         TRIM(rownum) || '','' || seq_id ||',
'                                         '',this.value)"'',',
'                         p_size       => 19)',
'         ELSE',
'          c.c017',
'       END mcd_voucher,',
'       ',
'       CASE',
'         WHEN c.c009 =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tfp_id_tarjeta_credito'') THEN',
'          apex_item.date_popup(p_idx         => 13,',
'                               p_value       => c018,',
'                               p_date_format => ''dd-mm-yyyy'',',
'                               ',
'                               p_attributes => '' unreadOnly   onchange="actualiza_fecha('' ||',
'                                               TRIM(rownum) || '','' || seq_id ||',
'                                               '' ,this.value,1)"'',',
'                               p_size       => 19)',
'         ELSE',
'          c.c018',
'       END mcd_fecha_deposito,',
'       c.c019 mcd_dep_transitorio,',
'       c.c020 mcd_titular_tarjeta,',
'       c.c021 banco,',
'       c.c022 tarjeta,',
'       CASE',
'         WHEN c.c009 =',
'              pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                 ''cn_tfp_id_tarjeta_credito'') THEN',
'         ',
'          apex_item.popup_from_lov(p_idx        => 14,',
'                                   p_value      => c024,',
'                                   p_lov_name   => ''LOV_ENTIDADES_CUENTAS'',',
'                                   p_attributes => '' unreadOnly   onchange="actualiza_cuenta_dep('' ||',
'                                                   TRIM(rownum) || '','' ||',
'                                                   seq_id ||',
'                                                   '' ,this.value,1)"'',',
'                                   p_width      => 4)',
'       ',
'         ELSE',
'          c.c024',
'       END cuenta_deposito,',
'       d.d cuenta',
'  FROM apex_collections c,',
'       asdm_tipos_formas_pago f,',
'       (SELECT regexp_replace(sys_connect_by_path((fn_limpia_cadena(ede.ede_descripcion)),',
'                            '' -> ''),'' -> '',''--'') AS d,',
'        ede.ede_id r',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.emp_id = :f_emp_id',
'           AND ede_tipo IN (SELECT ede_tipo',
'                              FROM asdm_tipos_entidades',
'                             WHERE ten_padre LIKE (''%TES%''))',
'           AND ede.ede_estado_registro =',
'               pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'         START WITH ede.ede_id_padre IS NULL',
'        CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre) d',
' WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''',
'   AND f.tfp_id = to_number(TRIM(c009))',
'   AND c024 = d.r(+)',
' ORDER BY c.seq_id;',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418832630096920973)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418832991286920977)
,p_query_column_id=>2
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>2
,p_column_heading=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418833362724920978)
,p_query_column_id=>3
,p_column_alias=>'TRX_ID'
,p_column_display_sequence=>3
,p_column_heading=>'TRX_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418833773185920979)
,p_query_column_id=>4
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>4
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418834167787920980)
,p_query_column_id=>5
,p_column_alias=>'MCA_FECHA'
,p_column_display_sequence=>6
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418834635878920983)
,p_query_column_id=>6
,p_column_alias=>'MCA_OBSERVACION'
,p_column_display_sequence=>7
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418834970385920984)
,p_query_column_id=>7
,p_column_alias=>'MCA_TOTAL'
,p_column_display_sequence=>8
,p_column_heading=>'MCA_TOTAL'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418835442747920985)
,p_query_column_id=>8
,p_column_alias=>'MCD_ID'
,p_column_display_sequence=>9
,p_column_heading=>'MCD_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418835776687920986)
,p_query_column_id=>9
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>10
,p_column_heading=>'EDE_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418836153424920986)
,p_query_column_id=>10
,p_column_alias=>'TFP_ID'
,p_column_display_sequence=>11
,p_column_heading=>'TFP_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418836577947920987)
,p_query_column_id=>11
,p_column_alias=>'TIPO_PAGO'
,p_column_display_sequence=>12
,p_column_heading=>'TIPO_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418837007271920989)
,p_query_column_id=>12
,p_column_alias=>'MCD_VALOR_MOVIMIENTO'
,p_column_display_sequence=>15
,p_column_heading=>'VALOR_MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418837385566920990)
,p_query_column_id=>13
,p_column_alias=>'MCD_NRO_AUT_REF'
,p_column_display_sequence=>22
,p_column_heading=>'Autorizaci&oacute;n'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418837753525920991)
,p_query_column_id=>14
,p_column_alias=>'MCD_NRO_LOTE'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418838204071920991)
,p_query_column_id=>15
,p_column_alias=>'MCD_NRO_RETENCION'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418838582814920992)
,p_query_column_id=>16
,p_column_alias=>'MCD_NRO_COMPROBANTE'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418838966627920993)
,p_query_column_id=>17
,p_column_alias=>'MCD_NRO_TARJETA'
,p_column_display_sequence=>19
,p_column_heading=>'NRO_TARJETA'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418839351655920993)
,p_query_column_id=>18
,p_column_alias=>'MCD_NRO_RECAP'
,p_column_display_sequence=>20
,p_column_heading=>'NRO_RECAP'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418839833415920994)
,p_query_column_id=>19
,p_column_alias=>'MCD_VOUCHER'
,p_column_display_sequence=>21
,p_column_heading=>'VOUCHER'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418840235589920995)
,p_query_column_id=>20
,p_column_alias=>'MCD_FECHA_DEPOSITO'
,p_column_display_sequence=>23
,p_column_heading=>'FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418840553534920995)
,p_query_column_id=>21
,p_column_alias=>'MCD_DEP_TRANSITORIO'
,p_column_display_sequence=>24
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418841044559920996)
,p_query_column_id=>22
,p_column_alias=>'MCD_TITULAR_TARJETA'
,p_column_display_sequence=>14
,p_column_heading=>'TITULAR_TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418841441165920997)
,p_query_column_id=>23
,p_column_alias=>'BANCO'
,p_column_display_sequence=>5
,p_column_heading=>'BANCO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418841755255920998)
,p_query_column_id=>24
,p_column_alias=>'TARJETA'
,p_column_display_sequence=>13
,p_column_heading=>'TARJETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418842221932920999)
,p_query_column_id=>25
,p_column_alias=>'CUENTA_DEPOSITO'
,p_column_display_sequence=>25
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418842636606920999)
,p_query_column_id=>26
,p_column_alias=>'CUENTA'
,p_column_display_sequence=>26
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(7521292985275745972)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *  FROM apex_collections C',
' WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''',
'order by C.SEQ_ID;;'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418845311557921010)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418845653678921012)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418846057292921013)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418846493162921014)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418846950721921014)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418847303931921015)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418847660065921020)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418848107643921020)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418848517559921021)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418848876163921021)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418849309794921022)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418849702751921022)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418850117596921023)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418850545278921023)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418850879004921024)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418851310483921024)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418851729964921025)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418852070539921025)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418852456364921027)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418852910389921028)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418853296963921028)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418853717080921030)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418854097455921031)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418854540468921031)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418854857796921032)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418855251889921032)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418855686044921033)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418856144525921041)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418856542197921042)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418856943598921046)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418857331570921047)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418857704867921047)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418858061148921048)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418858483502921048)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418858910901921049)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418859288570921049)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418859709892921050)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418860123909921051)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418860472830921051)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418860926146921052)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418861291652921052)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418861690053921053)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418862119399921053)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418862548839921054)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418862924644921054)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418863276316921055)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418863666765921055)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418864079700921056)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418864475741921056)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418864864666921057)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418865349367921057)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418865713227921058)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418866067602921058)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418866504419921059)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418866945926921059)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418867350388921068)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418867708225921069)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418868073460921069)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418868537252921070)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418868858055921070)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418869303198921071)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418869743694921071)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418870135593921072)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418870467424921075)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418870949105921076)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(7418871342013921078)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7418842976231921000)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(7512283104072080866)
,p_button_name=>'GUARDAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535660381046676)
,p_button_image_alt=>'Guardar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7418772423868459447)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(7418772461614459448)
,p_button_name=>'CARGAR_TODOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar todos'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(7418843370074921005)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(7512283104072080866)
,p_button_name=>'CANCELAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:59:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7418872456669921114)
,p_branch_action=>'f?p=&APP_ID.:137:&SESSION.:GUARDAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(7418842976231921000)
,p_branch_sequence=>20
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 03-JAN-2012 11:55 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7418872853875921118)
,p_branch_action=>'f?p=&APP_ID.:137:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>30
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'GUARDAR'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 03-JAN-2012 16:29 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(7418873255016921118)
,p_branch_action=>'f?p=&APP_ID.:37:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(7418843370074921005)
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 03-JAN-2012 16:30 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418771952893459443)
,p_name=>'P137_NRO_RECAP'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7418772461614459448)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418772145346459444)
,p_name=>'P137_VOUCHER'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7418772461614459448)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418772155598459445)
,p_name=>'P137_FECHA_DEP'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7418772461614459448)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>unistr('Fecha dep\00F3sito')
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418772280908459446)
,p_name=>'P137_ENTIDADES_CUEN'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(7418772461614459448)
,p_prompt=>unistr('Cuenta Dep\00F3sito')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ENTIDADES_CUENTAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  ',
' DECLARE',
'    lv_lov varchar2(3000);',
'BEGIN',
'lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ENTIDADES_CTA'');',
'RETURN (lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418828760874920942)
,p_name=>'P137_BANCO_DEPOSITO'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_prompt=>unistr('Banco Dep\00BF\00BFsito')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENT_DESTINO_X_UGE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccion Banco---'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418829192148920953)
,p_name=>'P137_CUENTA_DEPOSITO'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_prompt=>'Cuenta Deposito'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion d,',
'       c.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  A.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id = :f_uge_id',
'       AND d.ede_id = :p137_banco_deposito   AND A.UCB_ESTADO_REGISTRO =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')',
'ORDER  BY a.ucb_prioridad'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccione Cuenta---'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P137_BANCO_DEPOSITO'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418829555206920954)
,p_name=>'P137_TOTAL_DEP_TRANSITORIO'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Dep Transitorio'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sum(C.C010) total_movimiento',
'  FROM apex_collections C',
' WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''',
'and C.C024 IS NOT NULL',
'       AND C.C018 IS NOT NULL',
'       AND c.c016 IS NOT NULL',
'       AND C.C017 IS NOT NULL'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418830010474920955)
,p_name=>'P137_FECHA_DEPOSITO'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_prompt=>'Fecha Deposito'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P137_BANCO_DEPOSITO'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418830391996920955)
,p_name=>'P137_NUMERO_PAPELETA'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_prompt=>'Numero Papeleta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P137_BANCO_DEPOSITO'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418830752617920956)
,p_name=>'P137_VALOR_DESCUADRE'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Descuadre'
,p_source=>'nvl(:P137_TOTAL_DEP_TRANSITORIO,0)-nvl(:P137_VALOR_DEPOSITO,0)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418831232571920956)
,p_name=>'P137_RAZON_DESCUADRE'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(7511613001313251230)
,p_prompt=>'Razon Descuadre'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'NVL(:P137_TOTAL_DEP_TRANSITORIO,0)!=',
'NVL(:P137_VALOR_DEPOSITO,0)'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418831933946920959)
,p_name=>'P137_DEPOSITO_TRANSITORIO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(7511706892598544749)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deposito Transitorio'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select mca_fecha || '' - '' || sum(mcd_valor_movimiento) || '' - '' || TFP d, R',
'  from (SELECT a.mca_fecha,',
'               d.mcd_valor_movimiento,',
'               decode(d.tfp_id,',
'                      pq_constantes.fn_retorna_constante(a.emp_id,',
'                                                         ''cn_tfp_id_efectivo''),',
'                      ''Efectivo/Cheque'',',
'                      pq_constantes.fn_retorna_constante(a.emp_id,',
'                                                         ''cn_tfp_id_cheque''),',
'                      ''Efectivo/Cheque'',',
'                      pq_constantes.fn_retorna_constante(a.emp_id,',
'                                                         ''cn_tfp_id_tarjeta_credito''),',
unistr('                      ''Tarjeta Cr\00BF\00BFdito'') TFP,'),
'               a.mca_id r',
'          FROM asdm_movimientos_caja a, asdm_movimientos_caja_detalle d',
'         WHERE a.trx_id IN',
'               (SELECT trx_id',
'                  FROM asdm_transacciones',
'                 WHERE ttr_id IN',
'                       (pq_constantes.fn_retorna_constante(NULL,',
'                                                           ''cn_ttr_id_egr_caj_dep_tran''),',
'                        pq_constantes.fn_retorna_constante(NULL,',
'                                                           ''cn_ttr_id_cierre_caja'')))',
'           AND a.EDE_id =',
'               DECODE(:p137_CUENTA_DEPOSITO, NULL, 0, :p137_CUENTA_DEPOSITO)',
'           AND a.mca_id NOT IN',
'               (select DD.MCA_ID',
'                  from ven_depositos_reales d, ven_depositos_reales_det dd',
'                 where d.dre_id = dd.DRE_ID',
'                   AND DD.DRD_ESTADO_REGISTRO =',
'                       pq_constantes.fn_retorna_constante(NULL,',
'                                                          ''cv_estado_reg_activo'')',
'                   and d.dre_estado_registro =',
'                       pq_constantes.fn_retorna_constante(NULL,',
'                                                          ''cv_estado_reg_activo''))',
'           and a.mca_id = d.mca_id',
'           AND A.MCA_ID NOT IN',
'               (SELECT C001 MCA_ID',
'                  FROM apex_collections',
'                 WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''))',
' group by mca_fecha, TFP, R',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'---Seleccione Dep. Transitorio---'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418843774111921005)
,p_name=>'P137_VALOR_DEPOSITO'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(7512283104072080866)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Deposito'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(to_number(NVL(C.C010,0)))   FROM apex_collections C',
' WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''',
'       and C.C024 IS NOT NULL',
'       AND C.C018 IS NOT NULL',
'       AND c.c016 IS NOT NULL',
'       AND C.C017 IS NOT NULL;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-BOTTOM'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P137_BANCO_DEPOSITO'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418844188839921006)
,p_name=>'P137_PCA_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(7512283104072080866)
,p_prompt=>'Pca Id'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7418844552162921006)
,p_name=>'P137_PUE_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(7512283104072080866)
,p_prompt=>'PUE_ID'
,p_source=>'P0_PUE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7418872020687921112)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GUARDAR_DEP_REAL'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_VEN_DEPOSITOS_REALES.PR_GUARDAR_DEPOSITO_RECAP(                                         ',
'                                     pv_nombre_coleccion    => ''COL_DEPOSITOS_TRANSITORIOS'',',
'                                     pn_emp_id              =>:f_emp_id,',
'                                     pn_ede_id              =>:P137_CUENTA_DEPOSITO,',
'                                     pn_uge_id              =>:f_uge_id,',
'                                     pn_usu_id              =>:f_user_id,',
'                                     pd_drr_fecha           => sysdate,',
'                                     pn_drr_valor           =>:P137_VALOR_DEPOSITO,',
'                                     pn_pca_id              => :f_pca_id,',
'                                     PV_ERROR               =>:p0_error);',
'IF apex_collection.collection_exists(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'') THEN',
'    apex_collection.delete_collection(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'');',
'end if;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(7418842976231921000)
,p_process_when=>'GUARDAR'
,p_internal_uid=>7386618869418156186
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7418772607825459449)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_TODOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  CURSOR CU_TRANSITORIOS IS',
'    SELECT SEQ_ID ',
'     FROM apex_collections c,',
'       asdm_tipos_formas_pago f,',
'       (SELECT sys_connect_by_path(REPLACE(ede.ede_descripcion, ''%'', ''pct.''),',
'                                   '' -> '') AS d,',
'               ede.ede_id r',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.emp_id = :f_emp_id',
'           AND ede_tipo IN (SELECT ede_tipo',
'                              FROM asdm_tipos_entidades',
'                             WHERE ten_padre LIKE (''%TES%''))',
'           AND ede.ede_estado_registro =',
'               pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'         START WITH ede.ede_id_padre IS NULL',
'        CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre) d',
'        ',
' WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''',
'   AND f.tfp_id = to_number(trim(c009))',
'   AND c024 = d.r(+)',
' ORDER BY c.seq_id;',
' ',
'',
'BEGIN',
'  FOR REG IN CU_TRANSITORIOS LOOP',
'  ',
'   /* apex_collection.update_member_attribute(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'',',
'                                            p_seq             => REG.SEQ_ID,',
'                                            p_attr_number     => 16,',
'                                            p_attr_value      => :P137_NRO_RECAP);',
'  ',
'    apex_collection.update_member_attribute(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'',',
'                                            p_seq             => REG.SEQ_ID,',
'                                            p_attr_number     => 17,',
'                                            p_attr_value      => :P137_VOUCHER);*/',
'  ',
'    apex_collection.update_member_attribute(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'',',
'                                            p_seq             => REG.SEQ_ID,',
'                                            p_attr_number     => 18,',
'                                            p_attr_value      => :P137_FECHA_DEP);',
'  ',
'    apex_collection.update_member_attribute(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'',',
'                                            p_seq             => REG.SEQ_ID,',
'                                            p_attr_number     => 24,',
'                                            p_attr_value      => :P137_ENTIDADES_CUEN);',
'  ',
'  END LOOP;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(7418772423868459447)
,p_internal_uid=>7386519456555694523
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(7418871698063921110)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CREAR_COLECCION'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'  nom_coleccion CONSTANT VARCHAR2(30) := ''COL_DEPOSITOS_TRANSITORIOS'';',
'BEGIN  ',
'',
'',
'  pq_ven_depositos_reales.pr_crear_coleccion(''COL_DEPOSITOS_TRANSITORIOS'',',
'                                             :f_pca_id,',
'                                             :p137_pue_id,',
'                                             :f_emp_id);',
'',
'',
'DECLARE',
'  CURSOR CU_TRANSITORIOS IS',
'    SELECT SEQ_ID ',
'     FROM apex_collections c,',
'       asdm_tipos_formas_pago f,',
'       (SELECT sys_connect_by_path(REPLACE(ede.ede_descripcion, ''%'', ''pct.''),',
'                                   '' -> '') AS d,',
'               ede.ede_id r',
'          FROM asdm_entidades_destinos ede',
'         WHERE ede.emp_id = :f_emp_id',
'           AND ede_tipo IN (SELECT ede_tipo',
'                              FROM asdm_tipos_entidades',
'                             WHERE ten_padre LIKE (''%TES%''))',
'           AND ede.ede_estado_registro =',
'               pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'         START WITH ede.ede_id_padre IS NULL',
'        CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre) d',
'        ',
' WHERE collection_name = ''COL_DEPOSITOS_TRANSITORIOS''',
'   AND f.tfp_id = to_number(trim(c009))',
'   AND c024 = d.r(+)',
' ORDER BY c.seq_id;',
' ',
'',
'BEGIN',
'  FOR REG IN CU_TRANSITORIOS LOOP  ',
'    apex_collection.update_member_attribute(p_collection_name => ''COL_DEPOSITOS_TRANSITORIOS'',',
'                                            p_seq             => REG.SEQ_ID,',
'                                            p_attr_number     => 18,',
'                                            p_attr_value      => SYSDATE);',
'  ',
'',
'  ',
'  END LOOP;',
'',
'END;',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'recap'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>7386618546794156184
);
wwv_flow_imp.component_end;
end;
/
