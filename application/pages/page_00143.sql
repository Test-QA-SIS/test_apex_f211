prompt --application/pages/page_00143
begin
--   Manifest
--     PAGE: 00143
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>143
,p_name=>'factura'
,p_page_mode=>'MODAL'
,p_step_title=>'factura'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(28584123219306198548)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20240122125831'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(44817531761794385063)
,p_plug_name=>'DATOS FACTURA'
,p_plug_display_sequence=>10
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44817531899251385064)
,p_name=>'DETALLE'
,p_parent_plug_id=>wwv_flow_imp.id(44817531761794385063)
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ite_sec_id, c.ite_descripcion_larga, v.vcd_valor_variable',
'  FROM car_cuentas_por_cobrar a, ven_comprobantes_det b, inv_items c, ven_var_comprobantes_det v',
' WHERE b.com_id = a.com_id',
'   AND c.ite_sku_id = b.ite_sku_id',
'   and v.cde_id = b.cde_id',
'   and v.var_id = 955',
'   AND a.cxc_id =  :P143_CXC_ID',
'UNION',
'SELECT 1186, ''SEGURO'', 8.9',
'  FROM car_cuentas_por_cobrar cx',
' WHERE cx.cxc_id =  :P143_CXC_ID',
'   AND cx.ede_id = 1186',
'UNION',
'SELECT NULL,A.CEC_PRODUCTOS,NULL',
'FROM ASDM_E.CAR_CXC_EMPRESAS_CAB A',
'WHERE A.CXC_ID = :P143_CXC_ID',
'      AND ROWNUM = 1;',
'   ',
'   ',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532588310385071)
,p_query_column_id=>1
,p_column_alias=>'ITE_SEC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ite sec id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532662874385072)
,p_query_column_id=>2
,p_column_alias=>'ITE_DESCRIPCION_LARGA'
,p_column_display_sequence=>2
,p_column_heading=>'descripcion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44840790604905029527)
,p_query_column_id=>3
,p_column_alias=>'VCD_VALOR_VARIABLE'
,p_column_display_sequence=>3
,p_column_heading=>'Valor '
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(44817532050554385065)
,p_name=>'Factura'
,p_parent_plug_id=>wwv_flow_imp.id(44817531761794385063)
,p_template=>wwv_flow_imp.id(270520370913046666)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (SELECT u.uge_nombre',
'          FROM asdm_unidades_gestion u',
'         WHERE u.uge_id = a.uge_id) unidad_gestion,',
'       p.per_nro_identificacion,',
'       TRIM(p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'            p.per_primer_apellido || '' '' || p.per_segundo_apellido || '' '' ||',
'            p.per_razon_social) nombre,',
'       a.com_id,',
'       (SELECT d.uge_num_sri || ''-'' || d.pue_num_sri || ''-'' ||',
'               lpad(d.com_numero, 9, 0)',
'          FROM ven_comprobantes d',
'         WHERE d.com_id = a.com_id) folio,',
'       a.cxc_fecha_adicion fecha_venta,',
'       (SELECT e.ede_descripcion',
'          FROM asdm_entidades_destinos e',
'         WHERE e.ede_id = a.ede_id) cartera,',
'       CASE',
'         WHEN a.ede_id = 1186 THEN',
'          (SELECT p1.per_primer_apellido || '' '' || p1.per_segundo_apellido || '' '' ||',
'                  p1.per_primer_nombre || '' '' || p1.per_segundo_nombre',
'             FROM ASDM_E.car_cuentas_seguros se, asdm_personas p1, asdm_agentes t',
'            WHERE se.cxc_id = a.cxc_id',
'              and t.age_id = se.age_id_agente',
'              and t.per_id = p1.per_id)',
'         ELSE',
'          (SELECT p1.per_primer_apellido || '' '' || p1.per_segundo_apellido || '' '' ||',
'                  p1.per_primer_nombre || '' '' || p1.per_segundo_nombre',
'             FROM asdm_agentes t, ven_comprobantes v, asdm_personas p1',
'            WHERE t.age_id = v.age_id_agente',
'              AND t.per_id = p1.per_id',
'              AND v.com_id = a.com_id)',
'       END vendedor,',
'       (select p.pol_id||'' - ''||p.pol_descripcion_larga',
'       from ppr_politicas p',
'       where p.pol_id = a.pol_id) Politica',
'  FROM car_cuentas_por_cobrar a, asdm_clientes b, asdm_personas p',
' WHERE p.per_id = b.per_id',
'   AND b.cli_id = a.cli_id',
'   AND a.cxc_id = :P143_CXC_ID;',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>2
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532059085385066)
,p_query_column_id=>1
,p_column_alias=>'UNIDAD_GESTION'
,p_column_display_sequence=>1
,p_column_heading=>'Unidad Gestion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532174936385067)
,p_query_column_id=>2
,p_column_alias=>'PER_NRO_IDENTIFICACION'
,p_column_display_sequence=>2
,p_column_heading=>'Nro Identificacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532339090385068)
,p_query_column_id=>3
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>3
,p_column_heading=>'Nombre'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532390603385069)
,p_query_column_id=>4
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Com id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532494728385070)
,p_query_column_id=>5
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>5
,p_column_heading=>'Folio'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(44817532950889385074)
,p_query_column_id=>6
,p_column_alias=>'FECHA_VENTA'
,p_column_display_sequence=>6
,p_column_heading=>'Fecha venta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38525614563459238565)
,p_query_column_id=>7
,p_column_alias=>'CARTERA'
,p_column_display_sequence=>7
,p_column_heading=>'Cartera'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(38525614698224238566)
,p_query_column_id=>8
,p_column_alias=>'VENDEDOR'
,p_column_display_sequence=>8
,p_column_heading=>'Vendedor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(41474933640678565)
,p_query_column_id=>9
,p_column_alias=>'POLITICA'
,p_column_display_sequence=>9
,p_column_heading=>'Politica'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44817532773104385073)
,p_name=>'P143_CXC_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(44817532050554385065)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
