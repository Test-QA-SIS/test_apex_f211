prompt --application/pages/page_00070
begin
--   Manifest
--     PAGE: 00070
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>70
,p_name=>'NOTAS CREDITO POR REBAJA'
,p_step_title=>'NOTAS CREDITO POR REBAJA'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_updated_by=>'CAREVALO'
,p_last_upd_yyyymmddhh24miss=>'20240202152154'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(109284759924543058)
,p_plug_name=>'NOTA DE CREDITO POR REBAJA <B>&P0_TTR_DESCRIPCION.   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(112218952525103682)
,p_plug_name=>'NOTAS DE CREDITO APLICADAS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'       select *',
'  from v_ven_comprobantes c where c.COM_ID_REF=:p70_com_id',
'  and c.COM_ESTADO_REGISTRO=PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,''cv_estado_reg_activo'')'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(112219077425103686)
,p_name=>'NOTAS DE CREDITO APLICADAS'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>79965926155338760
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221465216108246)
,p_db_column_name=>'COM_ID'
,p_display_order=>1
,p_column_identifier=>'O'
,p_column_label=>'Com Id Nota Credito'
,p_column_link=>'f?p=&APP_ID.:70:&SESSION.::&DEBUG.::P70_COM_ID_NOTA:#COM_ID#'
,p_column_linktext=>'#COM_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220069443107985)
,p_db_column_name=>'USU_ID'
,p_display_order=>2
,p_column_identifier=>'A'
,p_column_label=>'Usu Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'USU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220176006108227)
,p_db_column_name=>'TRX_FECHA_EJECUCION'
,p_display_order=>3
,p_column_identifier=>'B'
,p_column_label=>'Trx Fecha Ejecucion'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_FECHA_EJECUCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220258749108227)
,p_db_column_name=>'TTR_ID'
,p_display_order=>4
,p_column_identifier=>'C'
,p_column_label=>'Ttr Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220382651108238)
,p_db_column_name=>'TTR_DESCRIPCION'
,p_display_order=>5
,p_column_identifier=>'D'
,p_column_label=>'Ttr Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220455028108238)
,p_db_column_name=>'POL_ID'
,p_display_order=>6
,p_column_identifier=>'E'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220557108108238)
,p_db_column_name=>'POL_DESCRIPCION_LARGA'
,p_display_order=>7
,p_column_identifier=>'F'
,p_column_label=>'Pol Descripcion Larga'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POL_DESCRIPCION_LARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220660255108240)
,p_db_column_name=>'PUE_NOMBRE'
,p_display_order=>8
,p_column_identifier=>'G'
,p_column_label=>'Pue Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220777397108240)
,p_db_column_name=>'UGE_ID_FACTURA'
,p_display_order=>9
,p_column_identifier=>'H'
,p_column_label=>'Uge Id Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220861980108240)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>10
,p_column_identifier=>'I'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112220964492108240)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>11
,p_column_identifier=>'J'
,p_column_label=>'Age Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221062946108246)
,p_db_column_name=>'AGE_NOMBRE_COMERCIAL'
,p_display_order=>12
,p_column_identifier=>'K'
,p_column_label=>'Age Nombre Comercial'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_NOMBRE_COMERCIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221183343108246)
,p_db_column_name=>'NOMBRE_COMPLETO'
,p_display_order=>13
,p_column_identifier=>'L'
,p_column_label=>'Nombre Completo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221277584108246)
,p_db_column_name=>'PER_TIPO_IDENTIFICACION'
,p_display_order=>14
,p_column_identifier=>'M'
,p_column_label=>'Per Tipo Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_TIPO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221375496108246)
,p_db_column_name=>'PER_NRO_IDENTIFICACION'
,p_display_order=>15
,p_column_identifier=>'N'
,p_column_label=>'Per Nro Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221583554108246)
,p_db_column_name=>'EMP_ID'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221678641108246)
,p_db_column_name=>'TRX_ID'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Trx Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221776830108246)
,p_db_column_name=>'PUE_ID'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221872233108246)
,p_db_column_name=>'CLI_ID'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112221968876108246)
,p_db_column_name=>'DIR_ID_ENVIAR_FACTURA'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Dir Id Enviar Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIR_ID_ENVIAR_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222051285108246)
,p_db_column_name=>'COM_TIPO'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Com Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222160321108246)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Com Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222352395108246)
,p_db_column_name=>'UGE_NUM_SRI'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Uge Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222454901108266)
,p_db_column_name=>'PUE_NUM_SRI'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Pue Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222561981108266)
,p_db_column_name=>'COM_ESTADO_REGISTRO'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Com Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222660489108266)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222759131108266)
,p_db_column_name=>'AGENTE'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222854680108269)
,p_db_column_name=>'ORD_ID'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112222963149108271)
,p_db_column_name=>'COM_OBSERVACION'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Com Observacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223079208108272)
,p_db_column_name=>'COM_SALDO'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Com Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223181748108280)
,p_db_column_name=>'COM_PLAZO'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Com Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223282944108317)
,p_db_column_name=>'EDE_ID'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223380384108317)
,p_db_column_name=>'COM_NRO_IMPRESION'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Com Nro Impresion'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NRO_IMPRESION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223466878108318)
,p_db_column_name=>'CIN_ID'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'Cin Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CIN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223574975108320)
,p_db_column_name=>'COM_ID_REF'
,p_display_order=>36
,p_column_identifier=>'AJ'
,p_column_label=>'Com Id Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_REF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(112223660565108320)
,p_db_column_name=>'TSE_ID'
,p_display_order=>37
,p_column_identifier=>'AK'
,p_column_label=>'Tse Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(53272675247402457)
,p_db_column_name=>'COM_NUMERO'
,p_display_order=>38
,p_column_identifier=>'AL'
,p_column_label=>'Com Numero'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(53272767365402457)
,p_db_column_name=>'PER_ID'
,p_display_order=>39
,p_column_identifier=>'AM'
,p_column_label=>'Per Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PER_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(53272869185402458)
,p_db_column_name=>'FOL_NRO_FOLIO'
,p_display_order=>40
,p_column_identifier=>'AN'
,p_column_label=>'Fol Nro Folio'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'FOL_NRO_FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(112224066531116672)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'799710'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'COM_ID:TRX_FECHA_EJECUCION:TTR_ID:TTR_DESCRIPCION:PUE_NOMBRE:UGE_ID_FACTURA:UNIDAD_GESTION:AGE_ID_AGENCIA:AGE_NOMBRE_COMERCIAL:NOMBRE_COMPLETO:PER_TIPO_IDENTIFICACION:PER_NRO_IDENTIFICACION:TRX_ID:PUE_ID:COM_TIPO:COM_FECHA:COM_NUMERO:UGE_NUM_SRI:PUE_'
||'NUM_SRI:AGENTE:COM_OBSERVACION:COM_SALDO:COM_NRO_IMPRESION:COM_ID_REF'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(109284960205543074)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_button_name=>'GUARDAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Guardar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37193361522888415)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_button_name=>'RECALCULAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Recalcular'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:98:&SESSION.::&DEBUG.::P98_COM_ID:&P70_COM_ID.'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(110807178399336556)
,p_branch_action=>'f?p=&APP_ID.:70:&SESSION.:GENERAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(109284960205543074)
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 28-NOV-2011 09:53 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37262161849787385)
,p_name=>'P70_UGE_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(53271370399401122)
,p_name=>'P70_COM_ID_NOTA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109285173220543128)
,p_name=>'P70_SECUENCIA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Secuencia'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P70_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109285383088543129)
,p_name=>'P70_CLIENTE'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Cliente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select per.per_nro_identificacion||'' '' || per.per_primer_nombre || '' '' || per.per_segundo_nombre || '' '' ||',
'              per.per_primer_apellido || '' '' || per.per_segundo_apellido clientes',
'         from asdm_personas per',
'        where per.per_id in',
'              (select cli.per_id',
'                 from asdm_clientes cli',
'                where cli.cli_id in (select c.cli_id',
'                                       from ven_comprobantes c',
'                                      where c.com_id = :P70_COM_ID));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>100
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109285577574543129)
,p_name=>'P70_CONCEPTO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Concepto Documento'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109285761487543130)
,p_name=>'P70_TOTAL'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Total'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109285981329543130)
,p_name=>'P70_CLAVE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109286180123543130)
,p_name=>'P70_FECHA_DOCUMENTO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Documento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109286372052543145)
,p_name=>'P70_CAJERO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Cajero'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  per.per_primer_nombre||'' ''||per.per_segundo_nombre||'' ''||per.per_primer_apellido||'' ''||per.per_segundo_apellido cajero',
'      FROM asdm_agentes age, asdm_personas per',
'     WHERE age.per_id = per.per_id',
'       AND age.usu_id = :F_USER_ID',
'       AND age.emp_id = :F_EMP_ID      ',
'       AND per.emp_id = :F_EMP_ID',
'       AND age.age_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'       AND per.per_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109286558044543146)
,p_name=>'P70_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Factura'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''Ref&'' || (SELECT a.secuencia_factura',
'                    FROM asdm_homologacion_com a',
'                   WHERE a.com_id = v.com_id) || '' '' || v.com_tipo || ''/'' ||',
'       v.com_id || ''- folio& '' || v.uge_num_sri || ''-'' || v.pue_num_sri || ''-'' ||',
'       v.com_numero || '' - Cliente& '' || per.per_razon_social || '' '' ||',
'       per.per_primer_apellido || '' '' || per.per_segundo_apellido || '' '' ||',
'       per.per_primer_nombre || '' '' || per.per_segundo_nombre || '' C.I.'' ||',
'       per.per_nro_identificacion d,',
'       v.com_id r',
'  FROM ven_comprobantes v, asdm_clientes cli, asdm_personas per',
' WHERE v.com_tipo = ''F''',
'   AND cli.per_id = per.per_id',
'   AND v.cli_id = cli.cli_id',
'   AND v.emp_id = cli.emp_id',
'   AND v.cli_id = :p70_cliente --edwinTG 11/01/2016',
'   AND ((v.tse_id = 1) or',
'       (:f_uge_id =',
'       asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                   ''cn_uge_temporal_nc_rebaja'')))',
'   AND trunc(v.com_fecha) >= ''01/01/2014''',
'   AND v.emp_id = :f_emp_id',
'   AND per.emp_id = v.emp_id',
'   AND cli.emp_id = per.emp_id',
'   AND per.per_estado_registro = ''0''',
'   AND v.com_estado_registro = ''0'''))
,p_cSize=>100
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109286763177543146)
,p_name=>'P70_PCA_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109306165783667676)
,p_name=>'P70_SECUENCIA_ACTUAL'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Secuencia Actual'
,p_source=>'P0_FOL_SEC_ACTUAL'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(172712466006091203)
,p_name=>'P70_PUE_ELECTRONICO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(313725163341203973)
,p_name=>'P70_MNC_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Motivo'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_MOTIVO_REBAJA_NC'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(500);',
'BEGIN',
'  lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_MOTIVO_REBAJA_NC'');',
'   RETURN(lv_lov);',
'END;',
'     '))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Selecione...'
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(313917771568652210)
,p_name=>'P70_CLIENTE_LOV'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(109284759924543058)
,p_prompt=>'Cliente'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(109308182883682053)
,p_validation_name=>'P70_SECUENCIA'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if trim(:P70_SECUENCIA) = trim(:P0_FOL_SEC_ACTUAL) then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_imp.id(109285173220543128)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(314009168613884002)
,p_name=>'da_carga_cliente'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P70_CLIENTE_LOV'
,p_condition_element=>'P70_CLIENTE_LOV'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(314009576745884125)
,p_event_id=>wwv_flow_imp.id(314009168613884002)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT per.cli_id INTO :P70_CLIENTE',
'  FROM v_asdm_datos_clientes per',
' WHERE per.per_estado_registro = 0',
'   AND per.cli_estado_registro = 0',
'   AND per.emp_id = :f_emp_id',
'   AND per.per_nro_identificacion = :p70_cliente_lov;'))
,p_attribute_02=>'P70_CLIENTE_LOV,F_EMP_ID'
,p_attribute_03=>'P70_CLIENTE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(53267758624397695)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_VEN_NCREDITO_REBAJA.pr_imprimir_nc(:F_EMP_ID,',
'                           :P70_COM_ID_NOTA,',
'                           :p0_error);',
'',
':P70_COM_ID_NOTA:=NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P70_COM_ID_NOTA'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>21014607354632769
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109287477586548147)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_DATOS_FOLIO_CAJA_USU'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%type;',
'BEGIN',
'',
'',
'  ln_tgr_id  :=  pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito'');',
'    :P0_TTR_ID := pq_constantes.fn_retorna_constante(:F_EMP_ID, ''cn_ttr_id_nota_credito_rebaja'');',
'',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'      :F_EMP_ID,',
'      :F_PCA_ID,',
'       LN_TGR_ID,',
'      :P0_TTR_DESCRIPCION, ',
'      :P0_PERIODO,',
'      :P0_DATFOLIO,',
'      :P0_FOL_SEC_ACTUAL,',
'      :P0_PUE_NUM_SRI  ,',
'      :P0_UGE_NUM_EST_SRI ,',
'      :P0_PUE_ID,',
'      pq_constantes.fn_retorna_constante(0,''cn_ttr_id_nota_credito'') ,--:P0_TTR_ID,',
'      :P0_NRO_FOLIO,',
'      :P0_ERROR);',
'',
':P70_PUE_ELECTRONICO:= pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :P0_PUE_ID);',
'IF :P70_PUE_ELECTRONICO= ''E'' or :P70_PUE_ELECTRONICO= ''P'' THEN ',
':P70_SECUENCIA :=:P0_FOL_SEC_ACTUAL;',
'END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>77034326316783221
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109786059882157627)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_NOTA_CREDITO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_num_nc NUMBER;',
'ln_cont number;',
'ln_tgr_id  number :=  pq_constantes.fn_retorna_constante(NULL, ''cn_tgr_id_nota_credito'');',
'BEGIN',
'begin',
'SELECT     f.fol_sec_actual',
'            INTO :p70_secuencia_actual',
'            FROM asdm_folios f',
'           WHERE f.pue_id = :p0_pue_id',
'             AND f.tgr_id = ln_tgr_id',
'             AND f.emp_id = :f_emp_id',
'             AND f.fol_estado_registro = ''0''',
'             AND f.fol_nro_folio =',
'                 (SELECT MIN(f.fol_nro_folio)',
'                    FROM asdm_folios f',
'                   WHERE f.pue_id = :p0_pue_id',
'                     AND f.tgr_id = ln_tgr_id',
'                     AND f.emp_id = :f_emp_id',
'                     AND f.fol_estado_registro = ''0'');',
'exception',
'  when no_data_found then',
'    :p70_secuencia_actual := null;',
'end;',
'',
'if :p70_secuencia_actual is not null then',
'  :p70_secuencia := :p70_secuencia_actual;',
'end if;',
'--SELECT count(*) into ln_cont FROM ven_comprobantes c, asdm_agencias a WHERE c.age_id_agencia=a.age_id AND a.age_compen_solidaria IS NOT NULL AND c.com_id=:p70_com_id  AND a.tse_id=2;',
'',
'',
'--if ln_cont=0 then',
'',
'',
'  IF :p70_secuencia = :p70_secuencia_actual THEN',
'',
' /*  EXECUTE IMMEDIATE ''SELECT COUNT(*)',
'  FROM ven_comprobantes co',
' WHERE co.com_id_ref = :p70_com_id',
'   AND EXISTS (SELECT NULL',
'          FROM asdm_e.asdm_motivos_nc mn',
'         WHERE mn.mnc_tipo = :cv_nc_x_rebaja',
'           AND mn.emp_id = :f_emp_id',
'           AND mn.mnc_estado_registro = :cv_estado_reg_activo',
'           AND mn.mnc_id = co.mnc_id)''',
'      INTO ln_num_nc',
'      USING :p70_com_id, pq_constantes.fn_retorna_constante(0, ''cv_nc_x_rebaja''),:f_emp_id, ''0'';',
'',
'',
'    IF ln_num_nc > 0 THEN',
'      raise_application_error(-20000,',
'                              ''YA SE ENCUENTRA RELIZADA UNA NC POR REBAJA'');',
'',
'    ELSE*/',
'',
'      IF :p70_mnc_id IS NOT NULL THEN',
'       asdm_p.pq_ven_ncredito_rebaja.pr_guarda_nota_credito(pn_com_id_fac => :p70_com_id,',
'                                                            pn_emp_id => :f_emp_id,',
'                                                            pn_uge_id => :f_uge_id,',
'                                                            pn_usu_id => :f_user_id,',
'                                                            pn_mnc_id => :p70_mnc_id,',
'                                                            pn_age_id_agencia => :f_age_id_agencia,',
'                                                            pn_fol_sec_actual => :p0_fol_sec_actual,',
'                                                            pv_pue_num_sri => :p0_pue_num_sri,',
'                                                            pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'                                                            pn_pue_id => :p0_pue_id,',
'                                                            pn_valor_aplicado => :p70_total,',
'                                                            pv_com_observacion => :p70_concepto,',
'                                                            pd_fecha_documento => :p70_fecha_documento,',
'                                                            pv_clave => :p70_clave,',
'                                                            pn_nro_folio => :p0_nro_folio,',
'                                                            pv_error => :p0_error);',
'        :p70_secuencia := NULL;',
'        :p70_clave     := NULL;',
'      ELSE',
unistr('        :p0_error := ''Seleccione un motivo de la Nota de cr\00E9dito'';'),
'      END IF;',
'  --- END IF;',
'  ',
'  ELSE',
'    :p0_error := ''La Secuencia del Folio no es la Correcta'';',
'  END IF;',
'',
'/*else',
unistr('raise_application_error(-20000,''No se puede generar notas de credito por rebaja a esa factura - bloqueo compensaci\00F3n solidaria - Comunicarse con Dep. Contabilidad'');'),
unistr('    :p0_error := ''No se puede generar notas de credito por rebaja a esa factura - bloqueo compensaci\00F3n solidaria'';'),
'end if;*/',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(109284960205543074)
,p_process_when=>'GENERAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>77532908612392701
);
wwv_flow_imp.component_end;
end;
/
