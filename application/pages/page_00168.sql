prompt --application/pages/page_00168
begin
--   Manifest
--     PAGE: 00168
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>168
,p_name=>'Movimientos Caja Matriz'
,p_step_title=>'Movimientos Caja Matriz'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script>',
'history.forward(); ',
'</script> '))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112518'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(715325755385559448)
,p_name=>'Movimientos Caja Detalle'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_movimientos_caja.fn_movimientos_detalle(:f_emp_id, :f_uge_id, :f_pca_id, :p0_error);'
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS_INITCAP'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Descargar a Excel'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_exp_separator=>';'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612056217489301865)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'COL01'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612056570139301875)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'COL02'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612057019720301875)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'COL03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612057399064301876)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'COL04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612057810296301876)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'COL05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612058209247301880)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'COL06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612058630064301880)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'COL07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612058961772301880)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'COL08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612059374343301881)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'COL09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612059791631301881)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'COL10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612060250608301881)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'COL11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612060648207301881)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'COL12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612061010298301882)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'COL13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612061379600301882)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'COL14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612061801789301882)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'COL15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612062242361301883)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'COL16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612062566720301883)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'COL17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612063016736301883)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'COL18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612063433235301883)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'COL19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612063840285301884)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'COL20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612064250755301885)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'COL21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612064615475301885)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'COL22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612065001830301885)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'COL23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612065430644301886)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'COL24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612065842978301886)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'COL25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612066197534301886)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'COL26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612066499262301887)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'COL27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612066904876301887)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'COL28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612067310701301887)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'COL29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612067700454301887)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'COL30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612068123305301888)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'COL31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612068488307301888)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'COL32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612068866510301888)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'COL33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612069335656301889)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'COL34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612069723416301889)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'COL35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612070127320301889)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'COL36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612070507156301889)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'COL37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612070921148301890)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'COL38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612071293932301895)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'COL39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612071729019301895)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'COL40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612072141864301895)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'COL41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612072516951301896)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'COL42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612072943710301896)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'COL43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612073338179301896)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'COL44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612073728297301896)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'COL45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612074084355301899)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'COL46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612074548494301902)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'COL47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612074924149301902)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'COL48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612075305697301902)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'COL49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612075698267301903)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'COL50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612076107247301903)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'COL51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612076464460301903)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'COL52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612076916790301904)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'COL53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612077292593301905)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'COL54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612077656603301905)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'COL55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612078067920301905)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'COL56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612078464590301906)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'COL57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612078858984301906)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'COL58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612079329947301906)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'COL59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(612079662621301906)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'COL60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(828271758202059188)
,p_plug_name=>'Movimientos  <b>&P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca.pca_id,',
'       mca.mca_fecha,',
'       mca.mca_id,',
'       ttr.ttr_descripcion,',
'       mca.cli_id,',
'       (SELECT TRIM(p.per_primer_apellido || '' '' || p.per_segundo_apellido || '' '' ||',
'                    p.per_primer_nombre || '' '' || p.per_segundo_nombre || '' '' ||',
'                    p.per_razon_social)',
'          FROM asdm_personas p, asdm_clientes c',
'         WHERE c.per_id = p.per_id',
'           AND c.cli_id = mca.cli_id',
'           AND c.cli_estado_registro = 0',
'           AND p.per_estado_registro = 0',
'           AND c.emp_id = :f_emp_id',
'           AND p.emp_id = :f_emp_id) cliente,       ',
'       decode(ttr.ttr_funcion, ''D'', ''Debito'', ''C'', ''Credito'') funcion,',
'       mca.mca_estado_mc,',
'       mca.mca_id_referencia,        ',
'       mca.mca_estado_registro,',
'       ''Detalle Movimiento'' detalle,',
'       (SELECT trx.mca_id',
'          FROM asdm_transacciones trx',
'         WHERE trx.trx_id = mca.trx_id) comprobante_contable,',
'       CASE',
'         WHEN ttr.ttr_funcion = ''D'' THEN',
'          mca.mca_total',
'       END ingreso,',
'       CASE',
'         WHEN ttr.ttr_funcion = ''C'' THEN',
'          mca.mca_total',
'       END egreso,',
'       SUM(CASE',
'             WHEN ttr.ttr_funcion = ''D'' THEN',
'              mca.mca_total',
'             WHEN ttr.ttr_funcion = ''C'' THEN',
'              mca.mca_total * -1',
'           END) over(PARTITION BY pca_id ORDER BY mca_fecha rows unbounded preceding) saldo,',
'      (select username from kseg_e.kseg_usuarios us where us.usuario_id = mca.usu_id) usuario',
'  FROM asdm_movimientos_caja mca, asdm_tipos_transacciones ttr',
' WHERE ttr.ttr_id = mca.ttr_id',
'       and mca.cli_id = :P168_CLI_ID',
'       and mca.uge_id = ASDM_P.PQ_CONSTANTES.fn_retorna_constante(:f_emp_id,''cn_uge_id_matriz'')',
'       and mca.emp_id = :f_emp_id'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(828271857673059188)
,p_name=>'Movimientos Caja'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No existen movimientos'
,p_allow_report_categories=>'N'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>796018706403294262
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612083241716301951)
,p_db_column_name=>'PCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Periodo<br>Caja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612083595840301951)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612084039604301951)
,p_db_column_name=>'MCA_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'#'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612084352391301952)
,p_db_column_name=>'TTR_DESCRIPCION'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>unistr('Descripci\00F3n')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'TTR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612084790867301952)
,p_db_column_name=>'CLI_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Id. Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612085217750301952)
,p_db_column_name=>'FUNCION'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>unistr('Funci\00F3n')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'FUNCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612085556105301952)
,p_db_column_name=>'MCA_ESTADO_MC'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_ESTADO_MC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612085956794301953)
,p_db_column_name=>'MCA_ESTADO_REGISTRO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612086432892301953)
,p_db_column_name=>'INGRESO'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Ingreso'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'INGRESO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612086794808301953)
,p_db_column_name=>'EGRESO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Egreso'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EGRESO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612087247410301953)
,p_db_column_name=>'SALDO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612087629510301954)
,p_db_column_name=>'DETALLE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Detalle'
,p_column_link=>'f?p=&APP_ID.:22:&SESSION.:prueba:&DEBUG.::P22_MCA_ID,P22_NOMBRE_CLIENTE,P22_NOMBREMOVIMIENTO,P22_PAGINA:#MCA_ID#,#CLIENTE#,#TTR_DESCRIPCION#,168'
,p_column_linktext=>'Detalle Movimiento'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'DETALLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612088035486301955)
,p_db_column_name=>'CLIENTE'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612082451096301949)
,p_db_column_name=>'MCA_ID_REFERENCIA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Mca Id Referencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_ID_REFERENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612082784090301951)
,p_db_column_name=>'COMPROBANTE_CONTABLE'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Comprobante <br>Contable'
,p_column_link=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.::P19_MCA_ID_CON:#COMPROBANTE_CONTABLE#'
,p_column_linktext=>'#COMPROBANTE_CONTABLE#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'COMPROBANTE_CONTABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(612088435683301955)
,p_db_column_name=>'USUARIO'
,p_display_order=>25
,p_column_identifier=>'P'
,p_column_label=>'Usuario'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(828275044330067031)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'5798356'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_report_columns=>'MCA_ID:PCA_ID:MCA_FECHA:TTR_DESCRIPCION:CLIENTE:CLI_ID:FUNCION:INGRESO:EGRESO:SALDO:DETALLE:MCA_ESTADO_MC:MCA_ID_REFERENCIA:COMPROBANTE_CONTABLE:USUARIO'
,p_sort_column_1=>'MCA_ID'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'MCA_FECHA'
,p_sort_direction_2=>'DESC'
,p_sum_columns_on_break=>'INGRESO:EGRESO'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(612080059970301911)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(715325755385559448)
,p_button_name=>'Imprimir_Recibos'
,p_button_action=>'REDIRECT_APP'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir recibos'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=283:66:&SESSION.:busca:&DEBUG.::F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT:&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL_DESC.,&P0_TREE_ROOT.'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(591298490708768965)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_button_name=>'btn_carga_datos_cliente'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar Datos Cliente'
,p_button_alignment=>'LEFT-CENTER'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(612080512307301917)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(715325755385559448)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=cierre_caja.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&pn_pca_id=&F_PCA_ID.&pn_emp_id=&F_EMP_ID.'', 850, 500,85));'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(612089619137301968)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_button_name=>'COMPROBANTES'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Listado Comprobantes'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'javascript:popupURL(''../../reports/rwservlet?module=listado_comprobantes.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&PD_FECHA_FIN=&P168_FECHA_FIN.&PD_FECHA_INI=&P168_FECHA_INI.&PN_AGE_ID=&F_AGE_ID_AGENCIA.&PN_EMP_ID=&F_EMP_ID.&PV_USU_ID=&F_USER_ID.'')'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(612089996087301969)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_button_name=>'PAGO_CUOTA'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'>>IR AL PAGO DE CUOTA'
,p_button_position=>'TOP_AND_BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:6::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(612090449644301969)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_button_name=>'new_pago'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo Pago de Cuota'
,p_button_position=>'TOP_AND_BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:7:&SESSION.::&DEBUG.:RP,7::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(612100034264302017)
,p_branch_name=>'Go To Page 19'
,p_branch_action=>'f?p=&APP_ID.:168:&SESSION.::&DEBUG.:RP:P168_MCA_ID:&P168_MCA_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(591297992144768960)
,p_name=>'P168_NRO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_prompt=>'Nro identificacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(591298135846768961)
,p_name=>'P168_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_prompt=>'Cli id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(591298270899768963)
,p_name=>'P168_NOMBRE_CLIENTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_prompt=>'Nombre cliente'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(612080906425301922)
,p_name=>'P168_MCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(715325755385559448)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(612081308053301939)
,p_name=>'P168_COM_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(715325755385559448)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(612081745173301939)
,p_name=>'P168_MCA_ID_CON'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(715325755385559448)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(612090763739301969)
,p_name=>'P168_FECHA_INI'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_use_cache_before_default=>'NO'
,p_item_default=>'trunc(sysdate)'
,p_source=>'to_char(sysdate,''dd-mm-yyyy'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(612091176868301970)
,p_name=>'P168_FECHA_FIN'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_use_cache_before_default=>'NO'
,p_item_default=>'trunc(sysdate)'
,p_source=>'to_char(sysdate,''dd-mm-yyyy'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(612091597664301970)
,p_name=>'P168_PAGO_REFINANCIAMIENTO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(828271758202059188)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when=>':P168_PAGO_REFINANCIAMIENTO = 1'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(612099028632302010)
,p_name=>'subMit'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(612099519011302013)
,p_event_id=>wwv_flow_imp.id(612099028632302010)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(612097880524302002)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_COMPROBANTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_mca_id_con NUMBER;',
'  ln_cont       NUMBER;',
'  ln_mca_id_nc  NUMBER;',
'',
'  CURSOR cu_mov_caja IS',
'    SELECT cx.*',
'      FROM asdm_e.car_movimientos_cab   ca,',
'           car_cuentas_por_cobrar       cx,',
'           asdm_movimientos_promociones pro',
'     WHERE ca.cxc_id = cx.cxc_id',
'       AND ca.mca_id_movcaja = :p168_mca_id',
'       AND cx.com_id = pro.com_id_fact_origen;',
'',
'BEGIN',
'',
'  IF :p168_mca_id IS NOT NULL THEN',
'  ',
'    pq_ven_movimientos_caja.pr_imprimir_mov_caja(pn_emp_id => :f_emp_id,',
'                                                 pn_mca_id => :p168_mca_id,',
'                                                 pv_error  => :p0_error);',
'  END IF;',
'',
'  pq_con_imprimir.pr_imprimir(pn_emp_id => :f_emp_id,',
'                              pn_mca_id => :p168_mca_id_con,',
'                              pn_mon_id => 1,',
'                              pv_error  => :p0_error);',
'',
'  FOR reg IN cu_mov_caja LOOP',
'  ',
'    pq_asdm_promociones.pr_imprimir_puntualito(pn_emp_id => reg.emp_id,',
'                                               pn_cli_id => reg.cli_id,',
'                                               pn_com_id => reg.com_id,',
'                                               pn_ttr_id => pq_constantes.fn_retorna_constante(0,',
'                                                                                               ''cn_ttr_id_genera_promo''),',
'                                               pv_error  => :p0_error);',
'  ',
'  END LOOP;',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P168_MCA_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>579844729254537076
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(612098647826302009)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_mov_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
' pq_ven_movimientos_caja.pr_imprimir_mov_caja(pn_emp_id => :f_emp_id,',
'                                                     pn_mca_id => :P168_MCA_ID,',
'                                                     pv_error  => :P0_ERROR);   ',
'                                                     ',
'                                                     ',
':P168_MCA_ID:=null;                                                     ',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_type=>'NEVER'
,p_internal_uid=>579845496556537083
,p_process_comment=>'P19_MCA_ID'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(591298412892768964)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_DATOS_CLIENTE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'SELECT A.PER_PRIMER_APELLIDO||'' ''||A.PER_SEGUNDO_APELLIDO||'' ''||',
'       A.PER_PRIMER_NOMBRE||'' ''||A.PER_SEGUNDO_NOMBRE||'' ''||A.PER_RAZON_SOCIAL,',
'       B.CLI_ID',
'INTO :P168_NOMBRE_CLIENTE,',
'     :P168_CLI_ID',
'FROM ASDM_E.ASDM_PERSONAS A,',
'     ASDM_E.ASDM_CLIENTES B',
'WHERE A.PER_ID = B.PER_ID',
'      AND A.PER_NRO_IDENTIFICACION = :P168_NRO_IDENTIFICACION',
'      AND A.PER_ESTADO_REGISTRO = ''0''',
'      AND B.CLI_ESTADO_REGISTRO = ''0''',
'      AND B.EMP_ID = :f_emp_id;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(591298490708768965)
,p_internal_uid=>559045261623004038
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(612098182680302006)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACTUALIZA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    ln_pve_id number;',
'',
'begin',
'',
'    SELECT pve_id ',
'    INTO ln_pve_id',
'    FROM inv_paybahn_ventas ',
'    WHERE com_id = :P168_COM_ID;',
'    --raise_application_error(-20000, ''Llego'' || ln_pve_id);',
'    --ln_pve_id := 0;',
'    if nvl(ln_pve_id, 0) != 0 then',
'    ',
'        -- Call the procedure',
'        pq_car_paybahn.pr_activacion_cliente(pn_pve_id => ln_pve_id,',
'                                             pn_emp_id => :F_EMP_ID);',
'    else',
'        apex_application.g_print_success_message := ''<span style="color:RED">No se pudo recuperar una venta.</span>'';',
'                                                ',
'    end if;',
' EXCEPTION',
'    WHEN OTHERS THEN ',
'        apex_application.g_print_success_message := ''<span style="color:RED">Error al enviar a Actualizar el Dispositivo: '' ||',
'                                                SQLERRM || ''</span>'';',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'activar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>579845031410537080
);
wwv_flow_imp.component_end;
end;
/
