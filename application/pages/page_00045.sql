prompt --application/pages/page_00045
begin
--   Manifest
--     PAGE: 00045
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>45
,p_name=>'Report ASDM_CHEQUES_RECIBIDOS'
,p_step_title=>'Report Asdm Cheques Recibidos'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(46232175974692743)
,p_name=>'RESPALDOS PENDIENTES DE EFECTIVIZAR QUE SE REALIZARON NOTAS DE CREDITO '
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.cxc_id,',
'       (select vco.com_id_ref',
'          from ven_comprobantes vco',
'         where vco.com_id = b.com_id) com_id_factura,',
'       b.com_id com_id_nc,',
'       c.div_nro_vencimiento nro_vencimiento,',
'       c.div_fecha_vencimiento fecha_vencimiento,',
'       c.div_saldo_cuota saldo_cuota,',
'       b.rdi_valor_respaldado valor_respaldado,',
'       d.cre_nro_cheque nro_cheque,',
'       d.cre_nro_cuenta nro_cuenta,',
'       d.cre_nro_pin nro_pin,',
'       d.cre_valor valor_cheque,',
'       d.cre_fecha_deposito Fecha_deposito',
'  from car_respaldos_cartera    a,',
'       car_respaldos_dividendos b,',
'       car_dividendos           c,',
'       asdm_cheques_recibidos   d',
' where b.rca_id = a.rca_id',
'   and b.emp_id = a.emp_id',
'   and c.div_id = b.div_id',
'   and c.emp_id = b.emp_id',
'   and d.rca_id = a.rca_id',
'   and d.cre_estado_registro = 0',
'   and b.rdi_estado_registro = 0',
'   and a.rca_estado_registro = 0',
'   and a.emp_id = :F_EMP_ID',
'   and a.cli_id = :p45_cli_id2',
'   and b.com_id is not null',
'   and b.rdi_estado is null',
' order by c.cxc_id, c.div_nro_vencimiento'))
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select null',
'  from car_respaldos_cartera    a,',
'       car_respaldos_dividendos b',
' where b.rca_id = a.rca_id',
'   and b.emp_id = a.emp_id',
'   and b.rdi_estado_registro = 0',
'   and a.rca_estado_registro = 0',
'   and a.emp_id = :f_emp_id',
'   and a.cli_id = :p45_cli_id2',
'   and b.com_id is not null',
'   and b.rdi_estado is null'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46232480182692903)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46232562009692913)
,p_query_column_id=>2
,p_column_alias=>'COM_ID_FACTURA'
,p_column_display_sequence=>2
,p_column_heading=>'COM_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46232682519692913)
,p_query_column_id=>3
,p_column_alias=>'COM_ID_NC'
,p_column_display_sequence=>3
,p_column_heading=>'COM_ID_NC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46232779760692913)
,p_query_column_id=>4
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46232858292692913)
,p_query_column_id=>5
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46232978933692913)
,p_query_column_id=>6
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>6
,p_column_heading=>'SALDO_CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46233065714692913)
,p_query_column_id=>7
,p_column_alias=>'VALOR_RESPALDADO'
,p_column_display_sequence=>7
,p_column_heading=>'VALOR_RESPALDADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46233165568692913)
,p_query_column_id=>8
,p_column_alias=>'NRO_CHEQUE'
,p_column_display_sequence=>8
,p_column_heading=>'NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46233258930692913)
,p_query_column_id=>9
,p_column_alias=>'NRO_CUENTA'
,p_column_display_sequence=>9
,p_column_heading=>'NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46233361041692913)
,p_query_column_id=>10
,p_column_alias=>'NRO_PIN'
,p_column_display_sequence=>10
,p_column_heading=>'NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46233470431692913)
,p_query_column_id=>11
,p_column_alias=>'VALOR_CHEQUE'
,p_column_display_sequence=>11
,p_column_heading=>'VALOR_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(46237053296723976)
,p_query_column_id=>12
,p_column_alias=>'FECHA_DEPOSITO'
,p_column_display_sequence=>12
,p_column_heading=>'Fecha Deposito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(62199172980818184)
,p_plug_name=>'<SPAN STYLE="font-size: 12pt"> Ingreso de Cheques Posfechados - Datos de Cliente </span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(62199366977818187)
,p_plug_name=>'<SPAN STYLE="font-size: 12pt"> Listado de Cheques Posfechados Periodo  </span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cre.cre_id,',
'       cre.emp_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(7, cre.ede_id) entidad_financiera,',
'       cre.cre_id_reemplazado,',
'       cre.rca_id rca_id,',
'       cre.cre_nro_cheque,',
'       cre.cre_nro_cuenta,',
'       cre.cre_nro_pin,',
'       cre.cre_valor,',
'       cre.cre_titular_cuenta,',
'       cre.cre_observaciones,',
'       pq_lv_kdda.fn_obtiene_valor_lov(183, cre.cre_tipo) tipo_cheque,',
'       cre.cre_fecha_deposito,',
'       cre.cre_saldo_cheque,',
'       pq_lv_kdda.fn_obtiene_valor_lov(78, cre.cre_estado_registro) estado_registro,',
'       cre.cli_id cli_id,',
'       cre.cre_beneficiario beneficiario,',
'       rca.rca_fecha_ingreso fecha_ingreso,',
'       ech.ech_descripcion estado_cheque,',
'       (select pue.pue_nombre',
'          from asdm_puntos_emision pue',
'         where pue.pue_id = rca.pue_id) CAJA',
'  FROM asdm_cheques_recibidos cre,',
'       car_respaldos_cartera  rca,',
'       asdm_estados_cheques   ech',
' WHERE cre_tipo =',
'       pq_constantes.fn_retorna_constante(0, ''cv_tch_posfechado'')',
'   AND cre.rca_id = rca.rca_id',
'   AND cre.cli_id = :p45_cli_id2',
'   AND cre.emp_id = :f_emp_id',
'   AND rca.emp_id = :f_emp_id',
'   AND cre.ech_id = ech.ech_id',
'   AND cre.cre_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_column_width=>'valign=top'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'',
'function pull_multi_value(pValue){',
'',
'    var ln_identificacion = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=dummy'',0);',
'    ln_identificacion.add(pValue,$x(pValue).value)',
'    gReturn = ln_identificacion.get();',
'    ',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ',
'    ''APPLICATION_PROCESS=PR_AJUSTE_RESP_DIVIDENDOS'',0);',
'    if(pValue)',
'    {',
'        get.add(''F244_CLI_ID'',pValue)',
'    }else{',
'        get.add(''F244_CLI_ID'',''null'')',
'    }    ',
'    gReturn = get.get(''XML'');',
'    if(gReturn)',
'    {',
'     var l_Count = gReturn.getElementsByTagName("item").length;',
'    for(var i = 0;i<l_Count;i++)',
'    {',
'    var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'    var l_ID = l_Opt_Xml.getAttribute(''id'');',
'    var l_El = html_GetElement(l_ID);    ',
'    if(l_Opt_Xml.firstChild)',
'    {',
'    var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'    }else{',
'    var l_Value = '''';',
'    }',
'',
'    if(l_El){',
'    if(l_El.tagName == ''INPUT''){',
'    l_El.value = l_Value;',
'    }else if(l_El.tagName == ''SPAN'' && ',
'    l_El.className == ''grabber'')',
'    {',
'    l_El.parentNode.innerHTML = l_Value;',
'    l_El.parentNode.id = l_ID;',
'    }else{',
'    l_El.innerHTML = l_Value;',
'    }',
'    }',
'    }',
'    }',
'    get = null;',
'',
'',
'}',
'',
'',
'</script>',
''))
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(62199575313818191)
,p_name=>'Listado de Cheques Posfechados'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y_OF_Z'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link=>'f?p=&APP_ID.:46:&SESSION.:consulta:&DEBUG.:46:P46_CRE_ID,P46_RCA_FECHA_INGRESO,P46_RCA_ID,P46_CLI_ID:#CRE_ID#,#FECHA_INGRESO#,#RCA_ID#,#CLI_ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#ed-item.gif"  border="0">'
,p_detail_link_attr=>'class="lock_ui_row"'
,p_owner=>'JARIAS'
,p_internal_uid=>29946424044053265
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62199675017818201)
,p_db_column_name=>'CRE_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'CRE_ID'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62199752638818202)
,p_db_column_name=>'EMP_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'EMP_ID'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62199882757818202)
,p_db_column_name=>'CRE_ID_REEMPLAZADO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'ID_REEMPLAZADO'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'CRE_ID_REEMPLAZADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62199952891818202)
,p_db_column_name=>'RCA_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Resp_id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'RCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200056272818202)
,p_db_column_name=>'CRE_NRO_CHEQUE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Nro Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200159968818202)
,p_db_column_name=>'CRE_NRO_CUENTA'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Nro Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200263882818203)
,p_db_column_name=>'CRE_NRO_PIN'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Nro Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200364366818203)
,p_db_column_name=>'CRE_VALOR'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'CRE_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200461736818203)
,p_db_column_name=>'CRE_TITULAR_CUENTA'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Titular Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_TITULAR_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200551663818205)
,p_db_column_name=>'CRE_FECHA_DEPOSITO'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>unistr('Fecha Dep\00BF\00BFsito')
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_static_id=>'CRE_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200673552818206)
,p_db_column_name=>'CLI_ID'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'CLI_ID'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200769243818206)
,p_db_column_name=>'TIPO_CHEQUE'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Tipo Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'TIPO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200878820818206)
,p_db_column_name=>'BENEFICIARIO'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Beneficiario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'BENEFICIARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62200977847818206)
,p_db_column_name=>'FECHA_INGRESO'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Fecha Ingreso'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_static_id=>'FECHA_INGRESO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62201069672818206)
,p_db_column_name=>'CRE_OBSERVACIONES'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62201162864818206)
,p_db_column_name=>'ESTADO_REGISTRO'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(62201271621818207)
,p_db_column_name=>'ENTIDAD_FINANCIERA'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Entidad Financiera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ENTIDAD_FINANCIERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(44060969618347638)
,p_db_column_name=>'ESTADO_CHEQUE'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Estado Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ESTADO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(75420561782616152)
,p_db_column_name=>'CRE_SALDO_CHEQUE'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Saldo Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CRE_SALDO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(7708126077395190152)
,p_db_column_name=>'CAJA'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Caja'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(62201472926818211)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1293214802919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'RCA_ID:ENTIDAD_FINANCIERA:CRE_NRO_CUENTA:CRE_TITULAR_CUENTA:CRE_NRO_CHEQUE:CRE_NRO_PIN:CRE_FECHA_DEPOSITO:CRE_VALOR:CRE_SALDO_CHEQUE:ESTADO_CHEQUE:CAJA'
,p_sort_column_1=>'RCA_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(7708151954323193002)
,p_report_id=>wwv_flow_imp.id(62201472926818211)
,p_condition_type=>'FILTER'
,p_allow_delete=>'Y'
,p_column_name=>'ESTADO_CHEQUE'
,p_operator=>'='
,p_expr=>'Ingresado'
,p_condition_sql=>'"ESTADO_CHEQUE" = #APXWS_EXPR#'
,p_condition_display=>'#APXWS_COL_NAME# = ''Ingresado''  '
,p_enabled=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(62201957665818216)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(62199366977818187)
,p_button_name=>'NUEVO'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:46:&SESSION.:nuevo:&DEBUG.:46:P46_CLI_ID:&P45_CLI_ID2.'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(62204264260818239)
,p_branch_action=>'f?p=&FLOW_ID.:4:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(62201957665818216)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(62204468650818243)
,p_branch_action=>'f?p=&APP_ID.:45:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>20
,p_branch_comment=>'Created 03-DIC-2009 15:29 by JARIAS'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62202173287818225)
,p_name=>'P45_CLIENTE_EXISTE'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_prompt=>'Cliente Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62202375815818229)
,p_name=>'P45_CLI_ID2'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_prompt=>'Cliente:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_RETORNA_ID1'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(500);',
'BEGIN',
'  lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LV_CLIENTES_CAR_VEN'');',
'  lv_lov := lv_lov ||'' where per.emp_id=''||:f_emp_id;',
'  RETURN(lv_lov);',
'END;'))
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62202580874818230)
,p_name=>'P45_NOMBRE_CLI'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nombre:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.nombre_completo',
'FROM v_asdm_clientes_personas a',
'WHERE a.cli_id=:p45_cli_id2'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62202775146818230)
,p_name=>'P45_IDENTIFICACION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Nro. Identificaci\00F3n:')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.per_nro_identificacion',
'FROM v_asdm_clientes_personas a',
'WHERE a.cli_id=:p45_cli_id2'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62203163571818231)
,p_name=>'P45_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Direcci\00F3n &P45_TIPO_DIRECCION.:')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT dir.dir_descripcion direccion',
'  FROM asdm_personas        per,',
'       asdm_clientes        cli,',
'       asdm_direcciones     dir,',
'       asdm_tipos_direccion tdi',
' WHERE cli.cli_id = :p45_cli_id2',
'   AND per.emp_id = :f_emp_id',
'   AND cli.per_id = per.per_id',
'   AND per.per_id = dir.per_id(+)',
'   AND dir.tdi_id = tdi.tdi_id(+)',
'   AND dir.dir_id(+) =',
'       pq_asdm_clientes.fn_dir_principal_mostrar(pn_per_id => per.per_id,',
'                                                 pn_tse_id => :f195_segmento,',
'                                                 pn_emp_id => :f_emp_id)',
'   AND dir.dir_estado_registro(+) =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62203380675818231)
,p_name=>'P45_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Tipo Tel\00BF\00BFfono')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tte.tte_descripcion tipo_telefono',
'  FROM asdm_personas        per,',
'       asdm_clientes        cli,',
'       asdm_telefonos       tel,',
'       asdm_tipos_telefonos tte',
' WHERE cli.cli_id = :p45_cli_id2',
'   AND per.emp_id = :f_emp_id',
'   AND cli.per_id = per.per_id',
'   AND per.per_id = tel.per_id(+)',
'   AND tel.tte_id = tte.tte_id(+)',
'   AND tel.tel_id(+) =',
'       pq_asdm_clientes.fn_tel_principal_mostrar(pn_per_id => per.per_id,',
'                                                 pn_tse_id => :f195_segmento)',
'   AND tel.tel_estado_registro(+) =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62203563831818231)
,p_name=>'P45_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Tel\00E9fono &P45_TIPO_TELEFONO.:')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tel.tel_numero telefono',
'  FROM asdm_personas        per,',
'       asdm_clientes        cli,',
'       asdm_telefonos       tel,',
'       asdm_tipos_telefonos tte',
' WHERE cli.cli_id = :p45_cli_id2',
'   AND per.emp_id = :f_emp_id',
'   AND cli.per_id = per.per_id',
'   AND per.per_id = tel.per_id(+)',
'   AND tel.tte_id = tte.tte_id(+)',
'   AND tel.tel_id(+) =',
'       pq_asdm_clientes.fn_tel_principal_mostrar(pn_per_id => per.per_id,',
'                                                 pn_tse_id => :f195_segmento)',
'   AND tel.tel_estado_registro(+) =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_estado_reg_activo'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes2=>'align = "right"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62203776599818231)
,p_name=>'P45_CLI_ID'
,p_item_sequence=>101
,p_item_plug_id=>wwv_flow_imp.id(62199366977818187)
,p_prompt=>'Cliente Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''pull_multi_value(this.value)'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62419963291970455)
,p_name=>'P45_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tipo Direccion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tipo_direccion',
'  FROM (SELECT tdi.tdi_descripcion tipo_direccion',
'          FROM asdm_personas        per,',
'               asdm_clientes        cli,',
'               asdm_direcciones     dir,',
'               asdm_tipos_direccion tdi',
'         WHERE cli.cli_id = :p45_cli_id2',
'           AND per.emp_id = :f_emp_id',
'           AND cli.per_id = per.per_id',
'           AND per.per_id = dir.per_id(+)',
'           AND dir.tdi_id = tdi.tdi_id(+)',
'           AND dir.dir_id(+) =',
'               pq_asdm_clientes.fn_dir_principal_mostrar(pn_per_id => per.per_id,',
'                                                         pn_tse_id => :f195_segmento,',
'                                                         pn_emp_id => :f_emp_id)',
'           AND dir.dir_estado_registro(+) =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_estado_reg_activo''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(8888524874366418059)
,p_name=>'P45_MENSAJE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(62199172980818184)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ',
'  ln_fol_facturacion number := 0;',
'  ln_fol_notacredito number := 0;',
'  lv_error           varchar2(2500) := null;',
'  lv_caja            asdm_puntos_emision.pue_nombre%TYPE;',
'  lv_tipo_punto      varchar2(1);',
'  ',
'begin',
'',
'  select pue.pue_nombre',
'    into lv_caja',
'    from asdm_puntos_emision pue',
'   where pue.pue_id = :p0_pue_id',
'     and pue.emp_id = :f_emp_id;',
'',
'  lv_tipo_punto := pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :p0_pue_id);',
'',
'',
'  if lv_tipo_punto = ''P'' then',
'',
'  select count(*)',
'   into ln_fol_facturacion',
'    from asdm_folios fol, asdm_tipos_grupo_transaccion b',
'   where fol.pue_id = :p0_pue_id',
'     and fol.fol_estado_registro = 0',
'     and fol.fol_estado = ''V''',
'     and b.tgr_id = fol.tgr_id',
'     and fol.emp_id = :f_emp_id',
'     and b.tgr_estado_registro = 0',
'     and b.tgr_id =',
'         pq_constantes.fn_retorna_constante(0, ''cn_tgr_id_facturacion'')',
'     and trunc(fol.fol_fecha_caducidad) >= trunc(sysdate);',
'     ',
'   else',
'     ',
'',
'  select count(*)',
'    into ln_fol_facturacion',
'    from asdm_folios fol, asdm_tipos_grupo_transaccion b',
'   where fol.pue_id = :p0_pue_id',
'     and fol.fol_estado_registro = 0',
'     and fol.fol_estado = ''V''',
'     and b.tgr_id = fol.tgr_id',
'     and fol.emp_id = :f_emp_id',
'     and b.tgr_estado_registro = 0',
'     and b.tgr_id =',
'         pq_constantes.fn_retorna_constante(0, ''cn_tgr_id_facturacion'');   ',
'   ',
'   end if;     ',
'     ',
'  if ln_fol_facturacion = 0 THEN',
'    ',
'    select count(*)',
'      into ln_fol_facturacion',
'      from asdm_folios fol, asdm_tipos_grupo_transaccion b',
'     where fol.pue_id = :p0_pue_id',
'       and fol.fol_estado_registro = 0',
'       AND fol.fol_estado = ''P''',
'       and b.tgr_id = fol.tgr_id',
'       and fol.emp_id = :f_emp_id',
'       and b.tgr_estado_registro = 0',
'       AND b.tgr_id =',
'           pq_constantes.fn_retorna_constante(0, ''cn_tgr_id_facturacion'')',
'       and trunc(fol.fol_fecha_caducidad) >= trunc(sysdate);',
'       ',
'    if ln_fol_facturacion = 0 THEN',
'      lv_error := '' FACTURACION; '';',
'    END IF;',
'    ',
'  end if;',
'',
'',
'  if lv_tipo_punto = ''P'' then',
'  select count(*)',
'    into ln_fol_notacredito',
'    from asdm_folios fol, asdm_tipos_grupo_transaccion b',
'   where fol.pue_id = :p0_pue_id',
'     and fol.fol_estado_registro = 0',
'     and fol.fol_estado = ''V''',
'     and b.tgr_id = fol.tgr_id',
'     and fol.emp_id = :f_emp_id',
'     and b.tgr_estado_registro = 0',
'     and b.tgr_id =',
'         pq_constantes.fn_retorna_constante(0, ''cn_tgr_id_nota_credito'')',
'     and trunc(fol.fol_fecha_caducidad) >= trunc(sysdate);',
'     ',
'     ',
'   else',
'     ',
'  select count(*)',
'    into ln_fol_notacredito',
'    from asdm_folios fol, asdm_tipos_grupo_transaccion b',
'   where fol.pue_id = :p0_pue_id',
'     and fol.fol_estado_registro = 0',
'     and fol.fol_estado = ''V''',
'     and b.tgr_id = fol.tgr_id',
'     and fol.emp_id = :f_emp_id',
'     and b.tgr_estado_registro = 0',
'     and b.tgr_id =',
'         pq_constantes.fn_retorna_constante(0, ''cn_tgr_id_nota_credito'');',
'   ',
'   end if;  ',
'     ',
'  if ln_fol_notacredito = 0 THEN',
'    select count(*)',
'      into ln_fol_notacredito',
'      from asdm_folios fol, asdm_tipos_grupo_transaccion b',
'     where fol.pue_id = :p0_pue_id',
'       and fol.fol_estado_registro = 0',
'       and fol.fol_estado = ''P''',
'       and b.tgr_id = fol.tgr_id',
'       and fol.emp_id = :f_emp_id',
'       and b.tgr_estado_registro = 0',
'       and b.tgr_id =',
'           pq_constantes.fn_retorna_constante(0, ''cn_tgr_id_nota_credito'')',
'       and trunc(fol.fol_fecha_caducidad) >= trunc(sysdate);',
'    if ln_fol_notacredito = 0 then',
'      lv_error := lv_error || '' NOTA DE CREDITO; '';',
'    END IF;',
'  end if;',
'',
'  if ln_fol_facturacion = 0 or ln_fol_notacredito = 0 then',
'    lv_error := ''EN LA CAJA INGRESADA: '' || lv_caja ||',
'                '' NO EXISTEN FOLIOS PARA LAS TRANSACCIONES:'' || LV_ERROR ||',
'                '' REVISE SI EN ESTA CAJA DEBE INGRESAR LOS CHEQUES'';',
'  END IF;',
'  return lv_error;',
'end;'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(62448662702102734)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'',
'BEGIN',
'  :p0_ttr_descripcion := NULL;',
'  :p0_datfolio        := NULL;',
'  :p0_fol_sec_actual  := NULL;',
'  :p0_ttr_id          := pq_constantes.fn_retorna_constante(NULL,',
'                                                            ''cn_ttr_id_ing_ch_posf'');',
'  :p0_periodo         := NULL;',
'  ln_tgr_id           := pq_constantes.fn_retorna_constante(0,',
'                                                            ''cn_tgr_id_mov_cheques'');',
'  pq_ven_movimientos_caja.pr_datos_folio_caja_usu(pn_emp_id          => :f_emp_id,',
'                                                  pn_pca_id          => :f_pca_id,',
'                                                  pn_tgr_id          => ln_tgr_id,',
'                                                  pv_ttr_descripcion => :p0_ttr_descripcion,',
'                                                  pv_periodo         => :p0_periodo,',
'                                                  pv_datfolio        => :p0_datfolio,',
'                                                  pn_fol_sec_actual  => :p0_fol_sec_actual,',
'                                                  pn_pue_num_sri     => :p0_pue_num_sri,',
'                                                  pn_uge_num_est_sri => :p0_pue_num_sri,',
'                                                  pn_pue_id          => :p0_pue_id,',
'                                                  pn_ttr_id          => :p0_ttr_id,',
'                                                  pn_nro_folio       => :p0_nro_folio,',
'                                                  pv_error           => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>30195511432337808
);
null;
wwv_flow_imp.component_end;
end;
/
