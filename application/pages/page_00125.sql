prompt --application/pages/page_00125
begin
--   Manifest
--     PAGE: 00125
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>125
,p_name=>'REVERSO PRECANCELACION'
,p_alias=>'REVERSO_PRECANCELACION'
,p_step_title=>'REVERSO PRECANCELACION'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(79242373175120285)
,p_plug_name=>'Datos Cliente'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(109121477769503075)
,p_name=>'creditos_precancelados'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.cxc_id,',
'       a.mca_id mov_caja,',
'       a.cpr_fecha,',
'       a.cpr_cap_cancelado,',
'       CASE',
'         WHEN trunc(a.cpr_fecha) != trunc(SYSDATE) THEN',
'          ''DEVOLUCION''',
'         ELSE',
'          ''REVERSO''',
'       END reversar,',
'       CASE',
'         WHEN trunc(a.cpr_fecha) != trunc(SYSDATE) THEN',
'          pq_constantes.fn_retorna_constante(a.emp_id,',
'                                             ''cn_tfp_id_devolucion_pago_cuota'')',
'         ELSE',
'          pq_constantes.fn_retorna_constante(a.emp_id,',
'                                             ''cn_tfp_id_reverso_pago_cuota'')',
'       END tipo_pago',
'  FROM car_cxc_precancelacion a',
' WHERE a.cpr_estado_registro = 0',
'AND a.cli_id = :p125_cli_Id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109121775394503142)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109123068614516360)
,p_query_column_id=>2
,p_column_alias=>'MOV_CAJA'
,p_column_display_sequence=>2
,p_column_heading=>'MOV CAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109121857610503150)
,p_query_column_id=>3
,p_column_alias=>'CPR_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109121953836503150)
,p_query_column_id=>4
,p_column_alias=>'CPR_CAP_CANCELADO'
,p_column_display_sequence=>4
,p_column_heading=>'MONTO CANCELADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109122076289503150)
,p_query_column_id=>5
,p_column_alias=>'REVERSAR'
,p_column_display_sequence=>5
,p_column_heading=>'REVERSAR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:125:&SESSION.::&DEBUG.::P125_MCA_ID_MOVCAJA,P125_TFP_ID:#MOV_CAJA#,#TIPO_PAGO#'
,p_column_linktext=>'#REVERSAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109122182366503150)
,p_query_column_id=>6
,p_column_alias=>'TIPO_PAGO'
,p_column_display_sequence=>6
,p_column_heading=>'TIPO PAGO'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(109127554492590914)
,p_name=>'CREDITO PRECANCELADO'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C.CXC_ID, c.div_nro_vencimiento,',
'       c.div_fecha_vencimiento,',
'       c.div_valor_cuota,',
'       SUM(b.mde_valor)',
'  FROM car_movimientos_cab a, car_movimientos_det b, car_dividendos c',
' WHERE b.mca_id = a.mca_id',
'   AND mca_id_movcaja = :P125_MCA_ID_MOVCAJA',
'   AND c.div_id = b.div_id',
' GROUP BY c.cxc_id, c.div_nro_vencimiento, c.div_fecha_vencimiento, c.div_valor_cuota',
' ORDER BY c.div_nro_vencimiento'))
,p_display_when_condition=>'p125_cli_id'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>30
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109129153240599494)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109127864059590925)
,p_query_column_id=>2
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'NRO VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109127971609590925)
,p_query_column_id=>3
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109128061709590925)
,p_query_column_id=>4
,p_column_alias=>'DIV_VALOR_CUOTA'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR CUOTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(109128178588590925)
,p_query_column_id=>5
,p_column_alias=>'SUM(B.MDE_VALOR)'
,p_column_display_sequence=>5
,p_column_heading=>'VALOR APLICADO'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(109130953977628612)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(109127554492590914)
,p_button_name=>'GENERAR_REVERSO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270536469680046676)
,p_button_image_alt=>'GENERAR REVERSO'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_condition=>'P125_MCA_ID_MOVCAJA'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(109224854816026703)
,p_branch_name=>'pr_graba_reverso'
,p_branch_action=>'f?p=&APP_ID.:125:&SESSION.:GENERAR_REVERSO:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(109130953977628612)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79242570750120307)
,p_name=>'P125_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Nro Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onChange="doSubmit(''cliente'')"'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79242778542120326)
,p_name=>'P125_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79242961152120327)
,p_name=>'P125_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79243172414120327)
,p_name=>'P125_TIPO_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79243368114120328)
,p_name=>'P125_DIRECCION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79243560884120328)
,p_name=>'P125_TIPO_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79243775108120328)
,p_name=>'P125_TELEFONO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79243961706120328)
,p_name=>'P125_CORREO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79244181217120329)
,p_name=>'P125_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(79242373175120285)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onChange="doSubmit(''limpia'')"'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109128664454594244)
,p_name=>'P125_MCA_ID_MOVCAJA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(109127554492590914)
,p_prompt=>'MOV CAJA'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(109128859278596700)
,p_name=>'P125_TFP_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(109127554492590914)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(109131870946633590)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_reverso_precancelacion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_car_precancelacion.pr_reverso_precancelacion(pn_emp_id         => :f_emp_id,',
'                                                  pn_mca_id_movcaja => :p125_mca_id_movcaja,',
'                                                  pn_tfp_id         => :p125_tfp_id,',
'                                                  pn_uge_id         => :f_uge_id,',
'                                                  pn_usu_id         => :f_user_id,',
'                                                  pn_age_id_agencia => :f_age_id_agencia,',
'                                                  pn_cli_id         => :p125_cli_id,',
'                                                  pn_tse_id         => :f_seg_id,',
'                                                  pv_error          => :p0_error);',
'',
'  :p125_tipo_identificacion := NULL;',
'  :p125_identificacion      := NULL;',
'  :p125_cli_id              := NULL;',
'  :p125_nombre              := NULL;',
'  :p125_tipo_direccion      := NULL;',
'  :p125_direccion           := NULL;',
'  :p125_tipo_telefono       := NULL;',
'  :p125_telefono            := NULL;',
'  :p125_correo              := NULL;',
'  :p125_mca_id_movcaja      := NULL;',
'  :p125_tfp_id              := NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(109130953977628612)
,p_process_when=>'GENERAR_REVERSO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Reverso generado correctamente'
,p_internal_uid=>76878719676868664
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(79244965341127452)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_cliente_existe      VARCHAR2(100);',
'  ln_dir_id              asdm_direcciones.dir_id%TYPE;',
'  lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'  ln_cli_id_referido     asdm_clientes.cli_id%TYPE;',
'  lv_col_mov_caja        VARCHAR2(60) := pq_constantes.fn_retorna_constante(0,',
'                                                                            ''cv_coleccion_mov_caja'');',
'BEGIN',
'',
'  pq_ven_listas2.pr_datos_clientes(188, --le envio la agencia matriz ya que el rol no tiene agencia y da error y no necesitan la agencia',
'                                  :f_emp_id,',
'                                  :P125_IDENTIFICACION,',
'                                  :P125_NOMBRE,',
'                                  :P125_CLI_ID,',
'                                  :P125_CORREO,',
'                                  :P125_DIRECCION,',
'                                  :P125_TIPO_DIRECCION,',
'                                  :P125_TELEFONO,',
'                                  :P125_TIPO_TELEFONO,',
'                                  lv_cliente_existe,',
'                                  ln_dir_id,',
'                                  ln_cli_id_referido,',
'                                  lv_nombre_cli_referido,',
'                                  :P125_TIPO_IDENTIFICACION,',
'                                  :P0_ERROR);',
' ',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>46991814071362526
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(79245169843128702)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_col_mov_caja varchar2(60):= pq_constantes.fn_retorna_constante(0,''cv_coleccion_mov_caja'');',
'begin',
':P124_IDENTIFICACION := NULL;',
'	:P124_CLI_ID	:= NULL;',
'	:P124_NOMBRE	:= NULL;',
'	:P124_TIPO_DIRECCION	:= NULL;',
'	:P124_DIRECCION	:= NULL;',
'	:P124_TIPO_TELEFONO	:= NULL;',
'	:P124_TELEFONO	:= NULL;',
'	:P124_CORREO := NULL;',
':P124_CXC_ID := NULL;',
' IF apex_collection.collection_exists(''COL_CXC_PRECANCELA'') THEN',
'      apex_collection.delete_collection(''COL_CXC_PRECANCELA'');',
' END IF;',
'',
' IF apex_collection.collection_exists(lv_col_mov_caja) THEN',
'      apex_collection.delete_collection(lv_col_mov_caja);',
' END IF;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'limpia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>46992018573363776
);
wwv_flow_imp.component_end;
end;
/
