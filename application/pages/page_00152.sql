prompt --application/pages/page_00152
begin
--   Manifest
--     PAGE: 00152
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>152
,p_name=>'Factura Punto Venta'
,p_page_mode=>'MODAL'
,p_step_title=>'Factura Punto Venta'
,p_reload_on_submit=>'A'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_imp.id(37370553897200142)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_height=>'500'
,p_dialog_width=>'800'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(162679730308088958041)
,p_name=>'col_mov_caja'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select C001, C002, C003, C004, C005, C006, C007 from apex_collections where collection_name = ''COL_MOV_CAJA'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871193955505693526)
,p_query_column_id=>1
,p_column_alias=>'C001'
,p_column_display_sequence=>1
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871194423852693526)
,p_query_column_id=>2
,p_column_alias=>'C002'
,p_column_display_sequence=>2
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871194803180693527)
,p_query_column_id=>3
,p_column_alias=>'C003'
,p_column_display_sequence=>3
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871195169813693531)
,p_query_column_id=>4
,p_column_alias=>'C004'
,p_column_display_sequence=>4
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871195617041693531)
,p_query_column_id=>5
,p_column_alias=>'C005'
,p_column_display_sequence=>5
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871195971087693531)
,p_query_column_id=>6
,p_column_alias=>'C006'
,p_column_display_sequence=>6
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871196425226693531)
,p_query_column_id=>7
,p_column_alias=>'C007'
,p_column_display_sequence=>7
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(166262533502039542715)
,p_plug_name=>'<SPAN STYLE="font-size: 10pt">PAGO DE CLIENTE - &P152_CLI_ID.  - &P152_NOMBRES.</span>'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156361372453576934662)
,p_plug_name=>'tfp_Cheques'
,p_parent_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156361384488104964236)
,p_plug_name=>'IngresoValor'
,p_parent_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156361404290324101595)
,p_plug_name=>'tfp_deposito_transferencia'
,p_parent_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156388120302925184841)
,p_plug_name=>'tfp Datos Tarjeta Credito'
,p_parent_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P152_TFP_ID= pq_constantes.fn_retorna_constante(:f_emp_id ,''cn_tfp_id_tarjeta_credito'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(156409995828600905422)
,p_plug_name=>'tfp_retencion'
,p_parent_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_retencion'')'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(157806056316291603466)
,p_name=>'Retenciones'
,p_parent_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'seq_id id,',
'to_number(c001) com_id,',
'c002 fecha_ret,',
'c003 fecha_validez,',
'c004 nro_retencion,',
'c005 nro_autorizacion,',
'c006 nro_establecimiento,',
'c007 nro_pto_emision,',
'c008 porcentaje,',
'to_number(c009) base,',
'to_number(c013) valor_calculado,',
'apex_item.text(p_idx        => 25,',
'                      p_value      => to_number(c010),',
'                      p_attributes => ''unreadonly id="f47_'' || TRIM(rownum) || ''"'' ||',
'                                      ''" onchange="actualiza('' || TRIM(rownum)||'',''||c013||'',''||seq_id||'',this.value)"'',',
'                      p_size       => ''10'') valor_retencion,',
'''X'' eliminar,',
'c026 tipo',
'',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION''',
'UNION',
'select ',
'NULL id,',
'NULL com_id,',
'NULL fecha_ret,',
'NULL fecha_validez,',
'NULL nro_retencion,',
'NULL nro_autorizacion,',
'NULL nro_establecimiento,',
'NULL nro_pto_emision,',
'NULL porcentaje,',
'SUM(to_number(c009)) base,',
'sum(to_number(c013)) valor_calculado,',
'TO_CHAR(SUM(to_number(c010))) valor_retencion,',
'NULL eliminar,',
'null tipo',
'from apex_collections where collection_name = ''COLL_VALOR_RETENCION'''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871216639066693568)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>13
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871216960007693570)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ord Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871217434213693571)
,p_query_column_id=>3
,p_column_alias=>'FECHA_RET'
,p_column_display_sequence=>2
,p_column_heading=>'Fecha Ret'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871217803228693571)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VALIDEZ'
,p_column_display_sequence=>3
,p_column_heading=>'Fecha Validez'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871218209440693571)
,p_query_column_id=>5
,p_column_alias=>'NRO_RETENCION'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Retencion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871218561871693572)
,p_query_column_id=>6
,p_column_alias=>'NRO_AUTORIZACION'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Autorizacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871218973192693572)
,p_query_column_id=>7
,p_column_alias=>'NRO_ESTABLECIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Nro Establecimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871219359225693572)
,p_query_column_id=>8
,p_column_alias=>'NRO_PTO_EMISION'
,p_column_display_sequence=>7
,p_column_heading=>'Nro Pto Emision'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871219821957693572)
,p_query_column_id=>9
,p_column_alias=>'PORCENTAJE'
,p_column_display_sequence=>8
,p_column_heading=>'Porcentaje'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871220153688693573)
,p_query_column_id=>10
,p_column_alias=>'BASE'
,p_column_display_sequence=>10
,p_column_heading=>'Base'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871220628062693573)
,p_query_column_id=>11
,p_column_alias=>'VALOR_CALCULADO'
,p_column_display_sequence=>11
,p_column_heading=>'Valor Calculado'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871220966780693573)
,p_query_column_id=>12
,p_column_alias=>'VALOR_RETENCION'
,p_column_display_sequence=>12
,p_column_heading=>'Valor Retencion'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871221424106693574)
,p_query_column_id=>13
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>14
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:ELIMINA_RET:&DEBUG.:RP:P30_SEQ_ID_RET:#ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871221777679693574)
,p_query_column_id=>14
,p_column_alias=>'TIPO'
,p_column_display_sequence=>9
,p_column_heading=>'Tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(166262534281227542723)
,p_name=>'DETALLE PAGO'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tfp_descripcion,',
'       to_number(c006) valor,',
'       c040 detalle_pago,',
'       co.seq_id',
'  FROM apex_collections co, asdm_tipos_formas_pago p',
' WHERE collection_name = ''CO_MOV_CAJA''',
' and p.tfp_id = to_number(co.c005)',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871190617522693479)
,p_query_column_id=>1
,p_column_alias=>'TFP_DESCRIPCION'
,p_column_display_sequence=>1
,p_column_heading=>'Forma de Pago'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:20;">#TFP_DESCRIPCION#</span>'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871191024175693485)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_column_html_expression=>'<span style="font-weight:bold;font-size:20;">#VALOR#</span>'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871191369539693485)
,p_query_column_id=>3
,p_column_alias=>'DETALLE_PAGO'
,p_column_display_sequence=>3
,p_column_heading=>'Detalles Adicionales'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117871191838756693486)
,p_query_column_id=>4
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:152:&SESSION.:ELIMINA_MOVCAJA:&DEBUG.:RP:P152_SEQ_ID_MOVCAJA:#SEQ_ID#'
,p_column_linktext=>'<img src="#WORKSPACE_IMAGES#eliminar.gif" class="elimina">'
,p_column_link_attr=>'class="lock_ui_row" '
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871192224893693487)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(166262534281227542723)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'GRABAR'
,p_button_position=>'BELOW_BOX'
,p_button_condition_type=>'NEVER'
,p_button_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select sum(to_number(c006))',
'from apex_collections',
'where collection_name = ''CO_MOV_CAJA'') = :P152_VALOR_TOTAL_PAGOS'))
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871197037312693536)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(166262534281227542723)
,p_button_name=>'REGRESAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BELOW_BOX'
,p_button_execute_validations=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117875879288545351347)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(166262534281227542723)
,p_button_name=>'BT_CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BELOW_BOX'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(select round(sum(to_number(c006)),2)',
'from apex_collections',
'where collection_name = ''CO_MOV_CAJA'') = :P152_VALOR_TOTAL_PAGOS'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871231667494693583)
,p_button_sequence=>500
,p_button_plug_id=>wwv_flow_imp.id(156361384488104964236)
,p_button_name=>'CARGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:152:&SESSION.:cargar_tfp:&DEBUG.:::'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(117871247892074693658)
,p_branch_name=>'br_grabar'
,p_branch_action=>'f?p=&APP_ID.:152:&SESSION.:GRABAR:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(117871192224893693487)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115122578320336687827)
,p_name=>'P152_SALDO_PROMO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Saldo Total Promocion'
,p_source=>'to_number(pq_asdm_promociones.fn_obtener_saldo_actual(:P152_CLI_ID,:F_EMP_ID,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),NULL,:P0_Error))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115122578432871687828)
,p_name=>'P152_SALDO_MAX_APLICAR'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Saldo Maximo a Aplicar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_promociones.fn_saldo_maximo_aplica_masiva(pn_emp_id => :f_emp_id,',
'                                                               pn_cli_id => :P152_cli_id,',
'                                                               pn_pro_id => pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),',
'                                                               pn_total_pago => :P152_SALDO_PAGO,',
'                                                               pv_error => :P0_error)'))
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115122578540045687829)
,p_name=>'P152_SALDO_PROMO_DEV'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Saldo Total Promocion Devuelta'
,p_source=>'to_number(pq_asdm_promociones.fn_obtener_saldo_act_dev(:P152_CLI_ID,:F_EMP_ID,pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),NULL,:P0_Error))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_dev_promo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115122578619941687830)
,p_name=>'P152_SALDO_MAX_APL_DEV'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Saldo Maximo a Aplicar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_promociones.fn_saldo_max_apl_dev_pventa(pn_emp_id => :f_emp_id,',
'                                                               pn_cli_id => :P152_cli_id,',
'                                                               pn_pro_id => pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_pro_id_promocion_pr''),',
'                                                               pn_total_pago => :P152_SALDO_PAGO,',
'                                                               pv_error => :P0_error)'))
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_dev_promo'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868286315358362237)
,p_name=>'P152_PVC_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868286732278362241)
,p_name=>'P152_PCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871192616819693500)
,p_name=>'P152_SEQ_ID_MOVCAJA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(166262534281227542723)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871197428275693539)
,p_name=>'P152_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871197842700693540)
,p_name=>'P152_CXC_ID_REF'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Cxc id ref'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871198181754693540)
,p_name=>'P152_NOMBRES'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871198629840693540)
,p_name=>'P152_SALDO_ANTICIPOS'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Anticipos'
,p_source=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P152_CLI_ID,:f_uge_id))'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>'to_number(pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:P30_CLI_ID,:f_uge_id))'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871199006328693541)
,p_name=>'P152_VALOR_TOTAL_PAGOS'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'TOTAL PAGAR:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:24"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871199388826693541)
,p_name=>'P152_SALDO_PAGO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Pago'
,p_pre_element_text=>'<b>'
,p_source=>'select NVL(nvl(:p152_valor_total_pagos,0) - to_number(sum(nvl(c006,0))) , :p152_valor_total_pagos) VALOR  from apex_collections where collection_name = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:24"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871199765176693545)
,p_name=>'P152_TFP_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'IF NVL(:P152_SALDO_ANTICIPOS,0) = 0 THEN',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
'ELSE',
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes''); ',
'END IF;',
'END;'))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_FORMA_PAGO_PV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov :=PQ_CAR_PUNTO_VENTA.fn_lov_formas_pago(pn_cli_id => :p152_cli_id,',
'                                                   pn_emp_id => :F_emp_id); ',
' RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P152_VALOR_TOTAL_PAGOS > 0'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871200170340693546)
,p_name=>'P152_EDE_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_item_default=>'NULL'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Entidad Destino:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
' return pq_ven_listas_caja.fn_lov_entidad_destin_tfp_pag',
'(:F_EMP_ID,',
':P152_TFP_ID,',
':P0_ERROR)'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') or ',
':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or ',
':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia'')'))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'POPUP'
,p_attribute_08=>'CIC'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871200587099693548)
,p_name=>'P152_CRE_TITULAR_CUENTA'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Titular Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>100
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onblur="this.value = this.value.toUpperCase();"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871201325412693551)
,p_name=>'P152_MCD_NRO_COMPROBANTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(156361404290324101595)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Comprobante:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>13
,p_cMaxlength=>12
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871201730266693553)
,p_name=>'P152_MCD_FECHA_DEPOSITO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(156361404290324101595)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Deposito:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871202382162693554)
,p_name=>'P152_TJ_ENTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Entidad'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_TARJETA_CREDITO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    lv_lov varchar2(8000);',
'begin',
'    ',
'    lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_PLAN_TARJETA_CORRIENTE'');',
'',
'',
'    RETURN lv_lov;',
'end;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871202770631693554)
,p_name=>'P152_TITULAR_TC'
,p_item_sequence=>15
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Titular Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp=''convertir_mayu(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871203226947693558)
,p_name=>'P152_BANCOS'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Banco:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'SELECT a.ede_descripcion,a.ede_id FROM asdm_entidades_destinos a WHERE a.ede_tipo=''EFI'' and a.emp_id = :f_emp_id'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871203553406693558)
,p_name=>'P152_TARJETA'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Tarjeta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id FROM asdm_entidades_destinos b WHERE b.ede_tipo=''TJ''',
'AND b.ede_id_padre=:p152_bancos',
'and b.emp_id = :f_emp_id'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P152_BANCOS'
,p_ajax_items_to_submit=>'P152_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871204012635693558)
,p_name=>'P152_TIPO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Tipo:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion,c.ede_id FROM asdm_entidades_destinos c WHERE c.ede_tipo=''PTJ''',
'AND c.ede_id_padre=:p152_tarjeta',
'and c.emp_id = :f_emp_id'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P152_TARJETA'
,p_ajax_items_to_submit=>'P152_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871204424076693559)
,p_name=>'P152_PLAN'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Plan:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_descripcion, ''->'') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=pq_constantes.fn_retorna_constante(NULL,''cv_tede_detalle_plan_tarjeta'')',
' and ede.emp_id = :f_emp_id',
'START WITH ede.ede_id_padre =:P152_TIPO',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_lov_cascade_parent_items=>'P152_TIPO'
,p_ajax_items_to_submit=>'P152_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871204805628693559)
,p_name=>'P152_TJ_TITULAR_Y'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Titular:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871205172255693559)
,p_name=>'P152_TJ_NUMERO'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Nro Tarjeta: '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871205589756693559)
,p_name=>'P152_TJ_NUMERO_VOUCHER'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>'Nro Voucher:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871205966961693560)
,p_name=>'P152_TJ_RECAP'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871206406994693560)
,p_name=>'P152_LOTE'
,p_item_sequence=>322
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871206826118693560)
,p_name=>'P152_MCD_NRO_AUT_REF'
,p_item_sequence=>332
,p_item_plug_id=>wwv_flow_imp.id(156388120302925184841)
,p_prompt=>unistr('Autorizaci\00C3\00B3n:')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''valida_numero(this)'';'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871207491844693561)
,p_name=>'P152_RET_IVA'
,p_item_sequence=>255
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'Retiene Iva'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Si;S,No;N'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871207891231693561)
,p_name=>'P152_RAP_NRO_ESTABL_RET'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'Nro Retencion:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_PEMISION_RET'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871208277411693561)
,p_name=>'P152_RAP_NRO_PEMISION_RET'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>1
,p_cMaxlength=>3
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_RETENCION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871208665889693562)
,p_name=>'P152_RAP_NRO_RETENCION'
,p_item_sequence=>262
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>' - '
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>9
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_NRO_AUTORIZACION'', this)"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871209070301693563)
,p_name=>'P152_RAP_NRO_AUTORIZACION'
,p_item_sequence=>272
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'Nro Autorizacion:'
,p_post_element_text=>' 10 - 37 - 49 digitos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>49
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onkeyup="return valida_numEnterTab(event,''P30_RAP_FECHA'', this)"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871209462392693563)
,p_name=>'P152_RAP_FECHA'
,p_item_sequence=>282
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Retencion:'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871209942062693564)
,p_name=>'P152_RAP_FECHA_VALIDEZ'
,p_item_sequence=>292
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Validez:'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>10
,p_cMaxlength=>10
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871210276961693564)
,p_name=>'P152_RAP_COM_ID'
,p_item_sequence=>302
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'Factura:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT vco.com_tipo || ''/'' || vco.com_id || '' -'' || vco.com_fecha d,',
'       vco.com_id v',
'  FROM ven_comprobantes vco',
' WHERE com_tipo = ''F''   ',
'   AND EXISTS',
' (SELECT NULL FROM car_cuotas_pagar p ',
'   WHERE p.com_id = vco.com_id',
'    and p.cli_id = :p152_cli_id',
'    and p.usu_id = :f_user_id)',
'   AND NOT EXISTS (SELECT NULL',
'          FROM asdm_retenciones_aplicadas r',
'         WHERE r.com_id = vco.com_id',
'           AND r.rap_estado_registro = 0)',
'   AND NOT EXISTS (SELECT NULL',
'          FROM apex_collections co',
'         WHERE co.collection_name = ''CO_MOV_CAJA''',
'           AND co.c019 = vco.com_id)',
''))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Facturas Disponibles --'
,p_cSize=>20
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871210695449693564)
,p_name=>'P152_SALDO_RETENCION'
,p_item_sequence=>312
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'<SPAN STYLE="font-size: 12pt;color:RED;"> Saldo Retencion: </span>'
,p_post_element_text=>'  para Anticipo de Clientes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871211142672693565)
,p_name=>'P152_PRE_ID'
,p_item_sequence=>372
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871211454611693565)
,p_name=>'P152_TRE_ID'
,p_item_sequence=>1961
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'%'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov_language=>'PLSQL'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  if :P152_RET_IVA = ''S'' then',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion , RPO_id',
'FROM CAR_RET_PORCENTAJES',
'where rpo_emp_id = ''||:f_emp_id;',
'  else',
'    lv_lov := ''SELECT RPO_PORCENTAJE || '''' - ''''  || rpo_descripcion, RPO_id',
'FROM CAR_RET_PORCENTAJES',
'WHERE RPO_DESCRIPCION = ''''RET FUENTE''''',
'and rpo_emp_id = ''|| :f_emp_id;',
'  end if;',
'  RETURN(lv_lov);',
'END;'))
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871211901876693565)
,p_name=>'P152_SEQ_ID_RET'
,p_item_sequence=>1971
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871212306803693565)
,p_name=>'P152_SUBTOTAL_IVA'
,p_item_sequence=>1981
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P152_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_con_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871212719494693566)
,p_name=>'P152_SUBTOTAL_SIN_IVA'
,p_item_sequence=>1991
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Subtotal Sin Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P152_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_subtotal_sin_iva'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871213065715693566)
,p_name=>'P152_SUBTOTAL'
,p_item_sequence=>1992
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_item_default=>'(nvl(:P152_SUBTOTAL_IVA,0) + nvl(:P152_SUBTOTAL_SIN_IVA,0)) - nvl(:P152_FLETE, 0)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Subtotal'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871213538843693566)
,p_name=>'P152_FLETE'
,p_item_sequence=>2011
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Flete'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P152_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_servicio_manejo_producto'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871213900580693566)
,p_name=>'P152_IVA'
,p_item_sequence=>2021
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Iva'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P152_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_iva_calculo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871214339054693567)
,p_name=>'P152_IVA_BIENES'
,p_item_sequence=>2031
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P152_IVA - (:P152_FLETE * 0.12)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Bienes'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871214743669693567)
,p_name=>'P152_IVA_FLETE'
,p_item_sequence=>2041
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P152_FLETE * 0.14'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Iva Flete'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871215143979693567)
,p_name=>'P152_INT_DIFERIDO'
,p_item_sequence=>2051
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Int Diferido'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.vor_valor_variable',
'        from ven_var_ordenes a',
'       where a.ord_id = :P152_ORD_ID',
'         and a.emp_id = :f_emp_id',
'         and a.var_id =',
'             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                ''cn_var_id_int_diferido'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871215509922693567)
,p_name=>'P152_BASE'
,p_item_sequence=>2061
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'Base'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGA_RET'');" '
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871215865281693567)
,p_name=>'P152_PRECIONEENTER11'
,p_item_sequence=>2071
,p_item_plug_id=>wwv_flow_imp.id(156409995828600905422)
,p_prompt=>'<SPAN STYLE="font-size: 10pt;color:RED;">Presione Enter para cargar el valor</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871222205017693574)
,p_name=>'P152_OBSERVACIONES_RET'
,p_item_sequence=>2081
,p_item_plug_id=>wwv_flow_imp.id(157806056316291603466)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:15px;color:RED;"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871229443869693581)
,p_name=>'P152_CRE_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(156361372453576934662)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871229795091693581)
,p_name=>'P152_CRE_FECHA_DEPOSITO'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(156361372453576934662)
,p_prompt=>'Fecha Cheque:'
,p_source=>'to_char(sysdate, ''DD/MM/YYYY'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871230227554693582)
,p_name=>'P152_CRE_NRO_CHEQUE'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(156361372453576934662)
,p_prompt=>'Nro Cheque:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871230621660693582)
,p_name=>'P152_CRE_NRO_CUENTA'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(156361372453576934662)
,p_prompt=>'Nro Cuenta:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871231049975693582)
,p_name=>'P152_CRE_NRO_PIN'
,p_item_sequence=>14
,p_item_plug_id=>wwv_flow_imp.id(156361372453576934662)
,p_prompt=>'Nro Pin:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>25
,p_cMaxlength=>20
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871232070644693583)
,p_name=>'P152_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(156361384488104964236)
,p_prompt=>'Valor Recibido:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''CARGAR_TFP'');" '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_help_text=>'Presione Enter para cargar el valor'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871233019221693586)
,p_name=>'P152_PRECIONEENTER'
,p_item_sequence=>17
,p_item_plug_id=>wwv_flow_imp.id(156361384488104964236)
,p_prompt=>'Presione Enter para cargar el valor'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871233385278693587)
,p_name=>'P152_VUELTO'
,p_item_sequence=>18
,p_item_plug_id=>wwv_flow_imp.id(156361384488104964236)
,p_prompt=>'Vuelto/Cambio Efectivo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:20"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871233796646693587)
,p_name=>'P152_SALDO_CONDONACION'
,p_item_sequence=>402
,p_item_plug_id=>wwv_flow_imp.id(156361384488104964236)
,p_prompt=>'Saldo Condonacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select sum(cc.ccc_saldo)',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p152_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p152_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871234169838693587)
,p_name=>'P152_TOTAL_PAGO_CONDONACION'
,p_item_sequence=>412
,p_item_plug_id=>wwv_flow_imp.id(156361384488104964236)
,p_prompt=>'Total Pago Condonacion'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(c016), 0)',
'',
'  fROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'   AND EXISTS',
' (select CC.CCC_ID',
'          from asdm_e.car_condonacion_cab cc, CAR_CONDONACION_DETALLE DE',
'         where cc.cli_id = :p152_cli_id',
'           and cc.ccc_estado_registro = 0',
'           and cc.ccc_saldo > 0',
'           AND CC.CCC_ID = DE.CCC_ID',
'           AND DE.CXC_ID = to_number(c007));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'  from asdm_e.car_condonacion_cab cc',
' where cc.cli_id = :p152_cli_id',
'   and cc.ccc_estado_registro  = 0',
'   and cc.ccc_saldo > 0;'))
,p_display_when_type=>'EXISTS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117907143036887534435)
,p_name=>'P152_CUPO_EMPLEADO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_use_cache_before_default=>'NO'
,p_item_default=>'return PQ_CAR_PUNTO_VENTA.fn_cupo_disponible_empleado(pn_cli_id => :p152_cli_id);'
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Cupo Empleado/Cliente'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="border-style:none;font-size:14px;color:RED;text-align:left;"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p152_tfp_id in (    pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_credito_punto_venta'')  , pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_desc_rol_punto_venta'')  )'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117907143074894534436)
,p_name=>'P152_OBSERVACION'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(166262533502039542715)
,p_prompt=>'Observacion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="border-style:none;font-size:14px;color:RED;text-align:left;"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871235465697693633)
,p_validation_name=>'EDE_ID'
,p_validation_sequence=>10
,p_validation=>'P152_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione Una Entidad'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'(:REQUEST = ''CARGAR_TFP'') ',
'AND ((:P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')) or (:P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito''))',
'    or (:P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))',
'    )'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871235774924693633)
,p_validation_name=>'CRE_TITULAR_CUENTA'
,p_validation_sequence=>20
,p_validation=>'P152_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la cuenta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871236177224693633)
,p_validation_name=>'CRE_NRO_CHEQUE'
,p_validation_sequence=>30
,p_validation=>'P152_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cheque'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871236638640693633)
,p_validation_name=>'CRE_NRO_CUENTA'
,p_validation_sequence=>40
,p_validation=>'P152_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cuenta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871236957177693634)
,p_validation_name=>'CRE_NRO_PIN'
,p_validation_sequence=>50
,p_validation=>'P152_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el PIN'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_cheque'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871237422083693634)
,p_validation_name=>'MCD_NRO_COMPROBANTE'
,p_validation_sequence=>60
,p_validation=>'P152_MCD_NRO_COMPROBANTE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>unistr('Ingrese el n\00FAmero de comprobante de dep\00F3sito o transferencia')
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''CARGAR_TFP''',
'AND (:P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871237845124693635)
,p_validation_name=>'PLAN'
,p_validation_sequence=>70
,p_validation=>'P152_PLAN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe Seleccionar el Plan'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871238246720693635)
,p_validation_name=>'TITULAR_TC'
,p_validation_sequence=>80
,p_validation=>'P152_TITULAR_TC'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el titular de la Tarjeta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871234734544693631)
,p_validation_name=>'BANCOS'
,p_validation_sequence=>90
,p_validation=>'P152_BANCOS'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar el Banco'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871235144118693632)
,p_validation_name=>'TARJETA'
,p_validation_sequence=>100
,p_validation=>'P152_TARJETA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingresar la Tarjeta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871238558150693635)
,p_validation_name=>'TJ_NUMERO'
,p_validation_sequence=>120
,p_validation=>'P152_TJ_NUMERO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el numero de la Tarjeta'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871238973941693635)
,p_validation_name=>'TJ_NUMERO_VOUCHER'
,p_validation_sequence=>130
,p_validation=>'P152_TJ_NUMERO_VOUCHER'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Numero del Voucher'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871239354191693636)
,p_validation_name=>'MCD_NRO_AUT_REF'
,p_validation_sequence=>140
,p_validation=>'P152_MCD_NRO_AUT_REF'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la Autorizacion'
,p_validation_condition=>':request = ''CARGAR_TFP'' AND :P152_TFP_ID = pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_tarjeta_credito'')'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871239821092693636)
,p_validation_name=>'MCD_FECHA_DEPOSITO'
,p_validation_sequence=>150
,p_validation=>'P152_MCD_FECHA_DEPOSITO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese la fecha de deposito'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''CARGAR_TFP''',
'AND (:P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(117871240162153693636)
,p_validation_name=>'p152_MCD_FECHA_DEPOSITO'
,p_validation_sequence=>160
,p_validation=>'trunc(to_date(:P152_MCD_FECHA_DEPOSITO)) <= trunc(sysdate)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Fecha deposito no puede ser mayor a la fecha actual'
,p_validation_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':REQUEST = ''cargar_tfp''',
'AND (:P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_deposito'') or :P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_transferencia''))'))
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(115122578721151687831)
,p_validation_name=>'PR_VALIDA_PUNTULITO'
,p_validation_sequence=>170
,p_validation=>'round(to_number(nvl(:P152_SALDO_MAX_APLICAR,0)),4)>= round(to_number(nvl(:P152_MCD_VALOR_MOVIMIENTO,0)),4)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El valor no debe ser mayor al "Saldo M\00E1ximo a Aplicar....')
,p_validation_condition=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_puntualito_regalon'')  and :P152_MCD_VALOR_MOVIMIENTO > 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(117871232070644693583)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(115122578798955687832)
,p_validation_name=>'PR_VALIDA_PUNTULITO_DEV'
,p_validation_sequence=>180
,p_validation=>'round(to_number(nvl(:P152_SALDO_MAX_APL_DEV,0)),4)>= round(to_number(nvl(:P152_MCD_VALOR_MOVIMIENTO,0)),4)'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>unistr('El valor no debe ser mayor al "Saldo M\00E1ximo a Aplicar....')
,p_validation_condition=>':P152_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_dev_promo'')  and :P152_MCD_VALOR_MOVIMIENTO > 0'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(117871232070644693583)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871243256861693645)
,p_name=>'ad_elimina'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.elimina'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871243845444693651)
,p_event_id=>wwv_flow_imp.id(117871243256861693645)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'apex_collection.delete_member(''CO_MOV_CAJA'', :P152_SEQ_ID_MOVCAJA);'
,p_attribute_02=>'P152_SEQ_ID_MOVCAJA'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871244206070693654)
,p_name=>'pr_grabar'
,p_event_sequence=>20
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(38525611224867238531)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871244725431693655)
,p_event_id=>wwv_flow_imp.id(117871244206070693654)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'GRABAR'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871242087373693643)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
' ',
'cn_var_id_total_cred_entrada number := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total_cred_entrada'');',
'',
'',
'BEGIN',
'',
'/*',
'select sum(vpd_var12) ',
'into :P152_VALOR_TOTAL_PAGOS',
'from VEN_PUNTO_VENTA_DET where pvc_id = :p152_pvc_id;*/',
'',
'',
'',
'  pr_pruebas_ja(pn_number  => 2,',
'                pv_varchar => ''CARGA - '' || :P152_VALOR_TOTAL_PAGOS,',
'                pc_clob    => NULL);',
'',
'',
'',
'',
'',
'select round(sum(vpv.vpv_valor),2)',
'into :P152_VALOR_TOTAL_PAGOS',
'  from ven_punto_venta_det pvd, ven_var_punto_venta vpv',
' where pvd.pvd_id = vpv.pvd_id',
'   and vpv.var_id = cn_var_id_total_cred_entrada',
'   and pvd.pvc_id = :p152_pvc_id;',
'  ',
'if :p152_saldo_anticipos > 0 then',
':p152_tfp_id := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_anticipo_clientes'');',
'else',
':p152_tfp_id := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
'end if;',
'',
':p152_seq_id               := NULL;',
':p152_cre_id               := NULL;',
':p152_cre_fecha_deposito   := SYSDATE;',
':p152_ede_id               := NULL;',
':p152_cre_titular_cuenta   := NULL;',
':p152_cre_nro_cheque       := NULL;',
':p152_cre_nro_cuenta       := NULL;',
':p152_cre_nro_pin          := NULL;',
':p152_mcd_valor_movimiento := NULL;',
'',
':p152_MCD_FECHA_DEPOSITO := NULL;',
':p152_TJ_ENTIDAD         := NULL;',
':p152_BANCOS             := NULL;',
':p152_TARJETA            := NULL;',
':p152_TIPO               := NULL;',
':p152_PLAN               := NULL;        ',
':p152_TJ_NUMERO           := NULL;',
':p152_TJ_NUMERO_VOUCHER   := NULL;',
':p152_TJ_RECAP            := NULL;',
':p152_LOTE                := NULL;',
':p152_MCD_NRO_AUT_REF     := NULL;',
':p152_MCD_NRO_COMPROBANTE := NULL;',
':p152_MODIFICACION        := NULL;',
':p152_MCD_FECHA_DEPOSITO  := NULL;',
'',
':p152_RAP_NRO_RETENCION    := NULL;',
':p152_RAP_NRO_AUTORIZACION := NULL;',
':p152_RAP_FECHA            := NULL;',
':p152_RAP_FECHA_VALIDEZ    := NULL;',
':p152_RAP_COM_ID           := NULL;',
':p152_SALDO_RETENCION      := NULl;',
':p152_RAP_NRO_ESTABL_RET   := NULL;',
':p152_RAP_NRO_PEMISION_RET := NULL;',
':p152_PRE_ID               := NULL;',
':p152_vuelto               := NULL;',
'    ',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'PAGAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117838988936103928717
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871241328794693640)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_seq_id number;',
'begin',
'',
'select seq_id',
'into ln_seq_id',
'from apex_collections where collection_name = ''CO_MOV_CAJA''',
'and seq_id = :P152_SEQ_ID_MOVCAJA;',
'if ln_seq_id > 0 then',
'apex_collection.delete_member(''CO_MOV_CAJA'', :P152_SEQ_ID_MOVCAJA);',
'end if;',
':P152_VUELTO :=null;',
':P152_MCD_VALOR_MOVIMIENTO := null;',
'exception when no_data_found then null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'ELIMINA_MOVCAJA'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117838988177524928714
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871241734987693640)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_mca_id          asdm_movimientos_caja.mca_id%TYPE;',
'  ln_age_id_agente   asdm_agentes.age_id%TYPE;',
'  ln_ttr_id_pago     asdm_tipos_transacciones.ttr_id%TYPE;',
'  ln_ttr_id_car      asdm_tipos_transacciones.ttr_id%TYPE;',
'BEGIN',
'',
'ln_age_id_agente := pq_car_credito_interfaz.fn_retorna_agente_conectado(:f_user_id,',
'                                                                            :f_emp_id);',
'/*',
'ln_ttr_id_pago := pq_constantes.fn_retorna_constante(0,',
'                                                       ''cn_ttr_id_pago_cuota'');',
'ln_ttr_id_car  := pq_constantes.fn_retorna_constante(0,',
'                                                       ''cn_ttr_id_credito_car_pago_cuota'');',
'',
'PQ_CAR_PAGO_CUOTA.pr_graba_pago_cuota(pn_mca_id                => ln_mca_id,',
'                                      pn_emp_id                => :f_emp_id,',
'                                      pn_uge_id                => :f_uge_id,',
'                                      pn_uge_id_gasto          => :f_uge_id_gasto,',
'                                      pn_user_id               => :f_user_id,',
'                                      pn_pca_id                => :f_pca_id,',
'                                      pn_age_id_gestionado_por => ln_age_id_agente,',
'                                      pn_cli_id                => :p152_cli_id,',
'                                      pn_mca_total             => :p152_valor_total_pagos,',
'                                      pn_tse_id                => :f_seg_id,',
'                                      pn_age_id_agente         => ln_age_id_agente,',
'                                      pn_age_id_agencia        => :f_age_id_agencia,',
'                                      pn_ttr_id_origen         => ln_ttr_id_pago,',
'                                      pn_ttr_id_cartera        => ln_ttr_id_car,',
'                                      pn_mca_id_reversar       => null,',
'                                      pn_cxc_id_ref            => :P152_CXC_ID_REF,',
'                                      pv_error                 => :p0_error);',
'                                      ',
'pr_url(pn_app_destino => 211,',
'       pn_pag_destino => 19,',
'       pv_request     => ''imprimir'',',
'       pv_parametros  => ''F_PARAMETRO,P19_MCA_ID'',',
'       pv_valores     => :f_parametro || '','' || ln_mca_id);                 */          ',
' --- Null los campos ---',
'      ',
':p152_seq_id               := NULL;',
':p152_cre_id               := NULL;',
':p152_cre_fecha_deposito   := SYSDATE;',
':p152_ede_id               := NULL;',
':p152_cre_titular_cuenta   := NULL;',
':p152_cre_nro_cheque       := NULL;',
':p152_cre_nro_cuenta       := NULL;',
':p152_cre_nro_pin          := NULL;',
':p152_mcd_valor_movimiento := NULL;',
'',
':p152_MCD_FECHA_DEPOSITO := NULL;',
':p152_TJ_ENTIDAD         := NULL;',
':p152_BANCOS             := NULL;',
':p152_TARJETA            := NULL;',
':p152_TIPO               := NULL;',
':p152_PLAN               := NULL;        ',
':p152_TJ_NUMERO           := NULL;',
':p152_TJ_NUMERO_VOUCHER   := NULL;',
':p152_TJ_RECAP            := NULL;',
':p152_LOTE                := NULL;',
':p152_MCD_NRO_AUT_REF     := NULL;',
':p152_MCD_NRO_COMPROBANTE := NULL;',
':p152_MODIFICACION        := NULL;',
':p152_MCD_FECHA_DEPOSITO  := NULL;',
'',
':p152_RAP_NRO_RETENCION    := NULL;',
':p152_RAP_NRO_AUTORIZACION := NULL;',
':p152_RAP_FECHA            := NULL;',
':p152_RAP_FECHA_VALIDEZ    := NULL;',
':p152_RAP_COM_ID           := NULL;',
':p152_SALDO_RETENCION      := NULl;',
':p152_RAP_NRO_ESTABL_RET   := NULL;',
':p152_RAP_NRO_PEMISION_RET := NULL;',
':p152_PRE_ID               := NULL;',
':P152_SEQ_ID_MOVCAJA       := null;',
':p152_mcd_valor_movimiento := null;',
':P152_CXC_ID_REF:=NULL;',
':p152_tfp_id := pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');    ',
'',
'                                      ',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117871192224893693487)
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117838988583717928714
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871240507624693636)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_com_id_retencion ven_comprobantes.com_id%TYPE;',
'',
'BEGIN',
'',
'  /*Agregado por Andres Calle',
'  07/septiembre/2011',
'  Cuando se realiza un abono por tarjeta de credito la entidad destino tomo la del plan de tarjeta de credito',
'  */',
'  IF :p152_tfp_id =',
'     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                        ''cn_tfp_id_tarjeta_credito'') THEN',
'    :p152_ede_id             := :p152_plan;',
'    :p152_cre_titular_cuenta := :p152_titular_tc; -- Para que se muestre en el mismo titular del cheque pero igual se guarde en el titular de TC',
'    /*Agregado por Andres Calle',
'    14/Octubre/2011',
'    valido que la comision de la nueva tarjeta de credito no sea mayor a la comision de la tarjeta original',
'    */',
'  ',
'    IF :p152_comision > 0 THEN',
'      pq_ven_movimientos_caja.pr_comision_tarjeta(pn_emp_id   => :f_emp_id,',
'                                                  pn_ede_id   => :p152_ede_id,',
'                                                  pn_comision => :p152_comision,',
'                                                  pv_error    => :p0_error);',
'    ',
'    END IF;',
'  END IF;',
'',
'  :p152_vuelto := NULL;',
'  ',
'  ',
'  ',
'  if :p152_tfp_id in ( pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_credito_punto_venta'')  , pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_tfp_id_desc_rol_punto_venta'') ) and  nvl(to_number(:p152_mcd_valor_movimiento),',
'                                                                               0)  > nvl(to_number(:P152_CUPO_EMPLEADO), 0) then',
'    ',
'     ',
'   :P152_OBSERVACION  := ''El valor cargado excede el cupo disponoble del cliente'';',
'  ',
'   else',
'  ',
'     :P152_OBSERVACION  := '''';',
'',
'  ',
'  ',
'  pq_car_pago_cuota.pr_carga_coleccion_mov_caja(pn_ede_id               => :p152_ede_id,',
'                                                pn_cre_id               => :p152_cre_id,',
'                                                pn_tfp_id               => :p152_tfp_id,',
'                                                pn_mcd_valor_movimiento => nvl(to_number(:p152_mcd_valor_movimiento),',
'                                                                               0),',
'                                                pn_mcd_nro_aut_ref      => :p152_mcd_nro_aut_ref,',
'                                                pn_mcd_nro_lote         => :p152_lote,',
'                                                pn_mcd_nro_retencion    => :p152_rap_nro_retencion,',
'                                                pn_mcd_nro_comprobante  => :p152_mcd_nro_comprobante,',
'                                                pv_titular_cuenta       => :p152_cre_titular_cuenta,',
'                                                pv_cre_nro_cheque       => :p152_cre_nro_cheque,',
'                                                pv_cre_nro_cuenta       => :p152_cre_nro_cuenta,',
'                                                pv_nro_pin              => :p152_cre_nro_pin,',
'                                                pv_nro_tarjeta          => :p152_tj_numero,',
'                                                pv_voucher              => :p152_tj_numero_voucher,',
'                                                pn_saldo_retencion      => nvl(to_number(:p152_saldo_retencion),',
'                                                                               0),',
'                                                pn_com_id               => ln_com_id_retencion,',
'                                                pd_rap_fecha            => :p152_rap_fecha,',
'                                                pd_rap_fecha_validez    => :p152_rap_fecha_validez,',
'                                                pn_rap_nro_autorizacion => :p152_rap_nro_autorizacion,',
'                                                pn_rap_nro_establ_ret   => :p152_rap_nro_establ_ret,',
'                                                pn_rap_nro_pemision_ret => :p152_rap_nro_pemision_ret,',
'                                                pn_pre_id               => :p152_pre_id,',
'                                                pd_fecha_deposito       => :p152_mcd_fecha_deposito,',
'                                                pv_titular_tarjeta_cre  => :p152_titular_tc,',
'                                                pn_emp_id               => :f_emp_id,',
'                                                pn_total_pagos          => :p152_valor_total_pagos,',
'                                                pn_vueltos              => :p152_vuelto,',
'                                                pn_cli_id               => :p152_cli_id,',
'                                                pn_uge_id               => :f_uge_id,',
'                                                pv_error                => :p0_error);',
'',
'',
'end if;',
'  --- Null los campos ---',
'',
'  :p152_seq_id               := NULL;',
'  :p152_cre_id               := NULL;',
'  :p152_cre_fecha_deposito   := SYSDATE;',
'  :p152_ede_id               := NULL;',
'  :p152_cre_titular_cuenta   := NULL;',
'  :p152_cre_nro_cheque       := NULL;',
'  :p152_cre_nro_cuenta       := NULL;',
'  :p152_cre_nro_pin          := NULL;',
'  :p152_mcd_valor_movimiento := NULL;',
'',
'  :p152_mcd_fecha_deposito  := NULL;',
'  :p152_tj_entidad          := NULL;',
'  :p152_bancos              := NULL;',
'  :p152_tarjeta             := NULL;',
'  :p152_tipo                := NULL;',
'  :p152_plan                := NULL;',
'  :p152_tj_numero           := NULL;',
'  :p152_tj_numero_voucher   := NULL;',
'  :p152_tj_recap            := NULL;',
'  :p152_lote                := NULL;',
'  :p152_mcd_nro_aut_ref     := NULL;',
'  :p152_mcd_nro_comprobante := NULL;',
'  :p152_modificacion        := NULL;',
'  :p152_mcd_fecha_deposito  := NULL;',
'',
'  :p152_rap_nro_retencion    := NULL;',
'  :p152_rap_nro_autorizacion := NULL;',
'  :p152_rap_fecha            := NULL;',
'  :p152_rap_fecha_validez    := NULL;',
'  :p152_rap_com_id           := NULL;',
'  :p152_saldo_retencion      := NULL;',
'  :p152_rap_nro_establ_ret   := NULL;',
'  :p152_rap_nro_pemision_ret := NULL;',
'  :p152_pre_id               := NULL;',
'  :p152_seq_id_movcaja       := NULL;',
'  :p152_mcd_valor_movimiento := NULL;',
'  :p152_tfp_id               := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                   ''cn_tfp_id_efectivo'');',
'                                                                   ',
'                                                                   ',
'exception',
'  when others then',
'    raise_application_error(-20000, sqlerrm);                                                                   ',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CARGAR_TFP'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117838987356354928710
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871240936972693640)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_valida_retencion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_Ret NUMBER;',
'',
'BEGIN',
'  IF :P152_BASE > 0 THEN',
'    pq_ven_pagos_cuota_retencion.pr_carga_val_retenciones(PN_EMP_ID               => :f_emp_id,',
'                                                          PN_UGE_ID               => :f_uge_id,',
'                                                          PN_TSE_ID               => :f_seg_id,',
'                                                          pn_com_id               => :P152_ORD_ID,',
'                                                          pd_rap_fecha            => :P152_RAP_FECHA,',
'                                                          pd_rap_fecha_validez    => :P152_RAP_FECHA_VALIDEZ,',
'                                                          pn_rap_nro_retencion    => :P152_RAP_NRO_RETENCION,',
'                                                          pn_rap_nro_autorizacion => :P152_RAP_NRO_AUTORIZACION,',
'                                                          pn_RAP_NRO_ESTABL_RET   => :P152_RAP_NRO_ESTABL_RET,',
'                                                          pn_RAP_NRO_PEMISION_RET => :P152_RAP_NRO_PEMISION_RET,',
'                                                          pn_porcentaje           => :P152_tRE_ID,',
'                                                          pn_base_ret             => :p152_base,',
'                                                          pv_ret_iva              => :p152_ret_iva,',
'                                                          pv_observacion          => :P152_OBSERVACIONES_RET,',
'                                                          pv_sesion               => :P152_DEA_SECCION,',
'                                                         pn_ord_id                => :P152_ORD_ID,',
'                                                          pn_rpo_id  => :P152_tRE_ID,',
'                                                          PV_ERROR                => :p0_error);',
'  END IF;',
'',
'  IF :P30_OBSERVACIONES_RET IS NULL THEN',
'  ',
'    SELECT sum(to_number(c010))',
'      INTO :P152_MCD_VALOR_MOVIMIENTO',
'      FROM apex_collections',
'     WHERE collection_name = ''COLL_VALOR_RETENCION'';',
'  ',
'  ELSE',
'    :P152_MCD_VALOR_MOVIMIENTO := NULL;',
'  ',
'  END IF;',
'',
'  :P152_TRE_ID := NULL;',
'  :P152_BASE   := 0;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CARGA_RET'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>117838987785702928714
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871242462349693644)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Anular'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_car_recibo_cobro_manual.pr_anula_recibos(:f_uge_id,',
'                                           :P152_SECUENCIA,',
'                                            :P152_RAZON,:P0_ERROR                                           );'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(63184448552999115466)
,p_internal_uid=>117838989311079928718
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871242933675693644)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia_pantalla'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'  :P152_EDE_ID               := NULL;',
'  :P152_CRE_TITULAR_CUENTA   := NULL;',
'  :P152_MODIFICACION         := NULL;',
'  :P152_TJ_ENTIDAD           := NULL;',
'  :P152_BANCOS               := NULL;',
'  :P152_TARJETA              := NULL;',
'  :P152_TJ_NUMERO            := NULL;',
'  :P152_TJ_NUMERO_VOUCHER    := NULL;',
'  :P152_TJ_RECAP             := NULL;',
'  :P152_LOTE                 := NULL;',
'  :P152_MCD_NRO_AUT_REF      := NULL;',
'  :P152_CRE_NRO_CHEQUE       := NULL;',
'  :P152_CRE_NRO_CUENTA       := NULL;',
'  :P152_CRE_NRO_PIN          := NULL;',
'  :P152_MCD_NRO_COMPROBANTE  := NULL;',
'  :P152_RAP_NRO_ESTABL_RET   := NULL;',
'  :P152_RAP_NRO_PEMISION_RET := NULL;',
'  :P152_RAP_NRO_RETENCION    := NULL;',
'  :P152_RAP_NRO_AUTORIZACION := NULL;',
'  :P152_RAP_FECHA            := NULL;',
'  :P152_RAP_FECHA_VALIDEZ    := NULL;',
'  :P152_RAP_COM_ID           := NULL;',
'  :P152_SALDO_RETENCION      := NULL;',
'  :P152_PRE_ID               := NULL;',
'  :P152_entrada              := NULL;',
'  :P152_titular_tc           := NULL;',
'  :P152_MCD_VALOR_MOVIMIENTO := 0;  ',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request = ''limpia'' and :p0_error is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_process_success_message=>unistr('Validaci\00C3\00B3n aprobada')
,p_internal_uid=>117838989782405928718
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117875880336394351357)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_CLOSE_WINDOW'
,p_process_name=>'pr_close_dialog'
,p_attribute_02=>'N'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'BT_CARGAR,REGRESAR'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>117843627185124586431
);
wwv_flow_imp.component_end;
end;
/
