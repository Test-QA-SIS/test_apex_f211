prompt --application/pages/page_00033
begin
--   Manifest
--     PAGE: 00033
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>33
,p_name=>'NotaCreditoDevolucion'
,p_step_title=>'NotaCreditoDevolucion'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'function imprimir(url){',
'  ',
'   doSubmit(''graba_nc'');',
'   window.open(url,"", "width=808, height=565, top=85");',
'		} ',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'CAREVALO'
,p_last_upd_yyyymmddhh24miss=>'20240202152319'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37660062984082108)
,p_name=>'variables calculadas'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>120
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'        from (SELECT distinct a.var_id,',
'                              b.mca_id,',
'                              b.ttr_id,',
'                              c.pol_id,',
'                              b.vtt_orden,',
'                              a.var_descripcion,',
'                              b.vtt_mostrar_detalle,',
'                              b.vtt_mostrar_cabecera,',
'                              b.vtt_ingr_detalle,',
'                              b.vtt_ingr_cabecera,',
'                              b.vtt_ant_etiqueta || '' '' || b.vtt_etiqueta vtt_etiqueta,',
'                              b.vtt_prorrateo,',
'                              b.vtt_distribucion,',
'                              b.vtt_obtiene_de_documento,',
'                              b.vtt_func_graba_cab,',
'                              d.ite_sku_id sku_id,',
'                              to_number(e.ite_sku_id) sku_rel_id,',
'                              e.tre_id,',
'                              i.bpr_id,',
'                              d.cei_id,',
'                              null ieb_id,',
'                              f.vcd_valor_variable',
'              ',
'                FROM ppr_variables               a,',
'                     ppr_variables_ttransaccion  b,',
'                     ven_comprobantes            c,',
'                     ven_comprobantes_det        d,',
'                     ven_var_comprobantes_det    f,',
'                     ven_comprobantes_det_rel    e,',
'                     inv_comprobantes_inventario i,',
'                     apex_collections            l',
'               WHERE a.emp_id = :f_emp_id',
'                 AND a.emp_id = b.emp_id',
'                 AND a.var_id = b.var_id',
'                 AND a.var_id = f.var_id',
'                 AND b.ttr_id = 103',
'                 AND f.cde_id = d.cde_id',
'                 AND d.emp_id = a.emp_id',
'                 AND c.com_id = d.com_id',
'                 AND c.emp_id = d.emp_id',
'                 AND d.cde_id = e.cde_id(+)',
'                 AND i.com_id = c.com_id',
'                 AND d.com_id = i.com_id',
'                 AND c.com_id = 23632',
'                 AND i.cin_id = 362044',
'                 and l.collection_name = ''CO_DETALLE''',
'                 and d.ite_sku_id = l.c003',
'                 and i.bpr_id = l.c031 --unificar diferentes bodegas tomas solo de la bodega dond se ingreso la mercaderIA',
'                 and l.c006 is null',
'              union',
'              -- GEX',
'              SELECT distinct a.var_id,',
'                              b.mca_id,',
'                              b.ttr_id,',
'                              c.pol_id,',
'                              b.vtt_orden,',
'                              a.var_descripcion,',
'                              b.vtt_mostrar_detalle,',
'                              b.vtt_mostrar_cabecera,',
'                              b.vtt_ingr_detalle,',
'                              b.vtt_ingr_cabecera,',
'                              b.vtt_ant_etiqueta || '' '' || b.vtt_etiqueta vtt_etiqueta,',
'                              b.vtt_prorrateo,',
'                              b.vtt_distribucion,',
'                              b.vtt_obtiene_de_documento,',
'                              b.vtt_func_graba_cab,',
'                              d.ite_sku_id sku_id,',
'                              to_number(l.c006) sku_rel_id,',
'                              to_number(l.c007) tre_id,',
'                              i.bpr_id bpr_id,',
'                              d.cei_id,',
'                              null ieb_id,',
'                              f.vcd_valor_variable',
'                FROM ppr_variables               a,',
'                     ppr_variables_ttransaccion  b,',
'                     ven_comprobantes            c,',
'                     ven_comprobantes_det        d,',
'                     ven_var_comprobantes_det    f,',
'                     ven_comprobantes_det_rel    e,',
'                     inv_comprobantes_inventario i,',
'                     ',
'                     apex_collections l',
'               WHERE a.emp_id = :f_emp_id',
'                 AND a.emp_id = b.emp_id',
'                 AND a.var_id = b.var_id',
'                 AND a.var_id = f.var_id',
'                 AND b.ttr_id = 103',
'                 AND f.cde_id = d.cde_id',
'                 AND d.emp_id = a.emp_id',
'                 AND c.com_id = d.com_id',
'                 AND d.com_id = i.com_id',
'                 AND i.cin_id = 362044',
'                 AND c.emp_id = d.emp_id',
'                 AND d.cde_id = e.cde_id',
'                 AND c.com_id = 23632',
'                 and l.collection_name = ''CO_DETALLE''',
'                 and d.ite_sku_id = l.c003',
'                 and e.ite_sku_id = l.c006',
'                 and i.bpr_id = l.c031 --unificar diferentes bodegas tomas solo de la bodega dond se ingreso la mercaderIA',
'                 and l.c006 is not null',
'              union',
'              --items tipo regalo',
'              SELECT distinct a.var_id,',
'                              b.mca_id,',
'                              b.ttr_id,',
'                              c.pol_id,',
'                              b.vtt_orden,',
'                              a.var_descripcion,',
'                              b.vtt_mostrar_detalle,',
'                              b.vtt_mostrar_cabecera,',
'                              b.vtt_ingr_detalle,',
'                              b.vtt_ingr_cabecera,',
'                              b.vtt_ant_etiqueta || '' '' || b.vtt_etiqueta vtt_etiqueta,',
'                              b.vtt_prorrateo,',
'                              b.vtt_distribucion,',
'                              b.vtt_obtiene_de_documento,',
'                              b.vtt_func_graba_cab,',
'                              d.ite_sku_id sku_id,',
'                              to_number(e.ite_sku_id) sku_rel_id,',
'                              e.tre_id,',
'                              null bpr_id,',
'                              d.cei_id,',
'                              null ieb_id,',
'                              f.vcd_valor_variable',
'                FROM ppr_variables              a,',
'                     ppr_variables_ttransaccion b,',
'                     ven_comprobantes           c,',
'                     ven_comprobantes_det       d,',
'                     ven_var_comprobantes_det   f,',
'                     ven_comprobantes_det_rel   e,',
'                     apex_collections           l',
'               WHERE a.emp_id = :f_emp_id',
'                 AND a.emp_id = b.emp_id',
'                 AND a.var_id = b.var_id',
'                 AND a.var_id = f.var_id',
'                 AND b.ttr_id = 103',
'                 AND f.cde_id = d.cde_id',
'                 AND d.emp_id = a.emp_id',
'                 AND c.com_id = d.com_id',
'                 AND c.emp_id = d.emp_id',
'                 AND d.cde_id = e.cde_id(+)',
'                 AND c.com_id = 23632',
'                 and l.collection_name = ''CO_DETALLE''',
'                 and d.ite_sku_id = l.c003',
'                 and l.c006 is null',
'                 and 0 = 0 -- para que en las parciales no cargue los regalos',
'              )',
'       order by vtt_orden'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660256223082111)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'VAR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660380897082111)
,p_query_column_id=>2
,p_column_alias=>'MCA_ID'
,p_column_display_sequence=>2
,p_column_heading=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660460855082111)
,p_query_column_id=>3
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>3
,p_column_heading=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660578870082111)
,p_query_column_id=>4
,p_column_alias=>'POL_ID'
,p_column_display_sequence=>4
,p_column_heading=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660676860082111)
,p_query_column_id=>5
,p_column_alias=>'VTT_ORDEN'
,p_column_display_sequence=>5
,p_column_heading=>'VTT_ORDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660752904082111)
,p_query_column_id=>6
,p_column_alias=>'VAR_DESCRIPCION'
,p_column_display_sequence=>6
,p_column_heading=>'VAR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660881152082111)
,p_query_column_id=>7
,p_column_alias=>'VTT_MOSTRAR_DETALLE'
,p_column_display_sequence=>7
,p_column_heading=>'VTT_MOSTRAR_DETALLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37660973917082112)
,p_query_column_id=>8
,p_column_alias=>'VTT_MOSTRAR_CABECERA'
,p_column_display_sequence=>8
,p_column_heading=>'VTT_MOSTRAR_CABECERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661055793082112)
,p_query_column_id=>9
,p_column_alias=>'VTT_INGR_DETALLE'
,p_column_display_sequence=>9
,p_column_heading=>'VTT_INGR_DETALLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661151564082112)
,p_query_column_id=>10
,p_column_alias=>'VTT_INGR_CABECERA'
,p_column_display_sequence=>10
,p_column_heading=>'VTT_INGR_CABECERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661275421082112)
,p_query_column_id=>11
,p_column_alias=>'VTT_ETIQUETA'
,p_column_display_sequence=>11
,p_column_heading=>'VTT_ETIQUETA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661361689082112)
,p_query_column_id=>12
,p_column_alias=>'VTT_PRORRATEO'
,p_column_display_sequence=>12
,p_column_heading=>'VTT_PRORRATEO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661451683082112)
,p_query_column_id=>13
,p_column_alias=>'VTT_DISTRIBUCION'
,p_column_display_sequence=>13
,p_column_heading=>'VTT_DISTRIBUCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661583387082112)
,p_query_column_id=>14
,p_column_alias=>'VTT_OBTIENE_DE_DOCUMENTO'
,p_column_display_sequence=>14
,p_column_heading=>'VTT_OBTIENE_DE_DOCUMENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661667867082112)
,p_query_column_id=>15
,p_column_alias=>'VTT_FUNC_GRABA_CAB'
,p_column_display_sequence=>15
,p_column_heading=>'VTT_FUNC_GRABA_CAB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661778699082112)
,p_query_column_id=>16
,p_column_alias=>'SKU_ID'
,p_column_display_sequence=>16
,p_column_heading=>'SKU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661881961082112)
,p_query_column_id=>17
,p_column_alias=>'SKU_REL_ID'
,p_column_display_sequence=>17
,p_column_heading=>'SKU_REL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37661954443082112)
,p_query_column_id=>18
,p_column_alias=>'TRE_ID'
,p_column_display_sequence=>18
,p_column_heading=>'TRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37662065064082112)
,p_query_column_id=>19
,p_column_alias=>'BPR_ID'
,p_column_display_sequence=>19
,p_column_heading=>'BPR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37662167556082112)
,p_query_column_id=>20
,p_column_alias=>'CEI_ID'
,p_column_display_sequence=>20
,p_column_heading=>'CEI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37662255482082112)
,p_query_column_id=>21
,p_column_alias=>'IEB_ID'
,p_column_display_sequence=>21
,p_column_heading=>'IEB_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37662361395082112)
,p_query_column_id=>22
,p_column_alias=>'VCD_VALOR_VARIABLE'
,p_column_display_sequence=>22
,p_column_heading=>'VCD_VALOR_VARIABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(37662481949082112)
,p_plug_name=>'ADICIONALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(37663663617082116)
,p_plug_name=>'Datos Originales Factura'
,p_parent_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>9
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37664277819082117)
,p_name=>'DatosCredito'
,p_parent_plug_id=>wwv_flow_imp.id(37663663617082116)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (CASE',
'         WHEN d.div_nro_vencimiento = 0 THEN',
'          ''ENTRADA''',
'         ELSE',
'          ''CUOTA''',
'       END) TIPO,',
'      ',
'       d.div_valor_cuota valor',
'  FROM car_dividendos d, car_cuentas_por_cobrar c',
' WHERE c.com_id = :P33_COM_ID_FACTURA',
'   AND c.cxc_id = d.cxc_id',
'   ANd c.cxc_id = :P33_CXC_ID',
'   AND div_nro_vencimiento IN (0, 1);',
'',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(155179963345965725)
,p_query_column_id=>1
,p_column_alias=>'TIPO'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37664581254082117)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(2802001108560776741)
,p_name=>'Cartera'
,p_parent_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.cxc_id,',
'       c.cxc_id_refin,',
'       c.cxc_saldo,',
'       c.cxc_fecha_adicion,',
'       c.cxc_plazo,',
'       c.cxc_estado,',
'       f.ref_saldo_interes_np,',
'       f.valor_nota_debito',
'  FROM CAR_CUENTAS_POR_COBRAR c, car_refinanciamientos f',
' WHERE C.com_id = :P33_COM_ID_FACTURA',
'   and f.cxc_id_ant(+) = c.cxc_id',
' order by 1',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001189171776742)
,p_query_column_id=>1
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cxc id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001316377776743)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID_REFIN'
,p_column_display_sequence=>2
,p_column_heading=>'Cxc id refin'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001391132776744)
,p_query_column_id=>3
,p_column_alias=>'CXC_SALDO'
,p_column_display_sequence=>3
,p_column_heading=>'Cxc saldo'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001523732776745)
,p_query_column_id=>4
,p_column_alias=>'CXC_FECHA_ADICION'
,p_column_display_sequence=>4
,p_column_heading=>'Cxc fecha adicion'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001590003776746)
,p_query_column_id=>5
,p_column_alias=>'CXC_PLAZO'
,p_column_display_sequence=>5
,p_column_heading=>'Cxc plazo'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001715299776747)
,p_query_column_id=>6
,p_column_alias=>'CXC_ESTADO'
,p_column_display_sequence=>6
,p_column_heading=>'Cxc estado'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001849179776748)
,p_query_column_id=>7
,p_column_alias=>'REF_SALDO_INTERES_NP'
,p_column_display_sequence=>7
,p_column_heading=>'Ref saldo interes np'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802001914148776749)
,p_query_column_id=>8
,p_column_alias=>'VALOR_NOTA_DEBITO'
,p_column_display_sequence=>8
,p_column_heading=>'Valor nota debito'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(2802002019834776750)
,p_name=>'Notas de Credito y Notas de Debito ya generadas'
,p_parent_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>12
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COM_ID,',
'       COM_TIPO,',
'       COM_FECHA,',
'       (CASE',
'         WHEN VAL = 0 THEN',
'          VAL_ANT',
'         ELSE',
'          VAL',
'       END) Valor,',
'       COM_OBSERVACION,',
'       TTR_ID,',
'       TTR_DESCRIPCION,',
'       MNC_DESCRIPCION',
'  from (SELECT r.com_id,',
'               r.com_tipo,',
'               r.com_fecha,',
'               r.com_observacion,',
'               m.mnc_descripcion,',
'               r.ttr_id,',
'               t.ttr_descripcion,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total_cred_entrada'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) Val,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) VAL_ANT',
'          FROM VEN_COMPROBANTES         R,',
'               asdm_motivos_nc          m,',
'               asdm_tipos_transacciones t',
'         ',
'         WHERE R.COM_ID_REF = :P33_COM_ID_FACTURA',
'           and m.mnc_id(+) = r.mnc_id',
'        and r.com_estado_registro= 0',
'           and t.ttr_id(+) = r.ttr_id)'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002111864776751)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Com id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002172271776752)
,p_query_column_id=>2
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>2
,p_column_heading=>'Com tipo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002310232776753)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'Com fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002404754776754)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002510257776755)
,p_query_column_id=>5
,p_column_alias=>'COM_OBSERVACION'
,p_column_display_sequence=>5
,p_column_heading=>'Com observacion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002595351776756)
,p_query_column_id=>6
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Ttr id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002689442776757)
,p_query_column_id=>7
,p_column_alias=>'TTR_DESCRIPCION'
,p_column_display_sequence=>7
,p_column_heading=>'Ttr descripcion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(2802002799061776758)
,p_query_column_id=>8
,p_column_alias=>'MNC_DESCRIPCION'
,p_column_display_sequence=>8
,p_column_heading=>'Mnc descripcion'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(10395854367155103075)
,p_name=>'Notas de Credito y Notas de Debito ya generadas'
,p_parent_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>150
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COM_ID,',
'       COM_TIPO,',
'       COM_FECHA,',
'       (CASE',
'         WHEN VAL = 0 THEN',
'          VAL_ANT',
'         ELSE',
'          VAL',
'       END) Valor,',
'       COM_OBSERVACION,',
'       TTR_ID,',
'       TTR_DESCRIPCION,',
'       MNC_DESCRIPCION,',
'       com_estado_registro',
'  from (SELECT r.com_id,',
'               r.com_tipo,',
'               r.com_fecha,',
'               r.com_observacion,',
'               m.mnc_descripcion,',
'               r.ttr_id,',
'               t.ttr_descripcion,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total_cred_entrada'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) Val,',
'               (select sum(valor_var(pn_cde_id => d.cde_id,',
'                                     pn_var_id => PQ_CONSTANTES.fn_retorna_constante(R.EMP_ID,',
'                                                                                     ''cn_var_id_total'')))',
'                  from ven_comprobantes_det d',
'                 where d.com_id = r.com_id) VAL_ANT,',
'        r.com_estado_registro',
'          FROM VEN_COMPROBANTES         R,',
'               asdm_motivos_nc          m,',
'               asdm_tipos_transacciones t',
'         WHERE R.COM_ID_REF = :P33_COM_ID_FACTURA',
'           and m.mnc_id(+) = r.mnc_id',
'           and t.ttr_id(+) = r.ttr_id)'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen ND o NC generadas para esta factura'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855272713103113)
,p_query_column_id=>1
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>1
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855358160103113)
,p_query_column_id=>2
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>2
,p_column_heading=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855453028103113)
,p_query_column_id=>3
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>3
,p_column_heading=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855583515103113)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855682640103114)
,p_query_column_id=>5
,p_column_alias=>'COM_OBSERVACION'
,p_column_display_sequence=>5
,p_column_heading=>'OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855756234103114)
,p_query_column_id=>6
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>6
,p_column_heading=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855874240103114)
,p_query_column_id=>7
,p_column_alias=>'TTR_DESCRIPCION'
,p_column_display_sequence=>7
,p_column_heading=>'DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(10395855974893103114)
,p_query_column_id=>8
,p_column_alias=>'MNC_DESCRIPCION'
,p_column_display_sequence=>8
,p_column_heading=>'MOTIVO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(29244188968852600727)
,p_query_column_id=>9
,p_column_alias=>'COM_ESTADO_REGISTRO'
,p_column_display_sequence=>9
,p_column_heading=>'Com estado registro'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(37667153106082119)
,p_plug_name=>'<B>&P0_TTR_DESCRIPCION.   -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
,p_plug_comment=>'&P0_DATFOLIO.'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37672964664082127)
,p_name=>'Detalle Nota Credito'
,p_display_sequence=>20
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return  pq_ven_nota_credito.fn_apex_item_notacre_det(pn_emp_id => :f_emp_id,',
'                                                     pn_com_id => :P33_COM_ID_FACTURA,',
'                                                     pn_ttr_id =>  pq_constantes.fn_retorna_constante(0,''cn_ttr_id_nota_credito''),',
'                                                     pv_error => :p0_error);'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>120
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'MILLIMETERS'
,p_prn_paper_size=>'A4'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>297
,p_prn_height=>210
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'LEFT'
,p_prn_page_footer_alignment=>'LEFT'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675862963082129)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675961544082129)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676080110082129)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676163407082129)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676269964082129)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676362273082129)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676475567082130)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676579017082130)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676674091082130)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676752393082130)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676854595082130)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37676969336082130)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677078394082130)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677182196082130)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677271837082130)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677378575082130)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677456265082130)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677582019082130)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677661796082130)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677769746082130)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677852474082130)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37677966429082130)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678078783082130)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678169235082130)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678265131082130)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678365324082131)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678455138082131)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678576683082131)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678672105082131)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678775834082131)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678872025082131)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37678975035082131)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679057490082131)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679183148082131)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679256340082131)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679365988082131)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679473995082131)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679583710082131)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679661876082131)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679772485082131)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679865764082131)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37679960607082131)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680081045082131)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680166260082132)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680277320082132)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680373967082132)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680453788082132)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680556991082132)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680681641082132)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680755736082132)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680868546082132)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37680971684082132)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681053034082132)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681161482082132)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681278433082132)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681364238082132)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681474895082132)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681577083082132)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681677759082132)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681773650082132)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681859427082132)
,p_query_column_id=>61
,p_column_alias=>'COL61'
,p_column_display_sequence=>61
,p_column_heading=>'Col61'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37681967389082133)
,p_query_column_id=>62
,p_column_alias=>'COL62'
,p_column_display_sequence=>62
,p_column_heading=>'Col62'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682062040082133)
,p_query_column_id=>63
,p_column_alias=>'COL63'
,p_column_display_sequence=>63
,p_column_heading=>'Col63'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682180970082133)
,p_query_column_id=>64
,p_column_alias=>'COL64'
,p_column_display_sequence=>64
,p_column_heading=>'Col64'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682281832082133)
,p_query_column_id=>65
,p_column_alias=>'COL65'
,p_column_display_sequence=>65
,p_column_heading=>'Col65'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682352864082133)
,p_query_column_id=>66
,p_column_alias=>'COL66'
,p_column_display_sequence=>66
,p_column_heading=>'Col66'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682461297082133)
,p_query_column_id=>67
,p_column_alias=>'COL67'
,p_column_display_sequence=>67
,p_column_heading=>'Col67'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682580080082133)
,p_query_column_id=>68
,p_column_alias=>'COL68'
,p_column_display_sequence=>68
,p_column_heading=>'Col68'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682654083082133)
,p_query_column_id=>69
,p_column_alias=>'COL69'
,p_column_display_sequence=>69
,p_column_heading=>'Col69'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682782090082133)
,p_query_column_id=>70
,p_column_alias=>'COL70'
,p_column_display_sequence=>70
,p_column_heading=>'Col70'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682861274082133)
,p_query_column_id=>71
,p_column_alias=>'COL71'
,p_column_display_sequence=>71
,p_column_heading=>'Col71'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37682974558082133)
,p_query_column_id=>72
,p_column_alias=>'COL72'
,p_column_display_sequence=>72
,p_column_heading=>'Col72'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683068456082135)
,p_query_column_id=>73
,p_column_alias=>'COL73'
,p_column_display_sequence=>73
,p_column_heading=>'Col73'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683182741082135)
,p_query_column_id=>74
,p_column_alias=>'COL74'
,p_column_display_sequence=>74
,p_column_heading=>'Col74'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683269634082135)
,p_query_column_id=>75
,p_column_alias=>'COL75'
,p_column_display_sequence=>75
,p_column_heading=>'Col75'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683356933082135)
,p_query_column_id=>76
,p_column_alias=>'COL76'
,p_column_display_sequence=>76
,p_column_heading=>'Col76'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683470404082135)
,p_query_column_id=>77
,p_column_alias=>'COL77'
,p_column_display_sequence=>77
,p_column_heading=>'Col77'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683581513082135)
,p_query_column_id=>78
,p_column_alias=>'COL78'
,p_column_display_sequence=>78
,p_column_heading=>'Col78'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683676225082135)
,p_query_column_id=>79
,p_column_alias=>'COL79'
,p_column_display_sequence=>79
,p_column_heading=>'Col79'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683775819082135)
,p_query_column_id=>80
,p_column_alias=>'COL80'
,p_column_display_sequence=>80
,p_column_heading=>'Col80'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683859882082136)
,p_query_column_id=>81
,p_column_alias=>'COL81'
,p_column_display_sequence=>81
,p_column_heading=>'Col81'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37683954714082136)
,p_query_column_id=>82
,p_column_alias=>'COL82'
,p_column_display_sequence=>82
,p_column_heading=>'Col82'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684066146082136)
,p_query_column_id=>83
,p_column_alias=>'COL83'
,p_column_display_sequence=>83
,p_column_heading=>'Col83'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684154025082136)
,p_query_column_id=>84
,p_column_alias=>'COL84'
,p_column_display_sequence=>84
,p_column_heading=>'Col84'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684270528082136)
,p_query_column_id=>85
,p_column_alias=>'COL85'
,p_column_display_sequence=>85
,p_column_heading=>'Col85'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684363844082136)
,p_query_column_id=>86
,p_column_alias=>'COL86'
,p_column_display_sequence=>86
,p_column_heading=>'Col86'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684464112082136)
,p_query_column_id=>87
,p_column_alias=>'COL87'
,p_column_display_sequence=>87
,p_column_heading=>'Col87'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684552932082136)
,p_query_column_id=>88
,p_column_alias=>'COL88'
,p_column_display_sequence=>88
,p_column_heading=>'Col88'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684677227082136)
,p_query_column_id=>89
,p_column_alias=>'COL89'
,p_column_display_sequence=>89
,p_column_heading=>'Col89'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684768933082136)
,p_query_column_id=>90
,p_column_alias=>'COL90'
,p_column_display_sequence=>90
,p_column_heading=>'Col90'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684873923082136)
,p_query_column_id=>91
,p_column_alias=>'COL91'
,p_column_display_sequence=>91
,p_column_heading=>'Col91'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37684956426082136)
,p_query_column_id=>92
,p_column_alias=>'COL92'
,p_column_display_sequence=>92
,p_column_heading=>'Col92'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37685064504082136)
,p_query_column_id=>93
,p_column_alias=>'COL93'
,p_column_display_sequence=>93
,p_column_heading=>'Col93'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673153053082128)
,p_query_column_id=>94
,p_column_alias=>'COL94'
,p_column_display_sequence=>94
,p_column_heading=>'Col94'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673267123082128)
,p_query_column_id=>95
,p_column_alias=>'COL95'
,p_column_display_sequence=>95
,p_column_heading=>'Col95'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673374579082128)
,p_query_column_id=>96
,p_column_alias=>'COL96'
,p_column_display_sequence=>96
,p_column_heading=>'Col96'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673467622082128)
,p_query_column_id=>97
,p_column_alias=>'COL97'
,p_column_display_sequence=>97
,p_column_heading=>'Col97'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673578394082128)
,p_query_column_id=>98
,p_column_alias=>'COL98'
,p_column_display_sequence=>98
,p_column_heading=>'Col98'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673678837082128)
,p_query_column_id=>99
,p_column_alias=>'COL99'
,p_column_display_sequence=>99
,p_column_heading=>'Col99'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673753113082128)
,p_query_column_id=>100
,p_column_alias=>'COL100'
,p_column_display_sequence=>100
,p_column_heading=>'Col100'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673866116082128)
,p_query_column_id=>101
,p_column_alias=>'COL101'
,p_column_display_sequence=>101
,p_column_heading=>'Col101'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37673980800082128)
,p_query_column_id=>102
,p_column_alias=>'COL102'
,p_column_display_sequence=>102
,p_column_heading=>'Col102'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674058029082128)
,p_query_column_id=>103
,p_column_alias=>'COL103'
,p_column_display_sequence=>103
,p_column_heading=>'Col103'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674154179082128)
,p_query_column_id=>104
,p_column_alias=>'COL104'
,p_column_display_sequence=>104
,p_column_heading=>'Col104'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674253940082128)
,p_query_column_id=>105
,p_column_alias=>'COL105'
,p_column_display_sequence=>105
,p_column_heading=>'Col105'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674364516082128)
,p_query_column_id=>106
,p_column_alias=>'COL106'
,p_column_display_sequence=>106
,p_column_heading=>'Col106'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674452271082128)
,p_query_column_id=>107
,p_column_alias=>'COL107'
,p_column_display_sequence=>107
,p_column_heading=>'Col107'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674570747082129)
,p_query_column_id=>108
,p_column_alias=>'COL108'
,p_column_display_sequence=>108
,p_column_heading=>'Col108'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674679510082129)
,p_query_column_id=>109
,p_column_alias=>'COL109'
,p_column_display_sequence=>109
,p_column_heading=>'Col109'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674766340082129)
,p_query_column_id=>110
,p_column_alias=>'COL110'
,p_column_display_sequence=>110
,p_column_heading=>'Col110'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674862193082129)
,p_query_column_id=>111
,p_column_alias=>'COL111'
,p_column_display_sequence=>111
,p_column_heading=>'Col111'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37674953977082129)
,p_query_column_id=>112
,p_column_alias=>'COL112'
,p_column_display_sequence=>112
,p_column_heading=>'Col112'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675058472082129)
,p_query_column_id=>113
,p_column_alias=>'COL113'
,p_column_display_sequence=>113
,p_column_heading=>'Col113'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675180064082129)
,p_query_column_id=>114
,p_column_alias=>'COL114'
,p_column_display_sequence=>114
,p_column_heading=>'Col114'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675269525082129)
,p_query_column_id=>115
,p_column_alias=>'COL115'
,p_column_display_sequence=>115
,p_column_heading=>'Col115'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675366300082129)
,p_query_column_id=>116
,p_column_alias=>'COL116'
,p_column_display_sequence=>116
,p_column_heading=>'Col116'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675751340082129)
,p_query_column_id=>117
,p_column_alias=>'COL117'
,p_column_display_sequence=>117
,p_column_heading=>'Col117'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675474034082129)
,p_query_column_id=>118
,p_column_alias=>'COL118'
,p_column_display_sequence=>118
,p_column_heading=>'Col118'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675575449082129)
,p_query_column_id=>119
,p_column_alias=>'COL119'
,p_column_display_sequence=>119
,p_column_heading=>'Col119'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37675661621082129)
,p_query_column_id=>120
,p_column_alias=>'COL120'
,p_column_display_sequence=>120
,p_column_heading=>'Col120'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37686979114082138)
,p_name=>'Variables_detalle'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
' from ',
'apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                             ''cv_coleccionvardet'') ',
'--and c007 in (955,3,79,80,1,4,10,14,1014,3203,3202,3195)'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>5000
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'DescargarExcel'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_exp_filename=>'VariablesNotCred'
,p_plug_query_exp_separator=>','
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690377990082142)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690462663082142)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690559210082142)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690661436082142)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690758972082142)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690876084082142)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690976380082142)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691079915082142)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691170443082142)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691282210082142)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691367437082142)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691458643082142)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691581284082142)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691653605082142)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691755365082142)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691851741082142)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37691981077082143)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692063926082143)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692182798082143)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692265587082143)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692364385082143)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692479614082143)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692560910082143)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692663919082143)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692765537082143)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692868576082143)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37692967652082143)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693064132082143)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693179035082143)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693276681082143)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693353925082143)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693482091082143)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693577568082143)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693670331082143)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687163009082139)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687265628082139)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687364888082139)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687470739082139)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687582786082139)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687672580082139)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687770763082139)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687854423082139)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37687973314082139)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688063110082139)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688162123082139)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688259520082139)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688383626082139)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688476742082140)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688556423082140)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688681557082140)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688754461082140)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688853034082140)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37688968013082140)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689058495082140)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689182612082141)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689281510082141)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689360971082141)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689476022082141)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689555838082141)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689659280082141)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689764548082141)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689882410082141)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37689966326082141)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690078304082142)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690165206082142)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37690282676082142)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_print_col_width=>'0'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37693762583082144)
,p_name=>'CO_DETALLE'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                    ''cv_colecciondetalle'') '))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37693961067082145)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694080664082145)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694182703082145)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694256127082145)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694353847082145)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694454444082145)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694554183082145)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694652489082145)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694781601082145)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694878271082145)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37694962627082145)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695074106082145)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695178047082145)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695253465082146)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695359910082146)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695479808082146)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695580671082146)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695678102082146)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695767886082146)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695861652082146)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37695975556082146)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696056491082146)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696169592082146)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696263917082146)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696380093082146)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696454713082146)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696553840082146)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696675173082146)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696752968082146)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696853586082146)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37696972951082146)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697058667082146)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697156358082146)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697260617082147)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697358619082147)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697474923082147)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697563134082147)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697651519082147)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697774835082147)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697873658082147)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37697963383082147)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698080235082147)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698164761082147)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698262598082147)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698359634082147)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698460841082147)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698565388082147)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698666429082147)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698780837082147)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698851782082147)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37698977925082147)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699059197082147)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699166622082147)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699254282082147)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699376248082148)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699469820082148)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699579542082148)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699657095082148)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699762255082148)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699854201082148)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37699974260082148)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700060321082148)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700160610082148)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700261282082148)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700374597082148)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700456799082148)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37700560058082148)
,p_name=>'VAR ENTRADA'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_05'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name  = ''CO_VARIABLES_ENTRADA'''
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'DescargarExcel'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_exp_filename=>'VariablesNotCredEntrada'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700752921082149)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700864241082149)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37700955911082149)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701065662082149)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701167905082149)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701255553082149)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701369823082149)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701455064082149)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701553414082149)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701677118082149)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701772089082149)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701865476082149)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37701976716082149)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702074791082149)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702175785082149)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702272130082149)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702365543082149)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702477845082150)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702565854082150)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702679115082150)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702751581082150)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702864012082150)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37702965209082150)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703069823082150)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703160990082150)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703257539082150)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703366512082150)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703479311082150)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703576929082150)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703678321082150)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703766146082150)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703880363082150)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37703956671082150)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704068058082150)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704160124082150)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704279534082150)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704367828082150)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704466842082151)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704574604082151)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704682184082151)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704767508082151)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704878002082151)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37704957989082151)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705060652082151)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705177066082151)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705258448082151)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705367732082151)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705476091082151)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705579672082151)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705662179082151)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705754528082151)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707255827082152)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705865521082151)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37705965643082151)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>55
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706056671082151)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>56
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706170248082151)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>57
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706267215082151)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>58
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706377085082151)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>59
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706462414082152)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>60
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706571192082152)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>61
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706668296082152)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>62
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706754192082152)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>63
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706869784082152)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>64
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37706977516082152)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>65
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707083823082152)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>66
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707155625082152)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37707357002082152)
,p_name=>'TOTALES'
,p_template=>wwv_flow_imp.id(270526955861046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_tot_nc(:P0_ERROR,:f_emp_id);'
,p_display_when_condition=>'CALCULAR'
,p_display_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270527874080046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'NO_HEADINGS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707565514082153)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707679438082153)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707764634082153)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707869166082153)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37707957810082153)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708056602082153)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708171720082153)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708277645082153)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708354809082153)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708478938082153)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708581532082153)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708672321082153)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708764136082153)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708857997082154)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37708963511082154)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709077510082154)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709157015082154)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709282841082154)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709373650082154)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709482689082154)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709568714082154)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709676762082154)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709760307082154)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709858534082154)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37709959693082154)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710075115082154)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710183536082154)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710265195082154)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710377101082154)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710470580082155)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710553960082155)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710662638082155)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710760250082155)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710859700082155)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37710979711082155)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711065122082155)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711159067082155)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711256208082155)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711368729082155)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711453055082155)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711578097082155)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711682397082155)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711768153082155)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711854890082155)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37711952911082155)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712067882082155)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712153627082155)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712274466082155)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712356217082155)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712460242082156)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712560432082156)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712679853082156)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712760892082156)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712854272082156)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37712957401082156)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37713056770082156)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37713178125082156)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37713273731082156)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37713362310082156)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37713480504082156)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(37714182836082157)
,p_plug_name=>'Clave de Autorizacion'
,p_parent_plug_id=>wwv_flow_imp.id(37707357002082152)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526955861046670)
,p_plug_display_sequence=>110
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P33_VALIDA_PAGO_TARJETA > 0'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(153743081867311778)
,p_plug_name=>'Solicitar clave anticipo'
,p_parent_plug_id=>wwv_flow_imp.id(37707357002082152)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>130
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(37714780936082158)
,p_name=>'Mov_caja_nota_credito'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select *',
'from apex_collections ',
'where collection_name=pq_constantes.fn_retorna_constante(NULL,',
'                                                           ''cv_coleccion_mov_caja'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37714965605082159)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715076239082159)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715160518082159)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>6
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715279059082159)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>7
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715356356082159)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>8
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715457757082159)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>9
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715557929082159)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>1
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715671457082159)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>2
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715769882082159)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>10
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715880683082159)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>11
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37715959179082159)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>3
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716064344082159)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716158370082159)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716278963082159)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716358346082159)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716478839082159)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716578982082159)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716680300082159)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716768552082160)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716879756082160)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37716970616082160)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717058490082160)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717155433082160)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717255928082160)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717358513082160)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717480724082160)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717570599082160)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717683718082160)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717756585082160)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717864822082160)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37717959800082160)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718057260082160)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718167863082160)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718283337082160)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718382020082160)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718463392082160)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718575530082160)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718670735082160)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718753760082161)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718869022082161)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37718964487082161)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719067219082161)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719182448082161)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719262713082161)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719364229082161)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719451984082161)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719577211082161)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719671904082161)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719756359082161)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719873505082161)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37719961423082161)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720083443082161)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720165209082161)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720257321082161)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'Blob001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720371863082161)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'Xmltype001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720475270082161)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720578285082162)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720664451082162)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720753083082162)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720855861082162)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37720982231082162)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37721053227082162)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37721173625082162)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37721257128082162)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37721357638082162)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(37721481518082162)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(28584179707886296034)
,p_name=>'No Negativos'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'REGION_POSITION_05'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT c007 var_id, round(c012, 4) valor, v.vtt_etiqueta , a.c001, a.c002  ,a.c003,a.c004,a.c005,a.c006,a.c007,a.c008,a.c012   ',
'        FROM apex_collections a, ppr_variables_ttransaccion v',
'       WHERE collection_name = ''CO_VARIABLES_DETALLE''',
'         AND a.c007 = v.var_id',
'         AND v.vtt_permite_negativos = ''N''',
'         AND v.ttr_id = 103',
'       ORDER BY seq_id;'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584179846925296035)
,p_query_column_id=>1
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Var id'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584179853688296036)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180042862296037)
,p_query_column_id=>3
,p_column_alias=>'VTT_ETIQUETA'
,p_column_display_sequence=>3
,p_column_heading=>'Vtt etiqueta'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180140970296038)
,p_query_column_id=>4
,p_column_alias=>'C001'
,p_column_display_sequence=>4
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180169335296039)
,p_query_column_id=>5
,p_column_alias=>'C002'
,p_column_display_sequence=>5
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180261624296040)
,p_query_column_id=>6
,p_column_alias=>'C003'
,p_column_display_sequence=>6
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180451265296041)
,p_query_column_id=>7
,p_column_alias=>'C004'
,p_column_display_sequence=>7
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180457012296042)
,p_query_column_id=>8
,p_column_alias=>'C005'
,p_column_display_sequence=>8
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180584263296043)
,p_query_column_id=>9
,p_column_alias=>'C006'
,p_column_display_sequence=>9
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180660748296044)
,p_query_column_id=>10
,p_column_alias=>'C007'
,p_column_display_sequence=>10
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180825573296045)
,p_query_column_id=>11
,p_column_alias=>'C008'
,p_column_display_sequence=>11
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(28584180890010296046)
,p_query_column_id=>12
,p_column_alias=>'C012'
,p_column_display_sequence=>12
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37685173978082136)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_button_name=>'CARGARNUEVACANTIDAD'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CargarNuevaCantidad'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37685367985082136)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_button_name=>'ELIMINAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37685562782082137)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37713563429082156)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_imp.id(37707357002082152)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'GRABAR NOTA DE CREDITO'
,p_button_position=>'BOTTOM'
,p_button_condition=>'CALCULAR'
,p_button_condition_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37662654421082113)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_button_name=>'CALCULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'** CALCULAR NOTA DE CREDITO **'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(37662865996082114)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_button_name=>'ACTUALIZAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Actualizar'
,p_button_position=>'BOTTOM'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(37723061869082169)
,p_branch_action=>'f?p=&APP_ID.:33:&SESSION.:GRABAR:&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(37713563429082156)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(37723267106082170)
,p_branch_action=>'f?p=&APP_ID.:33:&SESSION.:CALCULAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(37662654421082113)
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 11-FEB-2012 17:48 by FPENAFIEL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37663067378082115)
,p_name=>'P33_VALOR_CASTIGO'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_prompt=>'Valor a &P33_DESCRIPCION_CASTIGO.'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*:P33_TOT_PAR = 0 and  :f_seg_id = pq_constantes.fn_retorna_constante(null,''cn_tse_id_minoreo'')*/',
'',
':P33_PCO_CASTIGO_INGRESABLE=''S''',
''))
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37663274891082115)
,p_name=>'P33_CASTIGO_RECOMENDADO'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Castigo Recomendado'
,p_source=>'return pq_ven_nota_credito.fn_retorna_valor_sugerido(:f_emp_id,:P33_COM_ID_FACTURA,:P33_CXC_ID,:P0_ERROR);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:16px; color: green;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':f_seg_id=2'
,p_display_when2=>'0'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37663461481082115)
,p_name=>'P33_X'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(37662481949082112)
,p_item_default=>':P33_TVE_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'X'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37663852513082116)
,p_name=>'P33_TOTAL_FACTURA'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(37663663617082116)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Factura'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round(sum(v.vcd_valor_variable), 2)',
'  from ven_var_comprobantes_det v, ven_comprobantes_det d',
' where v.cde_id = d.cde_id',
'   and d.com_id = :P33_COM_ID_FACTURA',
'   and var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total'')',
'   and d.ite_sku_id not in',
'       (select ite_sku_padre',
'          from inv_relaciones_item d',
'         where d.tre_id =',
'               pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                  ''cn_tre_id_combo''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37664062256082116)
,p_name=>'P33_SALDO_CARTERA'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(37663663617082116)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Saldo Cartera'
,p_source=>'select sum(round(cxc_saldo,2)) from car_cuentas_por_cobrar where com_id = :P33_COM_ID_FACTURA AND cxc_estado = ''GEN'''
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37667367249082120)
,p_name=>'P33_MCA_ID_ANT_CLI'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37667563241082121)
,p_name=>'P33_TVE_ID'
,p_item_sequence=>95
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tve Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT var.vco_valor_variable',
'  FROM ven_var_comprobantes var',
' WHERE var.com_id = :P33_COM_ID_FACTURA',
'   AND var.var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_tve'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37667755382082121)
,p_name=>'P33_COM_ID_NCR'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Ncr Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37667981733082121)
,p_name=>'P33_TRX_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37668166238082121)
,p_name=>'P33_MNC_ID'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Motivo nota de cr\00C3\00A9dito:')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_MNC_ID'
,p_lov=>'SELECT  MO.MNC_DESCRIPCION, MO.MNC_ID FROM ASDM_MOTIVOS_NC MO, VEN_PARAMETRIZACION_COMP PC WHERE MO.MNC_ID=PC.MNC_ID AND PC.PCO_ID = (SELECT PCO_ID FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P33_CIN_ID)'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37668351855082122)
,p_name=>'P33_MCA_OBSERVACION'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>unistr('Observaci\00C3\00B3n:')
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cMaxlength=>200
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37668579979082122)
,p_name=>'P33_MCA_ID'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37668780787082122)
,p_name=>'P33_FACTURA_ORACLE'
,p_item_sequence=>22
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Fac Oracle'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37668966762082122)
,p_name=>'P33_CLI_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37669168446082122)
,p_name=>'P33_PER_NRO_IDENTIFICACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'# Identificaci&oacute;n:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37669362830082123)
,p_name=>'P33_FAC_NUMERO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'# Factura:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge_num_sri||'' - ''||pue_num_sri||'' - ''||com_numero',
'FROM VEN_COMPROBANTES WHERE COM_ID = nvl(:P33_COM_ID_FACTURA,0)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37669552406082123)
,p_name=>'P33_AGE_ID_AGENTE'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Age Id Agente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37669767208082123)
,p_name=>'P33_ORD_ID'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Ord Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37669955482082123)
,p_name=>'P33_PUE_NUM_SRI'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Punto de Emisi&oacute;n:'
,p_source=>':P0_PUE_NUM_SRI'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37670161099082124)
,p_name=>'P33_COM_NUM'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Secuencia NC:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P33_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37670373915082124)
,p_name=>'P33_POLITICA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Politica:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT A.POL_DESCRIPCION_LARGA',
'FROM PPR_POLITICAS A',
'WHERE POL_ID = NVL(:P33_POL_ID,0)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37670575199082124)
,p_name=>'P33_TOT_PAR'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Tot Par'
,p_source=>'RETURN PQ_VEN_COMUNES.fn_retorna_nc_partot(pn_com_id =>:P33_COM_ID_FACTURA, pn_emp_id => :f_emp_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37670774073082124)
,p_name=>'P33_CXC_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37670982755082125)
,p_name=>'P33_FORMA_VENTA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Forma Venta:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT t.tve_descripcion',
'  FROM ven_terminos_venta t',
' WHERE t.emp_id = :f_emp_id',
'   AND t.tve_id = :P33_TVE_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37671178562082125)
,p_name=>'P33_PLAZO'
,p_item_sequence=>104
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Plazo:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37671369575082125)
,p_name=>'P33_UGE_NUM_EST_SRI'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Establecimiento:'
,p_source=>':P0_UGE_NUM_EST_SRI'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37671578567082125)
,p_name=>'P33_NOMBRE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cliente:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.nombre_cliente',
'  from v_asdm_clientes_facturar c',
' where c.cli_id = :P33_CLI_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>50
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37671758793082126)
,p_name=>'P33_CIN_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Cin Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37671982720082126)
,p_name=>'P33_PCO_ID'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pco Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PCO_ID FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P33_CIN_ID',
'',
'/*declare',
'ln_pco_id number;',
'begin',
'SELECT PCO_ID into ln_pco_id FROM INV_ORDENES_DEVOLUCION WHERE CIN_ID=:P33_CIN_ID;',
':P33_PCO_ID := ln_pco_id;',
'end;*/'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37672181167082126)
,p_name=>'P33_PCO_CASTIGO_INGRESABLE'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Pco Castigo Ingresable'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select f.pco_castigo_ingresable',
'   ',
'      from ven_parametrizacion_comp f',
'     where f.pco_id = :P33_PCO_ID',
'       and f.emp_id = :f_emp_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37672372066082126)
,p_name=>'P33_COM_ID_FACTURA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'#Com Ref SIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37672565317082127)
,p_name=>'P33_COM_NUMERO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Com Numero Factura'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge_num_sri||'' - ''||pue_num_sri||'' - ''||com_numero',
'FROM VEN_COMPROBANTES WHERE COM_ID = nvl(:P33_COM_ID_FACTURA,0)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37672773119082127)
,p_name=>'P33_POL_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_prompt=>'Pol Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37685752940082137)
,p_name=>'P33_SEQ_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37685951839082137)
,p_name=>'P33_ITE_SKU_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37686160782082137)
,p_name=>'P33_IEB_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37686379042082138)
,p_name=>'P33_CEI_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37686559522082138)
,p_name=>'P33_CANTIDAD'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(d.vcd_valor_variable,0)',
'  FROM ven_var_comprobantes_det d',
' WHERE d.cde_id = :P33_CDE_ID',
'   AND d.var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_var_id_cantidad'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37686761624082138)
,p_name=>'P33_CDE_ID'
,p_item_sequence=>105
,p_item_plug_id=>wwv_flow_imp.id(37672964664082127)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37713763536082156)
,p_name=>'33_111_SQL'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(37707357002082152)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Sql'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_tot_nc(:P0_ERROR,:f_emp_id);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>80
,p_cMaxlength=>4000
,p_cHeight=>5
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37713981020082157)
,p_name=>'P33_MCA_TOTAL'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(37707357002082152)
,p_prompt=>'Mca Total'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37714353796082157)
,p_name=>'P33_CLAVE_AUTORIZACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(37714182836082157)
,p_prompt=>'Calve Autorizacion'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(37714575121082158)
,p_name=>'P33_ALERTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(37714182836082157)
,p_item_default=>'Solicitar Clave de Autorizacion a Cartera'
,p_prompt=>' '
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:12;color:RED;font-family:Arial Narrow"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153743373239320175)
,p_name=>'P33_IDENTIFICACION'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(153743081867311778)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Identificacion'
,p_source=>'P33_PER_NRO_IDENTIFICACION'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;Color:Blue"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153743562887324980)
,p_name=>'P33_COMPROBANTE'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(153743081867311778)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Factura_interna'
,p_source=>'P33_COM_ID_FACTURA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;Color:Blue"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153743752320329893)
,p_name=>'P33_VALOR_ANTICIPO'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(153743081867311778)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Anticipo'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round((SELECT nvl(sum (d.c012),0)',
'  FROM APEX_COLLECTIONS D',
' WHERE D.collection_name =',
'       asdm_p.pq_constantes.fn_retorna_constante(null, ''cv_coleccionvardet'') and d.c007= asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total_cred_entrada''))-nvl((',
'select ROUND(SUM(cxc_saldo),2) from car_cuentas_por_cobrar where com_id = :p33_com_id_factura AND cxc_estado = ''GEN''),2),0) from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_tag_attributes=>'style="font-size:20;Color:Red"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(153743970854336548)
,p_name=>'P33_CLAVE'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(153743081867311778)
,p_prompt=>'Clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:20;Color:Blue"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155093969337163603)
,p_name=>'P33_TOTAL_ABONADO'
,p_item_sequence=>269
,p_item_plug_id=>wwv_flow_imp.id(37663663617082116)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Abonado'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (SUM(b.div_valor_cuota) - SUM(div_saldo_cuota)) abonado',
'          FROM car_cuentas_por_cobrar a, car_dividendos b',
'         WHERE b.cxc_id = a.cxc_id',
'           AND a.com_id = :p33_com_id_factura',
'           AND a.cxc_estado = ''GEN'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round((select round(sum(v.vcd_valor_variable), 2)',
'                from ven_var_comprobantes_det v, ven_comprobantes_det d',
'               where v.cde_id = d.cde_id',
'                 and d.com_id = :P33_COM_ID_FACTURA',
'                 and var_id =',
'                     pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                        ''cn_var_id_total_credito'')',
'                 and d.ite_sku_id not in',
'                     (select ite_sku_padre',
'                        from inv_relaciones_item d',
'                       where d.tre_id =',
'                             pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                ''cn_tre_id_combo''))) -',
'             (select sum(cxc_saldo)',
'                from car_cuentas_por_cobrar',
'               where com_id = :P33_COM_ID_FACTURA),',
'             2) val',
'  from dual'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(155177081282911762)
,p_name=>'P33_TOTAL_FACTURA_FIN'
,p_item_sequence=>261
,p_item_plug_id=>wwv_flow_imp.id(37663663617082116)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Factura Fin'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select round(sum(v.vcd_valor_variable), 2)',
'  from ven_var_comprobantes_det v, ven_comprobantes_det d',
' where v.cde_id = d.cde_id',
'   and d.com_id = :P33_COM_ID_FACTURA',
'   and var_id =',
'       pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_total_cred_entrada'')',
'   and d.ite_sku_id not in',
'       (select ite_sku_padre',
'          from inv_relaciones_item d',
'         where d.tre_id =',
'               pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                  ''cn_tre_id_combo''))'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(537363180803077724)
,p_name=>'P33_VALIDA_PAGO_TARJETA'
,p_item_sequence=>380
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valida Pago Tarjeta'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'RETURN pq_car_tarjetas_credito.fn_valida_pago_con_tarjeta(pn_com_id => :P33_COM_ID_FACTURA,',
'                                                                pn_emp_id => :f_emp_id);'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(8626837182746983883)
,p_name=>'P33_DESCRIPCION_CASTIGO'
,p_item_sequence=>390
,p_item_plug_id=>wwv_flow_imp.id(37663663617082116)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(17383730581391127550)
,p_name=>'P33_PUE_ELECTRONICO'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(80173533389293447361)
,p_name=>'P33_PAGINA_ORIGEN'
,p_item_sequence=>400
,p_item_plug_id=>wwv_flow_imp.id(37667153106082119)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(37721675688082164)
,p_validation_name=>'P33_VALOR_CASTIGO'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P33_VALOR_CASTIGO = ''10'';',
'END'))
,p_validation_type=>'PLSQL_ERROR'
,p_error_message=>'EL VALOR NO PUEDE SER MAYOR QUE LA ENTRADA'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(121144064669822974)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(37721867665082165)
,p_validation_name=>'P33_COM_NUMERO'
,p_validation_sequence=>20
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :p33_com_num = :P0_FOL_SEC_ACTUAL then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_imp.id(37672565317082127)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(80173533321711447360)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_compra_protegida'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  LV_ERROR VARCHAR2(4000);',
'BEGIN',
' IF NVL(:P33_PAGINA_ORIGEN,0) = 0 OR NVL(:P33_PAGINA_ORIGEN,0) <> 60 THEN',
'  ASDM_P.PQ_VEN_INTANGIBLES.pr_verifica_efectivizacion_sjs(pn_emp_id => :F_EMP_ID,',
'                                                           pn_com_id   => :P33_COM_ID_FACTURA,',
'                                                           pn_pagina   => :APP_PAGE_ID,',
'                                                           pc_error    => LV_ERROR);',
' END IF;                                                           ',
'END;                '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'carga_det'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>80141280170441682434
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(37721965013082165)
,p_process_sequence=>70
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_NOTA_CREDITO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  valida_tarjetas    EXCEPTION;',
'  valida_tarj_dia    EXCEPTION;',
'  valida_tarj_diasig EXCEPTION;',
'  ln_clave         NUMBER;',
'  ln_total_factura NUMBER;',
'  lv_error         LONG;',
'  ld_fecha         DATE;',
'  ln_ttr_id        asdm_transacciones.ttr_id%TYPE;',
'  ln_pol_id        NUMBER;',
'',
'  cn_var_id_total NUMBER := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                      ''cn_var_id_total'');',
'                                                                      ',
' ln_cxc_id car_cuentas_por_cobrar.cxc_id%TYPE;                                                                      ',
'  ln_tipo_identificacion asdm_personas.per_tipo_identificacion%TYPE;',
'  ln_nro_identificacion asdm_personas.per_nro_identificacion%TYPE;',
'  ',
'BEGIN',
'',
unistr('  --- AGREGADO POR JAIME ANDRES ORDO\00D1EZ -- VALIDAR CLAVES DE AUTORIZACION CUAN LA FORMA DE PAGO ES TARJETA DE CREDITO'),
'',
'  BEGIN',
'  ',
'    SELECT round(co.vco_valor_variable, 2)',
'      INTO ln_total_factura',
'      FROM ven_var_comprobantes co',
'     WHERE co.com_id = to_number(:p33_com_id_factura)',
'       AND co.var_id = cn_var_id_total;',
'    SELECT vco.com_fecha, vco.pol_id',
'      INTO ld_fecha, ln_pol_id',
'      FROM ASDM_E.VEN_COMPROBANTES vco',
'     WHERE vco.com_id = to_number(:p33_com_id_factura);',
'  ',
'  ',
' ',
'    IF (trunc(ld_fecha) = trunc(SYSDATE)) AND',
'       (:P33_VALIDA_PAGO_TARJETA > 0) THEN',
'      pq_car_tarjetas_credito.pr_clave_nota_tarje_mismo_dia(pn_cli_id     => to_number(:p33_cli_id),',
'                                                            pn_per_id     => NULL,',
'                                                            pn_emp_id     => :f_emp_id,',
'                                                            pn_valor      => ln_total_factura,',
'                                                            pn_resultado  => ln_clave,',
'                                                            pn_uge_id     => :f_uge_id,',
'                                                            pn_usu_id     => :f_user_id,',
'                                                            pn_tipo_clave => 8,',
'                                                            pn_tipo_uso   => 0,',
'                                                            pv_error      => :p0_error);',
'    ',
'      IF (to_number(ln_clave) = to_number(:p33_clave_autorizacion)) AND',
'         (:P33_VALIDA_PAGO_TARJETA > 0) THEN',
'        NULL;',
'      ELSE',
'        RAISE valida_tarj_dia;',
'      END IF;',
'    ELSIF (trunc(ld_fecha) < trunc(SYSDATE) AND :P33_ES_POL_GOBIERNO = 0) OR',
'          (:P33_VALIDA_PAGO_TARJETA > 0) THEN',
'    ',
'      pq_car_tarjetas_credito.pr_clave_nota_credito_tarje(pn_cli_id     => to_number(:p33_cli_id),',
'                                                          pn_per_id     => NULL,',
'                                                          pn_emp_id     => :f_emp_id,',
'                                                          pn_valor      => ln_total_factura,',
'                                                          pn_resultado  => ln_clave,',
'                                                          pn_uge_id     => :f_uge_id,',
'                                                          pn_usu_id     => :f_user_id,',
'                                                          pn_tipo_clave => 4,',
'                                                          PN_TIPO_USO   => 0,',
'                                                          pv_error      => :p0_error);',
'    ',
'      IF (to_number(ln_clave) = to_number(:p33_clave_autorizacion)) AND',
'         (:P33_VALIDA_PAGO_TARJETA > 0) THEN',
'        NULL;',
'      ELSE',
'        RAISE valida_tarj_diasig;',
'      END IF;',
'    ELSE',
'      IF :P33_ES_POL_GOBIERNO = 0 OR :P33_VALIDA_PAGO_TARJETA > 0 THEN',
'        RAISE valida_tarjetas;',
'      ELSE',
'        NULL;',
'      END IF;',
'    END IF;',
'  ',
'',
'  EXCEPTION',
'    WHEN valida_tarj_dia THEN',
'      ROLLBACK;',
'      lv_error := ''1 N/C - Forma de Pago - Tarjeta de Credito Clave Incorrecta Solicitar Clave al Jefe/a de Agencia o Cartera '' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    ',
'    WHEN valida_tarj_diasig THEN',
'      ROLLBACK;',
'      lv_error := ''2 N/C - Forma de Pago - Tarjeta de Credito Clave Incorrecta Solicitar Clave a Cartera'' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    ',
'    WHEN valida_tarjetas THEN',
'      ROLLBACK;',
'      lv_error := ''3 N/C - Forma de Pago - La fecha de la factura es mayor a la fecha actual, solicite la clave a Cartera! 2'' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    WHEN OTHERS THEN',
'    ',
'      ROLLBACK;',
'      lv_error := ''4 N/C - Forma de Pago - Tarjeta de Credito Clave Incorrecta Solicitar Clave a Cartera 3'' ||',
'                  SQLERRM;',
'      raise_application_error(-20000, lv_error);',
'    ',
'  END;',
'',
'  --- TERMINA JAIME ANDRES   ',
'',
'  IF :pv_error IS NULL THEN',
'  ',
'  /* ASDM_P.PRUEBAS_PAOB(pn_numero  => 99999999999,',
'                    pc_texto1  => NULL,',
'                    pv_texto2  => NULL,',
'                    pn_numero1 => :P33_COM_ID_FACTURA);*/',
'                    ',
'   -- INICIO PAOBERNAL 03/DICIEMBRE/2018',
unistr('   -- LLAMO AL PROCEDIMIENTO <<PR_CANCELA_CUEN_ORD>>  QUE VALIDA SI LA ORDEN DE VENTA TIENE ALGUN ITEM DE INDUCCI\00D3N PARA LIBERARLO '),
'   -- DE LA TABLA <<VEN_ORDENES_CUEN>>    ',
'       ASDM_P.PQ_VEN_ORDENES.PR_CANCELA_CUEN_COM(pn_emp_id => :f_emp_id,                                                 ',
'                                                 PN_COM_ID => :P33_COM_ID_FACTURA,',
'                                                 pv_error  => :p0_error);',
'   -- FIN PAOBERNAL 03/DICIEMBRE/2018',
'    ',
'    ---GRABA TRANSACCION NOTA DE CREDITO DEV',
'    ----FPENAFIEL GRABA NOTA CREDITO DEV 17/12/2011',
'  ',
'    pq_asdm_dml.pr_ins_asdm_transacciones(pn_trx_id              => :p33_trx_id,',
'                                          pn_emp_id              => :f_emp_id,',
'                                          pn_uge_id              => :f_uge_id,',
'                                          pn_usu_id              => :f_user_id,',
'                                          pn_ttr_id              => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                       ''cn_ttr_id_nota_credito''),',
'                                          pd_trx_fecha_ejecucion => to_date(SYSDATE,',
'                                                                            ''DD/MM/YYYY HH24:MI:SS''),',
'                                          pv_trx_estado_registro => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                       ''cv_estado_reg_activo''),',
'                                          pn_uge_id_gasto        => :f_uge_id_gasto,',
'                                          pv_error               => :p0_error);',
'  ',
'    ---GRABA COMPROBANTE NOTA DE CREDITO DEVOLUCION',
'    pq_ven_comprobantes.pr_graba_comprobantes(pn_com_id                => :p33_com_id_ncr,',
'                                              pn_emp_id                => :f_emp_id,',
'                                              pn_trx_id                => :p33_trx_id,',
'                                              pn_pue_id                => :p0_pue_id,',
'                                              pn_pol_id                => :p33_pol_id,',
'                                              pn_cli_id                => :p33_cli_id,',
'                                              pn_dir_id_enviar_factura => NULL,',
'                                              pv_com_tipo              => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                             ''cv_com_tipo_nc''),',
'                                              pd_com_fecha             => to_date(SYSDATE,',
'                                                                                  ''DD/MM/YYYY HH24:MI:SS''),',
'                                              pn_com_numero            => :P33_COM_NUM,',
'                                              pv_uge_num_sri           => :p0_uge_num_est_sri,',
'                                              pv_pue_num_sri           => :p0_pue_num_sri,',
'                                              pn_age_id_agente         => :p33_age_id_agente,',
'                                              pn_age_id_agencia        => :f_age_id_agencia,',
'                                              pn_ord_id                => :p33_ord_id,',
'                                              pv_com_observacion       => :p33_mca_observacion,',
'                                              pn_com_saldo             => NULL,',
'                                              pn_com_plazo             => NULL,',
'                                              pn_ede_id                => NULL,',
'                                              pn_com_nro_impresion     => NULL,',
'                                              pn_ttr_id                => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                             ''cn_ttr_id_nota_credito''),',
'                                              pn_uge_id                => :f_uge_id,',
'                                              pn_uge_id_gasto          => :f_uge_id_gasto,',
'                                              pn_usu_id                => :f_user_id,',
'                                              pn_cin_id                => :p33_cin_id,',
'                                              pn_com_id_ref            => :p33_com_id_factura,',
'                                              pn_mnc_id                => :p33_mnc_id,',
'                                              pn_plazo_cre             => NULL,',
'                                              pn_entrada_cre           => NULL,',
'                                              pn_valor_financiar_cre   => NULL,',
'                                              pn_tasa_cre              => NULL,',
'                                              pn_cuota_cre             => NULL,',
'                                              pn_total_cre             => NULL,',
'                                              pn_termino_venta         => NULL,',
'                                              pn_nro_folio             => :p0_nro_folio,',
'                                              pv_error                 => :p0_error);',
'  ',
'    --IVEGA.',
unistr('    --SE AGREGA PROCEDIMIENTO DE INACTIVACI\00D3N DE ITEMS LEIDOS.'),
'    --08/06/2015 18:39',
'    --IF :p0_error IS NULL THEN',
'    pq_lectores_slm.pr_inactivar_lista(:P33_ORD_ID);',
'    --END IF;',
'  ',
'    pq_ven_comprobantes.pr_imprimir(pn_emp_id    => :f_emp_id,',
'                                    pn_com_id    => :p33_com_id_ncr,',
'                                    pn_ttr_id    => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cn_ttr_id_nota_credito''),',
'                                    pn_age_id    => :f_age_id_agencia,',
'                                    pn_fac_desde => NULL,',
'                                    pn_fac_hasta => NULL,',
'                                    pv_error     => :p0_error);',
'  ',
'    IF :p0_error IS NULL THEN',
'    ',
'     /*',
'    Andres Calle',
'    25/05/2017',
'    proceso para enviar a la pantalla de refinanciamiento cada vez que haya una nota de credito parcial        ',
' ',
'    */',
'    begin',
'    select cxc_id, c.per_tipo_identificacion, c.per_nro_identificacion',
'    into ln_cxc_id, ln_tipo_identificacion, ln_nro_identificacion',
'    from ven_nc_parciales a, asdm_clientes b, asdm_personas c',
'                       where a.com_id_nc = :p33_com_id_ncr',
'                      and b.cli_id = a.cli_id',
'                      and c.per_id = b.per_id',
'                      and a.cxc_saldo > 0;    ',
'    ',
'    exception when no_data_found then',
'    ln_cxc_id := 0;',
'    end;',
'    if ln_cxc_id > 0 AND :F_SEG_ID = 2 then',
'   /*  pr_url(pn_app_origen => 211,',
'         pn_app_destino => 211,',
'         pn_pag_origen => 33,',
'         pn_pag_destino => 102,',
'         pv_sesion => :SESSION,',
'         pv_request => ''REFINNC'',',
'         pv_parametros => ''P102_TIPO_IDENTIFICACION,P102_IDENTIFICACION,P102_CXC_NC'',',
'         pv_valores => ln_tipo_identificacion||'',''||ln_nro_identificacion||'',''||ln_cxc_id,',
'         pv_error => :p0_error);*/',
'         ',
'          pr_url( ',
'         pn_app_destino => 211,',
'         pn_pag_destino => 102,',
'         pv_request => ''REFINNC'',',
'         pv_parametros => ''P102_TIPO_IDENTIFICACION,P102_IDENTIFICACION,P102_CXC_NC'',',
'         pv_valores => ln_tipo_identificacion||'',''||ln_nro_identificacion||'',''||ln_cxc_id);',
'         ',
'           ',
'         ',
'         ',
'    else    ',
'    ',
'      pq_ven_comprobantes.pr_redirect(pv_desde        => ''NC'',',
'                                      pn_app_id       => NULL,',
'                                      pn_pag_id       => NULL,',
'                                      pn_emp_id       => :f_emp_id,',
'                                      pv_session      => :session,',
'                                      pv_token        => :f_token,',
'                                      pn_user_id      => :f_user_id,',
'                                      pn_rol          => :p0_rol,',
'                                      pv_rol_desc     => :p0_rol_desc,',
'                                      pn_tree_rot     => :p0_tree_root,',
'                                      pn_opcion       => :f_opcion_id,',
'                                      pv_parametro    => :f_parametro,',
'                                      pv_empresa      => :f_empresa,',
'                                      pn_uge_id       => :f_uge_id,',
'                                      pn_uge_id_gasto => :f_uge_id_gasto,',
'                                      pv_ugestion     => :f_ugestion,',
'                                      pv_error        => :p0_error);',
'    end if;                                      ',
'    ',
'    END IF;',
'    pq_ven_comunes.pr_elimina_coleccion(:p0_error);',
'  ',
'  END IF;',
'',
'  --raise_application_error(-20000, ''aqui'');',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(37713563429082156)
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':request =''GRABAR''',
''))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>5468813743317239
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(37722161009082167)
,p_process_sequence=>80
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_detalle_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  --raise_application_error(-20000,''El sistema de NC no se encuentra disponible'');',
'  pq_ven_nota_credito.pr_carga_nota_credito(:p33_com_id_factura,',
'                                            :f_emp_id,',
'                                            :p0_ttr_id,',
'                                            :p33_cin_id,',
'                                            nvl(:p33_valor_castigo, 0),',
'                                            :p0_error);',
'',
'  BEGIN',
'    IF pq_constantes.fn_retorna_constante(0, ''cn_emp_id_marcimex'') =',
'       :f_emp_id THEN',
'      pr_upd_var_2252_fa(pn_com_id => :p33_com_id_factura,',
'                         pn_emp_id => :f_emp_id);',
'    END IF;',
'  END;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
'carga_det',
''))
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>5469009739317241
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(37722364577082167)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_RECALCULAR_NC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  valida_tarjetas EXCEPTION;',
'  ln_clave         NUMBER;',
'  ln_total_factura NUMBER;',
'  lv_error         LONG;',
'  lb_existe        BOOLEAN;',
'  ln_carga_manual  NUMBER;',
'  cn_var_id_total  NUMBER := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                       ''cn_var_id_total'');',
'BEGIN',
unistr('  --- AGREGADO POR JAIME ANDRES ORDO\00D1EZ -- VALIDAR CLAVES DE AUTORIZACION CUAN LA FORMA DE PAGO ES TARJETA DE CREDITO'),
'  IF :p0_error IS NULL THEN',
'    SELECT COUNT(var_id)',
'      INTO ln_carga_manual',
'      FROM ven_var_comprobantes_detman c',
'     WHERE c.com_id = :p33_com_id_factura;',
'    IF ln_carga_manual > 0 THEN',
'      NULL;',
'    ELSE',
'    ',
'      pq_ven_nota_credito.pr_carga_nota_credito(:p33_com_id_factura,',
'                                                :f_emp_id,',
'                                                :p0_ttr_id,',
'                                                :p33_cin_id,',
'                                                :p33_valor_castigo,',
'                                                :p0_error);',
'    END IF;',
'',
'',
'',
'pq_ven_nota_credito.pr_recalcula_nota_credito(:p33_com_id_factura,',
'                                                  :f_emp_id,',
'                                                  :p33_valor_castigo,',
'                                                  :p0_ttr_id,',
'                                                  :p33_tot_par,',
'                                                  :p33_cxc_id,',
'                                                  :p33_cin_id,',
'                                                  :p0_error);',
'    ',
'    pq_ven_nota_credito.pr_calcula_castigo(:p33_valor_castigo,',
'                                             :P33_TVE_ID,',
'                                             :f_emp_id,',
'                                             :P33_COM_ID_FACTURA,',
'                                             :P33_CXC_ID,',
'                                             :P0_TTR_ID,',
'                                             :p0_error);',
'                                             ',
'',
'',
'  END IF;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(37662654421082113)
,p_internal_uid=>5469213307317241
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(37722581116082167)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'ACTUALIZAR SEGURO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'BEGIN',
'UPDATE VEN_COMPROBANTES_DET_REL X',
'SET X.ITE_SKU_ID=3864',
'WHERE X.CDR_ID=244748;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(1587222568402082478)
,p_process_when_type=>'NEVER'
,p_internal_uid=>5469429846317241
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(157406763686055746)
,p_process_sequence=>100
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'prueba'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare ',
'',
'lv_query varchar(15000);',
'begin ',
'',
'  lv_query:=  ''create table co_detalle as select * from apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                    ''''cv_colecciondetalle'''')'' ;',
'                                           EXECUTE IMMEDIATE (lv_query);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(37662865996082114)
,p_internal_uid=>125153612416290820
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(37722777432082168)
,p_process_sequence=>100
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'lv_pue_tipo_impresion asdm_puntos_emision.pue_tipo_impresion%type;',
'ln_politica_gobierno number;',
'ln_cxc_min number;',
'ln_cxc_max number;',
'lv_pasa varchar2(5) := null;',
'BEGIN',
'  :p0_ttr_id := pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_nota_credito'');',
'   ln_tgr_id := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_nota_credito'');',
'   pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'lv_pasa := ''a'';',
'   pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                   :f_pca_id,',
'                                                    ln_tgr_id,',
'                                                  --:p0_ttr_id,',
'                                                   :p0_ttr_descripcion,',
'                                                   :p0_periodo,',
'                                                   :p0_datfolio,',
'                                                   :p0_fol_sec_actual,',
'                                                   :p0_pue_num_sri,',
'                                                   :p0_uge_num_est_sri,',
'                                                   :p0_pue_id,',
'                                                   :p0_ttr_id,',
'                                                   :P0_NRO_FOLIO,',
'                                                   :p0_error);',
'',
'lv_pasa := ''b'';',
':P33_PUE_ELECTRONICO :=pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id=>:p0_pue_id);',
'if :P33_PUE_ELECTRONICO= ''E'' OR :P33_PUE_ELECTRONICO= ''P''  THEN',
' :P33_COM_NUM:=:p0_fol_sec_actual;',
'END IF;',
'',
'lv_pasa := ''c'';',
' SELECT MIN(cx.cxc_id), MAX(cx.cxc_id)',
'              INTO ln_cxc_min, ln_cxc_max',
'              FROM car_cuentas_por_cobrar cx',
'             WHERE cx.com_id = :P33_COM_ID_FACTURA;',
'lv_pasa := ''d'';             ',
'',
'if :p33_pol_id in ( pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_pol_id_cuota_balon''),',
'                   pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_pol_id_cuota_balon_mp''))then                                                                                          ',
':p33_cxc_id := ln_cxc_min;',
'else',
':p33_cxc_id := ln_cxc_max;',
'end if;',
'lv_pasa := ''e'';',
'exception when others then',
'raise_application_error(-20000,''Error Nota de Credito. ''||lv_pasa||:p0_error||'' com_id: ''||:P33_COM_ID_FACTURA||sqlerrm);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>5469626162317242
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(8626625482598977607)
,p_process_sequence=>110
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_castigo_o_devolucion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--etenesaca 22/07/2016 NC Castigo a Mayoreo',
'IF :f_seg_id = pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'') THEN',
'  :P33_DESCRIPCION_CASTIGO:= ''castigar'';',
'else ',
'  :P33_DESCRIPCION_CASTIGO:= ''devolver'';',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>8594372331329212681
);
wwv_flow_imp.component_end;
end;
/
