prompt --application/pages/page_00044
begin
--   Manifest
--     PAGE: 00044
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>44
,p_name=>'Condonacion IM y GC'
,p_step_title=>'Condonacion Im Y Gc'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(286688577220376328)
,p_plug_name=>'region'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>1
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(286688759465376330)
,p_name=>'Dividendos de la CXC:  &P14_CXC_ID.'
,p_template=>wwv_flow_imp.id(270523080594046668)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT ''LINK'' link,',
'       car.cli_id,',
'       car.cxc_id,',
'       car.cxc_div_id div_id,',
'       car.nro_vencimiento,',
'       car.fecha_vencimiento,',
'       car.valor_cuota,',
'       car.saldo_cuota,',
'       car.gto_cob_generado,',
'       car.saldo_gto_cob_generado,',
'       car.int_mora_generado,',
'       car.saldo_int_mora,',
'       car.saldo_total_dividendo',
'  FROM v_car_cartera_clientes car',
' WHERE car.emp_id = :f_emp_id',
'   AND car.cli_id = :p44_cli_id',
'   AND car.cxc_id = :p44_cxc_id'))
,p_display_when_condition=>':p44_cxc_id is not null and :P44_DESDE_PAGO_CAJA = ''N'''
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(286688759465376330)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286688977835376342)
,p_query_column_id=>1
,p_column_alias=>'LINK'
,p_column_display_sequence=>1
,p_column_heading=>'Link'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:14:&SESSION.:cargar:&DEBUG.::P14_DIV_ID,P14_CXC_ID:#DIV_ID#,#CXC_ID#'
,p_column_linktext=>'<img src="#IMAGE_PREFIX#e2.gif" alt="Edit">'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689069215376342)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Cli Id'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689152463376342)
,p_query_column_id=>3
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689268417376342)
,p_query_column_id=>4
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689360392376343)
,p_query_column_id=>5
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>5
,p_column_heading=>'Nro Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689459585376343)
,p_query_column_id=>6
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>6
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689560450376343)
,p_query_column_id=>7
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>7
,p_column_heading=>'Valor Cuota'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689674066376343)
,p_query_column_id=>8
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>8
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689762141376343)
,p_query_column_id=>9
,p_column_alias=>'GTO_COB_GENERADO'
,p_column_display_sequence=>9
,p_column_heading=>'Gto Cob Generado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689868759376343)
,p_query_column_id=>10
,p_column_alias=>'SALDO_GTO_COB_GENERADO'
,p_column_display_sequence=>10
,p_column_heading=>'Saldo Gto Cob Generado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286689966014376343)
,p_query_column_id=>11
,p_column_alias=>'INT_MORA_GENERADO'
,p_column_display_sequence=>11
,p_column_heading=>'Int Mora Generado'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690065394376343)
,p_query_column_id=>12
,p_column_alias=>'SALDO_INT_MORA'
,p_column_display_sequence=>12
,p_column_heading=>'Saldo Int Mora'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690171122376343)
,p_query_column_id=>13
,p_column_alias=>'SALDO_TOTAL_DIVIDENDO'
,p_column_display_sequence=>13
,p_column_heading=>'Saldo Total Dividendo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(286690274437376343)
,p_name=>'<SPAN STYLE="font-size: 12pt">Valores a condonar</span>'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id, c001 div_id, c002 int_condonar, c003 gto_condonar,''Eliminar'' eliminar',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_CONDONAR'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen datos'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(286690274437376343)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690770046376344)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690466251376344)
,p_query_column_id=>2
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690562038376344)
,p_query_column_id=>3
,p_column_alias=>'INT_CONDONAR'
,p_column_display_sequence=>2
,p_column_heading=>unistr('Inter\00BF\00BFs<br>a Condonar')
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690674024376344)
,p_query_column_id=>4
,p_column_alias=>'GTO_CONDONAR'
,p_column_display_sequence=>3
,p_column_heading=>'Gasto Cobranza <br>a Condonar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286690853065376345)
,p_query_column_id=>5
,p_column_alias=>'ELIMINAR'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:44:&SESSION.:eliminar:&DEBUG.::P44_SEQ_ID:#SEQ_ID#'
,p_column_linktext=>'#ELIMINAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(286690981509376345)
,p_name=>'coleccion desde pago de cuota'
,p_template=>wwv_flow_imp.id(270523080594046668)
,p_display_sequence=>11
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT c.cli_id || '' - <br> '' || c.nombre_cliente cliente,',
'                ''<b>Cxc:</b>'' || c.cxc_id || '' <b>NroVenc:</b>'' ||',
'                c.nro_vencimiento || '' <b>FechaVenc:</b>'' ||',
'                c.fecha_vencimiento vencimiento,',
'              /*  ''<b>Agencia:</b>'' || c.nombre_unidad_gestion ||*/',
'              /*  '' <b>Factura:</b>'' || c.comprobante_id factura,*/',
'                c.cxc_div_id,',
'                /*c.gto_cob_generado gasto_cob_generado,',
'                c.saldo_gto_cob_generado saldo_gasto_cob,',
'                c.int_mora_generado int_mora_generado,',
'                c.saldo_int_mora saldo_int_mora,*/',
'                c.dias_mora dias_mora,',
'                c.dias_retraso dias_retraso,',
'                apex_item.text(11, NULL, 6) int_condonar,',
'                apex_item.text(12, NULL, 6) gto_condonar,',
'                apex_item.checkbox(10, c.cxc_div_id) seleccionar',
'  FROM v_car_cartera_clientes c, apex_collections p',
' WHERE /*(c.saldo_gto_cob_generado > 0 OR c.saldo_int_mora > 0)',
'   AND */c.emp_id = :f_emp_id',
'   AND p.collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_coleccion_pago_cuota'')',
'   AND p.c005 = c.cxc_div_id'))
,p_display_when_condition=>':P44_DESDE_PAGO_CAJA=''S'''
,p_display_when_cond2=>'SQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(286690981509376345)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286762583118815778)
,p_query_column_id=>1
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>1
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286762664614815779)
,p_query_column_id=>2
,p_column_alias=>'VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'Vencimiento'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286762858436815779)
,p_query_column_id=>3
,p_column_alias=>'CXC_DIV_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286692378653376346)
,p_query_column_id=>4
,p_column_alias=>'DIAS_MORA'
,p_column_display_sequence=>6
,p_column_heading=>'Dias Mora'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286692467153376346)
,p_query_column_id=>5
,p_column_alias=>'DIAS_RETRASO'
,p_column_display_sequence=>7
,p_column_heading=>'Dias Retraso'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286692555719376346)
,p_query_column_id=>6
,p_column_alias=>'INT_CONDONAR'
,p_column_display_sequence=>5
,p_column_heading=>unistr('Inter\00BF\00BFsCondonar')
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286692661335376346)
,p_query_column_id=>7
,p_column_alias=>'GTO_CONDONAR'
,p_column_display_sequence=>4
,p_column_heading=>'Gto Condonar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(286692775538376346)
,p_query_column_id=>8
,p_column_alias=>'SELECCIONAR'
,p_column_display_sequence=>8
,p_column_heading=>'Seleccionar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(286692882460376346)
,p_plug_name=>'Totales a condonar'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(286693072587376351)
,p_plug_name=>'<SPAN STYLE="font-size: 12pt">Intereses y Gastos de Cobranza Generados</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT distinct c.cli_id,',
'       c.fecha_concesion        fecha,',
'       c.fecha_vencimiento      fecha_venc,',
'       c.nro_vencimiento        nro_venc,',
'       c.nombre_unidad_gestion  agencia,',
'       c.comprobante_id         id_factura,',
'       c.cxc_div_id             div_id,',
'       c.cxc_id                 cxc_id,',
'       c.gto_cob_generado       gasto_cob_generado,',
'       c.saldo_gto_cob_generado saldo_gasto_cob,',
'       c.int_mora_generado      int_mora_generado,',
'       c.saldo_int_mora         saldo_int_mora,',
'       c.dias_mora              dias_mora,',
'       c.dias_retraso           dias_retraso,',
'       apex_item.text(11, 0,6) int_condonar,',
'       apex_item.text(12, 0,6) gto_condonar,',
'       apex_item.checkbox(10, c.cxc_div_id) seleccionar',
'  FROM v_car_cartera_clientes c',
' WHERE c.cli_id = :p44_cli_id',
'   AND (c.saldo_gto_cob_generado > 0 OR c.saldo_int_mora > 0)',
'   AND c.emp_id = :f_emp_id*/',
'',
'',
'SELECT distinct c.cli_id,',
'       c.fecha_cxc              fecha,',
'       c.fecha_vencimiento      fecha_venc,',
'       c.nro_vencimiento        nro_venc,       ',
'       c.agencia                agencia,',
'       c.com_id                 id_factura,',
'       c.cxc_div_id             div_id,',
'       c.cxc_id                 cxc_id,',
'       c.gtos_cob_gen       gasto_cob_generado,',
'       c.gtos_cob_gen - (c.gasto_cobr_pagado + c.div_gasto_cobr_condonado) saldo_gasto_cob,',
'       c.interes_mora_gen      int_mora_generado,',
'       c.interes_mora_gen - (c.div_int_mora_pagado + c.div_int_mora_condonado) saldo_int_mora,',
'       c.dias_mora              dias_mora,',
'       c.dias_retraso           dias_retraso,',
'       apex_item.text(11, 0,6) int_condonar,',
'       apex_item.text(12, 0,6) gto_condonar,',
'       apex_item.checkbox(10, c.cxc_div_id) seleccionar',
'  FROM v_car_cartera_total_clientes c',
' WHERE c.cli_id = :p44_cli_id',
'   AND ((c.gtos_cob_gen - (c.gasto_cobr_pagado + c.div_gasto_cobr_condonado)) > 0 OR (c.interes_mora_gen - (c.div_int_mora_pagado + c.div_int_mora_condonado)) > 0)',
'   AND c.emp_id = :F_EMP_ID'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_plug_display_when_condition=>'P44_DESDE_PAGO_CAJA'
,p_plug_display_when_cond2=>'N'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(286693272253376358)
,p_name=>'Cuentas x cobrar'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_owner=>'JARIAS'
,p_internal_uid=>254440120983611432
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694981591376383)
,p_db_column_name=>'CLI_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286693376753376379)
,p_db_column_name=>'CXC_ID'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286693466274376380)
,p_db_column_name=>'INT_MORA_GENERADO'
,p_display_order=>38
,p_column_identifier=>'AL'
,p_column_label=>'Int.Mora<br>Generado'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'INT_MORA_GENERADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286693562581376380)
,p_db_column_name=>'SALDO_INT_MORA'
,p_display_order=>39
,p_column_identifier=>'AM'
,p_column_label=>'Saldo<br>Int.Mora'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'SALDO_INT_MORA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286693683158376380)
,p_db_column_name=>'DIAS_MORA'
,p_display_order=>40
,p_column_identifier=>'AN'
,p_column_label=>'Dias<br>Mora'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'DIAS_MORA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286693766189376380)
,p_db_column_name=>'DIAS_RETRASO'
,p_display_order=>41
,p_column_identifier=>'AO'
,p_column_label=>'Dias<br>Retraso'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'DIAS_RETRASO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286693961573376380)
,p_db_column_name=>'FECHA_VENC'
,p_display_order=>43
,p_column_identifier=>'AQ'
,p_column_label=>'Fecha<br>Venc'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'FECHA_VENC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694083035376380)
,p_db_column_name=>'NRO_VENC'
,p_display_order=>44
,p_column_identifier=>'AR'
,p_column_label=>'Nro.<br>Venc'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'NRO_VENC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694280651376381)
,p_db_column_name=>'ID_FACTURA'
,p_display_order=>46
,p_column_identifier=>'AT'
,p_column_label=>'Id Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694364119376381)
,p_db_column_name=>'DIV_ID'
,p_display_order=>47
,p_column_identifier=>'AU'
,p_column_label=>'Div Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'DIV_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694460884376381)
,p_db_column_name=>'GASTO_COB_GENERADO'
,p_display_order=>48
,p_column_identifier=>'AV'
,p_column_label=>'GastoCob<br>Generado'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'GASTO_COB_GENERADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694563260376381)
,p_db_column_name=>'SALDO_GASTO_COB'
,p_display_order=>49
,p_column_identifier=>'AW'
,p_column_label=>'Saldo<br>GastoCob'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'SALDO_GASTO_COB'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694681624376382)
,p_db_column_name=>'SELECCIONAR'
,p_display_order=>50
,p_column_identifier=>'AX'
,p_column_label=>' '
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'SELECCIONAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694777472376382)
,p_db_column_name=>'INT_CONDONAR'
,p_display_order=>51
,p_column_identifier=>'AY'
,p_column_label=>unistr('Inter\00BF\00BFs<br>Condonar')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'INT_CONDONAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(286694868737376382)
,p_db_column_name=>'GTO_CONDONAR'
,p_display_order=>52
,p_column_identifier=>'AZ'
,p_column_label=>'Gasto<br>Condonar'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'GTO_CONDONAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(92119479343012385)
,p_db_column_name=>'FECHA'
,p_display_order=>53
,p_column_identifier=>'BA'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(92119557920012410)
,p_db_column_name=>'AGENCIA'
,p_display_order=>54
,p_column_identifier=>'BB'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(286695077771376383)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1293117722919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CXC_ID:FECHA:AGENCIA:ID_FACTURA:NRO_VENC:FECHA_VENC:INT_MORA_GENERADO:SALDO_INT_MORA:INT_CONDONAR:GASTO_COB_GENERADO:SALDO_GASTO_COB:GTO_CONDONAR:DIAS_MORA:DIAS_RETRASO:SELECCIONAR'
,p_break_on=>'CXC_ID:FECHA:AGENCIA:ID_FACTURA'
,p_break_enabled_on=>'CXC_ID:FECHA:AGENCIA:ID_FACTURA'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(286695375850376404)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(286692882460376346)
,p_button_name=>'CONDONAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Condonar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'P44_TOTAL_CONDONAR'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(286695183244376397)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(286692882460376346)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:::'
,p_button_condition=>'P44_DESDE_PAGO_CAJA'
,p_button_condition2=>'S'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(286695759171376405)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(286693072587376351)
,p_button_name=>'SELECCIONAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Seleccionar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(286695567421376405)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(286690981509376345)
,p_button_name=>'SELECCIONAR_PAGO_CUOTA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Seleccionar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(286698273487376438)
,p_branch_action=>'f?p=&APP_ID.:44:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(286698459092376440)
,p_branch_action=>'f?p=&FLOW_ID.:44:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(286695375850376404)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(286698665528376441)
,p_branch_action=>'f?p=&FLOW_ID.:44:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(286695567421376405)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(286698855426376441)
,p_branch_action=>'f?p=&FLOW_ID.:44:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(286695759171376405)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(286695961334376405)
,p_name=>'P44_TOTAL_GASTO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(286692882460376346)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Gasto Cobranza:'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(c003)',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_CONDONAR'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(286696175881376418)
,p_name=>'P44_TOTAL_INTERES'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(286692882460376346)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Total Inter\00BF\00BFs Mora:')
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(c002)',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_CONDONAR'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(286696366604376428)
,p_name=>'P44_CLI_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(286688577220376328)
,p_prompt=>'Cliente:'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_RETORNA_ID'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(500);',
'BEGIN',
'  lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LV_CLIENTES_CAR_VEN'');',
'  lv_lov := lv_lov || '' where per.emp_id=''||:f_emp_id;',
'  RETURN(lv_lov);',
'END;'))
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P44_DESDE_PAGO_CAJA'
,p_display_when2=>'N'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(286696571328376431)
,p_name=>'P44_TOTAL_CONDONAR'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(286692882460376346)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Condonar:'
,p_source=>':P44_TOTAL_GASTO + :P44_TOTAL_INTERES'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(286696783305376431)
,p_name=>'P44_DESDE_PAGO_CAJA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(286688577220376328)
,p_item_default=>':F_PARAMETRO'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Desde Pago Caja'
,p_source=>'F_PARAMETRO'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(286696973302376431)
,p_name=>'P44_SEQ_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(286690274437376343)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(286697173383376433)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_condonar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_car_cartera.pr_condonar(pn_emp_id       => :f_emp_id,',
'                             pn_uge_id       => :f_uge_id,',
'                             pn_usu_id       => :f_user_id,',
'                             pn_uge_id_gasto => :f_uge_id_gasto,',
'                             pv_error        => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(286695375850376404)
,p_internal_uid=>254444022113611507
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(286697572757376436)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_collection'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  pq_car_cartera_interfaz.pr_carga_div_condonar(pv_error => :p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'SELECCIONAR,SELECCIONAR_PAGO_CUOTA'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>254444421487611510
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(286697978543376437)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_resetear'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
':p44_cxc_id:=null;',
':p44_div_id:=null;',
':P44_TOTAL_GASTO:=null; ',
':P44_TOTAL_INTERES:=null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cargar'
,p_process_when_type=>'NEVER'
,p_internal_uid=>254444827273611511
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(286697361453376436)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_registro_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'    APEX_COLLECTION.DELETE_MEMBER(',
'       p_collection_name => ''COLL_DIV_CONDONAR'',',
'       p_seq             => :P44_SEQ_ID);',
'',
':P44_SEQ_ID:=null;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>254444210183611510
);
wwv_flow_imp.component_end;
end;
/
