prompt --application/pages/page_00012
begin
--   Manifest
--     PAGE: 00012
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>12
,p_name=>'ListadoAnticipoClientes'
,p_step_title=>'ListadoAnticipoClientes'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'ENARANJO'
,p_last_upd_yyyymmddhh24miss=>'20240116084206'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(42701766031474034)
,p_plug_name=>'Anticipo de Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select to_number(C001) id,',
'       C002 cli_id,',
'       C003 cliente,',
'       C004 fecha,',
'       C005 gestionado_por,',
'       C006 agencia,',
'       C007 movimiento,',
'       C008 detalle_anticipo,',
'       C009 Unidad_Gestion,',
'       C010 valor,',
'       C011 saldo_total,',
'       C012 observaciones,',
'       C013 com_id',
'  from apex_collections',
' where collection_name = ''COLL_ANTICIPO_CLIENTES'''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(42701851634474034)
,p_name=>'Anticipo de Clientes'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>10448700364709108
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42702168901474053)
,p_db_column_name=>'CLIENTE'
,p_display_order=>1
,p_column_identifier=>'B'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42702377776474053)
,p_db_column_name=>'GESTIONADO_POR'
,p_display_order=>2
,p_column_identifier=>'D'
,p_column_label=>'Gestionado<br>por'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'GESTIONADO_POR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42702468733474053)
,p_db_column_name=>'AGENCIA'
,p_display_order=>3
,p_column_identifier=>'E'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42702566623474054)
,p_db_column_name=>'MOVIMIENTO'
,p_display_order=>4
,p_column_identifier=>'F'
,p_column_label=>'Movimiento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(132903256759338579)
,p_db_column_name=>'OBSERVACIONES'
,p_display_order=>5
,p_column_identifier=>'P'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(42702772220474054)
,p_db_column_name=>'VALOR'
,p_display_order=>6
,p_column_identifier=>'H'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(75612675528936286)
,p_db_column_name=>'CLI_ID'
,p_display_order=>7
,p_column_identifier=>'J'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(75612779392936286)
,p_db_column_name=>'FECHA'
,p_display_order=>8
,p_column_identifier=>'K'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(75612875034936287)
,p_db_column_name=>'DETALLE_ANTICIPO'
,p_display_order=>9
,p_column_identifier=>'L'
,p_column_label=>'Detalle Anticipo'
,p_column_link=>'f?p=&APP_ID.:22:&SESSION.::&DEBUG.::P22_MCA_ID,P22_NOMBRE_CLIENTE,P22_NOMBREMOVIMIENTO:#DETALLE_ANTICIPO#,#CLIENTE#,#MOVIMIENTO#'
,p_column_linktext=>'#DETALLE_ANTICIPO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DETALLE_ANTICIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(75612964294936287)
,p_db_column_name=>'SALDO_TOTAL'
,p_display_order=>10
,p_column_identifier=>'M'
,p_column_label=>'Saldo Total'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SALDO_TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(92377060639667654)
,p_db_column_name=>'ID'
,p_display_order=>11
,p_column_identifier=>'N'
,p_column_label=>'Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(132875269740954421)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>12
,p_column_identifier=>'O'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6096013980413662478)
,p_db_column_name=>'COM_ID'
,p_display_order=>13
,p_column_identifier=>'Q'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(42703065384474294)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_type=>'REPORT'
,p_report_alias=>'104500'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'UNIDAD_GESTION:ID:CLI_ID:CLIENTE:FECHA:GESTIONADO_POR:AGENCIA:MOVIMIENTO:OBSERVACIONES:DETALLE_ANTICIPO:VALOR:SALDO_TOTAL:COM_ID'
,p_sort_column_1=>'ID'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'FECHA'
,p_sort_direction_2=>'ASC'
,p_sort_column_3=>'0'
,p_sort_direction_3=>'ASC'
,p_sort_column_4=>'0'
,p_sort_direction_4=>'ASC'
,p_sort_column_5=>'0'
,p_sort_direction_5=>'ASC'
,p_sort_column_6=>'0'
,p_sort_direction_6=>'ASC'
,p_break_on=>'UNIDAD_GESTION:AGENCIA:0:0:0:0'
,p_break_enabled_on=>'UNIDAD_GESTION:AGENCIA:0:0:0:0'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(75524769119498099)
,p_plug_name=>'Parametros'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>15
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(154256951457591902)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(42701766031474034)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'IMPRIMIR LISTADO ANTICIPOS'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:12:&SESSION.:anticipo:&DEBUG.:::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(34354362453574809)
,p_branch_action=>'f?p=&APP_ID.:12:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 24-DIC-2009 12:21 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34307878478586101)
,p_name=>'P12_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(75524769119498099)
,p_prompt=>unistr('Identificaci\00F3n')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES_RETORNA_ID'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(500);',
'BEGIN',
'  lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LV_CLIENTES_CAR_VEN'');',
'  lv_lov := lv_lov || '' where per.emp_id=''||:f_emp_id;',
'  RETURN(lv_lov);',
'END;'))
,p_cSize=>60
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit()"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P12_VER_POR = ''C'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75519857990466483)
,p_name=>'P12_VER_POR'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(75524769119498099)
,p_prompt=>'Ver Reporte por: '
,p_source=>'F'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC2:Cliente;C,Fecha;F'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75523270027488883)
,p_name=>'P12_DESDE'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(75524769119498099)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Desde'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P12_VER_POR = ''F'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'MONTH_AND_YEAR'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(75525554359503256)
,p_name=>'P12_HASTA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(75524769119498099)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Desde'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P12_VER_POR = ''F'''
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'MONTH_AND_YEAR'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(251483254607420655)
,p_name=>'P12_ACL_SALDO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(42701766031474034)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Acl Saldo'
,p_source=>'return pq_ven_listas_caja.fn_retorna_saldo_anticipo_cli(:f_emp_id,:p12_cli_id,:f_uge_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P12_CLI_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(38275596071645386844)
,p_name=>'P12_MCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(75524769119498099)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(38275596301986386846)
,p_name=>'submit'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_display_when_cond=>'carga_ventas'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(38275596423702386847)
,p_event_id=>wwv_flow_imp.id(38275596301986386846)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38275596215965386845)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimi'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'                                                   ',
'PQ_VEN_MOVIMIENTOS_CAJA.PR_IMPRIMIR_MOV_CAJA(pn_emp_id => :f_emp_id,',
'                                             pn_mca_id => :p12_mca_id,',
'                                             pv_error  => :p0_error);',
'                                             ',
'       ',
' ',
'       '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':P12_MCA_ID IS NOT NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>38243343064695621919
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1426348445391917173)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_anticipo'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
' lv_error  varchar2(500);',
'begin',
'    pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre => ''anticipos_clientes'',',
'                                                pv_nombre_parametros => ''pn_emp_id:pn_uge_id'',',
'                                                pv_valor_parametros => :F_EMP_ID||'':''||:F_UGE_ID,',
'                                                pn_emp_id => :F_EMP_ID,',
'                                                pv_formato => ''pdf'',',
'                                                pv_error => lv_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'anticipo'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1410221869757034710
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(75573865795632404)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coll_reporte_anti_cli'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_movimientos_caja.pr_carga_coll_reporte_anti_cli',
'                                         (:P12_CLI_ID,',
'                                          :f_emp_id,',
'                                          nvl(:P12_VER_POR,''F''),',
'                                          nvl(:P12_DESDE,sysdate),',
'                                          nvl(:P12_HASTA,sysdate),',
'                                          :f_uge_id,',
'                                          :p0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>43320714525867478
);
wwv_flow_imp.component_end;
end;
/
