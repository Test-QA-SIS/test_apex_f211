prompt --application/pages/page_00022
begin
--   Manifest
--     PAGE: 00022
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>22
,p_name=>'Report_movimientos_caja_detalle'
,p_step_title=>'Report_movimientos_caja_detalle'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'RALVARADO'
,p_last_upd_yyyymmddhh24miss=>'20240111143104'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(40297679994224824)
,p_name=>'Dividendos Pagados'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT cx.com_id comprob,',
'       cx.cxc_id,',
'       c.div_id dividendo,',
'       c.div_fecha_vencimiento fecha_vencimiento,',
'       b.mde_valor valor,',
'       a.ttr_id trans,',
'       (SELECT t.ttr_descripcion',
'          FROM asdm_tipos_transacciones t',
'         WHERE t.ttr_id = a.ttr_id) ttr_descripcion,',
'       c.div_id_ref div_ref,',
'       (SELECT e.ede_abreviatura',
'          FROM asdm_entidades_destinos e',
'         WHERE e.ede_id = c.ede_id) cartera',
'  FROM car_cuentas_por_cobrar cx,',
'       car_movimientos_cab    a,',
'       car_movimientos_det    b,',
'       car_dividendos         c',
' WHERE cx.cxc_id = a.cxc_id',
'   AND b.mca_id = a.mca_id',
'   AND c.div_id = b.div_id',
'   AND a.mca_id_movcaja = :P22_MCA_ID',
'   ORDER BY c.div_nro_vencimiento',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No existen Dividendos'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(106629272159622970)
,p_query_column_id=>1
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>1
,p_column_heading=>'Comprob'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(106629364260623002)
,p_query_column_id=>2
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(40297953788224844)
,p_query_column_id=>3
,p_column_alias=>'DIVIDENDO'
,p_column_display_sequence=>3
,p_column_heading=>'Dividendo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(40298176918224850)
,p_query_column_id=>4
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>4
,p_column_heading=>'Fecha Vencimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(40298283091224850)
,p_query_column_id=>5
,p_column_alias=>'VALOR'
,p_column_display_sequence=>5
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(106629471770623002)
,p_query_column_id=>6
,p_column_alias=>'TRANS'
,p_column_display_sequence=>6
,p_column_heading=>'# Ttr'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(106629565310623002)
,p_query_column_id=>7
,p_column_alias=>'TTR_DESCRIPCION'
,p_column_display_sequence=>7
,p_column_heading=>'Ttr Descripcion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(106630068050650213)
,p_query_column_id=>8
,p_column_alias=>'DIV_REF'
,p_column_display_sequence=>8
,p_column_heading=>'Div Ref'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(151730865986348047)
,p_query_column_id=>9
,p_column_alias=>'CARTERA'
,p_column_display_sequence=>9
,p_column_heading=>'Cartera'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(248497782076961955)
,p_plug_name=>'Detalle Movimientos Caja  <b> # &P22_MCA_ID. -  &P22_NOMBREMOVIMIENTO.  de &P22_NOMBRE_CLIENTE.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca.pca_id,',
'       mca.cli_id,',
'       mca.mca_fecha,',
'       mca.mca_observacion,',
'       mca.mca_total,',
'       mcd.tfp_id,',
'       (SELECT a.tfp_descripcion',
'          FROM asdm_tipos_formas_pago a',
'         WHERE a.tfp_id = mcd.tfp_id',
'           AND a.emp_id = mcd.emp_id) tfp_descripcion,',
'       CASE to_char(mcd.tfp_id)',
'         WHEN asdm_p.pq_constantes.fn_retorna_constante(mcd.emp_id,',
'                                                    ''cn_tfp_id_cheque'') THEN',
'         ',
'          (SELECT ''Nro Cheque = '' || cre.cre_nro_cheque',
'             FROM asdm_cheques_recibidos cre',
'            WHERE cre.cre_id = mcd.cre_id',
'              AND cre.emp_id = mcd.emp_id)',
'         WHEN',
'          asdm_p.pq_constantes.fn_retorna_constante(mcd.emp_id,',
'                                                    ''cn_tfp_id_orden_pago'') THEN',
'          ''Orden Pago = '' || mcd.opa_id',
'         WHEN',
'          asdm_p.pq_constantes.fn_retorna_constante(mcd.emp_id,',
'                                                    ''cn_tfp_id_tarjeta_credito'') THEN',
'          (SELECT ''Nro Tarjeta: '' || mcd.mcd_nro_tarjeta || '' Plan '' ||',
'                  sys_connect_by_path(e.ede_descripcion, ''->'')',
'             FROM asdm_entidades_destinos e',
'            WHERE e.ede_id = mcd.ede_id',
'              AND e.emp_id = mcd.emp_id',
'            START WITH e.ede_id_padre IS NULL',
'           CONNECT BY PRIOR e.ede_id = e.ede_id_padre)',
'         WHEN asdm_p.pq_constantes.fn_retorna_constante(mcd.emp_id,',
'                                                    ''cn_tfp_id_deposito'') THEN',
'          (SELECT sys_connect_by_path(ed.ede_descripcion, ''->'')',
'             FROM asdm_entidades_destinos ed',
'            WHERE ed.ede_id = mcd.ede_id',
'              AND ed.emp_id = mcd.emp_id',
'            START WITH ed.ede_id_padre IS NULL',
'           CONNECT BY PRIOR ed.ede_id = ed.ede_id_padre) ||',
'          '' Nro Papeleta = '' || mcd.mcd_nro_comprobante ||',
unistr('          '' Fecha Dep\00F3sito '' || mcd.mcd_fecha_deposito'),
'         WHEN',
'          asdm_p.pq_constantes.fn_retorna_constante(mcd.emp_id,',
'                                                    ''cn_tfp_id_transferencia'') THEN',
'          (SELECT sys_connect_by_path(ede.ede_descripcion, ''->'')',
'             FROM asdm_entidades_destinos ede',
'            WHERE ede.ede_id = mcd.ede_id',
'              AND ede.emp_id = mcd.emp_id',
'            START WITH ede.ede_id_padre IS NULL',
'           CONNECT BY PRIOR ede.ede_id = ede.ede_id_padre) ||',
'          ''Nro Comprobante = '' || mcd.mcd_nro_comprobante ||',
unistr('          '' Fecha Dep\00F3sito '' || mcd.mcd_fecha_deposito'),
'         WHEN asdm_p.pq_constantes.fn_retorna_constante(mcd.emp_id,',
'                                                    ''cn_tfp_id_retencion'') THEN',
'          (SELECT ''Nro Retencion '' || rap.rap_nro_establ_ret || ''-'' ||',
'                  rap.rap_nro_pemision_ret || ''-'' || rap.rap_nro_retencion ||',
'                  '' COM_ID '' || rap.com_id',
'             FROM asdm_retenciones_aplicadas rap',
'            WHERE rap.rap_id = mcd.mcd_nro_retencion',
'              AND rap.emp_id = mcd.emp_id)',
'       END datos_tfp,',
'       mcd.mcd_valor_movimiento',
'  FROM asdm_movimientos_caja mca, asdm_movimientos_caja_detalle mcd',
' WHERE mcd.mca_id = mca.mca_id',
'   AND mca.emp_id = :f_emp_id',
'   AND mcd.mca_id = :P22_MCA_ID',
'',
'',
'   AND mca.emp_id = :f_emp_id',
'   AND mcd.mca_id = :P22_MCA_ID',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(248497865148961955)
,p_name=>'Reporte Detalle Movimientos Caja'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>216244713879197029
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248498865815961962)
,p_db_column_name=>'TFP_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Tfp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'TFP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248507579996025048)
,p_db_column_name=>'TFP_DESCRIPCION'
,p_display_order=>10
,p_column_identifier=>'Q'
,p_column_label=>'Forma de Pago'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_column_alignment=>'RIGHT'
,p_static_id=>'TFP_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(248498975853961962)
,p_db_column_name=>'MCD_VALOR_MOVIMIENTO'
,p_display_order=>11
,p_column_identifier=>'J'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'MCD_VALOR_MOVIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58111662931655443)
,p_db_column_name=>'PCA_ID'
,p_display_order=>13
,p_column_identifier=>'S'
,p_column_label=>'Pca Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58111780477655443)
,p_db_column_name=>'CLI_ID'
,p_display_order=>14
,p_column_identifier=>'T'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58111963136655443)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>16
,p_column_identifier=>'V'
,p_column_label=>'Mca Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58112068504655443)
,p_db_column_name=>'MCA_OBSERVACION'
,p_display_order=>17
,p_column_identifier=>'W'
,p_column_label=>'Mca Observacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58112164726655443)
,p_db_column_name=>'MCA_TOTAL'
,p_display_order=>18
,p_column_identifier=>'X'
,p_column_label=>'Mca Total'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(58112281138655444)
,p_db_column_name=>'DATOS_TFP'
,p_display_order=>19
,p_column_identifier=>'Y'
,p_column_label=>'Detalle Forma de Pago'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DATOS_TFP'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(248499781345962234)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1276401216919693'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'TFP_DESCRIPCION:MCD_VALOR_MOVIMIENTO:DATOS_TFP'
,p_sum_columns_on_break=>'MCD_VALOR_MOVIMIENTO'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(262949061387258373)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:22:&SESSION.::&DEBUG.:RP,::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(76515959591778011)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_button_name=>'REIMPRIMIR_MOVIMIENTO_CAJA'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimprimir'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:fn_unlock(html_PopUp(''../../reports/rwservlet?module=PAGO_CUOTA.rdf&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&pagesize=10 x 10&pn_emp_id=&F_EMP_ID.&pn_mca_id=&P22_MCA_ID.'', 800, 800,75));'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(38275596621315386849)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_button_name=>'REIMPRIMIR_MOVIMIENTO_CAJA_1'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Reimprimir'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(136679055892978732)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=212.:27:&SESSION.:GENERAL:&DEBUG.::'
,p_button_condition=>'GENERAL'
,p_button_condition_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(38275596654465386850)
,p_branch_name=>'Go To Page 22'
,p_branch_action=>'f?p=&APP_ID.:22:&SESSION.:reimprimir:&DEBUG.:RP:P22_MCA_ID:&P22_MCA_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(38275596621315386849)
,p_branch_sequence=>5
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(262949380532258378)
,p_branch_action=>'f?p=&FLOW_ID.:19:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(262949061387258373)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(77028064771671323)
,p_name=>'P22_OPA_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(248501455707989674)
,p_name=>'P22_MCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_prompt=>'Mca Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(262945466881231615)
,p_name=>'P22_NOMBREMOVIMIENTO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_prompt=>'Nombremovimiento'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(262945971037232799)
,p_name=>'P22_NOMBRE_CLIENTE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(49644564598429879528)
,p_name=>'P22_PAGINA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(248497782076961955)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(77028160746679585)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_ORD_PAGO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'asdm_p.pq_tes_ordenes_pago.pr_imprimir_opa(',
'pn_opa_id =>:P22_OPA_ID,',
'pn_emp_id=>:f_emp_id);',
'',
':P22_OPA_ID:=NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P22_OPA_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>44775009476914659
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(38275596815390386851)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprmir'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
' pq_ven_movimientos_caja.pr_imprimir_mov_caja(pn_emp_id => :f_emp_id,',
'                                                     pn_mca_id =>:P22_MCA_ID,',
'                                                     pv_error  => :P0_ERROR);   ',
'',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'reimprimir'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>38243343664120621925
);
wwv_flow_imp.component_end;
end;
/
