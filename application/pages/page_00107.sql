prompt --application/pages/page_00107
begin
--   Manifest
--     PAGE: 00107
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>107
,p_tab_set=>'Menu'
,p_name=>'Report 1'
,p_step_title=>'Report 1'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(623994372257502819)
,p_name=>'Report 1'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
' "A",',
' "B",',
' "C",',
' "D"',
'from #OWNER#.ANTICIPOS_JA'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Download'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(623995279751502872)
,p_query_column_id=>1
,p_column_alias=>'A'
,p_column_display_sequence=>1
,p_column_heading=>'A'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(623995682036502880)
,p_query_column_id=>2
,p_column_alias=>'B'
,p_column_display_sequence=>2
,p_column_heading=>'B'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(623995763237502880)
,p_query_column_id=>3
,p_column_alias=>'C'
,p_column_display_sequence=>3
,p_column_heading=>'C'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(623995862889502880)
,p_query_column_id=>4
,p_column_alias=>'D'
,p_column_display_sequence=>4
,p_column_heading=>'D'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(647262155477672241)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(623994372257502819)
,p_button_name=>'TRASPASO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Traspaso'
,p_button_position=>'TOP'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(647220180972670123)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_traspaso'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  CURSOR anticipos IS',
'  ',
'    SELECT * FROM anticipos_ja;',
'',
'  lv_error VARCHAR2(3000);',
'',
'BEGIN',
'',
'  FOR reg IN anticipos LOOP',
'  ',
'    asdm_p.pq_ven_movimientos_caja.pr_traspaso_anticipos(pn_emp_id         => 1,',
'                                                         pn_cli_id         => reg.b,',
'                                                         pn_valor          => reg.d,',
'                                                         pn_uge_id_origen  => 232,',
'                                                         pn_uge_id_destino => 236,',
'                                                         pn_usu_id         => 770,',
'                                                         pn_pca_id         => 47621,',
'                                                         pv_observacion    => ''Traspaso por cierre de Agencia desde PORTOVIEJO ALAJUELA hacia PORTOVIEJO CORDOVA'',',
'                                                         pv_error          => lv_error);',
'  ',
'  END LOOP;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(647262155477672241)
,p_process_success_message=>'traspaso realizado'
,p_internal_uid=>614967029702905197
);
wwv_flow_imp.component_end;
end;
/
