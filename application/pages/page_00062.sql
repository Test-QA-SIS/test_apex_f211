prompt --application/pages/page_00062
begin
--   Manifest
--     PAGE: 00062
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>62
,p_name=>'Deposito Transitorio'
,p_step_title=>'Deposito Transitorio'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240129101113'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(94890858619172944)
,p_plug_name=>'DEPOSITO TRANSITORIO'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(94894259837172975)
,p_name=>'PAGOS TARJETA DE CREDITO'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT seq_id,',
'       c001       mcd_id,',
'       c002       tfp_id,',
'       c003       forma_pago,',
'       c004       ede_id,',
'       c005       entidad,',
'       C007       valor,',
'       c006       tarjeta,',
'       c008       autorizacion,',
'       c009       Lote,',
'       c011       titular_tarjeta',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_DEP_VOUCHER''*/',
'select * ',
'from v_ven_depositos_tarjetas dta',
'where dta.emp_id = :f_emp_id',
'and dta.uge_id = :f_uge_id',
'and dta.pca_id = :f_pca_id'))
,p_display_when_condition=>'p62_tipo'
,p_display_when_cond2=>'2'
,p_display_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(94894259837172975)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136575457382061430)
,p_query_column_id=>1
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136575557377061438)
,p_query_column_id=>2
,p_column_alias=>'PCA_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Pca Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136575667005061438)
,p_query_column_id=>3
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94894565029172980)
,p_query_column_id=>4
,p_column_alias=>'MCD_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Mcd Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94894653372172980)
,p_query_column_id=>5
,p_column_alias=>'TFP_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Tfp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94894760750172980)
,p_query_column_id=>6
,p_column_alias=>'FORMA_PAGO'
,p_column_display_sequence=>4
,p_column_heading=>'Forma Pago'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94894856110172981)
,p_query_column_id=>7
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ede Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94894983825172981)
,p_query_column_id=>8
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>5
,p_column_heading=>'Entidad'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136575779635061438)
,p_query_column_id=>9
,p_column_alias=>'MCD_NRO_TARJETA'
,p_column_display_sequence=>9
,p_column_heading=>'Mcd Nro Tarjeta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136575853298061438)
,p_query_column_id=>10
,p_column_alias=>'MCD_VALOR_MOVIMIENTO'
,p_column_display_sequence=>10
,p_column_heading=>'Mcd Valor Movimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136575973249061438)
,p_query_column_id=>11
,p_column_alias=>'MCD_NRO_AUT_REF'
,p_column_display_sequence=>11
,p_column_heading=>'Mcd Nro Aut Ref'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136576059751061438)
,p_query_column_id=>12
,p_column_alias=>'MCD_NRO_LOTE'
,p_column_display_sequence=>12
,p_column_heading=>'Mcd Nro Lote'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136576156830061438)
,p_query_column_id=>13
,p_column_alias=>'MCD_ID_REF'
,p_column_display_sequence=>13
,p_column_heading=>'Mcd Id Ref'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136576262649061438)
,p_query_column_id=>14
,p_column_alias=>'MCD_TITULAR_TARJETA'
,p_column_display_sequence=>14
,p_column_heading=>'Mcd Titular Tarjeta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(94895569137172989)
,p_name=>'CHEQUES'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT seq_id,',
'       c001       mcd_id,',
'       c002       tfp_id,',
'       c003       forma_pago,',
'       c004       ede_id,',
'       c005       entidad,',
'       c006       cre_id,',
'       c007       num_cheque,',
'       c008       num_cuenta,',
'       to_number(c009)       valor,',
'       c010       fecha,',
'       c011       titula_cuenta,',
'       c013       Observacion,',
'       c012       transaccion',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_DEP_CHEQUES''*/',
'select *',
'from v_ven_depositos_cheques dch',
'where dch.emp_id = :f_emp_id',
'and dch.uge_id = :f_uge_id',
'and dch.pca_id = :f_pca_id'))
,p_display_when_condition=>':p62_tipo = 1 and :p62_tipo_id is null'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(94895569137172989)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572277899048437)
,p_query_column_id=>1
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572352645048447)
,p_query_column_id=>2
,p_column_alias=>'PCA_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Pca Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572465285048448)
,p_query_column_id=>3
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>9
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94895965187173003)
,p_query_column_id=>4
,p_column_alias=>'MCD_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Mcd Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94896073004173003)
,p_query_column_id=>5
,p_column_alias=>'TFP_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Tfp Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94896178999173003)
,p_query_column_id=>6
,p_column_alias=>'FORMA_PAGO'
,p_column_display_sequence=>4
,p_column_heading=>'Forma Pago'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94896264491173003)
,p_query_column_id=>7
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Ede Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94896382676173003)
,p_query_column_id=>8
,p_column_alias=>'ENTIDAD'
,p_column_display_sequence=>6
,p_column_heading=>'Entidad'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(94896463001173003)
,p_query_column_id=>9
,p_column_alias=>'CRE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cre Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572554843048448)
,p_query_column_id=>10
,p_column_alias=>'CRE_NRO_CHEQUE'
,p_column_display_sequence=>10
,p_column_heading=>'Cre Nro Cheque'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572655999048448)
,p_query_column_id=>11
,p_column_alias=>'CRE_NRO_CUENTA'
,p_column_display_sequence=>11
,p_column_heading=>'Cre Nro Cuenta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572773501048448)
,p_query_column_id=>12
,p_column_alias=>'MCD_VALOR_MOVIMIENTO'
,p_column_display_sequence=>12
,p_column_heading=>'Mcd Valor Movimiento'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572873875048448)
,p_query_column_id=>13
,p_column_alias=>'CRE_FECHA_DEPOSITO'
,p_column_display_sequence=>13
,p_column_heading=>'Cre Fecha Deposito'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136572968467048448)
,p_query_column_id=>14
,p_column_alias=>'CRE_TITULAR_CUENTA'
,p_column_display_sequence=>14
,p_column_heading=>'Cre Titular Cuenta'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136573075992048466)
,p_query_column_id=>15
,p_column_alias=>'TTR_DESCRIPCION'
,p_column_display_sequence=>15
,p_column_heading=>'Ttr Descripcion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136573181908048466)
,p_query_column_id=>16
,p_column_alias=>'MCA_OBSERVACION'
,p_column_display_sequence=>16
,p_column_heading=>'Mca Observacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(95154853748457947)
,p_name=>'col_deposito_transitorio'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'select * from apex_collections where collection_name like ''COL_DEP_VOUCHER'''
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155169318457986)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155278256457991)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155379548457991)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155472133457991)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155553271457991)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155677333457991)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155752105458003)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155870769458003)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95155962439458003)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156055979458003)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156172454458003)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156270588458003)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156370630458003)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156471256458003)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156552861458003)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156656453458003)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156753117458003)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156877585458003)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95156978563458003)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157055550458004)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157164059458004)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157258934458004)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157375741458004)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157481450458004)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157575395458004)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157669165458004)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157751475458004)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157882076458004)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95157955412458004)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158064833458004)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158181338458004)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158278958458005)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158360697458005)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158453692458005)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158555901458005)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158678976458005)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158766002458005)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158882756458005)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95158965771458005)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159077042458006)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159159395458006)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159264883458006)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159360167458006)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159458596458006)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159561691458006)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159681851458006)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159751851458006)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159858221458006)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95159973254458006)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160081528458006)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160160509458006)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160252186458006)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160365911458006)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160465878458006)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160551575458006)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160668904458006)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160755824458007)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160880614458007)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95160978386458007)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161065732458007)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161180180458007)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161254652458007)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161365491458007)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161454401458007)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161580751458007)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(95161678156458007)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(136478364515928278)
,p_name=>'REPORTE CHEQUE'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select tfp.tfp_descripcion,',
'       cre.ede_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(7, cre.ede_id) entidad_financiera,',
'       cre.cre_nro_cheque,',
'       cre.cre_nro_cuenta,',
'       cre.cre_nro_pin,',
'       cre.cre_valor,',
'       cre.cre_titular_cuenta',
'  from asdm_movimientos_caja_detalle mcd,',
'       asdm_cheques_recibidos        cre,',
'       asdm_tipos_formas_pago        tfp',
' where cre.cre_id = mcd.cre_id',
'   and cre.emp_id = mcd.emp_id',
'   and tfp.tfp_id = mcd.tfp_id',
'   and tfp.emp_id = mcd.emp_id',
'   and mcd.mca_id = :p62_mca_id'))
,p_display_when_condition=>':request = ''graba'' and :p62_tipo_id = 1'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136478676838928312)
,p_query_column_id=>1
,p_column_alias=>'TFP_DESCRIPCION'
,p_column_display_sequence=>1
,p_column_heading=>'TFP_DESCRIPCION'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136478782944928342)
,p_query_column_id=>2
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'EDE_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136478860712928342)
,p_query_column_id=>3
,p_column_alias=>'ENTIDAD_FINANCIERA'
,p_column_display_sequence=>3
,p_column_heading=>'ENTIDAD_FINANCIERA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136478967500928342)
,p_query_column_id=>4
,p_column_alias=>'CRE_NRO_CHEQUE'
,p_column_display_sequence=>4
,p_column_heading=>'CRE_NRO_CHEQUE'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136479073060928342)
,p_query_column_id=>5
,p_column_alias=>'CRE_NRO_CUENTA'
,p_column_display_sequence=>5
,p_column_heading=>'CRE_NRO_CUENTA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136479154039928343)
,p_query_column_id=>6
,p_column_alias=>'CRE_NRO_PIN'
,p_column_display_sequence=>6
,p_column_heading=>'CRE_NRO_PIN'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136479259688928343)
,p_query_column_id=>7
,p_column_alias=>'CRE_VALOR'
,p_column_display_sequence=>7
,p_column_heading=>'CRE_VALOR'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136479373077928343)
,p_query_column_id=>8
,p_column_alias=>'CRE_TITULAR_CUENTA'
,p_column_display_sequence=>8
,p_column_heading=>'CRE_TITULAR_CUENTA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(136500866686985686)
,p_name=>'REPORTE TARJETAS'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select tfp.tfp_descripcion,',
'       mcd.ede_id,',
'       pq_lv_kdda.fn_obtiene_valor_lov(7, mcd.ede_id) entidad_financiera,',
'       mcd.mcd_nro_tarjeta,',
'       mcd.mcd_nro_aut_ref,',
'       mcd.mcd_titular_tarjeta',
'  from asdm_movimientos_caja_detalle mcd,',
'       asdm_tipos_formas_pago        tfp',
' where tfp.tfp_id = mcd.tfp_id',
'   and tfp.emp_id = mcd.emp_id',
'   and mcd.mca_id = :p62_mca_id'))
,p_display_when_condition=>':request = ''graba'' and :p62_tipo_id = 2'
,p_display_when_cond2=>'PLSQL'
,p_display_condition_type=>'EXPRESSION'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136501151353985696)
,p_query_column_id=>1
,p_column_alias=>'TFP_DESCRIPCION'
,p_column_display_sequence=>1
,p_column_heading=>'TFP_DESCRIPCION'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136501253231985710)
,p_query_column_id=>2
,p_column_alias=>'EDE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'EDE_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136501351708985710)
,p_query_column_id=>3
,p_column_alias=>'ENTIDAD_FINANCIERA'
,p_column_display_sequence=>3
,p_column_heading=>'ENTIDAD_FINANCIERA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136501470192985710)
,p_query_column_id=>4
,p_column_alias=>'MCD_NRO_TARJETA'
,p_column_display_sequence=>4
,p_column_heading=>'MCD_NRO_TARJETA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136501556318985710)
,p_query_column_id=>5
,p_column_alias=>'MCD_NRO_AUT_REF'
,p_column_display_sequence=>5
,p_column_heading=>'MCD_NRO_AUT_REF'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136501671831985710)
,p_query_column_id=>6
,p_column_alias=>'MCD_TITULAR_TARJETA'
,p_column_display_sequence=>6
,p_column_heading=>'MCD_TITULAR_TARJETA'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(94891054058172951)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'graba'
,p_button_condition_type=>'REQUEST_NOT_EQUAL_CONDITION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(94891256530172957)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_button_name=>'CANCELAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(94898655452173147)
,p_branch_action=>'f?p=&APP_ID.:62:&SESSION.:graba:&DEBUG.::P62_TIPO_ID:&P62_TIPO.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(94891054058172951)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(94898854844173163)
,p_branch_action=>'f?p=&FLOW_ID.:39:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(94891256530172957)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(94899065033173167)
,p_branch_action=>'f?p=&APP_ID.:62:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_comment=>'Created 13-NOV-2009 01:40 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94891452923172957)
,p_name=>'P62_TRX_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94891683490172966)
,p_name=>'P62_TTR_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Ttr Id'
,p_source=>'pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_egr_caj_dep_tran'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94891858937172968)
,p_name=>'P62_MCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Mca Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94892075502172968)
,p_name=>'P62_MCA_OBSERVACION'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Observacion'
,p_source=>'select ''DEPOSITO TRANSITORIO E/CH ''||to_char(sysdate,''dd/mm/yyyy hh24:mi:ss'') FROM DUAL'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>2
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94892261962172969)
,p_name=>'P62_EFECTIVO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Efectivo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p62_tipo = 1 AND :P62_MCA_ID IS NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94892483284172969)
,p_name=>'P62_CHEQUES'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Cheques'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p62_tipo = 1 AND :P62_MCA_ID IS NULL'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94892653093172970)
,p_name=>'P62_MCA_TOTAL'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select decode(:p62_tipo, 1,(:P62_EFECTIVO+:P62_CHEQUES),2,:P62_tarjeta,0)',
'from dual'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94892857933172970)
,p_name=>'P62_MCA_OBSERVACION1'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Observacion'
,p_source=>'select mca_observacion from asdm_movimientos_caja where mca_id=:p62_mca_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94893079268172970)
,p_name=>'P62_EFECTIVO1'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Efectivo y Cheques'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca_total - nvl(total.suma,0)',
'FROM   asdm_movimientos_caja,',
'       (SELECT SUM(c009) suma',
'        FROM   apex_collections',
'        WHERE  collection_name = ''COL_DEP_TRANSITORIOS''',
'               AND c012 = ''SI'') total',
'WHERE  mca_id =:p62_mca_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94893254787172970)
,p_name=>'P62_OTROS1'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Otros'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(c009),0)',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_DEP_TRANSITORIOS''',
'AND C012=''SI'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94893475105172971)
,p_name=>'P62_MCA_TOTAL1'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total'
,p_source=>'select to_char(mca_total,99999999999.99) from asdm_movimientos_caja where mca_id=:p62_mca_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_EQUALS_CONDITION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94893674772172971)
,p_name=>'P62_ENTIDAD_DESTINO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Entidad Destino'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENT_DESTINO_X_UGE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Seleccion Entidad'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94893857770172972)
,p_name=>'P62_EDE_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Cuenta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion d,',
'       c.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  A.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id = :f_uge_id',
'       AND d.ede_id = :p62_entidad_destino',
'ORDER  BY a.ucb_prioridad'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'p62_entidad_destino'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94894054107172972)
,p_name=>'P62_TIPO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Tipo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94897173455173004)
,p_name=>'P62_SEQ_ID'
,p_item_sequence=>100
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(94897366881173004)
,p_name=>'P62_ESTADO'
,p_item_sequence=>110
,p_item_display_point=>'LEGACY_ORPHAN_COMPONENTS'
,p_prompt=>'Estado'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(95058974837761081)
,p_name=>'P62_TARJETA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Voucher'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':p62_tipo = 2'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(136467979397856833)
,p_name=>'P62_TIPO_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(94890858619172944)
,p_prompt=>'Tipo Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(94897678650173032)
,p_validation_name=>'P62_ENTIDAD_DESTINO'
,p_validation_sequence=>10
,p_validation=>'P62_ENTIDAD_DESTINO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione la Entidad Destino.'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(94891054058172951)
,p_associated_item=>wwv_flow_imp.id(94893674772172971)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(95193574038078668)
,p_validation_name=>'P62_MCA_OBSERVACION'
,p_validation_sequence=>20
,p_validation=>'P62_MCA_OBSERVACION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Debe ingresar la Observacion'
,p_when_button_pressed=>wwv_flow_imp.id(94891054058172951)
,p_associated_item=>wwv_flow_imp.id(94892075502172968)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(25526662374951600997)
,p_name=>'ac_click'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexbeforepagesubmit'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(25526662657621601000)
,p_event_id=>wwv_flow_imp.id(25526662374951600997)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'$(''a[href="javascript:apex.submit(\''''+this.data+''\'');"]'').attr("href", "JavaScript:void(0);");'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(94898168526173120)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_deposito_transitorio'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  ln_efectivo asdm_movimientos_caja_detalle.mcd_valor_movimiento%TYPE;',
'  ln_cheque asdm_movimientos_caja_detalle.mcd_valor_movimiento%TYPE;',
'  ln_tarjeta asdm_movimientos_caja_detalle.mcd_valor_movimiento%TYPE;',
'  ln_total asdm_movimientos_caja_detalle.mcd_valor_movimiento%TYPE;',
'BEGIN',
':p62_seq_id:=null;',
':p62_estado:=null;',
'',
'',
'pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id => :f_emp_id,',
'                        pn_pca_id => :f_pca_id,',
'                        pn_valor_efectivo => ln_efectivo,',
'                        pn_valor_cheque => ln_cheque,',
'                        pn_valor_tarjeta => ln_tarjeta,',
'                        pv_error  => :p0_error);',
'',
'ln_total := ln_Efectivo + ln_cheque;',
'',
'if ln_total > 0 then',
'  pq_ven_movimientos_caja.pr_deposito_transitorio(pn_emp_id          =>:f_emp_id,',
'                                                  pn_uge_id          =>:f_uge_id,',
'                                                  pn_user_id         =>:f_user_id,',
'                                                  pn_ttr_id          =>:p62_ttr_id,',
'                                                  pn_trx_id          =>:p62_trx_id,',
'                                                  pn_uge_id_gasto    =>:f_uge_id_gasto,',
'                                                  pn_mca_id          =>:p62_mca_id,',
'                                                  pn_pca_id          =>:f_pca_id,',
'                                                  pv_mca_observacion =>:p62_mca_observacion,',
'                                                  pn_mca_total       =>ln_total,',
'                                                  pn_EDE_id          =>:P62_EDE_ID,',
'                                                  pn_tipo            =>:p62_tipo,',
'                                                  pv_error           =>:p0_error);',
'end if;',
'ln_total := 0;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(94891054058172951)
,p_internal_uid=>62645017256408194
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(94897756041173049)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_valor number;',
'ln_cheque number;',
'ln_tarjeta number;',
'begin',
'pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id => :f_emp_id,',
'                        pn_pca_id => :f_pca_id,',
'                        pn_valor_efectivo => :p62_efectivo,',
'                        pn_valor_cheque => :p62_cheques,',
'                        pn_valor_tarjeta => :p62_tarjeta,',
'                        pv_error  => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request not in (''cambia'',''graba'') or :p62_seq_id is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>62644604771408123
,p_process_comment=>':request not in (''cambia'',''graba'') or :p62_seq_id is null'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(94897968515173120)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_modifica_reg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'if :p62_estado=''SI'' then',
' apex_collection.update_member_attribute(p_collection_name =>''COL_DEP_TRANSITORIOS'',',
'                                          p_seq             =>:P62_SEQ_ID,',
'                                          p_attr_number     =>12,',
'                                          p_attr_value      =>''NO'');',
'else',
'apex_collection.update_member_attribute(p_collection_name =>''COL_DEP_TRANSITORIOS'',',
'                                          p_seq             =>:P62_SEQ_ID,',
'                                          p_attr_number     =>12,',
'                                          p_attr_value      =>''SI'');',
'end if;',
':p62_seq_id:=null;',
':p62_estado:=null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cambia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>62644817245408194
);
wwv_flow_imp.component_end;
end;
/
