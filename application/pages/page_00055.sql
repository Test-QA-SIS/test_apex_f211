prompt --application/pages/page_00055
begin
--   Manifest
--     PAGE: 00055
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>55
,p_name=>'Report Cheques Ingresados'
,p_step_title=>'Report Cheques Ingresados'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(77521562886796294)
,p_plug_name=>'Reporte Cheques Ingresados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'SELECT cre.cre_id,',
'       cre.cli_id,',
'       cli.per_primer_apellido || '' '' || cli.per_segundo_apellido || '' '' || cli.per_primer_nombre || '' '' || cli.per_segundo_nombre cliente,',
'       cre.ede_id,',
'       ede.ede_descripcion,',
'       cre.cre_id_reemplazado,',
'       cre.rca_id,',
'       cre.cre_nro_cheque,',
'       cre.cre_valor,',
'       cre.cre_tipo,',
'       pq_lv_kdda.fn_obtiene_valor_lov(183,cre.cre_tipo) tipocheque,',
'       cre.cre_nro_cuenta,',
'       cre.cre_nro_pin,',
'       cre.cre_titular_cuenta,',
'       cre.cre_observaciones, ',
'       cre.cre_fecha_deposito',
'FROM   asdm_empresas emp,',
'       asdm_cheques_recibidos cre,',
'       asdm_entidades_destinos ede,',
'       v_asdm_clientes_personas cli       ',
'WHERE  emp.emp_id = cre.emp_id',
'       AND ede.ede_id = cre.ede_id       ',
'       AND cli.cli_id = cre.cli_id',
'       AND emp.emp_id = :f_emp_id',
'--       AND cre.ech_id = pq_constantes.fn_retorna_constante(NULL,''cn_ech_id_ingresado'')',
'ORDER  BY cre_id'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(77521766483796333)
,p_name=>'Reporte Cheques Anulados'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more then 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No existen datos'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MURGILES'
,p_internal_uid=>45268615214031407
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77521866762796428)
,p_db_column_name=>'CRE_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Id.Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522072946796487)
,p_db_column_name=>'CLI_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Id.Cliente '
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522158909796487)
,p_db_column_name=>'CLIENTE'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522253978796492)
,p_db_column_name=>'EDE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Id.Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522369429796492)
,p_db_column_name=>'EDE_DESCRIPCION'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'EDE_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522469582796493)
,p_db_column_name=>'CRE_ID_REEMPLAZADO'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Reemplaza cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_ID_REEMPLAZADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522569198796493)
,p_db_column_name=>'RCA_ID'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Rca Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
,p_column_alignment=>'RIGHT'
,p_static_id=>'RCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522655764796493)
,p_db_column_name=>'CRE_NRO_CHEQUE'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Nro. Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_NRO_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522756279796493)
,p_db_column_name=>'CRE_VALOR'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Valor Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522869408796493)
,p_db_column_name=>'CRE_TIPO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Tipo Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_static_id=>'CRE_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77522956852796493)
,p_db_column_name=>'TIPOCHEQUE'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
,p_static_id=>'TIPOCHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77523062805796494)
,p_db_column_name=>'CRE_NRO_CUENTA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Nro. Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_NRO_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77523163843796494)
,p_db_column_name=>'CRE_NRO_PIN'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Nro. Pin'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'CRE_NRO_PIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77523277037796494)
,p_db_column_name=>'CRE_TITULAR_CUENTA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Titular Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_TITULAR_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77523356061796494)
,p_db_column_name=>'CRE_OBSERVACIONES'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Observaciones'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'CRE_OBSERVACIONES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(77523454913796494)
,p_db_column_name=>'CRE_FECHA_DEPOSITO'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>unistr('Fecha Dep\00BF\00BFsito')
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
,p_static_id=>'CRE_FECHA_DEPOSITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(77523575084796495)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'452705'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CRE_ID:CLI_ID:CLIENTE:EDE_ID:EDE_DESCRIPCION:CRE_NRO_CHEQUE:CRE_VALOR:TIPOCHEQUE:CRE_NRO_CUENTA:CRE_NRO_PIN:CRE_TITULAR_CUENTA:CRE_OBSERVACIONES:CRE_FECHA_DEPOSITO'
,p_sort_column_1=>'CRE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp.component_end;
end;
/
