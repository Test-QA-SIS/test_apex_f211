prompt --application/pages/page_00041
begin
--   Manifest
--     PAGE: 00041
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>41
,p_name=>'Reporte Listado de Facturas'
,p_step_title=>'Reporte Listado de Facturas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(55522259608392227)
,p_plug_name=>'Listado Facturas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>130
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT com.emp_id                   emp_id,',
'       com.com_id                   com_id,',
'       vcli.per_nro_identificacion,',
'       vcli.per_tipo_identificacion,',
'       --Inicio R3603-03 RALVARADO/FPALOMEQUE 14/03/2022 ''Subquery para advertir de necesidad de formulario licitud de fondos''',
'       (SELECT CASE',
'                 WHEN nvl(lfc_estado, ''-'') IN (''I'') THEN',
'                  ''<p style="color: #000000; background-color: #ffdfd0; font-weight:bold">&#128721; '' ||',
'                  ''Formulario Licitud de Fondos y/o Conozca su Cliente REQUERIDO -> '' ||',
'                  ''<a href="'' ||',
'                  apex_util.prepare_url(p_url           => ''f?p='' || 211 || '':'' || 210 ||',
'                                                           '':&SESSION.:for_lic_fac:NO:RP:P210_LFC_ID_FAC:'' ||',
'                                                           lfc_id,',
'                                        p_checksum_type => ''SESSION'') ||',
'                  ''" class="lock_ui_row"> Aqui </a></p>''',
'               END formulario',
'          FROM (SELECT MAX(lf.lfc_id) lfc_id, MAX(lf.lfc_estado) lfc_estado',
'                  FROM ven_comprobantes       co,',
'                       car_cuentas_por_cobrar cx,',
'                       car_movimientos_cab    cm,',
'                       car_licit_fondos_cab   lf',
'                 WHERE co.com_id = cx.com_id',
'                   AND cx.cxc_id = cm.cxc_id',
'                   AND ((cm.mca_id_movcaja = lf.mca_com_id AND',
'                       lf.lfc_tipo = ''LDF'') OR',
'                       (lf.lfc_tipo = ''CSC'' AND co.com_id = lf.mca_com_id))',
'                   AND co.com_id = com.com_id',
'                   AND lf.lfc_estado = ''I''',
'                 FETCH FIRST ROW ONLY)) formularios,',
'       --Fin R3603-03 RALVARADO/FPALOMEQUE 14/03/2022 ''Subquery para advertir de necesidad de formulario licitud de fondos''',
'       com.uge_num_sri || ''-'' || com.pue_num_sri || ''-'' ||',
'       lpad(com.com_numero, 7, 0) folio,',
'       com.com_fecha fecha,',
'       com.cli_id cli_id,',
'       nvl(vcli.per_razon_social,',
'           vcli.per_primer_nombre || '' '' || vcli.per_segundo_nombre || '' '' ||',
'           vcli.per_primer_apellido || '' '' || vcli.per_segundo_apellido) cliente,',
'       com.com_tipo tipo,',
'       com.pol_id pol_id,',
'       com.com_plazo plazo,',
'       com.ord_id ord_id,',
'       pq_ven_retorna_sec_id.fn_ord_sec_id(com.ord_id) ord_sec_id,',
'       com.age_id_agente age_id_agente,',
'       age.age_nombre_comercial agencia,',
'       com.com_sec_id com_sec_id,',
'       mc.mca_numero_comprobante contable,',
'       (SELECT t.tve_descripcion',
'          FROM ven_terminos_venta t',
'         WHERE t.tve_id =',
'               (valor_var_cab(pn_com_id => com.com_id,',
'                              pn_var_id => :p41_cn_var_id_tve))) termino_venta,',
'       com.sco_autorizacion autorizacion,',
'       com.com_estado_registro estado,',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_subtotal) subotal, --',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_dscto_fact) total_desc, --',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_subtotal_con_iva) subtotal_iva, --',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_subtotal_sin_iva) subtotal_iva_0, --',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_iva_calculo) iva, --',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_fin_total_factura) total_fac, --',
'       valor_var_cab(pn_com_id => com.com_id,',
'                     pn_var_id => :p41_cn_var_id_int_diferido) int_finan, --',
'       com.age_id_agencia,',
'       com.com_id_ref com_id_ref,',
'       com_aplica_negociacion neg,',
'       com.sco_id,',
'       ----------------JALVAREZ 12MAY2022 SE AUMENTA PARA EMITIR EL SEGURO',
'       CASE',
'         WHEN (SELECT COUNT(1)',
'                 FROM asdm_e.ven_ordenes_det b',
'                WHERE b.ord_id = com.ord_id',
'                  AND b.ode_estado_registro = 0',
'                  AND EXISTS',
'                (SELECT NULL',
'                         FROM inv_items_categorias h',
'                        WHERE h.ite_sku_id = b.ite_sku_id',
'                          AND h.cat_id IN',
'                              (SELECT c.cat_id',
'                                 FROM inv_categorias c',
'                                WHERE c.emp_id = :f_emp_id',
'                                  AND c.cat_estado_registro = 0',
'                               CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'                                START WITH c.cat_id = :p41_cat_id_celulares))) > 0 THEN',
'          ''EMITIR SEGURO''',
'         ELSE',
'          NULL',
'       END emitir_seguro,',
'       (SELECT CASE',
'                 WHEN cc.forma_credito = ''E'' THEN',
'                  ''Electronico''',
'                 ELSE',
'                  ''Manual''',
'               END',
'          FROM asdm_e.car_cuentas_por_cobrar cc',
'         WHERE cc.cxc_id IN',
'               (SELECT MAX(cxc_id)',
'                  FROM asdm_e.car_cuentas_por_cobrar tt',
'                 WHERE tt.com_id = com.com_id',
'                   AND tt.cxc_estado_registro = ''0'')) forma_credito',
'',
'   FROM ven_comprobantes         com,',
'       asdm_agencias            age,',
'       asdm_agentes             agt,',
'       asdm_personas            vcli,',
'       asdm_clientes            c,',
'       asdm_transacciones       t,',
'       con_movimientos_cabecera mc',
' WHERE age.age_id = com.age_id_agencia',
'   AND agt.age_id = com.age_id_agente',
'   AND vcli.per_id = c.per_id',
'   AND c.cli_id = com.cli_id',
'   AND vcli.emp_id = com.emp_id',
'   AND t.trx_id = com.trx_id',
'   AND mc.mca_id(+) = t.mca_id',
'   AND trunc(com.com_fecha) >=',
'       nvl(to_date(:p41_fechadesde, ''DD/MM/YYYY''), trunc(SYSDATE))',
'   AND trunc(com.com_fecha) <=',
'       nvl(to_date(:p41_fechahasta, ''DD/MM/YYYY''), trunc(SYSDATE))',
'   AND com.emp_id = :f_emp_id',
'   and com.uge_id = age.uge_id',
'   AND age.uge_id = :f_uge_id',
' ORDER BY com.com_id DESC',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(55522370729392227)
,p_name=>'Listado Facturas'
,p_max_row_count=>'50'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y_OF_Z'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>true
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_csv_output_separator=>';'
,p_detail_link=>'f?p=&APP_ID.:52:&SESSION.::&DEBUG.::P52_COM_ID,P52_AGENTE,P52_VALIDA_CONS_CARTERA,P52_PAG_ID:#COM_ID#,#AGE_ID_AGENTE#,0,41'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_detail_link_attr=>'class="lock_ui_row"'
,p_owner=>'MURGILES'
,p_internal_uid=>23269219459627301
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(144618858148265856)
,p_db_column_name=>'TERMINO_VENTA'
,p_display_order=>31
,p_column_identifier=>'BO'
,p_column_label=>'Termino Venta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TERMINO_VENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39205456939482129)
,p_db_column_name=>'FOLIO'
,p_display_order=>35
,p_column_identifier=>'BS'
,p_column_label=>'Folio'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39205765192482129)
,p_db_column_name=>'CLIENTE'
,p_display_order=>38
,p_column_identifier=>'BV'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39205863622482130)
,p_db_column_name=>'TIPO'
,p_display_order=>39
,p_column_identifier=>'BW'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39206561751482131)
,p_db_column_name=>'AGENCIA'
,p_display_order=>46
,p_column_identifier=>'CD'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(39206772375482131)
,p_db_column_name=>'CONTABLE'
,p_display_order=>48
,p_column_identifier=>'CF'
,p_column_label=>'Contable'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CONTABLE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349413819148248659)
,p_db_column_name=>'EMP_ID'
,p_display_order=>58
,p_column_identifier=>'CK'
,p_column_label=>'Emp id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349413929292248660)
,p_db_column_name=>'COM_ID'
,p_display_order=>68
,p_column_identifier=>'CL'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414021204248661)
,p_db_column_name=>'FECHA'
,p_display_order=>78
,p_column_identifier=>'CM'
,p_column_label=>'Fecha'
,p_column_type=>'DATE'
,p_column_alignment=>'CENTER'
,p_tz_dependent=>'N'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414096602248662)
,p_db_column_name=>'CLI_ID'
,p_display_order=>88
,p_column_identifier=>'CN'
,p_column_label=>'Cli id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414225850248663)
,p_db_column_name=>'POL_ID'
,p_display_order=>98
,p_column_identifier=>'CO'
,p_column_label=>'Pol id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414337004248664)
,p_db_column_name=>'PLAZO'
,p_display_order=>108
,p_column_identifier=>'CP'
,p_column_label=>'Plazo'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414404508248665)
,p_db_column_name=>'ORD_ID'
,p_display_order=>118
,p_column_identifier=>'CQ'
,p_column_label=>'Ord id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414543493248666)
,p_db_column_name=>'ORD_SEC_ID'
,p_display_order=>128
,p_column_identifier=>'CR'
,p_column_label=>'Ord sec id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414620466248667)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>138
,p_column_identifier=>'CS'
,p_column_label=>'Age id agente'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414687571248668)
,p_db_column_name=>'COM_SEC_ID'
,p_display_order=>148
,p_column_identifier=>'CT'
,p_column_label=>'Com sec id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414838995248669)
,p_db_column_name=>'AUTORIZACION'
,p_display_order=>158
,p_column_identifier=>'CU'
,p_column_label=>'Autorizacion'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349414865407248670)
,p_db_column_name=>'ESTADO'
,p_display_order=>168
,p_column_identifier=>'CV'
,p_column_label=>'Estado'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349415001193248671)
,p_db_column_name=>'SUBOTAL'
,p_display_order=>178
,p_column_identifier=>'CW'
,p_column_label=>'Subotal'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349415143094248672)
,p_db_column_name=>'TOTAL_DESC'
,p_display_order=>188
,p_column_identifier=>'CX'
,p_column_label=>'Total desc'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349415224148248673)
,p_db_column_name=>'SUBTOTAL_IVA'
,p_display_order=>198
,p_column_identifier=>'CY'
,p_column_label=>'Subtotal iva'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349415268231248674)
,p_db_column_name=>'SUBTOTAL_IVA_0'
,p_display_order=>208
,p_column_identifier=>'CZ'
,p_column_label=>'Subtotal iva 0'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349415399948248675)
,p_db_column_name=>'IVA'
,p_display_order=>218
,p_column_identifier=>'DA'
,p_column_label=>'Iva'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(15349415468466248676)
,p_db_column_name=>'TOTAL_FAC'
,p_display_order=>228
,p_column_identifier=>'DB'
,p_column_label=>'Total fac'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(29486261695322611828)
,p_db_column_name=>'INT_FINAN'
,p_display_order=>248
,p_column_identifier=>'DD'
,p_column_label=>'Int finan'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(29486261813463611829)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>258
,p_column_identifier=>'DE'
,p_column_label=>'Age id agencia'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(29486261903770611830)
,p_db_column_name=>'COM_ID_REF'
,p_display_order=>268
,p_column_identifier=>'DF'
,p_column_label=>'Com id ref'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(29486261965898611831)
,p_db_column_name=>'NEG'
,p_display_order=>278
,p_column_identifier=>'DG'
,p_column_label=>'Neg'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(80178368110080574145)
,p_db_column_name=>'SCO_ID'
,p_display_order=>288
,p_column_identifier=>'DH'
,p_column_label=>'Sco id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(165053702830004994235)
,p_db_column_name=>'FORMULARIOS'
,p_display_order=>298
,p_column_identifier=>'DI'
,p_column_label=>'Formularios'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(172799805919328868743)
,p_db_column_name=>'EMITIR_SEGURO'
,p_display_order=>308
,p_column_identifier=>'DJ'
,p_column_label=>'Emitir seguro'
,p_column_link=>'f?p=242:54:&SESSION.::&DEBUG.:RP:P54_CLI_ID:#CLI_ID#'
,p_column_linktext=>'#EMITIR_SEGURO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(172799806011389868744)
,p_db_column_name=>'PER_NRO_IDENTIFICACION'
,p_display_order=>318
,p_column_identifier=>'DK'
,p_column_label=>'Per nro identificacion'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(172799806135147868745)
,p_db_column_name=>'PER_TIPO_IDENTIFICACION'
,p_display_order=>328
,p_column_identifier=>'DL'
,p_column_label=>'Per tipo identificacion'
,p_column_type=>'STRING'
,p_display_text_as=>'HIDDEN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(277421522400092358)
,p_db_column_name=>'FORMA_CREDITO'
,p_display_order=>338
,p_column_identifier=>'DM'
,p_column_label=>'Forma credito'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(55530279033395997)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292930608919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'FORMULARIOS:COM_ID:FOLIO:FECHA:CLI_ID:CLIENTE:TIPO:PLAZO:ORD_ID:ORD_SEC_ID:AGE_ID_AGENTE:COM_SEC_ID:CONTABLE:TERMINO_VENTA:AUTORIZACION:ESTADO:SUBOTAL:TOTAL_DESC:SUBTOTAL_IVA:SUBTOTAL_IVA_0:IVA:TOTAL_FAC:INT_FINAN:AGENCIA:COM_ID_REF:NEG:SCO_ID::EMITI'
||'R_SEGURO:PER_NRO_IDENTIFICACION:PER_TIPO_IDENTIFICACION:FORMA_CREDITO'
,p_sort_column_1=>'C002'
,p_sort_direction_1=>'DESC'
,p_sort_column_2=>'TO_NUMBER(COL.C002)'
,p_sort_direction_2=>'DESC'
,p_sort_column_3=>'C003'
,p_sort_direction_3=>'DESC'
,p_sort_column_4=>'C004'
,p_sort_direction_4=>'DESC'
,p_sort_column_5=>'C011'
,p_sort_direction_5=>'DESC'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(62543383209438711)
,p_name=>'Impresion Seguros Masivos'
,p_template=>wwv_flow_imp.id(270526680051046670)
,p_display_sequence=>140
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.cse_fecha_ini,',
'       a.cli_id,',
'       (SELECT c.nombre_completo',
'          FROM v_asdm_datos_clientes c',
'         WHERE c.cli_id = b.cli_id) nombre,',
'       (SELECT p.pse_descripcion',
'          FROM asdm_plan_seguros p',
'         WHERE p.pse_id = a.pse_id) producto,',
'       ''IMPRIMIR'' imprime,',
'       a.ose_id,',
'       b.uge_id,',
'       CASE b.mnc_id',
'         WHEN NULL THEN',
'          NULL',
'         WHEN',
'          to_number(:P41_CN_MNC_ID_ANULACION_MISMODIA) THEN',
'          NULL',
'         WHEN',
'          to_number(:P41_CN_MNC_ID_RECHAZO_ASEGURADORA) THEN',
'          NULL',
'         ELSE',
'          CASE',
'            WHEN b.ese_id = ''ANU'' THEN',
'             ''IMPRIMIR''',
'            ELSE',
'             NULL',
'          END',
'       END imprimir_anu,',
'       decode(b.ese_id,',
'              ''EMI'',',
'              ''EMITIDO'',',
'              ''ENV'',',
'              ''ENVIADO'',',
'              ''ANU'',',
'              ''ANULADO'') estado,',
'       (SELECT mn.mnc_descripcion',
'          FROM asdm_motivos_nc mn',
'         WHERE mn.mnc_id = b.mnc_id) motivo,',
'         a.ord_origen',
'  FROM ven_ordenes_seguros a, car_cuentas_seguros b',
' WHERE b.cse_id = a.cse_id',
' and b.uge_id = :f_uge_id',
' AND  b.cse_fecha_ini between to_date(:P41_FECHADESDE,''DD-MM-YYYY'') and to_date(:P41_FECHAHASTA,''DD-MM-YYYY'')',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62543658114438777)
,p_query_column_id=>1
,p_column_alias=>'CSE_FECHA_INI'
,p_column_display_sequence=>1
,p_column_heading=>'CSE_FECHA_INI'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62543764621438780)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62543868217438780)
,p_query_column_id=>3
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>3
,p_column_heading=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62543952539438780)
,p_query_column_id=>4
,p_column_alias=>'PRODUCTO'
,p_column_display_sequence=>4
,p_column_heading=>'PRODUCTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62544063512438780)
,p_query_column_id=>5
,p_column_alias=>'IMPRIME'
,p_column_display_sequence=>5
,p_column_heading=>'IMPRIME CERTIFICADO'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:41:&SESSION.:IMPRIMIRS:&DEBUG.::P41_OSE_ID,P41_UGE_ID:#OSE_ID#,#UGE_ID#'
,p_column_linktext=>'#IMPRIME#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62550153731463931)
,p_query_column_id=>6
,p_column_alias=>'OSE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Ose Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(62550282535463932)
,p_query_column_id=>7
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(303438577839263713)
,p_query_column_id=>8
,p_column_alias=>'IMPRIMIR_ANU'
,p_column_display_sequence=>8
,p_column_heading=>'IMPRIME ANULACION'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:41:&SESSION.:IMPRIMIRANU:&DEBUG.::P41_OSE_ID,P41_UGE_ID:#OSE_ID#,#UGE_ID#'
,p_column_linktext=>'#IMPRIMIR_ANU#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(365436769389903618)
,p_query_column_id=>9
,p_column_alias=>'ESTADO'
,p_column_display_sequence=>9
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(365436883052903623)
,p_query_column_id=>10
,p_column_alias=>'MOTIVO'
,p_column_display_sequence=>10
,p_column_heading=>'Motivo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(150206036412418663667)
,p_query_column_id=>11
,p_column_alias=>'ORD_ORIGEN'
,p_column_display_sequence=>11
,p_column_heading=>'Ord origen'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(268453556860404950)
,p_name=>'prueba'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>150
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'*  from apex_collections col',
' where col.collection_name =',
'       pq_constantes.fn_retorna_constante(null, ''cv_col_comprobantes'')'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268453856404404980)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268453975134404980)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454059707404980)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454155464404980)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454266346404981)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454360091404981)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454464169404981)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454567631404981)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454675960404981)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454757588404981)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454856976404981)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268454972230404983)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455061392404983)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455168370404983)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455273489404983)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455366147404983)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455472784404983)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455578034404983)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455678268404983)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455762637404983)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455865214404983)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268455977329404983)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456068179404983)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456169035404983)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456254561404983)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456367673404983)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456480062404983)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456553261404983)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456672994404983)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456763149404983)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456877386404983)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268456982425404983)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457070500404983)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457183858404983)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457267110404983)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457379544404983)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457456352404983)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457553598404983)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457660704404983)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457769793404983)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457869893404983)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268457955493404983)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458066716404983)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458167398404984)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458277903404987)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458372097404987)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458456533404987)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458581374404987)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458676070404987)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458756834404987)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458857944404988)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268458979346404988)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459065849404988)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459153110404988)
,p_query_column_id=>54
,p_column_alias=>'BLOB001'
,p_column_display_sequence=>54
,p_column_heading=>'BLOB001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459255997404988)
,p_query_column_id=>55
,p_column_alias=>'XMLTYPE001'
,p_column_display_sequence=>55
,p_column_heading=>'XMLTYPE001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459359980404988)
,p_query_column_id=>56
,p_column_alias=>'N001'
,p_column_display_sequence=>56
,p_column_heading=>'N001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459453147404988)
,p_query_column_id=>57
,p_column_alias=>'N002'
,p_column_display_sequence=>57
,p_column_heading=>'N002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459561082404988)
,p_query_column_id=>58
,p_column_alias=>'N003'
,p_column_display_sequence=>58
,p_column_heading=>'N003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459652393404988)
,p_query_column_id=>59
,p_column_alias=>'N004'
,p_column_display_sequence=>59
,p_column_heading=>'N004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459773245404988)
,p_query_column_id=>60
,p_column_alias=>'N005'
,p_column_display_sequence=>60
,p_column_heading=>'N005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459867805404988)
,p_query_column_id=>61
,p_column_alias=>'D001'
,p_column_display_sequence=>61
,p_column_heading=>'D001'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268459956545404991)
,p_query_column_id=>62
,p_column_alias=>'D002'
,p_column_display_sequence=>62
,p_column_heading=>'D002'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268460061185404991)
,p_query_column_id=>63
,p_column_alias=>'D003'
,p_column_display_sequence=>63
,p_column_heading=>'D003'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268460176909404991)
,p_query_column_id=>64
,p_column_alias=>'D004'
,p_column_display_sequence=>64
,p_column_heading=>'D004'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268460255405404991)
,p_query_column_id=>65
,p_column_alias=>'D005'
,p_column_display_sequence=>65
,p_column_heading=>'D005'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(268460358532404991)
,p_query_column_id=>66
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>66
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(28584179013902296027)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_button_name=>'regresar_ordContado'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'IR a Orden de Venta Contado-TC'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:141:&SESSION.::&DEBUG.:RP:F_PARAMETRO:VEN'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(42859964199784624)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'CARGAR'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62551377010468300)
,p_name=>'P41_OSE_ID'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(62543383209438711)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(62551569893471609)
,p_name=>'P41_UGE_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(62543383209438711)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(67868581597721538)
,p_name=>'P41_CAT_ID_CELULARES'
,p_item_sequence=>20
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102242468253332292)
,p_name=>'P41_FECHADESDE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_item_default=>'trunc(sysdate)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'FechaDesde'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(102242679335335528)
,p_name=>'P41_FECHAHASTA'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_item_default=>'trunc(sysdate)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'FechaHasta'
,p_format_mask=>'DD-MM-YYYY'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(277143164454857982)
,p_name=>'P41_COM_ID'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785426691380983433)
,p_name=>'P41_CN_VAR_ID_TVE'
,p_item_sequence=>30
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785426773877983434)
,p_name=>'P41_CN_VAR_ID_SUBTOTAL'
,p_item_sequence=>40
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785426874126983435)
,p_name=>'P41_CN_VAR_ID_DSCTO_FACT'
,p_item_sequence=>50
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427022396983436)
,p_name=>'P41_CN_VAR_ID_SUBTOTAL_CON_IVA'
,p_item_sequence=>60
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427122909983437)
,p_name=>'P41_CN_VAR_ID_SUBTOTAL_SIN_IVA'
,p_item_sequence=>70
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427235238983438)
,p_name=>'P41_CN_VAR_ID_IVA_CALCULO'
,p_item_sequence=>80
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427258092983439)
,p_name=>'P41_CN_VAR_ID_FIN_TOTAL_FACTURA'
,p_item_sequence=>90
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427422119983440)
,p_name=>'P41_CN_VAR_ID_INT_DIFERIDO'
,p_item_sequence=>100
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427492914983441)
,p_name=>'P41_CN_MNC_ID_ANULACION_MISMODIA'
,p_item_sequence=>110
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(785427636199983442)
,p_name=>'P41_CN_MNC_ID_RECHAZO_ASEGURADORA'
,p_item_sequence=>120
,p_item_display_point=>'BODY_3'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(15590751867760806078)
,p_name=>'P41_SEG_ID'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_prompt=>'Seg Id'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    UNIQUE tse_id',
'FROM      asdm_agencias ',
'WHERE     uge_id = :F_UGE_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(44429919737468157640)
,p_name=>'P41_MCA_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(55522259608392227)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(44429919777410157641)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_mov_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'                    ',
'',
'',
' pq_ven_movimientos_caja.pr_imprimir_mov_caja(pn_emp_id => :f_emp_id,',
'                                                     pn_mca_id =>:P41_MCA_ID,',
'                                                     pv_error  => :P0_ERROR); ',
'',
'',
'',
'                                                     ',
'                                                     ',
':P41_MCA_ID:=null;                                                     ',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'P41_MCA_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>44397666626140392715
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(55050477474434206041)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_certificado_cp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
unistr('--INICIO R1994-08: FPALOMEQUE 05/06/2019 \2013 \2018Graba gestion de venta activa de factura\2019'),
'DECLARE',
'  ln_ord_id asdm_e.ven_ordenes.ord_id%TYPE;',
'  ln_tse_id asdm_e.asdm_tipos_segmento.tse_id%TYPE;',
'BEGIN',
'  SELECT c.ord_id, c.tse_id',
'    INTO ln_ord_id, ln_tse_id',
'    FROM ven_comprobantes c',
'   WHERE c.com_id = :p41_com_id;',
'',
'  BEGIN',
'    asdm_p.pq_crm_mantenimientos.pr_graba_gestion_factura(pn_emp_id => :f_emp_id,',
'                                                          pn_ord_id => ln_ord_id,',
'                                                          pn_tse_id => ln_tse_id,',
'                                                          pn_com_id => :p41_com_id);',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'  END;',
unistr('  --FIN R1994-08: FPALOMEQUE 05/06/2019 \2013 \2018Graba gestion de venta activa de factura\2019'),
'',
'  pq_car_imprimir.pr_imprimir_doc_caja(pn_emp_id     => :f_emp_id,',
'                                       pn_mon_id     => 1,',
'                                       pn_com_num    => NULL,',
'                                       pn_com_id     => :p41_com_id,',
'                                       pd_fecha      => NULL,',
'                                       pv_num_identi => NULL,',
'                                       pn_cli_id     => NULL,',
'                                       pn_documento  => 35,',
'                                       pv_error      => :p0_error);',
'',
'  :p41_com_id := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'P41_COM_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>55018224323164441115
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(62553251311476821)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_seguros.pr_impresion_certificado(pn_emp_id => :f_emp_id,',
'                                         pn_uge_id => :p41_uge_id,',
'                                         pn_ose_id => :p41_ose_id,',
'                                         pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIRS'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>30300100041711895
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(277147581352917846)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_IMPRIMIR_REPOSITORIO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'LN_TTR_ID NUMBER;',
'LN_TRX_ID NUMBER;',
'begin',
'',
'SELECT C.TRX_ID,C.TTR_ID ',
'INTO LN_TRX_ID,LN_TTR_ID ',
'FROM VEN_COMPROBANTES C WHERE C.COM_ID=:P41_COM_ID;',
'',
'asdm_p.pq_con_imprimir.pr_imprimir_rep(PN_EMP_ID=>:f_emp_id,',
'                        PN_TRX_ID=>LN_TRX_ID,',
'                        PN_MON_ID =>1,',
'                        PV_ERROR =>:P0_ERROR) ;',
'',
'',
'asdm_p.pq_con_imprimir.PR_IMPRIMIR_COMP_TTR(PN_EMP_ID=>:f_emp_id,',
'                                 PN_TRX_ID=>LN_TRX_ID,',
'                                 PN_TTR_ID=>LN_TTR_ID ,',
'                                 PV_ERROR =>:P0_ERROR);',
'',
'',
':P41_COM_ID:=null;',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'contable'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>244894430083152920
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(303440968819294837)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_anu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_asdm_seguros.pr_impresion_anulacion(pn_emp_id => :f_emp_id,',
'                                         pn_uge_id => :p41_uge_id,',
'                                         pn_ose_id => :p41_ose_id,',
'                                         pv_error  => :p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'IMPRIMIRANU'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>271187817549529911
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(854036017122429844)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_load_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if :P41_CN_VAR_ID_TVE is null then',
'    :P41_CAT_ID_CELULARES := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_cat_id_celulares'');',
'    :P41_CN_VAR_ID_TVE := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_tve'');',
'    :P41_CN_VAR_ID_SUBTOTAL := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_subtotal'');',
'    :P41_CN_VAR_ID_DSCTO_FACT := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_dscto_fact'');',
'    :P41_CN_VAR_ID_SUBTOTAL_CON_IVA := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_subtotal_con_iva'');',
'    :P41_CN_VAR_ID_SUBTOTAL_SIN_IVA := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_subtotal_sin_iva'');',
'    :P41_CN_VAR_ID_IVA_CALCULO := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_iva_calculo'');',
'    :P41_CN_VAR_ID_FIN_TOTAL_FACTURA := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_fin_total_factura'');',
'    :P41_CN_VAR_ID_INT_DIFERIDO := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_int_diferido'');',
'    :P41_CN_MNC_ID_ANULACION_MISMODIA := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_mnc_id_anulacion_mismodia'');',
'    :P41_CN_MNC_ID_RECHAZO_ASEGURADORA := pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_mnc_id_rechazo_aseguradora'');',
'end if;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>821782865852664918
);
wwv_flow_imp.component_end;
end;
/
