prompt --application/pages/page_00015
begin
--   Manifest
--     PAGE: 00015
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>15
,p_name=>'Generacion de Provisiones'
,p_step_title=>'Generacion De Provisiones'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'16'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246366477260922411)
,p_plug_name=>'TIPOS DE PROVISION'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246366674680922411)
,p_plug_name=>'CONFIRMACION: Esta seguro que desea generar provision por &P15_PROVISION.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P15_TPR_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246370952633927289)
,p_plug_name=>'ERROR'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P15_ERROR'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246371480337935324)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246372058997938633)
,p_plug_name=>'<B>&P0_TTR_DESCRIPCION.   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'function pull_multi_value(pValue){',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ''APPLICATION_PROCESS=P_BUSCA_PERSONAS'',0);',
'    if(pValue)',
'    {',
'        get.add(''F_PER_ID'',pValue)',
'    }else',
'    {',
'        get.add(''F_PER_ID'',''null'')',
'    }    ',
'    gReturn = get.get(''XML'');',
'    if(gReturn)',
'    {',
'            var l_Count = gReturn.getElementsByTagName("item").length;',
'            for(var i = 0;i<l_Count;i++)',
'            {',
'             var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'              var l_ID = l_Opt_Xml.getAttribute(''id'');',
'               var l_El = html_GetElement(l_ID);   ',
'                ',
'                if(l_Opt_Xml.firstChild)',
'                {',
'                var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'                 }',
'                 else',
'                 {',
'                 var l_Value = '''';',
'                 }',
'',
'                 if(l_El)',
'                 {',
'                 if(l_El.tagName == ''INPUT'')',
'                 {',
'                  l_El.value = l_Value;',
'                  }else',
'                   if(l_El.tagName == ''SPAN'' && l_El.className == ''grabber'')',
'                      {',
'                      l_El.parentNode.innerHTML = l_Value;',
'                      l_El.parentNode.id = l_ID;',
'                      }',
'                   else{',
'                       l_El.innerHTML = l_Value;',
'                       }',
'                       ',
'                  }',
'             }',
'    ',
'     }',
'     ',
'    get = null;',
'    ',
'    var ln_existe = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=dummy'',0);',
'   ',
'    g_existe = ln_existe.get();',
'    g_existe = ln_existe.get(''XML'');',
'',
' if ($x(''P14_CLIENTE_EXISTE'').value=="N")',
' {',
'javascript:popUp2(''http://oas.pdm:7777/pls/apex/f?p=&APP_ID.:201:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_'
||'ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,,1,1,S'', 650, 650);',
'  }',
'doSubmit();',
'}',
'',
'</script>'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(246366875886922414)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_button_name=>'SI'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Si'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(246367082109922417)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_button_name=>'NO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'No'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(246368962205922422)
,p_branch_action=>'f?p=&APP_ID.:15:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(246369166004922423)
,p_branch_action=>'f?p=&FLOW_ID.:15:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(246366875886922414)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(246369382796922423)
,p_branch_action=>'f?p=&FLOW_ID.:15:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(246367082109922417)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246367275185922417)
,p_name=>'P15_PROVISION'
,p_item_sequence=>72
,p_item_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_prompt=>'PROVISION:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246367462475922420)
,p_name=>'P15_PROXIMA_EJECUCION'
,p_item_sequence=>73
,p_item_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_prompt=>'PROXIMA EJECUCION:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246367664030922420)
,p_name=>'P15_FRECUENCIA_MESES'
,p_item_sequence=>74
,p_item_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_prompt=>'FRECUENCIA (Meses):'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246367883604922421)
,p_name=>'P15_ULTIMA_EJECUCION'
,p_item_sequence=>75
,p_item_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_prompt=>'ULTIMA EJECUCION:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246368080432922421)
,p_name=>'P15_OBSERVACIONES'
,p_item_sequence=>76
,p_item_plug_id=>wwv_flow_imp.id(246366674680922411)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246368272485922421)
,p_name=>'P15_TPR_ID'
,p_item_sequence=>86
,p_item_plug_id=>wwv_flow_imp.id(246366477260922411)
,p_prompt=>'Escoja una Provision'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_named_lov=>'LOV_TIPPROV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(1000);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_CAR_TIPOS_PROVISION_OTRO'')||'' AND emp_id = ''||:f_emp_id;',
'return (lv_lov);',
'END;'))
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'SUBMIT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246372373024938635)
,p_name=>'P15_AGE_ID_AGENCIA'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(246372058997938633)
,p_item_default=>'to_number(:f_age_id_agencia)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Age Id Agencia'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246372565791938635)
,p_name=>'P15_NC_FECHA'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(246372058997938633)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(246368482595922422)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PRESENTAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tpr_descripcion,',
'       tpr_proxima_ejecucion,',
'       tpr_frecuencia_meses,',
'       tpr_ultima_ejecucion',
'  INTO :p15_provision,',
'       :p15_proxima_ejecucion,',
'       :p15_frecuencia_meses,',
'       :p15_ultima_ejecucion',
'  FROM car_tipos_provisiones',
' WHERE tpr_id = :p15_tpr_id;',
'IF SYSDATE < to_date(:p15_proxima_ejecucion, ''DD-MM-YYYY'') THEN',
'  :p15_observaciones := '' Faltan '' ||',
'                        to_char(to_number(to_date(:p15_proxima_ejecucion,',
'                                                  ''DD-MM-YYYY'') - SYSDATE),',
unistr('                                ''990'') || '' dias para la proxima ejecuci\00BF\00BFn'';'),
'  /*      elsif add_months(to_date(:p15_ultima_ejecucion,''DD-MM-YYYY''),to_number(:p15_frecuencia_meses)-1) <=  to_date(:p15_proxima_ejecucion,''DD-MM-YYYY'') then',
unistr('  :P15_OBSERVACIONES  := :P15_OBSERVACIONES||'' La ultima ejecucion ''||:p15_ultiima_ejecucion||'' mas la frecuencia no coincide con la Fecha de Ejecuci\00BF\00BFn'';*/'),
'  NULL;',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P15_TPR_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>214115331326157496
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(246368660277922422)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'GENERAR'
,p_process_sql_clob=>'PQ_CAR_PROVISION.PR_GENERA_PROVISION(:F_EMP_ID,:F_AGE_ID_AGENCIA,:P15_TPR_ID);'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(246366875886922414)
,p_process_success_message=>'Provision &P15_PROVISION. Generada'
,p_internal_uid=>214115509008157496
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(246379956546060842)
,p_process_sequence=>110
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'null;',
'/*',
'ESTA COMENTADO PORQUE HAY QUE ARREGLAR LOS PARAMETROS',
':P0_TTR_ID := pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_prov_x_rebaj'');',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'   :F_EMP_ID,',
'   :F_PCA_ID,',
'   :P0_TTR_ID,',
'   :P0_TTR_DESCRIPCION, ',
'   :P0_PERIODO,',
'   :P0_DATFOLIO,',
'   :P0_FOL_SEC_ACTUAL,',
'   :P0_PUE_NUM_SRI  ,',
'   :P0_UGE_NUM_EST_SRI ,',
'   :P0_PUE_ID,',
'  :P0_ERROR);',
'',
'*/',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>214126805276295916
);
wwv_flow_imp.component_end;
end;
/
