prompt --application/pages/page_00043
begin
--   Manifest
--     PAGE: 00043
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>43
,p_name=>'VEN_DEPOSITOS_REALES'
,p_step_title=>'VEN_DEPOSITOS_REALES'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<!--',
'',
' htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'',
'//-->',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'16'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(51882253189574666)
,p_plug_name=>'VEN_DEPOSITOS_REALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51882568898574679)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_button_name=>'NUEVO'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo'
,p_button_position=>'CHANGE'
,p_button_redirect_url=>'f?p=&APP_ID.:43:&SESSION.::&DEBUG.:43::'
,p_button_condition=>'P43_DRE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51882861651574680)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cancelar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:37:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51882472381574679)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'CREATE'
,p_button_condition=>'P43_DRE_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51882664496574679)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Delete'
,p_button_position=>'DELETE'
,p_button_redirect_url=>'javascript:confirmDelete(htmldb_delete_message,''DELETE'');'
,p_button_condition=>'P43_DRE_ID'
,p_button_condition_type=>'NEVER'
,p_database_action=>'DELETE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(52408073043855246)
,p_branch_action=>'f?p=&APP_ID.:37:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(51882472381574679)
,p_branch_sequence=>11
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>':p0_error is null'
,p_branch_condition_text=>'PLSQL'
,p_branch_comment=>'Created 21-SEP-2010 11:14 by PLOPEZ'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(51883466056574691)
,p_branch_action=>'f?p=&APP_ID.:43:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51883676892574698)
,p_name=>'P43_DRE_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dre Id'
,p_source=>'DRE_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51883880774574709)
,p_name=>'P43_EMP_ID'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_item_default=>':f_emp_id'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Emp Id'
,p_source=>'EMP_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51884282657574718)
,p_name=>'P43_EDE_ID'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_prompt=>'Ucb Id'
,p_source=>'UCB_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion d,',
'       C.EDE_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE A.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id = :f_uge_id',
'       AND d.ede_id = decode(:p43_entidad_destino,''%'',0,:p43_entidad_destino)',
'ORDER  BY a.ucb_prioridad'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Seleccione Cuenta'
,p_lov_null_value=>'%'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'REDIRECT_SET_VALUE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51884674896574719)
,p_name=>'P43_TRX_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Trx Id'
,p_source=>'TRX_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51885080524574719)
,p_name=>'P43_MCA_ID'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_prompt=>'Deposito Transitorio'
,p_source=>'MCA_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT mca_fecha||'' - ''||mca_total d,mca_id r',
'FROM   asdm_movimientos_caja a',
'WHERE  a.trx_id IN',
'       (SELECT trx_id',
'        FROM   asdm_transacciones',
'        WHERE  ttr_id IN',
'               (pq_constantes.fn_retorna_constante(NULL,',
'                                                   ''cn_ttr_id_egr_caj_dep_tran''),',
'                pq_constantes.fn_retorna_constante(NULL,',
'                                                   ''cn_ttr_id_cierre_caja'')))',
'AND a.EDE_id=DECODE(:p43_EDE_id,NULL,0,:p43_EDE_id)',
' AND',
'       mca_id NOT IN',
'       (SELECT mca_id',
'        FROM   ven_depositos_reales z',
'        WHERE  z.dre_estado_registro =',
'               pq_constantes.fn_retorna_constante(NULL,',
'                                                  ''cv_estado_reg_activo''))'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Seleccione Deposito '
,p_lov_null_value=>'%'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'REDIRECT_SET_VALUE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51885466616574720)
,p_name=>'P43_DRE_FECHA'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha'
,p_source=>'DRE_FECHA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>15
,p_cMaxlength=>255
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51885878528574720)
,p_name=>'P43_DRE_NUMERO_PAPELETA'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_prompt=>'Numero Papeleta'
,p_source=>'DRE_NUMERO_PAPELETA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>15
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51886267894574721)
,p_name=>'P43_DRE_VALOR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_prompt=>'Valor'
,p_source=>'DRE_VALOR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51887083813574721)
,p_name=>'P43_DRE_RAZON_DESCUADRE'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Razon Descuadre'
,p_source=>'DRE_RAZON_DESCUADRE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>32
,p_cMaxlength=>100
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51887265596574722)
,p_name=>'P43_DRE_VALOR_DESCUADRE'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Descuadre'
,p_source=>'nvl(:p43_mca_total,0)-nvl(:p43_dre_valor,0)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>255
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51887465052574722)
,p_name=>'P43_DRE_ESTADO_REGISTRO'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Estado Registro'
,p_source=>'DRE_ESTADO_REGISTRO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTADO_REGISTRO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(300);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ESTADO_REG'');',
'return (lv_lov);',
'END;'))
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52065070883756558)
,p_name=>'P43_ENTIDAD_DESTINO'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_prompt=>'Entidad Destino'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENT_DESTINO_X_UGE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Seleccione Entidad'
,p_lov_null_value=>'%'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'REDIRECT_SET_VALUE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(52320559202226881)
,p_name=>'P43_MCA_TOTAL'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(51882253189574666)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Movimiento'
,p_source=>'select nvl(mca_total,0) from asdm_movimientos_caja where mca_id=:p43_mca_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51884175274574713)
,p_validation_name=>'P43_EMP_ID not null'
,p_validation_sequence=>20
,p_validation=>'P43_EMP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Emp Id must have some value.'
,p_when_button_pressed=>wwv_flow_imp.id(51882472381574679)
,p_associated_item=>wwv_flow_imp.id(51883880774574709)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51885351466574719)
,p_validation_name=>'P43_MCA_ID not null'
,p_validation_sequence=>50
,p_validation=>'P43_MCA_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Mca Id must have some value.'
,p_when_button_pressed=>wwv_flow_imp.id(51882472381574679)
,p_associated_item=>wwv_flow_imp.id(51885080524574719)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51885770306574720)
,p_validation_name=>'P43_DRE_FECHA not null'
,p_validation_sequence=>60
,p_validation=>'P43_DRE_FECHA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Dre Fecha must have some value.'
,p_when_button_pressed=>wwv_flow_imp.id(51882472381574679)
,p_associated_item=>wwv_flow_imp.id(51885466616574720)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51886162803574720)
,p_validation_name=>'P43_DRE_NUMERO_PAPELETA not null'
,p_validation_sequence=>70
,p_validation=>'P43_DRE_NUMERO_PAPELETA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Dre Numero Papeleta must have some value.'
,p_when_button_pressed=>wwv_flow_imp.id(51882472381574679)
,p_associated_item=>wwv_flow_imp.id(51885878528574720)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51886571576574721)
,p_validation_name=>'P43_DRE_VALOR not null'
,p_validation_sequence=>80
,p_validation=>'P43_DRE_VALOR'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Dre Valor must have some value.'
,p_when_button_pressed=>wwv_flow_imp.id(51882472381574679)
,p_associated_item=>wwv_flow_imp.id(51886267894574721)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51887762066574722)
,p_validation_name=>'P43_DRE_ESTADO_REGISTRO not null'
,p_validation_sequence=>120
,p_validation=>'P43_DRE_ESTADO_REGISTRO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Dre Estado Registro must have some value.'
,p_when_button_pressed=>wwv_flow_imp.id(51882472381574679)
,p_associated_item=>wwv_flow_imp.id(51887465052574722)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51887977990574723)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from VEN_DEPOSITOS_REALES'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'VEN_DEPOSITOS_REALES'
,p_attribute_03=>'P43_DRE_ID'
,p_attribute_04=>'DRE_ID'
,p_process_error_message=>'Unable to fetch row.'
,p_internal_uid=>19634826720809797
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51888358011574726)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_process_when_button_id=>wwv_flow_imp.id(51882664496574679)
,p_internal_uid=>19635206741809800
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(52389564414729776)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_deposito_real'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'if (:p43_dre_valor_descuadre =0 and :p43_dre_razon_descuadre is null) or (:p43_dre_valor_descuadre !=0 and :p43_dre_razon_descuadre is not null) then',
'  pq_ven_movimientos_caja.pr_deposito_real(pn_emp_id              => :f_emp_id,',
'                                           pn_uge_id              => :f_uge_id,',
'                                           pn_user_id             => :f_user_id,',
'                                           pn_ttr_id              => pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                        ''cn_ttr_id_dep_real''),',
'                                           pn_trx_id              => :p43_trx_id,',
'                                           pn_uge_id_gasto        => :f_uge_id_gasto,',
'                                           pn_dre_id              => :p43_dre_id,',
'                                           pn_EDE_id              => :p43_EDE_id,',
'                                           pn_mca_id              => :p43_mca_id,',
'                                           pd_dre_fecha           => :p43_dre_fecha,',
'                                           pv_dre_numero_papeleta => :p43_dre_numero_papeleta,',
'                                           pn_dre_valor           => :p43_dre_valor,',
'                                           pv_dre_razon_descuadre => :p43_dre_razon_descuadre,',
'                                           pn_dre_valor_descuadre => :p43_dre_valor_descuadre,',
'                                           pv_dre_estado_registro => :p43_dre_estado_registro,',
'                                           pv_error               => :p0_error);',
'else',
':p0_error:=''Determinar razon de descuadre'';',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(51882472381574679)
,p_internal_uid=>20136413144964850
);
wwv_flow_imp.component_end;
end;
/
