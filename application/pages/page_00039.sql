prompt --application/pages/page_00039
begin
--   Manifest
--     PAGE: 00039
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>39
,p_name=>'Depositos Transitorios'
,p_step_title=>'Depositos Transitorios'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_updated_by=>'XECALLE'
,p_last_upd_yyyymmddhh24miss=>'20240129094628'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(48170575403087499)
,p_plug_name=>'Depositos Transitorios'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT 1 TIPO_ID,',
'''EFECTIVO / CHEQUES'' TIPO, a.*',
'  FROM asdm_movimientos_caja a, asdm_transacciones b',
' where b.emp_id = a.emp_id',
'   and b.trx_id = a.trx_id',
'   and b.ttr_id IN(',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_egr_caj_dep_tran''),9)',
'   and not exists (select x.tfp_id',
'          from asdm_movimientos_caja_detalle x',
'         where x.mca_id = a.mca_id',
'           and x.emp_id = a.emp_id',
'           and x.tfp_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito''))',
'   and a.emp_id = :F_EMP_ID',
'   and a.pca_id = :F_PCA_ID',
'UNION',
'SELECT 2 TIPO_ID,',
'''TARJETA CREDITO'' TIPO, a.*',
'  FROM asdm_movimientos_caja a, asdm_transacciones b',
' where b.emp_id = a.emp_id',
'   and b.trx_id = a.trx_id',
'   and b.ttr_id =',
'       pq_constantes.fn_retorna_constante(NULL,',
'                                          ''cn_ttr_id_egr_caj_dep_tran'')',
'   and exists (select x.tfp_id',
'          from asdm_movimientos_caja_detalle x',
'         where x.mca_id = a.mca_id',
'           and x.emp_id = a.emp_id',
'           and x.tfp_id IN( pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito''),9))',
'   and a.emp_id = :F_EMP_ID',
'   and a.pca_id = :F_PCA_ID',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(48170673909087499)
,p_name=>'Report 1'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_detail_link=>'f?p=&APP_ID.:62:&SESSION.:graba:&DEBUG.::P62_MCA_ID,P62_TIPO_ID:#MCA_ID#,#TIPO_ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#magnifying_glass_white_bg.gif" alt="">'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'PLOPEZ'
,p_internal_uid=>15917522639322573
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48170876555087511)
,p_db_column_name=>'MCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Mov Caja.'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48170967092087515)
,p_db_column_name=>'EMP_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Empresa'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48171554451087516)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Fecha Deposito Transitorio'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48171662452087516)
,p_db_column_name=>'MCA_OBSERVACION'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>unistr('Observaci\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48171756404087516)
,p_db_column_name=>'MCA_ESTADO_MC'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Estado MC'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_ESTADO_MC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48171861372087517)
,p_db_column_name=>'MCA_TOTAL'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Total'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'MCA_TOTAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(48171968735087517)
,p_db_column_name=>'MCA_ESTADO_REGISTRO'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'MCA_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(53303377369861768)
,p_db_column_name=>'EDE_ID'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136408751685423145)
,p_db_column_name=>'TRX_ID'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'TRX'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136408852243423169)
,p_db_column_name=>'PCA_ID'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Periodo Caja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136408980146423170)
,p_db_column_name=>'MCA_ID_REFERENCIA'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Mca Id Referencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_ID_REFERENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136409062670423170)
,p_db_column_name=>'AGE_ID_GESTIONADO_POR'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Age Id Gestionado Por'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_GESTIONADO_POR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136409168125423170)
,p_db_column_name=>'CLI_ID'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136433175392666535)
,p_db_column_name=>'TIPO'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(136458166406815225)
,p_db_column_name=>'TIPO_ID'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Tipo Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TIPO_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(314447570199988489)
,p_db_column_name=>'UGE_ID'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(48172751363090570)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292726754919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'TIPO:MCA_ID:MCA_FECHA:MCA_OBSERVACION:MCA_TOTAL:PCA_ID:UGE_ID'
,p_break_on=>'TIPO:0:0:0:0:0'
,p_break_enabled_on=>'TIPO:0:0:0:0:0'
,p_sum_columns_on_break=>'MCA_TOTAL'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(123540183912899972)
,p_name=>'Movimientos Efectivo-Cheques'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>10
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''EFECTIVO Y CHEQUES'' FORMA_PAGO,',
'       (NVL(:P39_EFECTIVO, 0) + NVL(:P39_CHEQUES, 0)) VALOR_MOVIMIENTOS,',
'       ''DEPOSITAR'' DEPOSITAR',
'FROM DUAL',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136593176270180476)
,p_query_column_id=>1
,p_column_alias=>'FORMA_PAGO'
,p_column_display_sequence=>1
,p_column_heading=>'Forma Pago'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136593262886180476)
,p_query_column_id=>2
,p_column_alias=>'VALOR_MOVIMIENTOS'
,p_column_display_sequence=>2
,p_column_heading=>'Valor Movimientos'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136593370142180476)
,p_query_column_id=>3
,p_column_alias=>'DEPOSITAR'
,p_column_display_sequence=>3
,p_column_heading=>'Depositar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:62:&SESSION.::&DEBUG.::P62_TIPO:1'
,p_column_linktext=>'#DEPOSITAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(123612162245404596)
,p_name=>'Movimientos Tarjetas Cr&eacute;dito'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''TARJETAS DE CREDITO'' FORMA_PAGO, NVL(:P39_TARJETAS,0) VALOR_MOVIMIENTOS, ''DEPOSITAR'' DEPOSITAR',
'FROM DUAL',
'',
'/*select TFP_ID,',
'       FORMA_PAGO,',
'       sum(valor_movimiento) VALOR_MOVIMIENTO,',
'       ''DEPOSITAR'' DEPOSITAR',
'  from (select to_number(pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                            ''cn_tfp_id_tarjeta_credito'')) tfp_id,',
'               ''Tarjeta Credito'' forma_pago,',
'               0 valor_movimiento,',
'               ''DEPOSITAR'' DEPOSITAR',
'          from dual',
'        union',
'        select TFP_ID,',
'               FORMA_PAGO,',
'               sum(MCD_VALOR_MOVIMIENTO) VALOR_MOVIMIENTO,',
'               ''DEPOSITAR'' DEPOSITAR',
'          from (SELECT a.tfp_id,',
'                       (SELECT tfp_descripcion',
'                          FROM asdm_tipos_formas_pago',
'                         WHERE tfp_id = a.tfp_id) forma_pago,',
'                       a.ede_id,',
'                       (SELECT ede_descripcion',
'                          FROM asdm_entidades_destinos',
'                         WHERE ede_id = a.ede_id) entidad,',
'                       a.mcd_nro_tarjeta,',
'                       a.mcd_valor_movimiento,',
'                       a.mcd_nro_aut_ref,',
'                       a.mcd_nro_lote,',
'                       a.mcd_id_ref,',
'                       a.mcd_titular_tarjeta',
'                  FROM asdm_movimientos_caja_detalle a,',
'                       asdm_movimientos_caja         b,',
'                       asdm_tipos_formas_pago        c,',
'                       ven_periodos_caja             d,',
'                       asdm_transacciones            e',
'                 WHERE a.mca_id = b.mca_id',
'                   AND b.emp_id = a.emp_id',
'                   AND c.emp_id = a.emp_id',
'                   AND d.emp_id = a.emp_id',
'                   AND e.emp_id = a.emp_id',
'                   AND a.mcd_estado_registro =',
'                       pq_constantes.fn_retorna_constante(NULL,',
'                                                          ''cv_estado_reg_activo'')',
'                   AND b.pca_id = :f_pca_id',
'                   AND a.tfp_id = c.tfp_id',
'                   AND c.tfp_va_al_deposito = ''S''',
'                   AND d.pca_id = b.pca_id',
'                   AND d.pca_estado_pc =',
'                       pq_constantes.fn_retorna_constante(null,',
'                                                          ''cv_pca_estado_pc_abierto'')',
'                   AND d.pca_token =',
'                       pq_constantes.fn_retorna_constante(null,',
'                                                          ''cv_pca_estado_pc_abierto'')',
'                   AND a.tfp_id in',
'                       (pq_constantes.fn_retorna_constante(b.emp_id,',
'                                                           ''cn_tfp_id_tarjeta_credito''))',
'                   AND e.trx_id = b.trx_id',
'                   and e.ttr_id !=',
'                       pq_constantes.fn_retorna_constante(null,',
'                                                          ''cn_ttr_id_egr_caj_dep_tran'')',
'                   AND a.emp_id = :f_emp_id',
'                   AND nvl(A.MCD_DEP_TRANSITORIO, ''N'') !=',
'                       pq_constantes.fn_retorna_constante(null, ''cv_si''))',
'         GROUP BY TFP_ID, FORMA_PAGO)',
' GROUP BY TFP_ID, FORMA_PAGO',
' ORDER BY TFP_ID*/'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123612576650404608)
,p_query_column_id=>1
,p_column_alias=>'FORMA_PAGO'
,p_column_display_sequence=>1
,p_column_heading=>'FORMA_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(136587070336150393)
,p_query_column_id=>2
,p_column_alias=>'VALOR_MOVIMIENTOS'
,p_column_display_sequence=>2
,p_column_heading=>'Valor Movimientos'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(123612777802404613)
,p_query_column_id=>3
,p_column_alias=>'DEPOSITAR'
,p_column_display_sequence=>3
,p_column_heading=>'DEPOSITAR'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:81:&SESSION.::&DEBUG.:81::'
,p_column_linktext=>'#DEPOSITAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(48188073353188539)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(48170575403087499)
,p_button_name=>'NUEVO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(48188360256188541)
,p_branch_action=>'f?p=&APP_ID.:62:&SESSION.::&DEBUG.:62::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(48188073353188539)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(128980672577610060)
,p_name=>'P39_EFECTIVO'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(48170575403087499)
,p_prompt=>'Efectivo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(128981651583613433)
,p_name=>'P39_CHEQUES'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(48170575403087499)
,p_prompt=>'cheques'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(128981858064613435)
,p_name=>'P39_TARJETAS'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(48170575403087499)
,p_prompt=>'tarjetas'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(141409370900440706)
,p_name=>'P39_SALDO_INICIAL'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(48170575403087499)
,p_use_cache_before_default=>'NO'
,p_prompt=>'SALDO INICIAL DE CAJA   $   '
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PUE.PUE_SALDO_INICIAL',
'  FROM ASDM_PUNTOS_EMISION PUE, VEN_PERIODOS_CAJA PCA',
' WHERE PCA.EMP_ID = PUE.EMP_ID',
'   AND PCA.PUE_ID = PUE.PUE_ID',
'   AND PCA.PCA_ID = :F_PCA_ID',
'   AND PCA.EMP_ID = :F_EMP_ID'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:26"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(128979653530604585)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_depositos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id         => :f_emp_id,',
'                        pn_pca_id         => :f_pca_id,',
'                        pn_valor_efectivo => :p39_efectivo,',
'                        pn_valor_cheque   => :p39_cheques,',
'                        pn_valor_tarjeta  => :p39_tarjetas,',
'                        pv_error          => :p0_error);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>96726502260839659
);
wwv_flow_imp.component_end;
end;
/
