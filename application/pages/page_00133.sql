prompt --application/pages/page_00133
begin
--   Manifest
--     PAGE: 00133
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>133
,p_name=>'RENEGOCIACION MAYOREO'
,p_step_title=>'RENEGOCIACION MAYOREO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript" src="#WORKSPACE_IMAGES#ManejoProcesos.js">',
'</script>'))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771040070615894074)
,p_plug_name=>'Datos Clientes'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771050164023917070)
,p_plug_name=>'Cartera Cliente'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c002 NOmbre,',
'       c003 cxc_id,',
'       c004 cxc_saldo,',
'       c005 cartera,',
'       C006 Seleccionado,',
'      C007 unidad_gestion,',
'      C009 cxc_fecha_adicion,',
'      (select cxc.com_id from car_cuentas_por_cobrar cxc where cxc.cxc_id=c003) COM_ID,',
'      (SELECT nvl(SUM(nvl(rdi.rdi_valor_respaldado, 0)), 0)',
'  FROM car_respaldos_cartera rca, car_respaldos_dividendos rdi',
' WHERE rdi.div_id in (select div.div_id',
'                        from car_dividendos div',
'                       where div.cxc_id = c003',
'                         and div.div_estado_dividendo = ''P'')',
'   AND rca.rca_tipo_respaldo = ''CH''',
'   AND rdi.rca_id = rca.rca_id',
'   AND rca.rca_estado_registro = 0',
'   AND rdi.rdi_estado_registro = 0',
'   AND rdi.rdi_estado IS NULL) RESPALDO_CHEQUE,',
'  (SELECT CASE WHEN nvl(SUM(nvl(rdi.rdi_valor_respaldado, 0)), 0)>0 THEN',
'        '' ESTE CREDITO TIENE CHEQUES RESPALDADOS''',
'   ELSE',
'   NULL',
'        END',
'          FROM car_respaldos_cartera rca, car_respaldos_dividendos rdi',
'         WHERE rdi.div_id in',
'               (select div.div_id',
'                  from car_dividendos div',
'                 where div.cxc_id = c003',
'                   and div.div_estado_dividendo = ''P'')',
'           AND rca.rca_tipo_respaldo = ''CH''',
'           AND rdi.rca_id = rca.rca_id',
'           AND rca.rca_estado_registro = 0',
'           AND rdi.rdi_estado_registro = 0',
'           AND rdi.rdi_estado IS NULL) RESPALDO',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_CXC''',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(771050268521917070)
,p_name=>'Cartera Cliente'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'JALVAREZ'
,p_internal_uid=>738797117252152144
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771065368718737271)
,p_db_column_name=>'SEQ_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Seq Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075072668590457)
,p_db_column_name=>'CLI_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075154897590458)
,p_db_column_name=>'NOMBRE'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075276662590458)
,p_db_column_name=>'CXC_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Cxc Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075363120590459)
,p_db_column_name=>'CXC_SALDO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Cxc Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075473161590459)
,p_db_column_name=>'CARTERA'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Cartera'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CARTERA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075575926590459)
,p_db_column_name=>'SELECCIONADO'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Seleccionado'
,p_column_link=>'f?p=&APP_ID.:133:&SESSION.:cambio:&DEBUG.::P133_SEQ_ID,P133_SELECCIONADO:#SEQ_ID#,#SELECCIONADO#'
,p_column_linktext=>'#SELECCIONADO#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'SELECCIONADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075670903590459)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(771075771639590459)
,p_db_column_name=>'CXC_FECHA_ADICION'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Cxc Fecha Adicion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CXC_FECHA_ADICION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(198115765206584046)
,p_db_column_name=>'COM_ID'
,p_display_order=>19
,p_column_identifier=>'K'
,p_column_label=>'Com id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(219097424150192654)
,p_db_column_name=>'RESPALDO_CHEQUE'
,p_display_order=>29
,p_column_identifier=>'L'
,p_column_label=>'Respaldo cheque'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(219097542841192655)
,p_db_column_name=>'RESPALDO'
,p_display_order=>39
,p_column_identifier=>'M'
,p_column_label=>'Respaldo'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(771051365718918432)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'7387983'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'CLI_ID:NOMBRE:CXC_ID:CXC_SALDO:CARTERA:UNIDAD_GESTION:CXC_FECHA_ADICION:SELECCIONADO:COM_ID:RESPALDO_CHEQUE:RESPALDO:'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771265059085238771)
,p_name=>'Valores a Renegociar'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>'SELECT REN.REN_ID, REN.CLI_ID CLIENTE,REN.REN_TOTAL_CAPITAL VALOR_PRESENTE, ''CANCELAR RENEGOCIACION'' CANCELAR  FROM CAR_RENEGOCIACION REN WHERE REN.REN_ID=:P133_REN_ID'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
,p_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C001   CLI_ID,',
'             C002   NOMBRE,',
'             SUM(C004) CXC_SALDO,',
'             C005 CARTERA',
'        FROM APEX_COLLECTIONS',
'       WHERE COLLECTION_NAME = ''COL_CXC''',
'         AND C006= ''S''',
'       GROUP BY C001, C002, C005',
''))
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771282256368953404)
,p_query_column_id=>1
,p_column_alias=>'REN_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Ren Id'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771282370808953405)
,p_query_column_id=>2
,p_column_alias=>'CLIENTE'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771282457207953405)
,p_query_column_id=>3
,p_column_alias=>'VALOR_PRESENTE'
,p_column_display_sequence=>3
,p_column_heading=>'Valor Presente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771282575121953405)
,p_query_column_id=>4
,p_column_alias=>'CANCELAR'
,p_column_display_sequence=>4
,p_column_heading=>'Cancelar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:133:&SESSION.:ELIMINAR:&DEBUG.:::'
,p_column_linktext=>'#CANCELAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771295157170768739)
,p_plug_name=>unistr('Datos Renegociaci\00F3n')
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>40
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771302655195600871)
,p_name=>unistr('Cronograma Renegociaci\00F3n')
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'C001 Nro_Vencimiento,',
'c002 Fecha_Vencimiento,',
'to_number(c003) Valor_Capital,',
'to_number(c004) Valor_Interes,',
'to_number(c005) Valor_Cuota',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771302964687600883)
,p_query_column_id=>1
,p_column_alias=>'NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771303053835600883)
,p_query_column_id=>2
,p_column_alias=>'FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771303179204600883)
,p_query_column_id=>3
,p_column_alias=>'VALOR_CAPITAL'
,p_column_display_sequence=>3
,p_column_heading=>'VALOR_CAPITAL'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771303266900600883)
,p_query_column_id=>4
,p_column_alias=>'VALOR_INTERES'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR_INTERES'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771303362562600883)
,p_query_column_id=>5
,p_column_alias=>'VALOR_CUOTA'
,p_column_display_sequence=>5
,p_column_heading=>'VALOR_CUOTA'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771325868776577567)
,p_name=>'PRUEBA'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>110
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT C.C002',
'      FROM APEX_COLLECTIONS C',
'     WHERE C.COLLECTION_NAME = ''COLL_FECHAS_CORTE'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771326473601577576)
,p_query_column_id=>1
,p_column_alias=>'C002'
,p_column_display_sequence=>1
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771344174897721236)
,p_name=>unistr('Nota de D\00E9bito')
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return q''{SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''ND'''', this.value ,''''P133_UGE_ID'''',''''P133_USER_ID'''',''''P133_EMP_ID'''',''''P133_CLI_ID'''',''''R739091023627956310'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis ',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nd_refinanciamiento'')}'';'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771346565327784253)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Orden'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771346654146784253)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771346775981784253)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'# Comprob.'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771346868218784253)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Empresa'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771346955317784253)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Credito'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347071848784253)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Nuevo Int. Dif'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347163999784253)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>9
,p_column_heading=>'# Folio'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347252884784253)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>7
,p_column_heading=>'Establec'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347368365784253)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>8
,p_column_heading=>'Pto Emis'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347482316784253)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347566609784253)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347679580784253)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347764181784253)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347856806784253)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771347959883784253)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348064809784253)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348161328784253)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348281306784253)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348382983784253)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348452450784253)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348563157784253)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348662036784253)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348765442784253)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348880830784253)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771348975958784253)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349058978784253)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349173826784253)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349260858784253)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349358579784253)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349466769784253)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349571560784253)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349665626784253)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349752320784253)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349877516784253)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771349971512784253)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350071973784253)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350168212784253)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350264049784253)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350370022784253)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350478683784253)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350578803784254)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350662836784254)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350752163784254)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350867833784254)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771350951998784254)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351060601784254)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351179496784254)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351278576784254)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351379161784254)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351460461784254)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351561986784254)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351656481784254)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351776285784255)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351864422784255)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771351977311784255)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771352075420784255)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771352160704784255)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771352253115784255)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771352352791784255)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771352470810784255)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(771353780858978449)
,p_name=>unistr('Nota de Cr\00E9dito')
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>60
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001 cli_id,',
'       c003 comprob,',
'       c006 emp_id,',
'       c007 cxc_id,',
'       c010 intmora,',
'       apex_item.text(25,',
'                      p_value      => nvl(c020, 0),',
'                      p_size       => 8,',
'                      p_attributes => ''onChange="pr_ejecuta_proc_campos_datos(''''PR_VALIDA_FOLIOS_GASTOS'''','' ||',
'                                      seq_id ||',
'                                      '',''''NC'''', this.value ,''''P102_UGE_ID'''',''''P102_USER_ID'''',''''P102_EMP_ID'''',''''P102_CLI_ID'''',''''R739100629589213523'''')"'') folio,',
'       c011 establec,',
'       c012 ptoemis ',
'  FROM apex_collections',
' WHERE collection_name =',
'       pq_constantes.fn_retorna_constante(NULL, ''cv_col_nc_refinanciamiento'')'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354068237978453)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354174244978455)
,p_query_column_id=>2
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>2
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354255077978455)
,p_query_column_id=>3
,p_column_alias=>'COMPROB'
,p_column_display_sequence=>3
,p_column_heading=>'COMPROB'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354371075978455)
,p_query_column_id=>4
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>4
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354477178978455)
,p_query_column_id=>5
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CXC_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354580911978455)
,p_query_column_id=>6
,p_column_alias=>'INTMORA'
,p_column_display_sequence=>6
,p_column_heading=>'INT DIF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354656722978455)
,p_query_column_id=>7
,p_column_alias=>'FOLIO'
,p_column_display_sequence=>7
,p_column_heading=>'FOLIO'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354780645978455)
,p_query_column_id=>8
,p_column_alias=>'ESTABLEC'
,p_column_display_sequence=>8
,p_column_heading=>'ESTABLEC'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(771354872333978455)
,p_query_column_id=>9
,p_column_alias=>'PTOEMIS'
,p_column_display_sequence=>9
,p_column_heading=>'PTOEMIS'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(771358571733042048)
,p_plug_name=>'TOTAL RENEGOCIACION'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>100
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771078669291818040)
,p_button_sequence=>160
,p_button_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_button_name=>'P133_SELECCIONAR'
,p_button_static_id=>'P133_SELECCIONAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Seleccionar Todo'
,p_button_alignment=>'LEFT'
,p_request_source=>'SELECCIONAR TODO'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771302073325539825)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_button_name=>'CALCULAR_CRONOGRA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Calcular Cronograma'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771360768887135879)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(771358571733042048)
,p_button_name=>'GENERA_RENEGOCIACION'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Genera Renegociacion'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771485353232799872)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_button_name=>'IR_AL_PAGO_DE_INTERESES'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Ir Al Pago De Intereses'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:132:&SESSION.::&DEBUG.::P132_REN_ID,P132_PAGO_REFINANCIAMIENTO:&P133_REN_ID.,&P133_VALOR_PAGO_CAJA.'
,p_button_condition=>'P133_DIFERENCIA_ABONO_INTERES'
,p_button_condition2=>'0'
,p_button_condition_type=>'VAL_OF_ITEM_IN_COND_NOT_EQ_COND2'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(771269972019226579)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_button_name=>'CARGAR_CREDITOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Cargar Creditos'
,p_button_position=>'TOP'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(771363872900282122)
,p_branch_name=>'GRABA RENEGOCIACION'
,p_branch_action=>'f?p=&APP_ID.:133:&SESSION.:graba:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(771360768887135879)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(198113852735584027)
,p_name=>'P133_VALOR_PAGO_CAJA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor pago caja'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  ab.ren_total_intereses',
'    FROM CAR_RENEGOCIACION ab',
'   WHERE AB.REN_ID=:P133_REN_ID',
'     AND ab.ren_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771040362174914352)
,p_name=>'P133_TIPO_IDENTIFICACION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'onFocus="if(html_GetElement(''P8_RAP_NRO_RETENCION'').value>0){html_GetElement(''P8_RAP_NRO_RETENCION'').focus()}";'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771041063874943928)
,p_name=>'P133_IDENTIFICACION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Nro. de Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'   SELECT (PE.PER_RAZON_SOCIAL || PE.PER_PRIMER_NOMBRE || '' '' ||',
'          PE.PER_SEGUNDO_NOMBRE || '' '' || PE.PER_PRIMER_APELLIDO || '' '' ||',
'          PE.PER_SEGUNDO_APELLIDO) || '' - Cod: '' || CL.CLI_ID || '' - Id: '' ||',
'          PE.PER_NRO_IDENTIFICACION NOMBRE,',
'          PE.PER_NRO_IDENTIFICACION IDENTIFICACION',
'     FROM ASDM_CLIENTES CL, ASDM_PERSONAS PE',
'    WHERE CL.PER_ID = PE.PER_ID',
'      AND CL.EMP_ID = 1',
'      AND CL.CLI_ESTADO_REGISTRO = 0',
'      AND PE.PER_ESTADO_REGISTRO = 0'))
,p_lov_cascade_parent_items=>'P133_TIPO_IDENTIFICACION'
,p_ajax_items_to_submit=>'P133_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="doSubmit(''cliente'')"'
,p_colspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771042283665995568)
,p_name=>'P133_CLI_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771042554324024377)
,p_name=>'P133_NOMBRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771042959475052416)
,p_name=>'P133_DIRECCION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>unistr('Direcci\00F3n')
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771043257282099020)
,p_name=>'P133_TIPO_TELEFONO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Tipo Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771043477326104878)
,p_name=>'P133_TELEFONO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771047559585447384)
,p_name=>'P133_ALERTA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Alerta'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771047979414453418)
,p_name=>'P133_ALERTA_2'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Alerta 2'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771049873975838602)
,p_name=>'P133_TIPO_DIRECCION'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Tipo Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771052159402997294)
,p_name=>'P133_CORREO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(771040070615894074)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771066675163764699)
,p_name=>'P133_CHECK'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_prompt=>'Check'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771066869770767133)
,p_name=>'P133_REPORTE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_prompt=>'Reporte'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771076161417641201)
,p_name=>'P133_SEQ_ID'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771076356241643644)
,p_name=>'P133_SELECCIONADO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_prompt=>'Seleccionado'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771281770638901204)
,p_name=>'P133_REN_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(771050164023917070)
,p_prompt=>'Ren Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771287271501432671)
,p_name=>'P133_ALERTA_ABONO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:18;color:red;font-family:Arial Narrow"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771287678112520722)
,p_name=>'P133_VALOR_CAPITAL'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_prompt=>'Valor Pendiente'
,p_source=>'SELECT R.REN_TOTAL_CAPITAL FROM CAR_RENEGOCIACION R WHERE R.REN_ID=:P133_REN_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771287969615631026)
,p_name=>'P133_DIFERENCIA'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_prompt=>'Valor Abonado'
,p_source=>'SELECT R.REN_TOTAL_ABONO FROM CAR_RENEGOCIACION R WHERE R.REN_ID=:P133_REN_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771288254950637806)
,p_name=>'P133_VALOR_ABONADO'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Diferencia Abono Intereses'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT CASE',
'         WHEN (AB.REN_TOTAL_ABONO - AB.REN_TOTAL_INTERESES) < 0 THEN',
'          0',
'         ELSE',
'         ',
'          (AB.REN_TOTAL_ABONO - AB.REN_TOTAL_INTERESES)',
'       END ',
'  FROM CAR_RENEGOCIACION AB',
' WHERE AB.REN_ID=:P133_REN_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771288580373656412)
,p_name=>'P133_VALOR_A_PAGAR_CAJA'
,p_item_sequence=>220
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_source=>'RETURN pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p133_cxc_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771288760964665459)
,p_name=>'P133_TOTAL_FINANCIAR'
,p_item_sequence=>230
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total a Refinanciar'
,p_source=>':P133_VALOR_CAPITAL - NVL(:P133_VALOR_ABONADO,0)'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771288971303675832)
,p_name=>'P133_TASA'
,p_item_sequence=>240
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_item_default=>'RETURN ROUND(pq_car_refin_precancelacion.fn_retorna_tasa_refin(:p0_Error),4);'
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Tasa'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>15
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:14;color:BLACK"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_03=>'right'
,p_attribute_04=>'text'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771289283583685334)
,p_name=>'P133_PLAZO_APLICA'
,p_item_sequence=>250
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_item_default=>'24'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771289463730709751)
,p_name=>'P133_PLAZO'
,p_item_sequence=>260
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_prompt=>'Plazo'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TO_NUMBER(PEN_VALOR) PLAZO, TO_NUMBER(PEN_VALOR) ID',
'  FROM CAR_PAR_ENTIDADES',
' WHERE PAR_ID =',
'       PQ_CONSTANTES.FN_RETORNA_CONSTANTE(0,',
'                                          ''cn_par_id_plazos_refinanciamiento'')',
'   AND TO_NUMBER(PEN_VALOR) BETWEEN 1 AND :P133_PLAZO_APLICA',
' ORDER BY 1;'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:14;color:BLUE"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771290251641730527)
,p_name=>'P133_FECHA_CORTE'
,p_item_sequence=>270
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_prompt=>'Fecha Corte'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT C002',
'        FROM apex_collections c',
'       WHERE c.collection_name = ''COLL_FECHAS_CORTE'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_tag_attributes=>'READONLY style="font-size:14;color:BLACK"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771290672763735922)
,p_name=>'P133_VALOR_CAJA'
,p_item_sequence=>280
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_car_refin_precancelacion.pr_valida_caja(pn_cxc_id => :p133_cxc_id);'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771290981796762104)
,p_name=>'P133_SALDO_CXC_ORIGEN'
,p_item_sequence=>290
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_source=>'SELECT R.REN_TOTAL_SALDO_CXC_ANT FROM CAR_RENEGOCIACION R WHERE R.REN_ID=:P133_REN_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771291163033770851)
,p_name=>'P133_INTERESES_NO_PAG'
,p_item_sequence=>300
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Intereses No Pag'
,p_source=>'SELECT R.REN_TOTAL_INTERESES_NO_PAG FROM CAR_RENEGOCIACION R WHERE R.REN_ID=:P133_REN_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771291475960780018)
,p_name=>'P133_PUE_ID'
,p_item_sequence=>310
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771291667334784045)
,p_name=>'P133_VALIDA_PUNTO'
,p_item_sequence=>320
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771291858492788163)
,p_name=>'P133_PUE_NUM_SRI'
,p_item_sequence=>330
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771292155004835366)
,p_name=>'P133_CAR_CAPITAL'
,p_item_sequence=>340
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_prompt=>'Car Capital'
,p_source=>'SELECT R.REN_TOTAL_CAPITAL FROM CAR_RENEGOCIACION R WHERE R.REN_ID=:P133_REN_ID;'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771297672735829978)
,p_name=>'P133_PCA_ID'
,p_item_sequence=>350
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(771358878692051531)
,p_name=>'P133_TOTAL_CREDITO_REF'
,p_item_sequence=>360
,p_item_plug_id=>wwv_flow_imp.id(771358571733042048)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Total Credito'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT SUM(to_number(c005)) total_credito',
'  FROM apex_collections',
' WHERE collection_name = ''COLL_DIV_REFINANCIAMIENTO'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18;color:RED"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(7626558659844618866)
,p_name=>'P133_DIFERENCIA_ABONO_INTERES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771295157170768739)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT (AB.REN_TOTAL_ABONO - AB.REN_TOTAL_INTERESES) DIF',
'  FROM CAR_RENEGOCIACION AB',
' WHERE AB.REN_ID = :P133_REN_ID;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34303363707882094350)
,p_name=>'P133_CLAVE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(771358571733042048)
,p_prompt=>'Clave'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cAttributes=>'nowrap="nowrap"'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(34609455946506749661)
,p_name=>'P133_USER_COMISIONA'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(771358571733042048)
,p_prompt=>'Usuario comisiona'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct age.usu_id||'' - ''|| per.per_primer_apellido||'' ''||per.per_segundo_apellido||'' ''||per.per_primer_nombre||'' ''||per.per_segundo_nombre d, age.usu_id r',
'  FROM asdm_agentes age, asdm_personas per, asdm_agentes_tagentes ata',
' WHERE age.per_id = per.per_id',
'   AND age.emp_id = :f_emp_id',
'   AND per.emp_id = :f_emp_id',
'   and age.usu_id is not null',
'   AND age.age_estado_registro = 0',
'   AND per.per_estado_registro = age.age_estado_registro',
'   AND ata.ata_estado_registro = age.age_estado_registro',
'   AND age.age_id = ata.age_id',
'   AND ata.tag_id = pq_constantes.fn_retorna_constante(NULL, ''cn_tag_id_vendedor'');'))
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771078054644681293)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CAMBIA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'',
'PQ_CAR_RENEGOCIACION.pr_cambiar_estado(''COL_CXC'',',
'                                             :p133_SELECCIONADO,',
'                                             :P133_SEQ_ID,',
'                                             :p0_error );',
'',
':P133_SEQ_ID:=NULL;',
':P133_SELECCIONADO:=NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cambio'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>738824903374916367
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771047055705438590)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_cliente_existe varchar2(100);',
'ln_dir_id  asdm_direcciones.dir_id%TYPE;',
'lv_nombre_cli_referido asdm_personas.per_primer_nombre%TYPE;',
'ln_cli_id_referido asdm_clientes.cli_id%TYPE;',
'',
'',
'begin',
':P102_CALIFICACION_VALIDA:=null;',
'',
'  IF apex_collection.collection_exists(''COL_CARTERA_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CARTERA_REFIN'');',
'  ',
'  END IF;',
'',
'',
'',
'  IF apex_collection.collection_exists(''COL_CREDITOS_REFIN'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COL_CREDITOS_REFIN'');',
'  ',
'  END IF;',
'',
'',
'pq_ven_listas.pr_datos_clientes(',
':f_uge_id,',
':f_emp_id,',
':P133_IDENTIFICACION,',
':P133_NOMBRE,',
':P133_CLI_ID,',
':P133_CORREO,',
':P133_DIRECCION,',
':P133_TIPO_DIRECCION,        ',
':P133_TELEFONO,              ',
':P133_TIPO_TELEFONO,        ',
'lv_cliente_existe,',
'ln_dir_id ,',
'ln_cli_id_referido ,',
'lv_nombre_cli_referido ,',
':P133_TIPO_IDENTIFICACION,',
':P0_ERROR);',
'',
'',
'',
'  IF apex_collection.collection_exists(''COLL_DIV_REFINANCIAMIENTO'') THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name => ''COLL_DIV_REFINANCIAMIENTO'');',
'  ',
'  END IF;',
'',
'  IF apex_collection.collection_exists( pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento'')) THEN',
'  ',
'    apex_collection.delete_collection(p_collection_name =>  pq_constantes.fn_retorna_constante(NULL,',
'                                                                                       ''cv_col_nc_refinanciamiento''));',
'  ',
'  END IF;',
'',
'',
':P133_ALERTA_2 := '' '';',
'',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>738793904435673664
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771063665680712151)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CREAR REG_CREDITOS'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'PQ_CON_CONTABILIZACION_LOTE.pr_elimina_colecciones(''COL_CXC'',',
'                                                  :p0_error);',
'',
'',
'',
'',
'  pq_car_renegociacion.pr_crea_reg_creditos(pv_nombre_coleccion => ''COL_CXC'',',
'                                            pn_cli_id => :p133_cli_id,',
'                                            pn_emp_id => :F_emp_id,',
'                                            pv_error => :p0_error);',
'',
'',
':P133_CHECK:=''TODO'';',
':P133_REPORTE:=NULL;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cliente'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>738810514410947225
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771078975655829369)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_SELECCIONAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  if :P133_CHECK is null then',
'     :P133_CHECK:=''TODO'';',
'  elsif :P133_CHECK=''TODO'' then',
'     :P133_CHECK:='''';',
'  end if;',
'  PQ_CAR_RENEGOCIACION.pr_cambia_estado_completo(''COL_CXC'',',
'                                                       :P133_CHECK,',
'                                                       :p0_error);',
'    ',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(771078669291818040)
,p_internal_uid=>738825824386064443
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771270155368344741)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_CREDITOS_RENEGOCIACION'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'LN_PUE_ID number;',
'LN_PCA_ID number;',
'',
'begin',
'',
'pq_car_renegociacion.pr_cargar_datos_renegociacion(pn_user_id => :F_USER_ID,',
'                                                     pn_uge_id => :F_UGE_ID,',
'                                                     pn_emp_id => :F_EMP_ID,',
'                                                     pn_pue_id => :P133_PUE_ID,',
'                                                     pn_pca_id => :P133_PCA_ID,',
'                                                     pn_cli_id => :p133_cli_id,',
'                                                     pn_ren_id => :P133_REN_ID,',
'                                                     pv_error => :p0_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(771269972019226579)
,p_internal_uid=>739017004098579815
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771302253679590881)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CALCULA_CRONOGRAMA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_age_id_agencia     NUMBER;',
'  ld_fecha_corte        DATE;',
'  a                     NUMBER;',
'  b                     NUMBER;',
'  ln_cuota              NUMBER(15, 2) := 0;',
'  ln_cuota_minima       NUMBER;',
'  ln_plazo_cxc_original NUMBER;',
'  ln_com_id           NUMBER;',
'  lv_pue_sri      VARCHAR2(5);',
'  lv_uge_sri      VARCHAR2(5);',
'ln_int_nd_refi number;',
'  lv_estado_reg_activo  VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                          ''cv_estado_reg_activo'');',
'',
'  lv_div_estado_div_pendi VARCHAR2(1) := pq_constantes.fn_retorna_constante(0,',
'                                                                            ''cv_div_estado_div_pendiente'');',
'',
'ln_car_id number;',
'LN_INTERESES_NO_PAG number;',
'',
'BEGIN',
'',
'',
'          ',
'          ',
'          ',
'SELECT AB.REN_TOTAL_INTERESES_NO_PAG',
'INTO ln_intereses_no_pag',
'  FROM CAR_RENEGOCIACION AB',
' WHERE AB.REN_ID=:P133_REN_ID',
' AND AB.REN_ESTADO_REGISTRO=0;',
'',
'',
'',
'  ',
'    SELECT age.age_id',
'      INTO ln_age_id_agencia',
'      FROM asdm_agencias age',
'     WHERE age.uge_id = :f_uge_id;',
'  ',
'',
'',
'    ln_cuota := pq_car_refin_precancelacion.fn_valor_cuota(tasa => :p133_tasa,',
'                                                           nper => :p133_plazo,',
'                                                           va   => :P133_TOTAL_FINANCIAR);',
'',
'  ',
'',
'',
'',
'    PQ_CAR_RENEGOCIACION.pr_cronograma_refin(pn_emp_id    => :f_emp_id,',
'                                                    pn_total_ref => :P133_TOTAL_FINANCIAR,',
'                                                    pn_plazo     => :p133_plazo,',
'                                                    pn_tasa      => :p133_tasa,',
'                                                    pn_age_id    => ln_age_id_agencia,',
'                                                    pn_cli_id    => :p133_cli_id,',
'                                                    pn_tse_id    => :f_seg_id,',
'                                                    pn_meses_gracia => 0,',
'                                                    pv_error     => :p0_error);',
'  ',
'',
'----- cargo coleccion de intereses para la nueva nota de CREDITO',
'',
'',
'',
'',
'',
'   ln_com_id:=NULL;',
' ',
'',
'  SELECT pu.pue_num_sri, ug.uge_num_est_sri',
'    INTO lv_pue_sri, lv_uge_sri',
'    FROM asdm_puntos_emision pu, asdm_unidades_gestion ug',
'   WHERE pu.uge_id = ug.uge_id',
'     AND pu.pue_id = :p133_pue_id;',
'',
'',
'select ',
'SUM(to_number(c004)) Valor_Interes',
'into ln_int_nd_refi',
'from apex_collections where collection_name = ''COLL_DIV_REFINANCIAMIENTO'';',
'',
'  pq_car_refin_precancelacion.PR_CARGA_COLECCION_ND_REF(pn_cli_id        => :p133_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => NULL,',
'                                                        pn_valor_interes => ln_int_nd_refi,',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'',
'SELECT AB.REN_TOTAL_INTERESES_NO_PAG',
'INTO LN_INTERESES_NO_PAG',
'  FROM CAR_RENEGOCIACION AB',
' WHERE AB.REN_ID=:P133_REN_ID',
' AND AB.REN_ESTADO_REGISTRO=0;',
'',
'  pq_car_refin_precancelacion.pr_carga_coleccion_nc_ref(pn_cli_id        => :p133_cli_id,',
'                                                        pn_com_id        => ln_com_id,',
'                                                        pn_emp_id        => :f_emp_id,',
'                                                        pn_cxc_id        => :NULL,',
'                                                        pn_valor_interes => nvl(LN_INTERESES_NO_PAG,0),',
'                                                        pn_pue_sri       => lv_pue_sri,',
'                                                        pn_uge_sri       => lv_uge_sri,',
'                                                        pv_error         => :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(771302073325539825)
,p_internal_uid=>739049102409825955
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771362180156252680)
,p_process_sequence=>60
,p_process_point=>'BEFORE_BOX_BODY'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GENERA_REFINAN'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  -- Call the procedure',
'  ',
'  if :P133_USER_COMISIONA is not null then',
'  pq_car_renegociacion.pr_graba_renegociacion(pn_cxc_id => :p133_cxc_id,',
'                                              pn_tasa => :p133_tasa,',
'                                              pn_plazo => :p133_plazo,',
'                                              pn_total_financiar => :p133_total_financiar,',
'                                              pn_user_id => :P133_USER_COMISIONA,--:f_user_id,',
'                                              pn_emp_id => :f_emp_id,',
'                                              pn_uge_id => :f_uge_id,',
'                                              pn_cli_id => :p133_cli_id,',
'                                              pn_seg_id => :f_seg_id,',
'                                              pn_uge_id_gasto => :f_uge_id_gasto,',
'                                              pn_clave => :p133_clave,',
'                                              pn_garante => :p133_garante,',
'                                              pn_age_id_agencia => :f_age_id_agencia,',
'                                              pv_pue_num_sri => :p133_pue_num_sri,',
'                                              pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'                                              pn_pue_id => :p133_pue_id,',
'                                              pn_ren_id => :p133_ren_id,',
'                                              pn_total_credito_ref => :P133_TOTAL_CREDITO_REF,',
'                                              pv_error => :p0_error);',
'                                              ',
'                                              ',
'PQ_CON_CONTABILIZACION_LOTE.pr_elimina_colecciones(''COL_CXC'',',
'                                                  :p0_error);',
'                                                  :p133_ren_id:= null;',
'',
'',
'else',
':p0_error:= ''Debe elegir el vendedor que comisiona'';',
'end if;',
'end;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'graba'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>739109028886487754
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771284151876234332)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CANCELAR_RENEGOCIACION'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  pq_car_renegociacion.pr_eliminar_renegociacion(pn_ren_id => :p133_ren_id);',
':P133_IDENTIFICACION:=NULL;',
':p133_ren_id:=NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'ELIMINAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>739031000606469406
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(771300065678310518)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_VALIDA_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_intereses NUMBER;',
'  ln_abono     NUMBER;',
'  ln_diferencia NUMBER;',
'',
'BEGIN',
'',
'    SELECT ab.ren_total_abono, ab.ren_total_intereses',
'    INTO ln_abono, ln_intereses',
'    FROM CAR_RENEGOCIACION ab',
'   WHERE AB.REN_ID=:P133_REN_ID',
'     AND ab.ren_estado_registro =',
'         pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'');',
'         ',
'         ',
'  IF NVL(ln_abono,0) >= ln_intereses THEN',
'    ',
'    :P133_ALERTA_ABONO := NULL;',
'    ',
'  ELSE',
'    ',
'  ln_diferencia := ln_intereses - ln_abono;',
'',
'   ',
'',
'            :p133_alerta_abono := ''Para realizar el refinanciamiento debe cancelar: '' ||',
'                          ln_diferencia;    ',
'  ',
'',
'',
'',
'  END IF;',
' ',
'   ',
'EXCEPTION',
'  WHEN no_data_found THEN',
'    NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>739046914408545592
);
wwv_flow_imp.component_end;
end;
/
