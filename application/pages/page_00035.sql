prompt --application/pages/page_00035
begin
--   Manifest
--     PAGE: 00035
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>35
,p_name=>'PagoFacturaRespaldo6julio'
,p_step_title=>'PagoFacturaRespaldo6julio'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'function imprimir(url){',
'   doSubmit(''graba_mov'');',
'   window.open(url,"", "width=808, height=565, top=85");',
'		}',
'',
'</script>',
''))
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(283618362604277144)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283618565163277148)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Editar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_SEQ_ID,P30_MCD_VALOR_MOVIMIENTO,P30_TFP_ID,P30_EDE_ID,P30_CRE_TITULAR_CUENTA,P30_CRE_NRO_CHEQUE,P30_CRE_NRO_CUENTA,P30_CRE_NRO_PIN:#COL02#,#COL10#,#COL08#,#COL03#,#COL16#,#COL17#,#COL18#,#COL19#'
,p_column_linktext=>'Editar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283618679137277149)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283618776419277149)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>6
,p_column_heading=>'ede_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283618880052277149)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>7
,p_column_heading=>'Entidad Financiera'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283618979816277149)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>12
,p_column_heading=>'ptc'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619066801277149)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>13
,p_column_heading=>'ncr_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619167471277149)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>20
,p_column_heading=>'cre_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619273865277149)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>3
,p_column_heading=>'tfp_id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619369267277149)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>4
,p_column_heading=>'Forma Pago'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619477565277149)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>5
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619577151277149)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>14
,p_column_heading=>'aut_ref'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619654033277149)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>15
,p_column_heading=>'recap_lote'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619779346277149)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>16
,p_column_heading=>'valor_documento'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619855765277149)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>17
,p_column_heading=>'nro_ret'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283619971427277149)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>18
,p_column_heading=>'nro_comprobante'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620059518277149)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>8
,p_column_heading=>'Titular Cuenta'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620151862277149)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>9
,p_column_heading=>'Nro Cheque'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620273103277149)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>10
,p_column_heading=>'Nro Cuenta'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620357513277150)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>11
,p_column_heading=>'Nro Pin'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620455802277150)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>19
,p_column_heading=>'Eliminar'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:eliminar:&DEBUG.::P30_SEQ_ID,P30_MCD_VALOR_MOVIMIENTO:#COL02#,#COL10#'
,p_column_linktext=>'Eliminar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620573749277150)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620651516277150)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620769373277150)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620853170277150)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283620969990277150)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283621054411277150)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283621165474277150)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283621263781277150)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283621361597277151)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283621451644277151)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(283621581837277151)
,p_plug_name=>'Mensaje'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>2
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_plug_display_when_condition=>':P35_MENSAJE IS NOT NULL;'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(283621756239277152)
,p_plug_name=>'Agregar Forma Pago'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(283621961929277153)
,p_plug_name=>'Datos Cheque Nuevo'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'NEVER'
,p_plug_display_when_condition=>':P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque'')'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(283622178792277154)
,p_plug_name=>'Datos Guardar Factura desde Orden'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>70
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(283622382306277154)
,p_name=>'Dividendos a pagar55'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'c001 cli_id,',
'c002 com_id,',
'c003 com_numero,',
'c004 nro_ven,',
'c005 div_id,',
'c006 emp_id,',
'c007 cxc_id,',
'c008 ttr_id,',
'c009 fecha_ven,',
'c010 saldo,',
'c011 saldo_int_mor,',
'c012 int_con,',
'c013 sald_gast_cob,',
'c014 gas_cobr_cond,',
'c015 saldo_cuota,',
'c016 valor_pagar',
'',
' from apex_collections',
'where collection_name = pq_constantes.fn_retorna_constante(NULL,',
'                                                    ''cv_coleccion_pago_cuota'')'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(283622382306277154)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283622582768277155)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_column_heading=>'Cli Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283622677259277155)
,p_query_column_id=>2
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Com Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283622756527277155)
,p_query_column_id=>3
,p_column_alias=>'COM_NUMERO'
,p_column_display_sequence=>3
,p_column_heading=>'Com Numero'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283622878449277155)
,p_query_column_id=>4
,p_column_alias=>'NRO_VEN'
,p_column_display_sequence=>4
,p_column_heading=>'Nro Ven'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283622982370277155)
,p_query_column_id=>5
,p_column_alias=>'DIV_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Div Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623071320277155)
,p_query_column_id=>6
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Emp Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623159586277155)
,p_query_column_id=>7
,p_column_alias=>'CXC_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Cxc Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623259315277155)
,p_query_column_id=>8
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Ttr Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623359324277155)
,p_query_column_id=>9
,p_column_alias=>'FECHA_VEN'
,p_column_display_sequence=>9
,p_column_heading=>'Fecha Ven'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623464818277155)
,p_query_column_id=>10
,p_column_alias=>'SALDO'
,p_column_display_sequence=>10
,p_column_heading=>'Saldo'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623571761277155)
,p_query_column_id=>11
,p_column_alias=>'SALDO_INT_MOR'
,p_column_display_sequence=>11
,p_column_heading=>'Saldo Int Mor'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623674081277155)
,p_query_column_id=>12
,p_column_alias=>'INT_CON'
,p_column_display_sequence=>12
,p_column_heading=>'Int Con'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623774880277155)
,p_query_column_id=>13
,p_column_alias=>'SALD_GAST_COB'
,p_column_display_sequence=>13
,p_column_heading=>'Sald Gast Cob'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623865795277155)
,p_query_column_id=>14
,p_column_alias=>'GAS_COBR_COND'
,p_column_display_sequence=>14
,p_column_heading=>'Gas Cobr Cond'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283623981943277156)
,p_query_column_id=>15
,p_column_alias=>'SALDO_CUOTA'
,p_column_display_sequence=>15
,p_column_heading=>'Saldo Cuota'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624073364277156)
,p_query_column_id=>16
,p_column_alias=>'VALOR_PAGAR'
,p_column_display_sequence=>16
,p_column_heading=>'Valor Pagar'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(283624163403277156)
,p_name=>'preubas joha'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select * from apex_collections where collection_name=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                 ''cv_coleccion_pago_cuota'')'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624367386277157)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'COLLECTION_NAME'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624481924277157)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624581603277157)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624657661277157)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624770986277157)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624867339277157)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283624976889277157)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625080871277157)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625176447277157)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625261198277157)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625352373277157)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625460821277157)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625562883277157)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625664774277157)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625761607277157)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625872252277157)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283625965521277158)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626053623277158)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626166569277158)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626377273277165)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626454547277165)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626578875277165)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626668346277165)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626777076277165)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626860845277165)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626952482277165)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627058833277166)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627177513277166)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627283809277166)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627383906277166)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627473335277166)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627567776277166)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627665026277166)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627783354277166)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627857278277166)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283627972164277166)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628078532277166)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628162624277166)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628270281277166)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628369119277166)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628455851277166)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628572093277166)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628661527277166)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283626283173277158)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629173652277167)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629261633277167)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629376301277167)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629478090277167)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629572780277167)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629659978277167)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628756498277167)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628852787277167)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283628980683277167)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'CLOB001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(283629063502277167)
,p_query_column_id=>54
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'MD5_ORIGINAL'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(283629758063277167)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(283618362604277144)
,p_button_name=>'BT_REALIZAR_PAGO'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537277779046677)
,p_button_image_alt=>'Grabar/Imprimir'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:imprimir(''../../reports/rwservlet?module=FACTURA_MINOREO.rdf&userid=asdm_p/asdm_p@mxpdm&destype=cache&desformat=pdf&pn_emp_id=&F_EMP_ID.&pn_com_id=&P35_COM_ID.'');'
,p_button_condition=>'(:P35_VALOR_TOTAL_PAGOS = :P35_SUM_PAGOS) AND (:P35_VALOR_TOTAL_PAGOS > 0)'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(283630172720277169)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'to_number(:P35_VALOR_TOTAL_PAGOS) > to_number(:P35_SUM_PAGOS)'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(283629959494277168)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(283618362604277144)
,p_button_name=>'LIMPIA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Limpia'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(283637658783277209)
,p_branch_action=>'f?p=&APP_ID.:19:&SESSION.::&DEBUG.:35::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'graba_mov'
,p_branch_comment=>'Created 31-MAY-2010 12:00 by ADMIN'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(283638064958277209)
,p_branch_action=>'f?p=&APP_ID.:35:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(283630172720277169)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(283637883140277209)
,p_branch_action=>'f?p=&APP_ID.:35:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>30
,p_branch_comment=>'Created 01-MAR-2010 11:11 by ONARANJO'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283630353441277169)
,p_name=>'P35_CLI_ID'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283630566719277174)
,p_name=>'P35_CLI_NOMBRE'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283630754409277176)
,p_name=>'P35_TFP_ID'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_FORMAS_PAGO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(8000);',
'BEGIN',
'        lv_lov := pq_ven_listas_caja.fn_lov_formas_pago',
'(',
':F_EMP_ID,',
':P0_TTR_ID,',
':P30_POLITICA_VENTA);',
'',
'return (lv_lov);',
'',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283630961663277176)
,p_name=>'P35_MCD_VALOR_MOVIMIENTO'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'TO_NUMBER(:P35_VALOR_TOTAL_PAGOS) > TO_NUMBER(:P35_SUM_PAGOS) '
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283631182631277178)
,p_name=>'P35_EDE_ID'
,p_item_sequence=>66
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Entidad Destino'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENTIDADES_DESTINO_FINANCIERAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ede.ede_descripcion,',
'       ede.ede_id',
'FROM   asdm_entidades_destinos ede',
'WHERE  ede.ede_tipo = ''EFI''',
'and ede.emp_id = :f_emp_id',
'and ede.ede_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'union',
'select ''-- Seleccione Entidad --'' d, null r from dual',
'order by 1 '))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283631355658277179)
,p_name=>'P35_CRE_NRO_CHEQUE'
,p_item_sequence=>68
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Cheque'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283631551957277179)
,p_name=>'P35_CRE_NRO_CUENTA'
,p_item_sequence=>69
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Cuenta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283631781949277179)
,p_name=>'P35_CRE_NRO_PIN'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nro Pin'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283631982702277179)
,p_name=>'P35_FACTURAR'
,p_item_sequence=>112
,p_item_plug_id=>wwv_flow_imp.id(283622178792277154)
,p_prompt=>'Facturar'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283632176602277187)
,p_name=>'P35_COM_ID'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_imp.id(283622178792277154)
,p_prompt=>'Com Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283632362540277188)
,p_name=>'P35_TRX_ID'
,p_item_sequence=>132
,p_item_plug_id=>wwv_flow_imp.id(283622178792277154)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283632561742277188)
,p_name=>'P35_SUM_PAGOS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Sum Pagos'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(TO_NUMBER(c006)),0) suma_valor',
'  FROM apex_collections',
'WHERE collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283632757358277188)
,p_name=>'P35_VALOR_ANTICIPO_CLIENTES'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Disponible Anticipos'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'')'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283632958063277189)
,p_name=>'P35_ORD_ID'
,p_item_sequence=>102
,p_item_plug_id=>wwv_flow_imp.id(283622178792277154)
,p_prompt=>'Ord Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283633176452277189)
,p_name=>'P35_VALOR_TOTAL_PAGOS'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor a Pagar'
,p_format_mask=>'999G999G999G999G990D00'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'return pq_ven_listas_caja.fn_ret_valor_cuota0(:P35_ORD_ID, :p35_facturar,:P35_VALOR_TOTAL_PAGOS);',
'',
'',
'end;'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283633356117277189)
,p_name=>'P35_CRE_TITULAR_CUENTA'
,p_item_sequence=>67
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Titular Cuenta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283633562444277189)
,p_name=>'P35_CRE_FECHA_DEPOSITO'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_prompt=>unistr('Fecha Dep\00BF\00BFsito')
,p_source=>'sysdate'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283633776669277189)
,p_name=>'P35_CRE_ID'
,p_item_sequence=>64
,p_item_plug_id=>wwv_flow_imp.id(283621961929277153)
,p_prompt=>'Cre Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283633960940277190)
,p_name=>'P35_SEQ_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283634169134277190)
,p_name=>'P35_MENSAJE'
,p_item_sequence=>92
,p_item_plug_id=>wwv_flow_imp.id(283621581837277151)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283634367751277190)
,p_name=>'P35_CLI_IDENTIFICACION'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(283621756239277152)
,p_prompt=>unistr('Identificaci\00BF\00BFn Cliente')
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(283634571749277190)
,p_name=>'P35_ERROR'
,p_item_sequence=>142
,p_item_plug_id=>wwv_flow_imp.id(283622178792277154)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283634872542277191)
,p_validation_name=>'valida_mcd_valor_movimiento'
,p_validation_sequence=>10
,p_validation=>'NVL(:P35_MCD_VALOR_MOVIMIENTO,0) <> 0;'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Especifique un valor'
,p_validation_condition=>'carga_detalle'
,p_validation_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_associated_item=>wwv_flow_imp.id(283630961663277176)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283635653576277201)
,p_validation_name=>'valida_anticipo_cliente'
,p_validation_sequence=>10
,p_validation=>'TO_NUMBER(:P35_VALOR_ANTICIPO_CLIENTES) >= TO_NUMBER(:P35_MCD_VALOR_MOVIMIENTO);'
,p_validation2=>'PLSQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'El valor debe ser menor o igual al Saldo de Anticipo Cliente'
,p_validation_condition=>':P35_TFP_ID = pq_constantes.fn_cn_tfp_id_anticipo_clientes AND :P35_MCD_VALOR_MOVIMIENTO IS NOT NULL;'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(283630961663277176)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283635053898277201)
,p_validation_name=>'P35_EDE_ID'
,p_validation_sequence=>20
,p_validation=>'P35_EDE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione Una Entidad'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(283631182631277178)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283636082621277203)
,p_validation_name=>'P35_CRE_TITULAR_CUENTA'
,p_validation_sequence=>30
,p_validation=>'P35_CRE_TITULAR_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Titular de la cuenta'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(283633356117277189)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283635881386277202)
,p_validation_name=>'P35_CRE_NRO_CHEQUE'
,p_validation_sequence=>40
,p_validation=>'P35_CRE_NRO_CHEQUE'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cheque'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(283631355658277179)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283635276933277201)
,p_validation_name=>'P35_CRE_NRO_CUENTA'
,p_validation_sequence=>50
,p_validation=>'P35_CRE_NRO_CUENTA'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese Nro Cuenta'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(283631551957277179)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283635467127277201)
,p_validation_name=>'P35_CRE_NRO_PIN'
,p_validation_sequence=>60
,p_validation=>'P35_CRE_NRO_PIN'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Ingrese el Nro de Pin'
,p_validation_condition=>'(:REQUEST = ''carga_detalle'') AND (:P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_cheque''))'
,p_validation_condition2=>'PLSQL'
,p_validation_condition_type=>'EXPRESSION'
,p_associated_item=>wwv_flow_imp.id(283631781949277179)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(283636279879277203)
,p_validation_name=>'P35_TFP_ID'
,p_validation_sequence=>70
,p_validation=>'P35_TFP_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione una forma de pago'
,p_when_button_pressed=>wwv_flow_imp.id(283630172720277169)
,p_associated_item=>wwv_flow_imp.id(283630754409277176)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(283636959118277205)
,p_process_sequence=>1
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_BUSCA_DATOS_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
':P0_TTR_DESCRIPCION := NULL;',
':P0_DATFOLIO := NULL;',
':P0_FOL_SEC_ACTUAL := NULL;',
':P0_PERIODO := null;',
':P0_TTR_ID := pq_ven_listas_caja.fn_tipo_trans_factura_seg(:f_seg_id);',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:F_EMP_ID,',
'                                                        :F_PCA_ID,',
'                                                        :P0_TTR_ID,',
'                                                        :P0_TTR_DESCRIPCION, ',
'                                                        :P0_PERIODO,',
'                                                        :P0_DATFOLIO,',
'                                                        :P0_FOL_SEC_ACTUAL,',
'                                                        :P0_PUE_NUM_SRI,',
'                                                        :P0_UGE_NUM_EST_SRI,',
'                                                        :P0_PUE_ID,',
'                                                        :P0_ERROR);',
'',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>251383807848512279
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(283636377718277203)
,p_process_sequence=>60
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_linea_coleccion_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
' lv_nombre_coleccion VARCHAR2(50);',
'BEGIN',
'',
'',
'',
'IF :P35_TFP_ID_ELIMINAR =  pq_constantes.fn_retorna_constante(:F_EMP_ID,                                                                                        ''cn_tfp_id_anticipo_clientes'')',
'THEN',
'   :P35_VALOR_ANTICIPO_CLIENTES := :P35_VALOR_ANTICIPO_CLIENTES + :P35_MCD_VALOR_MOVIMIENTO;',
'END IF;',
'',
'lv_nombre_coleccion := pq_constantes.fn_retorna_constante(NULL,',
'                                                              ''cv_coleccion_mov_caja'');',
'',
'pq_inv_movimientos.pr_borra_reg_colecciones(:p35_seq_id ,lv_nombre_coleccion);',
'',
':P35_SEQ_ID := NULL;',
':P35_MCD_VALOR_MOVIMIENTO := NULL;',
':P35_TFP_ID_ELIMINAR := NULL;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>251383226448512277
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(283637156514277208)
,p_process_sequence=>70
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_FACTURA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'pq_ven_comprobantes.pr_graba_comprobante_orden(:f_emp_id,',
'                                                :f_uge_id,',
'                                                :F_UGE_ID_GASTO,',
'                                                :f_user_id,',
'                                                :p0_ttr_id,',
'                                                :P35_COM_ID,',
'                                                :P35_TRX_ID,',
'                                                :P0_PUE_ID,',
'                                                NULL,',
'                                                SYSDATE,',
'                                                :P0_FOL_SEC_ACTUAL,',
'                                                :P0_UGE_NUM_EST_SRI  ,',
'                                                :P0_PUE_NUM_SRI,',
'                                                null,-- pn_age_id_agente',
'                                                :F_AGE_ID_AGENCIA,',
'                                                :p35_ORD_ID,',
'                                                NULL,--OBS',
'                                                NULL,',
'                                                0,',
'                                                :F_SEG_ID,',
'                                                :P0_ERROR);'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(283629758063277167)
,p_process_when=>':P35_FACTURAR = ''S'' AND :P35_COM_ID IS NULL  '
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>251384005244512282
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(283636772806277205)
,p_process_sequence=>80
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_colec_cuota_cero'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'',
'pq_ven_pagos_cuota.pr_carga_colec_cuota_cero(:P35_CLI_ID,:f_emp_id,:P35_com_id,:p0_error);'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P35_FACTURAR '
,p_process_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_process_when2=>'S'
,p_internal_uid=>251383621536512279
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(283637363550277208)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion_boton'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'if :p0_error is null then',
'',
'IF :P35_TFP_ID = pq_constantes.fn_retorna_constante(:F_EMP_ID,''cn_tfp_id_anticipo_clientes'') THEN',
'   :P35_VALOR_ANTICIPO_CLIENTES := :P35_VALOR_ANTICIPO_CLIENTES - :P35_MCD_VALOR_MOVIMIENTO;',
'END IF;',
'pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(:P35_EDE_ID,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    :P35_CRE_ID,',
'                                                    :P35_TFP_ID,',
'                                                    :P35_MCD_VALOR_MOVIMIENTO,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    NULL,',
'                                                    :P35_CRE_TITULAR_CUENTA,',
'                                                    :P35_CRE_NRO_CHEQUE,',
'                                                    :P35_CRE_NRO_CUENTA,',
'                                                    :P35_CRE_NRO_PIN,',
'                                                    :P0_ERROR);',
'',
':P35_SEQ_ID := NULL;',
':P35_MCD_VALOR_MOVIMIENTO := NULL;',
':P35_CRE_ID := NULL;',
':P35_CRE_FECHA_DEPOSITO := SYSDATE;',
':P35_EDE_ID := NULL;',
':P35_CRE_TITULAR_CUENTA := NULL;',
':P35_CRE_NRO_CHEQUE := NULL;',
':P35_CRE_NRO_CUENTA := NULL;',
':P35_CRE_NRO_PIN := NULL;',
'end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(283630172720277169)
,p_internal_uid=>251384212280512282
,p_process_comment=>unistr('Se ejecuta con el submit creado en el screep del head de la p\00BF\00BFgina, este screep se ejecuta al dar click en el boton grabar, envia el url para imprimir la factura')
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(283636562622277205)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_pago_cuota'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'if :p0_error is null then',
'pq_ven_pagos_cuota.pr_graba_pago_cuota(:F_EMP_ID,',
'                                       :F_UGE_ID,',
'                                       :F_UGE_ID_GASTO,',
'                                       :F_USER_ID,',
'                                       :F_PCA_ID,',
'                                       :P35_AGE_ID_GESTIONADO_POR,',
'                                       :P35_CLI_ID,',
'                                       :P35_SUM_PAGOS,',
'                                       :P0_ERROR);',
'',
'',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'',
'end if;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'graba_mov'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'Pago Cuota Grabado'
,p_internal_uid=>251383411352512279
);
null;
wwv_flow_imp.component_end;
end;
/
