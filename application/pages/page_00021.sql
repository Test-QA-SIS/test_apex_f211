prompt --application/pages/page_00021
begin
--   Manifest
--     PAGE: 00021
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>21
,p_name=>'DEFINICION_NOTA_CREDITO'
,p_step_title=>'DEFINICION_NOTA_CREDITO'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(244931067966303020)
,p_plug_name=>'ADICIONALES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>65
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(244931266095303024)
,p_name=>'COL_AUT'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select seq_id, c001, c002, c003, c004, c005 from apex_collections',
'where collection_name = ''CO_AUTORIZACIONES'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244931455969303028)
,p_query_column_id=>1
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>1
,p_column_heading=>'SEQ_ID'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244931573268303034)
,p_query_column_id=>2
,p_column_alias=>'C001'
,p_column_display_sequence=>2
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244931663794303034)
,p_query_column_id=>3
,p_column_alias=>'C002'
,p_column_display_sequence=>3
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244931780611303035)
,p_query_column_id=>4
,p_column_alias=>'C003'
,p_column_display_sequence=>4
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244931853301303035)
,p_query_column_id=>5
,p_column_alias=>'C004'
,p_column_display_sequence=>5
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244931978184303035)
,p_query_column_id=>6
,p_column_alias=>'C005'
,p_column_display_sequence=>6
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(244932058436303035)
,p_name=>'CO_VARIABLES_DETALLE'
,p_template=>wwv_flow_imp.id(270520665472046666)
,p_display_sequence=>90
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_03'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_DETALLE'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Bajar Excel'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(244932058436303035)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932279696303035)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932365190303035)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932461642303035)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932571010303035)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932676529303035)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932780697303035)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932868828303035)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244932962351303036)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933073828303036)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933169023303036)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933254231303036)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933363391303036)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933470683303036)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933573103303036)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933660144303036)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933778067303036)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933879687303036)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244933962884303036)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934077293303036)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934179222303036)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934259137303036)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934371542303036)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934457577303036)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934571896303036)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934674683303036)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934772955303037)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934873827303037)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244934983722303037)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935070887303037)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935158143303037)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935280782303037)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935356826303037)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935458598303037)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935563906303037)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935658580303037)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935758000303037)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935865843303037)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244935954323303041)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936058540303041)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937258466303042)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937372624303042)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937463056303042)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937574103303042)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936160649303041)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936268304303041)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936363691303041)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936480542303041)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936575731303041)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936666289303041)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936775595303041)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936855443303041)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244936981801303041)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937065511303042)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937179449303042)
,p_query_column_id=>54
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(244937676696303042)
,p_name=>'CO_DETALLE'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>110
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM apex_collections',
'WHERE collection_name = ''CO_DETALLE'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(244937676696303042)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937865681303042)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244937957981303042)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938066662303042)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938179240303042)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938262361303042)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938356868303042)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938465466303042)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938573620303043)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938665294303043)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938760999303043)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938882622303048)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244938977754303048)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939078099303049)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939171012303049)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939279499303049)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939351554303049)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939461919303054)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939562942303054)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939667171303054)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939777926303054)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939868922303054)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244939969803303054)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940065875303054)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940183554303054)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940268409303054)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940360480303054)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940469897303054)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940577375303054)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940678906303054)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940777753303054)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940882419303054)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244940966043303055)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941056354303055)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941165101303055)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941271834303055)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941367258303055)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941453462303055)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941573458303055)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941677886303055)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941768370303055)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941867576303055)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244941980972303055)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942057010303055)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942156211303055)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942259363303055)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942351420303055)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942455673303055)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942563894303055)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942683945303055)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942754118303055)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942871713303056)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244942965608303056)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244943066508303056)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244943155203303056)
,p_query_column_id=>54
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(244943261408303056)
,p_plug_name=>'<B>&P0_TTR_DESCRIPCION.   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'function pull_multi_value(pValue){',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ''APPLICATION_PROCESS=P_BUSCA_PERSONAS'',0);',
'    if(pValue)',
'    {',
'        get.add(''F_PER_ID'',pValue)',
'    }else',
'    {',
'        get.add(''F_PER_ID'',''null'')',
'    }    ',
'    gReturn = get.get(''XML'');',
'    if(gReturn)',
'    {',
'            var l_Count = gReturn.getElementsByTagName("item").length;',
'            for(var i = 0;i<l_Count;i++)',
'            {',
'             var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'              var l_ID = l_Opt_Xml.getAttribute(''id'');',
'               var l_El = html_GetElement(l_ID);   ',
'                ',
'                if(l_Opt_Xml.firstChild)',
'                {',
'                var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'                 }',
'                 else',
'                 {',
'                 var l_Value = '''';',
'                 }',
'',
'                 if(l_El)',
'                 {',
'                 if(l_El.tagName == ''INPUT'')',
'                 {',
'                  l_El.value = l_Value;',
'                  }else',
'                   if(l_El.tagName == ''SPAN'' && l_El.className == ''grabber'')',
'                      {',
'                      l_El.parentNode.innerHTML = l_Value;',
'                      l_El.parentNode.id = l_ID;',
'                      }',
'                   else{',
'                       l_El.innerHTML = l_Value;',
'                       }',
'                       ',
'                  }',
'             }',
'    ',
'     }',
'     ',
'    get = null;',
'    ',
'    var ln_existe = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=dummy'',0);',
'   ',
'    g_existe = ln_existe.get();',
'    g_existe = ln_existe.get(''XML'');',
'',
' if ($x(''P21_CLIENTE_EXISTE'').value=="N")',
' {',
'javascript:popUp2(''http://oas.pdm:7777/pls/apex/f?p=&APP_ID.:201:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_'
||'ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,,1,1,S'', 650, 650);',
'  }',
'doSubmit();',
'}',
'',
'</script>'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(244943458358303058)
,p_name=>'DetalleFactura'
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_07'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_detalle;'
,p_display_when_condition=>'P21_POL_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>50
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(244943458358303058)
,p_plug_column_width=>'valign=top'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945979841303062)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244946067711303062)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:17:&SESSION.::&DEBUG.::P17_SEQ_ID,P17_AGE_ID_AGENTE,P17_IEB_ID,P17_ITE_SKU_ID,P17_DESCRIPCION_LARGA,P17_ESTADO,P17_ESTADO_DES,P17_CANTIDAD:#COL03#,#COL04#,#COL05#,#COL06#,#COL07#,#COL08#,#COL09#,#COL10#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244946163808303062)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244946282876303062)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244946380220303062)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244946464385303062)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244946571215303062)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244943681531303060)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244943755080303060)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244943874861303060)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244943963387303060)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944077838303060)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944183606303060)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944259938303060)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_column_alignment=>'RIGHT'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944358535303061)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944472451303061)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944572451303061)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944670992303061)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944765810303061)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944883131303061)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244944966473303061)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945062177303061)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945177730303061)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945268912303061)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945372654303061)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945467124303061)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945579480303061)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945666040303061)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945773971303061)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244945875829303061)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(244946676585303062)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(244946858599303062)
,p_name=>'CO_VARIABLES_ENTRADA'
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_ENTRADA'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947055238303063)
,p_query_column_id=>1
,p_column_alias=>'COLLECTION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'Collection Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947165400303063)
,p_query_column_id=>2
,p_column_alias=>'SEQ_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Seq Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>5
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947282749303063)
,p_query_column_id=>3
,p_column_alias=>'C001'
,p_column_display_sequence=>3
,p_column_heading=>'C001'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947361583303063)
,p_query_column_id=>4
,p_column_alias=>'C002'
,p_column_display_sequence=>4
,p_column_heading=>'C002'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947454306303063)
,p_query_column_id=>5
,p_column_alias=>'C003'
,p_column_display_sequence=>5
,p_column_heading=>'C003'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947570206303063)
,p_query_column_id=>6
,p_column_alias=>'C004'
,p_column_display_sequence=>6
,p_column_heading=>'C004'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947665338303063)
,p_query_column_id=>7
,p_column_alias=>'C005'
,p_column_display_sequence=>7
,p_column_heading=>'C005'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947755351303063)
,p_query_column_id=>8
,p_column_alias=>'C006'
,p_column_display_sequence=>8
,p_column_heading=>'C006'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947865708303063)
,p_query_column_id=>9
,p_column_alias=>'C007'
,p_column_display_sequence=>9
,p_column_heading=>'C007'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244947982039303063)
,p_query_column_id=>10
,p_column_alias=>'C008'
,p_column_display_sequence=>10
,p_column_heading=>'C008'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948059966303063)
,p_query_column_id=>11
,p_column_alias=>'C009'
,p_column_display_sequence=>11
,p_column_heading=>'C009'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948154695303063)
,p_query_column_id=>12
,p_column_alias=>'C010'
,p_column_display_sequence=>12
,p_column_heading=>'C010'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948251518303063)
,p_query_column_id=>13
,p_column_alias=>'C011'
,p_column_display_sequence=>13
,p_column_heading=>'C011'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948356481303063)
,p_query_column_id=>14
,p_column_alias=>'C012'
,p_column_display_sequence=>14
,p_column_heading=>'C012'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948480349303063)
,p_query_column_id=>15
,p_column_alias=>'C013'
,p_column_display_sequence=>15
,p_column_heading=>'C013'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948582649303063)
,p_query_column_id=>16
,p_column_alias=>'C014'
,p_column_display_sequence=>16
,p_column_heading=>'C014'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948663277303064)
,p_query_column_id=>17
,p_column_alias=>'C015'
,p_column_display_sequence=>17
,p_column_heading=>'C015'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948781315303064)
,p_query_column_id=>18
,p_column_alias=>'C016'
,p_column_display_sequence=>18
,p_column_heading=>'C016'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948875262303064)
,p_query_column_id=>19
,p_column_alias=>'C017'
,p_column_display_sequence=>19
,p_column_heading=>'C017'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244948953889303064)
,p_query_column_id=>20
,p_column_alias=>'C018'
,p_column_display_sequence=>20
,p_column_heading=>'C018'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949069194303064)
,p_query_column_id=>21
,p_column_alias=>'C019'
,p_column_display_sequence=>21
,p_column_heading=>'C019'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949183713303064)
,p_query_column_id=>22
,p_column_alias=>'C020'
,p_column_display_sequence=>22
,p_column_heading=>'C020'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949266040303064)
,p_query_column_id=>23
,p_column_alias=>'C021'
,p_column_display_sequence=>23
,p_column_heading=>'C021'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949351931303064)
,p_query_column_id=>24
,p_column_alias=>'C022'
,p_column_display_sequence=>24
,p_column_heading=>'C022'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949474009303064)
,p_query_column_id=>25
,p_column_alias=>'C023'
,p_column_display_sequence=>25
,p_column_heading=>'C023'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949574052303064)
,p_query_column_id=>26
,p_column_alias=>'C024'
,p_column_display_sequence=>26
,p_column_heading=>'C024'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949653997303064)
,p_query_column_id=>27
,p_column_alias=>'C025'
,p_column_display_sequence=>27
,p_column_heading=>'C025'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949777497303064)
,p_query_column_id=>28
,p_column_alias=>'C026'
,p_column_display_sequence=>28
,p_column_heading=>'C026'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949863568303064)
,p_query_column_id=>29
,p_column_alias=>'C027'
,p_column_display_sequence=>29
,p_column_heading=>'C027'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244949977481303064)
,p_query_column_id=>30
,p_column_alias=>'C028'
,p_column_display_sequence=>30
,p_column_heading=>'C028'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950057190303064)
,p_query_column_id=>31
,p_column_alias=>'C029'
,p_column_display_sequence=>31
,p_column_heading=>'C029'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950165740303064)
,p_query_column_id=>32
,p_column_alias=>'C030'
,p_column_display_sequence=>32
,p_column_heading=>'C030'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950276179303064)
,p_query_column_id=>33
,p_column_alias=>'C031'
,p_column_display_sequence=>33
,p_column_heading=>'C031'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950367593303064)
,p_query_column_id=>34
,p_column_alias=>'C032'
,p_column_display_sequence=>34
,p_column_heading=>'C032'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950462511303064)
,p_query_column_id=>35
,p_column_alias=>'C033'
,p_column_display_sequence=>35
,p_column_heading=>'C033'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950568914303065)
,p_query_column_id=>36
,p_column_alias=>'C034'
,p_column_display_sequence=>36
,p_column_heading=>'C034'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950663855303065)
,p_query_column_id=>37
,p_column_alias=>'C035'
,p_column_display_sequence=>37
,p_column_heading=>'C035'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950769128303065)
,p_query_column_id=>38
,p_column_alias=>'C036'
,p_column_display_sequence=>38
,p_column_heading=>'C036'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950855925303065)
,p_query_column_id=>39
,p_column_alias=>'C037'
,p_column_display_sequence=>39
,p_column_heading=>'C037'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244950958689303065)
,p_query_column_id=>40
,p_column_alias=>'C038'
,p_column_display_sequence=>40
,p_column_heading=>'C038'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951074901303065)
,p_query_column_id=>41
,p_column_alias=>'C039'
,p_column_display_sequence=>41
,p_column_heading=>'C039'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951155938303065)
,p_query_column_id=>42
,p_column_alias=>'C040'
,p_column_display_sequence=>42
,p_column_heading=>'C040'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951280671303065)
,p_query_column_id=>43
,p_column_alias=>'C041'
,p_column_display_sequence=>43
,p_column_heading=>'C041'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951363890303065)
,p_query_column_id=>44
,p_column_alias=>'C042'
,p_column_display_sequence=>44
,p_column_heading=>'C042'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951460504303065)
,p_query_column_id=>45
,p_column_alias=>'C043'
,p_column_display_sequence=>45
,p_column_heading=>'C043'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951573266303065)
,p_query_column_id=>46
,p_column_alias=>'C044'
,p_column_display_sequence=>46
,p_column_heading=>'C044'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951660265303065)
,p_query_column_id=>47
,p_column_alias=>'C045'
,p_column_display_sequence=>47
,p_column_heading=>'C045'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951765106303065)
,p_query_column_id=>48
,p_column_alias=>'C046'
,p_column_display_sequence=>48
,p_column_heading=>'C046'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951866152303065)
,p_query_column_id=>49
,p_column_alias=>'C047'
,p_column_display_sequence=>49
,p_column_heading=>'C047'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244951979525303065)
,p_query_column_id=>50
,p_column_alias=>'C048'
,p_column_display_sequence=>50
,p_column_heading=>'C048'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952057031303065)
,p_query_column_id=>51
,p_column_alias=>'C049'
,p_column_display_sequence=>51
,p_column_heading=>'C049'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952167013303065)
,p_query_column_id=>52
,p_column_alias=>'C050'
,p_column_display_sequence=>52
,p_column_heading=>'C050'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952277776303065)
,p_query_column_id=>53
,p_column_alias=>'CLOB001'
,p_column_display_sequence=>53
,p_column_heading=>'Clob001'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952367678303066)
,p_query_column_id=>54
,p_column_alias=>'MD5_ORIGINAL'
,p_column_display_sequence=>54
,p_column_heading=>'Md5 Original'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(244952480377303066)
,p_name=>'TOTALES'
,p_display_sequence=>80
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'REGION_POSITION_04'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>'return pq_ven_comunes.fn_mostrar_coleccion_totales;'
,p_display_when_condition=>'P21_POL_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div style="width:210px">',
''))
,p_footer=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>60
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>15
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.set_region_column_width(
 p_id=>wwv_flow_imp.id(244952480377303066)
,p_plug_column_width=>' valign="top"'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953866973303068)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953968845303068)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954055630303068)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_column_format=>'999G999G999G999G990D00'
,p_column_alignment=>'RIGHT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954151466303068)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954271773303068)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954362005303068)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954480122303068)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954675243303068)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954753248303068)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954857524303068)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955181753303068)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954957790303068)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955073634303068)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955265450303068)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955366888303068)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955459996303068)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955573092303068)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955654882303069)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955771536303069)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955873018303069)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244955978904303069)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956054059303069)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956183694303069)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956261104303069)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956362752303069)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956476483303069)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956575677303069)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956664468303069)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956758516303069)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956880202303069)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244956976638303069)
,p_query_column_id=>31
,p_column_alias=>'COL31'
,p_column_display_sequence=>31
,p_column_heading=>'Col31'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957054759303069)
,p_query_column_id=>32
,p_column_alias=>'COL32'
,p_column_display_sequence=>32
,p_column_heading=>'Col32'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957169523303069)
,p_query_column_id=>33
,p_column_alias=>'COL33'
,p_column_display_sequence=>33
,p_column_heading=>'Col33'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957279719303069)
,p_query_column_id=>34
,p_column_alias=>'COL34'
,p_column_display_sequence=>34
,p_column_heading=>'Col34'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957368658303069)
,p_query_column_id=>35
,p_column_alias=>'COL35'
,p_column_display_sequence=>35
,p_column_heading=>'Col35'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957466084303070)
,p_query_column_id=>36
,p_column_alias=>'COL36'
,p_column_display_sequence=>36
,p_column_heading=>'Col36'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957576037303070)
,p_query_column_id=>37
,p_column_alias=>'COL37'
,p_column_display_sequence=>37
,p_column_heading=>'Col37'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957669560303070)
,p_query_column_id=>38
,p_column_alias=>'COL38'
,p_column_display_sequence=>38
,p_column_heading=>'Col38'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244954557256303068)
,p_query_column_id=>39
,p_column_alias=>'COL39'
,p_column_display_sequence=>39
,p_column_heading=>'Col39'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957758259303070)
,p_query_column_id=>40
,p_column_alias=>'COL40'
,p_column_display_sequence=>40
,p_column_heading=>'Col40'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957855241303070)
,p_query_column_id=>41
,p_column_alias=>'COL41'
,p_column_display_sequence=>41
,p_column_heading=>'Col41'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244957976336303070)
,p_query_column_id=>42
,p_column_alias=>'COL42'
,p_column_display_sequence=>42
,p_column_heading=>'Col42'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244958057472303070)
,p_query_column_id=>43
,p_column_alias=>'COL43'
,p_column_display_sequence=>43
,p_column_heading=>'Col43'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244958160179303070)
,p_query_column_id=>44
,p_column_alias=>'COL44'
,p_column_display_sequence=>44
,p_column_heading=>'Col44'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244958259492303070)
,p_query_column_id=>45
,p_column_alias=>'COL45'
,p_column_display_sequence=>45
,p_column_heading=>'Col45'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244958370264303070)
,p_query_column_id=>46
,p_column_alias=>'COL46'
,p_column_display_sequence=>46
,p_column_heading=>'Col46'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244958462860303070)
,p_query_column_id=>47
,p_column_alias=>'COL47'
,p_column_display_sequence=>47
,p_column_heading=>'Col47'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244958574296303070)
,p_query_column_id=>48
,p_column_alias=>'COL48'
,p_column_display_sequence=>48
,p_column_heading=>'Col48'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952664795303067)
,p_query_column_id=>49
,p_column_alias=>'COL49'
,p_column_display_sequence=>49
,p_column_heading=>'Col49'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952754062303067)
,p_query_column_id=>50
,p_column_alias=>'COL50'
,p_column_display_sequence=>50
,p_column_heading=>'Col50'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952859723303067)
,p_query_column_id=>51
,p_column_alias=>'COL51'
,p_column_display_sequence=>51
,p_column_heading=>'Col51'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244952982795303067)
,p_query_column_id=>52
,p_column_alias=>'COL52'
,p_column_display_sequence=>52
,p_column_heading=>'Col52'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953063351303067)
,p_query_column_id=>53
,p_column_alias=>'COL53'
,p_column_display_sequence=>53
,p_column_heading=>'Col53'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953170195303067)
,p_query_column_id=>54
,p_column_alias=>'COL54'
,p_column_display_sequence=>54
,p_column_heading=>'Col54'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953275624303067)
,p_query_column_id=>55
,p_column_alias=>'COL55'
,p_column_display_sequence=>55
,p_column_heading=>'Col55'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953377007303067)
,p_query_column_id=>56
,p_column_alias=>'COL56'
,p_column_display_sequence=>56
,p_column_heading=>'Col56'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953462269303067)
,p_query_column_id=>57
,p_column_alias=>'COL57'
,p_column_display_sequence=>57
,p_column_heading=>'Col57'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953555899303067)
,p_query_column_id=>58
,p_column_alias=>'COL58'
,p_column_display_sequence=>58
,p_column_heading=>'Col58'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953663409303067)
,p_query_column_id=>59
,p_column_alias=>'COL59'
,p_column_display_sequence=>59
,p_column_heading=>'Col59'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(244953761949303067)
,p_query_column_id=>60
,p_column_alias=>'COL60'
,p_column_display_sequence=>60
,p_column_heading=>'Col60'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(244958656928303070)
,p_plug_name=>'INSERTA ARTICULOS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_07'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_plug_display_condition_type=>'ITEM_IS_NULL'
,p_plug_display_when_condition=>'P21_COM_ID'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<!--',
'',
'function pull_multi_value2(pValue){',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ''APPLICATION_PROCESS=P_BUSCA_ITEM'',0);',
'if(pValue){',
'get.add(''F_ITE_SKU_ID'',pValue)',
'}else{',
'get.add(''F_ITE_SKU_ID'',''null'')',
'}    ',
'',
'    gReturn = get.get(''XML'');',
'',
'    if(gReturn){',
'        var l_Count = gReturn.getElementsByTagName("item").length;',
'        for(var i = 0;i<l_Count;i++){',
'            var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'            var l_ID = l_Opt_Xml.getAttribute(''id'');',
'            var l_El = html_GetElement(l_ID);    ',
'            if(l_Opt_Xml.firstChild){',
'                var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'            }else{',
'                var l_Value = '''';',
'            }',
'',
'            if(l_El){',
'                if(l_El.tagName == ''INPUT''){',
'                    l_El.value = l_Value;',
'                }else if(l_El.tagName == ''SPAN'' && ',
'                l_El.className == ''grabber''){',
'                    l_El.parentNode.innerHTML = l_Value;',
'                    l_El.parentNode.id = l_ID;',
'                }else{',
'                    l_El.innerHTML = l_Value;',
'                }',
'            }',
'        }',
'    }',
'    get = null;',
'',
'}',
'',
'//-->',
'</script>'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(244958874332303070)
,p_plug_name=>'ERROR'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P21_ERROR'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244960082734303095)
,p_button_sequence=>2
,p_button_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_button_name=>'P21_BOTON_FACTURAR'
,p_button_static_id=>'P21_BOTON_FACTURAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Procesar'
,p_button_alignment=>'LEFT'
,p_request_source=>'FACTURAR'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244967657806303113)
,p_button_sequence=>13
,p_button_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_button_name=>'P21_AGREGAR_ARTICULOS'
,p_button_static_id=>'P21_AGREGAR_ARTICULOS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>unistr('Agregar Art\00BF\00BFculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>'P21_TVE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_request_source=>'pr_cargar_detalle'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244968651771303114)
,p_button_sequence=>430
,p_button_plug_id=>wwv_flow_imp.id(244943458358303058)
,p_button_name=>'P21_ELIMINAR_LINEA'
,p_button_static_id=>'P21_ELIMINAR_LINEA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>unistr('Eliminar_Art\00BF\00BFculo')
,p_button_alignment=>'LEFT'
,p_button_condition=>'P21_COM_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_request_source=>'Eliminar'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244968482555303114)
,p_button_sequence=>636
,p_button_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_button_name=>'P21_RECALCULAR'
,p_button_static_id=>'P21_RECALCULAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Recalcular'
,p_button_alignment=>'LEFT'
,p_button_condition=>'P21_COM_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_request_source=>'Recalcular'
,p_request_source_type=>'STATIC'
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244959083558303071)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(244946676585303062)
,p_button_name=>'BTN_CLIENTENUEVO'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Modificar_Cliente'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:popUp2(''http://oas.pdm:7777/pls/apex/f?p=195:3:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,,1,1,S'', 8000, 850);'||wwv_flow.LF||
''
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244959254678303091)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(244946676585303062)
,p_button_name=>'NUEVO_BASADO_EN_ESTA_FACTURA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Nuevo_Basado_En_Esta_Factura'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>'P21_COM_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244959464975303094)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(244946676585303062)
,p_button_name=>'LIMPIAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Limpiar_/_Nuevo'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244959677229303094)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(244946676585303062)
,p_button_name=>'GENERAR_FACTURA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Generar_Factura'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P21_COM_ID is null and :P21_CLI_ID is not null',
'and (select count(*) ',
'FROM apex_collections',
'WHERE collection_name = ''CO_VARIABLES_DETALLE'') > 0'))
,p_button_condition2=>'SQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(244959883133303094)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_imp.id(244946676585303062)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270535280069046676)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'http://192.168.100.237:7778/reports/rwservlet?module=VEN_COMPROBANTE_final.rdf&userid=asdm_p/asdm_p@mxpdm&destype=cache&desformat=pdf&pn_com_id=&P21_COM_ID.&pn_emp_id=&F_EMP_ID.&pn_usuario_id=&F_USER_ID.&pn_age_id_agencia=&P21_AGE_ID_AGENCIA.'
,p_button_condition=>'P21_COM_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(244976377003303155)
,p_branch_action=>'f?p=&APP_ID.:21:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(244959677229303094)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(244976560788303159)
,p_branch_action=>'f?p=&APP_ID.:21:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(244959464975303094)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(244976783864303159)
,p_branch_action=>'f?p=&APP_ID.:21:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>20
,p_branch_comment=>'Created 28-SEP-2009 09:03 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244960278714303098)
,p_name=>'P21_IEB_ID'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244960457616303098)
,p_name=>'P21_TIPO_TELEFONO'
,p_item_sequence=>753
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>unistr('T\00BF\00BFlefono')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244960667348303098)
,p_name=>'P21_TIPO_DIRECCION'
,p_item_sequence=>751
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>unistr('Direcci\00BF\00BFn')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244960855303303098)
,p_name=>'P21_COND_CAB'
,p_item_sequence=>765
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cond Cab'
,p_source=>':P21_POL_ID||''-''||:P21_TVE_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244961056870303099)
,p_name=>'P21_TOTAL_FACTURA'
,p_item_sequence=>637
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Total Factura'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244961271350303099)
,p_name=>'P21_TOTAL_FACTURA_ING'
,p_item_sequence=>638
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Ajustar a Total Factura:'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244961472619303099)
,p_name=>'P21_CONT'
,p_item_sequence=>639
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Cont'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244961682699303099)
,p_name=>'P21_POCE_NACIONAL'
,p_item_sequence=>640
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Poce Nacional'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244961881449303099)
,p_name=>'P21_PEKAS'
,p_item_sequence=>632
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>unistr('D\00BF\00BFlares por Pekas ')
,p_format_mask=>'999G999G999G999G990D00'
,p_source=>'return to_char(pq_ven_comunes.fn_obtener_descuento_pekas(:P21_CLI_ID,:P21_PLAZO_FACTURA,:P21_POL_ID,:P21_POCE_NACIONAL),''999,999,999,999.99'');'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244962071259303100)
,p_name=>'P21_NCR_ID'
,p_item_sequence=>743
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Factura #'
,p_pre_element_text=>'<b><font COLOR="#990000">'
,p_post_element_text=>'</font>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT-TOP'
,p_display_when=>'P21_COM_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'HTML_UNSAFE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244962269547303100)
,p_name=>'P21_AGE_ID_AGENCIA'
,p_item_sequence=>758
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Age Id Agencia'
,p_source=>'F_AGE_ID_AGENCIA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244962455588303100)
,p_name=>'P21_ERROR'
,p_item_sequence=>450
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Error'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244962679662303101)
,p_name=>'P21_CLIENTE_EXISTE'
,p_item_sequence=>763
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Cliente Existe'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange=''pull_multi_value(this.value)'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244962856445303101)
,p_name=>'P21_DESCTO_PUNTOS'
,p_item_sequence=>631
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Descuento Pekas'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244963063125303101)
,p_name=>'P21_TRX_ID'
,p_item_sequence=>764
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244963267223303101)
,p_name=>'P21_DESCTO_LINEA'
,p_item_sequence=>12
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_item_default=>'0'
,p_prompt=>'Descto Linea'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244963453317303108)
,p_name=>'P21_TIPO_DESCTO'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_item_default=>'0'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>'STATIC2:%;1,Valor;0'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244963683166303108)
,p_name=>'P21_PLAZO_FACTURA'
,p_item_sequence=>762
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Plazo Factura'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P21_TVE_ID'
,p_read_only_when2=>'2'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244963865004303108)
,p_name=>'P21_ENTRADA'
,p_item_sequence=>633
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Entrada'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244964051616303108)
,p_name=>'P21_PLAN_TARJETA'
,p_item_sequence=>761
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Plan Tarjeta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P21_TVE_ID'
,p_display_when2=>'3'
,p_display_when_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244964273262303109)
,p_name=>'P21_DESCTO_FACTURA'
,p_item_sequence=>635
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>'Descto Factura'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244964478909303109)
,p_name=>'P21_POL_ID'
,p_item_sequence=>759
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>unistr('Pol\00BF\00BFtica')
,p_source=>'pol_id'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LV_POLITICAS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_politicas(:f_emp_id,:f_uge_id,:P25_CLI_ID,:P20_TVE_ID, :F_AGE_ID_AGENCIA, :F_SEG_ID, :p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>':p21_cli_id'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P21_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Debe ingresar la politica con la cual desea facturar'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244964962031303111)
,p_name=>'P21_FACTURA'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Factura #'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT com_numero, com_id',
'FROM ven_comprobantes com',
'WHERE com.emp_id = :f_emp_id',
'AND com.com_estado_registro = pq_constantes.fn_cv_emp_estado_reg_activo',
'order by 1'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Seleccione la Orden '
,p_lov_null_value=>'null'
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244965166534303111)
,p_name=>'P21_VALOR_FINANCIAR_CRE'
,p_item_sequence=>702
,p_item_plug_id=>wwv_flow_imp.id(244952480377303066)
,p_prompt=>'Valor Financiar Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244965373040303111)
,p_name=>'P21_TASA_CRE'
,p_item_sequence=>712
,p_item_plug_id=>wwv_flow_imp.id(244952480377303066)
,p_prompt=>'Tasa Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244965570080303111)
,p_name=>'P21_CUOTA_CRE'
,p_item_sequence=>722
,p_item_plug_id=>wwv_flow_imp.id(244952480377303066)
,p_prompt=>'Cuota Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244965774658303111)
,p_name=>'P21_PLAZO_CRE'
,p_item_sequence=>732
,p_item_plug_id=>wwv_flow_imp.id(244952480377303066)
,p_prompt=>'Plazo Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244965962835303112)
,p_name=>'P21_TVE_ID'
,p_item_sequence=>760
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Terminos de Venta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TERMINOS_VENTA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_terminos_venta(:f_emp_id,null,:p0_error); ',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P21_POL_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P21_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Terminos de venta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244966467546303112)
,p_name=>'P21_NCR_FECHA'
,p_item_sequence=>746
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha'
,p_source=>'COM_FECHA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244966876013303112)
,p_name=>'P21_DIRECCION'
,p_item_sequence=>752
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244967076283303112)
,p_name=>'P21_IDENTIFICACION'
,p_item_sequence=>748
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>unistr('Identificaci\00BF\00BFn')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_CLIENTES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
' lv_lov := pq_ven_listas.fn_lov_cliente(:f_emp_id,:P0_ERROR); ',
' RETURN(lv_lov);',
'END;'))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="pull_multi_value(this.value)";'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>':'
,p_read_only_when=>'P21_COM_ID'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244967277490303113)
,p_name=>'P21_ITE_SKU_ID'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_prompt=>unistr('Art\00BF\00BFculo')
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_ITEMS_FAC'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_items_ordenes(',
'                         :f_emp_id,',
'                         :P25_BODEGA,',
'                         :P25_ESTADO, ',
'                         :p0_error); ',
'  RETURN(lv_lov);',
'END;',
'',
''))
,p_cSize=>5
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onchange="pull_multi_value2(this.value)";'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P21_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244967480453303113)
,p_name=>'P21_CANTIDAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>4
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>5
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_read_only_when=>'P21_COM_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244967870459303113)
,p_name=>'P21_STOCK'
,p_item_sequence=>9
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244968053675303113)
,p_name=>'P21_ESTADO'
,p_item_sequence=>4
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_item_default=>'pq_constantes.fn_cn_cei_id_estado_a;'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTADOS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lv_lov VARCHAR2(8000);',
'BEGIN',
'  lv_lov := pq_ven_listas.fn_lov_estados_calidad; ',
'  RETURN(lv_lov);',
'END;'))
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244968269563303114)
,p_name=>'P21_SEQ_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244968860786303114)
,p_name=>'P21_CORREO'
,p_item_sequence=>755
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Correo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>57
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244969055576303114)
,p_name=>'P21_NOMBRE'
,p_item_sequence=>750
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-BOTTOM'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244969253266303115)
,p_name=>'P21_TELEFONO'
,p_item_sequence=>754
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>100
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244969462485303115)
,p_name=>'P21_AGE_ID_AGENTE'
,p_item_sequence=>757
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Age_id'
,p_source=>'PQ_VEN_LISTAS.fn_retorna_vendedor(:f_user_id,:f_emp_id);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'P20_disable_value'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244969659751303115)
,p_name=>'P21_BODEGA'
,p_item_sequence=>2
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_source=>'pq_ven_listas.fn_retorna_bodega_xdefecto(:P21_AGE_ID_AGENCIA,:P21_ERROR);'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_read_only_when=>'P21_COM_ID'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244969881004303115)
,p_name=>'P21_DESCRIPCION_LARGA'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_prompt=>'Descripcion Larga'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>50
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>'P21_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244970070774303115)
,p_name=>'P21_ESTADO_DES'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('Estado Art\00BF\00BFculo')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>2
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244970265028303115)
,p_name=>'P21_ENTRADA_MINIMA'
,p_item_sequence=>634
,p_item_plug_id=>wwv_flow_imp.id(244931067966303020)
,p_prompt=>unistr('Entrada M\00BF\00BFnima')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244970481234303116)
,p_name=>'P21_PLAZO_ANT'
,p_item_sequence=>766
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Plazo Ant'
,p_source=>':P21_PLAZO_FACTURA'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244970678750303116)
,p_name=>'P21_ENTRADA_CRE'
,p_item_sequence=>692
,p_item_plug_id=>wwv_flow_imp.id(244952480377303066)
,p_prompt=>'Entrada Cre'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244970868248303116)
,p_name=>'P21_BODEGA_NOMBRE'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(244958656928303070)
,p_prompt=>'Bodega'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT uge.uge_nombre',
'FROM   inv_bodegas_propias   bpr,',
'       asdm_unidades_gestion uge',
'WHERE  bpr.uge_id = uge.uge_id',
'       AND bpr.bpr_id = :P21_BODEGA'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>15
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>'P21_PLAZO_FACTURA'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244971078112303116)
,p_name=>'P21_NCR_TIPO'
,p_item_sequence=>796
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Com Tipo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244971253059303116)
,p_name=>'P21_CLI_ID'
,p_item_sequence=>749
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Cliente id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>30
,p_begin_on_new_line=>'N'
,p_begin_on_new_field=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244971458949303117)
,p_name=>'P21_NCR_NUMERO'
,p_item_sequence=>786
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Numero Comprobante'
,p_source=>':P0_FOL_SEC_ACTUAL'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244971682185303117)
,p_name=>'P21_DIR_ID_ENVIAR_FACTURA'
,p_item_sequence=>806
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Dir Id Enviar Factura'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244971865882303117)
,p_name=>'P21_TTR_ID'
,p_item_sequence=>816
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Ttr Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(244972071114303118)
,p_name=>'P21_SALDO'
,p_item_sequence=>776
,p_item_plug_id=>wwv_flow_imp.id(244943261408303056)
,p_prompt=>'Saldo'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(244972778778303127)
,p_computation_sequence=>10
,p_computation_item=>'P21_PLAZO_FACTURA'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  Ln_valor number(3) := 0;',
'begin',
'  if :P21_TVE_ID = 2 then',
'       Ln_Valor := 12;',
'  else',
'      Ln_Valor := 0;',
'  end if;',
'   return  Ln_Valor;',
'end;'))
,p_compute_when=>':P21_TVE_ID is not null and :P21_COND_CAB != :P21_POL_ID||''-''||:P21_TVE_ID'
,p_compute_when_text=>'PLSQL'
,p_compute_when_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(244972374957303119)
,p_computation_sequence=>20
,p_computation_item=>'P0_DESPLIEGA_AUT'
,p_computation_type=>'FUNCTION_BODY'
,p_computation_language=>'PLSQL'
,p_computation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    if :P21_ENTRADA is null then',
'        return ''N'';',
'    end if;',
'    if :P21_ENTRADA_MINIMA > nvl(:P21_ENTRADA,0) then',
'        return ''S'';',
'    end if;',
'end;'))
);
wwv_flow_imp_page.create_page_computation(
 p_id=>wwv_flow_imp.id(244972557021303126)
,p_computation_sequence=>30
,p_computation_item=>'P0_TRX_AUT'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_computation=>'86'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244973363010303134)
,p_validation_name=>'VALIDA_PEKAS'
,p_validation_sequence=>10
,p_validation=>'nvl(:P21_DESCTO_PUNTOS,0) <= to_number(nvl(:P21_PEKAS,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'No puede exceder del limite de pekas'
,p_validation_condition=>'P21_DESCTO_PUNTOS'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244972951800303128)
,p_validation_name=>'VALIDA ENTRADA'
,p_validation_sequence=>20
,p_validation=>'nvl(:P21_ENTRADA,0) > to_number(nvl(:P21_ENTRADA_MINIMA,0))'
,p_validation2=>'SQL'
,p_validation_type=>'EXPRESSION'
,p_error_message=>'Entrada no puede ser menor a la minima, Solicite Autorizacion'
,p_validation_condition=>':P21_ENTRADA is not null and :P0_DESPLIEGA_AUT = ''S'' and :P0_AUTORIZACION is null'
,p_validation_condition2=>'SQL'
,p_validation_condition_type=>'EXPRESSION'
,p_when_button_pressed=>wwv_flow_imp.id(244968482555303114)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(244973182119303134)
,p_validation_name=>'VALIDA_AUTORIZACION'
,p_validation_sequence=>30
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   Lv_Autorizacion  varchar2(100);',
'begin',
'  FOR reg_usu in 1..APEX_APPLICATION.G_F30.COUNT LOOP',
'    return pq_asdm_seguridad.fn_verifica_autorizacion(',
'                          to_number(APEX_APPLICATION.G_F30(reg_usu)),',
'                          :P0_TRX_AUT,',
'                          :P21_IDENTIFICACION,',
'                          :P21_ENTRADA,:P0_AUTORIZACION,',
'                          Lv_Autorizacion,:P21_ERROR);',
'  END LOOP;',
'end;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_BOOLEAN'
,p_error_message=>'Autorizacion no valida, solicitela nuevamente '
,p_validation_condition=>'P0_AUTORIZACION'
,p_validation_condition_type=>'ITEM_IS_NOT_NULL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244974253401303142)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ING_CABECERA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'     pq_ven_comunes.pr_ing_fac_cab(:F_EMP_ID,',
'                                                                :P21_POL_ID,',
'                                                                ''0'',   --llave',
'                                                               pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                                :P21_TVE_ID,',
'                                                                :P21_PLAZO_FACTURA,',
'                                                                :P21_PLAN_TARJETA);',
' pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                               :P21_POL_ID,',
'                                                pq_constantes.fn_cn_ttr_id_comprobante,',
'                                               :P21_DESCTO_PUNTOS,',
'                                               :P21_DESCTO_FACTURA,',
'                                               :P21_TVE_ID,',
'                                               :P21_PLAN_TARJETA,',
'                                               :P21_ENTRADA,',
'                                               :P21_PLAZO_FACTURA,',
'                                               :P21_TOTAL_FACTURA,',
'                                               :P21_POCE_NACIONAL,',
'                                               :P21_ENTRADA_MINIMA,',
'                                               :P21_PLAZO_CRE,',
'                                               :P21_ENTRADA_CRE,',
'                                               :P21_VALOR_FINANCIAR_CRE,',
'                                               :P21_TASA_CRE,	',
'                                               :P21_CUOTA_CRE);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P21_POL_ID is not null and :P21_TVE_ID is not null and (',
':P21_COND_CAB != :P21_POL_ID||''-''||:P21_TVE_ID or :P21_PLAZO_ANT != :P21_PLAZO_FACTURA)',
''))
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'SQL'
,p_internal_uid=>212721102131538216
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244974053857303142)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare ',
'    Ln_total_factura number(15,2);',
'begin',
'  pq_ven_comunes.pr_carga_coleccion_detalle(:F_EMP_ID, ',
'                                                 pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                 :P21_ITE_SKU_ID,',
'                                                 null,  --ITE_SKU_REL,',
'                                                 null,  --TIPO_REL_ID',
'                                                 null,  --TIPO_ITE_COMBO',
'                                                 null,  --RIT_PORCENTAJE',
'                                                 :P21_IEB_ID,',
'                                                 :P21_ESTADO,',
'                                                 :P21_POL_ID,',
'                                                 :P21_COM_FECHA,',
'                                                 :P21_CANTIDAD,',
'                                                 :P21_TIPO_DESCTO,',
'                                                 :P21_DESCTO_LINEA,',
'                                                 :P21_DESCTO_PUNTOS,',
'                                                 :P21_DESCTO_FACTURA,',
'                                                 :P21_TVE_ID,',
'                                                 :P21_PLAN_TARJETA,',
'                                                 :P21_ENTRADA,',
'                                                 :P21_PLAZO_FACTURA,',
'                                                 :P21_AGE_ID_AGENTE,',
'                                                 :P21_AGE_ID_AGENCIA,',
'                                                 Ln_total_Factura,',
'                                                 :P21_POCE_NACIONAL,',
'                                                 :P21_ENTRADA_MINIMA);',
'',
' pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                               :P21_POL_ID,',
'                                               pq_constantes.fn_cn_ttr_id_comprobante,',
'                                               :P21_DESCTO_PUNTOS,',
'                                               :P21_DESCTO_FACTURA,',
'                                               :P21_TVE_ID,',
'                                               :P21_PLAN_TARJETA,',
'                                               :P21_ENTRADA,',
'                                               :P21_PLAZO_FACTURA,',
'                                               :P21_TOTAL_FACTURA,',
'                                               :P21_POCE_NACIONAL,',
'                                               :P21_ENTRADA_MINIMA,',
'                                               :P21_PLAZO_CRE,',
'                                               :P21_ENTRADA_CRE,',
'                                               :P21_VALOR_FINANCIAR_CRE,',
'                                               :P21_TASA_CRE,	',
'                                               :P21_CUOTA_CRE);',
'',
'',
'  :P21_ieb_id := null;',
'  :P21_DESCRIPCION_LARGA := null;',
'  :P21_ite_sku_id := null;',
'  :P21_age_id_agente := null;',
'  :P21_cantidad  := null;',
'  :P21_stock  := null;',
'  :P21_estado  := null;',
'  :P21_SEQ_ID := null;',
'  :P21_TOTAL_FACTURA := Ln_total_factura;',
'  :P21_ERROR := null;',
'exception when OTHERS then',
'  :P21_ERROR := sqlerrm;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244967657806303113)
,p_process_when=>'p21_seq_id'
,p_process_when_type=>'ITEM_IS_NULL'
,p_internal_uid=>212720902587538216
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244974461988303143)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'SET_VALUE_TO_SELECTED_NUMBER'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
':P21_IDENTIFICACION := :P211_PER_NRO_IDENTIFICACION;',
':P211_PER_NRO_IDENTIFICACION := NULL;',
'',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P211_PER_NRO_IDENTIFICACION'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>212721310718538217
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244974672742303143)
,p_process_sequence=>15
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_RECALCULAR_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   Ln_Desc_Factura_ant  number(11, 4) := :P21_DESCTO_FACTURA;',
'   Lv_Autorizacion  varchar2(100);',
'   Lb_Resultado  boolean:=TRUE;',
'begin',
'   :P21_ERROR := null;',
'   if :P21_TOTAL_FACTURA_ING is not null THEN',
'          :P21_DESCTO_FACTURA := null;',
'        pq_ven_comunes.pr_ajusta_precio(:f_emp_id,',
'                                        :p21_pol_id,',
'                                        :p21_tve_id,',
'                                        :p21_plan_tarjeta,',
'                                        :p21_entrada,',
'                                        :p21_plazo_factura,',
'                                        :p21_total_factura,',
'                                        :p21_total_factura_ing,',
'                                        :p21_descto_puntos,',
'                                        :p21_descto_factura,',
'                                        :p21_poce_nacional,',
'                                        :p21_entrada_minima,',
'                                               :P21_PLAZO_CRE,',
'                                               :P21_ENTRADA_CRE,',
'                                               :P21_VALOR_FINANCIAR_CRE,',
'                                               :P21_TASA_CRE,	',
'                                               :P21_CUOTA_CRE,',
'                                        :p21_cont,',
'                                        :p21_error);',
'   end if;',
'   if :P21_ERROR is not null or :P21_TOTAL_FACTURA_ING is null then',
'        :P21_DESCTO_FACTURA := Ln_Desc_Factura_ant;',
'        pq_ven_comunes.pr_recalcula_coleccion_detalle(:f_emp_id,',
'                                                      :p21_pol_id,',
'                                                      pq_constantes.fn_cn_ttr_id_comprobante,',
'                                                      :p21_descto_puntos,',
'                                                      :p21_descto_factura,',
'                                                      :p21_tve_id,',
'                                                      :p21_plan_tarjeta,',
'                                                      :p21_entrada,',
'                                                      :p21_plazo_factura,',
'                                                      :p21_total_factura,',
'                                                      :p21_poce_nacional,',
'                                                      :p21_entrada_minima,',
'                                               :P21_PLAZO_CRE,',
'                                               :P21_ENTRADA_CRE,',
'                                               :P21_VALOR_FINANCIAR_CRE,',
'                                               :P21_TASA_CRE,	',
'                                               :P21_CUOTA_CRE);',
'      if :P0_DESPLIEGA_AUT = ''S'' then',
'          :P0_DESPLIEGA_AUT := ''N'';',
'      end if;',
'   end if;',
'end;',
'',
'',
'  :P21_ieb_id := null;',
'  :P21_ite_sku_id := null;',
'  :P21_age_id_agente := null;',
'  :P21_com_id   := null;',
'  :P21_cantidad  := null;',
'  :P21_stock  := null;',
'  :P21_estado  := null;',
'  :P21_SEQ_ID := null;',
'   :P21_TOTAL_FACTURA_ING := null;',
'   :P0_AUTORIZACION:=null;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244968482555303114)
,p_process_when=>':P21_TOTAL_FACTURA_ING is not null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'SQL'
,p_internal_uid=>212721521472538217
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244974876532303143)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ACTUALIZA_DETALLE'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    Ln_total_factura number(15,2);',
'begin',
'     pq_ven_comunes.pr_elimina_coleccion_detalle(:P21_SEQ_ID||'':''||:P21_ITE_SKU_ID||'':''||:P21_IEB_ID||'':''||:P21_ESTADO);',
'',
'pq_ven_comunes.pr_carga_coleccion_detalle(:F_EMP_ID, ',
'                                          pq_constantes.fn_cn_ttr_id_cotizacion,',
'                                          :P21_ITE_SKU_ID,',
'                                          null,  --ITE_SKU_REL,',
'                                          null,  --TIPO_REL_ID',
'                                          null,  --TIPO_ITE_COMBO',
'                                          null,  --RIT_PORCENTAJE',
'                                          :P21_IEB_ID,',
'                                          :P21_ESTADO,',
'                                          :P21_POL_ID,',
'                                          :P21_COT_FECHA,',
'                                          :P21_CANTIDAD,',
'                                          :P21_TIPO_DESCTO,',
'                                          :P21_DESCTO_LINEA,',
'                                          :P21_DESCTO_PUNTOS,',
'                                          :P21_DESCTO_FACTURA,',
'                                          :P21_TVE_ID,',
'                                          :P21_PLAN_TARJETA,',
'                                          :P21_ENTRADA,',
'                                          :P21_PLAZO_FACTURA,',
'                                          :P21_AGE_ID_AGENTE,',
'                                          :P21_AGE_ID_AGENCIA,',
'                                          Ln_total_factura,',
'                                          :P21_POCE_NACIONAL,',
'                                          :P21_ENTRADA_MINIMA);',
'',
' pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                               :P21_POL_ID,',
'                                                 pq_constantes.fn_cn_ttr_id_cotizacion,',
'                                               :P21_DESCTO_PUNTOS,',
'                                               :P21_DESCTO_FACTURA,',
'                                               :P21_TVE_ID,',
'                                               :P21_PLAN_TARJETA,',
'                                               :P21_ENTRADA,',
'                                               :P21_PLAZO_FACTURA,',
'                                               :P21_TOTAL_FACTURA,',
'                                               :P21_POCE_NACIONAL,',
'                                               :P21_ENTRADA_MINIMA,',
'                                               :P21_PLAZO_CRE,',
'                                               :P21_ENTRADA_CRE,',
'                                               :P21_VALOR_FINANCIAR_CRE,',
'                                               :P21_TASA_CRE,	',
'                                               :P21_CUOTA_CRE);',
'',
':P21_ieb_id := null;',
':P21_ite_sku_id := null;',
':P21_age_id_agente := null;',
':P21_cot_id   := null;',
':P21_cantidad  := null;',
':P21_stock  := null;',
':P21_estado  := null;',
':P21_TOTAL_FACTURA := Ln_total_factura;',
'',
':P21_SEQ_ID := null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244967657806303113)
,p_process_when=>'P21_SEQ_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>212721725262538217
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244975059436303143)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABAR_FACTURA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'   Ln_eje_eve_id number;',
'   Lv_Error varchar2(250);',
'   cursor cu_autorizaciones is',
'       select c001 usu_aut, c002 ttr_id, c003 valor, c004 Est_aut',
'       from apex_collections ',
'       where collection_name =  pq_asdm_seguridad.cv_coleccionAut;',
'begin',
'   Lv_Error := null;',
'   pq_swi_gen.pr_iniciar_ejecucion_evento(19, Ln_eje_eve_id, Lv_Error);',
'   if Lv_Error is null then ',
'   --Graba transaccion comprobantes',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_trx_id'',:P21_TRX_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_emp_id'',:F_EMP_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_uge_id'',:F_UGE_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_usu_id'',:F_USER_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_ttr_id'',pq_constantes.fn_cn_ttr_id_comprobante, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pd_trx_fecha_ejecucion'',TO_DATE(sysdate,''DD/MM/YYYY HH24:MI:SS'') ,Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_trx_estado_registro'',pq_constantes.fn_cv_emp_estado_reg_activo, Lv_Error);',
'   --Graba comprobantes',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_id'',:P21_COM_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_tve_id'',:P21_TVE_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_caj_id'',:P0_CAJ_ID,  Lv_Error);       ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_pol_id'',:P21_POL_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_cli_id'',:P21_CLI_ID, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_dir_id_enviar_factura'',:P21_DIR_ID_ENVIAR_FACTURA, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_com_tipo'', :P21_COM_TIPO,  Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pd_com_fecha'',:P21_COM_FECHA, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_numero'',:P21_COM_NUMERO,Lv_Error);     ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_age_num_sri'',:P0_AGE_NUM_SRI,Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_caj_num_sri'',:P0_CAJ_NUM_SRI,Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_age_id_agente'',:P21_AGE_ID_AGENTE, Lv_Error); ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_age_id_agencia'',:P21_AGE_ID_AGENCIA, Lv_Error);',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_ord_id'',:P21_ORDEN_VENTA, Lv_Error); ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_com_observacion'',:P21_COM_OBSERVACIONES, Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_saldo'',:P21_SALDO, Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_com_plazo'',:P21_PLAZO_FACTURA, Lv_Error);  ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pn_ede_id'',:P21_PLAN_TARJETA, Lv_Error);       ',
'       pq_swi_gen.pr_add_parametro_evento(Ln_eje_eve_id,''pv_error'',null, Lv_Error);',
'         ',
'      if Lv_Error is null then',
'           pq_swi_gen.pr_ejecutar_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'',
'      if Lv_Error is null then',
'          pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id, ''pn_com_id'', :p21_COM_ID, Lv_Error);',
'          pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id, ''pn_trx_id'', :P21_TRX_ID, Lv_Error);',
'          pq_swi_gen.pr_get_parametro_evento(Ln_eje_eve_id, ''pv_error'',  :P21_ERROR, Lv_Error);',
'      end if;',
'',
'      if Lv_Error is null then',
'          pq_swi_gen.pr_cerrar_ejecucion_evento(Ln_eje_eve_id, Lv_Error);',
'      end if;',
'   end if;',
'   for reg_aud in cu_autorizaciones loop',
'         insert into kseg_e.kseg_autorizaciones values (null,sysdate, :P21_TRX_ID, reg_aud.valor, :F_USER_ID, ',
'                      reg_aud.usu_aut, reg_aud.ttr_id, reg_aud.est_aut);',
'   end loop;',
'   :P21_ERROR := Lv_Error;',
'   if :P21_ERROR is not null then',
'      raise_application_error(-20010,Lv_error);',
'   end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244959677229303094)
,p_process_success_message=>'Cotizacion grabada correctamente'
,p_internal_uid=>212721908166538217
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244975265555303146)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'PR_LIMPIAR'
,p_attribute_01=>'CLEAR_CACHE_FOR_PAGES'
,p_attribute_04=>'20'
,p_process_when_button_id=>wwv_flow_imp.id(244959464975303094)
,p_internal_uid=>212722114285538220
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244975476103303146)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR_COLECCIONES'
,p_process_sql_clob=>'pq_ven_comunes.pr_elimina_coleccion;'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244959464975303094)
,p_internal_uid=>212722324833538220
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244975659753303146)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_ELIMINA_LINEA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  FOR i IN 1.. APEX_APPLICATION.G_F50.COUNT LOOP',
'     pq_ven_comunes.pr_elimina_coleccion_detalle(APEX_APPLICATION.G_F50(i));',
'  END LOOP;',
' ',
'  pq_ven_comunes.pr_recalcula_coleccion_detalle(:F_EMP_ID,',
'                                                                                        :P21_POL_ID,',
'                                                                                        pq_constantes.fn_cn_ttr_id_cotizacion,',
'                                                                                        :P21_DESCTO_PUNTOS,',
'                                                                                        :P21_DESCTO_FACTURA,',
'                                                                                        :P21_TVE_ID,',
'                                                                                       :P21_PLAN_TARJETA,',
'                                                                                        :P21_ENTRADA,',
'                                                                                        :P21_PLAZO_FACTURA,',
'                                                                                        :P21_TOTAL_FACTURA,',
'                                                                                        :P21_POCE_NACIONAL,',
'                                                                                        :P21_ENTRADA_MINIMA,',
'                                               :P21_PLAZO_CRE,',
'                                               :P21_ENTRADA_CRE,',
'                                               :P21_VALOR_FINANCIAR_CRE,',
'                                               :P21_TASA_CRE,	',
'                                               :P21_CUOTA_CRE);',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244968651771303114)
,p_internal_uid=>212722508483538220
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244975876613303154)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_LIMPIAR_COT_ID'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
':P21_COM_ID := NULL;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244959254678303091)
,p_internal_uid=>212722725343538228
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244973881552303142)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_CABECERA_ORD'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
' if to_number(:p20_orden_venta) > 0 then',
'     pq_ven_comprobantes.pr_carga_factura_cabecera(:f_emp_id,',
'                                             :p20_orden_venta,',
'                                             :p20_identificacion,',
'                                             :p20_pol_id,',
'                                             :p20_tve_id,',
'                                             :p20_plazo_factura,',
'                                             :p20_total_factura,',
'                                             :p20_poce_nacional,',
'                                             :p20_entrada_minima,',
'                                             :p20_error);',
'',
'    :f_per_id := :p20_identificacion;',
'',
'    pq_ven_comunes.pr_carga_datos_clientes( :f_uge_id,',
'                                            :f_emp_id,',
'                                            :p20_identificacion,',
'                                            :p20_nombre,',
'                                            :p20_cli_id,',
'                                            :p20_correo,',
'                                            :p20_direccion,',
'                                            :p20_telefono,',
'                                            :p20_tipo_direccion,',
'                                            :p20_tipo_telefono,',
'                                            :p20_cliente_existe);',
'else',
'    ',
'     pq_ven_comunes.pr_elimina_coleccion;',
'     :p20_identificacion := null;',
'     :p20_nombre := null;',
'     :p20_cli_id := null;',
'     :p20_correo := null;',
'     :p20_direccion := null;',
'     :p20_telefono  := null;',
'     :p20_tipo_direccion := null;',
'     :p20_tipo_telefono  := null;',
'',
'',
'end if;                                      ',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244960082734303095)
,p_internal_uid=>212720730282538216
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244976083976303154)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGA_DETALLE_ORDEN'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'   ',
'   pq_ven_comunes.pr_elimina_coleccion;',
'',
'   pq_ven_comunes.pr_ing_fac_cab( :f_emp_id,',
'                                  :p21_pol_id,',
'                                  ''0'', --llave',
'                                    pq_constantes.fn_cn_ttr_id_orden_venta,',
'                                  :p21_tve_id,',
'                                  :p21_plazo_factura,',
'                                  :p21_plan_tarjeta);',
'',
'',
'     pq_ven_cot.pr_carga_ordenventa_detalle(:f_emp_id, ',
'                                            :P21_ORDEN_VENTA, ',
'                                            :p21_total_factura,',
'                                            :p21_poce_nacional,',
'                                            :p21_entrada_minima,',
'                                            :p21_error);',
'',
'',
'',
'    ',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(244960082734303095)
,p_process_when=>':P21_ORDEN_VENTA'
,p_process_when_type=>'NEVER'
,p_internal_uid=>212722932706538228
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244973479832303139)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_TTR_ID_FACTURA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:F_EMP_ID,',
'                                                        :F_PCA_ID,',
'                                                        :P21_TTR_ID,',
'                                                        :P0_TTR_DESCRIPCION, ',
'                                                        :P0_PERIODO,',
'                                                        :P0_DATFOLIO,',
'                                                        :P0_FOL_SEC_ACTUAL,',
'                                                        :P0_PUE_NUM_SRI,',
'                                                        :P0_UGE_NUM_EST_SRI,',
'                                                        :P0_PUE_ID,',
'                                                        :P0_ERROR);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>212720328562538213
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(244973679861303142)
,p_process_sequence=>30
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_llama_eliminar_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pq_ven_comunes.pr_elimina_coleccion_detalle(:p21_seq_id);',
':P21_EMP_ID := null;',
':P21_ieb_id := null;',
':P21_ite_sku_id := null;',
':P21_age_id_agente := null;',
':P21_pol_id_det := null;',
':P21_com_id   := null;',
':P21_cantidad  := null;',
':P21_stock  := null;',
':P21_estado  := null;',
':P21_costo  := null;',
':P21_precio  := null;',
':P21_iva  := null;',
':P21_valor  := null;',
':P21_SEQ_ID := null;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'Eliminar'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>212720528591538216
);
wwv_flow_imp.component_end;
end;
/
