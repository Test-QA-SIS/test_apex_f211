prompt --application/pages/page_00014
begin
--   Manifest
--     PAGE: 00014
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>14
,p_name=>'Generacion de NC Rebajas'
,p_step_title=>'Generacion de NC Rebajas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'16'
,p_last_upd_yyyymmddhh24miss=>'20220518102017'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246446563720620286)
,p_plug_name=>'TIPOS DE PROVISION'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246446772392620287)
,p_plug_name=>'CONFIRMACION: Esta seguro que desea generar NC Rebajas por &P14_NCPROVISION.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270524859981046669)
,p_plug_display_sequence=>50
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P14_TNP_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246446964439620287)
,p_plug_name=>'Opciones'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246447153442620287)
,p_plug_name=>'ERROR'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270522778424046667)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_1'
,p_plug_item_display_point=>'BELOW'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_column_width=>'valign=top'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P14_ERROR'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(246447373014620287)
,p_plug_name=>'<B>&P0_TTR_DESCRIPCION.   &P0_DATFOLIO.     -    &P0_PERIODO.'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270520370913046666)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_column_width=>'valign=top'
,p_plug_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="text/javascript">',
'',
'function pull_multi_value(pValue){',
'    var get = new htmldb_Get(null,html_GetElement(''pFlowId'').value,',
'    ''APPLICATION_PROCESS=P_BUSCA_PERSONAS'',0);',
'    if(pValue)',
'    {',
'        get.add(''F_PER_ID'',pValue)',
'    }else',
'    {',
'        get.add(''F_PER_ID'',''null'')',
'    }    ',
'    gReturn = get.get(''XML'');',
'    if(gReturn)',
'    {',
'            var l_Count = gReturn.getElementsByTagName("item").length;',
'            for(var i = 0;i<l_Count;i++)',
'            {',
'             var l_Opt_Xml = gReturn.getElementsByTagName("item")[i];',
'              var l_ID = l_Opt_Xml.getAttribute(''id'');',
'               var l_El = html_GetElement(l_ID);   ',
'                ',
'                if(l_Opt_Xml.firstChild)',
'                {',
'                var l_Value = l_Opt_Xml.firstChild.nodeValue;',
'                 }',
'                 else',
'                 {',
'                 var l_Value = '''';',
'                 }',
'',
'                 if(l_El)',
'                 {',
'                 if(l_El.tagName == ''INPUT'')',
'                 {',
'                  l_El.value = l_Value;',
'                  }else',
'                   if(l_El.tagName == ''SPAN'' && l_El.className == ''grabber'')',
'                      {',
'                      l_El.parentNode.innerHTML = l_Value;',
'                      l_El.parentNode.id = l_ID;',
'                      }',
'                   else{',
'                       l_El.innerHTML = l_Value;',
'                       }',
'                       ',
'                  }',
'             }',
'    ',
'     }',
'     ',
'    get = null;',
'    ',
'    var ln_existe = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=dummy'',0);',
'   ',
'    g_existe = ln_existe.get();',
'    g_existe = ln_existe.get(''XML'');',
'',
' if ($x(''P14_CLIENTE_EXISTE'').value=="N")',
' {',
'javascript:popUp2(''http://oas.pdm:7777/pls/apex/f?p=&APP_ID.:201:&SESSION.::NO::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_UGESTION,F_TOKEN,F_USER_ID,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_'
||'ID.,&F_UGESTION.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,,1,1,S'', 650, 650);',
'  }',
'doSubmit();',
'}',
'',
'</script>'))
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(246447577854620288)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_button_name=>'SI'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Si'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(246447758395620288)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_button_name=>'NO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'No'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(246450667206620299)
,p_branch_action=>'f?p=&APP_ID.:14:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(246450864884620299)
,p_branch_action=>'f?p=&FLOW_ID.:14:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(246447577854620288)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(246451058189620299)
,p_branch_action=>'f?p=&FLOW_ID.:14:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(246447758395620288)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246448376759620290)
,p_name=>'P14_PROXIMA_EJECUCION'
,p_item_sequence=>73
,p_item_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_prompt=>'PROXIMA EJECUCION:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246448567539620290)
,p_name=>'P14_FRECUENCIA_MESES'
,p_item_sequence=>74
,p_item_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_prompt=>'FRECUENCIA (Meses):'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246448757953620290)
,p_name=>'P14_ULTIMA_EJECUCION'
,p_item_sequence=>75
,p_item_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_prompt=>'ULTIMA EJECUCION:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246448976405620291)
,p_name=>'P14_OBSERVACIONES'
,p_item_sequence=>76
,p_item_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_prompt=>'Observaciones'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246449158097620291)
,p_name=>'P14_TNP_ID'
,p_item_sequence=>86
,p_item_plug_id=>wwv_flow_imp.id(246446563720620286)
,p_prompt=>'Escoja un Tipo de NC '
,p_display_as=>'NATIVE_RADIOGROUP'
,p_named_lov=>'LOV_TIPNCPROV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(1000);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_CAR_TIPNCPROV'')||'' AND emp_id = ''||:f_emp_id;',
'return (lv_lov);',
'END;'))
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'SUBMIT'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246449352350620291)
,p_name=>'P14_NCPROVISION'
,p_item_sequence=>72
,p_item_plug_id=>wwv_flow_imp.id(246446772392620287)
,p_prompt=>'PROVISION:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246449565719620291)
,p_name=>'P14_AGE_ID_AGENCIA'
,p_item_sequence=>16
,p_item_plug_id=>wwv_flow_imp.id(246447373014620287)
,p_item_default=>'to_number(:f_emp_id)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Age Id Agencia'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(246449782080620292)
,p_name=>'P14_NC_FECHA'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_imp.id(246447373014620287)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>255
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'Y'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(246449964600620293)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PRESENTAR'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tnp_descripcion,',
'       tnp_proxima_ejecucion,',
'       tnp_frecuencia_meses,',
'       tnp_ultima_ejecucion',
'  INTO :p14_ncprovision,',
'       :p14_proxima_ejecucion,',
'       :p14_frecuencia_meses,',
'       :p14_ultima_ejecucion',
'  FROM car_tipos_nc_prov',
' WHERE tnp_id = :p14_tnp_id;',
'IF SYSDATE < to_date(:p14_proxima_ejecucion, ''DD-MM-YYYY'') THEN',
'  :p14_observaciones := '' Faltan '' ||',
'                        to_char(to_number(to_date(:p14_proxima_ejecucion,',
'                                                  ''DD-MM-YYYY'') - SYSDATE),',
unistr('                                ''990'') || '' dias para la proxima ejecuci\00BF\00BFn'';'),
'  /*      elsif add_months(to_date(:p14_ultima_ejecucion,''DD-MM-YYYY''),to_number(:p14_frecuencia_meses)-1) <=  to_date(:p14_proxima_ejecucion,''DD-MM-YYYY'') then',
unistr('  :P14_OBSERVACIONES  := :P14_OBSERVACIONES||'' La ultima ejecucion ''||:p14_ultiima_ejecucion||'' mas la frecuencia no coincide con la Fecha de Ejecuci\00BF\00BFn'';*/'),
'  NULL;',
'END IF;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'P14_TNP_ID'
,p_process_when_type=>'ITEM_IS_NOT_NULL'
,p_internal_uid=>214196813330855367
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(246450182421620298)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'GENERAR'
,p_process_sql_clob=>'PQ_CAR_PROVISION.PR_GENERA_NCPROVISION(:F_EMP_ID,:F_AGE_ID_AGENCIA,:P14_TNP_ID);'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(246447577854620288)
,p_process_success_message=>'Provision &P14_PROVISION. Generada'
,p_internal_uid=>214197031151855372
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(246450382003620298)
,p_process_sequence=>110
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_datos_folio_caja_usu'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_tgr_id asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'BEGIN',
'ln_tgr_id  := pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja'');',
':P0_TTR_ID := pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_nota_credito'');',
'',
'/*pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:F_EMP_ID,',
'                                                        :F_PCA_ID,',
'                                                        :P0_TTR_ID,',
'                                                        :P0_TTR_DESCRIPCION, ',
'                                                        :P0_PERIODO,',
'                                                        :P0_DATFOLIO,',
'                                                        :P0_FOL_SEC_ACTUAL,',
'                                                        :P0_CAJ_NUM_SRI,',
'                                                        :P0_AGE_NUM_SRI,',
'                                                        :P0_CAJ_ID,',
'                                                        :P0_PV_ERROR);*/',
'',
'pq_ven_movimientos_caja.pr_datos_folio_caja_usu(',
'  :F_EMP_ID,',
'  :F_PCA_ID,',
'   ln_tgr_id,',
'  :P0_TTR_DESCRIPCION, ',
'  :P0_PERIODO,',
'  :P0_DATFOLIO,',
'  :P0_FOL_SEC_ACTUAL,',
'  :P0_PUE_NUM_SRI  ,',
'  :P0_UGE_NUM_EST_SRI ,',
'  :P0_PUE_ID,',
'  :P0_TTR_ID,',
'  :P0_ERROR);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>214197230733855372
);
wwv_flow_imp.component_end;
end;
/
