prompt --application/pages/page_00061
begin
--   Manifest
--     PAGE: 00061
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>61
,p_name=>'Consulta Documentos Anulados'
,p_step_title=>'Consulta Documentos Anulados'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(90107457927577189)
,p_plug_name=>'Consulta Documentos Anulados'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select d.dan_id,',
'       d.tgr_descripcion,',
'       d.dan_numero,',
'       d.dan_observacion,',
'       d.pue_id,',
'       d.pue_nombre,',
'       d.usu_id,',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(d.usu_id) usuario,',
'       d.trx_fecha_ejecucion',
'  from v_asdm_documentos_anulados d',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(90107582401577189)
,p_name=>'Report 1'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif" alt="" />'
,p_icon_view_columns_per_row=>1
,p_owner=>'DLOPEZ'
,p_internal_uid=>57854431131812263
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90107780218577267)
,p_db_column_name=>'DAN_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DAN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90107865512577286)
,p_db_column_name=>'DAN_NUMERO'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Documento Anulado'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DAN_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90107951954577286)
,p_db_column_name=>'DAN_OBSERVACION'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>unistr('Observaci\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DAN_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90108078277577287)
,p_db_column_name=>'PUE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90108276298577287)
,p_db_column_name=>'PUE_NOMBRE'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>unistr('Punto Emisi\00BF\00BFn')
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90108774546577296)
,p_db_column_name=>'USU_ID'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Usu Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'USU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90108872065577296)
,p_db_column_name=>'TRX_FECHA_EJECUCION'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_FECHA_EJECUCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90109380603577297)
,p_db_column_name=>'TGR_DESCRIPCION'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'TipoDocumento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TGR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(90113461656607683)
,p_db_column_name=>'USUARIO'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'USUARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(90109660250577495)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'578566'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'DAN_ID:TGR_DESCRIPCION:DAN_NUMERO:DAN_OBSERVACION:PUE_NOMBRE:TRX_FECHA_EJECUCION:USUARIO'
);
wwv_flow_imp.component_end;
end;
/
