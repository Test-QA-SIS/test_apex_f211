prompt --application/pages/page_00200
begin
--   Manifest
--     PAGE: 00200
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>200
,p_name=>'POS'
,p_step_title=>'POS'
,p_reload_on_submit=>'A'
,p_autocomplete_on_off=>'OFF'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script src="https://printjs-4de6.kxcdn.com/print.js"></script>',
'',
'<script language="JavaScript" type="text/javascript">',
'    ',
'var reg_detalle = ''R117839759642245861940''; ',
'var reg_totales = ''R117842019818167837321'';',
'var reg_detalle1 = ''117839759642245861940''; ',
'var reg_totales1 = ''114829009469427805015'';',
'var reg_detalle_pago = ''R117843625138834586411'';  ',
'var reg_detalle_pago1 = ''117843625138834586411'';    ',
'    ',
'',
'function funRefreshRegion(reg_id){',
'    $a_report(reg_id,''1'',500,''15'');',
'}    ',
'    ',
'function getKeyCode(e)',
'{',
'	e= (window.event)? event : e;',
'	intKey = (e.keyCode)? e.keyCode: e.charCode;',
'	return intKey;',
'}      ',
'',
'function fnEnter(){',
'	var event = jQuery.Event(''keypress'');',
'	event.which = 13; ',
'	event.keyCode = 13; //keycode to trigger this for simulating enter',
'	jQuery(document.getElementById(''P200_PER_NRO_IDENTIFICACION'')).trigger(event)',
'}',
'',
'function fnEnter1(obj){',
'	var event = jQuery.Event(''keypress'');',
'	event.which = 13; ',
'	event.keyCode = 13; //keycode to trigger this for simulating enter',
'	obj.trigger(event)',
'}',
'',
'/*',
'    Creacion: 23/06/2020',
'    Autor: TMontalvan',
'    Descripcion: Actualiza cantidad',
'    Historia: R2798',
'*/ ',
'function fn_upd_cant(pn_value,pn_pvd_id,pn_pvc_id)',
'{',
'    var ajaxRequest = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=PR_UPD_CANTIDAD'',0);',
'',
'    ajaxRequest.addParam(''x01'',pn_value);',
'    ajaxRequest.addParam(''x02'',pn_pvd_id);',
'    ajaxRequest.addParam(''x03'',pn_pvc_id);',
'   ',
'    var ajaxResult = ajaxRequest.get();',
'    if (!ajaxResult) ',
'    { ',
'       // despues de realizar el ajax de actualizacion refresco la region para visualizar los valores nuevos sin realizar submit',
'        //var region = apex.region(reg_detalle);',
'        //region.refresh();  ',
'        //var totales = apex.region(reg_totales);',
'        //totales.refresh();',
'        ',
'    }',
'    funRefreshRegion(reg_detalle1);',
'    funRefreshRegion(reg_totales1);',
'    funRefreshRegion(reg_detalle_pago1);',
'    ajaxRequest = null;    ',
'    ',
'}',
'    ',
'function fn_delete_item(pn_value,pn_pvd_id,pn_pvc_id)',
'{',
'    ',
unistr('    if (confirm(''\00BFDesea eliminar este item?'')) {'),
'        var ajaxRequest = new htmldb_Get(null,&APP_ID.,''APPLICATION_PROCESS=PR_DELETE_ITEM'',0);',
'',
'        ajaxRequest.addParam(''x01'',pn_pvd_id);',
'        ajaxRequest.addParam(''x02'',pn_pvc_id);',
'',
'        var ajaxResult = ajaxRequest.get();',
'        if (!ajaxResult) ',
'        { ',
'           // despues de realizar el ajax de actualizacion refresco la region para visualizar los valores nuevos sin realizar submit',
'            //var region = apex.region(reg_detalle);',
'            //region.refresh();  ',
'            //var totales = apex.region(reg_totales);',
'            //totales.refresh();',
'',
'        }',
'        funRefreshRegion(reg_detalle1);',
'        funRefreshRegion(reg_totales1);',
'        funRefreshRegion(reg_detalle_pago1);',
'        //location.reload();',
'        ajaxRequest = null;    // Save it!',
'    }    ',
'    document.querySelectorAll(''.eliminar'').forEach((x) => x.checked = false);',
'}',
'   ',
'</script>'))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'03'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20231115142137'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(114096785659177671668)
,p_name=>'Variables'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'REGION_POSITION_05'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT va.vtt_orden,',
'       va.var_id,',
'			 va.vtt_etiqueta,',
'       vpv.vpv_valor valor,',
'       vtt_func_graba_cab',
'  FROM asdm_e.ppr_variables_ttransaccion va,',
'       ven_var_punto_venta               vpv,',
'       asdm_e.ven_punto_venta_det        pvd',
' WHERE ttr_id = 112',
'   AND va.emp_id = :f_emp_id',
'   AND vpv.var_id = va.var_id',
'   AND vpv.pvd_id = pvd.pvd_id',
'   AND pvd.pvc_id = :p200_pvc_id',
'   AND va.vtt_estado_registro = 0',
'   AND pvd.vpd_estado_registro = 0'))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>10000
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'Y'
,p_csv_output_link_text=>'Download'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114096785812047671669)
,p_query_column_id=>1
,p_column_alias=>'VTT_ORDEN'
,p_column_display_sequence=>1
,p_column_heading=>'Vtt orden'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114096785923695671670)
,p_query_column_id=>2
,p_column_alias=>'VAR_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Var id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114096786007460671671)
,p_query_column_id=>3
,p_column_alias=>'VTT_ETIQUETA'
,p_column_display_sequence=>3
,p_column_heading=>'Vtt etiqueta'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114096786081694671672)
,p_query_column_id=>4
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114096786237368671673)
,p_query_column_id=>5
,p_column_alias=>'VTT_FUNC_GRABA_CAB'
,p_column_display_sequence=>5
,p_column_heading=>'Vtt func graba cab'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(114861262620697569941)
,p_name=>'TOTALES'
,p_display_sequence=>40
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'REGION_POSITION_04'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_punto_venta.fn_mostrar_totales(pn_emp_id => :F_emp_id,',
'                                                      pn_pvc_id => :P200_PVC_ID);'))
,p_display_when_condition=>'P200_PVC_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528182542046671)
,p_plug_query_max_columns=>4
,p_query_headings_type=>'NO_HEADINGS'
,p_query_num_rows=>20
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114861262992476569945)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114861263070056569946)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114861263166544569947)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(114861263346351569948)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(117872012793515626866)
,p_name=>'Detalle'
,p_template=>wwv_flow_imp.id(270526077972046669)
,p_display_sequence=>200
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid=>true
,p_grid_column_span=>9
,p_display_point=>'BODY_2'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_punto_venta.fn_mostrar_detalle(pn_emp_id => :f_emp_id,',
'                                                      pn_pvc_id => :P200_PVC_ID);'))
,p_display_when_condition=>'P200_PVC_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_num_rows=>75
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872012945074626867)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013045305626868)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013115759626869)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013210286626870)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013256367626871)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013371331626872)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013534268626873)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013556404626874)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_report_column_width=>10
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013702806626875)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_column_format=>'PCT_GRAPH:::10'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117872013812183626876)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271040875602227)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271092898602228)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271223113602229)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271299625602230)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271449479602231)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271533133602232)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271568735602233)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271685964602234)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271823086602235)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874271858229602236)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272020788602237)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272104341602238)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272188724602239)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272331554602240)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272410830602241)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272468955602242)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272630150602243)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272691954602244)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272837858602245)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117874272912263602246)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(117875878290104351337)
,p_name=>'Detalle de Pago'
,p_display_sequence=>10
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'REGION_POSITION_03'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT tfp_descripcion,',
'       to_number(c006) valor,',
'       c040 detalle_pago',
'  FROM apex_collections co, asdm_tipos_formas_pago p',
' WHERE collection_name = ''CO_MOV_CAJA''',
' and p.tfp_id = to_number(co.c005)',
''))
,p_display_when_condition=>'P200_PVC_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_no_data_found=>'Sin forma de pago seleccionada.'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117875878436728351338)
,p_query_column_id=>1
,p_column_alias=>'TFP_DESCRIPCION'
,p_column_display_sequence=>1
,p_column_heading=>'Descripcion Pago'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117875878466443351339)
,p_query_column_id=>2
,p_column_alias=>'VALOR'
,p_column_display_sequence=>2
,p_column_heading=>'Valor'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(117875878639374351340)
,p_query_column_id=>3
,p_column_alias=>'DETALLE_PAGO'
,p_column_display_sequence=>3
,p_column_heading=>'Detalle pago'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(118306926022278877439)
,p_plug_name=>'Producto'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523665656046668)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY_2'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P200_PER_NRO_IDENTIFICACION'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(243377795605013080756)
,p_plug_name=>'INSERTA ARTICULOS'
,p_parent_plug_id=>wwv_flow_imp.id(118306926022278877439)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_column_width=>'valign=top'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(243377611143973547380)
,p_plug_name=>'<SPAN STYLE="font-size: 16pt">Punto de Venta</span>'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_2'
,p_plug_item_display_point=>'BELOW'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(124646194250664769930)
,p_name=>'PERFIL CLIENTE'
,p_parent_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>100
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select cre.csc_id,',
'       csc_descripcion,',
'       csc_color,',
'       csc_nombre_comercial,',
'       cre.emp_id,',
'       csc_estado_registro,',
'       dbms_lob.getlength("CSC_IMAGEN") csc_imagen',
'',
'  FROM ASDM_e.Car_Segmentacion_Credito cre, asdm_clientes  cl',
' WHERE cl.CSC_ID = cre.csc_id',
' and cl.cli_id = :p200_cli_id'))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194345126769931)
,p_query_column_id=>1
,p_column_alias=>'CSC_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194418511769932)
,p_query_column_id=>2
,p_column_alias=>'CSC_DESCRIPCION'
,p_column_display_sequence=>2
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194520277769933)
,p_query_column_id=>3
,p_column_alias=>'CSC_COLOR'
,p_column_display_sequence=>3
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194643401769934)
,p_query_column_id=>4
,p_column_alias=>'CSC_NOMBRE_COMERCIAL'
,p_column_display_sequence=>4
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194699146769935)
,p_query_column_id=>5
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>5
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194788346769936)
,p_query_column_id=>6
,p_column_alias=>'CSC_ESTADO_REGISTRO'
,p_column_display_sequence=>6
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646194933031769937)
,p_query_column_id=>7
,p_column_alias=>'CSC_IMAGEN'
,p_column_display_sequence=>7
,p_use_as_row_header=>'N'
,p_column_format=>'IMAGE:CAR_SEGMENTACION_CREDITO:CSC_IMAGEN:CSC_ID::::::::'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(124646195022988769938)
,p_name=>'Cupo Disponible'
,p_parent_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>200
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select cl.cli_id , cl.cup_cupo_disponible',
'  from car_cupos_clientes cl',
' where cl.cli_id = :p200_cli_id',
'   and cl.cup_estado_registro = 0',
'   and cl.cup_tipo_cupo = ''M''',
''))
,p_ajax_enabled=>'Y'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646195056907769939)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(124646195240661769940)
,p_query_column_id=>2
,p_column_alias=>'CUP_CUPO_DISPONIBLE'
,p_column_display_sequence=>2
,p_column_heading=>'Cup cupo disponible'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(243324664072395674781)
,p_plug_name=>'OtrosDatosCabecera'
,p_parent_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>40
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(243800526243913755342)
,p_plug_name=>'DATOS_POL'
,p_parent_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P200_DIRECCION IS NOT NULL AND :P200_TELEFONO IS NOT NULL'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(243800526451878757697)
,p_plug_name=>'DATOS_TAR'
,p_parent_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>60
,p_plug_new_grid_row=>false
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(273919015192309872102)
,p_plug_name=>'Datos del Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_plug_display_sequence=>20
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(114861262668232569942)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(114861262620697569941)
,p_button_name=>'Pago'
,p_button_static_id=>'forma_pago'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Datos de Pago'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:152:&SESSION.:PAGAR:&DEBUG.:RP,152:P152_CLI_ID,P152_PVC_ID,P152_NOMBRES,P152_PCA_ID,F_POPUP:&P200_CLI_ID.,&P200_PVC_ID.,&P200_PER_NOMBRE.,&P200_PCA_ID.,S'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871345170499335847)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_button_name=>'editar_cliente'
,p_button_static_id=>'editar_cliente'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Editar cliente'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:145:&SESSION.:editar_cliente:&DEBUG.:RP:P145_PER_NRO_IDENTIFICACION,P145_PER_TIPO_IDENTIFICACION,P145_TDI_ID,P145_TTE_ID,P145_PER_ID,F_POPUP,P145_TEL_ID:&P200_PER_NRO_IDENTIFICACION.,&P200_PER_TIPO_IDENTIFICACION.,&P200_TDI_ID.,&P200_TTE_ID.,&P200_PER_ID.,S,&P200_TEL_ID.'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(114861262848894569943)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(114861262620697569941)
,p_button_name=>'Facturar'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Facturar'
,p_button_position=>'BELOW_BOX'
,p_button_comment=>':P200_TOTAL_FORMA_PAGO = :P200_TOTAL_ORDEN'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871345567036335850)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_button_name=>'No_POPUP'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'No popup'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:200:&SESSION.::&DEBUG.:RP:F_POPUP:N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(114861262903434569944)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(114861262620697569941)
,p_button_name=>'BT_IMPRIME'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprime'
,p_button_position=>'BELOW_BOX'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871345952062335853)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_button_name=>'nuevo_cliente'
,p_button_static_id=>'nuevo_cliente'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(28584142941930228604)
,p_button_image_alt=>'Nuevo cliente'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:145:&SESSION.:nuevo_cliente:&DEBUG.:RP:P145_PER_TIPO_IDENTIFICACION,P145_PER_NRO_IDENTIFICACION,P145_TDI_ID,P145_TTE_ID,F_POPUP,P145_TEL_ID:&P200_PER_TIPO_IDENTIFICACION.,&P200_PER_NRO_IDENTIFICACION.,&P200_TDI_ID.,&P200_TTE_ID.,S,&P200_TEL_ID.'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871346428581335853)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_button_name=>'Consumidor'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Consumidor Final'
,p_warn_on_unsaved_changes=>null
,p_grid_new_row=>'N'
,p_grid_new_column=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(117871329246313335824)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_button_name=>'LIMPIAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo'
,p_button_position=>'HELP'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(117876045434160807228)
,p_branch_name=>'br_imprime'
,p_branch_action=>'f?p=&APP_ID.:200:&SESSION.:imprime_fac:&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(114861262848894569943)
,p_branch_sequence=>20
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>':p0_error IS NULL and :p200_error_factura IS NULL'
,p_branch_condition_text=>'PLSQL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(114861261605550569931)
,p_name=>'P200_TGR_FACTURACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(115265005545373999857)
,p_name=>'P200_ERROR_FACTURA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868254842560142570)
,p_name=>'P200_TSE_ID_MINOREO'
,p_item_sequence=>333
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868254899908142571)
,p_name=>'P200_TSE_ID_MAYOREO'
,p_item_sequence=>343
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868255082456142573)
,p_name=>'P200_DESCUENTO_ROL'
,p_item_sequence=>353
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_item_default=>'N'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868255167440142574)
,p_name=>'P200_CUPO_CORRECTO'
,p_item_sequence=>363
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_item_default=>'N'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868255289953142575)
,p_name=>'P200_COT_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868255391793142576)
,p_name=>'P200_TAG_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117868286638703362240)
,p_name=>'P200_PCA_ID'
,p_item_sequence=>373
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871321743310335814)
,p_name=>'P200_ITE_SKU_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871322059210335814)
,p_name=>'P200_BARCODE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_prompt=>'C&oacute;digo de Barras'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871322533889335814)
,p_name=>'P200_IEB_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871323282507335816)
,p_name=>'P200_ITE_SEC_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_prompt=>'Item'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.ite_sec_id || '' - '' || d.ite_descripcion_corta || '' - Stock:'' ||',
'       SUM(a.ieb_cantidad) d,',
'       d.ite_sec_id r',
'  FROM inv_items_estado_bodega_stock a,',
'       inv_combinaciones_estado      b,',
'       inv_combinaciones_estado_det  c,',
'       inv_items                     d,',
'       inv_items_categorias          ica',
' WHERE b.ces_id = a.ces_id',
'   AND c.ces_id = b.ces_id',
'   AND d.ite_sku_id = a.ite_sku_id',
'   AND d.ite_sku_id = ica.ite_sku_id',
'   AND a.bpr_id = :p200_bpr_id',
'   AND c.cei_id = :p200_cei_id',
'   AND a.ieb_cantidad > 0',
'   AND b.ces_para_venta != ''N''',
'   AND ica.cat_id IN',
'       (SELECT c.cat_id',
'          FROM inv_categorias c',
'         WHERE c.emp_id = :F_EMP_ID',
'           AND c.cat_estado_registro = 0',
'        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'         START WITH c.cat_id IN',
'                    (SELECT p.cat_id',
'                       FROM ven_categorias_pos p',
'                      WHERE p.emp_id = :F_EMP_ID',
'                        AND p.cpo_estado_registro = 0))',
' GROUP BY d.ite_descripcion_corta, d.ite_sec_id, d.ite_sku_id;'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P200_BPR_ID,P200_CEI_ID'
,p_ajax_items_to_submit=>'P200_ITE_SKU_ID,P200_STOCK,P200_DESCRIPCION_LARGA'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>5
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871323672898335817)
,p_name=>'P200_STOCK'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Stock'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return ',
'pq_ven_listas.fn_retorna_stock(:P200_ITE_SKU_ID,:P200_ESTADO,:P200_BODEGA,:P200_LCM_ID,:F_EMP_ID,:P200_ORD_TIPO,:f_seg_id);'))
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>6
,p_cMaxlength=>2000
,p_tag_attributes=>'style="color:GREEN" readonly'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871324067450335818)
,p_name=>'P200_DESCRIPCION_LARGA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_prompt=>'Producto - Descripci&oacute;n'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>75
,p_cHeight=>1
,p_tag_attributes=>'style="font-size:16;color:BLUE;font-family:Arial Narrow" readonly'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P200_TVE_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871324488615335819)
,p_name=>'P200_TIPO_DESCTO'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_item_default=>'1'
,p_display_as=>'NATIVE_RADIOGROUP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DESCR, VAL',
'  FROM (SELECT ''%(Valor entero ejem 3%)'' DESCR, ''1'' VAL, ''1'' ORD',
'          FROM DUAL',
'        UNION',
'        SELECT ''Valor'' DESCR, ''0'' VAL, ''2'' ORD',
'          FROM DUAL',
'        UNION',
'        SELECT ''%Margen(Valor entero ejem 25%)'' DESCR, ''2'' VAL, ''3'' ORD',
'          FROM DUAL',
'        UNION',
'        SELECT ''Precio'' DESCR, ''3'' VAL, ''4'' ORD',
'          FROM DUAL',
'         WHERE :F_SEG_ID = ASDM_P.pq_constantes.fn_retorna_constante(0,''cn_tse_id_minoreo''))',
' ORDER BY ORD ASC'))
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_read_only_when=>':p200_accion <> ''E'' OR :P200_DESCUENTO_AUTO=''S'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_escape_on_http_output=>'N'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'1'
,p_attribute_02=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871324919472335819)
,p_name=>'P200_PLAZO_LISTA'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_prompt=>'Plazo<br>Lista'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_tag_attributes=>'onchange="doSubmit()"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871325324543335820)
,p_name=>'P200_DESCTO_LINEA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_item_default=>'0'
,p_prompt=>'<SPAN STYLE="font-size: 10pt;;color:Darkslategray">&P200_LABEL_TIPO_DESCUEN.</span>'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>2000
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_read_only_when=>':p200_accion <> ''E'' OR :P200_DESCUENTO_AUTO=''S'''
,p_read_only_when2=>'SQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871325703967335820)
,p_name=>'P200_SERVICIO_MANEJO_PRODUCTO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_prompt=>'Manejo Prod <br>Unitario'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>5
,p_cMaxlength=>4000
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
,p_item_comment=>':f_seg_id = pq_constantes.fn_retorna_constante(null,''cn_tse_id_mayoreo'') OR (pq_constantes.fn_retorna_constante(0,''cn_emp_id_cuarmuebles'') = :F_EMP_ID AND  pq_constantes.fn_retorna_constante(0,''cn_tse_id_minoreo'') = :f_seg_id)'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871326117222335821)
,p_name=>'P200_CANTIDAD'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_prompt=>'Cantidad'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>8
,p_cMaxlength=>2000
,p_tag_attributes=>'onkeypress="javascript:if (getKeyCode(event)==13) doSubmit(''carga_detalle'')"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871326456836335822)
,p_name=>'P200_PROMOCION_SN'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871326871781335822)
,p_name=>'P200_FACTOR_MARGEN'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_use_cache_before_default=>'NO'
,p_source=>'return pq_ppr_polit_precios.fn_get_factor(pq_constantes.fn_retorna_constante(0,''cn_fac_id_margen''),:P200_POL_ID,:P200_ITE_SKU_ID||''::''||:P200_ESTADO||'':''||:P200_BODEGA)*100;'
,p_source_type=>'FUNCTION_BODY'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871327295710335822)
,p_name=>'P200_ITEM_COMBO_YN'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871327681673335822)
,p_name=>'P200_CODIGO_ITEM_COMBO'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871328135128335822)
,p_name=>'P200_NUMERO_LINEAS'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    pq_ven_ordenes.fn_numero_lineas_factura(:F_EMP_ID)',
'FROM      DUAL;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871328548799335823)
,p_name=>'P200_CAT_ID_ITEM'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(243377795605013080756)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871329590763335825)
,p_name=>'P200_ORD_FECHA'
,p_item_sequence=>3
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_use_cache_before_default=>'NO'
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha:'
,p_source=>'ORD_FECHA'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871329981077335825)
,p_name=>'P200_ORD_TIPO'
,p_item_sequence=>13
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_prompt=>'Ord Tipo'
,p_source=>'VEN'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871330379715335825)
,p_name=>'P200_CLI_ID'
,p_item_sequence=>23
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_prompt=>'Cli_id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871330840693335825)
,p_name=>'P200_TIPO_IDENTIFICADOR'
,p_item_sequence=>33
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT TID_DESCRIPCION',
'  FROM INV_TIPOS_IDENTIFICADOR',
' WHERE TID_ID =  PQ_CONSTANTES.fn_retorna_constante(:f_emp_id, ''cn_tid_id_codigo_barras'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871331211569335829)
,p_name=>'P200_FORMA_PAGO'
,p_item_sequence=>43
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_source=>'select decode(:p200_tve_id,pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''),pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_tarjeta_credito''),null) tfp from dual'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871331603033335829)
,p_name=>'P200_AGE_ID_AGENCIA'
,p_item_sequence=>63
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_use_cache_before_default=>'NO'
,p_source=>'F_AGE_ID_AGENCIA'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871331995375335829)
,p_name=>'P200_TSE_ID'
,p_item_sequence=>73
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_use_cache_before_default=>'NO'
,p_source=>'F_SEG_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871332374758335829)
,p_name=>'P200_TTR_ID'
,p_item_sequence=>163
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_source=>'112'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871332778680335830)
,p_name=>'P200_TIPO_VENTA'
,p_item_sequence=>273
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_source=>'VEN'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871333158246335830)
,p_name=>'P200_APROBACION_COMERCIAL'
,p_item_sequence=>283
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871333620156335830)
,p_name=>'P200_PERMITE_ENTRADA'
,p_item_sequence=>313
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871334014581335830)
,p_name=>'P200_DESCUENTO_AUTO'
,p_item_sequence=>323
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871334732707335831)
,p_name=>'P200_ORD_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_prompt=>'ord_id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:15px; color: blue;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when=>'P200_COT_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871335056642335831)
,p_name=>'P200_ORD_SEC_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Orden'
,p_source=>'select ord_sec_id from ven_ordenes where ord_id = :p200_ord_id and emp_id = :f_emp_id'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'style="font-size:18px; color: blue;"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'P200_COT_ID'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871335531948335831)
,p_name=>'P200_DIA_CORTE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_source=>'P15_DIA_CORTE'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871335856220335832)
,p_name=>'P200_EOR_OBSERVACIONES'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_prompt=>'Observaciones para Despacho'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>50
,p_cMaxlength=>200
,p_cHeight=>5
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'Y'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871336264682335832)
,p_name=>'P200_TIPO_IDENTIFICACION'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_prompt=>unistr('Tipo Identificaci\00F3n')
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871336732113335832)
,p_name=>'P200_GARANTE'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_prompt=>'Garante'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_tag_attributes=>'onchange="doSubmit(''garante'')"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'CENTER-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'ENTERABLE'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871337100405335832)
,p_name=>'P200_NOMBRE_GARANTE'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871337522498335832)
,p_name=>'P200_COM_NUM'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871337794083335833)
,p_name=>'P200_PUE_ELECTRONICO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(243324664072395674781)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871338476227335834)
,p_name=>'P200_AGE_ID_AGENTE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_display_as=>'NATIVE_HIDDEN'
,p_lov_translated=>'Y'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871338858131335837)
,p_name=>'P200_TVE_ID'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_item_default=>'1'
,p_prompt=>'Forma Venta:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TERMINOS_VENTA_OVENTA_RAP'
,p_lov=>'SELECT L.TVE_DESCRIPCION D, L.TVE_ID R FROM VEN_TERMINOS_VENTA L WHERE L.TVE_ID IN (:F_TVE_CONTADO,:F_TVE_TC,2) and L.EMP_ID = :F_EMP_ID AND L.TVE_ESTADO_REGISTRO = 0'
,p_cHeight=>1
,p_tag_attributes2=>'onchange="doSubmit(''LIMPIA_VENTA'')"'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>'Terminos de venta'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871339777024335839)
,p_name=>'P200_POL_ID'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_item_default=>'859'
,p_prompt=>unistr('Pol\00EDtica:')
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pol.pol_id || '' '' || pol_descripcion_larga a, pol.pol_id b',
'  FROM ppr_politicas pol, ppr_politicas_agencia pag',
' WHERE pol.pol_id = pag.pol_id',
'   AND SYSDATE BETWEEN pol.pol_fecha_desde AND pol.pol_fecha_hasta',
'   AND pol.pol_estado_registro = 0',
'   AND pag.pag_estado_registro = 0',
'   AND pag.age_id = :f_age_id_agencia',
'   AND pol.pol_id IN (CASE WHEN',
'                      pq_car_empleados.fn_valida_empleado_credito(pn_cli_id => :p200_cli_id) = ''S'' THEN',
'                      :p200_pol_id_empleados END,',
'                      :p200_pol_id_comisariato)'))
,p_cHeight=>1
,p_rowspan=>1
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871340184777335839)
,p_name=>'P200_PLAZO_FACTURA'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_item_default=>'0'
,p_source=>'0'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871340613489335839)
,p_name=>'P200_FECHA_CORTE_CRE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_prompt=>'Fecha 1er Venc.:'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_FECHAS_CORTE2'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.c001 DESCRIPCION,',
'       a.c002 VALOR',
'  FROM apex_collections a',
' WHERE a.collection_name = ''COLL_FECHAS_CORTE'''))
,p_lov_cascade_parent_items=>'P200_PLAZO_FACTURA'
,p_ajax_items_to_submit=>'P200_PLAZO_FACTURA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-TOP'
,p_display_when=>':P200_TVE_ID = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_credito_propio'') '
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871341035368335840)
,p_name=>'P200_DIAS_GRACIA_PRIMER_VENC'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_prompt=>'D/Gracia primer Venc:'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'CENTER-CENTER'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871341416502335840)
,p_name=>'P200_BPR_ID'
,p_is_required=>true
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_prompt=>'Bodega Despacho'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.bpr_id || ''-'' || c.uge_nombre l, b.bpr_id r',
'  FROM asdm_bodegas_agencias a,',
'       inv_bodegas_propias   b,',
'       asdm_unidades_gestion c',
' WHERE b.bpr_id = a.bpr_id',
'   AND c.uge_id = b.uge_id',
'   AND a.bag_de_agencia = ''S''',
'   and a.age_id = :f_age_id_agencia'))
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P200_POL_ID IS NOT NULL',
'AND ',
':P200_ORD_TIPO NOT IN (pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig''),pq_constantes.fn_retorna_constante(null,''cv_ord_tipo_orden_factura_consig_moto''))',
'and :P200_PLAZO_FACTURA is not null'))
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871341825602335841)
,p_name=>'P200_CEI_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_item_default=>'20'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_item_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return  pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                          ''cn_cei_id_ventas'') ;'))
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871342200084335841)
,p_name=>'P200_PVC_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_prompt=>'PVC_ID'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871342566195335841)
,p_name=>'P200_PVD_ID'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871343283100335842)
,p_name=>'P200_BANCOS'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(243800526451878757697)
,p_prompt=>'Bancos'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.ede_descripcion l, a.ede_id r',
'  FROM asdm_entidades_destinos a',
' WHERE a.ede_tipo = ''EFI''',
'   AND a.EDE_ESTADO_REGISTRO = 0',
'   AND a.ede_es_induccion IS NULL',
'   AND a.emp_id = :F_EMP_ID',
'   AND EXISTS (SELECT NULL',
'          FROM asdm_entidades_destinos ed',
'         WHERE ed.ede_id_padre = a.ede_id',
'           AND ed.ede_tipo =',
'               pq_constantes.fn_retorna_constante(0,',
'                                                  ''cv_tede_tarjeta_credito'')',
'           AND ed.ede_estado_registro = 0',
'           AND ed.emp_id = :F_EMP_ID)'))
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871343736117335842)
,p_name=>'P200_TARJETA'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(243800526451878757697)
,p_prompt=>'Tarjeta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  b.ede_descripcion,b.ede_id',
'    FROM asdm_entidades_destinos b',
'    WHERE b.ede_tipo= pq_constantes.fn_retorna_constante(0,',
'                                                     ''cv_tede_tarjeta_credito'')',
'AND B.EDE_ESTADO_REGISTRO = 0',
'AND b.ede_id_padre= :p200_bancos',
' AND b.emp_id=:f_emp_id'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P200_BANCOS'
,p_ajax_items_to_submit=>'P200_BANCOS'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P200_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''))'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871344063865335844)
,p_name=>'P200_TIPO'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(243800526451878757697)
,p_prompt=>'Tipo'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
' SELECT c.ede_descripcion,c.ede_id',
'FROM asdm_entidades_destinos c',
'WHERE c.ede_tipo= pq_constantes.fn_retorna_constante(0,',
'                                                    ''cv_tede_plan_tarjeta'') ',
'',
'AND c.EDE_ESTADO_REGISTRO = 0',
'AND c.ede_id_padre= :p200_tarjeta',
'AND c.emp_id=:f_emp_id'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P200_TARJETA'
,p_ajax_items_to_submit=>'P200_TARJETA'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>':P200_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''))'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871344526364335845)
,p_name=>'P200_PLAN_TARJETA'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(243800526451878757697)
,p_prompt=>'Plan Tarjeta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sys_connect_by_path(ede.ede_id||''-''||ede.ede_descripcion,'' -> '') AS d,',
'ede.ede_id r',
' FROM asdm_entidades_destinos ede',
' WHERE ede.ede_tipo=    pq_constantes.fn_retorna_constante(0, ',
'                                                    ''cv_tede_detalle_plan_tarjeta'') ',
' and ede.emp_id=:f_emp_id',
' AND ede.EDE_ESTADO_REGISTRO = 0',
'START WITH ede.ede_id_padre =:p200_tipo',
'CONNECT BY PRIOR ede.ede_id =ede.ede_id_padre',
''))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P200_TIPO'
,p_ajax_items_to_submit=>'P200_TIPO'
,p_ajax_optimize_refresh=>'Y'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_rowspan=>1
,p_label_alignment=>'ABOVE'
,p_display_when=>':P200_TVE_ID in(pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_tarjeta_credito''), pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tve_id_contado''))'
,p_display_when2=>'PLSQL'
,p_display_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871346775010335853)
,p_name=>'P200_PER_ID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871347189538335854)
,p_name=>'P200_NRO_IDE_AUX'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871347559680335854)
,p_name=>'P200_PER_TIPO_IDENTIFICACION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_item_default=>'CED'
,p_prompt=>'Tipo Identificacion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPO_IDENTIFICACION'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_TIPO_IDENTIFICACION'');',
'return (lv_lov);',
'END;',
'',
'',
''))
,p_cHeight=>1
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871347954716335855)
,p_name=>'P200_PER_NRO_IDENTIFICACION'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_prompt=>'Nro Identificacion'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(decode(a.per_primer_nombre,',
'                  NULL,',
'                  NULL,',
'                  rtrim(a.per_primer_nombre)) ||',
'           decode(a.per_segundo_nombre,',
'                  NULL,',
'                  NULL,',
'                  '' '' || rtrim(a.per_segundo_nombre)) ||',
'           decode(a.per_primer_apellido,',
'                  NULL,',
'                  NULL,',
'                  '' '' || rtrim(a.per_primer_apellido)) ||',
'           decode(a.per_segundo_apellido,',
'                  NULL,',
'                  NULL,',
'                  '' '' || rtrim(a.per_segundo_apellido) || '' ->'' || a.per_id),',
'           a.per_razon_social) || '' - '' || a.per_nro_identificacion d,',
'       a.per_nro_identificacion r',
'  FROM asdm_personas a',
' WHERE a.per_tipo_identificacion = :p200_per_tipo_identificacion',
'   AND a.emp_id = :f_emp_id'))
,p_lov_display_null=>'YES'
,p_lov_cascade_parent_items=>'P200_PER_TIPO_IDENTIFICACION'
,p_ajax_optimize_refresh=>'Y'
,p_cSize=>50
,p_tag_attributes=>'onChange=''fnEnter()'''
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'N'
,p_attribute_05=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871348427915335855)
,p_name=>'P200_PER_NOMBRE'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_prompt=>'Nombre'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871348796792335855)
,p_name=>'P200_DIRECCION'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_prompt=>'Direccion'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871349202790335858)
,p_name=>'P200_TELEFONO'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_prompt=>'Telefono'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871349621464335860)
,p_name=>'P200_PER_CORREO_ELECTRONICO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_prompt=>'Correo Electronico'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871350023678335860)
,p_name=>'P200_PER_PRIMER_NOMBRE'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871350384789335860)
,p_name=>'P200_PER_SEGUNDO_NOMBRE'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871350839126335862)
,p_name=>'P200_PER_PRIMER_APELLIDO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871351224268335862)
,p_name=>'P200_PER_SEGUNDO_APELLIDO'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871351560819335862)
,p_name=>'P200_TDI_ID'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_source=>'10'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871351961792335863)
,p_name=>'P200_TTE_ID'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871352360360335863)
,p_name=>'P200_TEL_ID'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871352781963335863)
,p_name=>'P200_URL'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871353226240335863)
,p_name=>'P200_DIR_ID'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117871353642848335864)
,p_name=>'P200_RAZON_SOCIAL'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(273919015192309872102)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117875878925208351343)
,p_name=>'P200_DETALLE_PAG'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_prompt=>'<SPAN STYLE="font-size: 16pt">Detalle de Pago</span>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534776221046675)
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117875878987636351344)
,p_name=>'P200_TOTAL_FORMA_PAGO'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT sum(to_number(c006)) valor',
'  FROM apex_collections co, asdm_tipos_formas_pago p',
' WHERE collection_name = ''CO_MOV_CAJA''',
' and p.tfp_id = to_number(co.c005)'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117875879095854351345)
,p_name=>'P200_TOTAL_ORDEN'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(vp.vpv_valor),0)',
'    FROM ven_punto_venta_det de, ven_var_punto_venta vp',
'   WHERE de.pvc_id = :p200_pvc_id',
'     AND vp.pvd_id = de.pvd_id',
'     AND de.vpd_estado_registro = 0',
'     AND vp.var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total_cred_entrada'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117875879203204351346)
,p_name=>'P200_TTR_FACTURACION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(117876045454121807229)
,p_name=>'P200_COM_ID'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(117875878290104351337)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(120834282236881370127)
,p_name=>'P200_POL_ID_COMISARIATO'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(120834283963385370145)
,p_name=>'P200_POL_ID_EMPLEADOS'
,p_item_sequence=>130
,p_item_plug_id=>wwv_flow_imp.id(243800526243913755342)
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(124646194052953769929)
,p_name=>'P200_DESC_ROL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(243377611143973547380)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871359713392335912)
,p_name=>'da_hideShowButtons_new_edit_client'
,p_event_sequence=>10
,p_condition_element=>'P200_PER_ID'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871360225962335912)
,p_event_id=>wwv_flow_imp.id(117871359713392335912)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(117871345952062335853)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871360707449335913)
,p_event_id=>wwv_flow_imp.id(117871359713392335912)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(117871345170499335847)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871361177821335914)
,p_event_id=>wwv_flow_imp.id(117871359713392335912)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(117871345170499335847)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871361726536335914)
,p_event_id=>wwv_flow_imp.id(117871359713392335912)
,p_event_result=>'FALSE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'BUTTON'
,p_affected_button_id=>wwv_flow_imp.id(117871345952062335853)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871362192995335914)
,p_event_id=>wwv_flow_imp.id(117871359713392335912)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'F_POPUP'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'N'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871362668096335914)
,p_event_id=>wwv_flow_imp.id(117871359713392335912)
,p_event_result=>'FALSE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'F_POPUP'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'N'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871363064968335914)
,p_name=>'da_onClose_modalPage_nuevo'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#nuevo_cliente'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871363569640335915)
,p_event_id=>wwv_flow_imp.id(117871363064968335914)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP := ''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871364143725335915)
,p_event_id=>wwv_flow_imp.id(117871363064968335914)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P0_ERROR'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P0_ERROR'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871364632985335915)
,p_event_id=>wwv_flow_imp.id(117871363064968335914)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_NRO_IDE_AUX'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P145_PER_NRO_IDENTIFICACION'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871365107902335915)
,p_event_id=>wwv_flow_imp.id(117871363064968335914)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'window.onunload = null;'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871365595126335916)
,p_event_id=>wwv_flow_imp.id(117871363064968335914)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871365951342335916)
,p_name=>'da_onClose_modalPage_editar'
,p_event_sequence=>40
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#editar_cliente'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871366468190335916)
,p_event_id=>wwv_flow_imp.id(117871365951342335916)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP := ''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871367015255335916)
,p_event_id=>wwv_flow_imp.id(117871365951342335916)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P0_ERROR'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P0_ERROR'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871367485305335916)
,p_event_id=>wwv_flow_imp.id(117871365951342335916)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_NRO_IDE_AUX'
,p_attribute_01=>'DIALOG_RETURN_ITEM'
,p_attribute_09=>'N'
,p_attribute_10=>'P145_PER_NRO_IDENTIFICACION'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871368024043335917)
,p_event_id=>wwv_flow_imp.id(117871365951342335916)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'window.onunload = null;'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871368500129335917)
,p_event_id=>wwv_flow_imp.id(117871365951342335916)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117875798423353502227)
,p_name=>'da_onClose_modalPage_pagar'
,p_event_sequence=>50
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#forma_pago'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117875798549736502228)
,p_event_id=>wwv_flow_imp.id(117875798423353502227)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':F_POPUP := ''N'';'
,p_attribute_03=>'F_POPUP'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117875799050567502233)
,p_event_id=>wwv_flow_imp.id(117875798423353502227)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>'null;'
,p_attribute_02=>'F_POPUP'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117875881099211351365)
,p_name=>'da_onClose_modalPage_pagar_1'
,p_event_sequence=>60
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#forma_pago'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'apexafterclosedialog'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117875881589457351370)
,p_event_id=>wwv_flow_imp.id(117875881099211351365)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'$a_report(''117843625138834586411'',''1'',500,''15'');'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871368852270335917)
,p_name=>'da_onEnter_cedula_carga'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P200_PER_NRO_IDENTIFICACION,P200_PER_TIPO_IDENTIFICACION'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'(this.browserEvent.keyCode == 13 && document.getElementById("P200_PER_NRO_IDENTIFICACION").value != "" )'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871369427150335918)
,p_event_id=>wwv_flow_imp.id(117871368852270335917)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_cliente'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871369886018335919)
,p_event_id=>wwv_flow_imp.id(117871368852270335917)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_POL_ID,P200_BPR_ID,P200_CEI_ID'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871370339405335919)
,p_name=>'da_onEnter_cedula_borra'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P200_PER_NRO_IDENTIFICACION'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'this.browserEvent.keyCode == 13 && document.getElementById("P200_PER_NRO_IDENTIFICACION").value == "" && document.getElementById("P200_CLI_ID").value != ""'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'keypress'
,p_da_event_comment=>'&& document.getElementById(''P142_PER_NRO_IDENTIFICACION'').value != ""'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871370794467335919)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_NRO_IDE_AUX'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871371289742335920)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_PER_NOMBRE'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871371814895335920)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_DIRECCION'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871372344948335920)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_TELEFONO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871372806362335920)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>50
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_PER_ID'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871373307877335921)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_CLI_ID'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871373782846335921)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_PER_CORREO_ELECTRONICO'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871374335225335921)
,p_event_id=>wwv_flow_imp.id(117871370339405335919)
,p_event_result=>'TRUE'
,p_action_sequence=>90
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P200_PER_NRO_IDENTIFICACION:= null;',
':P200_NRO_IDE_AUX := NULL;',
':P200_PER_NOMBRE:= null;',
':P200_DIRECCION:= null;',
':P200_TELEFONO:= null;',
':P200_PER_ID := null;',
':P200_CLI_ID := nuLL;',
':P200_PER_CORREO_ELECTRONICO := null;'))
,p_attribute_03=>'P200_PER_NRO_IDENTIFICACION,P200_NRO_IDE_AUX,P200_PER_NOMBRE,P200_DIRECCION,P200_TELEFONO,P200_PER_ID,P200_CLI_ID,P200_PER_CORREO_ELECTRONICO'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871355968989335904)
,p_name=>'da_borra_cedula_spl'
,p_event_sequence=>90
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#superlov_button_id'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871356496727335907)
,p_event_id=>wwv_flow_imp.id(117871355968989335904)
,p_event_result=>'TRUE'
,p_action_sequence=>120
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P200_PER_NRO_IDENTIFICACION:= null;',
':P200_NRO_IDE_AUX := NULL;',
':P200_PER_NOMBRE:= null;',
':P200_DIRECCION:= null;',
':P200_TELEFONO:= null;',
':P200_PER_ID := null;',
':P200_CLI_ID := nuLL;',
':P200_PER_CORREO_ELECTRONICO := null;'))
,p_attribute_03=>'P200_PER_NRO_IDENTIFICACION,P200_NRO_IDE_AUX,P200_PER_NOMBRE,P200_DIRECCION,P200_TELEFONO,P200_PER_ID,P200_CLI_ID,P200_PER_CORREO_ELECTRONICO'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871357009942335910)
,p_event_id=>wwv_flow_imp.id(117871355968989335904)
,p_event_result=>'TRUE'
,p_action_sequence=>130
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871357410368335910)
,p_name=>'da_busca_item_sic'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P200_BARCODE'
,p_condition_element=>'P200_BARCODE'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871358398089335911)
,p_event_id=>wwv_flow_imp.id(117871357410368335910)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'SELECT d.ite_sku_id,',
'       SUM(a.ieb_cantidad) ieb_cantidad,',
'       d.ite_sec_id,',
'       substr(d.ite_descripcion_corta, 0, 200) descripcion',
'  into :p200_ite_sku_id, :p200_stock, :p200_ite_sec_id,:p200_descripcion_larga ',
'  FROM inv_items_estado_bodega_stock a,',
'       inv_combinaciones_estado      b,',
'       inv_combinaciones_estado_det  c,',
'       inv_items                     d,',
'       inv_items_categorias          ica,',
'       inv_items_identificador       iid',
' WHERE b.ces_id = a.ces_id',
'   AND c.ces_id = b.ces_id',
'   AND d.ite_sku_id = a.ite_sku_id',
'   AND d.ite_sku_id = ica.ite_sku_id',
'   AND a.bpr_id = :p200_bpr_id',
'   AND c.cei_id = :p200_cei_id',
'   AND iid.iid_valor = :p200_barcode',
'   AND iid.ite_sku_id = d.ite_sku_id',
'   AND a.emp_id = :f_emp_id',
'   AND EXISTS (SELECT NULL',
'          FROM inv_tipos_identificador t',
'         WHERE t.emp_id = :f_emp_id',
'           AND t.tid_estado_registro = 0',
'           AND t.tid_nombre = ''Codigo De Barras''',
'           AND t.tid_id = iid.tid_id)',
'  -- AND iid.iid_primario = ''N''',
'   AND a.ieb_cantidad > 0',
'   AND b.ces_para_venta != ''N''',
'   AND ica.cat_id IN',
'       (SELECT c.cat_id',
'          FROM inv_categorias c',
'         WHERE c.emp_id = :F_EMP_ID',
'           AND c.cat_estado_registro = 0',
'        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'         START WITH c.cat_id IN',
'                    (SELECT p.cat_id',
'                       FROM ven_categorias_pos p',
'                      WHERE p.emp_id = :f_emp_id',
'                        AND p.cpo_estado_registro = 0))',
' GROUP BY d.ite_descripcion_corta, d.ite_sec_id, d.ite_sku_id;',
'   :P200_CANTIDAD := 1;',
'   exception when others then',
'   :p200_ite_sku_id := null;',
'   :p200_stock := null;',
'   :p200_descripcion_larga := null;',
'   :p200_ite_sec_id := null;',
'   :P200_CANTIDAD := null;',
'end;'))
,p_attribute_02=>'P200_BARCODE,F_EMP_ID,P200_CEI_ID,P200_BPR_ID'
,p_attribute_03=>'P200_ITE_SKU_ID,P200_STOCK,P200_DESCRIPCION_LARGA,P200_ITE_SEC_ID,P200_CANTIDAD'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(120489771369952161630)
,p_event_id=>wwv_flow_imp.id(117871357410368335910)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_BARCODE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871374679753335921)
,p_name=>'ad_consumidor'
,p_event_sequence=>110
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_imp.id(117871346428581335853)
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871375165758335922)
,p_event_id=>wwv_flow_imp.id(117871374679753335921)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'busca_consumidor'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871358798498335911)
,p_name=>'da_carga_detalle_barcode'
,p_event_sequence=>120
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P200_CANTIDAD'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'document.getElementById("P200_BARCODE").value != null &&  document.getElementById("P200_CANTIDAD").value != null && document.getElementById("P200_BARCODE").value != "" && document.getElementById("P200_CANTIDAD").value != ""  && document.getElementByI'
||'d("P200_STOCK").value > 0'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871359282664335911)
,p_event_id=>wwv_flow_imp.id(117871358798498335911)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_01=>'carga_detalle'
,p_attribute_02=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117871375641013335922)
,p_name=>'ad_ite_sec_id'
,p_event_sequence=>130
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P200_ITE_SEC_ID1'
,p_condition_element=>'P200_ITE_SEC_ID1'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117871376110116335922)
,p_event_id=>wwv_flow_imp.id(117871375641013335922)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT d.ite_sku_id,',
'         a.ieb_cantidad,',
'         substr(d.ite_descripcion_corta, 0, 200) descripcion,',
'         d.ite_sec_id',
'    INTO :p200_ite_sku_id, :p200_stock, :p200_descripcion_larga,:p200_ite_sec_id',
'    FROM inv_items_estado_bodega_stock a,',
'         inv_combinaciones_estado      b,',
'         inv_combinaciones_estado_det  c,',
'         inv_items                     d,',
'         inv_items_categorias          ica',
'   WHERE b.ces_id = a.ces_id',
'     AND c.ces_id = b.ces_id',
'     AND d.ite_sku_id = a.ite_sku_id',
'     AND d.ite_sku_id = ica.ite_sku_id',
'     AND a.bpr_id = :p200_bpr_id',
'     AND c.cei_id = :p200_cei_id',
'     AND a.emp_id = :f_emp_id',
'     AND a.ieb_cantidad > 0',
'     AND b.ces_para_venta != ''N''',
'     AND d.ite_sec_id = :p200_ite_sec_id',
'     AND ica.cat_id IN (SELECT c.cat_id',
'                          FROM inv_categorias c',
'                         WHERE c.emp_id = :f_emp_id',
'                           AND c.cat_estado_registro = 0',
'                        CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'                         START WITH c.cat_id = 15465);',
'  --pr_car_log_pruebas_ac(1,1,''ingresa'',null,null)                       ;',
'  :p200_cantidad := 1;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    -- pr_car_log_pruebas_ac(1,:p200_ite_sec_id,''error'',sqlerrm,null)                       ;',
'    :p200_ite_sku_id        := NULL;',
'    :p200_stock             := NULL;',
'    :p200_descripcion_larga := NULL;',
'    :p200_ite_sec_id        := NULL;',
'    :p200_cantidad          := NULL;',
'END;',
''))
,p_attribute_02=>'P200_ITE_SEC_ID1'
,p_attribute_03=>'P200_ITE_SKU_ID,P200_STOCK,P200_DESCRIPCION_LARGA,P200_CANTIDAD'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(120489771539324161631)
,p_event_id=>wwv_flow_imp.id(117871375641013335922)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_CANTIDAD'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(117872062056154490665)
,p_name=>'da_update_detalle'
,p_event_sequence=>140
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'.cantidad'
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
,p_display_when_type=>'NEVER'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(117875879919573351353)
,p_event_id=>wwv_flow_imp.id(117872062056154490665)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(vp.vpv_valor),0) into :P200_TOTAL_ORDEN',
'    FROM ven_punto_venta_det de, ven_var_punto_venta vp',
'   WHERE de.pvc_id = :p200_pvc_id',
'     AND vp.pvd_id = de.pvd_id',
'     AND de.vpd_estado_registro = 0',
'     AND vp.var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total_cred_entrada'');'))
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(120489771194135161628)
,p_name=>'da_set_focus_barcode'
,p_event_sequence=>150
,p_bind_type=>'live'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(120489771308558161629)
,p_event_id=>wwv_flow_imp.id(120489771194135161628)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_FOCUS'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P200_BARCODE'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(288746989439824635)
,p_name=>'da_load_ite_info'
,p_event_sequence=>160
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P200_ITE_SEC_ID'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(288747060946824636)
,p_event_id=>wwv_flow_imp.id(288746989439824635)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  SELECT d.ite_sku_id,',
'         substr(d.ite_descripcion_corta, 0, 200),',
'         SUM(a.ieb_cantidad)',
'    INTO :p200_ite_sku_id, :p200_descripcion_larga, :p200_stock',
'    FROM inv_items_estado_bodega_stock a,',
'         inv_combinaciones_estado      b,',
'         inv_combinaciones_estado_det  c,',
'         inv_items                     d,',
'         inv_items_categorias          ica',
'   WHERE d.ite_sec_id = :p200_ite_sec_id',
'     AND b.ces_id = a.ces_id',
'     AND c.ces_id = b.ces_id',
'     AND d.ite_sku_id = a.ite_sku_id',
'     AND d.ite_sku_id = ica.ite_sku_id',
'     AND a.bpr_id = :p200_bpr_id',
'     AND c.cei_id = :p200_cei_id',
'     AND a.ieb_cantidad > 0',
'     AND b.ces_para_venta != ''N''',
'     AND ica.cat_id IN',
'         (SELECT c.cat_id',
'            FROM inv_categorias c',
'           WHERE c.emp_id = :f_emp_id',
'             AND c.cat_estado_registro = 0',
'          CONNECT BY PRIOR c.cat_id = c.cat_id_padre',
'           START WITH c.cat_id IN',
'                      (SELECT p.cat_id',
'                         FROM ven_categorias_pos p',
'                        WHERE p.emp_id = :f_emp_id',
'                          AND p.cpo_estado_registro = 0))',
'   GROUP BY d.ite_descripcion_corta, d.ite_sec_id, d.ite_sku_id;',
'END;',
''))
,p_attribute_02=>'P200_CEI_ID,P200_BPR_ID,P200_ITE_SEC_ID'
,p_attribute_03=>'P200_STOCK,P200_DESCRIPCION_LARGA,P200_ITE_SKU_ID'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871354812961335902)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_encera_ite_sec_id'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':p200_ite_Sec_id := null;',
'--raise_application_error(-20001,''BPR_OD:''||:p200_bpr_id||'' CEID:''||:p200_cei_id);'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_internal_uid=>117839101661691570976
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117868254996140142572)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_variables'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P200_TSE_ID_MINOREO := pq_constantes.fn_retorna_constante(0, ''cn_tse_id_minoreo'') ;',
':P200_TSE_ID_MAYOREO := pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'');',
':P200_TTR_FACTURACION := asdm_p.pq_constantes.fn_retorna_constante(0,''cn_ttr_id_comprobante'');',
':P200_TGR_FACTURACION := asdm_p.pq_constantes.fn_retorna_constante(0,''cn_tgr_id_facturacion'');',
':P200_POL_ID_COMISARIATO:=pq_constantes.fn_retorna_constante(:f_emp_id,''cn_pol_id_comisariato'');',
':P200_POL_ID_EMPLEADOS := asdm_p.pq_constantes.fn_retorna_constante(:f_emp_id,''cn_pol_id_empleados_min'');'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':P200_TSE_ID_MINOREO IS NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>117836001844870377646
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871355192478335903)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_get_datos_cliente'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'  IF :p200_per_nro_identificacion IS NULL THEN',
'    IF :p200_nro_ide_aux IS NOT NULL THEN',
'      :p200_per_nro_identificacion := :p200_nro_ide_aux;',
'    END IF;',
'  END IF;',
'  IF :p200_per_nro_identificacion IS NOT NULL THEN',
'    BEGIN',
'      SELECT p.per_id,',
'             c.cli_id,',
'             p.per_correo_electronico,',
'             p.per_primer_nombre,',
'             p.per_segundo_nombre,',
'             p.per_primer_apellido,',
'             p.per_segundo_apellido,',
'             p.per_razon_social,',
'             d.dir_descripcion,',
'             d.dir_id',
'        INTO :p200_per_id,',
'             :p200_cli_id,',
'             :p200_per_correo_electronico,',
'             :p200_per_primer_nombre,',
'             :p200_per_segundo_nombre,',
'             :p200_per_primer_apellido,',
'             :p200_per_segundo_apellido,',
'             :p200_razon_social,',
'             :p200_direccion,',
'             :p200_dir_id',
'        FROM asdm_personas p, asdm_clientes c, asdm_direcciones d',
'       WHERE p.per_nro_identificacion = :p200_per_nro_identificacion',
'         AND d.per_id(+) = p.per_id',
'         AND d.dir_estado_registro(+) = :f_estado_reg_activo',
'         AND d.tdi_id(+) = :p200_tdi_id',
'         AND p.emp_id = :f_emp_id',
'         AND c.per_id(+) = p.per_id',
'         AND p.per_tipo_identificacion = :p200_per_tipo_identificacion;',
'    ',
'      :p200_per_nombre := TRIM(:p200_per_primer_nombre || '' '' ||',
'                               :p200_per_segundo_nombre || '' '' ||',
'                               :p200_per_primer_apellido || '' '' ||',
'                               :p200_per_segundo_apellido || '' '' ||',
'                               :p200_razon_social);',
'    ',
'      BEGIN',
'        SELECT *',
'          INTO :p200_tel_id, :p200_telefono, :p200_tte_id',
'          FROM (SELECT tt.tel_id, tt.tel_numero, tt.tte_id',
'                  FROM asdm_telefonos tt, asdm_tipos_telefonos tte',
'                 WHERE tt.per_id = :p200_per_id',
'                   AND tt.tel_estado_registro = :f_estado_reg_activo',
'                   AND tt.emp_id = :f_emp_id',
'                   AND tte.tte_estado_registro = :f_estado_reg_activo',
'                   AND tte.tte_id = tt.tte_id',
'                 ORDER BY 1 DESC)',
'         WHERE rownum = 1;',
'      EXCEPTION',
'        WHEN OTHERS THEN',
'          :p200_tte_id := pq_constantes.fn_retorna_constante(0,',
'                                                             ''cn_tte_id_domicilio'');',
'      END;',
'    EXCEPTION',
'      WHEN no_data_found THEN',
'        IF :p200_per_primer_apellido IS NOT NULL THEN',
'          :p200_per_id                 := NULL;',
'          :p200_cli_id                 := NULL;',
'          :p200_per_correo_electronico := NULL;',
'          :p200_per_primer_nombre      := NULL;',
'          :p200_per_segundo_nombre     := NULL;',
'          :p200_per_primer_apellido    := NULL;',
'          :p200_per_segundo_apellido   := NULL;',
'          :p200_per_nombre             := NULL;',
'          :p200_direccion              := NULL;',
'          :p200_telefono               := NULL;',
'          :p200_per_nombre             := NULL;',
'          :p200_dir_id                 := NULL;',
'        END IF;',
'        :p200_tte_id := pq_constantes.fn_retorna_constante(0,',
'                                                           ''cn_tte_id_domicilio'');',
'    END;',
'  ELSE',
'    :p200_per_id                 := NULL;',
'    :p200_cli_id                 := NULL;',
'    :p200_per_correo_electronico := NULL;',
'    :p200_per_primer_nombre      := NULL;',
'    :p200_per_segundo_nombre     := NULL;',
'    :p200_per_primer_apellido    := NULL;',
'    :p200_per_segundo_apellido   := NULL;',
'    :p200_per_nombre             := NULL;',
'    :p200_direccion              := NULL;',
'    :p200_telefono               := NULL;',
'    :p200_per_nombre             := NULL;',
'    :p200_dir_id                 := NULL;',
'  END IF;',
'  :p200_pol_id := 859;',
'  :p200_bpr_id := 137;',
'  :p200_cei_id := 20;',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''busca_cliente'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>117839102041208570977
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871355645624335903)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_get_consumidor'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN ',
'',
':p200_per_nro_identificacion := ''9999999999999'';',
':p200_per_tipo_identificacion := ''RUC'';',
'',
'    if :p200_per_nro_identificacion IS NULL then',
'    if :P200_NRO_IDE_AUX IS NOT NULL THEN',
'      :p200_per_nro_identificacion := :p200_NRO_IDE_AUX;',
'    END IF;',
'  end if;',
'  IF :p200_per_nro_identificacion IS NOT NULL THEN',
'   BEGIN    ',
'      SELECT p.per_id,',
'             c.cli_id,',
'             p.per_correo_electronico,',
'             p.per_primer_nombre,',
'             p.per_segundo_nombre,',
'             p.per_primer_apellido,',
'             p.per_segundo_apellido,',
'             p.per_razon_social,',
'             d.dir_descripcion,',
'             d.dir_id',
'        INTO :p200_per_id,',
'             :p200_cli_id,',
'             :p200_per_correo_electronico,',
'             :p200_per_primer_nombre,',
'             :p200_per_segundo_nombre,',
'             :p200_per_primer_apellido,',
'             :p200_per_segundo_apellido,',
'             :p200_razon_social,',
'             :P200_direccion,',
'             :p200_DIR_ID',
'        FROM asdm_personas p, asdm_clientes c, asdm_direcciones d',
'       WHERE p.per_nro_identificacion = :p200_per_nro_identificacion',
'         and d.per_id(+) = p.per_id',
'         AND d.dir_estado_registro(+) = :f_estado_reg_activo',
'         AND d.tdi_id(+) = :p200_tdi_id',
'         AND p.emp_id = :f_emp_id',
'         AND c.per_id(+) = p.per_id',
'         AND p.per_tipo_identificacion = :p200_per_tipo_identificacion;',
'         ',
'     ',
'     :p200_per_nombre := trim(:p200_per_primer_nombre || '' '' ||',
'                            :p200_per_segundo_nombre || '' '' ||',
'                            :p200_per_primer_apellido || '' '' ||',
'                            :p200_per_segundo_apellido || '' '' || :p200_razon_social);      ',
'      ',
'    begin',
'          select *',
'            into :p200_tel_id, :p200_telefono, :p200_tte_id',
'            from (select tt.tel_id, tt.tel_numero, tt.tte_id',
'                    from asdm_telefonos tt, asdm_tipos_telefonos tte',
'                   where tt.per_id = :p200_per_id',
'                     and tt.tel_estado_registro = :f_estado_reg_activo',
'                     and tt.emp_id = :f_emp_id',
'                     and tte.tte_estado_registro = :f_estado_reg_activo',
'                     and tte.tte_id = tt.tte_id',
'                   order by 1 desc)',
'           where rownum = 1;',
'        exception',
'          when others then',
'            :p200_tte_id := pq_constantes.fn_retorna_constante(0,',
'                                                               ''cn_tte_id_domicilio'');',
'        end;',
'    EXCEPTION',
'      WHEN no_data_found THEN',
'        IF :p200_per_primer_apellido IS NOT NULL THEN',
'          :p200_per_id                 := NULL;',
'          :p200_cli_id                 := NULL;',
'          :p200_per_correo_electronico := NULL;',
'          :p200_per_primer_nombre      := NULL;',
'          :p200_per_segundo_nombre     := NULL;',
'          :p200_per_primer_apellido    := NULL;',
'          :p200_per_segundo_apellido   := NULL;',
'          :p200_per_nombre             := NULL;',
'          :p200_direccion              := NULL;',
'          :p200_telefono               := NULL;',
'          :p200_per_nombre             := NULL;',
'          :p200_DIR_ID                 := NULL;',
'        END IF;',
'        :p200_tte_id := pq_constantes.fn_retorna_constante(0,',
'                                                               ''cn_tte_id_domicilio'');        ',
'    END;  ',
'ELSE',
'    :p200_per_id                 := NULL;',
'    :p200_cli_id                 := NULL;',
'    :p200_per_correo_electronico := NULL;',
'    :p200_per_primer_nombre      := NULL;',
'    :p200_per_segundo_nombre     := NULL;',
'    :p200_per_primer_apellido    := NULL;',
'    :p200_per_segundo_apellido   := NULL;',
'    :p200_per_nombre             := NULL;',
'    :p200_direccion              := NULL;',
'    :p200_telefono               := NULL;',
'    :p200_per_nombre             := NULL;',
'    :p200_DIR_ID                 := NULL;',
'  END IF;',
'  :p200_pol_id := 859; ',
'  :p200_bpr_id := 137;',
'  :p200_cei_id := 20;',
'  pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja''));',
'pq_inv_movimientos.pr_elimina_colecciones(pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_pago_cuota''));',
'',
'END;',
'',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request = ''busca_consumidor'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>117839102494354570977
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871353973084335899)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_detalle'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_pvd_id number;',
'begin',
'--raise_application_error(-20000,''IL''||:p200_ite_sec_id);',
'  -- pr_et(-9, ''Traza:A0'', NULL);',
'   pq_ven_punto_venta.pr_ven_carga_cabecera(pn_pvc_id => :p200_pvc_id,',
'                                           pv_vpd_session => :APP_SESSION,',
'                                           pn_usu_id => :F_USER_ID,',
'                                           pn_cli_id => :P200_CLI_ID,',
'                                           pn_uge_id => :F_UGE_ID,',
'                                           pn_age_id => :F_AGE_ID_AGENCIA,',
'                                           pv_error => :p0_error);                                          ',
' -- pr_et(-9, ''Traza:A1'', NULL);',
'  pq_ven_punto_venta.pr_ven_carga_detalle(pn_pvd_id => ln_pvd_id,',
'                                          pn_pvc_id => :p200_pvc_id,',
'                                          pn_emp_id => :f_emp_id,',
'                                          pn_tve_id => :p200_tve_id,',
'                                          pn_pol_id => :p200_pol_id,',
'                                          pn_ttr_id => :p200_ttr_id,',
'                                          pn_bpr_id => :p200_bpr_id,',
'                                          pn_ite_sku_id => :p200_ite_sku_id,',
'                                          pn_ite_sec_id => :p200_ite_sec_id,',
'                                          pv_ite_descripcion => :P200_DESCRIPCION_LARGA,',
'                                          pn_cei_id => :p200_cei_id,',
'                                          pn_vpd_cantidad => :P200_CANTIDAD,',
'                                          pn_stock_item => :P200_STOCK,',
'                                          pv_error => :p0_error);',
'',
'  -- pr_et(-9, ''Traza:A2'', NULL);',
'   :p200_ite_sku_id := null;',
'   :p200_stock := null;',
'   :p200_descripcion_larga := null;',
'   :p200_ite_sec_id := null;',
'   :P200_CANTIDAD := null;',
'   :P200_BARCODE := null;',
'end;                                          '))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>':request =''carga_detalle'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>117839100821814570973
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117868254602889142568)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_orden_pos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'  ln_valor_financiar NUMBER; --cn_var_id_valor_financiar',
'  ln_monto_pagar     NUMBER; --cn_var_id_total_cred_entrada   ',
'',
'  cn_var_id_valor_financiar NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                         ''cn_var_id_valor_financiar'');',
'',
'  cn_var_id_total_cred_entrada NUMBER := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                            ''cn_var_id_total_cred_entrada'');',
'',
'  ln_val_pago    NUMBER;',
'  ln_val_order   NUMBER;',
'  lv_com_id      VARCHAR2(200);',
'  ln_max_cli_fin NUMBER;',
'  ln_msg_cli_fin VARCHAR2(3000);',
'  ln_cli_id_fin  NUMBER := 2744580;',
'',
'  cn_tfp_id_desc_rol_pv number := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_tfp_id_desc_rol_punto_venta'');',
'  cn_tfp_id_credito_pv number := pq_constantes.fn_retorna_constante(:f_emp_id,',
'                                                                     ''cn_tfp_id_credito_punto_venta'');',
'                                                                     ',
'  lv_descuento_rol varchar2(1);',
'',
'  ln_valor_descuento_rol number;',
'',
'BEGIN',
'  /*',
'  Inicio Reserva',
'  ',
'  ****************************************************************************************************************',
'  ******************************************** NO TOCAR ESTE CODIGO!! ********************************************',
'  ****************************************************************************************************************',
'  ',
'  IF :F_USER_ID = 1234 THEN',
'      pq_ven_punto_venta.pr_graba_items_consolidar(pn_pvc_id         => :p200_pvc_id,',
'                                                   pn_pol_id         => :p200_pol_id,',
'                                                   pn_tve_id         => :p200_tve_id,',
'                                                   pn_emp_id         => :f_emp_id,',
'                                                   pn_uge_id         => :f_uge_id,',
'                                                   pn_usu_id         => :f_user_id,',
'                                                   pn_age_id_agencia => :p200_age_id_agencia,',
'                                                   pn_bpr_id         => :p200_bpr_id,',
'                                                   pv_error          => :p0_error);',
'                                                   ',
'       ',
'  ELSE*/',
'',
'  BEGIN',
'  ',
'    :P200_ERROR_FACTURA := NULL;',
'  ',
'    SELECT NVL(SUM(to_number(c006)), 0) valor',
'      INTO ln_val_pago',
'      FROM apex_collections co, asdm_tipos_formas_pago p',
'     WHERE collection_name = ''CO_MOV_CAJA''',
'       AND p.tfp_id = to_number(co.c005)',
'       and p.tfp_id not in (cn_tfp_id_desc_rol_pv,cn_tfp_id_credito_pv);',
'  ',
'    SELECT nvl(SUM(to_number(c006)), 0) valor',
'      INTO ln_valor_descuento_rol',
'      FROM apex_collections co, asdm_tipos_formas_pago p',
'     WHERE collection_name = ''CO_MOV_CAJA''',
'       AND p.tfp_id = to_number(co.c005)',
'       and p.tfp_id = cn_tfp_id_desc_rol_pv;',
'       ',
'    if ln_valor_descuento_rol > 0 then',
'     lv_descuento_rol := ''S'';',
'     :p200_desc_rol   := ''S'';',
'    else',
'     SELECT nvl(SUM(to_number(c006)), 0) valor',
'      INTO ln_valor_descuento_rol',
'      FROM apex_collections co, asdm_tipos_formas_pago p',
'     WHERE collection_name = ''CO_MOV_CAJA''',
'       AND p.tfp_id = to_number(co.c005)',
'       and p.tfp_id = cn_tfp_id_credito_pv;',
'     lv_descuento_rol := ''N'';',
'     :p200_desc_rol   := ''N'';',
'    end if;',
'       ',
'  ',
'    SELECT nvl(SUM(vp.vpv_valor), 0)',
'      INTO ln_valor_financiar',
'      FROM ven_punto_venta_det de, ven_var_punto_venta vp',
'     WHERE de.pvc_id = :p200_pvc_id',
'       AND vp.pvd_id = de.pvd_id',
'       AND de.vpd_estado_registro = 0',
'       AND vp.var_id = cn_var_id_valor_financiar;',
'  ',
'    SELECT nvl(SUM(vp.vpv_valor), 0)',
'      INTO ln_monto_pagar',
'      FROM ven_punto_venta_det de, ven_var_punto_venta vp',
'     WHERE de.pvc_id = :p200_pvc_id',
'       AND vp.pvd_id = de.pvd_id',
'       AND de.vpd_estado_registro = 0',
'       AND vp.var_id = cn_var_id_total_cred_entrada;',
'    ln_valor_financiar := round(ln_valor_financiar, 2);',
'    ln_val_pago        := round(ln_val_pago, 2);',
'    ln_monto_pagar     := round(ln_monto_pagar, 2);',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      raise_application_error(-20000, ''Error al validar valores'');',
'  END;',
'',
'  BEGIN',
'    SELECT p.par_valor, p.par_texto',
'      INTO ln_max_cli_fin, ln_msg_cli_fin',
'      FROM ven_parametros p',
'     WHERE p.par_id = 77',
'       AND p.par_estado_registro = 0;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      ln_max_cli_fin := ln_monto_pagar;',
'      ln_msg_cli_fin := NULL;',
'  END;',
'',
'  -- IF ln_monto_pagar = ln_val_pago THEN',
'  IF ln_monto_pagar > ln_max_cli_fin AND :p200_cli_id = ln_cli_id_fin THEN',
'    raise_application_error(-20000, ''Advertencia! '' || ln_msg_cli_fin);',
'  ELSE',
'    pq_ven_ordenes.pr_genera_orden_pos(pn_tse_id                => :p200_tse_id,',
'                                       pn_emp_id                => :f_emp_id,',
'                                       pn_tve_id                => :p200_tve_id,',
'                                       pn_rol_id                => :p0_rol,',
'                                       pn_uge_id                => :f_uge_id,',
'                                       pn_usu_id                => :f_user_id,',
'                                       pn_uge_id_gasto          => :f_uge_id_gasto,',
'                                       pn_age_id_agencia        => :p200_age_id_agencia,',
'                                       pn_pol_id                => :p200_pol_id,',
'                                       pn_cli_id_destino        => :p200_cli_id,',
'                                       pn_age_id_agente         => :p200_age_id_agente,',
'                                       pn_cot_id                => :p200_cot_id,',
'                                       pv_ord_tipo              => :p200_ord_tipo,',
'                                       pn_cli_id_origen         => NULL,',
'                                       pn_ord_plazo             => :p200_plazo_factura,',
'                                       pn_ede_id                => :p200_plan_tarjeta,',
'                                       pn_ord_desc_usu_autoriza => NULL,',
'                                       pn_ord_rol_ing           => :p0_rol,',
'                                       pn_ord_rol_autoriza      => NULL,',
'                                       pn_clave_autoriza        => NULL,',
'                                       pv_aprob_financiera      => ''S'',',
'                                       pn_entrada               => 0,',
'                                       pn_monto_pagar           => ln_monto_pagar,',
'                                       pn_valor_financiar       => ln_valor_descuento_rol,',
'                                       pn_ord_id                => :p200_ord_id,',
'                                       pn_ord_sec_id            => :p200_ord_sec_id,',
'                                       pv_observacion_est       => :p200_eor_observaciones,',
'                                       pv_com_apl_neg           => NULL,',
'                                       pn_tag_id                => :p200_tag_id,',
'                                       pn_pvc_id                => :p200_pvc_id,',
'                                       pn_bpr_id                => :p200_bpr_id,',
'                                       pv_desc_rol              => lv_descuento_rol,',
'                                       pv_error                 => :p0_error);',
'  ',
'    IF :p0_error IS NOT NULL THEN',
'      :p200_error_factura := :p200_error_factura || '' '' || :p0_error;',
'    END IF;',
'  ',
'    /* FPALOMEQUE - Se llama a procedimiento de ACALLE*/',
'    pq_ven_punto_venta.pr_factura_orden(pn_emp_id         => :f_emp_id,',
'                                        pn_uge_id         => :f_uge_id,',
'                                        pn_usu_id         => :f_user_id,',
'                                        pn_cli_id         => :p200_cli_id,',
'                                        pn_ord_id         => :p200_ord_id,',
'                                        pn_age_id_agente  => :p200_age_id_agente,',
'                                        pn_age_id_agencia => :p200_age_id_agencia,',
'                                        pn_ttr_id         => :p200_ttr_facturacion,',
'                                        pn_tgr_id         => :p200_tgr_facturacion,',
'                                        pn_pca_id         => :p200_pca_id,',
'                                        pn_valor_pagar    => ln_monto_pagar -',
'                                                             ln_valor_descuento_rol,',
'                                        pn_com_id         => lv_com_id,',
'                                        pv_error          => :p0_error);',
'                                        ',
'                                        ',
'                                        ',
'  ',
'    IF :p0_error IS NOT NULL THEN',
'      :p200_error_factura := :p200_error_factura || '' '' || :p0_error;',
'    END IF;',
'  ',
'    IF lv_com_id IS NOT NULL THEN',
'      :p200_com_id := lv_com_id;',
'    END IF;',
'    ',
'  END IF;',
'    /* ELSE',
'    raise_application_error(-20000,',
'                            ''Error, no coincide el valor pagodo con el total a pagar!'');',
'  END IF;*/',
'  --END IF;--Fin If reserva',
'  ',
'  begin',
'  -- Call the procedure',
'  pq_car_manejo_cupos.pr_act_cupo(pn_emp_id => :f_emp_id,',
'                                  pn_cli_id => :p200_cli_id,',
'                                  pv_error => :p0_error);',
'end;',
'',
'  ',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(114861262848894569943)
,p_internal_uid=>117836001451619377642
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117872062034143490664)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'limpia_campos'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117871329246313335824)
,p_internal_uid=>117839808882873725738
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117875881661040351371)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'limpia_col_pago'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'  lv_col_name varchar2(100) := ''CO_MOV_CAJA'';',
'BEGIN',
'  lb_existe := apex_collection.collection_exists(p_collection_name => lv_col_name);',
'  IF lb_existe THEN',
'    apex_collection.delete_collection(p_collection_name => lv_col_name);',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_imp.id(117871329246313335824)
,p_internal_uid=>117843628509770586445
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117871354385853335901)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_datos_iniciales'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_caja   NUMBER := 0;',
'  ln_agente asdm_agentes.age_id%TYPE;',
'BEGIN',
'  --pr_et(-9, ''Traza:L0'', NULL);',
'  SELECT d.pca_id',
'    INTO :p200_pca_id',
'    FROM asdm_usuario_puntos_emision a,',
'         asdm_puntos_emision         b,',
'         asdm_unidades_gestion       c,',
'         ven_periodos_caja           d',
'   WHERE b.pue_id = a.pue_id',
'     AND c.uge_id = b.uge_id',
'     AND d.pue_id = b.pue_id',
'     AND d.pca_estado_pc = ''A''',
'     AND a.upe_estado_registro = 0',
'     AND b.pue_estado_registro = 0',
'     AND c.uge_estado_registro = 0',
'     AND c.uge_id = :f_uge_id',
'     AND a.usu_id = :f_user_id',
'     AND b.emp_id = :f_emp_id;',
'',
'  IF :p200_pca_id > 0 THEN',
'    BEGIN',
'      SELECT *',
'        INTO :p200_age_id_agente, :p200_tag_id',
'        FROM (SELECT s.age_id, b.tag_id',
'                FROM asdm_agentes          s,',
'                     asdm_agentes_tagentes b,',
'                     kseg_e.kseg_usuarios  c',
'               WHERE b.age_id = s.age_id',
'                 AND c.usuario_id = s.usu_id',
'                 AND b.emp_id = :f_emp_id',
'                 AND s.emp_id = :f_emp_id',
'                 AND b.uge_id = :f_uge_id',
'                 AND s.usu_id = :f_user_id',
'                 AND b.ata_estado_registro = 0',
'                 AND s.age_estado_registro = 0',
'                 AND EXISTS (SELECT NULL',
'                        FROM asdm_tipos_agentes tag',
'                       WHERE tag.tag_vende = ''S''',
'                         AND tag.tag_estado_registro = 0',
'                         AND tag.tag_id = b.tag_id)',
'                 AND c.estado = 0',
'               ORDER BY b.ata_id DESC)',
'       WHERE rownum = 1;',
'       ',
'    EXCEPTION',
'      WHEN too_many_rows THEN',
'        raise_application_error(-20000,',
unistr('                                ''El usuario tiene mas de un agente asignado a esta Unidad de Gesti\00F3n'');'),
'      WHEN no_data_found THEN',
'        raise_application_error(-20000,',
unistr('                                ''El usuario No est\00E1 asignado como agente a esta Unidad de Gesti\00F3n'');'),
'    END;',
'  ',
'  ELSE',
'    raise_application_error(-20000,',
'                            ''La caja debe estar abierta y debe estar asignado a la caja como cajero para poder vender'');',
'  END IF;',
'  exception when others then',
'    raise_application_error(-20000,''LA CAJA ESTA CERRADA'');',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'P200_AGE_ID_AGENTE'
,p_process_when_type=>'ITEM_IS_NULL'
,p_internal_uid=>117839101234583570975
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117875882153543351376)
,p_process_sequence=>70
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprime_factura'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'BEGIN',
'--:p200_com_id := 5482085;',
'/*if :f_user_id = 5722 then',
'null;',
'else*/',
'  IF nvl(:P200_COM_ID, 0) <> 0 AND :p0_error IS NULL and :p200_error_factura IS NULL THEN',
'    asdm_p.pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''Factura'',',
'                                                   pv_nombre_parametros => ''PN_COM_ID'',',
'                                                   pv_valor_parametros  => :P200_COM_ID,',
'                                                   pn_emp_id            => :f_emp_id,',
'                                                   pv_formato           => ''pdf'',',
'                                                   pv_error             => :p0_error);',
'                                                   ',
'    IF :P200_DESC_ROL = ''S'' THEN',
'      asdm_p.pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CAR_AUTORIZA_DESC_ROL_POS'',',
'                                                     pv_nombre_parametros => ''pn_com_id'',',
'                                                     pv_valor_parametros  => :P200_COM_ID,',
'                                                     pn_emp_id            => :f_emp_id,',
'                                                     pv_formato           => ''pdf'',',
'                                                     pv_error             => :p0_error);',
'      :p200_desc_rol   := NULL;',
'    END IF;',
'  ELSE',
'    raise_application_error(-20000,',
'                            ''Error al generar Factura. '' || :p0_error || '' '' || SQLERRM || '' '' || :p200_error_factura);',
'  END IF;',
'',
'--end if;-------------------',
'',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    raise_application_error(-20000,',
'                            ''Error al generar reporte jasper (Factura): '' ||',
'                            SQLERRM);',
'END;',
''))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request =''imprime_fac'' and :P200_COM_ID IS NOT NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>117843629002273586450
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117876045612307807230)
,p_process_sequence=>80
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'limpia_campos_1'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'imprime_fac'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>117843792461038042304
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(117876045664847807231)
,p_process_sequence=>90
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'limpia_col_pago_1'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  lb_existe BOOLEAN;',
'  lv_col_name varchar2(100) := ''CO_MOV_CAJA'';',
'BEGIN',
'  lb_existe := apex_collection.collection_exists(p_collection_name => lv_col_name);',
'  IF lb_existe THEN',
'    apex_collection.delete_collection(p_collection_name => lv_col_name);',
'  END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'imprime_fac'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_internal_uid=>117843792513578042305
);
wwv_flow_imp.component_end;
end;
/
