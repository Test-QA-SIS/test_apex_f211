prompt --application/pages/page_00108
begin
--   Manifest
--     PAGE: 00108
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>108
,p_name=>'NOTAS CREDITO POR MONTOS'
,p_step_title=>'NOTAS CREDITO POR MONTOS'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(45563761429349148)
,p_plug_name=>'NOTA DE CREDITO POR MONTOS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(45566777401349164)
,p_plug_name=>'NOTAS DE CREDITO APLICADAS'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'       select *',
'  from v_ven_comprobantes c where c.COM_ID_REF=:p108_com_id',
'  and c.COM_ESTADO_REGISTRO=PQ_CONSTANTES.FN_RETORNA_CONSTANTE(NULL,''cv_estado_reg_activo'')'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(45566970037349166)
,p_name=>'NOTAS DE CREDITO APLICADAS'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ADMIN'
,p_internal_uid=>13313818767584240
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569872362349190)
,p_db_column_name=>'COM_ID'
,p_display_order=>1
,p_column_identifier=>'O'
,p_column_label=>'Com Id Nota Credito'
,p_column_link=>'f?p=&APP_ID.:70:&SESSION.::&DEBUG.::P70_COM_ID_NOTA:#COM_ID#'
,p_column_linktext=>'#COM_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567065680349171)
,p_db_column_name=>'USU_ID'
,p_display_order=>2
,p_column_identifier=>'A'
,p_column_label=>'Usu Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'USU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568556661349176)
,p_db_column_name=>'TRX_FECHA_EJECUCION'
,p_display_order=>3
,p_column_identifier=>'B'
,p_column_label=>'Trx Fecha Ejecucion'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_FECHA_EJECUCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568671074349176)
,p_db_column_name=>'TTR_ID'
,p_display_order=>4
,p_column_identifier=>'C'
,p_column_label=>'Ttr Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568769181349176)
,p_db_column_name=>'TTR_DESCRIPCION'
,p_display_order=>5
,p_column_identifier=>'D'
,p_column_label=>'Ttr Descripcion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'TTR_DESCRIPCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568856925349176)
,p_db_column_name=>'POL_ID'
,p_display_order=>6
,p_column_identifier=>'E'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568964631349176)
,p_db_column_name=>'POL_DESCRIPCION_LARGA'
,p_display_order=>7
,p_column_identifier=>'F'
,p_column_label=>'Pol Descripcion Larga'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POL_DESCRIPCION_LARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569059028349180)
,p_db_column_name=>'PUE_NOMBRE'
,p_display_order=>8
,p_column_identifier=>'G'
,p_column_label=>'Pue Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569167226349180)
,p_db_column_name=>'UGE_ID_FACTURA'
,p_display_order=>9
,p_column_identifier=>'H'
,p_column_label=>'Uge Id Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569253048349180)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>10
,p_column_identifier=>'I'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569351733349180)
,p_db_column_name=>'AGE_ID_AGENCIA'
,p_display_order=>11
,p_column_identifier=>'J'
,p_column_label=>'Age Id Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENCIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569466065349185)
,p_db_column_name=>'AGE_NOMBRE_COMERCIAL'
,p_display_order=>12
,p_column_identifier=>'K'
,p_column_label=>'Age Nombre Comercial'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_NOMBRE_COMERCIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569555650349185)
,p_db_column_name=>'NOMBRE_COMPLETO'
,p_display_order=>13
,p_column_identifier=>'L'
,p_column_label=>'Nombre Completo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_COMPLETO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569673686349185)
,p_db_column_name=>'PER_TIPO_IDENTIFICACION'
,p_display_order=>14
,p_column_identifier=>'M'
,p_column_label=>'Per Tipo Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_TIPO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569768918349187)
,p_db_column_name=>'PER_NRO_IDENTIFICACION'
,p_display_order=>15
,p_column_identifier=>'N'
,p_column_label=>'Per Nro Identificacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45569970605349191)
,p_db_column_name=>'EMP_ID'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570069865349191)
,p_db_column_name=>'TRX_ID'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Trx Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570161588349191)
,p_db_column_name=>'PUE_ID'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570274895349191)
,p_db_column_name=>'CLI_ID'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570362444349191)
,p_db_column_name=>'DIR_ID_ENVIAR_FACTURA'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Dir Id Enviar Factura'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'DIR_ID_ENVIAR_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570478239349191)
,p_db_column_name=>'COM_TIPO'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Com Tipo'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_TIPO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570573144349191)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Com Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570664086349192)
,p_db_column_name=>'UGE_NUM_SRI'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Uge Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570777544349192)
,p_db_column_name=>'PUE_NUM_SRI'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Pue Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45570878348349192)
,p_db_column_name=>'COM_ESTADO_REGISTRO'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Com Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567160962349174)
,p_db_column_name=>'AGE_ID_AGENTE'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Age Id Agente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_ID_AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567258017349174)
,p_db_column_name=>'AGENTE'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Agente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567361908349174)
,p_db_column_name=>'ORD_ID'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Ord Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ORD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567455999349174)
,p_db_column_name=>'COM_OBSERVACION'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Com Observacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_OBSERVACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567583010349174)
,p_db_column_name=>'COM_SALDO'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Com Saldo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_SALDO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567682088349174)
,p_db_column_name=>'COM_PLAZO'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Com Plazo'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567751976349174)
,p_db_column_name=>'EDE_ID'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567863513349175)
,p_db_column_name=>'COM_NRO_IMPRESION'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Com Nro Impresion'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NRO_IMPRESION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45567968984349175)
,p_db_column_name=>'CIN_ID'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'Cin Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CIN_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568055484349175)
,p_db_column_name=>'COM_ID_REF'
,p_display_order=>36
,p_column_identifier=>'AJ'
,p_column_label=>'Com Id Ref'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID_REF'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568169506349175)
,p_db_column_name=>'TSE_ID'
,p_display_order=>37
,p_column_identifier=>'AK'
,p_column_label=>'Tse Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TSE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568256249349175)
,p_db_column_name=>'COM_NUMERO'
,p_display_order=>38
,p_column_identifier=>'AL'
,p_column_label=>'Com Numero'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568374992349175)
,p_db_column_name=>'PER_ID'
,p_display_order=>39
,p_column_identifier=>'AM'
,p_column_label=>'Per Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PER_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(45568471903349175)
,p_db_column_name=>'FOL_NRO_FOLIO'
,p_display_order=>40
,p_column_identifier=>'AN'
,p_column_label=>'Fol Nro Folio'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'FOL_NRO_FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(45570979188349192)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'133179'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'COM_ID:TRX_FECHA_EJECUCION:TTR_ID:TTR_DESCRIPCION:PUE_NOMBRE:UGE_ID_FACTURA:UNIDAD_GESTION:AGE_ID_AGENCIA:AGE_NOMBRE_COMERCIAL:NOMBRE_COMPLETO:PER_TIPO_IDENTIFICACION:PER_NRO_IDENTIFICACION:TRX_ID:PUE_ID:COM_TIPO:COM_FECHA:COM_NUMERO:UGE_NUM_SRI:PUE_'
||'NUM_SRI:AGENTE:COM_OBSERVACION:COM_SALDO:COM_NRO_IMPRESION:COM_ID_REF'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(524727383857165164)
,p_name=>'REPORT_DETALLE_FACTURAS'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT co.c001 CLI_ID,',
'       co.c002 NOMBRE,',
'       co.c003 VALOR,',
'       co.c004 NRO_DET,',
'       co.c005 FACTURA_MAS_ALTA,',
'       --co.c006 AGE,',
'       co.c007 INICIO,',
'       co.c008 FIN,',
'      -- co.c009 GENERAR_NC,',
'       --co.c010 CONSULTA,',
'       co.c011 com_id,',
'       co.c012 com_fecha,',
'       co.c013 com_tipo',
'       FROM apex_collections co',
' WHERE co.collection_name = ''COLL_NC_MONTOS_CLIENTE''',
'order by to_number(co.c003) DESC'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524727664172165178)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524727751856165180)
,p_query_column_id=>2
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>2
,p_column_heading=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524727865725165180)
,p_query_column_id=>3
,p_column_alias=>'VALOR'
,p_column_display_sequence=>4
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524727974993165181)
,p_query_column_id=>4
,p_column_alias=>'NRO_DET'
,p_column_display_sequence=>5
,p_column_heading=>'NRO_DET'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524728051905165181)
,p_query_column_id=>5
,p_column_alias=>'FACTURA_MAS_ALTA'
,p_column_display_sequence=>3
,p_column_heading=>'FACTURA_MAS_ALTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524728256565165181)
,p_query_column_id=>6
,p_column_alias=>'INICIO'
,p_column_display_sequence=>6
,p_column_heading=>'INICIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524728374133165181)
,p_query_column_id=>7
,p_column_alias=>'FIN'
,p_column_display_sequence=>7
,p_column_heading=>'FIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524728679760165181)
,p_query_column_id=>8
,p_column_alias=>'COM_ID'
,p_column_display_sequence=>8
,p_column_heading=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525179260877143272)
,p_query_column_id=>9
,p_column_alias=>'COM_FECHA'
,p_column_display_sequence=>9
,p_column_heading=>'Com Fecha'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525179376681143276)
,p_query_column_id=>10
,p_column_alias=>'COM_TIPO'
,p_column_display_sequence=>10
,p_column_heading=>'Com Tipo'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(524730778840201538)
,p_name=>'general'
,p_template=>wwv_flow_imp.id(270525766496046669)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT co.c001 CLI_ID,',
'       co.c002 NOMBRE,',
'       co.c003 VALOR,',
'       co.c004 NRO_DET,',
'       co.c005 FACTURA_MAS_ALTA,',
'       co.c006 AGE,',
'       co.c007 INICIO,',
'       co.c008 FIN,',
'       co.c009 GENERAR_NC,',
'       co.c010 CONSULTA',
'       FROM apex_collections co',
' WHERE co.collection_name = ''COLL_PARA_NC_MONTOS'''))
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731068053201542)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731163953201542)
,p_query_column_id=>2
,p_column_alias=>'NOMBRE'
,p_column_display_sequence=>2
,p_column_heading=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731251912201542)
,p_query_column_id=>3
,p_column_alias=>'VALOR'
,p_column_display_sequence=>3
,p_column_heading=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731355107201542)
,p_query_column_id=>4
,p_column_alias=>'NRO_DET'
,p_column_display_sequence=>4
,p_column_heading=>'NRO_DET'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731478302201542)
,p_query_column_id=>5
,p_column_alias=>'FACTURA_MAS_ALTA'
,p_column_display_sequence=>5
,p_column_heading=>'FACTURA_MAS_ALTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731556349201542)
,p_query_column_id=>6
,p_column_alias=>'AGE'
,p_column_display_sequence=>6
,p_column_heading=>'AGE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731651312201542)
,p_query_column_id=>7
,p_column_alias=>'INICIO'
,p_column_display_sequence=>7
,p_column_heading=>'INICIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731782729201542)
,p_query_column_id=>8
,p_column_alias=>'FIN'
,p_column_display_sequence=>8
,p_column_heading=>'FIN'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731861657201542)
,p_query_column_id=>9
,p_column_alias=>'GENERAR_NC'
,p_column_display_sequence=>9
,p_column_heading=>'GENERAR_NC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(524731954276201542)
,p_query_column_id=>10
,p_column_alias=>'CONSULTA'
,p_column_display_sequence=>10
,p_column_heading=>'CONSULTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(525510963139624873)
,p_name=>'Porcentajes de descuentos'
,p_template=>wwv_flow_imp.id(270523372472046668)
,p_display_sequence=>50
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT *',
'    FROM asdm_e.ven_montos_descuentos'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525511253671624908)
,p_query_column_id=>1
,p_column_alias=>'MDE_ID'
,p_column_display_sequence=>1
,p_column_heading=>'MDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525511354020624910)
,p_query_column_id=>2
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525511482457624910)
,p_query_column_id=>3
,p_column_alias=>'MDE_MONTO_INICIAL'
,p_column_display_sequence=>3
,p_column_heading=>'MDE_MONTO_INICIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525511560544624910)
,p_query_column_id=>4
,p_column_alias=>'MDE_MONTO_FINAL'
,p_column_display_sequence=>4
,p_column_heading=>'MDE_MONTO_FINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525511651775624910)
,p_query_column_id=>5
,p_column_alias=>'MDE_PORCENTAJE'
,p_column_display_sequence=>5
,p_column_heading=>'MDE_PORCENTAJE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(525511783902624910)
,p_query_column_id=>6
,p_column_alias=>'MDE_ESTADO_REGISTRO'
,p_column_display_sequence=>6
,p_column_heading=>'MDE_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(45563973452349153)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_button_name=>'GUARDAR'
,p_button_static_id=>'grabar'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Guardar'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(525182783010222317)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(45566777401349164)
,p_button_name=>'CANCELAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar al listado'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:113:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(615720477999150903)
,p_branch_name=>'br_regresar'
,p_branch_action=>'f?p=&APP_ID.:113:&SESSION.:carga:&DEBUG.:108::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>':request=''regresar'''
,p_branch_condition_text=>'PLSQL'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45564371288349156)
,p_name=>'P108_UGE_ID'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Uge Id'
,p_source=>'F_UGE_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45564560009349159)
,p_name=>'P108_COM_ID_NOTA'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Com Id Nota'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45564753996349159)
,p_name=>'P108_SECUENCIA'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Secuencia'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onChange=''doSubmit()'''
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>':P108_PUE_ELECTRONICO=''E'''
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45564970058349159)
,p_name=>'P108_CLIENTE'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Cliente'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select per.per_nro_identificacion||'' '' || per.per_primer_nombre || '' '' || per.per_segundo_nombre || '' '' ||',
'              per.per_primer_apellido || '' '' || per.per_segundo_apellido clientes',
'         from asdm_personas per',
'        where per.per_id in',
'              (select cli.per_id',
'                 from asdm_clientes cli',
'                where cli.cli_id in (select c.cli_id',
'                                       from ven_comprobantes c',
'                                      where c.com_id = :P108_COM_ID));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>100
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45565160058349160)
,p_name=>'P108_CONCEPTO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Concepto Documento'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>50
,p_cMaxlength=>4000
,p_cHeight=>3
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>'onKeyUp="javascript:this.value = this.value.toUpperCase( );"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45565380476349161)
,p_name=>'P108_TOTAL'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Total Ventas'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45565751849349161)
,p_name=>'P108_FECHA_DOCUMENTO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_item_default=>'sysdate'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Documento'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45565977265349162)
,p_name=>'P108_CAJERO'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Cajero'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT  per.per_primer_nombre||'' ''||per.per_segundo_nombre||'' ''||per.per_primer_apellido||'' ''||per.per_segundo_apellido cajero',
'      FROM asdm_agentes age, asdm_personas per',
'     WHERE age.per_id = per.per_id',
'       AND age.usu_id = :F_USER_ID',
'       AND age.emp_id = :F_EMP_ID      ',
'       AND per.emp_id = :F_EMP_ID',
'       AND age.age_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'')',
'       AND per.per_estado_registro = pq_constantes.fn_retorna_constante(NULL,''cv_estado_reg_activo'');'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45566383931349163)
,p_name=>'P108_PCA_ID'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Pca Id'
,p_source=>'F_PCA_ID'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45566571392349163)
,p_name=>'P108_SECUENCIA_ACTUAL'
,p_item_sequence=>65
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Secuencia Actual'
,p_source=>'P0_FOL_SEC_ACTUAL'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45578872178801968)
,p_name=>'P108_NOMBRE_CLIENTE'
,p_item_sequence=>45
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Nombre Cliente'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45585779387950583)
,p_name=>'P108_VALOR_NC'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor NC + IVA'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ((mde_porcentaje * :P108_TOTAL)/100)*(1+:p108_porcentaje_iva)',
'    FROM asdm_e.ven_montos_descuentos',
'   WHERE ROUND(:P108_TOTAL) BETWEEN mde_monto_inicial AND mde_monto_final'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font:14pt verdana;color:blue"'
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_read_only_when=>'1=1'
,p_read_only_when2=>'PLSQL'
,p_read_only_when_type=>'EXPRESSION'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45591373923044319)
,p_name=>'P108_FECHA_INICIO'
,p_item_sequence=>140
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45591570903045693)
,p_name=>'P108_FECHA_FIN'
,p_item_sequence=>150
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45603457274167567)
,p_name=>'P108_PUE_ELECTRONICO'
,p_item_sequence=>160
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45620556786502067)
,p_name=>'P108_COM_ID_FACTURA'
,p_item_sequence=>170
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Listado de Facturas'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''Com_id: '' || v.com_id || ''- folio: '' || v.uge_num_sri || ''-'' ||',
'       v.pue_num_sri || ''-'' || v.com_numero || '' F:'' || v.com_fecha d,',
'       v.com_id r',
'  FROM ven_comprobantes v, asdm_clientes cli',
' WHERE v.com_tipo =',
'       pq_constantes.fn_retorna_constante(0, ''cv_com_tipo_factura'')',
'   AND v.cli_id = cli.cli_id',
'   AND v.emp_id = cli.emp_id',
'   AND v.emp_id = :f_emp_id',
'   AND v.cli_id = :p108_cliente',
'   AND v.com_fecha BETWEEN to_date(:p108_fecha_inicio, ''dd/mm/yyyy'') AND',
'       to_date(:p108_fecha_fin, ''dd/mm/yyyy'') + 0.99999',
'   AND v.com_estado_registro =',
'       pq_constantes.fn_retorna_constante(0, ''cv_estado_reg_activo'')',
'   AND EXISTS (SELECT NULL',
'          FROM ven_comprobantes_det       d,',
'               inv_items_categorias       ic,',
'               ven_parametrizacion_cat_nc pc',
'         WHERE d.com_id = v.com_id',
'           AND d.ite_sku_id = ic.ite_sku_id',
'           AND ic.cat_id = pc.cat_id',
'           AND pc.emp_id = :f_emp_id',
'           AND d.emp_id = :f_emp_id)',
''))
,p_lov_display_null=>'YES'
,p_cSize=>65
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_03=>'N'
,p_attribute_04=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45620774890508942)
,p_name=>'P108_VALOR_ALTO'
,p_item_sequence=>180
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Valor factura alta'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(45701054056797670)
,p_name=>'P108_NRO_FACTURAS'
,p_item_sequence=>190
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'Nro Facturas'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(88798261483397874)
,p_name=>'P108_ACCION'
,p_item_sequence=>200
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_use_cache_before_default=>'NO'
,p_item_default=>'otro_valor'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(524785763041951636)
,p_name=>'P108_COM_ID_FA'
,p_item_sequence=>175
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_prompt=>'com_id-afectar'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_tag_attributes=>'style="font:14pt verdana;color:blue"'
,p_begin_on_new_line=>'N'
,p_colspan=>2
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(525510471549603147)
,p_name=>'P108_PORCENTAJE_IVA'
,p_item_sequence=>210
,p_item_plug_id=>wwv_flow_imp.id(45563761429349148)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(45571260023349196)
,p_validation_name=>'P108_SECUENCIA'
,p_validation_sequence=>10
,p_validation=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if trim(:P108_SECUENCIA) = trim(:P0_FOL_SEC_ACTUAL) then',
'return null;',
'else',
'return ''La secuencia ingresada no corresponde a la secuencia del folio.  Revisar Secuencia'';',
'end if;'))
,p_validation2=>'PLSQL'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_imp.id(45564753996349159)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(88935151768044227)
,p_name=>'br_reporte'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'#grabar'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'click'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(88935477803044235)
,p_event_id=>wwv_flow_imp.id(88935151768044227)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P108_ACCION'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'req_graba'
,p_attribute_09=>'N'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(88940565283123830)
,p_name=>'da_graba_imprime'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P108_ACCION'
,p_condition_element=>'P108_ACCION'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'req_graba'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(88940868284123831)
,p_event_id=>wwv_flow_imp.id(88940565283123830)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'doSubmit(''&P108_ACCION.'');'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(88950961360964829)
,p_name=>'ad_refresh'
,p_event_sequence=>50
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(88951276559964830)
,p_event_id=>wwv_flow_imp.id(88950961360964829)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P108_ACCION'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(615749654043424952)
,p_name=>'ad_onchange_P108_COM_ID_FACTURA'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P108_COM_ID_FACTURA'
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'change'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(615749983225424957)
,p_event_id=>wwv_flow_imp.id(615749654043424952)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P108_COM_ID_FA:=:P108_COM_ID_FACTURA;',
'',
'SELECT DISTINCT v.vcd_valor_variable val INTO :p108_porcentaje_iva',
'  FROM ven_comprobantes_det d, ven_var_comprobantes_det v',
' WHERE d.com_id = :p108_com_id_fa',
'   AND d.cde_id = v.cde_id',
'   AND v.var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_porc_iva'');--129'))
,p_attribute_02=>'P108_COM_ID_FACTURA,P108_COM_ID_FA,F_EMP_ID,P108_PORCENTAJE_IVA'
,p_attribute_03=>'P108_COM_ID_FA,P108_PORCENTAJE_IVA,P108_VALOR_NC'
,p_attribute_04=>'N'
,p_attribute_05=>'PLSQL'
,p_wait_for_result=>'Y'
);
wwv_flow_imp_page.create_page_da_event(
 p_id=>wwv_flow_imp.id(615750075083449950)
,p_name=>'da_P108_COM_ID_NOTA'
,p_event_sequence=>70
,p_bind_type=>'bind'
,p_execution_type=>'IMMEDIATE'
,p_bind_event_type=>'ready'
,p_display_when_type=>'ITEM_IS_NOT_NULL'
,p_display_when_cond=>'P108_COM_ID_NOTA'
);
wwv_flow_imp_page.create_page_da_action(
 p_id=>wwv_flow_imp.id(615750367886449952)
,p_event_id=>wwv_flow_imp.id(615750075083449950)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_imp.id(45563761429349148)
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(524726766495131786)
,p_process_sequence=>3
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_col_com_clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    la_nombres              apex_application_global.vc_arr2;',
'    la_valores              apex_application_global.vc_arr2;',
'    lv_nombres              VARCHAR2(2000);',
'    lv_valores              VARCHAR2(2000);',
'    lv_query                VARCHAR2(30000);',
'BEGIN',
'  lv_nombres:=''cv_com_tipo_fa:cv_com_tipo_nc:pd_desde:pd_hasta:cv_formato:f_emp_id:cn_var_id_venta_neta:f_age_id_agencia''; ',
'  lv_nombres:= lv_nombres||'':cn_tse_id_mayoreo:cv_estado_reg_activo:cv_espacio:pn_cli_id'';  --:cn_var_id_total_cred_entrada',
'  lv_valores:=pq_constantes.fn_retorna_constante(0,''cv_com_tipo_factura'');',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(0,''cv_com_tipo_nc'');',
'  lv_valores:=lv_valores||'':''||:p108_fecha_inicio;',
'  lv_valores:=lv_valores||'':''||:p108_fecha_fin;',
'  lv_valores:=lv_valores||'':''||''dd/mm/yyyy'';',
'  lv_valores:=lv_valores||'':''||:f_emp_id;',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_var_id_venta_neta'');',
'  lv_valores:=lv_valores||'':''||:f_age_id_agencia;',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(0, ''cn_tse_id_mayoreo'');',
'  lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(0,''cv_estado_reg_activo'');',
'  lv_valores:=lv_valores||'':''||'' '';',
'  lv_valores:=lv_valores||'':''||:P108_CLIENTE;',
'  --lv_valores:=lv_valores||'':''||pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total_cred_entrada'');',
'  ',
'lv_query:= ''SELECT c.cli_id,',
'       pe.per_razon_social || pe.per_primer_nombre || :cv_espacio ||',
'       pe.per_segundo_nombre || :cv_espacio || pe.per_primer_apellido || :cv_espacio ||',
'       pe.per_segundo_apellido nombre,',
'       round(SUM(CASE',
'                   WHEN c.com_tipo = :cv_com_tipo_fa THEN',
'                    vd.vcd_valor_variable',
'                   WHEN c.com_tipo = :cv_com_tipo_nc THEN',
'                    vd.vcd_valor_variable * -1',
'                 END),',
'             2) valor,',
'       round(COUNT(vd.vcd_valor_variable), 2) nro_det,',
'       round(SUM(vd.vcd_valor_variable), 2) factura_mas_alta,',
'       NULL age,',
'       ',
'        :pd_desde,',
'        :pd_hasta,',
'       NULL generar_nc,',
'       1 consulta,',
'       c.com_id,',
'       c.com_fecha,',
'       c.com_tipo',
'  FROM ven_comprobantes         c,',
'       ven_comprobantes_det     d,',
'       ven_var_comprobantes_det vd,',
'       inv_items_categorias     ic,',
'       asdm_clientes            cl,',
'       asdm_personas            pe',
' WHERE c.com_fecha BETWEEN to_date(:pd_desde,:cv_formato) AND to_date(:pd_hasta,:cv_formato) +  0.99999   ',
'   AND c.emp_id = :f_emp_id',
'   AND d.com_id = c.com_id',
'   AND d.ite_sku_id = ic.ite_sku_id',
'   AND d.cde_id = vd.cde_id',
'   AND cl.cli_id = :pn_cli_id',
'   AND vd.var_id = :cn_var_id_venta_neta',
'   AND c.age_id_agencia = :f_age_id_agencia',
'   AND ic.cat_id IN (SELECT cat_id FROM asdm_e.ven_parametrizacion_cat_nc)',
'   AND cl.cli_id = c.cli_id      ',
'   AND cl.per_id = pe.per_id',
'   AND c.tse_id = :cn_tse_id_mayoreo       ',
'   AND d.ite_sku_id NOT IN',
'       (SELECT ite_sku_id',
'          FROM asdm_e.ven_items_excl_nc nc',
'         WHERE c.com_fecha BETWEEN nc.ien_fecha_inicio AND',
'               nc.ien_fecha_fin + 0.99999',
'           AND nc.ien_estado_registro =:cv_estado_reg_activo )',
'   AND not exists (SELECT null ',
'                  FROM asdm_e.ven_comprobantes_nc_montos nn ',
'                  where nn.cde_id = d.cde_id)',
'   HAVING COUNT(UNIQUE(c.age_id_agencia)) = 1',
'',
' GROUP BY c.cli_id,',
'          pe.per_primer_nombre,',
'          pe.per_segundo_nombre,',
'          pe.per_primer_apellido,',
'          pe.per_segundo_apellido,',
'          pe.per_razon_social,',
'          c.com_id,',
'       c.com_fecha,',
'       c.com_tipo'';',
'  --  pr_pruebas_et(556,''AAA'',lv_nombres ||'' VALUES '' || lv_valores);',
'    la_nombres := apex_util.string_to_table(lv_nombres, '':'');',
'    la_valores := apex_util.string_to_table(lv_valores, '':'');',
'',
'    IF apex_collection.collection_exists(p_collection_name => ''COLL_NC_MONTOS_CLIENTE'') THEN',
'      apex_collection.delete_collection(''COLL_NC_MONTOS_CLIENTE'');',
'    END IF;',
'  ',
'    IF length(lv_nombres) > 0 THEN',
'      apex_collection.create_collection_from_query_b(p_collection_name => ''COLL_NC_MONTOS_CLIENTE'',',
'                                                     p_query           => lv_query,',
'                                                     p_names           => la_nombres,',
'                                                     p_values          => la_valores);',
'    END IF;',
'',
'begin',
'SELECT nombre, factura_mas_alta, com_id,com_id',
'  INTO :p108_nombre_cliente, :p108_valor_alto, :p108_com_id_fa, :p108_com_id_factura',
'  FROM (SELECT co.c002 nombre, co.c003 factura_mas_alta, co.c011 com_id',
'          FROM apex_collections co',
'         WHERE co.collection_name = ''COLL_NC_MONTOS_CLIENTE''',
'         ORDER BY to_number(co.c003) DESC)',
' WHERE rownum = 1;',
'',
'SELECT SUM(co.c003) valor, COUNT(co.c003) nro_facturas',
'  INTO :p108_total, :p108_nro_facturas',
'  FROM apex_collections co',
' WHERE co.collection_name = ''COLL_NC_MONTOS_CLIENTE''',
' ORDER BY to_number(co.c003) DESC;',
'exception',
'when no_data_found then',
'   :p108_nombre_cliente:=NULL;',
'   :p108_valor_alto:=NULL;',
'   :p108_com_id_fa:= NULL;',
'   :P108_CLIENTE:= NULL;',
'   :p0_error:=''No existe facturas para el periodo actual, verifique las fechas de la facturas'';',
'   raise_application_error(-20000,:p0_error||sqlerrm);',
'when others then ',
'   raise_application_error(-20000,:p0_error||sqlerrm);',
'end;',
':p108_com_id_factura:=:p108_com_id_fa;',
'EXCEPTION',
'  WHEN OTHERS THEN ',
'    raise_application_error(-20000,''Error.''||sqlerrm);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':p108_nombre_cliente IS NULL AND  (:P108_ACCION <> ''req_graba'' or :P108_ACCION IS NULL)'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>492473615225366860
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(45663578528101545)
,p_process_sequence=>5
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_GRABA_NC'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_com_id        ven_comprobantes.com_id%TYPE;',
'  ln_valor_factura NUMBER;',
'  ln_num_nc        NUMBER;',
'  ln_mnc_id_dscto_vol        asdm_motivos_nc.mnc_id%TYPE;',
'  ln_mnc_id_rebaja           asdm_motivos_nc.mnc_id%TYPE;',
'  ln_var_id_cred   ppr_variables.var_id%TYPE;',
'BEGIN',
'  IF :p108_com_id_fa IS NULL THEN',
'    raise_application_error(-20000,',
'                            ''Seleccione una factura para realizar la nota de credito'');',
'  END IF;',
'                            ',
'  ln_mnc_id_dscto_vol:= pq_constantes.fn_retorna_constante(:f_emp_id,''cn_mnc_id_descuento_volumen'');',
'  ln_var_id_cred:= pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_total_cred_entrada'');',
'  ln_mnc_id_rebaja:=  pq_constantes.fn_retorna_constante(:f_emp_id,''cn_mnc_id_rebaja'');',
'',
' SELECT round(vco_valor_variable, 2)',
'    INTO ln_valor_factura',
'    FROM ven_var_comprobantes',
'   WHERE com_id = :p108_com_id_fa',
'     AND var_id = ln_var_id_cred;',
'',
'  SELECT COUNT(com_id)',
'    INTO ln_num_nc',
'    FROM ven_comprobantes',
'   WHERE com_id_ref = :p108_com_id_fa',
'     AND mnc_id = ln_mnc_id_dscto_vol;',
'     ',
'  IF ln_num_nc > 0 THEN',
'    raise_application_error(-20000,',
'                            ''Ya existe una nc de montos con esta factura. '');',
'  END IF;',
'',
'  IF :p108_valor_nc > ln_valor_factura THEN',
'    raise_application_error(-20000,',
'                            ''EL VALOR DE LA NC: '' || :p108_valor_nc ||',
'                            '', es mayor al valor de la factura: '' ||',
'                            ln_valor_factura);',
'  END IF;',
'  ',
'  SELECT COUNT(com_id)',
'    INTO ln_num_nc',
'    FROM ven_comprobantes',
'   WHERE com_id_ref = :p108_com_id_fa--:p70_com_id',
'     AND mnc_id = ln_mnc_id_rebaja;',
'  ',
'  IF ln_num_nc > 0 THEN',
'    raise_application_error(-20000,',
'                            ''YA SE ENCUENTRA RELIZADA UNA NC POR REBAJA'');',
'  END IF;',
'',
'  IF :p108_secuencia = :p108_secuencia_actual THEN',
'    pq_ven_ncredito_rebaja.pr_genera_nc_montos(pn_com_id          => :p108_com_id_nota,',
'                                                pn_cli_id          => :p108_cliente,',
'                                                pd_fecha_inicio    => :p108_fecha_inicio,',
'                                                pd_fecha_fin       => :p108_fecha_fin,',
'                                                pn_emp_id          => :f_emp_id,',
'                                                pn_uge_id          => :f_uge_id,',
'                                                pn_usu_id          => :f_user_id,',
'                                                pn_age_id_agencia  => :f_age_id_agencia,',
'                                                pn_fol_sec_actual  => :p0_fol_sec_actual,',
'                                                pv_pue_num_sri     => :p0_pue_num_sri,',
'                                                pv_uge_num_est_sri => :p0_uge_num_est_sri,',
'                                                pn_pue_id          => :p0_pue_id,',
'                                                pn_monto_nc        => :p108_valor_nc,',
'                                                pn_monto_total_fac => :p108_total,',
'                                                pv_com_observacion => :p108_concepto,',
'                                                pn_com_id_fac      => :p108_com_id_fa,',
'                                                pn_num_comp        => :p108_nro_facturas,',
'                                                pv_error           => :p0_error);',
'  ',
'    pq_ven_ncredito_rebaja.pr_imprimir_nc(:f_emp_id,',
'                                          :p108_com_id_nota,',
'                                          :p0_error);',
'',
'',
'    :p108_secuencia := NULL;',
'    :p108_clave     := NULL;',
'    --raise_application_error(-20000,'':P108_COM_ID_NOTA: ''||:P108_COM_ID_NOTA);',
'    :p108_accion := ''req_gaba_fin'';',
'',
'/*',
'',
' pq_ven_comprobantes.pr_redirect(pv_desde        => ''NCM'',',
'                                    pn_app_id       => :APP_ID,',
'                                    pn_pag_id       => 113,',
'                                    pn_emp_id       => :f_emp_id,',
'                                    pv_session      => :session,',
'                                    pv_token        => :f_token,',
'                                    pn_user_id      => :f_user_id,',
'                                    pn_rol          => :p0_rol,',
'                                    pv_rol_desc     => :p0_rol_desc,',
'                                    pn_tree_rot     => :p0_tree_root,',
'                                    pn_opcion       => :f_opcion_id,',
'                                    pv_parametro    => :f_parametro,',
'                                    pv_empresa      => :f_empresa,',
'                                    pn_uge_id       => :f_uge_id,',
'                                    pn_uge_id_gasto => :f_uge_id_gasto,',
'                                    pv_ugestion     => :f_ugestion,',
'                                    pv_error        => :p0_error);',
'',
'  ',
'*/',
'  ELSE',
'    :p0_error := ''La Secuencia del Folio no es la Correcta'';',
'  END IF;',
':p108_accion:= ''req_gaba_fin'';',
':p0_error:= :request;',
'--raise_application_error(-20000,'':P108_COM_ID_NOTA: ''||:P108_COM_ID_NOTA||'' ttt:''||:p0_error);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(45563973452349153)
,p_process_when=>':p108_accion = ''req_graba'''
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>13410427258336619
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(45571353716349203)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_imprimir_nc'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'PQ_VEN_NCREDITO_REBAJA.pr_imprimir_nc(:F_EMP_ID,',
'                           :P108_COM_ID_NOTA,',
'                           :p0_error);',
'IF :p0_error IS NULL THEN ',
':p0_error:= ''NOTA DE CREDITO GRABADA CORRECTAMENTE'';',
'END IF;',
':P108_COM_ID_NOTA:=NULL;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'req_graba'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>13318202446584277
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(45571573850349204)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_DATOS_FOLIO_CAJA_USU'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  ln_tgr_id            asdm_tipos_grupo_transaccion.tgr_id%TYPE;',
'  ln_monto_inicial_min NUMBER;',
'  ln_com_id            NUMBER;',
'BEGIN',
'',
'  ln_tgr_id  := pq_constantes.fn_retorna_constante(NULL,',
'                                                   ''cn_tgr_id_nota_credito'');',
'  :p0_ttr_id := pq_constantes.fn_retorna_constante(0,',
'                                                   ''cn_ttr_id_nota_credito_rebaja''); --292',
'  pq_ven_movimientos_caja.pr_datos_folio_caja_usu(:f_emp_id,',
'                                                  :f_pca_id,',
'                                                  ln_tgr_id,',
'                                                  :p0_ttr_descripcion,',
'                                                  :p0_periodo,',
'                                                  :p0_datfolio,',
'                                                  :p0_fol_sec_actual,',
'                                                  :p0_pue_num_sri,',
'                                                  :p0_uge_num_est_sri,',
'                                                  :p0_pue_id,',
'                                                  :p0_ttr_id,',
'                                                  :p0_nro_folio,',
'                                                  :p0_error);',
'',
'  :p108_pue_electronico := pq_asdm_docs_xml.fn_tipo_punto(pn_pue_id => :p0_pue_id);',
'  IF :p108_pue_electronico = ''E'' THEN',
'    :p108_secuencia := :p0_fol_sec_actual;',
'  END IF;',
'  --------------------------------------------VALIDAR SI EL VALOR ESTA DENTO DE LOS PARAMETROS PARA NC----',
'  SELECT MIN(d.mde_monto_inicial)',
'    INTO ln_monto_inicial_min',
'    FROM asdm_e.ven_montos_descuentos d;',
'  IF ln_monto_inicial_min > :p108_total THEN',
'    raise_application_error(-20000,',
'                            ''EL VALOR MINIMO PARA GENERAR LA NC ES DE :'' ||',
'                            ln_monto_inicial_min ||',
'                            '' Y EL MONTO INGRESADO ES: '' || :p108_total);',
'  END IF;',
'',
'  ---------- DSESCRIPCION NC',
'  :p108_concepto := ''MONTOS POR COMPRA DESDE: '' || :p108_fecha_inicio ||',
'                    '' HASTA:'' || :p108_fecha_fin;',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'req_graba'
,p_process_when_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_internal_uid=>13318422580584278
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(615738965574210653)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_redirect'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'htp.init;',
'owa_util.redirect_url(''f?p=&APP_ID.:113:&APP_SESSION.'');',
'apex_application.stop_apex_engine;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_type=>'NEVER'
,p_internal_uid=>583485814304445727
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(2149589956236702133)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_obtener_base_iva'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'SELECT DISTINCT v.vcd_valor_variable val INTO :p108_porcentaje_iva',
'  FROM ven_comprobantes_det d, ven_var_comprobantes_det v',
' WHERE d.com_id = :p108_com_id_fa',
'   AND d.cde_id = v.cde_id',
'   AND v.var_id = pq_constantes.fn_retorna_constante(:f_emp_id,''cn_var_id_porc_iva'');--129',
'EXCEPTION',
'  WHEN OTHERS THEN ',
'   :p0_error:=''No se encuentra base imponible o hay mas de una en la factura: com_id:''||:p108_com_id_fa ||'' ''||sqlerrm;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>2117336804966937207
);
wwv_flow_imp.component_end;
end;
/
