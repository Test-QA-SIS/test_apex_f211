prompt --application/pages/page_00112
begin
--   Manifest
--     PAGE: 00112
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>112
,p_name=>'FP_RELACIONAR_DOCUMENTOS'
,p_step_title=>'Relacionar Documentos'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'02'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(68952477293325811)
,p_name=>'Relaciones del documento'
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    a.*, b.*',
'FROM      asdm_relaciones_documentos a,',
'          v_asdm_documentos b',
'WHERE     a.doc_id_hijo = :P112_DOC_ID_HIJO',
'AND       b.ttr_id = a.ttr_id_padre',
'AND       b.documento = a.doc_id_padre'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68952758734325830)
,p_query_column_id=>1
,p_column_alias=>'RDO_ID'
,p_column_display_sequence=>1
,p_column_heading=>unistr('C\00F3d.')
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:112:&SESSION.::&DEBUG.:RP:P112_RDO_ID:#RDO_ID#'
,p_column_linktext=>'Modificar #RDO_ID#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68952868350325835)
,p_query_column_id=>2
,p_column_alias=>'EMP_ID'
,p_column_display_sequence=>10
,p_column_heading=>'EMP_ID'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68952980505325835)
,p_query_column_id=>3
,p_column_alias=>'TTR_ID_PADRE'
,p_column_display_sequence=>2
,p_column_heading=>'Tipo Trans. Padre'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT_FROM_LOV_ESC'
,p_named_lov=>wwv_flow_imp.id(57551852241400679)
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68953054799325835)
,p_query_column_id=>4
,p_column_alias=>'DOC_ID_PADRE'
,p_column_display_sequence=>13
,p_column_heading=>'Documento Padre'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68953153570325835)
,p_query_column_id=>5
,p_column_alias=>'TTR_ID_HIJO'
,p_column_display_sequence=>14
,p_column_heading=>'Tipo Trans. Hijo'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT_FROM_LOV_ESC'
,p_named_lov=>wwv_flow_imp.id(57551852241400679)
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68953252860325835)
,p_query_column_id=>6
,p_column_alias=>'DOC_ID_HIJO'
,p_column_display_sequence=>15
,p_column_heading=>'Documento Hijo'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68953365693325835)
,p_query_column_id=>7
,p_column_alias=>'RDO_FECHA_RELACION'
,p_column_display_sequence=>4
,p_column_heading=>'Fecha'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68953468301325835)
,p_query_column_id=>8
,p_column_alias=>'RDO_OBSERVACIONES'
,p_column_display_sequence=>5
,p_column_heading=>'Observaciones'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(68953567992325835)
,p_query_column_id=>9
,p_column_alias=>'RDO_ESTADO_REGISTRO'
,p_column_display_sequence=>6
,p_column_heading=>'Estado'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT_FROM_LOV_ESC'
,p_named_lov=>wwv_flow_imp.id(259896810965110526)
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69827779819086249)
,p_query_column_id=>10
,p_column_alias=>'DOCUMENTO'
,p_column_display_sequence=>7
,p_column_heading=>'Documento'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69827852575086317)
,p_query_column_id=>11
,p_column_alias=>'TRANSACCION'
,p_column_display_sequence=>9
,p_column_heading=>'Transaccion'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69827953993086317)
,p_query_column_id=>12
,p_column_alias=>'TTR_ID'
,p_column_display_sequence=>11
,p_column_heading=>'Ttr Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69869980698550049)
,p_query_column_id=>13
,p_column_alias=>'NUMERO_DE_FOLIO'
,p_column_display_sequence=>16
,p_column_heading=>'Numero De Folio'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69870067935550058)
,p_query_column_id=>14
,p_column_alias=>'COD_CLIENTE'
,p_column_display_sequence=>17
,p_column_heading=>'Cod Cliente'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69828263756086318)
,p_query_column_id=>15
,p_column_alias=>'IDENTIFICACION'
,p_column_display_sequence=>3
,p_column_heading=>'Identificacion'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69870164185550058)
,p_query_column_id=>16
,p_column_alias=>'NOMBRE_CLIENTE'
,p_column_display_sequence=>18
,p_column_heading=>'Nombre Cliente'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69828460568086318)
,p_query_column_id=>17
,p_column_alias=>'UGE_ID'
,p_column_display_sequence=>8
,p_column_heading=>'Uge Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(69828564045086318)
,p_query_column_id=>18
,p_column_alias=>'SEGMENTO'
,p_column_display_sequence=>12
,p_column_heading=>'Segmento'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(69651659853487194)
,p_plug_name=>'Relacionar Documentos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270523080594046668)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(69651978034487201)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_button_name=>'SAVE'
,p_button_static_id=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Grabar'
,p_button_position=>'CHANGE'
,p_button_condition=>'P112_RDO_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_button_cattributes=>'style="font-size:18;font-family:Arial Narrow;"'
,p_database_action=>'UPDATE'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(69652273417487201)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_button_name=>'CANCEL'
,p_button_static_id=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'<< Regresar'
,p_button_position=>'CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:41:&SESSION.::&DEBUG.:112::'
,p_button_cattributes=>'style="font-size:18;font-family:Arial Narrow;"'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(69651882415487201)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_button_name=>'CREATE'
,p_button_static_id=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Grabar'
,p_button_position=>'CREATE'
,p_button_condition=>':P112_RDO_ID IS NULL'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
,p_button_cattributes=>'style="font-size:18;font-family:Arial Narrow;"'
,p_database_action=>'INSERT'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(69652051701487201)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_button_name=>'DELETE'
,p_button_static_id=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(32493128068920419)
,p_button_image_alt=>'Eliminar'
,p_button_position=>'DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P112_RDO_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_button_cattributes=>'style="font-size:18;font-family:Arial Narrow;"'
,p_database_action=>'DELETE'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(69652868586487208)
,p_branch_action=>'f?p=&APP_ID.:112:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69653072496487212)
,p_name=>'P112_RDO_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_use_cache_before_default=>'NO'
,p_prompt=>unistr('C\00F3digo')
,p_source=>'RDO_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69653268270487217)
,p_name=>'P112_EMP_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_item_default=>':F_EMP_ID'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Emp Id'
,p_source=>'EMP_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69653454394487218)
,p_name=>'P112_TTR_ID_PADRE'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_prompt=>'Tipo Documento Padre'
,p_source=>'TTR_ID_PADRE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_POPUP_LOV'
,p_named_lov=>'LOV_TIPOS_TRANSACCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ASDM_TIPOS_TRANSACCIONES_MUESTRA_ID'');',
'return (lv_lov);',
'END;',
''))
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_tag_attributes=>'onchange="doSubmit('''');"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'DIALOG'
,p_attribute_02=>'FIRST_ROWSET'
,p_attribute_04=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69653667292487218)
,p_name=>'P112_DOC_ID_PADRE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_prompt=>'Documento Padre'
,p_source=>'DOC_ID_PADRE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'PLUGIN_COM_SKILLBUILDERS_SUPER_LOV'
,p_named_lov=>'LOV_ASDM_DOCUMENTOS_4'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   lv_lov varchar2(500);',
'BEGIN',
'   lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_ASDM_DOCUMENTOS'');',
'   lv_lov := lv_lov ||'' WHERE ttr_id = ''||NVL(NVL(:P112_TTR_ID_PADRE,:p0_ttr_id),''ttr_id'');',
'   lv_lov := lv_lov ||'' AND uge_id = ''||NVL(:F_UGE_ID,''UGE_ID'');',
'   lv_lov := lv_lov ||'' AND SEGMENTO = ''||NVL(:F_SEG_ID,''SEGMENTO'');',
'   lv_lov := lv_lov ||'' AND Documento NOT IN (SELECT    doc_id_padre',
'                                              FROM      asdm_relaciones_documentos)'';',
'',
'   RETURN (lv_lov);',
'END;'))
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_03=>'2,1'
,p_attribute_08=>'NOT_ENTERABLE'
,p_attribute_09=>'15'
,p_attribute_10=>'&nbsp;'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69653883637487219)
,p_name=>'P112_TTR_ID_HIJO'
,p_is_required=>true
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_prompt=>'Tipo Documento Hijo'
,p_source=>'TTR_ID_HIJO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TIPOS_TRANSACCIONES'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(500);',
'BEGIN',
' lv_lov := lv_lov|| kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ASDM_TIPOS_TRANSACCIONES_MUESTRA_ID'');',
'return (lv_lov);',
'END;',
''))
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69654073392487220)
,p_name=>'P112_DOC_ID_HIJO'
,p_is_required=>true
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_prompt=>'Documento Hijo'
,p_source=>'DOC_ID_HIJO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'PLUGIN_COM_SKILLBUILDERS_SUPER_LOV'
,p_named_lov=>'LOV_ASDM_DOCUMENTOS'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   lv_lov varchar2(500);',
'BEGIN',
'   lv_lov := lv_lov || kdda_p.pq_kdda_cursores.fn_query_lov(''LOV_ASDM_DOCUMENTOS'');',
'   lv_lov := lv_lov ||'' WHERE ttr_id = ''||NVL(NVL(:P30_TTR_ID_PADRE,:p0_ttr_id),''ttr_id'');',
'   lv_lov := lv_lov ||'' AND uge_id = ''||NVL(:F_UGE_ID,''UGE_ID'');',
'   lv_lov := lv_lov ||'' AND SEGMENTO = ''||NVL(:F_SEG_ID,''SEGMENTO'');',
'   lv_lov := lv_lov ||'' AND Documento NOT IN (SELECT    doc_id_padre',
'                                              FROM      asdm_relaciones_documentos)'';',
'',
'   RETURN (lv_lov);',
'END;'))
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_03=>'2,1'
,p_attribute_08=>'NOT_ENTERABLE'
,p_attribute_09=>'15'
,p_attribute_10=>'&nbsp;'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69654261081487220)
,p_name=>'P112_RDO_FECHA_RELACION'
,p_is_required=>true
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_use_cache_before_default=>'NO'
,p_item_default=>'SYSDATE'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha'
,p_source=>'RDO_FECHA_RELACION'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_read_only_when_type=>'ALWAYS'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69654458582487221)
,p_name=>'P112_RDO_OBSERVACIONES'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_prompt=>'Observaciones'
,p_source=>'RDO_OBSERVACIONES'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>300
,p_cHeight=>4
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69654671887487221)
,p_name=>'P112_RDO_ESTADO_REGISTRO'
,p_is_required=>true
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Estado'
,p_source=>'RDO_ESTADO_REGISTRO'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ESTADO_REGISTRO'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'    lv_lov varchar2(300);',
'BEGIN',
'        lv_lov := kdda_p.pq_kdda_cursores.fn_query_lov(''LV_ESTADO_REG'');',
'return (lv_lov);',
'END;'))
,p_cSize=>32
,p_cMaxlength=>1
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69790051325844245)
,p_name=>'P112_TRX_ID_HIJO'
,p_item_sequence=>0
,p_item_plug_id=>wwv_flow_imp.id(69651659853487194)
,p_prompt=>'Trx Id Hijo'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(69654958846487221)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from ASDM_RELACIONES_DOCUMENTOS'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'ASDM_RELACIONES_DOCUMENTOS'
,p_attribute_03=>'P112_RDO_ID'
,p_attribute_04=>'RDO_ID'
,p_process_error_message=>'Unable to fetch row.'
,p_internal_uid=>37401807576722295
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(69791765271905077)
,p_process_sequence=>50
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_TIPO_TRANSACCION_HIJO'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'BEGIN',
'   SELECT    ttr_id',
'   INTO      :P112_TTR_ID_HIJO',
'   FROM      asdm_transacciones',
'   WHERE     trx_id = :P112_TRX_ID_HIJO;',
'EXCEPTION',
'   WHEN OTHERS THEN',
'      RAISE_APPLICATION_ERROR(-20001,''TRANSACCION HIJO NO EXISTE.''||SQLERRM);',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P112_TRX_ID_HIJO IS NOT NULL'
,p_internal_uid=>37538614002140151
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(69845355305334661)
,p_process_sequence=>1
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_valida_padre'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   ln_registros NUMBER(10);',
'BEGIN',
'   SELECT    COUNT(*)',
'   INTO      ln_registros',
'   FROM      asdm_relaciones_documentos a',
'   WHERE     a.doc_id_hijo = :P112_DOC_ID_HIJO;',
'',
'   IF ln_registros > 0 THEN',
'      RAISE_APPLICATION_ERROR(-20001,''Documento ya esta relacionado.'');',
'   END IF;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(69651882415487201)
,p_internal_uid=>37592204035569735
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(69319058075108341)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar_documentos'
,p_process_sql_clob=>'NULL;'
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(69319379546114513)
,p_internal_uid=>37065906805343415
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(69655175408487224)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of ASDM_RELACIONES_DOCUMENTOS'
,p_attribute_01=>'ASDM_E'
,p_attribute_02=>'ASDM_RELACIONES_DOCUMENTOS'
,p_attribute_03=>'P112_RDO_ID'
,p_attribute_04=>'RDO_ID'
,p_attribute_09=>'P112_RDO_ID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_process_error_message=>'Unable to process row of table ASDM_RELACIONES_DOCUMENTOS.'
,p_process_success_message=>'Action Processed.'
,p_internal_uid=>37402024138722298
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(69655356900487225)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_process_when_button_id=>wwv_flow_imp.id(69652051701487201)
,p_internal_uid=>37402205630722299
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(74657757509961981)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'PR_CARGAR_TIPO_TRANSACCION_PADRE'
,p_process_sql_clob=>':P112_TTR_ID_PADRE := 52;'
,p_process_clob_language=>'PLSQL'
,p_process_when=>':P112_TTR_ID_PADRE IS NULL'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>42404606240197055
);
wwv_flow_imp.component_end;
end;
/
