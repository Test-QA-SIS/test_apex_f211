prompt --application/pages/page_00040
begin
--   Manifest
--     PAGE: 00040
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>40
,p_name=>'Cierre de Caja'
,p_step_title=>'Cierre de Caja'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_updated_by=>'JTORRES'
,p_last_upd_yyyymmddhh24miss=>'20240126161508'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(51701275466725654)
,p_plug_name=>'detalle'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT seq_id,',
'       c001       mcd_id,',
'       c002       tfp_id,',
'       c003       forma_pago,',
'       c004       ede_id,',
'       c005       entidad,',
'       c006       cre_id,',
'       c007       num_cheque,',
'       c008       num_cuenta,',
'       c009       valor,',
'       c010       fecha,',
'       c011       titula_cuenta,',
'       c012       envia',
'FROM   apex_collections',
'WHERE  collection_name = ''COL_DEP_TRANSITORIOS''',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'NEVER'
,p_plug_display_when_condition=>'graba'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(51701456030725667)
,p_name=>'detalle'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_owner=>'PLOPEZ'
,p_internal_uid=>19448304760960741
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51701562864725681)
,p_db_column_name=>'SEQ_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Seq Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'SEQ_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51701674925725685)
,p_db_column_name=>'MCD_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Mcd Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'MCD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51701780894725685)
,p_db_column_name=>'TFP_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Tfp Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'TFP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51701879665725688)
,p_db_column_name=>'FORMA_PAGO'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Forma Pago'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'FORMA_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51701953216725688)
,p_db_column_name=>'EDE_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Ede Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'EDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702072261725688)
,p_db_column_name=>'ENTIDAD'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Entidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ENTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702174582725688)
,p_db_column_name=>'CRE_ID'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Cre Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'CRE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702268934725688)
,p_db_column_name=>'NUM_CHEQUE'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Num Cheque'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NUM_CHEQUE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702358387725688)
,p_db_column_name=>'NUM_CUENTA'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Num Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'NUM_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702479246725689)
,p_db_column_name=>'VALOR'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702554717725689)
,p_db_column_name=>'FECHA'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702671874725689)
,p_db_column_name=>'TITULA_CUENTA'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Titula Cuenta'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'TITULA_CUENTA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51702783308725690)
,p_db_column_name=>'ENVIA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Envia'
,p_column_link=>'f?p=&APP_ID.:9:&SESSION.:cambia:&DEBUG.::P9_SEQ_ID,P9_ESTADO:#SEQ_ID#,#ENVIA#'
,p_column_linktext=>'#ENVIA#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ENVIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(51702859209725690)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1292816663919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'SEQ_ID:FORMA_PAGO:ENTIDAD:NUM_CHEQUE:NUM_CUENTA:VALOR:FECHA:TITULA_CUENTA:ENVIA:''NOENVIAR'''
,p_sort_column_1=>'CRE_FECHA_DEPOSITO'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(51702952332725693)
,p_plug_name=>'CIERRE DE CAJA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51704560799725710)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Grabar'
,p_button_position=>'BOTTOM'
,p_button_condition=>'graba'
,p_button_condition_type=>'REQUEST_NOT_EQUAL_CONDITION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(76436777879639205)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_button_name=>'IMPRIMIR'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'TOP'
,p_button_redirect_url=>'javascript:popUp2(''../../reports/rwservlet?module=cierre_caja.RDF&userid=asdm_p/asdm_p@mxpdm&destype=cache&desformat=pdf&pn_pca_id=&P0_PCA_ID.&pn_emp_id=&F_EMP_ID.'', 850, 500,85);'
,p_button_condition_type=>'NEVER'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(1453464184040107275)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_button_name=>'IMPRIMIR2'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Imprimir'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.:imprimir_caja:&DEBUG.:::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(74608071402332971)
,p_branch_action=>'f?p=&APP_ID.:59:&SESSION.::&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(51704560799725710)
,p_branch_sequence=>20
,p_branch_condition_type=>'ITEM_IS_NOT_NULL'
,p_branch_condition=>'p40_mca_id'
,p_branch_comment=>'Created 26-NOV-2010 14:02 by PLOPEZ'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(51708859369725754)
,p_branch_action=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>100
,p_branch_comment=>'Created 13-NOV-2009 01:40 by ADMIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51705173979725720)
,p_name=>'P40_SEQ_ID'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(51701275466725654)
,p_prompt=>'Seq Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51705374462725720)
,p_name=>'P40_TRX_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Trx Id'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51705556324725720)
,p_name=>'P40_ESTADO'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_imp.id(51701275466725654)
,p_prompt=>'Estado'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51705764725725721)
,p_name=>'P40_TTR_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'Ttr Id'
,p_source=>'pq_constantes.fn_retorna_constante(NULL,''cn_ttr_id_cierre_caja'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51705966016725721)
,p_name=>'P40_MCA_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'Mca Id'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'N'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51706777396725724)
,p_name=>'P40_MCA_TOTAL'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'Total'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'graba'
,p_display_when_type=>'REQUEST_NOT_EQUAL_CONDITION'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51706958833725724)
,p_name=>'P40_ENTIDAD_DESTINO'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'Entidad Destino'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_ENT_DESTINO_X_UGE'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT distinct(d.ede_descripcion) d,',
'       d.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  a.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id =:F_UGE_ID',
'       AND A.UCB_ESTADO_REGISTRO=pq_constantes.fn_retorna_constante(NULL,',
'                                                                                                                ''cv_estado_reg_activo'')'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'Seleccion Entidad'
,p_lov_null_value=>'%null%'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'REDIRECT_SET_VALUE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51707352580725728)
,p_name=>'P40_EDE_ID'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'Cuenta'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.ede_descripcion d,',
'       c.ede_id          r',
'FROM   asdm_ugestion_ctabancos a,',
'       asdm_entidades_destinos c,',
'       asdm_entidades_destinos d',
'WHERE  A.ede_id = c.ede_id',
'       AND c.ede_id_padre = d.ede_id',
'       AND a.uge_id = :f_uge_id',
'       AND d.ede_id = :p40_entidad_destino',
'ORDER  BY a.ucb_prioridad',
''))
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_display_when=>'p40_entidad_destino'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_imp.id(270535158703046676)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(51721780946810646)
,p_name=>'P40_CIERRE_SISTEMA'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cierre Sistema'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT nvl(SUM(mca_total*DECODE(c.ttr_funcion,''C'',-1,1) ),0)',
'FROM   asdm_movimientos_caja    a,',
'       asdm_transacciones       b,',
'       asdm_tipos_transacciones c',
'WHERE  b.trx_id = a.trx_id',
'       AND b.ttr_id = c.ttr_id',
'       AND a.pca_id = :f_pca_id',
'       AND',
'       c.ttr_id NOT IN',
'       (pq_constantes.fn_retorna_constante(NULL,',
'                                           ''cn_ttr_id_egr_caj_dep_tran''),',
'        pq_constantes.fn_retorna_constante(NULL, ''cn_ttr_id_cierre_caja''));'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(136601378657228489)
,p_name=>'P40_EFECTIVO'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'EFECTIVO'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(136601557673228489)
,p_name=>'P40_CHEQUES'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'CHEQUES'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(136601768929228489)
,p_name=>'P40_TARJETAS'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_prompt=>'TARJETAS'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cAttributes=>'nowrap="nowrap"'
,p_begin_on_new_line=>'N'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(138687962174256640)
,p_name=>'P40_FECHA_SISTEMA'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_imp.id(51702952332725693)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Fecha Sistema'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT PCA_FECHA_SISTEMA FROM VEN_PERIODOS_CAJA',
'WHERE EMP_ID = :f_emp_id',
'AND USU_ID = :f_user_id',
'AND PCA_ID = :f_pca_id',
'AND PCA_ESTADO_PC = ''A'''))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(51707866386725732)
,p_validation_name=>'P40_ENTIDAD_DESTINO'
,p_validation_sequence=>10
,p_validation=>'P40_ENTIDAD_DESTINO'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Value must be specified.'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_imp.id(51704560799725710)
,p_associated_item=>wwv_flow_imp.id(51706958833725724)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1453464076952107274)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>' PR_IMPRIMIR_CIERRE_CAJA'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_error varchar2(250);',
'begin',
'',
'pq_aco_jasper_reports.pr_ejecutar_reporte(pv_jre_nombre        => ''CIERRE_CAJA'',',
'                                                pv_nombre_parametros => ''pn_emp_id:pn_pca_id'',',
'                                                pv_valor_parametros  => :f_emp_id||'':''||:P0_PCA_ID,',
'                                                pn_emp_id            => :f_emp_id,',
'                                                pv_formato           => ''pdf'',',
'                                                pv_error             => lv_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'imprimir_caja'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>1437337501317224811
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51708382963725746)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_graba_cierre_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'ln_diferencia number;',
'BEGIN',
'ln_diferencia:=:p40_mca_total+nvl(:p40_dep_transitorios,0)-:p40_cierre_sistema;',
'--if ln_diferencia=0 OR :P40_AUTORIZACION IS NOT NULL then',
':p40_seq_id:=null;',
':p40_estado:=null;',
'  pq_ven_movimientos_caja.pr_cierre_caja(pn_emp_id          =>:f_emp_id,',
'                                                  pn_uge_id          =>:f_uge_id,',
'                                                  pn_user_id         =>:f_user_id,',
'                                                  pn_trx_id          =>:p40_trx_id,',
'                                                  pn_uge_id_gasto    =>:f_uge_id_gasto,',
'                                                  pn_mca_id          =>:p40_mca_id,',
'                                                  pn_pca_id          =>:f_pca_id,',
'                                                  pv_error           =>:p0_error);',
'--end if;',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(51704560799725710)
,p_internal_uid=>19455231693960820
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51707978941725742)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_coleccion'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'pq_ven_movimientos_caja.pr_carga_coll_depositos(pn_emp_id => :f_emp_id,',
'                        pn_pca_id => :f_pca_id,',
'                        pn_valor_efectivo => :P40_EFECTIVO,',
'                        pn_valor_cheque => :P40_CHEQUES,',
'                        pn_valor_tarjeta => :P40_TARJETAS,',
'                        pv_error  => :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>':request not in (''cambia'',''graba'') or :p40_seq_id is null'
,p_process_when_type=>'EXPRESSION'
,p_process_when2=>'PLSQL'
,p_internal_uid=>19454827671960816
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(51708152818725746)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_modifica_reg'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'',
'if :p40_estado=''SI'' then',
' apex_collection.update_member_attribute(p_collection_name =>''COL_DEP_TRANSITORIOS'',',
'                                          p_seq             =>:P40_SEQ_ID,',
'                                          p_attr_number     =>12,',
'                                          p_attr_value      =>''NO'');',
'else',
'apex_collection.update_member_attribute(p_collection_name =>''COL_DEP_TRANSITORIOS'',',
'                                          p_seq             =>:P40_SEQ_ID,',
'                                          p_attr_number     =>12,',
'                                          p_attr_value      =>''SI'');',
'end if;',
'--:p40_estado:=null;',
'--:p40_seq_id:=null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'cambia'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>19455001548960820
);
wwv_flow_imp.component_end;
end;
/
