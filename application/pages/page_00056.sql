prompt --application/pages/page_00056
begin
--   Manifest
--     PAGE: 00056
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>56
,p_name=>'Ingresos y Egresos de Caja'
,p_step_title=>'Ingresos y Egresos de Caja'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script type="Text/JavaScript">',
'',
'var startVal = $(''#P56_TRANSACCION'').val(); ',
'',
'function checkSave(p) {',
'',
'    if (confirm(''Have you saved details for this person?'')) {',
'',
'       location.href=''f?p=&APP_ID.:8:&SESSION.::NO::P8_TTR_ID:''+ p.options[p.selectedIndex].value;',
'',
'    } else {',
'',
'       for (i=0;i<p.options.length;i++) {',
'',
'          if (p.options[i].value == startVal) {',
'',
'             p.options[i].selected = true;',
'',
'          } else {',
'',
'             p.options[i].selected = false;',
'',
'          }',
'',
'       }',
'',
'    }',
'',
'}',
'',
'</script> '))
,p_step_template=>wwv_flow_imp.id(70829267270734112)
,p_page_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_updated_by=>'MFIDROVO'
,p_last_upd_yyyymmddhh24miss=>'20240122112533'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(36904875917396331)
,p_plug_name=>'ORDENES DE PAGO PENDIENTES'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.mca_id mov_caja,',
'       a.cli_id,',
'       (SELECT c.nombre_completo',
'          FROM v_asdm_datos_clientes c',
'         WHERE c.cli_id = a.cli_id) cliente,',
'       a.mca_fecha fecha,',
'       a.mca_total valor,',
'       b.mcd_id,',
'       (SELECT p.pro_id',
'          FROM asdm_clientes c, asdm_proveedores p',
'         WHERE p.per_id = c.per_id',
'           AND c.cli_id = a.cli_id) pro_id,',
'       to_number(pq_constantes.fn_retorna_constante(0, ''cn_mon_id_dolar'')) moneda,',
'       a.usu_id,',
'       upper(kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(a.usu_id)) usuario,',
'       asdm_p.PQ_TES_LISTAS.fn_retorna_uuge(:f_user_id,:f_emp_id,:f_uge_id) uug_id,',
'       case when :f_emp_id!=1 then',
'       ''GENERAR ORDEN'' ',
'       else',
'       NULL',
'       end ',
'       AS generar,',
'      ',
'       ''GENERAR ORDEN DE PAGO EN SAP'' ',
'       AS GENERAR_SAP',
'        ',
'  FROM asdm_movimientos_caja a, asdm_movimientos_caja_detalle b',
' WHERE b.mca_id = a.mca_id',
'   AND a.ttr_id = :P56_TRANSACCION',
'   AND b.tfp_id = :cn_tfp_id_orden_pago',
'   AND b.opa_id IS NULL',
'   and a.uge_id = :f_uge_id;'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P56_MOVIMIENTOS > 0 and :P56_TRANSACCION is not null'
,p_plug_display_when_cond2=>'PLSQL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(36904957937396331)
,p_name=>'ORDENES DE PAGO PENDIENTES'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_owner=>'ACALLE'
,p_internal_uid=>4651806667631405
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36905168614396371)
,p_db_column_name=>'MCD_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Mcd Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCD_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36911866857512259)
,p_db_column_name=>'MOV_CAJA'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Mov Caja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MOV_CAJA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36911955127512271)
,p_db_column_name=>'CLI_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912063198512272)
,p_db_column_name=>'CLIENTE'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912162976512272)
,p_db_column_name=>'FECHA'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912282058512272)
,p_db_column_name=>'VALOR'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912372160512272)
,p_db_column_name=>'PRO_ID'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Pro Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PRO_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912480481512272)
,p_db_column_name=>'MONEDA'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Moneda'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MONEDA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912565587512273)
,p_db_column_name=>'USU_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Usu Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'USU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912677276512273)
,p_db_column_name=>'USUARIO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'USUARIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36912753086512274)
,p_db_column_name=>'GENERAR'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Generar'
,p_column_link=>'f?p=225:5:&SESSION.:carga_ventas:&DEBUG.::F_LOGOUT_URL,F_EMP_ID,F_EMPRESA,F_UGE_ID,F_TOKEN,F_USER_ID,P0_ROL,P0_ROL_DESC,P0_TREE_ROOT,F_OPCION_ID,F_PARAMETRO,F_POPUP,P5_PRO_ID,P5_VALOR_ANTICIPO_VENTAS,P5_FECHA_AUX,P5_OPA_FECHA,P5_MON_ID,P5_UUG_ID,P5_M'
||'OSTRAR_PANTALLA,P5_MOSTRAR_PANTALLA_X,F225_MOSTRAR_PANTALLA_ORDEN,P5_INGRESO,P5_MCD_ID,F_UGESTION:f?p=&APP_ID.,&F_EMP_ID.,&F_EMPRESA.,&F_UGE_ID.,&F_TOKEN.,&F_USER_ID.,&P0_ROL.,&P0_ROL_DESC.,&P0_TREE_ROOT.,&F_OPCION_ID.,&F_PARAMETRO.,N,#PRO_ID#,#VALOR'
||'#,#FECHA#,#FECHA#,#MONEDA#,#UUG_ID#,m,S,S,S,#MCD_ID#,&F_UGESTION.'
,p_column_linktext=>'#GENERAR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'GENERAR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(36920976001600051)
,p_db_column_name=>'UUG_ID'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Uug Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UUG_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(126661824991351414727)
,p_db_column_name=>'GENERAR_SAP'
,p_display_order=>22
,p_column_identifier=>'M'
,p_column_label=>'Generar sap'
,p_column_type=>'STRING'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(36907275054396861)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'46542'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'MCD_ID:MOV_CAJA:CLI_ID:CLIENTE:FECHA:VALOR:PRO_ID:MONEDA:USU_ID:USUARIO:GENERAR:GENERAR_SAP'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(79139355026421724)
,p_plug_name=>'Ingresos y Egresos de Caja'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(79157460838518027)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(79139355026421724)
,p_button_name=>'CARGAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.:8:P8_TTR_ID,P8_SEQ_ID,P8_MODIFICACION,P8_AUTORIZACION:&P56_TRANSACCION.,,,'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(44842532105833862843)
,p_branch_name=>'go_to_page_146'
,p_branch_action=>'f?p=&APP_ID.:142:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>5
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>':P56_TRANSACCION in (564)'
,p_branch_condition_text=>'PLSQL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(44842532342131862845)
,p_branch_name=>'go_to_page_148'
,p_branch_action=>'f?p=&APP_ID.:148:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>15
,p_branch_condition_type=>'EXPRESSION'
,p_branch_condition=>':P56_TRANSACCION in (565)'
,p_branch_condition_text=>'PLSQL'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(92001778941858111)
,p_branch_name=>'Go To Page 8'
,p_branch_action=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.:8:P8_TTR_ID,P8_SEQ_ID,P8_MODIFICACION,P8_AUTORIZACION:&P56_TRANSACCION.,,,'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>25
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 16-SEP-2011 08:31 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(36055279045233003)
,p_name=>'P56_MATRIZ'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(79139355026421724)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Matriz'
,p_source=>'pq_constantes.fn_retorna_constante(:f_emp_id, ''cn_uge_id_matriz'')'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(79140365415424740)
,p_name=>'P56_TRANSACCION'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(79139355026421724)
,p_prompt=>'Transaccion'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'LOV_TRANSACCIONES_CAJA'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT b.ttr_descripcion d, b.ttr_id r',
'FROM asdm_tipos_transaccion_empresa a, asdm_tipos_transacciones b',
'WHERE a.ttr_id = b.ttr_id',
'AND a.tgr_id = pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja_otros'')',
'and a.emp_id = :f_emp_id',
'and a.tte_estado_registro=0',
'and b.ttr_estado_registro=0',
'union',
'SELECT b.ttr_descripcion d, b.ttr_id r',
'FROM asdm_tipos_transaccion_empresa a, asdm_tipos_transacciones b',
'WHERE a.ttr_id = b.ttr_id',
'and a.emp_id = :f_emp_id',
'AND a.tgr_id = pq_constantes.fn_retorna_constante(null,''cn_tgr_id_mov_caja_matriz'')',
'AND :f_uge_id = :P56_MATRIZ',
'and a.tte_estado_registro=0',
'and b.ttr_estado_registro=0',
'ORDER BY R'))
,p_lov_display_null=>'YES'
,p_lov_null_text=>'-- Seleccione --'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(1460251242274868471)
,p_name=>'P56_MOVIMIENTOS'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(79139355026421724)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_validation(
 p_id=>wwv_flow_imp.id(94610565877596240)
,p_validation_name=>'P56_TRANSACCION'
,p_validation_sequence=>10
,p_validation=>'P56_TRANSACCION'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Seleccione el Tipo de Movimiento a realizar'
,p_associated_item=>wwv_flow_imp.id(79140365415424740)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(99259163992557717)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_elimina_mov_caja'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'lv_nombre_coleccion VARCHAR2(50);',
'  ',
'begin',
'    if apex_collection.collection_exists(''CO_MOV_CAJA'') then',
'      apex_collection.delete_collection(''CO_MOV_CAJA'');',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>67006012722792791
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(1460251174503868470)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cargar_datos'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'SELECT count(''x'') into :P56_MOVIMIENTOS ',
'  FROM asdm_movimientos_caja a, asdm_movimientos_caja_detalle b',
' WHERE b.mca_id = a.mca_id',
'   AND b.tfp_id = :cn_tfp_id_orden_pago',
'   AND b.opa_id IS NULL',
'   and a.uge_id = :f_uge_id;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>1444124598868986007
);
wwv_flow_imp.component_end;
end;
/
