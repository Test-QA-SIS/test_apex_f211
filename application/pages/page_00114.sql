prompt --application/pages/page_00114
begin
--   Manifest
--     PAGE: 00114
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>114
,p_tab_set=>'Menu'
,p_name=>'FR_APROBACION_ELECTRICA'
,p_step_title=>'Listado de Facturas por Aprobar'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(37728181068364710)
,p_plug_name=>'Listado de Facturas'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT    ROWNUM nro_registro,',
'          a.com_id, a.emp_id, a.pue_id, a.pol_id, a.cli_id, TRUNC(a.com_fecha) com_fecha, a.com_numero, a.uge_num_sri, a.pue_num_sri, a.fol_nro_folio, ',
'          a.uge_num_sri||''-''||a.pue_num_sri||''-''||LPAD(a.com_numero,7,''0'') folio,',
'          g.uge_id, ',
'          null usu_id,--a.usu_id, ',
'          NVL(a.com_aprobacion_electrica,''P'') com_aprobacion_electrica,',
'          b.ite_sku_id, ',
'          null cde_cantidad,--b.cde_cantidad, ',
'          b.bpr_id, b.ieb_id, b.cde_id, ',
'          c.ite_descripcion_larga, c.ite_serie_obligatoria, c.Ite_Sec_Id,',
'          e.per_nro_identificacion, ',
'          NVL(e.per_razon_social, e.per_primer_apellido||'' ''||e.per_segundo_apellido||'' ''||e.per_primer_nombre) nombre_cliente,',
'          null uge_nombre,--f.uge_nombre,',
'          g.age_nombre_comercial,',
'          k.uge_nombre nombre_bodega,',
'          l.pol_descripcion_larga',
'',
'',
'FROM      ven_comprobantes a,',
'          ven_comprobantes_det b,',
'          inv_items c,',
'          asdm_clientes d,',
'          asdm_personas e,',
'          --asdm_unidades_gestion f,',
'          asdm_agencias g,',
'          inv_bodegas_propias j,',
'          asdm_unidades_gestion k,',
'          ppr_politicas l',
'',
'WHERE     b.com_id = a.com_id',
'AND       c.ite_sku_id = b.ite_sku_id',
'AND       c.ite_serie_obligatoria = ''S''',
'--AND       NVL(a.com_aprobacion_electrica,''N'') = ''S''',
'AND       d.cli_id = a.cli_id',
'AND       e.per_id = d.per_id',
'--AND       f.uge_id = a.uge_id',
'AND       g.age_id = a.age_id_agencia',
'AND       j.bpr_id = b.bpr_id',
'AND       k.uge_id = j.uge_id',
'AND       l.pol_id = a.pol_id',
'AND       a.com_fecha >= TO_DATE(''01-08-2014'',''DD-MM-RRRR'')'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(37728278317364710)
,p_name=>'Report 1'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'IVEGA'
,p_internal_uid=>5475127047599784
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37728471034364729)
,p_db_column_name=>'NRO_REGISTRO'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Nro.'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37728563867364729)
,p_db_column_name=>'COM_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Comp.'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37728672522364729)
,p_db_column_name=>'EMP_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37728766308364729)
,p_db_column_name=>'PUE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37728883526364729)
,p_db_column_name=>'POL_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Pol Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'POL_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37728961029364730)
,p_db_column_name=>'CLI_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'C.Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729082262364730)
,p_db_column_name=>'COM_FECHA'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Fecha'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'COM_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729155123364730)
,p_db_column_name=>'COM_NUMERO'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Com Numero'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_NUMERO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729272370364730)
,p_db_column_name=>'UGE_NUM_SRI'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Uge Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729381714364730)
,p_db_column_name=>'PUE_NUM_SRI'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Pue Num Sri'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PUE_NUM_SRI'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729473300364730)
,p_db_column_name=>'FOL_NRO_FOLIO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Fol Nro Folio'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'FOL_NRO_FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729564625364731)
,p_db_column_name=>'UGE_ID'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37729768670364731)
,p_db_column_name=>'COM_APROBACION_ELECTRICA'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Aprobado?'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'COM_APROBACION_ELECTRICA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736058864374762)
,p_db_column_name=>'ITE_SKU_ID'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Ite Sku Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ITE_SKU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736265040374763)
,p_db_column_name=>'BPR_ID'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Bpr Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'BPR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736364119374763)
,p_db_column_name=>'IEB_ID'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Ieb Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'IEB_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736458957374763)
,p_db_column_name=>'CDE_ID'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Cde Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CDE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736580729374763)
,p_db_column_name=>'ITE_DESCRIPCION_LARGA'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'Descripcion Item'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ITE_DESCRIPCION_LARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736681544374763)
,p_db_column_name=>'ITE_SERIE_OBLIGATORIA'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Ite Serie Obligatoria'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ITE_SERIE_OBLIGATORIA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736771229374764)
,p_db_column_name=>'PER_NRO_IDENTIFICACION'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Ident.'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PER_NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37736977449374764)
,p_db_column_name=>'NOMBRE_CLIENTE'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Cliente'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_CLIENTE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37737064954374769)
,p_db_column_name=>'UGE_NOMBRE'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Uge Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37737153877374769)
,p_db_column_name=>'AGE_NOMBRE_COMERCIAL'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Agencia'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'AGE_NOMBRE_COMERCIAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37742374317458827)
,p_db_column_name=>'FOLIO'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Folio'
,p_column_link=>'f?p=&APP_ID.:115:&SESSION.::&DEBUG.::P115_COM_ID,P115_CILENTE,P115_FOLIO:#COM_ID#,#NOMBRE_CLIENTE#,#FOLIO#'
,p_column_linktext=>'#FOLIO#'
,p_column_link_attr=>'title="Aprobar esta Factura"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FOLIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37742779456501981)
,p_db_column_name=>'NOMBRE_BODEGA'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Bodega'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE_BODEGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37744671655551210)
,p_db_column_name=>'ITE_SEC_ID'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'C.Item'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'ITE_SEC_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(37745360644571478)
,p_db_column_name=>'POL_DESCRIPCION_LARGA'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Politica'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'POL_DESCRIPCION_LARGA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(8871853557018135217)
,p_db_column_name=>'USU_ID'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Usu Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'USU_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(8871853672330135230)
,p_db_column_name=>'CDE_CANTIDAD'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Cde Cantidad'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CDE_CANTIDAD'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(37730579351365397)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'54775'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'COM_ID:FOLIO:COM_FECHA:POL_DESCRIPCION_LARGA:AGE_NOMBRE_COMERCIAL:CLI_ID:PER_NRO_IDENTIFICACION:NOMBRE_CLIENTE:ITE_SEC_ID:ITE_DESCRIPCION_LARGA:NOMBRE_BODEGA:COM_APROBACION_ELECTRICA:'
,p_break_on=>'AGE_NOMBRE_COMERCIAL:0:0:0:0:0'
,p_break_enabled_on=>'AGE_NOMBRE_COMERCIAL:0:0:0:0:0'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(8871944572480142495)
,p_report_id=>wwv_flow_imp.id(37730579351365397)
,p_condition_type=>'HIGHLIGHT'
,p_allow_delete=>'Y'
,p_column_name=>'COM_APROBACION_ELECTRICA'
,p_operator=>'='
,p_expr=>'S'
,p_condition_sql=>' (case when ("COM_APROBACION_ELECTRICA" = #APXWS_EXPR#) then #APXWS_HL_ID# end) '
,p_condition_display=>'#APXWS_COL_NAME# = ''S''  '
,p_enabled=>'Y'
,p_highlight_sequence=>10
,p_row_bg_color=>'#EDECB0'
,p_row_font_color=>'#000000'
);
wwv_flow_imp_page.create_worksheet_condition(
 p_id=>wwv_flow_imp.id(8871944773827142495)
,p_report_id=>wwv_flow_imp.id(37730579351365397)
,p_name=>'Row text contains ''cuenca'''
,p_condition_type=>'SEARCH'
,p_allow_delete=>'Y'
,p_expr=>'cuenca'
,p_enabled=>'Y'
);
wwv_flow_imp.component_end;
end;
/
