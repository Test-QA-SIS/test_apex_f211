prompt --application/pages/page_00068
begin
--   Manifest
--     PAGE: 00068
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>68
,p_tab_set=>'T_PAGODECUOTARETENCION'
,p_name=>'Reporte_Retenciones_Aplicadas'
,p_step_title=>'Reporte_Retenciones_Aplicadas'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240105093834'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(101908366115155174)
,p_plug_name=>'Reporte de Retenciones Aplicadas '
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c.mca_id,',
'       a.rap_id,',
'       c.uge_id,',
'       (select uge.uge_nombre',
'          from asdm_unidades_gestion uge',
'         where uge.uge_id = c.uge_id',
'           and uge.emp_id = c.emp_id) Unidad_Gestion,',
'       c.cli_id,',
'       (select cli.nombre_completo',
'          from v_asdm_datos_clientes cli',
'         where cli.cli_id = c.cli_id',
'           and cli.emp_id = c.emp_id) nombre,',
'       (select cli.per_nro_identificacion',
'          from v_asdm_datos_clientes cli',
'         where cli.cli_id = c.cli_id',
'           and cli.emp_id = c.emp_id) nro_documento,',
'       b.mcd_valor_movimiento valor,',
'       a.rap_nro_establ_ret || ''-'' || a.rap_nro_pemision_ret || ''-'' ||',
'       a.rap_nro_retencion nro_retencion,',
'       a.rap_nro_autorizacion,',
'       a.rap_fecha,',
'       a.rap_fecha_validez,',
'       a.com_id,',
'       (select com.uge_num_sri || ''-'' || com.pue_num_sri || ''-'' ||',
'               com.com_numero',
'          from ven_comprobantes com',
'         where com.com_id = a.com_id',
'           and com.emp_id = a.emp_id) folio_factura,',
'       c.mca_fecha',
'  from asdm_retenciones_aplicadas    a,',
'       asdm_movimientos_caja_detalle b,',
'       asdm_movimientos_caja         c',
' where b.mcd_nro_retencion = a.rap_id',
'   and c.mca_id = b.mca_id',
'   and b.mcd_estado_registro = 0',
'   and a.rap_estado_registro = 0',
'   and c.mca_estado_registro = 0',
'   and c.emp_id = :f_emp_id',
'   and b.emp_id = :f_emp_id',
'   and a.emp_id = :f_emp_id',
'   AND TRUNC(c.mca_fecha) between :p68_fecha_ini and :p68_fecha_fin'))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(101908460891155174)
,p_name=>'Reporte_Retenciones_Aplicadas'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'YGUAMAN'
,p_internal_uid=>69655309621390248
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101908679642155195)
,p_db_column_name=>'RAP_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Rap Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'RAP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101909073812155199)
,p_db_column_name=>'COM_ID'
,p_display_order=>7
,p_column_identifier=>'E'
,p_column_label=>'Com Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COM_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101909183325155199)
,p_db_column_name=>'RAP_FECHA'
,p_display_order=>8
,p_column_identifier=>'F'
,p_column_label=>'Fecha Ret'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'RAP_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(101909266782155199)
,p_db_column_name=>'RAP_FECHA_VALIDEZ'
,p_display_order=>9
,p_column_identifier=>'G'
,p_column_label=>'Fecha Validez'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'RAP_FECHA_VALIDEZ'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119290353660877665)
,p_db_column_name=>'CLI_ID'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Cli Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(119290451867877670)
,p_db_column_name=>'NOMBRE'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144060130517734)
,p_db_column_name=>'MCA_ID'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Mov Caja'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144175571517738)
,p_db_column_name=>'UGE_ID'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'Uge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144274994517738)
,p_db_column_name=>'UNIDAD_GESTION'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'UNIDAD_GESTION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144351917517738)
,p_db_column_name=>'VALOR'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Valor'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'VALOR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144480997517738)
,p_db_column_name=>'NRO_RETENCION'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Nro Retencion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_RETENCION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144554601517739)
,p_db_column_name=>'FOLIO_FACTURA'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Folio Factura'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FOLIO_FACTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6106144778471517739)
,p_db_column_name=>'MCA_FECHA'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'Fecha Adicion'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'MCA_FECHA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(6107832170275653107)
,p_db_column_name=>'NRO_DOCUMENTO'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'Nro Documento'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NRO_DOCUMENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(18943038657905969764)
,p_db_column_name=>'RAP_NRO_AUTORIZACION'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Rap Nro Autorizacion'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'RAP_NRO_AUTORIZACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(101910276070155342)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'696572'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'MCA_ID:RAP_ID:UGE_ID:UNIDAD_GESTION:CLI_ID:NOMBRE:MCA_FECHA:VALOR:NRO_RETENCION:RAP_FECHA:RAP_FECHA_VALIDEZ:COM_ID:FOLIO_FACTURA:RAP_NRO_AUTORIZACION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(6106759564941566357)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(101908366115155174)
,p_button_name=>'CARGAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cargar'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6106644269311558227)
,p_name=>'P68_FECHA_INI'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(101908366115155174)
,p_item_default=>'trunc(sysdate)-30'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Ini'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(6106694581432561721)
,p_name=>'P68_FECHA_FIN'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(101908366115155174)
,p_item_default=>'trunc(sysdate)'
,p_item_default_type=>'EXPRESSION'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Fecha Fin'
,p_display_as=>'NATIVE_DATE_PICKER_APEX'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_imp.component_end;
end;
/
