prompt --application/pages/page_00042
begin
--   Manifest
--     PAGE: 00042
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>42
,p_name=>'CIERRES DE CAJA'
,p_step_title=>'CIERRES DE CAJA'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20240112112519'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(51776569140089374)
,p_plug_name=>'CIERRE DE CAJA'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT pca.pca_id,',
'       pca.emp_id,',
'       kseg_p.pq_kseg_devuelve_datos.fn_devuelve_uname(usu_id) username,',
'       pca.pue_id,',
'       p.pue_nombre,',
'       pca.pca_fechahora_inicio,',
'       pca.pca_fechahora_final,',
'       pca.pca_valor_cierre_sistema,',
'       pca.pca_valor_cierre_fisico,',
'       pq_lv_kdda.fn_obtiene_valor_lov(176,pca.pca_estado_pc) estado_pc,',
'      ''imprimir'' imprimir,',
'       pca.pca_fecha_sistema,',
'       pq_lv_kdda.fn_obtiene_valor_lov(78,pca.pca_estado_registro) estado                                    ',
'',
'FROM  ',
'       ven_periodos_caja pca,',
'       asdm_puntos_emision p',
'       ',
'WHERE  pca.emp_id = :f_emp_id',
'       AND pca.usu_id = :f_user_id',
'       and p.pue_id = pca.pue_id',
'       and p.uge_id = :f_uge_id',
'ORDER  BY 1 DESC',
''))
,p_plug_source_type=>'NATIVE_IR'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(51776681850089374)
,p_name=>'CIERRE DE CAJA'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than 10,000 rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_enable_mail_download=>'N'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'PLOPEZ'
,p_internal_uid=>19523530580324448
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51776864399089378)
,p_db_column_name=>'PCA_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Periodo #'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51776976940089378)
,p_db_column_name=>'PCA_FECHAHORA_INICIO'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Fecha Apertura'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'PCA_FECHAHORA_INICIO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51777057678089380)
,p_db_column_name=>'PCA_FECHAHORA_FINAL'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Fecha Fin'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_static_id=>'PCA_FECHAHORA_FINAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51777171080089380)
,p_db_column_name=>'PCA_VALOR_CIERRE_FISICO'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Valor Cierre'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_VALOR_CIERRE_FISICO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(51777272512089380)
,p_db_column_name=>'PCA_VALOR_CIERRE_SISTEMA'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Valor Cierre<br>Automatico'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PCA_VALOR_CIERRE_SISTEMA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76153576000603776)
,p_db_column_name=>'USERNAME'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Usuario'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'USERNAME'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76154163571609963)
,p_db_column_name=>'ESTADO_PC'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ESTADO_PC'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76154960586618572)
,p_db_column_name=>'EMP_ID'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Emp Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'EMP_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76155076850618572)
,p_db_column_name=>'PUE_ID'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Pue Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_static_id=>'PUE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76155175548618572)
,p_db_column_name=>'PCA_FECHA_SISTEMA'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Pca Fecha Sistema'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_static_id=>'PCA_FECHA_SISTEMA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76155269531618572)
,p_db_column_name=>'ESTADO'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Estado'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'ESTADO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76155963403647755)
,p_db_column_name=>'PUE_NOMBRE'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Pue Nombre'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'PUE_NOMBRE'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(76657673918227436)
,p_db_column_name=>'IMPRIMIR'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Imprimir'
,p_column_link=>'javascript:popUp2(''../../reports/rwservlet?module=cierre_caja.RDF&userid=asdm_p/asdm_p@&P0_BD.&destype=cache&desformat=pdf&pn_pca_id=#PCA_ID#&pn_emp_id=&F_EMP_ID.'', 850, 500,85);'
,p_column_linktext=>'#IMPRIMIR#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_static_id=>'IMPRIMIR'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(51777475586089533)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'1293009878919698'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PUE_NOMBRE:PCA_ID:PCA_FECHAHORA_INICIO:PCA_FECHAHORA_FINAL:PCA_VALOR_CIERRE_FISICO:PCA_VALOR_CIERRE_SISTEMA:USERNAME:ESTADO_PC:IMPRIMIR'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(134141968908589741)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(51776569140089374)
,p_button_name=>'CIERRE_DE_TODO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Cierre De Todo'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(51778076567093126)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(51776569140089374)
,p_button_name=>'NUEVO'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Nuevo'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(51778367822093129)
,p_branch_action=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:40::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(51778076567093126)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(134144374711610263)
,p_branch_action=>'f?p=&APP_ID.:42:&SESSION.:CIERRE_DE_TODO:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(134141968908589741)
,p_branch_sequence=>20
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 24-JAN-2012 18:38 by ACALLE'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(134142176527591897)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_cierra'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'cursor datos is',
'select pue.pue_id, pue.uge_id, pca.pca_id, pca.usu_id from asdm_puntos_emision pue, ven_periodos_caja pca',
'where pca.pue_id = pue.pue_id',
'and pca.emp_id = pue.emp_id',
'and pca.pca_estado_pc = ''A'';',
'',
'ln_trx_id asdm_transacciones.trx_id%TYPE;',
'ln_mca_id asdm_movimientos_caja.mca_id%TYPE;',
'pv_error varchar2(100);',
'begin',
'  for c in datos loop',
'PRUEBAS_CAR_AUX(666,c.pca_id);',
'pq_ven_movimientos_caja.pr_cierre_caja(pn_emp_id       => 1,',
'                         pn_uge_id       => c.uge_id,',
'                         pn_user_id      => c.usu_id,',
'                         pn_trx_id       => ln_trx_id,',
'                         pn_uge_id_gasto => null,',
'                         pn_mca_id       => ln_mca_id,',
'                         pn_pca_id       => c.pca_id,',
'                         pn_mca_total    => 0,',
'                         pn_ede_id       => 5,',
'                         pn_diferencia   => 0,',
'                         pv_error        => pv_error);',
'end loop;',
'--ROLLBACK;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'CIERRE_DE_TODO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>101889025257826971
);
wwv_flow_imp.component_end;
end;
/
