prompt --application/pages/page_00121
begin
--   Manifest
--     PAGE: 00121
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>121
,p_name=>'EMISION DE SEGUROS'
,p_alias=>'EMISION_SEGUROS'
,p_step_title=>'EMISION DE SEGUROS'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_help_text=>'No help is available for this page.'
,p_page_component_map=>'03'
,p_last_upd_yyyymmddhh24miss=>'20240112115256'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(64925263762982501)
,p_plug_name=>'Emision Certificado de Seguro'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(64937983090158401)
,p_name=>'Datos del Cliente'
,p_parent_plug_id=>wwv_flow_imp.id(64925263762982501)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.cli_id,',
'       c.per_nro_identificacion nro_identificacion,',
'       c.nombre_completo nombres,',
'       b.pse_descripcion plan_seguro,',
'       a.pse_valor_mensual valor_mensual,',
'       a.ose_plazo_cobertura plazo',
'  FROM ven_ordenes_seguros a, asdm_plan_seguros b, v_asdm_datos_clientes c',
' WHERE b.pse_id = a.pse_id',
'   AND c.cli_id = a.cli_id',
'   AND a.ose_id = :p121_ose_id'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>2
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_row_count_max=>500
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64938262429158418)
,p_query_column_id=>1
,p_column_alias=>'CLI_ID'
,p_column_display_sequence=>1
,p_column_heading=>'CLI_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64938356031158420)
,p_query_column_id=>2
,p_column_alias=>'NRO_IDENTIFICACION'
,p_column_display_sequence=>2
,p_column_heading=>'NRO_IDENTIFICACION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64938473672158420)
,p_query_column_id=>3
,p_column_alias=>'NOMBRES'
,p_column_display_sequence=>3
,p_column_heading=>'NOMBRES'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64938574935158420)
,p_query_column_id=>4
,p_column_alias=>'PLAN_SEGURO'
,p_column_display_sequence=>4
,p_column_heading=>'PLAN_SEGURO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64938667192158420)
,p_query_column_id=>5
,p_column_alias=>'VALOR_MENSUAL'
,p_column_display_sequence=>5
,p_column_heading=>'VALOR_MENSUAL'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64938756990158420)
,p_query_column_id=>6
,p_column_alias=>'PLAZO'
,p_column_display_sequence=>6
,p_column_heading=>'PLAZO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(728257557477780095)
,p_plug_name=>'PAGO ENTRADA'
,p_parent_plug_id=>wwv_flow_imp.id(64937983090158401)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270525766496046669)
,p_plug_display_sequence=>60
,p_plug_display_point=>'SUB_REGIONS'
,p_plug_display_condition_type=>'EXPRESSION'
,p_plug_display_when_condition=>':P121_ENTRADA > 0'
,p_plug_display_when_cond2=>'PLSQL'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(728262052947901780)
,p_name=>'Detalle Pagos Movimiento Caja'
,p_parent_plug_id=>wwv_flow_imp.id(728257557477780095)
,p_template=>wwv_flow_imp.id(270524859981046669)
,p_display_sequence=>70
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_point=>'SUB_REGIONS'
,p_item_display_point=>'BELOW'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'FUNC_BODY_RETURNING_SQL'
,p_function_body_language=>'PLSQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_ven_movimientos_caja.fn_mostrar_coleccion_mov_caja(:P0_ERROR);',
''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_plug_query_max_columns=>30
,p_query_headings_type=>'QUERY_COLUMNS'
,p_query_options=>'GENERIC_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_num_rows_type=>'0'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_sort_null=>'F'
,p_query_asc_image_attr=>'width="13" height="12" alt=""'
,p_query_desc_image_attr=>'width="13" height="12" alt=""'
,p_plug_query_strip_html=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262252078901794)
,p_query_column_id=>1
,p_column_alias=>'COL01'
,p_column_display_sequence=>1
,p_column_heading=>'Col01'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_SEQ_ID,P30_TFP_ID,P30_EDE_ID,P30_PLAN,P30_MCD_VALOR_MOVIMIENTO,P30_CRE_TITULAR_CUENTA,P30_CRE_NRO_CUENTA,P30_CRE_NRO_CHEQUE,P30_CRE_NRO_PIN,P30_TJ_NUMERO,P30_MCD_NRO_AUT_REF,P30_TJ_NUMERO_VOUCHER,P30_LOTE,P30_M'
||'CD_NRO_COMPROBANTE,P30_RAP_NRO_RETENCION,P30_SALDO_RETENCION,P30_RAP_COM_ID,P30_RAP_FECHA,P30_RAP_FECHA_VALIDEZ,P30_RAP_NRO_AUTORIZACION,P30_MODIFICACION,P30_RAP_NRO_ESTABL_RET,P30_RAP_NRO_PEMISION_RET,P30_PRE_ID:#COL03#,#COL04#,#COL05#,#COL05#,#COL3'
||'0#,#COL11#,#COL12#,#COL13#,#COL14#,#COL15#,#COL16#,#COL17#,#COL18#,#COL19#,#COL20#,#COL21#,#COL22#,#COL23#,#COL24#,#COL25#,S,#COL26#,#COL27#,#COL28#'
,p_column_linktext=>'Editar'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262375572901799)
,p_query_column_id=>2
,p_column_alias=>'COL02'
,p_column_display_sequence=>2
,p_column_heading=>'Col02'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:30:&SESSION.:eliminar:&DEBUG.::P30_SEQ_ID:#COL03#'
,p_column_linktext=>'#COL02#'
,p_column_link_attr=>'class="lock_ui_row"'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262462218901799)
,p_query_column_id=>3
,p_column_alias=>'COL03'
,p_column_display_sequence=>3
,p_column_heading=>'Col03'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262575802901799)
,p_query_column_id=>4
,p_column_alias=>'COL04'
,p_column_display_sequence=>4
,p_column_heading=>'Col04'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262679326901799)
,p_query_column_id=>5
,p_column_alias=>'COL05'
,p_column_display_sequence=>5
,p_column_heading=>'Col05'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262777520901799)
,p_query_column_id=>6
,p_column_alias=>'COL06'
,p_column_display_sequence=>6
,p_column_heading=>'Col06'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262866034901799)
,p_query_column_id=>7
,p_column_alias=>'COL07'
,p_column_display_sequence=>7
,p_column_heading=>'Col07'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728262962982901799)
,p_query_column_id=>8
,p_column_alias=>'COL08'
,p_column_display_sequence=>8
,p_column_heading=>'Col08'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263064886901799)
,p_query_column_id=>9
,p_column_alias=>'COL09'
,p_column_display_sequence=>9
,p_column_heading=>'Col09'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263182520901799)
,p_query_column_id=>10
,p_column_alias=>'COL10'
,p_column_display_sequence=>10
,p_column_heading=>'Col10'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263264486901799)
,p_query_column_id=>11
,p_column_alias=>'COL11'
,p_column_display_sequence=>11
,p_column_heading=>'Col11'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263381731901799)
,p_query_column_id=>12
,p_column_alias=>'COL12'
,p_column_display_sequence=>12
,p_column_heading=>'Col12'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263451906901799)
,p_query_column_id=>13
,p_column_alias=>'COL13'
,p_column_display_sequence=>13
,p_column_heading=>'Col13'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263554680901799)
,p_query_column_id=>14
,p_column_alias=>'COL14'
,p_column_display_sequence=>14
,p_column_heading=>'Col14'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263662470901799)
,p_query_column_id=>15
,p_column_alias=>'COL15'
,p_column_display_sequence=>15
,p_column_heading=>'Col15'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263767183901799)
,p_query_column_id=>16
,p_column_alias=>'COL16'
,p_column_display_sequence=>16
,p_column_heading=>'Col16'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263852133901799)
,p_query_column_id=>17
,p_column_alias=>'COL17'
,p_column_display_sequence=>17
,p_column_heading=>'Col17'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728263983392901799)
,p_query_column_id=>18
,p_column_alias=>'COL18'
,p_column_display_sequence=>18
,p_column_heading=>'Col18'
,p_use_as_row_header=>'N'
,p_column_linktext=>'Eliminar'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264055987901799)
,p_query_column_id=>19
,p_column_alias=>'COL19'
,p_column_display_sequence=>19
,p_column_heading=>'Col19'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264165070901799)
,p_query_column_id=>20
,p_column_alias=>'COL20'
,p_column_display_sequence=>20
,p_column_heading=>'Col20'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264252733901799)
,p_query_column_id=>21
,p_column_alias=>'COL21'
,p_column_display_sequence=>21
,p_column_heading=>'Col21'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_cattributes_element=>'style="color:RED;"'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264358288901799)
,p_query_column_id=>22
,p_column_alias=>'COL22'
,p_column_display_sequence=>22
,p_column_heading=>'Col22'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264483787901799)
,p_query_column_id=>23
,p_column_alias=>'COL23'
,p_column_display_sequence=>23
,p_column_heading=>'Col23'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264570930901799)
,p_query_column_id=>24
,p_column_alias=>'COL24'
,p_column_display_sequence=>24
,p_column_heading=>'Col24'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264673593901799)
,p_query_column_id=>25
,p_column_alias=>'COL25'
,p_column_display_sequence=>25
,p_column_heading=>'Col25'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264754406901799)
,p_query_column_id=>26
,p_column_alias=>'COL26'
,p_column_display_sequence=>26
,p_column_heading=>'Col26'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264872998901799)
,p_query_column_id=>27
,p_column_alias=>'COL27'
,p_column_display_sequence=>27
,p_column_heading=>'Col27'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728264958821901799)
,p_query_column_id=>28
,p_column_alias=>'COL28'
,p_column_display_sequence=>28
,p_column_heading=>'Col28'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728265077495901799)
,p_query_column_id=>29
,p_column_alias=>'COL29'
,p_column_display_sequence=>29
,p_column_heading=>'Col29'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXPRESSION'
,p_display_when_condition=>':P30_FACTURAR <> ''R'''
,p_display_when_condition2=>'PLSQL'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(728265162566901799)
,p_query_column_id=>30
,p_column_alias=>'COL30'
,p_column_display_sequence=>30
,p_column_heading=>'Col30'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(728298154304092814)
,p_plug_name=>'pago'
,p_parent_plug_id=>wwv_flow_imp.id(64937983090158401)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>80
,p_plug_display_point=>'SUB_REGIONS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(65007275302911745)
,p_plug_name=>'DETALLE'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>50
,p_plug_display_point=>'BODY_3'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(64997356508849515)
,p_name=>'Cronograma Pagos'
,p_parent_plug_id=>wwv_flow_imp.id(65007275302911745)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>30
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.div_nro_vencimiento, d.div_fecha_vencimiento, d.div_valor_cuota',
'  FROM ven_ordenes_seguros o, car_cuentas_por_cobrar c, car_dividendos d',
' WHERE c.cxc_id = o.cxc_id_seguro',
'   AND d.cxc_id = c.cxc_id',
'   AND o.ose_id = :p121_ose_id',
'   and d.div_nro_vencimiento > 0'))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64997651625849636)
,p_query_column_id=>1
,p_column_alias=>'DIV_NRO_VENCIMIENTO'
,p_column_display_sequence=>1
,p_column_heading=>'DIV_NRO_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64997781044849638)
,p_query_column_id=>2
,p_column_alias=>'DIV_FECHA_VENCIMIENTO'
,p_column_display_sequence=>2
,p_column_heading=>'DIV_FECHA_VENCIMIENTO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(64997865899849638)
,p_query_column_id=>3
,p_column_alias=>'DIV_VALOR_CUOTA'
,p_column_display_sequence=>3
,p_column_heading=>'DIV_VALOR_CUOTA'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_report_region(
 p_id=>wwv_flow_imp.id(65001372485882497)
,p_name=>'Cronograma Cobertura'
,p_parent_plug_id=>wwv_flow_imp.id(65007275302911745)
,p_template=>wwv_flow_imp.id(270526367644046670)
,p_display_sequence=>40
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_display_point=>'SUB_REGIONS'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select c001 Nro_Pago, c002 Fecha_Cobertura, to_number(c003) Valor_Cobertura',
'from apex_collections where collection_name = ''COL_CRON_COB_MUESTRA'''))
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_query_row_template=>wwv_flow_imp.id(270528766641046671)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65001664145882596)
,p_query_column_id=>1
,p_column_alias=>'NRO_PAGO'
,p_column_display_sequence=>1
,p_column_heading=>'NRO_PAGO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65001761584882599)
,p_query_column_id=>2
,p_column_alias=>'FECHA_COBERTURA'
,p_column_display_sequence=>2
,p_column_heading=>'FECHA_COBERTURA'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_report_columns(
 p_id=>wwv_flow_imp.id(65001875341882599)
,p_query_column_id=>3
,p_column_alias=>'VALOR_COBERTURA'
,p_column_display_sequence=>3
,p_column_heading=>'VALOR_COBERTURA'
,p_use_as_row_header=>'N'
,p_sum_column=>'Y'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(65015272925939362)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(728298154304092814)
,p_button_name=>'GRABAR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Emitir Seguro'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_condition=>'(:P121_ENTRADA = NVL(:P121_SUMA_PAGOS,0))'
,p_button_condition2=>'PLSQL'
,p_button_condition_type=>'EXPRESSION'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(65005261104907643)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(64937983090158401)
,p_button_name=>'REGRESAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Regresar'
,p_button_position=>'TOP'
,p_button_redirect_url=>'f?p=&APP_ID.:27:&SESSION.::&DEBUG.:::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(65015763977949039)
,p_branch_name=>'br_redirect'
,p_branch_action=>'f?p=&APP_ID.:121:&SESSION.:GRABAR:&DEBUG.:::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_imp.id(65015272925939362)
,p_branch_sequence=>10
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(64925665406984702)
,p_name=>'P121_CLI_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(64925263762982501)
,p_prompt=>'Cli Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(64925873978984716)
,p_name=>'P121_OSE_ID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(64925263762982501)
,p_prompt=>'Ose Id'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(728258174911785268)
,p_name=>'P121_VALOR_PAGAR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(728257557477780095)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Valor Pagar'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.div_saldo_cuota',
'  FROM ven_ordenes_seguros o, car_cuentas_por_cobrar c, car_dividendos d',
' WHERE d.cxc_id = c.cxc_id',
'   AND c.cxc_id = o.cxc_id_seguro',
'   AND o.ose_id = :p121_ose_id',
'   AND o.cse_estado_registro = 0',
'   and d.div_nro_vencimiento = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'style="font-size:24"'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(728258365133785275)
,p_name=>'P121_SALDO_PAGO'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(728257557477780095)
,p_prompt=>'Saldo Pago'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(728258555377785275)
,p_name=>'P121_TOTAL'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_imp.id(728257557477780095)
,p_prompt=>'Total'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>10
,p_cMaxlength=>10
,p_cHeight=>1
,p_cAttributes=>'nowrap'
,p_tag_attributes=>'onKeyDown="if(event.keyCode==13) doSubmit(''cargar_tfp'');" '
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(728258953776795118)
,p_name=>'P121_FORMA_PAGO'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(728257557477780095)
,p_item_default=>wwv_flow_string.join(wwv_flow_t_varchar2(
'return pq_constantes.fn_retorna_constante(:f_emp_id,''cn_tfp_id_efectivo'');',
''))
,p_item_default_type=>'FUNCTION_BODY'
,p_item_default_language=>'PLSQL'
,p_prompt=>'Forma Pago'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC2:Efectivo;1'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_tag_attributes=>' onChange="doSubmit(''limpia'')"; '
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_imp.id(270535082543046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(728271278331087561)
,p_name=>'P121_SUMA_PAGOS'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_imp.id(728257557477780095)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Suma Pagos'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT NVL(SUM(TO_NUMBER(c006)),0) suma_valor',
'  FROM apex_collections',
'WHERE collection_name  = pq_constantes.fn_retorna_constante(NULL,''cv_coleccion_mov_caja'')'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(728304459593393953)
,p_name=>'P121_ENTRADA'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_imp.id(64925263762982501)
,p_use_cache_before_default=>'NO'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT d.div_saldo_cuota',
'  FROM ven_ordenes_seguros o, car_cuentas_por_cobrar c, car_dividendos d',
' WHERE d.cxc_id = c.cxc_id',
'   AND c.cxc_id = o.cxc_id_seguro',
'   AND o.ose_id = :p121_ose_id',
'   AND o.cse_estado_registro = 0',
'   and d.div_nro_vencimiento = 0'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(65015461974945738)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_grabar'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    ln_ord_origen number;',
unistr('    ln_pse_id           number; --JALVAREZ R3584-02 AJUSTAR PROCESO DE VALIDACI\00D3N DE CUOTAS INTERNO DONDE CONSIDERA O NO EL VALOR DE LACUOTA DEL SEGURO O PLAN CON LA CUOTA ASIGNADA DEL CLIENTE.'),
'    lv_valida_cuota     asdm_plan_seguros.pse_valida_cuota%type;',
'begin',
'',
'begin',
'    select ord_origen,pse_id',
unistr('    into ln_ord_origen, ln_pse_id--JALVAREZ R3584-02 AJUSTAR PROCESO DE VALIDACI\00D3N DE CUOTAS INTERNO DONDE CONSIDERA O NO EL VALOR DE LACUOTA DEL SEGURO O PLAN CON LA CUOTA ASIGNADA DEL CLIENTE.'),
'    from ven_ordenes_seguros',
'    where ose_id=:P121_OSE_ID;',
'exception',
'    when others then',
'    ln_ord_origen := null;',
'end;',
unistr('       ---INICIO JALVAREZ R3584-02 AJUSTAR PROCESO DE VALIDACI\00D3N DE CUOTAS INTERNO DONDE CONSIDERA O NO EL VALOR DE LACUOTA DEL SEGURO O PLAN CON LA CUOTA ASIGNADA DEL CLIENTE.'),
'            BEGIN',
'              select nvl(s.pse_valida_cuota, ''N'')',
'                into lv_valida_cuota',
'                from asdm_plan_seguros s',
'               where s.pse_id = ln_pse_id;',
'            EXCEPTION',
'              WHEN OTHERS THEN',
'                lv_valida_cuota := ''N'';',
'            END;',
unistr('            ---FIN JALVAREZ R3584-02 AJUSTAR PROCESO DE VALIDACI\00D3N DE CUOTAS INTERNO DONDE CONSIDERA O NO EL VALOR DE LACUOTA DEL SEGURO O PLAN CON LA CUOTA ASIGNADA DEL CLIENTE.'),
'          ',
'    pq_asdm_seguros.pr_gen_cartera_cobertura(pn_emp_id => :f_emp_id,',
'                                            pn_uge_id => :f_uge_id,',
'                                            pn_usu_id => :f_user_id,',
'                                            pn_ose_id => :p121_ose_id,',
'                                            pn_valor_entrada => nvl(:p121_valor_pagar,0),',
'                                            pn_ord_origen    => ln_ord_origen,',
unistr('                                            pv_valida_cupo   => lv_valida_cuota, --JALVAREZ R3584-02 AJUSTAR PROCESO DE VALIDACI\00D3N DE CUOTAS INTERNO DONDE CONSIDERA O NO EL VALOR DE LACUOTA DEL SEGURO O PLAN CON LA CUOTA ASIGNADA DEL CLIENTE.'),
'                                            pv_error  => :p0_error);',
'',
'if apex_collection.collection_exists(''COL_CRON_COB_MUESTRA'') then',
'apex_collection.delete_collection(''COL_CRON_COB_MUESTRA'');',
'end if;',
' pq_ven_comprobantes.pr_redirect(pv_desde        => ''F'',',
'                                      pn_app_id       => NULL,',
'                                      pn_pag_id       => NULL,',
'                                      pn_emp_id       => :f_emp_id,',
'                                      pv_session      => :session,',
'                                      pv_token        => :f_token,',
'                                      pn_user_id      => :f_user_id,',
'                                      pn_rol          => :p0_rol,',
'                                      pv_rol_desc     => :p0_rol_desc,',
'                                      pn_tree_rot     => :p0_tree_root,',
'                                      pn_opcion       => :f_opcion_id,',
'                                      pv_parametro    => :f_parametro,',
'                                      pv_empresa      => :f_empresa,',
'                                      pn_uge_id       => :f_uge_id,',
'                                      pn_uge_id_gasto => :f_uge_id_gasto,',
'                                      pv_ugestion     => :f_ugestion,',
'                                      pv_error        => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when_button_id=>wwv_flow_imp.id(65015272925939362)
,p_process_when=>'GRABAR'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>32762310705180812
);
wwv_flow_imp.component_end;
end;
/
begin
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(728261852469892147)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_carga_tfp'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
' pq_ven_movimientos_caja.pr_valida_tfp_repetidos(pn_emp_id => :f_emp_id,',
'                                                    pn_tfp_id => :p121_forma_pago,',
'                                                    pv_error  => :p0_error);',
'  pq_ven_movimientos_caja.pr_carga_coleccion_mov_caja(pn_ede_id               => NULL,',
'                                                      pn_cre_id               => NULL,',
'                                                      pn_tfp_id               => :p121_forma_pago,',
'                                                      pn_mcd_valor_movimiento => :p121_total, ',
'                                                      pn_mcd_nro_aut_ref      => NULL,',
'                                                      pn_mcd_nro_lote         => NULL,',
'                                                      pn_mcd_nro_retencion    => NULL,',
'                                                      pn_mcd_nro_comprobante  => NULL,',
'                                                      pv_titular_cuenta       => NULL,',
'                                                      pv_cre_nro_cheque       => NULL,',
'                                                      pv_cre_nro_cuenta       => NULL,',
'                                                      pv_nro_pin              => NULL,',
'                                                      pv_nro_tarjeta          => NULL,',
'                                                      pv_voucher              => NULL,',
'                                                      pn_saldo_retencion      => 0,',
'                                                      pn_com_id               => NULL,',
'                                                      pd_rap_fecha            => NULL,',
'                                                      pd_rap_fecha_validez    => NULL,',
'                                                      pn_rap_nro_autorizacion => NULL,',
'                                                      pn_rap_nro_establ_ret   => NULL,',
'                                                      pn_rap_nro_pemision_ret => NULL,',
'                                                      pn_pre_id               => NULL,',
'                                                      pd_fecha_deposito       => NULL,',
'                                                      pv_titular_tarjeta_cre  => NULL,',
'                                                      pv_error                => :p0_error);',
'',
'END;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'cargar_tfp'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>696008701200127221
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(65000873046873182)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_col_cobertura'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'  lr_orden_seguro      ven_ordenes_seguros%ROWTYPE;',
'  ld_fecha_vencimiento date;',
'  ln_porc_seguro       number;',
'  ln_valor_cuota       number;',
'begin',
'',
'  select *',
'    into lr_orden_seguro',
'    from ven_ordenes_seguros',
'   where ose_id = :p121_ose_id',
'     and cse_id is null;',
'',
'  select pse.pse_porcentaje_capital',
'     into ln_porc_seguro',
'     from asdm_e.Asdm_plan_seguros pse',
'    where pse.pse_id = lr_orden_seguro.pse_id;',
'',
'  apex_collection.create_or_truncate_collection(''COL_CRON_COB_MUESTRA'');',
'  ld_fecha_vencimiento := trunc(SYSDATE);',
'',
'',
'  FOR ln_div IN 1 .. lr_orden_seguro.ose_plazo_cobertura LOOP',
'    ld_fecha_vencimiento := add_months(ld_fecha_vencimiento, 1);',
'    ln_valor_cuota       := pq_asdm_seguros.fn_valor_cuota(pn_porcentaje_capital => ln_porc_seguro,',
'                                                           pn_valor_mensual      => lr_orden_seguro.pse_valor_mensual,',
'                                                           pn_num_div            => ln_div,',
'                                                           pn_cxc_id             => lr_orden_seguro.cxc_id_seguro);',
'  ',
'    apex_collection.add_member(''COL_CRON_COB_MUESTRA'',',
'                               ln_div,',
'                               ld_fecha_vencimiento,',
'                               ln_valor_cuota);',
'  END LOOP;',
'exception',
'  when no_data_found then',
'    null;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>32747721777108256
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(728268261913951669)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_limpia'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Declare',
'lv_nombre_coleccion varchar2(100):= pq_constantes.fn_retorna_constante(0,',
'                                                              ''cv_coleccion_mov_caja'');',
'begin',
'if apex_collection.collection_exists(lv_nombre_coleccion) then',
'apex_collection.delete_collection(lv_nombre_coleccion);',
'end if;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_when=>'NUEVO'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_internal_uid=>696015110644186743
);
wwv_flow_imp.component_end;
end;
/
