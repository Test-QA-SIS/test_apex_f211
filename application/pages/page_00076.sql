prompt --application/pages/page_00076
begin
--   Manifest
--     PAGE: 00076
--   Manifest End
wwv_flow_imp.component_begin (
 p_version_yyyy_mm_dd=>'2023.10.31'
,p_release=>'23.2.0'
,p_default_workspace_id=>27225405329845973
,p_default_application_id=>211
,p_default_id_offset=>16126575634882463
,p_default_owner=>'ASDM_P'
);
wwv_flow_imp_page.create_page(
 p_id=>76
,p_name=>'PROVISIONES POR MONTOS'
,p_step_title=>'PROVISIONES POR MONTOS'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_imp.id(270517474606046661)
,p_page_template_options=>'#DEFAULT#'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_component_map=>'18'
,p_last_upd_yyyymmddhh24miss=>'20220518102018'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(122945354466364636)
,p_plug_name=>'Generar Provisiones x Montos'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_imp_page.create_page_plug(
 p_id=>wwv_flow_imp.id(122946776970364660)
,p_plug_name=>'Provisiones Generadas'
,p_parent_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_imp.id(270526367644046670)
,p_plug_display_sequence=>10
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'SUB_REGIONS'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select a.*',
'  from car_provisiones_generadas a, car_tipos_provisiones b',
' where b.tpr_id = a.tpr_id',
' and tnp_id = 2'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_imp_page.create_worksheet(
 p_id=>wwv_flow_imp.id(122946969717364665)
,p_name=>'Provisiones Generadas'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_lazy_loading=>false
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML'
,p_enable_mail_download=>'Y'
,p_detail_link=>'f?p=&APP_ID.:82:&SESSION.::&DEBUG.::P82_TGE_ID:#PGE_ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#magnifying_glass_white_bg.gif" alt="">'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'ACALLE'
,p_internal_uid=>90693818447599739
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947058456364668)
,p_db_column_name=>'TPR_ID'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Tipo Provision'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TPR_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947151295364668)
,p_db_column_name=>'PGE_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Pge Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947276684364668)
,p_db_column_name=>'UGE_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Unidad Gestion'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'UGE_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947366244364668)
,p_db_column_name=>'TRX_ID'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'trx_id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'TRX_ID'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947481907364668)
,p_db_column_name=>'PGE_MES_PROVISION'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Mes'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_MES_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947559981364668)
,p_db_column_name=>'PGE_ANIO_PROVISION'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Anio'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_ANIO_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947659623364669)
,p_db_column_name=>'PGE_NUM_PROVISION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Numero Provision'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_NUM_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947783966364669)
,p_db_column_name=>'PGE_VALOR_PROVISION'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Valor Provision'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_VALOR_PROVISION'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947878095364669)
,p_db_column_name=>'PGE_ESTADO_REGISTRO'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Pge Estado Registro'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_ESTADO_REGISTRO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_column(
 p_id=>wwv_flow_imp.id(122947970187364669)
,p_db_column_name=>'PGE_NOTA_CREDITO'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Pge Nota Credito'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PGE_NOTA_CREDITO'
,p_use_as_row_header=>'N'
);
wwv_flow_imp_page.create_worksheet_rpt(
 p_id=>wwv_flow_imp.id(122948073286364670)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'906950'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PGE_ID:TPR_ID:UGE_ID:TRX_ID:PGE_MES_PROVISION:PGE_ANIO_PROVISION:PGE_NUM_PROVISION:PGE_VALOR_PROVISION:PGE_ESTADO_REGISTRO:PGE_NOTA_CREDITO'
,p_sort_column_1=>'PGE_ID'
,p_sort_direction_1=>'DESC'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(122945552998364645)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_button_name=>'GENERA'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Generar Nueva Provision'
,p_button_position=>'BOTTOM'
);
wwv_flow_imp_page.create_page_button(
 p_id=>wwv_flow_imp.id(122945767500364646)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_button_name=>'CONSULTAR_PROVISION'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_imp.id(270537079433046677)
,p_button_image_alt=>'Consultar Nueva Provision'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:77:&SESSION.::&DEBUG.:77::'
);
wwv_flow_imp_page.create_page_branch(
 p_id=>wwv_flow_imp.id(122948773914364692)
,p_branch_action=>'f?p=&APP_ID.:76:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'Created 21-NOV-2011 18:51 by ACALLE'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(69945763085579085)
,p_name=>'P76_AGE_ID'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Age Id'
,p_source=>':F_AGE_ID_AGENCIA'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(70214066507794623)
,p_name=>'P76_NUM_PROVISION'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Numero de Provisiones: '
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select tnp_frecuencia_meses',
'  From car_tipos_nc_prov',
' where tnp_id =',
'       pq_constantes.fn_retorna_constante(null, ''cn_tnp_id_reb_monto'')',
'and emp_id = :f_emp_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122945959074364647)
,p_name=>'P76_UGE_DESCRIPCION'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_prompt=>'Unidad de Gestion: '
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select age.age_nombre_comercial',
'  from asdm_unidades_gestion uge, asdm_agencias age',
' where age.uge_id = uge.uge_id',
'   and age.emp_id = uge.emp_id',
'   and uge.emp_id = :f_emp_id',
'   and uge.uge_id = :p76_uge_id'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122946166152364651)
,p_name=>'P76_FECHA_PROVISION'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Nueva Provision: '
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select decode(to_number(substr(lpad(:p76_provision, 6, 0) , 0, 2)),',
'              01,',
'              ''ENERO'',',
'              02,',
'              ''FEBRERO'',',
'              03,',
'              ''MARZO'',',
'              04,',
'              ''ABRIL'',',
'              05,',
'              ''MAYO'',',
'              06,',
'              ''JUNIO'',',
'              07,',
'              ''JULIO'',',
'              08,',
'              ''AGOSTO'',',
'              09,',
'              ''SEPTIEMBRE'',',
'              10,',
'              ''OCTUBRE'',',
'              11,',
'              ''NOVIEMBRE'',',
'              12,',
'              ''DICIEMBRE'') || ''/'' || to_number(substr(lpad(:p76_provision, 6, 0) , 3, 6))',
'  from dual;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534862053046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
,p_attribute_05=>'PLAIN'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122946371431364651)
,p_name=>'P76_PROVISION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_prompt=>'Provision'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_item(
 p_id=>wwv_flow_imp.id(122946557887364651)
,p_name=>'P76_UGE_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_imp.id(122945354466364636)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Uge Id'
,p_source=>':F_UGE_ID'
,p_source_type=>'EXPRESSION'
,p_source_language=>'PLSQL'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_cAttributes=>'nowrap="nowrap"'
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_imp.id(270534960270046675)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_encrypt_session_state_yn=>'N'
,p_attribute_01=>'N'
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(122948253336364672)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_genera_provision'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'pq_car_provisiones.pr_provisiones(pn_emp_id       => :f_emp_id,',
'                     pn_uge_id       => :f_uge_id,',
'                     pn_uge_id_gasto => :f_uge_id_gasto,',
'                     pn_usu_id       => :f_user_id,',
'                     pn_mes_anio     => :P76_PROVISION,',
'                     pn_pue_id       => :p0_pue_id,',
'                     pn_tnp_id       => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_monto''),',
'                     pv_error        => :p0_error);',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_process_error_message=>'Error'
,p_process_when_button_id=>wwv_flow_imp.id(122945552998364645)
,p_process_success_message=>'Provisiones Generadas Correctamente'
,p_internal_uid=>90695102066599746
);
wwv_flow_imp_page.create_page_process(
 p_id=>wwv_flow_imp.id(122948464592364690)
,p_process_sequence=>20
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'pr_actualiza_Clientes'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'',
'LN_MES VARCHAR2(6) := null;',
'begin',
'',
':p76_provision := pq_car_provisiones.fn_retorna_nueva_prov(pn_emp_id => :f_emp_id,',
'                                                      pn_uge_id => :f_uge_id,',
'                                                      pn_tnp_id => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_monto''));',
'',
'IF apex_collection.collection_exists(p_collection_name => ''COL_PROVISIONES_MONTOS'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COL_PROVISIONES_MONTOS'');',
'end if;',
'IF apex_collection.collection_exists(p_collection_name => ''COL_PROV_MONTOS_DET'') THEN',
'      apex_collection.delete_collection(p_collection_name => ''COL_PROV_MONTOS_DET'');',
'end if;',
'',
'--actualizo los clientes excuidos',
'pq_car_cartera_interfaz.pr_clientes_mayoreo_nc(pn_emp_id => :f_emp_id,',
'                       pv_error  => :p0_error);',
'',
'',
'--cargo coleccion de las provisiones',
'pq_car_provisiones.pr_col_provisiones(pn_emp_id       => :f_emp_id,',
'                                      pn_uge_id       => :f_uge_id,',
'                                      pn_mes_anio     => :p76_provision,',
'                                      pn_tnp_id       => pq_constantes.fn_retorna_constante(null,',
'                                                                                                      ''cn_tnp_id_reb_monto''),',
'                                      pn_age_id       => :f_age_id_agencia,',
'                                      pv_error        => :p0_error);',
'',
'',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_internal_uid=>90695313322599764
);
wwv_flow_imp.component_end;
end;
/
